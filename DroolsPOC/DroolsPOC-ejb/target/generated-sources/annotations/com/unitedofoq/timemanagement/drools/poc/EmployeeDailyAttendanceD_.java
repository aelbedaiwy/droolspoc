package com.unitedofoq.timemanagement.drools.poc;

import com.unitedofoq.timemanagement.drools.poc.EmployeeD;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-01-24T16:08:01")
@StaticMetamodel(EmployeeDailyAttendanceD.class)
public class EmployeeDailyAttendanceD_ { 

    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Long> id;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, String> dayNature;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Integer> timeInHour;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Integer> overTimeAfterDayEnd;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Date> dayDate;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Integer> earlyLeave;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Integer> timeOutHour;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Integer> delay;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, String> dayName;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, EmployeeD> employee;
    public static volatile SingularAttribute<EmployeeDailyAttendanceD, Integer> overTimeBeforeDayStart;

}