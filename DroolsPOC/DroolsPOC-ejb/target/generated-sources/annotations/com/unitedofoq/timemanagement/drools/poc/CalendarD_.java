package com.unitedofoq.timemanagement.drools.poc;

import com.unitedofoq.timemanagement.drools.poc.OverTimeSegmentD;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-01-24T16:08:01")
@StaticMetamodel(CalendarD.class)
public class CalendarD_ { 

    public static volatile SingularAttribute<CalendarD, Long> id;
    public static volatile SingularAttribute<CalendarD, OverTimeSegmentD> otDaySegment;
    public static volatile SingularAttribute<CalendarD, Integer> delayTolerance;
    public static volatile SingularAttribute<CalendarD, OverTimeSegmentD> otNightSegment;
    public static volatile SingularAttribute<CalendarD, Integer> plannedInHour;
    public static volatile SingularAttribute<CalendarD, String> name;
    public static volatile SingularAttribute<CalendarD, Integer> plannedOutHour;
    public static volatile SingularAttribute<CalendarD, List> offDays;

}