package com.unitedofoq.timemanagement.drools.poc;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-01-24T16:08:01")
@StaticMetamodel(OverTimeSegmentD.class)
public class OverTimeSegmentD_ { 

    public static volatile SingularAttribute<OverTimeSegmentD, Long> id;
    public static volatile SingularAttribute<OverTimeSegmentD, Integer> timeTo;
    public static volatile SingularAttribute<OverTimeSegmentD, Integer> timeFrom;

}