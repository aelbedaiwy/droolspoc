package com.unitedofoq.timemanagement.drools.poc;

import com.unitedofoq.timemanagement.drools.poc.CalendarD;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-01-24T16:08:01")
@StaticMetamodel(EmployeeD.class)
public class EmployeeD_ { 

    public static volatile SingularAttribute<EmployeeD, Long> id;
    public static volatile SingularAttribute<EmployeeD, String> name;
    public static volatile SingularAttribute<EmployeeD, CalendarD> calendar;

}