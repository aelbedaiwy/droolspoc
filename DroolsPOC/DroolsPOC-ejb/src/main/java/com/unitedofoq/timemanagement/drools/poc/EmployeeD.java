/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.timemanagement.drools.poc;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author dev
 */
@Entity
public class EmployeeD implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String name;
    @ManyToOne
    @JoinColumn(name = "calendar_id", referencedColumnName = "id")
    private CalendarD calendar;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CalendarD getCalendar() {
        return calendar;
    }

    public void setCalendar(CalendarD calendar) {
        this.calendar = calendar;
    }

    @Override
    public String toString() {
        return "EmployeeD{" + "id=" + id + ", name=" + name + ", calendar=" + calendar + '}';
    }

}
