/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.timemanagement.drools.poc;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author dev
 */
@Entity
public class EmployeeDailyAttendanceD implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dayDate;
    private String dayNature;
    private int timeInHour;
    private int timeOutHour;
    private int delay;
    private int earlyLeave;
    private int overTimeBeforeDayStart;
    private int overTimeAfterDayEnd;
    private String dayName;
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private EmployeeD employee;

    public Date getDayDate() {
        return dayDate;
    }

    public void setDayDate(Date dayDate) {
        this.dayDate = dayDate;
    }

    public String getDayNature() {
        return dayNature;
    }

    public void setDayNature(String dayNature) {
        this.dayNature = dayNature;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getOverTimeMinutes() {
        return overTimeBeforeDayStart;
    }

    public void setOverTimeMinutes(int OverTimeMinutes) {
        this.overTimeBeforeDayStart = OverTimeMinutes;
    }

    public EmployeeD getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeD employee) {
        this.employee = employee;
    }

    public int getTimeInHour() {
        return timeInHour;
    }

    public void setTimeInHour(int timeInHour) {
        this.timeInHour = timeInHour;
    }

    public int getTimeOutHour() {
        return timeOutHour;
    }

    public void setTimeOutHour(int timeOutHour) {
        this.timeOutHour = timeOutHour;
    }

    public int getOverTimeBeforeDayStart() {
        return overTimeBeforeDayStart;
    }

    public void setOverTimeBeforeDayStart(int OverTimeBeforeDayStart) {
        this.overTimeBeforeDayStart = OverTimeBeforeDayStart;
    }

    public int getOverTimeAfterDayEnd() {
        return overTimeAfterDayEnd;
    }

    public void setOverTimeAfterDayEnd(int OverTimeAfterDayEnd) {
        this.overTimeAfterDayEnd = OverTimeAfterDayEnd;
    }

    public int getEarlyLeave() {
        return earlyLeave;
    }

    public void setEarlyLeave(int earlyLeave) {
        this.earlyLeave = earlyLeave;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    @Override
    public String toString() {
        return "EmployeeDailyAttendanceD{" + "id=" + id + ", dayDate=" + dayDate + ", dayNature=" + dayNature + ", timeInHour=" + timeInHour + ", timeOutHour=" + timeOutHour + ", delay=" + delay + ", earlyLeave=" + earlyLeave + ", overTimeBeforeDayStart=" + overTimeBeforeDayStart + ", overTimeAfterDayEnd=" + overTimeAfterDayEnd + ", dayName=" + dayName + ", employee=" + employee + '}';
    }

}
