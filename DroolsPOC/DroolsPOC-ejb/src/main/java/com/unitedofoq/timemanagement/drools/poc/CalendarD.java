/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.timemanagement.drools.poc;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author dev
 */
@Entity
public class CalendarD implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    private int delayTolerance;
    private String name;
    private int plannedInHour;
    private int plannedOutHour;
    @ManyToOne
    @JoinColumn(name = "otDaySegment_id", referencedColumnName = "id")
    private OverTimeSegmentD otDaySegment;
    @ManyToOne
    @JoinColumn(name = "otNightSegment_id", referencedColumnName = "id")
    private OverTimeSegmentD otNightSegment;
    private List<Date> offDays;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OverTimeSegmentD getOtDaySegment() {
        return otDaySegment;
    }

    public void setOtDaySegment(OverTimeSegmentD otDaySegment) {
        this.otDaySegment = otDaySegment;
    }

    public int getPlannedInHour() {
        return plannedInHour;
    }

    public void setPlannedInHour(int plannedInHour) {
        this.plannedInHour = plannedInHour;
    }

    public int getPlannedOutHour() {
        return plannedOutHour;
    }

    public void setPlannedOutHour(int plannedOutHour) {
        this.plannedOutHour = plannedOutHour;
    }

    public OverTimeSegmentD getOtNightSegment() {
        return otNightSegment;
    }

    public void setOtNightSegment(OverTimeSegmentD otNightSegment) {
        this.otNightSegment = otNightSegment;
    }

    public List<Date> getOffDays() {
        return offDays;
    }

    public void setOffDays(List<Date> offDays) {
        this.offDays = offDays;
    }

    public int getDelayTolerance() {
        return delayTolerance;
    }

    public void setDelayTolerance(int delayTolerance) {
        this.delayTolerance = delayTolerance;
    }

    @Override
    public String toString() {
        return "CalendarD{" + "id=" + id + ", name=" + name + ", plannedInHour=" + plannedInHour + ", plannedOutHour=" + plannedOutHour + ", otDaySegment=" + otDaySegment + ", otNightSegment=" + otNightSegment + ", offDays=" + offDays + '}';
    }

}
