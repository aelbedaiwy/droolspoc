/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.sessionbean;

import com.unitedofoq.droolspoc.drools.DroolsServiceLocal;
import com.unitedofoq.timemanagement.drools.poc.EmployeeAppraisal;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dev
 */
@Stateless
public class EmployeeAppraisalFacade extends AbstractFacade<EmployeeAppraisal> implements EmployeeAppraisalFacadeLocal {

    @PersistenceContext(unitName = "com.unitedofoq_DroolsPOC-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @EJB
    private DroolsServiceLocal droolsService;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeAppraisalFacade() {
        super(EmployeeAppraisal.class);
    }

    @Override
    public void fireRules(Object o) {

    }

}
