/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.drools;

import com.unitedofoq.timemanagement.drools.poc.EmployeeDailyAttendanceD;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dev
 */
@Local
public interface DroolsServiceLocal {

    List<EmployeeDailyAttendanceD> fireRules(List<EmployeeDailyAttendanceD> o);

    void appendRules(String rule);
}
