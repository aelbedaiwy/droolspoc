/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.sessionbean;

import com.unitedofoq.timemanagement.drools.poc.CalendarD;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dev
 */
@Local
public interface CalendarDFacadeLocal {

    void create(CalendarD calendarD);

    void edit(CalendarD calendarD);

    void remove(CalendarD calendarD);

    CalendarD find(Object id);

    List<CalendarD> findAll();

    List<CalendarD> findRange(int[] range);

    int count();

}
