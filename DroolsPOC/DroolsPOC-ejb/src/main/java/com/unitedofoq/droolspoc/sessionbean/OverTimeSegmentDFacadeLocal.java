/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.sessionbean;

import com.unitedofoq.timemanagement.drools.poc.OverTimeSegmentD;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dev
 */
@Local
public interface OverTimeSegmentDFacadeLocal {

    void create(OverTimeSegmentD overTimeSegmentD);

    void edit(OverTimeSegmentD overTimeSegmentD);

    void remove(OverTimeSegmentD overTimeSegmentD);

    OverTimeSegmentD find(Object id);

    List<OverTimeSegmentD> findAll();

    List<OverTimeSegmentD> findRange(int[] range);

    int count();

}
