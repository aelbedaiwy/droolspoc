/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.drools;

import com.unitedofoq.timemanagement.drools.poc.EmployeeDailyAttendanceD;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.ReleaseId;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 *
 * @author dev
 */
@Stateless
public class DroolsService implements DroolsServiceLocal {

    @EJB
    public com.unitedofoq.droolspoc.sessionbean.EmployeeDailyAttendanceDFacadeLocal employeeDailyAttendanceDFacade;

    @Override
    public List<EmployeeDailyAttendanceD> fireRules(List<EmployeeDailyAttendanceD> o) {
        try {
            // load up the knowledge base
//            KieServices ks = KieServices.Factory.get();
//            KieContainer kContainer = ks.getKieClasspathContainer();
//            KieSession kSession = kContainer.newKieSession("ksession-rules");
//            kSession.addEventListener(new DebugAgendaEventListener());
//            kSession.addEventListener(new DebugRuleRuntimeEventListener());
//            kSession.update(null, o);
//            // go !
//            for (int i = 0; i < o.size(); i++) {
//                kSession.insert(o.get(i));
//            }
//            kSession.fireAllRules();
//            if (kSession.getKieBase().getKiePackage("rules") != null) {
//                kSession.getKieBase().removeKiePackage("rules");
//            }
            long load_start_time = System.nanoTime();
            KieServices kieServices = KieServices.Factory.get();
            KieFileSystem kfs = kieServices.newKieFileSystem();

            String ruleFilePath = "src/main/resources/rules/rule.drl";
            FileInputStream fis = null;
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("rules/rule.drl").getFile());
            try {
                fis = new FileInputStream(file.getAbsoluteFile());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            kfs = kfs.write(ruleFilePath, kieServices.getResources().newInputStreamResource(fis));

            KieBuilder kieBuilder = kieServices.newKieBuilder(kfs).buildAll();
            Results results = kieBuilder.getResults();
            if (results.hasMessages(Message.Level.ERROR)) {
                System.out.println("~~ERROR~~:");
                System.out.println(results.getMessages());
                throw new IllegalStateException("### errors ###");
            }
            ReleaseId releaseId1 = kieServices.newReleaseId("org.default", "artifact", "1.0.0-SNAPSHOT");
            KieContainer kieContainer = kieServices.newKieContainer(releaseId1);

            KieBase kieBase = kieContainer.newKieBase(kieServices.newKieBaseConfiguration());
            long load_end_time = System.nanoTime();
            double load_time = (load_end_time - load_start_time) / 1e6;
            Logger.getLogger(DroolsService.class.getName()).log(Level.SEVERE, "Time Elapsed for loading container*****" + load_time + "******");

            KieSession kSession = kieBase.newKieSession();
            long fire_rule_start_time = System.nanoTime();
            for (int i = 0; i < o.size(); i++) {
                kSession.insert(o.get(i));
            }
            //kSession.setGlobal("em", employeeDailyAttendanceDFacade);
            kSession.fireAllRules();

            long fire_rule_end_time = System.nanoTime();
            double fire_time = (fire_rule_end_time - fire_rule_start_time) / 1e6;
            Logger.getLogger(DroolsService.class.getName()).log(Level.SEVERE, "Time Elapsed for firing rules*****" + fire_time + "******");
            kSession.dispose();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return o;
    }

    @Override
    public void appendRules(String rule) {
        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("rules/rule.drl").getFile());
        try {
            BufferedWriter bw = null;
            FileWriter fw = null;
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(rule);
            bw.close();
        } catch (Exception ex) {

        }
    }
}
