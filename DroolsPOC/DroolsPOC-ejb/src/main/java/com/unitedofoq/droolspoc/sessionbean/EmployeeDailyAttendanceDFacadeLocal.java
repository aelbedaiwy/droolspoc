/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.sessionbean;

import com.unitedofoq.timemanagement.drools.poc.EmployeeDailyAttendanceD;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author dev
 */
@Remote
public interface EmployeeDailyAttendanceDFacadeLocal {

    void create(EmployeeDailyAttendanceD employeeDailyAttendanceD);

    void edit(EmployeeDailyAttendanceD employeeDailyAttendanceD);

    void remove(EmployeeDailyAttendanceD employeeDailyAttendanceD);

    EmployeeDailyAttendanceD find(Object id);

    List<EmployeeDailyAttendanceD> findAll();

    List<EmployeeDailyAttendanceD> findRange(int[] range);

    int count();

    public List<EmployeeDailyAttendanceD> fireRules(List<EmployeeDailyAttendanceD> o);
}
