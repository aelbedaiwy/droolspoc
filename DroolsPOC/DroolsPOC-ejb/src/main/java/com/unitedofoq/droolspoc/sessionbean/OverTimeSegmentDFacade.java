/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.sessionbean;

import com.unitedofoq.timemanagement.drools.poc.OverTimeSegmentD;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dev
 */
@Stateless
public class OverTimeSegmentDFacade extends AbstractFacade<OverTimeSegmentD> implements OverTimeSegmentDFacadeLocal {
    @PersistenceContext(unitName = "com.unitedofoq_DroolsPOC-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OverTimeSegmentDFacade() {
        super(OverTimeSegmentD.class);
    }

}
