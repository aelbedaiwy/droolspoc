/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.sessionbean;

import com.unitedofoq.timemanagement.drools.poc.EmployeeAppraisal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dev
 */
@Local
public interface EmployeeAppraisalFacadeLocal {

    void create(EmployeeAppraisal employeeAppraisal);

    void edit(EmployeeAppraisal employeeAppraisal);

    void remove(EmployeeAppraisal employeeAppraisal);

    EmployeeAppraisal find(Object id);

    List<EmployeeAppraisal> findAll();

    List<EmployeeAppraisal> findRange(int[] range);

    int count();

    void fireRules(Object o);
}
