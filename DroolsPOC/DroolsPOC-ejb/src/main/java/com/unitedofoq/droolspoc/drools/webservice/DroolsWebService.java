/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.drools.webservice;

import com.unitedofoq.droolspoc.drools.DroolsServiceLocal;
import com.unitedofoq.timemanagement.drools.poc.EmployeeDailyAttendanceD;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author dev
 */
@WebService(serviceName = "DroolsWebService")
@Stateless()
public class DroolsWebService {

    @EJB
    private DroolsServiceLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Web Service > Add Operation"

    @WebMethod(operationName = "fireRules")
    public List<EmployeeDailyAttendanceD> fireRules(@WebParam(name = "o") List<EmployeeDailyAttendanceD> o) {
        return ejbRef.fireRules(o);
    }

}
