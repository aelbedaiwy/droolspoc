/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.droolspoc.sessionbean;

import com.unitedofoq.timemanagement.drools.poc.EmployeeD;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dev
 */
@Local
public interface EmployeeDFacadeLocal {

    void create(EmployeeD employeeD);

    void edit(EmployeeD employeeD);

    void remove(EmployeeD employeeD);

    EmployeeD find(Object id);

    List<EmployeeD> findAll();

    List<EmployeeD> findRange(int[] range);

    int count();

}
