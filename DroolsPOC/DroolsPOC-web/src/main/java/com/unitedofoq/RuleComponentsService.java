package com.unitedofoq;

import com.unitedofoq.frameworkpoc.Entities.RuleComponents;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author mohamed
 */
@ManagedBean(name = "ruleComponentsService")
@SessionScoped
public class RuleComponentsService {

    @EJB
    private com.unitedofoq.droolspoc.drools.DroolsServiceLocal droolsService;

    public void copyRuleToDrlFile(String name, int priority) {

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("DrlRules/" + name.replaceAll(" ", "") + ".drl").getFile());
            String rule = new Scanner(file).useDelimiter("\\Z").next();
            droolsService.appendRules("\n" + rule + "\n");
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public ArrayList<RuleComponents> generateAllRulesFromJSONFile() {
        ArrayList<RuleComponents> rulesList = null;
        rulesList = new ArrayList<RuleComponents>();
        JsonObject childJSONObject;
        try {
            String JSON_FILE = "ruleComponents.json";
            InputStream fis = new FileInputStream(JSON_FILE);

            JsonReader jsonReader = Json.createReader(fis);
            JsonArray jsonArray = jsonReader.readArray();
            jsonReader.close();
            fis.close();
            for (int i = 0; i < jsonArray.size(); i++) {
                RuleComponents ruleComponents = new RuleComponents();
                childJSONObject = jsonArray.getJsonObject(i);
                ruleComponents.setId(i);
                ruleComponents.setName(childJSONObject.getString("name"));
                ruleComponents.setHaveParameters(childJSONObject.getBoolean("haveParameter"));
                ruleComponents.setDescription(childJSONObject.getString("description"));

                rulesList.add(ruleComponents);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return rulesList;
    }

}
