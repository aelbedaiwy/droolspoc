package com.unitedofoq.frameworkpoc.Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 *
 * @author mohamed
 */
public class RuleComponents implements Serializable {

    public RuleComponents() {
    }

    public RuleComponents(int id, String name, Boolean haveParameters, String description, int priority, List<Parameters> parameters) {
        this.id = id;
        this.name = name;
        this.haveParameters = haveParameters;
        this.description = description;
        this.priority = priority;
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHaveParameters() {
        return haveParameters;
    }

    public void setHaveParameters(Boolean haveParameters) {
        this.haveParameters = haveParameters;
    }

    public List<Parameters> getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = (List<Parameters>) parameters;
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    String name;
    Boolean haveParameters;
    String description;
    int priority;

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @OneToMany
    @JoinColumn(name = "id", referencedColumnName = "id")
    List<Parameters> parameters;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RuleComponents other = (RuleComponents) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.frameworkpoc.Entities.RuleComponents[ id=" + id + " ]";
    }

}
