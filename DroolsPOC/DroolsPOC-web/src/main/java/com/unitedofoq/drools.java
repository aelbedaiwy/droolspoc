/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq;

import com.unitedofoq.timemanagement.drools.poc.CalendarD;
import com.unitedofoq.timemanagement.drools.poc.EmployeeD;
import com.unitedofoq.timemanagement.drools.poc.EmployeeDailyAttendanceD;
import com.unitedofoq.timemanagement.drools.poc.OverTimeSegmentD;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author dev
 */
@ManagedBean(name = "drools")
@ViewScoped
public class drools {

    @EJB
    private com.unitedofoq.droolspoc.sessionbean.CalendarDFacadeLocal calendarDFacade;
    @EJB
    private com.unitedofoq.droolspoc.sessionbean.EmployeeDFacadeLocal employeeDFacade;
    @EJB
    private com.unitedofoq.droolspoc.sessionbean.OverTimeSegmentDFacadeLocal overTimeSegmentDFacade;
    @EJB
    private com.unitedofoq.droolspoc.sessionbean.EmployeeDailyAttendanceDFacadeLocal employeeDailyAttendanceDFacade;
    /**
     * Creates a new instance of drools
     */
    private List all = new ArrayList();
    List<Date> offDays = new ArrayList<>();
    List<EmployeeDailyAttendanceD> data = new ArrayList<>();
    int plannedIn;
    int plannedOut;
    int timeIn;
    int timeOut;

    public drools() {
    }

    @PostConstruct
    public void init() {
        OverTimeSegmentD otNightSegment = new OverTimeSegmentD();
        otNightSegment.setTimeFrom(13);
        otNightSegment.setTimeTo(15);
        OverTimeSegmentD otDaySegment = new OverTimeSegmentD();
        otDaySegment.setTimeFrom(7);
        otDaySegment.setTimeTo(9);
        overTimeSegmentDFacade.create(otDaySegment);
        overTimeSegmentDFacade.create(otNightSegment);
        CalendarD calendar = new CalendarD();
        try {
            offDays.add(new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-06"));
            offDays.add(new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-07"));
        } catch (Exception ex) {

        }
        calendar.setOffDays(offDays);
        calendar.setName("Calendar One");
        calendar.setOtNightSegment(otNightSegment);
        calendar.setOtDaySegment(otDaySegment);
        calendar.setPlannedInHour(9);
        calendar.setPlannedOutHour(15);
        calendar.setDelayTolerance(1);
        calendarDFacade.create(calendar);
        EmployeeD employee = new EmployeeD();
        employee.setName("Aly");
        employee.setCalendar(calendar);
        employeeDFacade.create(employee);
        EmployeeDailyAttendanceD eda = new EmployeeDailyAttendanceD();
        try {
            eda.setDayDate(new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-08"));
        } catch (ParseException ex) {
            Logger.getLogger(drools.class.getName()).log(Level.SEVERE, null, ex);
        }
        eda.setDayName(new SimpleDateFormat("EE").format(eda.getDayDate()));
        eda.setEmployee(employee);
        eda.setDayNature("Abscence");
        //eda.setTimeInHour(8);
        //eda.setTimeOutHour(12);
        EmployeeDailyAttendanceD eda1 = new EmployeeDailyAttendanceD();
        try {
            eda1.setDayDate(new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-08"));
        } catch (ParseException ex) {
            Logger.getLogger(drools.class.getName()).log(Level.SEVERE, null, ex);
        }
        eda1.setDayName(new SimpleDateFormat("EE").format(eda.getDayDate()));
        eda1.setEmployee(employee);
        eda1.setTimeInHour(11);
        eda1.setTimeOutHour(15);
        employeeDailyAttendanceDFacade.create(eda);
        employeeDailyAttendanceDFacade.create(eda1);
        data.add(eda);
        plannedIn = calendar.getPlannedInHour();
        plannedOut = calendar.getPlannedOutHour();
        //data.add(eda1);
    }

    public void fireRules() {
        data.get(0).setTimeInHour(timeIn);
        data.get(0).setTimeOutHour(timeOut);
        data.get(0).setDelay(0);
        data.get(0).setEarlyLeave(0);
        data.get(0).setOverTimeAfterDayEnd(0);
        data.get(0).setOverTimeBeforeDayStart(0);
        employeeDailyAttendanceDFacade.fireRules(data);
        all.clear();
        all.addAll(data);
    }

    public void performanceTest() {
        List<EmployeeDailyAttendanceD> testData = new ArrayList();

        OverTimeSegmentD otNightSegment = new OverTimeSegmentD();

        otNightSegment.setTimeFrom(13);
        otNightSegment.setTimeTo(15);
        OverTimeSegmentD otDaySegment = new OverTimeSegmentD();
        otDaySegment.setTimeFrom(7);
        otDaySegment.setTimeTo(9);

        CalendarD calendar = new CalendarD();

        calendar.setName("Calendar One");
        calendar.setOtNightSegment(otNightSegment);

        calendar.setOtDaySegment(otDaySegment);

        calendar.setPlannedInHour(9);
        calendar.setPlannedOutHour(15);
        // calendar.setDelayTolerance(1);
        EmployeeD employee = new EmployeeD();

        employee.setName("Aly");
        employee.setCalendar(calendar);
        for (int i = 0; i < 10000; i++) {
            EmployeeDailyAttendanceD eda = new EmployeeDailyAttendanceD();
            eda.setEmployee(employee);
            eda.setTimeInHour(10);
            eda.setTimeOutHour(16);
            eda.setDayNature("Abscence");
            testData.add(eda);
        }
        employeeDailyAttendanceDFacade.fireRules(testData);
        System.out.println("ffffffffffff");
    }

    public List getAll() {
        return all;
    }

    public void setAll(List all) {
        this.all = all;
    }

    public int getPlannedIn() {
        return plannedIn;
    }

    public void setPlannedIn(int plannedIn) {
        this.plannedIn = plannedIn;
    }

    public int getPlannedOut() {
        return plannedOut;
    }

    public void setPlannedOut(int plannedOut) {
        this.plannedOut = plannedOut;
    }

    public int getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(int timeIn) {
        this.timeIn = timeIn;
    }

    public int getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

}
