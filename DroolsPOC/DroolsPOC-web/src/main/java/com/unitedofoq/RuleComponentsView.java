package com.unitedofoq;

import com.unitedofoq.frameworkpoc.Entities.RuleComponents;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.ReorderEvent;

@ManagedBean(name = "ruleComponentsView")
@SessionScoped
public class RuleComponentsView implements Serializable {

    @ManagedProperty("#{ruleComponentsService}")
    private RuleComponentsService service;

    private ArrayList<RuleComponents> ruleComponents = new ArrayList<RuleComponents>();

    private ArrayList<RuleComponents> droppedRuleComponents = new ArrayList<RuleComponents>();

    private RuleComponents selectedRuleComponent;

    private RuleComponents removeRuleComponent;

    private RuleComponents ruleComponent;

    private int rowIndex;

    @PostConstruct
    public void init() {
        ruleComponents = service.generateAllRulesFromJSONFile();
        //droppedRuleComponents.add(ruleComponents.get(0));
    }

    public void onComponentDrop(DragDropEvent ddEvent) {
        ruleComponent = ((RuleComponents) ddEvent.getData());

        droppedRuleComponents.add(ruleComponent);
        ruleComponents.remove(ruleComponent);
    }

    public void submitRule() throws IOException {
        for (int i = 0; i < droppedRuleComponents.size(); i++) {
            service.copyRuleToDrlFile(droppedRuleComponents.get(i).getName(), droppedRuleComponents.get(i).getPriority());
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Successful", "Rules Added Successfully"));
        context.getExternalContext().redirect("summary.xhtml");
    }

    public void setService(RuleComponentsService service) {
        this.service = service;
    }

    public List<RuleComponents> getRuleComponents() {
        return ruleComponents;
    }

    public void setRuleComponents(ArrayList<RuleComponents> ruleComponents) {
        this.ruleComponents = ruleComponents;
    }

    public List<RuleComponents> getDroppedRuleComponents() {
        return droppedRuleComponents;
    }

    public void setDroppedRuleComponents(ArrayList<RuleComponents> droppedRuleComponents) {
        this.droppedRuleComponents = droppedRuleComponents;
    }

    public RuleComponents getSelectedRuleComponent() {
        return selectedRuleComponent;
    }

    public void setSelectedRuleComponent(RuleComponents selectedRuleComponent) {
        this.selectedRuleComponent = selectedRuleComponent;
    }

    public RuleComponents getRemoveRuleComponent() {
        return removeRuleComponent;
    }

    public void setRemoveRuleComponent(RuleComponents removeRuleComponent) {
        this.removeRuleComponent = removeRuleComponent;
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowReorder(ReorderEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Row Moved", "From: " + (event.getFromIndex() + 1) + ", To:" + (event.getToIndex() + 1));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void deleteRow() {
        droppedRuleComponents.remove(removeRuleComponent);
        removeRuleComponent = null;
    }

}
