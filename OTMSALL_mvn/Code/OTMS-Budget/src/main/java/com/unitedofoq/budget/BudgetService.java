package com.unitedofoq.budget;

import com.unitedofoq.fabs.core.alert.entities.CustomAlert;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.budget.BudgetElement;
import com.unitedofoq.otms.budget.BudgetInstance;
import com.unitedofoq.otms.budget.EmployeeBudgetElement;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.budget.BudgetServiceLocal",
        beanInterface = BudgetServiceLocal.class)
public class BudgetService implements BudgetServiceLocal {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    EntitySetupServiceRemote entitySetupService;
    @EJB
    UserMessageServiceRemote userMsgService;

    @Override
    public OFunctionResult createBudgetInstance(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            BudgetInstance budgetInstance = (BudgetInstance) odm.getData().get(0);
            Long companyDBID = loggedUser.getUserPrefrence1();

            if (companyDBID == null) {
                oFR.addError(userMsgService.getUserMessage("UserHasNoCompany", loggedUser));
                return oFR;
            }
            List<Employee> employees = oem.loadEntityList("Employee", Arrays.asList(
                    "positionSimpleMode.unit.company.dbid = " + companyDBID), null, null, loggedUser);
            List<BudgetElement> budgetElements = oem.loadEntityList("BudgetElement", Arrays.asList(
                    "company.dbid = " + companyDBID), null, null, loggedUser);
            for (Employee employee : employees) {
                for (BudgetElement budgetElement : budgetElements) {
                    EmployeeBudgetElement employeeBudgetElement = new EmployeeBudgetElement();
                    employeeBudgetElement.setBudgetInstance(budgetInstance);
                    employeeBudgetElement.setBudgetElement(budgetElement);
                    employeeBudgetElement.setEmployee(employee);
                    oFR.append(entitySetupService.callEntityCreateAction(employeeBudgetElement, loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult calcActualTotalChangeFunction(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeBudgetElement employeeBudgetElement = (EmployeeBudgetElement) odm.getData().get(0);
            EmployeeBudgetElement empBudgetElementDB = (EmployeeBudgetElement) oem.loadEntity("EmployeeBudgetElement",
                    employeeBudgetElement.getDbid(), null, null, loggedUser);

            // If Total is modified directly
            if (empBudgetElementDB != null && empBudgetElementDB.getActualTotalValue() != null
                    && employeeBudgetElement.getActualTotalValue() != null
                    && empBudgetElementDB.getActualTotalValue().compareTo(
                            employeeBudgetElement.getActualTotalValue()) != 0) {
                return oFR;
            }
            BigDecimal actualTotal
                    = employeeBudgetElement.getActualJanValue().add(employeeBudgetElement.getActualFebValue())
                    .add(employeeBudgetElement.getActualMarValue().add(employeeBudgetElement.getActualAprValue()))
                    .add(employeeBudgetElement.getActualMayValue().add(employeeBudgetElement.getActualJunValue()))
                    .add(employeeBudgetElement.getActualJulValue().add(employeeBudgetElement.getActualAugValue()))
                    .add(employeeBudgetElement.getActualSepValue().add(employeeBudgetElement.getActualOctValue()))
                    .add(employeeBudgetElement.getActualNovValue().add(employeeBudgetElement.getActualDecValue()));

            employeeBudgetElement.setActualTotalValue(actualTotal);
            oFR.setReturnedDataMessage(odm);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult calcPlannedTotalChangeFunction(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeBudgetElement employeeBudgetElement = (EmployeeBudgetElement) odm.getData().get(0);
            EmployeeBudgetElement empBudgetElementDB = (EmployeeBudgetElement) oem.loadEntity("EmployeeBudgetElement",
                    employeeBudgetElement.getDbid(), null, null, loggedUser);

            // If Total is modified directly
            if (empBudgetElementDB != null
                    && empBudgetElementDB.getPlannedTotalValue() != null
                    && employeeBudgetElement.getPlannedTotalValue() != null
                    && empBudgetElementDB.getPlannedTotalValue().compareTo(
                            employeeBudgetElement.getPlannedTotalValue()) != 0) {
                return oFR;
            }
            BigDecimal plannedTotal
                    = employeeBudgetElement.getPlannedJanValue().add(employeeBudgetElement.getPlannedFebValue())
                    .add(employeeBudgetElement.getPlannedMarValue().add(employeeBudgetElement.getPlannedAprValue()))
                    .add(employeeBudgetElement.getPlannedMayValue().add(employeeBudgetElement.getPlannedJunValue()))
                    .add(employeeBudgetElement.getPlannedJulValue().add(employeeBudgetElement.getPlannedAugValue()))
                    .add(employeeBudgetElement.getPlannedSepValue().add(employeeBudgetElement.getPlannedOctValue()))
                    .add(employeeBudgetElement.getPlannedNovValue().add(employeeBudgetElement.getPlannedDecValue()));

            employeeBudgetElement.setPlannedTotalValue(plannedTotal);
            employeeBudgetElement.setPlannedTotalValueMask(plannedTotal);
            oFR.setReturnedDataMessage(odm);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult diffrenceChangeFunction(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        EmployeeBudgetElement empBudElm = (EmployeeBudgetElement) odm.getData().get(0);
        String fldExp = odm.getData().get(1).toString();
        Object newVal = odm.getData().get(2);

        if (fldExp.equals("plannedJanValueMask")) {
            empBudElm.setDiffJanValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualJanValueMask()));
        } else if (fldExp.equals("plannedFebValueMask")) {
            empBudElm.setDiffFebValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualFebValueMask()));
        } else if (fldExp.equals("plannedMarValueMask")) {
            empBudElm.setDiffMarValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualMarValueMask()));
        } else if (fldExp.equals("plannedAprValueMask")) {
            empBudElm.setDiffAprValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualAprValueMask()));
        } else if (fldExp.equals("plannedMayValueMask")) {
            empBudElm.setDiffMayValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualMayValueMask()));
        } else if (fldExp.equals("plannedJunValueMask")) {
            empBudElm.setDiffJunValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualJunValueMask()));
        } else if (fldExp.equals("plannedJulValueMask")) {
            empBudElm.setDiffJulValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualJulValueMask()));
        } else if (fldExp.equals("plannedAugValueMask")) {
            empBudElm.setDiffAugValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualAugValueMask()));
        } else if (fldExp.equals("plannedSepValueMask")) {
            empBudElm.setDiffSepValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualSepValueMask()));
        } else if (fldExp.equals("plannedOctValueMask")) {
            empBudElm.setDiffOctValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualOctValueMask()));
        } else if (fldExp.equals("plannedNovValueMask")) {
            empBudElm.setDiffNovValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualNovValueMask()));
        } else if (fldExp.equals("plannedDecValueMask")) {
            empBudElm.setDiffDecValueMask((new BigDecimal(newVal.toString())).subtract(empBudElm.getActualDecValueMask()));
        } else if (fldExp.equals("plannedTotalValueMask")) {
            empBudElm.setDiffTotalValueMask(((new BigDecimal(newVal.toString()))).subtract(empBudElm.getActualTotalValueMask()));

        } else if (fldExp.equals("actualJanValueMask")) {
            empBudElm.setDiffJanValueMask(empBudElm.getPlannedJanValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualFebValueMask")) {
            empBudElm.setDiffFebValueMask(empBudElm.getPlannedFebValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualMarValueMask")) {
            empBudElm.setDiffMarValueMask(empBudElm.getPlannedMarValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualAprValueMask")) {
            empBudElm.setDiffAprValueMask(empBudElm.getPlannedAprValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualMayValueMask")) {
            empBudElm.setDiffMayValueMask(empBudElm.getPlannedMayValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualJunValueMask")) {
            empBudElm.setDiffJunValueMask(empBudElm.getPlannedJunValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualJulValueMask")) {
            empBudElm.setDiffJulValueMask(empBudElm.getPlannedJulValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualAugValueMask")) {
            empBudElm.setDiffAugValueMask(empBudElm.getPlannedAugValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualSepValueMask")) {
            empBudElm.setDiffSepValueMask(empBudElm.getPlannedSepValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualOctValueMask")) {
            empBudElm.setDiffOctValueMask(empBudElm.getPlannedOctValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualNovValueMask")) {
            empBudElm.setDiffNovValueMask(empBudElm.getPlannedNovValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualDecValueMask")) {
            empBudElm.setDiffDecValueMask(empBudElm.getPlannedDecValueMask().subtract((new BigDecimal(newVal.toString()))));
        } else if (fldExp.equals("actualTotalValueMask")) {
            empBudElm.setDiffTotalValueMask(empBudElm.getPlannedTotalValueMask().subtract((new BigDecimal(newVal.toString()))));
        }
        ofr.setReturnedDataMessage(odm);
        return ofr;
    }

    @Override
    public OFunctionResult sendBudgetInstanceAlert(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<Position> positions = (List<Position>) oem.loadEntityList("Position",
                    Collections.singletonList("headcount=1"), null, null, loggedUser);
            for (int i = 0; i < positions.size(); i++) {
                Position position = positions.get(i);
                List<Employee> employees = position.getEmployeesSimpleMode();
                for (int j = 0; j < employees.size(); j++) {
                    Employee employee = employees.get(j);
                    CustomAlert alert = new CustomAlert();
                    alert.setTitle("Title");
                    alert.setMessageBody("Body");
                    alert.setMessageSubject("Subject");
                    alert.setToSQL("SELECT email from oemployee where deleted = 0 AND dbid = " + employee.getDbid());
                    entitySetupService.callEntityCreateAction(alert, loggedUser);
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }
}
