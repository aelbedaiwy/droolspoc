package com.unitedofoq.budget;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface BudgetServiceLocal {
    public OFunctionResult createBudgetInstance(ODataMessage odm, OFunctionParms params, OUser loggedUser);
    public OFunctionResult calcPlannedTotalChangeFunction(ODataMessage odm, OFunctionParms params, OUser loggedUser);
    public OFunctionResult calcActualTotalChangeFunction(ODataMessage odm, OFunctionParms params, OUser loggedUser);
    public OFunctionResult diffrenceChangeFunction(ODataMessage odm,OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult sendBudgetInstanceAlert(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);
}
