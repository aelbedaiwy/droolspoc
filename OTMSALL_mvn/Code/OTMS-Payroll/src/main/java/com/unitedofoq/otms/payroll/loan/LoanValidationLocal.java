/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author mragab
 */
@Local
public interface LoanValidationLocal {

    public OFunctionResult validateLoanRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult cancelLoan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult calculateMonthLoanValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult loanApproval(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult loanSuspend(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult loanReschedule(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult loanSettelment(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult loanEditing(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateLoanForPenalty(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult getLoanMaxValue(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult loanBalanceSheet(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult loanBalanceSheetForAll(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult importEmpLoans(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult cancelLoanSettelment(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
