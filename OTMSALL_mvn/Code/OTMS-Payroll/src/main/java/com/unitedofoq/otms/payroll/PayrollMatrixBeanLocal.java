package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author Mohamed Aboelnour
 */
@Local
public interface PayrollMatrixBeanLocal {

    public OFunctionResult payrollMatrixAffect(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult cancelPayrollMatrixAffect(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult payrollMatrixAffectForAll(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
