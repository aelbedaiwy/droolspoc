package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.costcenter.AlstomCCAllocFilter;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.gl.AlstomGlMappedAccount;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.payroll.AlstomServiceLocal",
        beanInterface = AlstomServiceLocal.class)
public class AlstomServiceBean implements AlstomServiceLocal {

    @EJB
    private UserMessageServiceRemote userMessageService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;

       /**
     *
     * @param odm
     * @param params
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult costCenterAllocation(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            AlstomCCAllocFilter ccFilter = (AlstomCCAllocFilter) odm.getData().get(0);

            CalculatedPeriod calcPeriod = ccFilter.getCalcPeriod();
            if (calcPeriod == null || (calcPeriod != null && calcPeriod.getDbid() == 0)) {
                ofr.addError(userMessageService.getUserMessage("ALSCCA000001", loggedUser));
                return ofr;
            }

            String filterResult = getFilter(ccFilter);

            String deleteSQL;
            deleteSQL = "Delete From AlstomCCAllocation Where calcPeriod_dbid = " + calcPeriod.getDbid() + filterResult;
            oem.executeEntityUpdateNativeQuery(deleteSQL, loggedUser);

            String selectStat1 = "select employee, salary_element_id, calculated_period_id, income_or_deduction, lec_net_amount, "
                    + " creditaccount, debitaccount from periodic_salary_elements Where calculated_Period_Id = " + calcPeriod.getDbid()
                    + filterResult.replace(" employee_dbid = ", " employee = ").replace(" AND employee_dbid in ", " AND employee in ");
            List periodicSalElements = oem.executeEntityListNativeQuery(selectStat1, loggedUser);
            if (periodicSalElements != null && !periodicSalElements.isEmpty()) {

                String selectStat2 = "select sapCostCode_dbid, costCodeFunction_dbid, precentage "
                        + " from empsapintcostcode "
                        + " where deleted = 0" + filterResult;
                List costCodeFunctions = oem.executeEntityListNativeQuery(selectStat2, loggedUser);
                if (costCodeFunctions != null && !costCodeFunctions.isEmpty()) {

                    Object[] periodicSE;
                    Object[] costCodeFunc;

                    BigDecimal val;
                    BigDecimal percent;
                    BigDecimal newVal;
                    String credit, debit, salaryElementDbid, costCenterDbid, ccFuncDbid, employeeDbid;
                    String insertSQL;

                    for (int i = 0; i < periodicSalElements.size(); i++) {
                        periodicSE = (Object[]) periodicSalElements.get(i);

                        val = (BigDecimal) periodicSE[4];
                        credit = periodicSE[5].toString();
                        debit = periodicSE[6].toString();
                        salaryElementDbid = periodicSE[1].toString();
                        employeeDbid = periodicSE[0].toString();

                        for (int j = 0; j < costCodeFunctions.size(); j++) {
                            costCodeFunc = (Object[]) costCodeFunctions.get(j);

                            ccFuncDbid = costCodeFunc[1].toString();
                            percent = (BigDecimal) costCodeFunc[2];
                            costCenterDbid = costCodeFunc[0].toString();

                            newVal = percent.multiply(val).divide(BigDecimal.valueOf(100));
                            insertSQL = "insert into AlstomCCAllocation(calcPeriod_dbid,employee_dbid,costCenter_dbid,ccFunction_dbid,percentage,"
                                    + "calculatedValue,salaryElement_dbid,credit,debit) values(" + calcPeriod.getDbid() + "," + employeeDbid + ","
                                    + costCenterDbid + "," + ccFuncDbid + "," + percent + "," + newVal + "," + salaryElementDbid + ","
                                    + "'" + credit + "','" + debit + "')";
                            oem.executeEntityUpdateNativeQuery(insertSQL, loggedUser);
                        }
                        
                    }
                    ofr.addSuccess(userMessageService.getUserMessage("ALSCCA000002", loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("ALSCCA000003", loggedUser));
        }
        return ofr;
    } 
    
    
        private String getFilter(FilterBase filter) {
        String whereSQL = "";
        Employee employee = filter.getEmployee();
        CostCenter costCenter = filter.getCostCenter();
        UDC location = filter.getLocation();
        UDC hiringType = filter.getHiringType();
        Position position = filter.getPosition();
        Unit unit = filter.getUnit();
        Job job = filter.getJob();
        //_____________________________Filter____________________________________
        if (employee != null && employee.getDbid() != 0) {
            whereSQL = " AND employee_dbid = " + employee.getDbid();
        }
        if (costCenter != null && costCenter.getDbid() != 0) {
            whereSQL += " AND employee_dbid in (select employee_dbid from employeepayroll where costcenter_dbid = " + costCenter.getDbid() + ") ";
        }
        if (location != null && location.getDbid() != 0) {
            whereSQL += " AND employee_dbid in (select employee_dbid from employeepayroll where location_dbid = " + location.getDbid() + ") ";
        }
        if (hiringType != null && hiringType.getDbid() != 0) {
            whereSQL += " AND employee_dbid in (select employee_dbid from employeepayroll where hiringtype_dbid = " + hiringType.getDbid() + ") ";
        }
        if (position != null && position.getDbid() != 0) {
            whereSQL += " AND employee_dbid in (select dbid from oemployee where position_dbid = " + position.getDbid() + ") ";
        }
        if (unit != null && unit.getDbid() != 0) {
            whereSQL += " AND employee_dbid in (select dbid from oemployee where position_dbid in (select dbid from position where unit_dbid =" + unit.getDbid() + ") )";
        }
        if (job != null && job.getDbid() != 0) {
            whereSQL += " AND employee_dbid in (select dbid from oemployee where position_dbid in (select dbid from position where job_dbid =" + job.getDbid() + ") )";
        }

        return whereSQL;
    }

    

    @Override
    public OFunctionResult writeCostCenterAllocationToFile(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {

        String[] monthName = {"January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"};

        final String payrollHeader = "-HEADER-PAYROLL-";
        final String payrollLines = "-LINES-PAYROLL-";
        final String provisionHeader = "-HEADER-PROVISIONS-";
        final String provisionLines = "-LINES-PROVISIONS-";

        OFunctionResult ofr = new OFunctionResult();
        List<String> fileNames = new ArrayList<String>();


        FileWriter fos = null;
        PrintWriter dos = null;

        String filePath =
                ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext()).getRealPath("/AlstomCCAlocationFiles");
        File alstomCCAlocationFilesDirectory = new File(filePath);
        if (!alstomCCAlocationFilesDirectory.exists()) {
            alstomCCAlocationFilesDirectory.mkdir();
        }

        String headerFilePath = "";

        String resultsfilePath = "";


        File alstomCCAlocationDataFile = null;

        String tmp = "";

        try {

            //alstomCCAlocationDataFile.createNewFile();

            AlstomCCAllocFilter ccFilter = (AlstomCCAllocFilter) odm.getData().get(0);

            CalculatedPeriod calcPeriod = ccFilter.getCalcPeriod();
            if (calcPeriod == null || (calcPeriod != null && calcPeriod.getDbid() == 0)) {
                ofr.addError(userMessageService.getUserMessage("ALSCCA000001", loggedUser));
                return ofr;
            }

            //select all departments dbid
            String slctDepts = "select DISTINCT Dept FROM AlsCCAlocationDet where Dept is not null";
            List<Object> deptsResultSet = new ArrayList();
            List<Long> deptsList = new ArrayList();

            deptsResultSet = (List<Object>) oem.executeEntityListNativeQuery(slctDepts, loggedUser);

            //write the result to a file
            if (deptsResultSet != null) {

                for (Object object : deptsResultSet) {
                    deptsList.add(Long.parseLong(object.toString()));
                }
            }
            String whereClause = "";
            String selectStmt = "";
            List<Object[]> resultSet;
            String payrolHeaderFileName = "";
            String payrolResultsFileName = "";
            String unitCode = "";
            String dateTimeString = "";

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy-hh-mma");
            Calendar calendar = new GregorianCalendar();
            dateTimeString = sdf.format(calendar.getTime());
            Unit dept;
            //for each dapartment, generate two files
            for (Long deptId : deptsList) {

                dept = (Unit) oem.loadEntity(Unit.class.getSimpleName(), deptId, null, null, loggedUser);
                unitCode = dept.getCode() != null ? dept.getCode() : deptId.toString();
                //alstomGLMappedAccount = (AlstomGlMappedAccount) oem.loadEntity("AlstomGlMappedAccount", alstomGLMappedAccount.getDbid(), null, null, loggedUser);
                payrolHeaderFileName = "/" + unitCode + payrollHeader + dateTimeString;
                headerFilePath = alstomCCAlocationFilesDirectory + payrolHeaderFileName + ".txt";
                fos = new FileWriter(headerFilePath);
                dos = new PrintWriter(fos);

                dos.print("H" + "\t");
                dos.print("1" + "\t");
                dos.print("PV" + "\t \t \t");
                dos.print("Payroll ");
                tmp = monthName[(calcPeriod.getMonth() - 1)];
                dos.print(tmp);
                dos.print(" ");
                dos.print(String.valueOf(calcPeriod.getYear()));

                if (dos != null) {
                    dos.close();
                }
                if (fos != null) {
                    fos.close();
                }
                //fileNames.add(headerFilePath);
                payrolResultsFileName = "/" + unitCode + payrollLines + dateTimeString;

                resultsfilePath =
                        alstomCCAlocationFilesDirectory + payrolResultsFileName + ".txt";

                alstomCCAlocationDataFile = new File(resultsfilePath);

                whereClause = " AND Dept =" + deptId.toString();

                //select the records from the database
                selectStmt = "SELECT * FROM AlsCCAlocationDet WHERE calcPeriod_dbid = " + calcPeriod.getDbid();

                selectStmt += whereClause;

                resultSet = new ArrayList();

                resultSet = (List<Object[]>) oem.executeEntityListNativeQuery(selectStmt, loggedUser);

                if ((resultSet != null) && (resultSet.size() > 0)) {
                    fos = new FileWriter(alstomCCAlocationDataFile);
                    dos = new PrintWriter(fos);
                    String account = "";
                    String debitCredit = null;
                    String debitAmount = null;
                    String creditAmount = null;
                    String costCenterCode = null;
                    String functionCode = null;
                    for (Object[] ccAllocRecord : resultSet) {
                        dos.print("I2\t");          //I2 - Fixed
                        dos.print("1\t");           //1-Fixed
                        account = ccAllocRecord[3].toString();
                        dos.print(account);             //account number
                        dos.print("\t");
                        debitCredit = ccAllocRecord[4].toString();
                        if ((debitCredit != null) && ("d".equalsIgnoreCase(debitCredit))) {
                            debitAmount = ccAllocRecord[2].toString();
                            creditAmount = null;
                            dos.print(debitAmount);
                            dos.print("\t");
                        } else if ((debitCredit != null) && ("c".equalsIgnoreCase(debitCredit))) {
                            debitAmount = null;
                            creditAmount = ccAllocRecord[2].toString();
                            dos.print("\t");
                            dos.print(creditAmount);
                        }
                        dos.print("\t");
                        costCenterCode = ccAllocRecord[7] == null ? "" : ccAllocRecord[7].toString();
                        dos.print(costCenterCode);
                        dos.print("\t");
                        functionCode = ccAllocRecord[8] == null ? "" : ccAllocRecord[8].toString();
                        dos.print(functionCode);
                        dos.print("\t\t");
                        dos.print("Payroll");
                        dos.print(" ");
                        dos.print(monthName[(calcPeriod.getMonth() - 1)]);
                        dos.print(" ");
                        dos.print(calcPeriod.getYear() + " \r\n");

                    }

                    if (dos != null) {
                        dos.close();
                    }
                    if (fos != null) {
                        fos.close();
                    }
                    fileNames.add(headerFilePath);
                    fileNames.add(resultsfilePath);
                }
            }

            //Provision File
            //select all departments dbid
            String slctDeptsProvision = "select DISTINCT Dept FROM AlsCCAlocationProvision where Dept is not null";
            List<Object> deptsResultSetProvision = new ArrayList();
            List<Long> deptsListProvision = new ArrayList();

            deptsResultSetProvision = (List<Object>) oem.executeEntityListNativeQuery(slctDeptsProvision, loggedUser);
            if (deptsResultSetProvision != null) {


                for (Object object : deptsResultSetProvision) {
                    deptsListProvision.add(Long.parseLong(object.toString()));
                }
            }
            //////////////////////////////
            String whereClauseProvision = "";
            String selectStmtProvision = "";
            List<Object[]> resultSetProvision;
            String provisionHeaderFileName = "";
            String headerFilePathProvision = "";
            String provisionResultsFileName = "";


            //for each dapartment, generate two files
            for (Long deptId : deptsListProvision) {

                dept = (Unit) oem.loadEntity(Unit.class.getSimpleName(), deptId, null, null, loggedUser);

                unitCode = dept.getCode() != null ? dept.getCode() : deptId.toString();

                //payrolResultsFileName = "/" + unitCode + payrollLines + dateTimeString;
                //resultsfilePath =
                //      alstomCCAlocationFilesDirectory + payrolResultsFileName + ".txt";

                provisionHeaderFileName = "/" + unitCode + provisionHeader + dateTimeString;

                headerFilePathProvision = alstomCCAlocationFilesDirectory + provisionHeaderFileName + ".txt";

                fos = new FileWriter(headerFilePathProvision);

                dos = new PrintWriter(fos);

                dos.print("H" + "\t");
                dos.print("1" + "\t");
                dos.print("PV" + "\t \t \t");
                dos.print("Salary Provisions ");
                tmp = monthName[(calcPeriod.getMonth() - 1)];
                dos.print(tmp);
                dos.print(" ");
                dos.print(String.valueOf(calcPeriod.getYear()));

                if (dos != null) {
                    dos.close();
                }
                if (fos != null) {
                    fos.close();
                }
                //fileNames.add(headerFilePathProvision);
                provisionResultsFileName = "/" + unitCode + provisionLines + dateTimeString;
                resultsfilePath =
                        alstomCCAlocationFilesDirectory + provisionResultsFileName + ".txt";

                alstomCCAlocationDataFile = new File(resultsfilePath);
                whereClauseProvision = " AND Dept =" + deptId.toString();

                //select the records from the database
                selectStmtProvision = "SELECT * FROM AlsCCAlocationProvision WHERE calcPeriod_dbid = " + calcPeriod.getDbid();

                selectStmtProvision += whereClauseProvision;

                resultSetProvision = new ArrayList();

                resultSetProvision = (List<Object[]>) oem.executeEntityListNativeQuery(selectStmtProvision, loggedUser);

                if ((resultSetProvision != null) && (resultSetProvision.size() > 0)) {
                    fos = new FileWriter(alstomCCAlocationDataFile);
                    dos = new PrintWriter(fos);
                    String account = "";
                    String debitCredit = null;
                    String debitAmount = null;
                    String creditAmount = null;
                    String costCenterCode = null;
                    String functionCode = null;
                    for (Object[] ccAllocRecord : resultSetProvision) {
                        dos.print("I2\t");          //I2 - Fixed
                        dos.print("1\t");           //1-Fixed
                        account = ccAllocRecord[3].toString();
                        dos.print(account);             //account number
                        dos.print("\t");
                        debitCredit = ccAllocRecord[4].toString();
                        if ((debitCredit != null) && ("d".equalsIgnoreCase(debitCredit))) {
                            debitAmount = ccAllocRecord[2].toString();
                            creditAmount = null;
                            dos.print(debitAmount);
                            dos.print("\t");
                        } else if ((debitCredit != null) && ("c".equalsIgnoreCase(debitCredit))) {
                            debitAmount = null;
                            creditAmount = ccAllocRecord[2].toString();
                            dos.print("\t");
                            dos.print(creditAmount);
                        }
                        dos.print("\t");
                        costCenterCode = ccAllocRecord[7] == null ? "" : ccAllocRecord[7].toString();
                        dos.print(costCenterCode);
                        dos.print("\t");
                        functionCode = ccAllocRecord[8] == null ? "" : ccAllocRecord[8].toString();
                        dos.print(functionCode);
                        dos.print("\t\t");
                        dos.print("Salary Provisions");
                        dos.print(" ");
                        dos.print(monthName[(calcPeriod.getMonth() - 1)]);
                        dos.print(" ");
                        dos.print(calcPeriod.getYear() + " \r\n");

                    }

                    if (dos != null) {
                        dos.close();
                    }
                    if (fos != null) {
                        fos.close();
                    }
                    fileNames.add(headerFilePathProvision);
                    fileNames.add(resultsfilePath);
                }
            }
            //check if filenames are empty, return error
            if (fileNames.isEmpty()) {
                ofr.addError(userMessageServiceRemote.getUserMessage("EXAlSCC000001", loggedUser));
                return ofr;
            }

            ////////////////////
            String zipfileName =
                    alstomCCAlocationFilesDirectory + "/alstomCCAllocationResults" + new Date().getTime() + ".zip";
            //zip the files
            FileOutputStream zipFos = new FileOutputStream(zipfileName);
            ZipOutputStream zos = new ZipOutputStream(zipFos);
            byte[] buffer = new byte[1024];
            Object[] fileNamesArray = fileNames.toArray();
            int slashIndex = -1;
            String fileNameWithoutPath = "";
            File fileObject;
            for (Object fileName : fileNamesArray) {
                String fileNameString = fileName.toString();

                slashIndex = fileNameString.lastIndexOf("/");
                fileNameWithoutPath = fileNameString.substring(slashIndex + 1);
                ZipEntry ze = new ZipEntry((String) fileNameWithoutPath);
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream((String) fileName);

                int len;
                //byte[] buffer = null;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
                zos.closeEntry();
                fileObject = new File(fileNameString);
                fileObject.delete();
            }

            //remember close it
            zos.close();


            ofr.addReturnValue(zipfileName);
            //success message
            ofr.addSuccess(userMessageService.getUserMessage("EXALSCC000002", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("EXALSCC000003", loggedUser));
        }
        return ofr;


    }


    /**
     * Alstom Gl Mapped Account Setup -- combination of the salary element and
     * location
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult insertAlstomGLMappedAccount(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        String insertQuery;
        Long glMappedACcountDbid;
        try {
            AlstomGlMappedAccount alstomGLMappedAccount = (AlstomGlMappedAccount) odm.getData().get(0);
            if (alstomGLMappedAccount != null) {
                alstomGLMappedAccount = (AlstomGlMappedAccount) oem.loadEntity("AlstomGlMappedAccount", alstomGLMappedAccount.getDbid(), null, null, loggedUser);

                if (alstomGLMappedAccount.getGlMappedAccount() == null) {
                    glMappedACcountDbid = Long.valueOf("0");
                } else {

                    glMappedACcountDbid = alstomGLMappedAccount.getGlMappedAccount().getDbid();
                }
                if (glMappedACcountDbid == 0) {
                    insertQuery = "insert into glmappedaccount ( salaryelement_dbid , "
                            + "location_dbid, creditaccount, debitaccount)values ( "
                            + alstomGLMappedAccount.getSalaryElement().getDbid() + ","
                            + alstomGLMappedAccount.getLocation().getDbid() + ","
                            + "'" + alstomGLMappedAccount.getCreditAccount() + "'" + ","
                            + "'" + alstomGLMappedAccount.getDebitAccount() + "'" + " )";

                    oem.executeEntityUpdateNativeQuery(insertQuery, loggedUser);

                    String updateQuery = "update AlsGL  set AlsGL.glmappedaccount_dbid = GL.DBID"
                            + " from AlstomGlMappedAccount AlsGL , glmappedaccount GL "
                            + " where AlsGL.salaryelement_dbid = GL.salaryelement_dbid"
                            + " and AlsGL.location_dbid = GL.location_dbid"
                            + " and AlsGL.salaryelement_dbid = " + alstomGLMappedAccount.getSalaryElement().getDbid()
                            + " and AlsGL.location_dbid = " + alstomGLMappedAccount.getLocation().getDbid();

                    oem.executeEntityUpdateNativeQuery(updateQuery, loggedUser);
                } else {
                    insertQuery = "update glmappedaccount "
                            + "set salaryelement_dbid = " + alstomGLMappedAccount.getSalaryElement().getDbid()
                            + " , location_dbid = " + alstomGLMappedAccount.getLocation().getDbid()
                            + " , creditaccount = " + "'" + alstomGLMappedAccount.getCreditAccount() + "'"
                            + " , debitaccount = " + "'" + alstomGLMappedAccount.getDebitAccount() + "'"
                            + " where dbid = " + glMappedACcountDbid;
                    oem.executeEntityUpdateNativeQuery(insertQuery, loggedUser);
                }
            }


        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }
    /*
     * a method to select data from the database and write them to a file
     */
}
