package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface PayrollValidationLocal {

    OFunctionResult validateSalaryElement(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    OFunctionResult validateGeneratedCode(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    OFunctionResult inActivateEmployeeForPayrollClosing(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    OFunctionResult calculatedPeriodClosing(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validatePaymetMethod(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult GetPaymetMethod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult runPayrollCalcutaionWrapper(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult getAccumlatedNetSal(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runPersonalClosingWrapper(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateCashOrBank(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult payrollChangeAction(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult salaryElementInActive(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult payrollCalculationValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult salaryElementDescDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult mobileBillDeduction(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult employeeBillChangeAction(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateFixedSalaryElementMinMaxVal(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult createGradeSalaryElementsForSalaryElement(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult createGradeSalaryElementsForPayGrade(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult updateEmployeeSalaryElementsWithGradeMid(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult getESEOldValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runRetroCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult applySalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult cancelSalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult updateGradeSalaryElementsForSalaryElement(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult salaryUpgradePercent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult postSalaryElementFormula(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateSalaryUpgrade(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateEmpSalaryUpgrade(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult modifySalaryUpgrade(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult salaryUpgradePercentChangeAction(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult importEmpSalaryUpgrade(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult importSalaryUpgradeValidation(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult costCenterCalculation(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult setEmployeeCostCenter(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateCCTotalDistPercentage(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateEmpCostCenter(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult runPayCalcWarapperNewTx(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser, String wsCode);

    public OFunctionResult updateEmployeeDeductionsIncentivePenalty(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult updateEmployeeDeductionsIncentiveVacation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult updateEmployeeDeductionsIncentiveAbsence(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult postDeleteSEFormula(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult convertFromNetToGross(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult netToGrossCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult createEmpSEAudit(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult changeESEPayMethod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runPersonalClosingWrapperNewTx(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runPRSClosingAndPayrollCalcProcess(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult applySalaryUpgradeServerJob(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult applySEMaintenance(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult netToGrossCalculationPreSave(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult importEmployeeSalaryElementHistory(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult generateRelatedCalculatedPeriods(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
