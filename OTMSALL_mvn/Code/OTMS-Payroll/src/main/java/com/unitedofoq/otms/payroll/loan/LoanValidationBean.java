package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryElement;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.personnel.penalty.EmployeePenaltyRequest;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.sql.DataSource;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.payroll.loan.LoanValidationLocal", beanInterface = LoanValidationLocal.class)
public class LoanValidationBean implements LoanValidationLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private WSUtilityLocal webServiceUtility;
    @EJB
    private DataEncryptorServiceLocal dataEncryptorServiceLocal;
    @EJB
    private DataSecurityServiceLocal dataSecurityServiceLocal;

    private DateTimeUtility dateTimeUtility = new DateTimeUtility();

    @Override
    public OFunctionResult validateLoanRequest(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(1);
            BigDecimal loanMinValue, loanMaxValue, loanValue;
            Loan loan;
            loan = employeeLoan.getLoan();
            loanMinValue = loan.getMinMonthValue();
            // <editor-fold defaultstate="collapsed"
            // desc="calculate maxValue for loan">
//            Date employeeHiringDate = employeeLoan.getEmployee()
//                    .getHiringDate();
            Integer startYear = employeeLoan.getStartYear();
            Integer startMonth = employeeLoan.getStartMonth();
//            if (employeeHiringDate != null) {
//                SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy");
//                String year = simpleDateformat.format(employeeHiringDate);
//
//                SimpleDateFormat monthDateformat = new SimpleDateFormat("MM");
//                String month = monthDateformat.format(employeeHiringDate);
//
//                if (Integer.parseInt(year) > startYear
//                        || (Integer.parseInt(month) > startMonth && Integer
//                        .parseInt(year) == startYear)) {
//                    functionResult.addError(userMessageServiceRemote
//                            .getUserMessage("InvalidLoanDate", loggedUser));
//                    return functionResult;
//                }
//                functionResult.append(calculateLoanMaximumValue(employeeLoan.getEmployee(), employeeLoan.getLoan(), year, loggedUser));
//                if (functionResult.getReturnValues() != null && !functionResult.getReturnValues().isEmpty()) {
//                    loanMaxValue = new BigDecimal(functionResult.getReturnValues().get(0).toString());
//                } else {
//                    loanMaxValue = new BigDecimal(0);
//                }
//
//                if (loanMaxValue.compareTo(employeeLoan.getLoanValue()) < 0) {
//                    List<String> errorParameters = new ArrayList<String>();
//                    errorParameters.add(loanMaxValue.setScale(2,
//                            RoundingMode.UP).toString());
//                    functionResult.addError(userMessageServiceRemote
//                            .getUserMessage("LoanExceedsMaxValue",
//                                    errorParameters, loggedUser));
//                }
//            }
//            // </editor-fold>
//            // Validate Loan Value
//            loanValue = employeeLoan.getLoanValue();
//            if (loanValue == null || loanValue.doubleValue() <= 0) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("InValidLoanValue", loggedUser));
//                return functionResult;
//            }
//            if (loanValue.doubleValue() < loanMinValue.doubleValue()) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("ValidateMinLoanValue", loggedUser));
//                return functionResult;
//            }
//            // Validate No. Of Installment
//            Integer loanInstallmentNumber;
//            BigDecimal minInstallmentNo, maxInstallmentNo;
//            loanInstallmentNumber = employeeLoan.getInstallmentNumber();
//            minInstallmentNo = loan.getMinInstallmentNo();
//            // should come from web service
//            // Calling Max Ins No Loan WS
//            maxInstallmentNo = loan.getMaxInstallmentNo();
//            if (loanInstallmentNumber.intValue() > maxInstallmentNo.intValue()) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("ValidateMaxLoanInstallmentNo",
//                                loggedUser));
//                return functionResult;
//            }
//            if (loanInstallmentNumber.intValue() < minInstallmentNo.intValue()) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("ValidateMinLoanInstallmentNo",
//                                loggedUser));
//                return functionResult;
//            }
//            if (loanInstallmentNumber.intValue() <= 0) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("InValidInstallmentNo", loggedUser));
//                return functionResult;
//            }
//            // Validate Monthly Value
//            BigDecimal loanMonthlyValue, maxMonthlyValue, minMonthlyValue;
//            loanMonthlyValue = employeeLoan.getMonthValue();
//            minMonthlyValue = loan.getMinMonthValue();
//            // Should Come From WS
//            // List loanData = new ArrayList();
//            // loanData.add(employeeLoan.getLoan().getDbid());
//            // String webServiceCode = "WSF_LMV";
//            // String result =
//            // webServiceUtility.callingWebService(webServiceCode, loanData,
//            // loggedUser);
//            // if (!result.equals("")) {
//            // maxMonthlyValue = new BigDecimal(result);
//            // } else {
//            // maxMonthlyValue = loan.getMaxMonthValue();
//            // }
//            maxMonthlyValue = loan.getMaxMonthValue();
//            if (loanMonthlyValue.doubleValue() > maxMonthlyValue.doubleValue()) {
//                functionResult
//                        .addError(userMessageServiceRemote.getUserMessage(
//                                        "ValidateMaxLoanMonthValue", loggedUser));
//                return functionResult;
//            }
//            if (loanMonthlyValue.doubleValue() < minMonthlyValue.doubleValue()) {
//                functionResult
//                        .addError(userMessageServiceRemote.getUserMessage(
//                                        "ValidateMinLoanMonthValue", loggedUser));
//                return functionResult;
//            }
//            if (loanMonthlyValue.doubleValue() <= 0) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("InValidLoanMonthValue", loggedUser));
//                return functionResult;
//            }
//
//            // Validate Month
//            if (startMonth > 12 || startMonth < 1) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("InValidLoanStartMonth", loggedUser));
//                return functionResult;
//            }
//            // Validate Year
//            if (startYear.toString().length() < 4) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("InValidLoanStartYear", loggedUser));
//                return functionResult;
//            }
//            // Validate If This Month CLosed Or Not
//            List<String> conditions = new ArrayList<String>();
//            conditions.add(" month = " + startMonth);
//            conditions.add(" year = " + startYear);
//            conditions.add(" closed = 'Y'");
//            conditions.add(" paymentMethod.unit = 'M'");
//            conditions.add(" paymentMethod.company.dbid = "
//                    + loan.getCompany().getDbid());
//            List<CalculatedPeriod> calculatedPeriods = oem.loadEntityList(
//                    CalculatedPeriod.class.getSimpleName(), conditions, null,
//                    null, loggedUser);
//            if (calculatedPeriods.size() > 0) {
//                functionResult.addError(userMessageServiceRemote
//                        .getUserMessage("InValidLoanCPDate", loggedUser));
//                return functionResult;
//            }
//            // Validate Refreshment(WS)
//
//            // Validate Need Approval Or Not
//            if (loan.isNeedApproval()) {
//                employeeLoan.setStatus("W");
//            } else {
//                employeeLoan.setStatus("A");
//            }
//            // Set Minimum & Maximum Of Employee Loan
//            // employeeLoan.setMaximumValue(loanMaxValue);
//            employeeLoan.setMinimumValue(loanMinValue);
            
           //(string loan, string employee, 
           //string month, string year, decimal loan_value,
           //string guarantee_person, string wsdbid, string userdbid, 
           //string userloginname, string userpass, string custname)
            
            // CALL VACATION Validation OPERATION
            List loanValidationdata = new ArrayList();
            loanValidationdata.add(String.valueOf(loan.getDbid()));
            loanValidationdata.add(String.valueOf(employeeLoan.getEmployee().getDbid()));
            loanValidationdata.add(startMonth);
            loanValidationdata.add(startYear);
            loanValidationdata.add(employeeLoan.getLoanValue());
            loanValidationdata.add(employeeLoan.getGuaranteePerson());
//            loanValidationdata.add("500570730");
//            loanValidationdata.add(loggedUser.getDbid());
//            loanValidationdata.add(loggedUser.getLoginName());
//            loanValidationdata.add(loggedUser.getPassword());
//            loanValidationdata.add(loggedUser.getCustom1());
            
            

            String webServiceCode = "WSF_VLOAN";
            String result = webServiceUtility.callingWebService(webServiceCode,
                    loanValidationdata, loggedUser);
            if (result == null || result.compareTo(" ") == 0 || result.equals("")) {
                functionResult.addSuccess(userMessageServiceRemote.getUserMessage(
                    "SavedSuccessfully", loggedUser));
            } else {
                UserMessage msg = new UserMessage();
                msg.setMessageText(result);
                msg.setMessageTitle("Validation Error");
                msg.setName("Validation Error");
                msg.setMessageTextTranslated(result);
                msg.setMessageType(UserMessage.TYPE_ERROR);
                functionResult.addError(msg);
            }

        } catch (Exception ex) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
            OLog.logException(ex, loggedUser);
        }
        return functionResult;
    }

    @Override
    public OFunctionResult calculateMonthLoanValue(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            String fldExp = odm.getData().get(1).toString();
            Object newVal = odm.getData().get(2);
            BigDecimal monthValue, loanValue, paidAmount;
            Integer installmentNo;

            if ((fldExp.equalsIgnoreCase("loanValue"))
                    || (fldExp.equalsIgnoreCase("loanValueMask"))) {
                loanValue = new BigDecimal(newVal.toString());

                if (employeeLoan.getLoanValue() == null
                        || (loanValue.doubleValue() != employeeLoan
                        .getLoanValue().doubleValue())) {
                    Integer empLoanInsNo = employeeLoan.getInstallmentNumber();
                    if ((empLoanInsNo == null) || (empLoanInsNo == 0)) {
                        return functionResult;
                    }

                    employeeLoan.setMonthValue(loanValue);
                    Double monthValueDouble;
                    monthValueDouble = loanValue.doubleValue() / empLoanInsNo;
                    monthValue = new BigDecimal(monthValueDouble.toString());
                    employeeLoan.setMonthValue(monthValue);
                    employeeLoan.setMonthValueMask(monthValue);
                }
            }
            if (fldExp.equalsIgnoreCase("installmentNumber")) {
                installmentNo = Integer.valueOf("" + newVal);
                if (installmentNo == null) {
                    return functionResult;
                }

                Integer empLoanInsNo = employeeLoan.getInstallmentNumber();
                if (empLoanInsNo == null) {
                    empLoanInsNo = 0;
                }

                if (installmentNo.intValue() == empLoanInsNo.intValue()
                        || installmentNo.intValue() <= 0) {
                    return functionResult;
                }
                loanValue = employeeLoan.getLoanValue();
                Double monthValueDouble;
                monthValueDouble = loanValue.doubleValue() / installmentNo;
                monthValue = new BigDecimal(monthValueDouble.toString());
                employeeLoan.setMonthValue(monthValue);
                employeeLoan.setMonthValueMask(monthValue);
            }
            if (fldExp.equalsIgnoreCase("settInstallmentNumber")) {
                installmentNo = (Integer) newVal;
                if (installmentNo == null) {
                    return functionResult;
                }

                Integer empLoanInsNo = employeeLoan.getSettInstallmentNumber();
                if (empLoanInsNo == null) {
                    empLoanInsNo = 0;
                }

                if (installmentNo.intValue() == empLoanInsNo.intValue()
                        || installmentNo.intValue() <= 0) {
                    return functionResult;
                }
                Double monthValueDouble;
                loanValue = employeeLoan.getMonthValue();
                monthValueDouble = loanValue.doubleValue() * installmentNo;
                monthValue = new BigDecimal(monthValueDouble.toString());
                employeeLoan.setSettLoanValue(monthValue);
                employeeLoan.setSettLoanValueMask(monthValue);
            }
            if (fldExp.equalsIgnoreCase("ResInstallmentNumber")) {
                installmentNo = Integer.valueOf((String) newVal);
                if (installmentNo == null) {
                    return functionResult;
                }

                Integer empLoanInsNo = employeeLoan.getResInstallmentNumber();
                if (empLoanInsNo == null) {
                    empLoanInsNo = 0;
                }

                if (installmentNo.intValue() == empLoanInsNo.intValue()
                        || installmentNo.intValue() <= 0) {
                    return functionResult;
                }
                loanValue = employeeLoan.getLoanValue();
                paidAmount = employeeLoan.getPaidAmount();
                if (paidAmount != null && paidAmount.doubleValue() > 0) {
                    loanValue = loanValue.subtract(paidAmount);
                }
                Double monthValueDouble;
                monthValueDouble = loanValue.doubleValue() / installmentNo;
                monthValue = new BigDecimal(monthValueDouble.toString());
                employeeLoan.setResMonthValue(monthValue);
                employeeLoan.setResMonthValueMask(monthValue);
            }
            functionResult.setReturnedDataMessage(odm);
        } catch (Exception ex) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
            return functionResult;
        }
        return functionResult;
    }

    @Override
    public OFunctionResult cancelLoan(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            functionResult.append(validateCancelLoan(employeeLoan, loggedUser));
            if (functionResult.getErrors().isEmpty()) {
                cancelLoanSettelment(employeeLoan, loggedUser);
                cancelLoan(employeeLoan, loggedUser);
            }
        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    @Override
    public OFunctionResult loanApproval(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            if (employeeLoan.getStatus().equals("C")
                    || employeeLoan.getStatus().equals("N")) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("LoanApproval002", loggedUser));
                return functionResult;
            } else if (employeeLoan.getStatus().equals("A")) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("LoanApproval001", loggedUser));
                return functionResult;
            } else {
                employeeLoan.setStatus("A");
            }
            oem.saveEntity(employeeLoan, loggedUser);
            functionResult.addSuccess(userMessageServiceRemote.getUserMessage(
                    "LoanApprovalSuccess", loggedUser));

        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    @Override
    public OFunctionResult loanSuspend(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            Loan loan = employeeLoan.getLoan();
            if (employeeLoan.getStatus().equals("N")
                    || employeeLoan.getStatus().equals("C")) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("LoanSuspend001", loggedUser));
                return functionResult;
            }
            if (employeeLoan.getStatus().equals("W")) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("LoanSuspend002", loggedUser));
                return functionResult;
            }
            Integer susMonth = employeeLoan.getSuspendedToMonth();
            if (susMonth > 12 || susMonth < 1) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("InValidLoanSusMonth", loggedUser));
                return functionResult;
            }
            // Validate Year
            Integer susYear = employeeLoan.getSuspendedToYear();
            if (susYear.toString().length() < 4) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("InValidLoanSusYear", loggedUser));
                return functionResult;
            }
            // Validate If This Month CLosed Or Not
            List<String> conditions = new ArrayList<String>();
            conditions.add(" month = " + susMonth);
            conditions.add(" year = " + susYear);
            conditions.add(" closed = 'Y'");
            conditions.add(" paymentMethod.unit = 'M'");
            conditions.add(" paymentMethod.company.dbid = "
                    + loan.getCompany().getDbid());
            List<CalculatedPeriod> calculatedPeriods = oem.loadEntityList(
                    CalculatedPeriod.class.getSimpleName(), conditions, null,
                    null, loggedUser);
            if (calculatedPeriods.size() > 0) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("InValidLoanCPDate", loggedUser));
                return functionResult;
            }
            // Calling Suspend Loan WS
            List loanData = new ArrayList();
            String suspendFlag;
            suspendFlag = employeeLoan.getSuspendOrCancel();
            if (suspendFlag == null) {
                suspendFlag = "Y";
            }

            loanData.add(employeeLoan.getDbid());
            loanData.add(susMonth);
            loanData.add(susYear);
            loanData.add(suspendFlag);

            String webServiceCode = "WSF_LS";
            webServiceUtility.callingWebService(webServiceCode, loanData,
                    loggedUser);
            if (suspendFlag.equals("Y")) {
                functionResult.addSuccess(userMessageServiceRemote
                        .getUserMessage("LoanSuspend003", loggedUser));
            } else {
                functionResult.addSuccess(userMessageServiceRemote
                        .getUserMessage("LoanSuspend004", loggedUser));
            }
        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    @Override
    public OFunctionResult loanReschedule(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            if (employeeLoan.getStatus().equals("W")
                    || employeeLoan.getStatus().equals("N")
                    || employeeLoan.getStatus().equals("C")) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("LoanReschedule002", loggedUser));
                return functionResult;
            }
            // Calling Reschedule Loan WS
            List loanData = new ArrayList();

            loanData.add(employeeLoan.getDbid());
            loanData.add(employeeLoan.getResInstallmentNumber());
            loanData.add(employeeLoan.getResMonthValue());

            String webServiceCode = "WSF_LR";
            webServiceUtility.callingWebService(webServiceCode, loanData,
                    loggedUser);
            functionResult.addSuccess(userMessageServiceRemote.getUserMessage(
                    "LoanReschedule001", loggedUser));

        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    @Override
    public OFunctionResult loanSettelment(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            if (functionResult.getErrors().isEmpty()) {
                functionResult.append(loanSettelment(employeeLoan, loggedUser));
                if (functionResult.getErrors().isEmpty()) {
                    functionResult.addSuccess(userMessageServiceRemote.getUserMessage("LoanSettelement001", loggedUser));
                }

            }
//            functionResult.addSuccess(userMessageServiceRemote.getUserMessage(
//                    "LoanSettelement001", loggedUser));
//
        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    @Override
    public OFunctionResult loanEditing(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            if (employeeLoan.getStatus().equals("C")
                    || employeeLoan.getStatus().equals("N")
                    || employeeLoan.getStatus().equals("A")) {
                functionResult.addError(userMessageServiceRemote
                        .getUserMessage("LoanEditing001", loggedUser));
                return functionResult;
            }

            oem.saveEntity(employeeLoan, loggedUser);
            functionResult.addSuccess(userMessageServiceRemote.getUserMessage(
                    "LoanEditing002", loggedUser));

        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    @Override
    public OFunctionResult validateLoanForPenalty(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
        try {
            // oem.getEM(loggedUser);
            Employee employee = employeeLoan.getEmployee();
            ArrayList<EmployeePenaltyRequest> employeePenaltiesRequest = (ArrayList<EmployeePenaltyRequest>) oem
                    .loadEntityList(
                            "EmployeePenaltyRequest",
                            Collections.singletonList("employee.dbid = '"
                                    + employee.getDbid() + "'"), null, null,
                            loggedUser);

            boolean accept = true;
            for (EmployeePenaltyRequest employeePenalty : employeePenaltiesRequest) {
                Date currentDate = new Date();
                Date penaltyDate = employeePenalty.getPenaltyDate();
                Penalty penalty = employeePenalty.getPenalty();

                String internalCode = penalty.getInternalCode();

                if (("INV1".equals(internalCode) || "INV2".equals(internalCode)
                        || "INV3".equals(internalCode)
                        || "INV4".equals(internalCode)
                        || "INV5".equals(internalCode)
                        || "INV6".equals(internalCode)
                        || "INV7".equals(internalCode)
                        || "INV8".equals(internalCode)
                        || "INV9".equals(internalCode) || "INV10"
                        .equals(internalCode)) == false) {
                    continue;
                }

                String refreshOnYearBegin = penalty.getRefreshOnYearBegin();
                if (refreshOnYearBegin != null
                        && refreshOnYearBegin.equals("Y")) {
                    // 1) Refresh on year begin
                    if (currentDate.getYear() <= penaltyDate.getYear()) {
                        accept = false;
                        break;
                    }
                }

                String refreshMethod = penalty.getRefreshMethod();
                Calendar cal = Calendar.getInstance();
                BigDecimal refreshAfterPeriod = penalty.getRefreshAfterPeriod();
                String refreshUnitMeasure = penalty.getRefreshUnitMeasure();
                int refreshAfterPeriodDays = 0;
                if (refreshUnitMeasure.equalsIgnoreCase("D")) {
                    refreshAfterPeriodDays = refreshAfterPeriod.intValue();
                } else if (refreshUnitMeasure.equalsIgnoreCase("M")) {
                    refreshAfterPeriodDays = refreshAfterPeriod.intValue() * 30;
                } else if (refreshUnitMeasure.equalsIgnoreCase("Y")) {
                    refreshAfterPeriodDays = refreshAfterPeriod.intValue() * 365;
                }

                if (refreshMethod != null && refreshMethod.equals("H")) {
                    // 2) Hiring Date
                    Date hiringDate = employee.getHiringDate();
                    cal.setTime(hiringDate);
                    cal.add(Calendar.DATE, refreshAfterPeriodDays);
                    Date periodAfterH = cal.getTime();
                    if (periodAfterH.getYear() > currentDate.getYear()) {
                        accept = false;
                        break;
                    }
                    if (periodAfterH.getYear() == currentDate.getYear()) {
                        if (periodAfterH.getMonth() >= currentDate.getMonth()) {
                            accept = false;
                            break;
                        }
                    }
                } else if (refreshMethod != null && refreshMethod.equals("P")) {
                    // 3) First Penalty Date
                    List conditions = new ArrayList();
                    conditions.add("employee.dbid = '" + employee.getDbid()
                            + "'");
                    conditions.add("penalty.refreshMethod = 'P'");
                    ArrayList<EmployeePenaltyRequest> eprSamePenType = (ArrayList<EmployeePenaltyRequest>) oem
                            .loadEntityList("EmployeePenaltyRequest",
                                    conditions, null, null, loggedUser);

                    // Get first penalty date
                    Date firstPenDate = new Date();
                    for (EmployeePenaltyRequest penSameType : eprSamePenType) {
                        if (penSameType.getPenaltyDate().before(firstPenDate)) {
                            firstPenDate = penSameType.getPenaltyDate();
                        }
                    }

                    cal.setTime(firstPenDate);
                    cal.add(Calendar.DATE, refreshAfterPeriodDays);
                    Date dateAfterAddingPenaltyRefresh = cal.getTime();

                    if (dateAfterAddingPenaltyRefresh.getYear() > currentDate
                            .getYear()) {
                        accept = false;
                        break;
                    }
                    if (dateAfterAddingPenaltyRefresh.getYear() == currentDate
                            .getYear()) {
                        if (dateAfterAddingPenaltyRefresh.getMonth() >= currentDate
                                .getMonth()) {
                            accept = false;
                            break;
                        }
                    }
                }
            }

            if (accept == false) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "LoanRequestWithinPenaltyPeriod", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } finally {
            // oem.closeEM(loggedUser);
            return oFR;
        }
    }

    @Override
    public OFunctionResult getLoanMaxValue(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);

        String fldExp = odm.getData().get(1).toString();
        Object newVal = odm.getData().get(2);
        Loan loan;
        BigDecimal loanMaxValue;
        loanMaxValue = BigDecimal.ZERO;
        // loan = employeeLoan.getLoan();
        if (fldExp.contains("loan")) {
            loan = (Loan) newVal;
            try {
                // <editor-fold defaultstate="collapsed"
                // desc="calculate maxValue for loan">
                Date employeeHiringDate = employeeLoan.getEmployee()
                        .getHiringDate();
                if (employeeHiringDate == null) {
                    List<String> hiringConditions = new ArrayList<String>();
                    hiringConditions.add("employee.dbid = "
                            + employeeLoan.getEmployee().getDbid());
                    Employee employeeHiring = (Employee) oem.loadEntity(
                            Employee.class.getSimpleName(), hiringConditions,
                            null, loggedUser);

                    if (employeeHiring != null) {
                        employeeHiringDate = employeeHiring.getHiringDate();
                    }
                }
                if (employeeHiringDate != null) {
                    SimpleDateFormat simpleDateformat = new SimpleDateFormat(
                            "yyyy");
                    String year = simpleDateformat.format(employeeHiringDate);
                    oFR.append(calculateLoanMaximumValue(employeeLoan.getEmployee(), loan, year, loggedUser));
                    if (oFR.getReturnValues() != null && !oFR.getReturnValues().isEmpty()) {
                        loanMaxValue = new BigDecimal(oFR.getReturnValues().get(0).toString());
                    } else {
                        loanMaxValue = new BigDecimal(0);
                    }
                    employeeLoan.setMaximumValue(loanMaxValue);

                }

            } catch (Exception ex) {
                OLog.logException(ex, loggedUser);
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "SystemInternalError", loggedUser), ex);

            }
        }
        return oFR;

    }

    // </editor-fold>
    @Override
    public OFunctionResult loanBalanceSheet(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);

            if (employeeLoan != null) {
                if (!(employeeLoan.getStatus().equals("N") || employeeLoan
                        .getStatus().equals("W"))) {
                    loanBalanceCalculation(employeeLoan.getDbid(), loggedUser);
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInernalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult loanBalanceSheetForAll(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        try {
            String expSQL;
            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            String securityCondSQL = "";
            if (!employeeDbidList.isEmpty()) {
                String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                securityCondSQL = " AND employee_dbid in " + employeeDbidStr;
            }
            expSQL = "SELECT dbid FROM EmployeeLoan"
                    + " WHERE status not in('N','W')"
                    + securityCondSQL;

            List<Long> dbids = oem.executeEntityListNativeQuery(expSQL,
                    loggedUser);

            if (dbids != null) {

                for (int i = 0; i < dbids.size(); i++) {
                    loanBalanceCalculation(dbids.get(i), loggedUser);
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInernalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    private OFunctionResult loanBalanceCalculation(Long empLoanDbid,
            OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        try {
            String expSQL;
            Object status;

            expSQL = "select det.dbid from udc det, udc par where det.code in"
                    + " ( 'LS_Closed') " + "AND" + " det.type_dbid = par.dbid "
                    + "AND " + "par.code = 'LoanBalanceSheet'";

            status = (Object) oem.executeEntityNativeQuery(expSQL, loggedUser);

            Long inProcessDbid = null, closedDbid = null;

            if (status != null) {
                closedDbid = Long.parseLong(status.toString());
            }

            expSQL = "select det.dbid from udc det, udc par where det.code in"
                    + " ('LS_Inprocess') " + "AND"
                    + " det.type_dbid = par.dbid " + "AND "
                    + "par.code = 'LoanBalanceSheet'";

            status = (Object) oem.executeEntityNativeQuery(expSQL, loggedUser);
            if (status != null) {
                inProcessDbid = Long.parseLong(status.toString());
            }

            expSQL = "delete from loanbalancesheet where employeeloan_dbid  = "
                    + empLoanDbid;

            oem.executeEntityUpdateNativeQuery(expSQL, loggedUser);

            String selectSQL = "select valueData from PayrollParameter where description = 'ApplyEncryption'";
            Object applyEncResult = oem.executeEntityNativeQuery(selectSQL, loggedUser);
            String applyEnc = applyEncResult == null ? "" : applyEncResult.toString();

            Integer startYear = 0;
            Integer startMonth = 0;
            BigDecimal monthValue = null;
            BigDecimal loanValue = null;
            BigDecimal paidAmount = null;
            Integer lastMonth = 0;
            Integer lastYear = 0;

            EmployeeLoan employeeLoan = null;
            Object[] results = null;
            if (applyEnc.equalsIgnoreCase("yes")) {
                employeeLoan = (EmployeeLoan) oem.loadEntity(EmployeeLoan.class.getSimpleName(), empLoanDbid, null, null, loggedUser);
                if (employeeLoan != null) {
                    startYear = employeeLoan.getStartYear();
                    startMonth = employeeLoan.getStartMonth();
                    monthValue = employeeLoan.getMonthValue();
                    loanValue = employeeLoan.getLoanValue();
                    paidAmount = employeeLoan.getPaidAmount();
                    lastMonth = employeeLoan.getLastMonth();
                    lastYear = employeeLoan.getLastYear();
                }
            } else {
                expSQL = "SELECT employee_dbid, loan_dbid, startYear, startMonth, "
                        + " installmentNumber,monthValue,loanValue, paidAmount, paidInstallment ,lastMonth, lastYear FROM EmployeeLoan "
                        + "WHERE DBID = "
                        + empLoanDbid;

                results = (Object[]) oem.executeEntityNativeQuery(expSQL,
                        loggedUser);

                if (results != null && results.length > 0) {

                    startYear = Integer.parseInt(results[2].toString());
                    startMonth = Integer.parseInt(results[3].toString());
                    monthValue = new BigDecimal(results[5].toString());
                    loanValue = new BigDecimal(results[6].toString());

                    if (results[7] != null) {
                        paidAmount = new BigDecimal(results[7].toString());
                    } else {
                        paidAmount = null;
                    }

                    if (results[8] != null) {
                    } else {
                    }
                    if (results[9] != null) {
                        lastMonth = Integer.parseInt(results[9].toString());
                    } else {
                        lastMonth = null;
                    }

                    if (results[10] != null) {
                        lastYear = Integer.parseInt(results[10].toString());
                    } else {
                        lastYear = null;
                    }

                }
            }

            if (employeeLoan != null || (results != null && results.length > 0)) {

                BigDecimal loanBalance;
                BigDecimal installementValue;
                Integer year, month;
                loanBalance = loanValue;
                year = startYear;
                month = startMonth;

                if (paidAmount != null) {
                    loanBalance = loanValue.subtract(paidAmount);
                    year = lastYear;
                    month = lastMonth;
                }
                installementValue = monthValue;

                // long status_dbid = Long.parseLong(status[0].toString());
                Long statuString;

                while (loanBalance.longValue() > 0) {
                    if (installementValue.longValue() == (loanBalance
                            .longValue())) {
                        statuString = closedDbid;
                    } else {
                        statuString = inProcessDbid;
                    }

                    //TODO : FIX for Encryption
                    expSQL = "INSERT INTO LoanBalanceSheet ( loanBalance, "
                            + "employeeLoan_dbid, startYear, startMonth,"
                            + "installementValue, status_dbid, ACTIVE, DELETED ) "
                            + "values(" + loanBalance + ", " + empLoanDbid
                            + ", " + year + ", " + month + ", "
                            + installementValue + ", " + statuString + ",1,0)";

                    oem.executeEntityUpdateNativeQuery(expSQL, loggedUser);

                    if (loanBalance.doubleValue() < monthValue.doubleValue()) {
                        loanBalance = new BigDecimal(0);
                        installementValue = loanBalance;

                        // status_dbid = 1000800015089L;
                    } else {
                        loanBalance = loanBalance.subtract(installementValue);
                        // status_dbid = 1000800015090L;
                    }

                    month++;
                    if (month > 12) {
                        month = 1;
                        year++;
                    }
                }

            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInernalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult importEmpLoans(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        try {
            String sql = "";

            DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
            Connection connection = datasource.getConnection();

            if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("oracle")) {
                String selectSQL = "select valueData from PayrollParameter where description = 'ApplyEncryption'";
                Object applyEncResult = oem.executeEntityNativeQuery(selectSQL, loggedUser);
                String applyEnc = applyEncResult == null ? "" : applyEncResult.toString();

                if (applyEnc.equalsIgnoreCase("yes")) {
                    String symmKey = dataEncryptorServiceLocal.getSymmetricKey();
                    String ivKey = dataEncryptorServiceLocal.getIvKey();
                    //dataEncryptorServiceLocal.

                    sql = "SELECT employeeCode, loanCode, startMonth, "
                            + "startYear, cast(decrypt(loanValueenc,'" + symmKey + "'," + ivKey + ") as varchar(255)) as loanValue, "
                            + "cast(decrypt(monthValueenc,'" + symmKey + "'," + ivKey + ") as varchar(255)) as monthValue,"
                            + " cast(decrypt(installmentNumberenc,'" + symmKey + "'," + ivKey + ") as varchar(255)) as installmentNumber, "
                            + "done, notes, approvedBy, dbid FROM ImportEmployeeLoan where done = 0";
                }
            } else {
                sql = "SELECT employeeCode, loanCode, startMonth, "
                        + "startYear, loanValue, monthValue, installmentNumber, "
                        + "done, notes, approvedBy, dbid FROM ImportEmployeeLoan where done = 0";
            }
            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            String securityCond = "";
            if (!employeeDbidList.isEmpty()) {
                String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                securityCond = " AND employeeCode in (SELECT CODE FROM oemployee where dbid in " + employeeDbidStr;
            }

            sql += securityCond;
            Vector<Object[]> importEmployeeLoans = (Vector<Object[]>) oem
                    .executeEntityListNativeQuery(sql, loggedUser);
            boolean saved = false;
            Employee employee = null, approvedBy = null;

            Loan loan;
            Integer startMonth;
            Integer startYear;
            Integer installmentNumber;
            BigDecimal loanValue;
            BigDecimal monthValue;
            String dbids = "";

            for (Object[] objects : importEmployeeLoans) {

                if (!(objects[0].equals("")) && (objects[0] != null)
                        && !(objects[1].equals("")) && (objects[1] != null)) {

                    employee = (Employee) oem.loadEntity(
                            Employee.class.getSimpleName(),
                            Collections.singletonList("code = '" + objects[0]
                                    + "'"), null, loggedUser);

                    loan = (Loan) oem.loadEntity(
                            Loan.class.getSimpleName(),
                            Collections.singletonList("code = '" + objects[1]
                                    + "'"), null, loggedUser);

                    if (objects[9] != null) {
                        approvedBy = (Employee) oem.loadEntity(
                                Employee.class.getSimpleName(),
                                Collections.singletonList("code = '"
                                        + objects[9] + "'"), null, loggedUser);
                    }

                    if (employee != null && loan != null) {
                        if (objects[2] == null || objects[3] == null
                                || objects[4] == null || objects[5] == null) {
                            continue;
                        }
                        saved = false;

                        startMonth = Integer.parseInt(objects[2].toString());
                        startYear = Integer.parseInt(objects[3].toString());
                        loanValue = new BigDecimal(objects[4].toString());
                        monthValue = new BigDecimal(objects[5].toString());

                        if (!objects[6].equals("")) {
                            installmentNumber = Integer.parseInt(objects[6]
                                    .toString());

                        } else {
                            installmentNumber = (loanValue.intValue() / monthValue
                                    .intValue());
                        }

                        saved = createEmployeLoan(employee, loan, startMonth,
                                startYear, loanValue, monthValue,
                                installmentNumber, approvedBy, loggedUser);
                        if (saved == true) {
                            dbids = dbids.concat(objects[10].toString())
                                    .concat(",");
                        }
                    }
                }
            }
            if (dbids.length() > 0) {
                dbids = dbids.substring(0, dbids.length() - 1);
                sql = "update ImportEmployeeLoan set done = 1 where dbid in ("
                        + dbids + ")";
                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInernalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    private boolean createEmployeLoan(Employee employee, Loan loan,
            Integer startMonth, Integer startYear, BigDecimal loanValue,
            BigDecimal monthValue, Integer installmentNumber,
            Employee approvedBy, OUser loggedUser) {

        EmployeeLoan employeeLoan = new EmployeeLoan();

        employeeLoan.setEmployee(employee);
        employeeLoan.setLoan(loan);
        employeeLoan.setStartMonth(startMonth);
        employeeLoan.setStartYear(startYear);
        employeeLoan.setLoanValue(loanValue);
        employeeLoan.setMonthValue(monthValue);
        employeeLoan.setStatus("A");
        employeeLoan.setInstallmentNumber(installmentNumber);
        employeeLoan.setApprovedBy(approvedBy);

        OFunctionResult returnResult = entitySetupService
                .callEntityCreateAction(employeeLoan, loggedUser);
        if (returnResult.getErrors().size() > 0) {
            return false;
        }
        return true;
    }

    private OFunctionResult calculateLoanMaximumValue(Employee employee, Loan loan, String year, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {

            BigDecimal loanMaxValue;
            Calendar calendar = Calendar.getInstance();
            String currentYear = dateTimeUtility.YEAR_FORMAT.format(calendar.getTime());
            Integer experienceYears = Integer.parseInt(currentYear)
                    - Integer.parseInt(year);
            List<String> condition = new ArrayList<String>();
            condition.add("fromYears <= " + experienceYears);
            condition.add("toYears >= " + experienceYears);
            condition.add("loan.dbid = " + loan.getDbid());
            LoanMaximumValue loanMaximumValue = (LoanMaximumValue) oem
                    .loadEntity(LoanMaximumValue.class.getSimpleName(),
                            condition, null, loggedUser);
            if (loanMaximumValue != null) {
                BigDecimal maxAsAmount = loanMaximumValue
                        .getMaximumAsAmountMask();
                if (maxAsAmount == null) {
                    maxAsAmount = new BigDecimal(BigInteger.ZERO);
                }
                if (maxAsAmount.compareTo(new BigDecimal(0)) != 0) {
                    loanMaxValue = maxAsAmount;
                } else {
                    BigDecimal savingBoxVal = new BigDecimal(0);
                    BigDecimal basicSalaryVal = new BigDecimal(0);
                    BigDecimal basicSalaryPercentage = loanMaximumValue.getBasicSalaryPercentage();
                    BigDecimal basicSalary = new BigDecimal(0);

                    BigDecimal savingBox = employee.getPayroll().getSavingBox();
                    BigDecimal savingBoxPercentage = loanMaximumValue
                            .getSavingBoxPercentage();
                    if (savingBox != null && savingBoxPercentage != null) {
                        savingBoxVal = savingBox
                                .multiply(savingBoxPercentage
                                        .divide(new BigDecimal(100)));
                    }

                    condition = new ArrayList<String>();
                    condition.add("employee.dbid = "
                            + employee.getDbid());
                    condition.add("salaryElement.internalCode = 'BASIC1'");
                    EmployeeSalaryElement basic1 = (EmployeeSalaryElement) oem
                            .loadEntity(EmployeeSalaryElement.class
                                    .getSimpleName(), condition, null,
                                    loggedUser);
                    if (basic1 != null) {
                        basicSalary = basicSalary.add(basic1.getValue());
                    }

                    condition = new ArrayList<String>();
                    condition.add("employee.dbid = "
                            + employee.getDbid());
                    condition.add("salaryElement.internalCode = 'BASIC2'");
                    EmployeeSalaryElement basic2 = (EmployeeSalaryElement) oem
                            .loadEntity(EmployeeSalaryElement.class
                                    .getSimpleName(), condition, null,
                                    loggedUser);
                    if (basic2 != null) {
                        basicSalary = basicSalary.add(basic2.getValue());
                    }

                    condition = new ArrayList<String>();
                    condition.add("employee.dbid = "
                            + employee.getDbid());
                    condition.add("salaryElement.internalCode = 'BASIC3'");
                    EmployeeSalaryElement basic3 = (EmployeeSalaryElement) oem
                            .loadEntity(EmployeeSalaryElement.class
                                    .getSimpleName(), condition, null,
                                    loggedUser);
                    if (basic3 != null) {
                        basicSalary = basicSalary.add(basic3.getValue());
                    }

                    condition = new ArrayList<String>();
                    condition.add("employee.dbid = "
                            + employee.getDbid());
                    condition.add("salaryElement.internalCode IN ('Net','net','NET')");

                    EmployeeSalaryElement net = (EmployeeSalaryElement) oem
                            .loadEntity(EmployeeSalaryElement.class
                                    .getSimpleName(), condition, null,
                                    loggedUser);
                    if (net != null) {
                        basicSalary = basicSalary.add(net.getValue());
                    }

                    if (basicSalaryPercentage != null
                            || basicSalary != null) {
                        basicSalaryVal = basicSalary
                                .multiply(basicSalaryPercentage
                                        .divide(new BigDecimal(100)));
                    }

                    loanMaxValue = savingBoxVal.add(basicSalaryVal);
                    // ---------Check If this loan is Accumulative or
                    // Not------------------
                    // --------Get All loan values of the current
                    // year-----------------
                    if (loan.isAccumulative()) {
                        int currYear = Calendar.getInstance().get(
                                Calendar.YEAR);
                        condition = new ArrayList<String>();
                        condition.add(" loan.dbid = " + loan.getDbid());
                        condition.add(" employee.dbid = "
                                + employee.getDbid());
                        condition.add(" startYear = " + currYear);
                        condition.add(" status <> 'N'");
                        List<EmployeeLoan> empLoan = oem.loadEntityList(
                                EmployeeLoan.class.getSimpleName(),
                                condition, null, null, loggedUser);
                        condition.clear();
                        if (empLoan.size() > 0) {
                            for (int i = 0; i < empLoan.size(); i++) {
                                EmployeeLoan employeeLoan1 = empLoan.get(i);
                                loanMaxValue = loanMaxValue
                                        .subtract(employeeLoan1
                                                .getLoanValue());
                            }
                        }
                    }
                }
                functionResult.addReturnValue(loanMaxValue);
            }
        } catch (Exception ex) {
        }
        return functionResult;
    }

    @Override
    public OFunctionResult cancelLoanSettelment(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            EmployeeLoan employeeLoan = (EmployeeLoan) odm.getData().get(0);
            functionResult.append(validateCancelLoanSettelment(employeeLoan, loggedUser));
            if (functionResult.getErrors().isEmpty()) {
                BigDecimal settlementValue;
                settlementValue = employeeLoan.getSettLoanValue();
                EmployeeLoanSettlement settlement;
                settlement = employeeLoan.getLastSettlement();
                if ((settlementValue != null && settlementValue.equals(BigDecimal.ZERO)) || (settlement == null)) {
                    functionResult.addError(userMessageServiceRemote.getUserMessage("CancelSettelement003", loggedUser));
                    return functionResult;
                }
                functionResult.append(cancelLoanSettelment(employeeLoan, loggedUser));
                if (functionResult.getErrors().isEmpty()) {
                    functionResult.addSuccess(userMessageServiceRemote.getUserMessage("CancelSettelement004", loggedUser));
                }
            }

        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    private OFunctionResult validateCancelLoanSettelment(EmployeeLoan employeeLoan, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            if (employeeLoan.getSettDeduction() == null || employeeLoan.getSettDeduction().getDbid() == 0) {
                functionResult.addError(userMessageServiceRemote.getUserMessage("CancelSettelement001", loggedUser));
                return functionResult;
            }
            if (employeeLoan.getStatus().equals("W") || employeeLoan.getStatus().equals("N")) {
                functionResult.addError(userMessageServiceRemote.getUserMessage("CancelSettelement002", loggedUser));
                return functionResult;
            }
        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    public OFunctionResult cancelLoanSettelment(EmployeeLoan employeeLoan, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            BigDecimal settlementValue;
            settlementValue = employeeLoan.getSettLoanValue();
            if (!settlementValue.equals(BigDecimal.ZERO)) {
                EmployeeLoanSettlement settlement;
                settlement = employeeLoan.getLastSettlement();
                if (settlement != null) {
                    settlement.setCancelled("Y");
                    settlement.setCancelledBy(employeeLoan.getSettCancelledBy());
                    settlement.setCancelledByComments(employeeLoan.getSettCancelledByComments());
                    oem.saveEntity(settlement, loggedUser);
                    //_______________________Reset Employee Loan Data_________________________________
                    employeeLoan.setPaidAmount(settlement.getPaidAmountB4Settlement());
                    employeeLoan.setPaidInstallment(settlement.getPaidInstallmentB4Settlement());
                    employeeLoan.setSettDeduction(null);
                    employeeLoan.setSettInstallmentNumber(0);
                    employeeLoan.setSettLoanValue(BigDecimal.ZERO);
                    employeeLoan.setSettCancelledBy(null);
                    employeeLoan.setSettCancelledByComments("");
                    oem.saveEntity(employeeLoan, loggedUser);

                    // Update Element Value With Setteled Value
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("employee.dbid = " + employeeLoan.getEmployee().getDbid());
                    conditions.add("salaryElement.dbid = " + settlement.getSettDeduction().getDbid());
                    EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                    if (employeeSalaryElement != null) {
                        BigDecimal oldValue = employeeSalaryElement.getValue();
                        BigDecimal value = oldValue.subtract(settlementValue);
                        employeeSalaryElement.setValue(value);
                        oem.saveEntity(employeeSalaryElement, loggedUser);
                    }
                }

            }

        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    private OFunctionResult validateLoanSettelment(EmployeeLoan employeeLoan, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            if (employeeLoan.getSettDeduction() == null || employeeLoan.getSettDeduction().getDbid() == 0) {
                functionResult.addError(userMessageServiceRemote.getUserMessage("LoanSettelement002", loggedUser));
                return functionResult;
            }
            if (employeeLoan.getStatus().equals("W") || employeeLoan.getStatus().equals("N") || employeeLoan.getStatus().equals("C")) {
                functionResult.addError(userMessageServiceRemote.getUserMessage("LoanSettelement003", loggedUser));
                return functionResult;
            }
        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    private OFunctionResult loanSettelment(EmployeeLoan employeeLoan, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            Integer settlementInsNo = employeeLoan.getSettInstallmentNumber();
            BigDecimal settlementValue = employeeLoan.getSettLoanValue();

            BigDecimal paidAmountAfterSettlement = employeeLoan.getPaidAmount().add(settlementValue);
            Integer installmentAfterSettlement = employeeLoan.getPaidInstallment() + settlementInsNo;
            //_______________________Loan Settlement Entity_________________________
            EmployeeLoanSettlement settlement = new EmployeeLoanSettlement();
            settlement.setEmployeeLoan(employeeLoan);
            settlement.setPaidAmountB4Settlement(employeeLoan.getPaidAmount());
            settlement.setPaidInstallmentB4Settlement(employeeLoan.getPaidInstallment());
            settlement.setStatusB4Settlement(employeeLoan.getStatus());
            settlement.setCancelled("N");
            Deduction salaryDeduction = null;
            if (employeeLoan.getSettDeduction() != null && employeeLoan.getSettDeduction().getDbid() != 0) {
                salaryDeduction = employeeLoan.getSettDeduction();
            } else {
                salaryDeduction = employeeLoan.getLoan().getAutomaticSetDeduction();
            }
            settlement.setSettDeduction(salaryDeduction);
            settlement.setSettInstallmentNumber(settlementInsNo);
            settlement.setSettLoanValue(settlementValue);
            settlement.setPaidAmount(paidAmountAfterSettlement);
            settlement.setPaidInstallment(installmentAfterSettlement);
            oem.saveEntity(settlement, loggedUser);
            //_______________________Loan Settlement Entity_________________________
            employeeLoan.setPaidAmount(paidAmountAfterSettlement);
            employeeLoan.setPaidInstallment(installmentAfterSettlement);
            employeeLoan.setLastSettlement(settlement);
            oem.saveEntity(employeeLoan, loggedUser);
            // Update Element Value With Setteled Value
            List<String> conditions = new ArrayList<String>();
            conditions.add("employee.dbid = " + employeeLoan.getEmployee().getDbid());
            conditions.add("salaryElement.dbid = " + salaryDeduction.getDbid());
            EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
            if (employeeSalaryElement != null) {
                employeeSalaryElement.setValue(employeeLoan.getSettLoanValue());
                oem.saveEntity(employeeSalaryElement, loggedUser);
            }
        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    private OFunctionResult validateCancelLoan(EmployeeLoan employeeLoan, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            if (employeeLoan.getStatus().equals("N") || employeeLoan.getStatus().equals("C")) {
                functionResult.addError(userMessageServiceRemote.getUserMessage("LoanCancel001", loggedUser));
            }
        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }

    private OFunctionResult cancelLoan(EmployeeLoan employeeLoan, OUser loggedUser) {
        OFunctionResult functionResult = new OFunctionResult();
        try {
            // Calling Cancel Loan WS
            List loanData = new ArrayList();

            loanData.add(employeeLoan.getDbid());

            String webServiceCode = "WSF_CL";
            webServiceUtility.callingWebService(webServiceCode, loanData, loggedUser);
            functionResult.addSuccess(userMessageServiceRemote.getUserMessage("LoanCancel002", loggedUser));
            employeeLoan.setStatus("N");
            oem.saveEntity(employeeLoan, loggedUser);

        } catch (Exception e) {
            functionResult.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return functionResult;
    }
}
