package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface EmployeeHiringLocal {
    public OFunctionResult assignEmployeeActive(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult createTxTFile(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);
}
