package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeUser;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.payroll.foundation.employee.EmployeeHiringLocal",
beanInterface = EmployeeHiringLocal.class)
public class EmployeeHiringSeviceBean implements EmployeeHiringLocal {

    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;

    @Override
    public OFunctionResult assignEmployeeActive(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Employee employeeHiring = (Employee) odm.getData().get(0);
            boolean isActive = employeeHiring.isActive();

            employeeHiring.setActive(isActive);
            employeeHiring.setInActive(!isActive);
            oem.saveEntity(employeeHiring, loggedUser);

            if (employeeHiring.getEmployeeUser() != null) {
                EmployeeUser empUser = (EmployeeUser) oem.loadEntity(EmployeeUser.class.getSimpleName(),
                        employeeHiring.getEmployeeUser().getDbid(), null, null, loggedUser);

                if (empUser != null) {
                    empUser.setInActive(!isActive);
                    empUser.setActive(isActive);
                    entitySetupService.callEntityUpdateAction(empUser, loggedUser);
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult createTxTFile(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        PrintWriter writer = null;
        String pathFile = "/home/mostafa/testFile";
        try {
            writer = new PrintWriter(pathFile, "UTF-8");
            writer.println("The first line");
            writer.println("The second line");
            writer.close();
            throw new UnsupportedOperationException("Not supported yet.");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeHiringSeviceBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EmployeeHiringSeviceBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
        return ofr;
    }
}
