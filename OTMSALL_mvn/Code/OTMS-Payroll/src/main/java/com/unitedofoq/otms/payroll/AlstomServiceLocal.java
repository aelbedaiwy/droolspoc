package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface AlstomServiceLocal {

    public OFunctionResult costCenterAllocation(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult insertAlstomGLMappedAccount(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult writeCostCenterAllocationToFile(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
}
