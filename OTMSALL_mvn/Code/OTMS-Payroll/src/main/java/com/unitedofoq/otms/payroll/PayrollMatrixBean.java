package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import static com.unitedofoq.fabs.core.encryption.DataEncryptorService.loggedUser;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeProfileHistory;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeePayroll;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryElement;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryUpgrade;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.audit.BusinessAuditBeanLocal;
import com.unitedofoq.otms.utilities.ejb.EntityUtilities;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import com.unitedofoq.otms.utilities.encryption.EncryptionUtility;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mohamed Aboelnour
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.payroll.PayrollMatrixBeanLocal",
        beanInterface = PayrollMatrixBeanLocal.class)

public class PayrollMatrixBean implements PayrollMatrixBeanLocal {

    @EJB
    private OEntityManagerRemote oem;

    @EJB
    private DataTypeServiceRemote dataTypeService;

    @EJB
    private WSUtilityLocal webServiceUtility;

    @EJB
    private PayrollCalculationBeanLocal payrollService;

    @EJB
    private EntitySetupServiceRemote entitySetupService;

    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;

    @EJB
    private DataEncryptorServiceLocal dataEncryptorService;

    @EJB
    private BusinessAuditBeanLocal audit;

    private DateTimeUtility dateTimeUtility = new DateTimeUtility();

    String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

    private boolean applyEncryption;

    String debugmMsg = null;

    //static Logger logger = Logger.getLogger(PayrollMatrixBean.class);
    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PayrollMatrixBean.class);

    private OFunctionResult salaryUpgradeRequest(Employee employee, Income income, BigDecimal oldValue, BigDecimal newValue,
            Date applyDate, OUser loggedUser, BaseEntity baseEntity) {
        EmployeeSalaryUpgrade empSalUpgrade = new EmployeeSalaryUpgrade();
        OFunctionResult oFR = new OFunctionResult();
        DateTimeUtility dateTimeUtility = new DateTimeUtility();
        try {
            empSalUpgrade.setEmployee(employee);
            empSalUpgrade.setIncome(income);
            empSalUpgrade.setValueBefore(oldValue);
            empSalUpgrade.setValueAfter(newValue);
            empSalUpgrade.setApplyDate(applyDate);
            empSalUpgrade.setCancelled("N");
            //Get From & To Calculated Period
            CalculatedPeriod from = null, to = null;
            String sql;
            sql = "select dbid from CalculatedPeriod where code like 'M%' and month = " + dateTimeUtility.MONTH_FORMAT.format(applyDate) + " and year = "
                    + dateTimeUtility.YEAR_FORMAT.format(applyDate);
            Object obj = oem.executeEntityNativeQuery(sql, loggedUser);
            if (obj != null) {
                from = (CalculatedPeriod) oem.loadEntity("CalculatedPeriod", Long.parseLong(obj.toString()), null, null, loggedUser);
            }
            sql = "select dbid from CalculatedPeriod where code like 'M%' and closed ='Y' and startdate = ("
                    + "select max(startdate) from  CalculatedPeriod where code like 'M%' and closed ='Y' )";
            obj = oem.executeEntityNativeQuery(sql, loggedUser);
            if (obj != null) {
                to = (CalculatedPeriod) oem.loadEntity("CalculatedPeriod", Long.parseLong(obj.toString()), null, null, loggedUser);
            } else {
                to = from;
            }
            if (!(from == null || to == null)) {
                if (from.getStartDate().compareTo(to.getStartDate()) > 0) {
                    from = null;
                    to = null;
                }
            }
            empSalUpgrade.setFromCalculatedPeriod(from);
            empSalUpgrade.setToCalculatedPeriod(to);

            if (baseEntity instanceof EmployeeProfileHistory) {
                empSalUpgrade.setProfileHistory((EmployeeProfileHistory) baseEntity);
            } else if (baseEntity instanceof PayrollMatrix) {
                empSalUpgrade.setPayrollMatrix((PayrollMatrix) baseEntity);
            }
            sql = "Select Max(TransactionNumber) "
                    + " From EmployeeSalaryUpgrade "
                    + " Where employee_Dbid = " + employee.getDbid()
                    + " AND income_dbid = " + income.getDbid();
            obj = oem.executeEntityNativeQuery(sql, loggedUser);
            BigDecimal transactionNo = BigDecimal.ONE;
            if (obj != null) {
                transactionNo = new BigDecimal(obj.toString());
                transactionNo = transactionNo.add(BigDecimal.ONE);
            }
            empSalUpgrade.setTransactionNumber(transactionNo);
            empSalUpgrade.setApplied("Y");
            oem.saveEntity(empSalUpgrade, loggedUser);
            oFR.append(applySalaryUpgrade(empSalUpgrade, loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    private OFunctionResult applySalaryUpgrade(EmployeeSalaryUpgrade salaryUpgrade, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            List conditions = new ArrayList();
            conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
            conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
            EmployeeSalaryElement ese = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
            if (ese != null) {
                ese.setValue(salaryUpgrade.getValueAfter());
                ese.setTransactionType("Apply Payroll Matrix");
                oem.saveEntity(ese, loggedUser);
            }
            if (salaryUpgrade.getFromCalculatedPeriod() != null
                    && salaryUpgrade.getToCalculatedPeriod() != null) {
                //________________________________
                String sql;
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String symmetricKey = dataEncryptorService.getSymmetricKey();
                String ivKey = dataEncryptorService.getIvKey();

                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    applyEncryption = EncryptionUtility.checkApplyingEncryption(loggedUser);
                    if (applyEncryption) {
                        sql = "Update Emp_Sal_Elements_History "
                                + " Set Element_ValueEnc = encrypt(" + salaryUpgrade.getValueAfter() + ",'" + symmetricKey + "'," + ivKey + ")"
                                + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                                + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                                + " and calculated_period_id in "
                                + " (select calculated_period_id from calculated_period,payment_method "
                                + " where  to_date(start_date) >= to_date('"
                                + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "','YYYY-MM-dd')"
                                + " AND to_date(start_date) <= to_date('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "','YYYY-MM-dd')"
                                + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                                + " AND payment_method.payment_unit_id = 'M' )";
                    } else {
                        sql = "Update Emp_Sal_Elements_History "
                                + " Set Element_Value = '" + salaryUpgrade.getValueAfter() + "'"
                                + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                                + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                                + " and calculated_period_id in "
                                + " (select calculated_period_id from calculated_period,payment_method "
                                + " where  to_date(start_date) >= to_date('"
                                + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "','YYYY-MM-dd')"
                                + " AND to_date(start_date) <= to_date('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "','YYYY-MM-dd')"
                                + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                                + " AND payment_method.payment_unit_id = 'M' )";
                    }
                } else {
                    sql = "Update Emp_Sal_Elements_History "
                            + " Set Element_Value = " + salaryUpgrade.getValueAfter()
                            + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                            + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                            + " and calculated_period_id in "
                            + " (select calculated_period_id from calculated_period,payment_method "
                            + " where start_date >='" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "'"
                            + " AND start_date <='" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "'"
                            + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                            + " AND payment_method.payment_unit_id = 'M' )";
                }
                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                //________________________________
            }
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("salaryUpgrade003", loggedUser));
            //___________________Apply Net To Gross Business________________________________
            String internalCode;
            internalCode = salaryUpgrade.getIncome().getInternalCode();
            if (internalCode != null && (internalCode.toLowerCase().contains("gross")
                    || internalCode.toLowerCase().contains("net")
                    || internalCode.toLowerCase().contains("july"))) {
                conditions.clear();
                conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
                EmployeeSalaryElement ESE = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                if (ESE != null) {
                    entitySetupService.callEntityUpdateAction(ESE, loggedUser);
                }
            }
            //___________________Apply Net To Gross Business________________________________
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult payrollMatrixAffect(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult oFR = new OFunctionResult();
        try {
            if (odm.getData().get(0).getClass().getSimpleName().equals("PayrollMatrix")) {
                PayrollMatrix payrollMatrix = (PayrollMatrix) odm.getData().get(0);
                oFR.append(applyPayrollMatrix(payrollMatrix, loggedUser));
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("Employee")) {
                Employee employee = (Employee) odm.getData().get(0);
                oFR.append(applyPayrollMatrix(employee, loggedUser));
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeProfileHistory")) {
                oFR.append(cancelPayrollMatrixAffect(odm, functionParms, loggedUser));
                EmployeeProfileHistory empProfileHistory = (EmployeeProfileHistory) odm.getData().get(0);
                oFR.append(applyPayrollMatrix(empProfileHistory, loggedUser));
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeePayroll")) {
                EmployeePayroll payroll = (EmployeePayroll) odm.getData().get(0);
                oFR.append(applyPayrollMatrix(payroll, loggedUser));
            }
            //should be changed to core error codes
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("PayMatrixApply-001", loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            //should be changed to core error codes
            oFR.addError(userMessageServiceRemote.getUserMessage("PayMatrixApply-002", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult cancelPayrollMatrixAffect(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            logger.info("In Cancel Function");
            logger.debug("This is debugging message");
            logger.trace("Trace Message test");

            String sql = "";
            if (odm.getData().get(0).getClass().getSimpleName().equals("PayrollMatrix")) {
                PayrollMatrix payrollMatrix = (PayrollMatrix) odm.getData().get(0);
                if (payrollMatrix != null) {
                    sql = "select dbid from EmployeeSalaryUpgrade where cancelled = 'N' and payrollMatrix_dbid = " + payrollMatrix.getDbid();
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeProfileHistory")) {
                EmployeeProfileHistory profileHistory = (EmployeeProfileHistory) odm.getData().get(0);
                if (profileHistory != null) {
                    sql = "select dbid from EmployeeSalaryUpgrade where cancelled = 'N' and profileHistory_dbid = " + profileHistory.getDbid();
                }
            }
            List<Object> objects = oem.executeEntityListNativeQuery(sql, loggedUser);
            String tmpSting;
            int counter;
            List<String> dbidsCond;
            dbidsCond = new ArrayList<String>();
            if (!objects.isEmpty()) {
                tmpSting = "dbid in (";
                counter = 0;
                for (Object obj : objects) {
                    if (counter == 0) {
                        tmpSting += obj.toString();
                    } else {
                        tmpSting += "," + obj.toString();
                    }
                    counter++;
                }
                tmpSting += ")";
                dbidsCond.add(tmpSting);
                List<EmployeeSalaryUpgrade> salaryUpgrades = oem.loadEntityList("EmployeeSalaryUpgrade", dbidsCond, null, null, loggedUser);
                for (EmployeeSalaryUpgrade salaryUpgrade : salaryUpgrades) {
                    oFR.append(cancelSalaryUpgrade(salaryUpgrade, loggedUser));
                    audit.log(salaryUpgrade.getPayrollMatrix(), salaryUpgrade.getEmployee(), true, salaryUpgrade.getValueBefore(), true, new Date(), null, Thread.currentThread().getStackTrace()[1].getMethodName(), loggedUser);
                }
            }
            if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeProfileHistory")) {
                //Should Remove Rules From Employee Salary Upgrade If Not match Criteria
                //By Downgrade value to zero.
                //This code should be refactored
                EmployeeProfileHistory profileHistory = ((EmployeeProfileHistory) odm.getData().get(0));
                sql = "Select dbid From PayrollMatrix where deleted = 0 and salaryElement_dbid in ("
                        + "Select Distinct income_dbid From EmployeeSalaryUpgrade "
                        + "Where (cancelled = 'N') AND (payrollMatrix_dbid is not null or "
                        + "profileHistory_dbid is not null) AND (employee_dbid = " + profileHistory.getEmployee().getDbid()
                        + ") ) ";

                objects = oem.executeEntityListNativeQuery(sql, loggedUser);
                if (!objects.isEmpty()) {
                    tmpSting = "dbid in (";
                    counter = 0;
                    for (Object obj : objects) {
                        if (counter == 0) {
                            tmpSting += obj.toString();
                        } else {
                            tmpSting += "," + obj.toString();
                        }
                        counter++;
                    }
                    tmpSting += ")";

                    List sorts = new ArrayList();
                    sorts.add("sequences asc");
                    dbidsCond.clear();
                    dbidsCond.add(tmpSting);
                    EmployeeSalaryElement empSalElement;
                    List<PayrollMatrix> payrollMatrixs = oem.loadEntityList("PayrollMatrix", dbidsCond, null, sorts, loggedUser);
                    for (PayrollMatrix payMatrix : payrollMatrixs) {
                        List conditions = getPayrollMatrixConditions(payMatrix, loggedUser);
                        conditions.add("dbid =" + profileHistory.getEmployee().getDbid());
                        Employee emp = (Employee) oem.loadEntity("Employee", conditions, null, loggedUser);
                        if (emp == null) {
                            conditions.clear();
                            conditions.add("employee.dbid =" + profileHistory.getEmployee().getDbid());
                            conditions.add("salaryElement.dbid=" + payMatrix.getSalaryElement().getDbid());
                            empSalElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                            if (empSalElement != null
                                    && empSalElement.getValue().doubleValue() != 0.0) {
                                oFR.append(salaryUpgradeRequest(profileHistory.getEmployee(), (Income) payMatrix.getSalaryElement(), empSalElement.getValue(), BigDecimal.ZERO,
                                        profileHistory.getActionDate(), loggedUser, payMatrix));
                            }
                        }
                    }
                }
            }
            //should be changed to core error codes
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("PayMatrixCancel-001", loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("PayMatrixCancel-002", loggedUser));
        }

        return oFR;
    }

    private List<String> getPayrollMatrixConditions(PayrollMatrix payrollMatrix, OUser loggedUser) {
        List conds = new ArrayList();
        try {
            if (payrollMatrix.getUnit() != null && payrollMatrix.getUnit().getDbid() != 0) {
                if (payrollMatrix.isIncludeSubUnits()) {
                    conds.add("positionSimpleMode.unit.hierarchycode like '" + payrollMatrix.getUnit().getHierarchycode() + "%'");
                } else {
                    conds.add("positionSimpleMode.unit.dbid = " + payrollMatrix.getUnit().getDbid());
                }
            }
            if (payrollMatrix.getPosition() != null && payrollMatrix.getPosition().getDbid() != 0) {
                if (payrollMatrix.isIncludeSubPositions()) {
                    conds.add("positionSimpleMode.hierarchycode like '" + payrollMatrix.getPosition().getHierarchycode() + "%'");
                } else {
                    conds.add("positionSimpleMode.dbid = " + payrollMatrix.getPosition().getDbid());
                }
            }
            if (payrollMatrix.getJob() != null && payrollMatrix.getJob().getDbid() != 0) {
                conds.add("positionSimpleMode.job.dbid = " + payrollMatrix.getJob().getDbid());
            }
            if (payrollMatrix.getPayGrade() != null && payrollMatrix.getPayGrade().getDbid() != 0) {
                conds.add("payroll.payGrade.dbid = " + payrollMatrix.getPayGrade().getDbid());
            }
            if (payrollMatrix.getLocation() != null && payrollMatrix.getLocation().getDbid() != 0) {
                conds.add("payroll.location.dbid = " + payrollMatrix.getLocation().getDbid());
            }
            if (payrollMatrix.getHiringType() != null && payrollMatrix.getHiringType().getDbid() != 0) {
                conds.add("hiringType.dbid = " + payrollMatrix.getHiringType().getDbid());
            }

            if (payrollMatrix.getCc1() != null && payrollMatrix.getCc1().getDbid() != 0) {
                conds.add("cc1.dbid = " + payrollMatrix.getCc1().getDbid());
            }
            if (payrollMatrix.getCc2() != null && payrollMatrix.getCc2().getDbid() != 0) {
                conds.add("cc2.dbid = " + payrollMatrix.getCc2().getDbid());
            }
            if (payrollMatrix.getCc3() != null && payrollMatrix.getCc3().getDbid() != 0) {
                conds.add("cc3.dbid = " + payrollMatrix.getCc3().getDbid());
            }
            if (payrollMatrix.getCc4() != null && payrollMatrix.getCc4().getDbid() != 0) {
                conds.add("cc4.dbid = " + payrollMatrix.getCc4().getDbid());
            }
            if (payrollMatrix.getCc5() != null && payrollMatrix.getCc5().getDbid() != 0) {
                conds.add("cc5.dbid = " + payrollMatrix.getCc5().getDbid());
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return conds;

    }

    private OFunctionResult cancelSalaryUpgrade(EmployeeSalaryUpgrade salaryUpgrade, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {

            if (salaryUpgrade == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            if (salaryUpgrade.getCancelled().equals("Y")) {
                return oFR;
            }
            if (salaryUpgrade.getApplied() != null && salaryUpgrade.getApplied().equals("Y")) {
                List conditions = new ArrayList();
                conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
                EmployeeSalaryElement ese = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                if (ese != null) {
                    ese.setValue(salaryUpgrade.getValueBefore());
                    ese.setTransactionType("Cancel Payroll Matrix");
                    oem.saveEntity(ese, loggedUser);
                }
                if (salaryUpgrade.getFromCalculatedPeriod() != null
                        && salaryUpgrade.getToCalculatedPeriod() != null) {
                    //________________________________
                    String sql;
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String symmetricKey = dataEncryptorService.getSymmetricKey();
                    String ivKey = dataEncryptorService.getIvKey();

                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        applyEncryption = EncryptionUtility.checkApplyingEncryption(loggedUser);
                        if (applyEncryption) {
                            sql = "Update Emp_Sal_Elements_History "
                                    + " Set Element_ValueEnc = encrypt(" + salaryUpgrade.getValueBefore() + ",'" + symmetricKey + "'," + ivKey + ")"
                                    + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                                    + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                                    + " and calculated_period_id in "
                                    + " (select calculated_period_id from calculated_period,payment_method "
                                    + " where  to_date(start_date) >= to_date('"
                                    + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "','YYYY-MM-dd')"
                                    + " AND to_date(start_date) <= to_date('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "','YYYY-MM-dd')"
                                    + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                                    + " AND payment_method.payment_unit_id = 'M' )";
                        } else {
                            sql = "Update Emp_Sal_Elements_History "
                                    + " Set Element_Value = '" + salaryUpgrade.getValueBefore() + "'"
                                    + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                                    + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                                    + " and calculated_period_id in "
                                    + " (select calculated_period_id from calculated_period,payment_method "
                                    + " where  to_date(start_date) >= to_date('"
                                    + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "','YYYY-MM-dd')"
                                    + " AND to_date(start_date) <= to_date('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "','YYYY-MM-dd')"
                                    + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                                    + " AND payment_method.payment_unit_id = 'M' )";
                        }
                    } else {
                        sql = "Update Emp_Sal_Elements_History "
                                + " Set Element_Value = " + salaryUpgrade.getValueBefore()
                                + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                                + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                                + " and calculated_period_id in "
                                + " (select calculated_period_id from calculated_period,payment_method "
                                + " where start_date >='" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "'"
                                + " AND start_date <='" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "'"
                                + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                                + " AND payment_method.payment_unit_id = 'M' )";
                    }
                    oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                    //________________________________
                }

            }
            salaryUpgrade.setCancelled("Y");
            oem.saveEntity(salaryUpgrade, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return oFR;
    }

    @Override
    public OFunctionResult payrollMatrixAffectForAll(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult oFR = new OFunctionResult();
        try {
            if (odm.getData().get(0).getClass().getSimpleName().equals("PayrollMatrix")) {
                List<PayrollMatrix> payrollMatrixs;
                List sorts = new ArrayList();
                sorts.add("sequences asc");
                payrollMatrixs = oem.loadEntityList("PayrollMatrix", null, null, sorts, loggedUser);

                for (PayrollMatrix payrollMatrix : payrollMatrixs) {
                    oFR.append(applyPayrollMatrix(payrollMatrix, loggedUser));
                }
            }
            //should be changed to core error codes
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("PayMatrixApply-001", loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            //should be changed to core error codes
            oFR.addError(userMessageServiceRemote.getUserMessage("PayMatrixApply-002", loggedUser));
        }
        return oFR;
    }

    private OFunctionResult applyPayrollMatrix(PayrollMatrix payrollMatrix, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List conds = getPayrollMatrixConditions(payrollMatrix, loggedUser);
            List<Employee> emps = oem.loadEntityList("Employee", conds, null, null, loggedUser);
            for (Employee employee : emps) {
                conds.clear();
                conds.add("employee.dbid =" + employee.getDbid());
                conds.add("salaryElement.dbid=" + payrollMatrix.getSalaryElement().getDbid());
                EmployeeSalaryElement empSalElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conds, null, loggedUser);
                if (empSalElement != null) {
                    if (empSalElement.getValue().doubleValue() != payrollMatrix.getAmount().doubleValue()) {
                        ofr.append(salaryUpgradeRequest(employee, (Income) payrollMatrix.getSalaryElement(), empSalElement.getValue(), payrollMatrix.getAmount(),
                                payrollMatrix.getApplyDate() == null ? new Date() : payrollMatrix.getApplyDate(),
                                loggedUser, payrollMatrix));
                        audit.log(payrollMatrix, employee, true, BigDecimal.valueOf(empSalElement.getValue().doubleValue()), true, new Date(), null, Thread.currentThread().getStackTrace()[1].getMethodName(), loggedUser);
                    } else {
                        audit.log(payrollMatrix, employee, true, BigDecimal.valueOf(empSalElement.getValue().doubleValue()), false, new Date(), null, Thread.currentThread().getStackTrace()[1].getMethodName(), loggedUser);
                    }
                } else {
                    audit.log(payrollMatrix, employee, false, null, false, new Date(), null, Thread.currentThread().getStackTrace()[1].getMethodName(), loggedUser);
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult applyPayrollMatrix(Employee employee, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List conds = new ArrayList();
            conds.add("sequences asc");
            String salaryElementDbids = "";
            List<PayrollMatrix> payrollMatrix = oem.loadEntityList("PayrollMatrix", null, null, conds, loggedUser);
            for (PayrollMatrix payMatrix : payrollMatrix) {
                List conditions = getPayrollMatrixConditions(payMatrix, loggedUser);
                conditions.add("dbid =" + employee.getDbid());
                Employee emp = (Employee) oem.loadEntity("Employee", conditions, null, loggedUser);
                if (emp != null) {
                    if (!salaryElementDbids.contains(Long.toString(payMatrix.getSalaryElement().getDbid()))) {
                        salaryElementDbids += "," + payMatrix.getSalaryElement().getDbid();
                        conds.clear();
                        conds.add("employee.dbid =" + employee.getDbid());
                        conds.add("salaryElement.dbid=" + payMatrix.getSalaryElement().getDbid());
                        EmployeeSalaryElement empSalElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conds, null, loggedUser);
                        if (empSalElement != null) {
                            empSalElement.setValue(payMatrix.getAmount());
                            oem.saveEntity(empSalElement, loggedUser);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult applyPayrollMatrix(EmployeeProfileHistory empProfileHistory, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List sorts = new ArrayList();
            sorts.add("sequences asc");
            String salaryElementDbids = "";

            String sql = getPayrollMatrixSQL(empProfileHistory);

            if (sql.equals("")) {
                return ofr;
            }
            List<Object> dbids;

            dbids = oem.executeEntityListNativeQuery(sql, loggedUser);

            if (dbids.isEmpty()) {
                return ofr;
            }

            String tmpSting = "dbid in (";
            int counter = 0;
            for (Object obj : dbids) {
                if (counter == 0) {
                    tmpSting += obj.toString();
                } else {
                    tmpSting += "," + obj.toString();
                }
                counter++;
            }
            tmpSting += ")";
            List<String> dbidsCond;
            dbidsCond = new ArrayList<String>();
            dbidsCond.add(tmpSting);

            List<PayrollMatrix> payrollMatrixList = oem.loadEntityList("PayrollMatrix", dbidsCond, null, sorts, loggedUser);
            ofr.append(applyPayrollMatrix(payrollMatrixList, empProfileHistory, loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult applyPayrollMatrix(EmployeePayroll payroll, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List sorts = new ArrayList();
            sorts.add("sequences asc");
            String sql = getPayrollMatrixSQL(payroll);
            if (sql.equals("")) {
                return ofr;
            }

            List<Long> dbids;
            dbids = oem.executeEntityListNativeQuery(sql, loggedUser);

            if (dbids.isEmpty()) {
                return ofr;
            }

            String tmpSting = "dbid in (";
            int counter = 0;
            for (Object obj : dbids) {
                if (counter == 0) {
                    tmpSting += obj.toString();
                } else {
                    tmpSting += "," + obj.toString();
                }
                counter++;
            }
            tmpSting += ")";
            List<String> dbidsCond;
            dbidsCond = new ArrayList<String>();
            dbidsCond.add(tmpSting);

            List<PayrollMatrix> payrollMatrix = oem.loadEntityList("PayrollMatrix", dbidsCond, null, null, loggedUser);
            ofr.append(applyPayrollMatrix(payrollMatrix, payroll, loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult applyPayrollMatrix(List<PayrollMatrix> payrollMatrixList, BaseEntity baseEntity, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String salaryElementDbids = "";
            long employeeDbid = 0;
            Date actionDate = new Date();
            if (baseEntity instanceof EmployeeProfileHistory) {
                employeeDbid = ((EmployeeProfileHistory) baseEntity).getEmployee().getDbid();
                actionDate = ((EmployeeProfileHistory) baseEntity).getActionDate();
            } else if (baseEntity instanceof EmployeePayroll) {
                employeeDbid = ((EmployeePayroll) baseEntity).getEmployee().getDbid();
                actionDate = new Date();
            } else if (baseEntity instanceof PayrollMatrix) {
                employeeDbid = 0;
                actionDate = ((PayrollMatrix) baseEntity).getApplyDate();
            }

            for (PayrollMatrix payrollMatrix : payrollMatrixList) {
                List conditions = getPayrollMatrixConditions(payrollMatrix, loggedUser);
                conditions.add("dbid =" + employeeDbid);
                Employee emp = (Employee) oem.loadEntity("Employee", conditions, null, loggedUser);
                if (emp != null) {
                    if (!salaryElementDbids.contains(Long.toString(payrollMatrix.getSalaryElement().getDbid()))) {
                        salaryElementDbids += "," + payrollMatrix.getSalaryElement().getDbid();
                        conditions.clear();
                        conditions.add("employee.dbid =" + emp.getDbid());
                        conditions.add("salaryElement.dbid=" + payrollMatrix.getSalaryElement().getDbid());
                        EmployeeSalaryElement empSalElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                        if (empSalElement != null) {
                            if (empSalElement.getValue().doubleValue() != payrollMatrix.getAmount().doubleValue()) {
                                ofr.append(salaryUpgradeRequest(emp, (Income) payrollMatrix.getSalaryElement(), empSalElement.getValue(),
                                        payrollMatrix.getAmount(), actionDate, loggedUser, baseEntity));
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private String getPayrollMatrixSQL(EmployeeProfileHistory profileHistory) {
        String sql = "";
        if (profileHistory.getPayGrade() != null) {
            sql = "select dbid from PayrollMatrix where payGrade_dbid = " + profileHistory.getPayGrade().getDbid();
        }
        if (profileHistory.getLocation() != null) {
            if (!sql.equals("")) {
                sql += " union ";
            }
            sql += "select dbid from PayrollMatrix where location_dbid = " + profileHistory.getLocation().getDbid();
        }
        if (profileHistory.getHiringType() != null) {
            if (!sql.equals("")) {
                sql += " union ";
            }
            sql += "select dbid from PayrollMatrix where hiringType_dbid = " + profileHistory.getHiringType().getDbid();
        }
        if (profileHistory.getPosition().getDbid()
                != profileHistory.getEmployee().getPositionSimpleMode().getDbid()) {
            if (!sql.equals("")) {
                sql += " union ";
            }
            sql += "select dbid from PayrollMatrix where position_dbid = " + profileHistory.getPosition().getDbid();
        }
        return sql;
    }

    private String getPayrollMatrixSQL(EmployeePayroll payroll) {
        String sql = "";
        if (payroll.getPayGrade() != null) {
            sql = "select dbid from PayrollMatrix where payGrade_dbid = " + payroll.getPayGrade().getDbid();
        }
        if (payroll.getLocation() != null) {
            if (!sql.equals("")) {
                sql += " union ";
            }
            sql += "select dbid from PayrollMatrix where location_dbid = " + payroll.getLocation().getDbid();
        }

        return sql;
    }

}
