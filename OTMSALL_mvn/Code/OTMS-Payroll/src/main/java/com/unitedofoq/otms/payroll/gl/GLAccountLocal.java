/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author lahmed
 */
@Local
public interface GLAccountLocal {

    public OFunctionResult insertGLMappedAccount(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);
}