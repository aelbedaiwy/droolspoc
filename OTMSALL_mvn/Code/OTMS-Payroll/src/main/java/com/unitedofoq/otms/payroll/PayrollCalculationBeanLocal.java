package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface PayrollCalculationBeanLocal {

    public OFunctionResult initializeMonthlyCalculatedPeriod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult initializeSeparateCalculatedPeriod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runSalaryCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runSeparateCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runRetroCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runPersonnelClosing(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult runPersonnelClosingAndSalaryCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult closeMonth(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}
