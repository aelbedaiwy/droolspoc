package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.dd.DD;
import static com.unitedofoq.fabs.core.encryption.DataEncryptorService.loggedUser;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManager;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.WebServiceFunction;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.setup.FABSSetupLocal;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCType;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.webservice.OWebService;
import com.unitedofoq.fabs.core.webservice.WebServiceFilter;
import com.unitedofoq.otms.core.AuditTrail;
import com.unitedofoq.otms.foundation.company.CompanyCostCenter;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeDependance;
import com.unitedofoq.otms.foundation.employee.EmployeeProfileHistory;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.costcenter.CostCenterCalculation;
import com.unitedofoq.otms.payroll.costcenter.EmpCostCenterException;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeBenifit;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeBill;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeCostCenter;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeePayroll;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSEAssign;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSEMaintenance;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalary;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryElement;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryUpgrade;
import com.unitedofoq.otms.payroll.insurance.Insurance;
import com.unitedofoq.otms.payroll.insurance.InsuranceCategory;
import com.unitedofoq.otms.payroll.insurance.InsuranceGroup;
import com.unitedofoq.otms.payroll.insurance.InsuranceGroupMember;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.GradeSalaryElement;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElementFormula;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElementType;
import com.unitedofoq.otms.payroll.salaryupgrade.ImportEmployeeSalaryUpgrade;
import com.unitedofoq.otms.payroll.tax.Tax;
import com.unitedofoq.otms.payroll.tax.TaxBreak;
import com.unitedofoq.otms.payroll.tax.TaxGroup;
import com.unitedofoq.otms.payroll.tax.TaxGroupCalculation;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.personnel.penalty.EmployeePenaltyRequest;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationRequest;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.ejb.EntityUtilities;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import com.unitedofoq.otms.utilities.encryption.EncryptionUtility;
import com.unitedofoq.otms.utilities.filter.FilterUtility;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.InitialContext;
import javax.sql.DataSource;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.payroll.PayrollValidationLocal",
        beanInterface = PayrollValidationLocal.class)
public class PayrollValidationBean implements PayrollValidationLocal {

    @EJB
    private OFunctionServiceRemote functionService;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private PayrollValidationLocal payrollService;
    @EJB
    private WSUtilityLocal webServiceUtility;
    @EJB
    private FABSSetupLocal fabsSetupRemote;
    @EJB
    private DataSecurityServiceLocal dataSecurityServiceLocal;
    public static final DateFormat DATE_FOTMAT = new SimpleDateFormat("yyyy-MM-dd");

    String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

    private boolean applyEncryption;

    @EJB
    private DataEncryptorServiceLocal dataEncryptorService;

    /**
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult validateSalaryElement(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) odm.getData().get(0);
            //1-Get all employee salary element if the salary element already
            //assigned to the employee so stop save
            List<String> employeeSalaryConditions = new ArrayList<String>();
            employeeSalaryConditions.add("employee.dbid = " + employeeSalaryElement.getEmployee().getDbid());
            employeeSalaryConditions.add("salaryElement.dbid = " + employeeSalaryElement.getSalaryElement().getDbid());
            employeeSalaryConditions.add("inActive = false");

            List<EmployeeSalaryElement> employeeSalaryElements = oem.loadEntityList(
                    EmployeeSalaryElement.class.getSimpleName(),
                    employeeSalaryConditions, null, null, loggedUser);

            if (employeeSalaryElements.size() == 1
                    && employeeSalaryElements.get(0).getDbid() != employeeSalaryElement.getDbid()) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "SalaryElementAssign", systemUser));

            } else if (employeeSalaryElements.size() > 1) {
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "SalaryElementAssign", systemUser));
            } else //Case1: The  employeeSalaryElement is new object set sequence and monthly values.
            {
                if (employeeSalaryElement.getDbid() == 0 && employeeSalaryElement.getSalaryElement() != null) {
                    employeeSalaryElement.setSortIndex(employeeSalaryElement.getSalaryElement().getSortIndex());
                    employeeSalaryElement.setMonthly(employeeSalaryElement.getSalaryElement().getMonthly());
                } else {
                    //Load the orginal employee salary element to check the other two cases.
                    EmployeeSalaryElement orginalEmployeeSalaryElement = (EmployeeSalaryElement) oem.loadEntity(EmployeeSalaryElement.class.getSimpleName(),
                            employeeSalaryElement.getDbid(), null, null, loggedUser);
                    //Case2: The Salary Element has been changed. Get the new one values and set them to the employeeSalaryElement.
                    if (!orginalEmployeeSalaryElement.getSalaryElement().equals(
                            employeeSalaryElement.getSalaryElement())) {
                        employeeSalaryElement.setSortIndex(employeeSalaryElement.getSalaryElement().getSortIndex());
                        employeeSalaryElement.setMonthly(employeeSalaryElement.getSalaryElement().getMonthly());
                    }
                    //Other cases: Do Nothing
                }
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
        } finally {
        }

        return oFR;
    }

    /*
     * - Brief Description: when the user chose the data of the calculated period (mandatory fields) system will auto
     *   generate the calculated period code
     * - Algorithms:
     *   a.	Calculated period generation function ( by selecting the month/ year/ pay method/ pay period)
     *   b.	The generation of the code is as following:
     *     o	Case pay method of types: M,E,A,R
     *      I.	Application count the existing calculated period of same type and add 1
     *      II.	The coding will be :
     *         •	in case count < 999 : the type (M or E or A or R) + serial in form of 3 digits
     *         •	in case count > 999: the type (M or E or A or R) + serial
     *     o	case pay method of type: S
     *      I.	Application count the existing calculated period of same type (for all S pay period and pay method) and add 1
     *      II.	The coding will be :
     *          •	in case count < 999 : the type (M or E or A or R) + serial in form of 3 digits
     *          •	in case count > 999: the type (M or E or A or R) + serial
     *     where user can create multi separate per month  provided that they have different pay method and pay period
     *     o	case pay method of type: W
     *      I.	Application count the existing calculated period of same type  and add 1
     *      II.	The coding will be :
     *          •	in case count < 999 : the type (M or E or A or R) + serial in form of 3 digits
     *          •	in case count > 999: the type (M or E or A or R) + serial
     *      where it will create by max 5 weekly per month (please refer to end date and start date use case)
     */
    /**
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult validateGeneratedCode(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, CalculatedPeriod.class);
        if (inputValidationErrors != null) {
            return inputValidationErrors;
        }
        // </editor-fold>
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            String newCode = "";
            CalculatedPeriod calculatedPeriod = (CalculatedPeriod) odm.getData().get(0);
            if (calculatedPeriod.getPaymentMethod().getUnit() != null && !"".equals(calculatedPeriod.getPaymentMethod().getUnit())) {
                if (calculatedPeriod.getPaymentMethod().getUnit().equals("M")
                        || calculatedPeriod.getPaymentMethod().getUnit().equals("E")
                        || calculatedPeriod.getPaymentMethod().getUnit().equals("A")
                        || calculatedPeriod.getPaymentMethod().getUnit().equals("R")) {
                    int month = calculatedPeriod.getMonth();
                    int year = calculatedPeriod.getYear();

                    List<String> conditions = new ArrayList();
                    conditions.add("month = " + month);
                    conditions.add("year = " + year);
                    conditions.add("payPeriod.dbid = " + calculatedPeriod.getPayPeriod().getDbid());
                    conditions.add("paymentMethod.dbid = " + calculatedPeriod.getPaymentMethod().getDbid());
                    conditions.add("paymentMethod.company.dbid = " + calculatedPeriod.getPaymentMethod().getCompany().getDbid());

                    if (calculatedPeriod.getDbid() != 0) {
                        conditions.add("dbid <> " + calculatedPeriod.getDbid());
                    }

                    try {
                        List<CalculatedPeriod> calPeriods = oem.loadEntityList(CalculatedPeriod.class.getSimpleName(), conditions, null, null, loggedUser);
                        if (calPeriods != null && calPeriods.size() > 0) {
                            OLog.logError("Can not save another calculated period with in the same year and month with this type.",
                                    loggedUser, "Calculated Period", calculatedPeriod);
                            oFR.addError(userMessageServiceRemote.getUserMessage("CPValidationError", systemUser));
                            oFR.addReturnValue(calculatedPeriod);
                            return oFR;
                        }
                    } catch (Exception ex) {
                        OLog.logError("Can not save another calculated period with in the same year and month with this type.",
                                loggedUser, "Calculated Period", calculatedPeriod);
                        oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
                        oFR.addReturnValue(calculatedPeriod);
                        return oFR;
                    }

                } else if (calculatedPeriod.getPaymentMethod().getUnit().equals("S")) {
                    List<String> sConds = new ArrayList<String>();
                    sConds.add("paymentMethod.dbid = " + calculatedPeriod.getPaymentMethod().getDbid());
                    sConds.add("month = " + calculatedPeriod.getMonth());
                    sConds.add("year = " + calculatedPeriod.getYear());

                    List<CalculatedPeriod> calcPeriod = oem.loadEntityList(CalculatedPeriod.class.getSimpleName(), sConds, null, null, loggedUser);

                    if (calcPeriod.size() > 0) {
                        //Error: There must be one calculated period with this payment method with this unit.
                        OLog.logError("There must be one calculated period with this payment method with this unit.",
                                loggedUser, "Calculated Period", calculatedPeriod);
                        oFR.addError(userMessageServiceRemote.getUserMessage(
                                "SystemInternalError", systemUser));
                        oFR.addReturnValue(calculatedPeriod);
                        return oFR;
                    }
                } else if (calculatedPeriod.getPaymentMethod().getUnit().equals("W")) {
                    int year = calculatedPeriod.getYear();
                    int month = calculatedPeriod.getMonth();
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("year = " + year);
                    conditions.add("month = " + month);
                    conditions.add("paymentMethod.unit = 'W'");
                    conditions.add("paymentMethod.company.dbid = " + calculatedPeriod.getPaymentMethod().getCompany().getDbid());

                    List<CalculatedPeriod> calculatedPeriods = oem.loadEntityList(CalculatedPeriod.class.getSimpleName(),
                            conditions, null, null, loggedUser);
                    if (calculatedPeriods.size() > 4) {
                        //Error: Cannot add another calculated period of type weekly.
                        OLog.logError("Cannot add another calculated period of type weekly.",
                                loggedUser, "Calculated Period", calculatedPeriod);
                        oFR.addError(userMessageServiceRemote.getUserMessage(
                                "SystemInternalError", systemUser));
                        oFR.addReturnValue(calculatedPeriod);
                        return oFR;
                    } else {
                        Date startDate = calculatedPeriod.getStartDate();
                        Date endDate = calculatedPeriod.getEndDate();
                        long diff = startDate.getTime() - endDate.getTime();
                        long numOfDays = (diff / (1000 * 60 * 60 * 24));
                        if (numOfDays > 28 && numOfDays < 32) {
                        } else if (calculatedPeriods.size() > 4) {
                            //Error: Cannot add another calculated period of type weekly.
                            OLog.logError("Cannot add another calculated period of type weekly.",
                                    loggedUser, "Calculated Period", calculatedPeriod);
                            oFR.addError(userMessageServiceRemote.getUserMessage(
                                    "SystemInternalError", systemUser));
                            oFR.addReturnValue(calculatedPeriod);
                            return oFR;
                        }
                    }
                }
                if (calculatedPeriod.getDbid() == 0) {

                    newCode = (String) calculateNextGeneratedCodeNumer(calculatedPeriod.getPaymentMethod().getUnit(), loggedUser).getReturnValues().get(0);
                    if (!"".equals(newCode)) {
                        calculatedPeriod.setCode(newCode);
                        oFR.setReturnedDataMessage(odm);
                    } else {
                        //Error: Could not generate code.
                        OLog.logError("Could not generate code.", loggedUser,
                                "Calculated Period", calculatedPeriod);
                        oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
                        oFR.addReturnValue(calculatedPeriod);
                        return oFR;
                    }
                }
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
        } finally {
        }
        return oFR;
    }

    private OFunctionResult calculateNextGeneratedCodeNumer(String unit, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            List<String> fieldExpression = new ArrayList<String>();
            fieldExpression.add("code");
            List<CalculatedPeriod> codes = oem.loadEntityList(CalculatedPeriod.class.getSimpleName(),
                    Collections.singletonList("code like '" + unit + "%'"),
                    null, fieldExpression, loggedUser);
            int codeNum = 0;
            if (codes != null && codes.size() != 0
                    && codes.get(codes.size() - 1).getCode() != null && !"".equals(codes.get(0).getCode())) {
                String latestCode = codes.get(codes.size() - 1).getCode();

                codeNum = Integer.parseInt(latestCode.substring(1));
                if (codeNum < 999) {
                    codeNum++;
                    String codeNumStr = Integer.toString(codeNum);
                    String zeros = "";
                    for (int i = codeNumStr.length(); i < 3; i++) {
                        zeros += "0";
                    }
                    oFR.addReturnValue(unit + zeros + codeNumStr);
                    return oFR;
                } else {
                    codeNum++;
                    oFR.addReturnValue(unit + Integer.toString(codeNum));
                    return oFR;
                }

            } else {
                oFR.addReturnValue(unit + "001");
                return oFR;
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "Unit", unit);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
            return oFR;
        }
    }

    @Override
    public OFunctionResult inActivateEmployeeForPayrollClosing(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        Date cpStartDate, cpEndDate;

        OFunctionResult oFR, oFRUpdate;
        oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            CalculatedPeriod newCalculatedPeriod = (CalculatedPeriod) odm.getData().get(0);
            if (newCalculatedPeriod.getClosed().equals("N")) {
                cpStartDate = newCalculatedPeriod.getStartDate();
                cpEndDate = newCalculatedPeriod.getEndDate();

                List<Employee> employees = oem.createListNamedQuery("getEmployeeForClosing", loggedUser,
                        "from", cpStartDate,
                        "to", cpEndDate);
                for (Employee employee : employees) {
                    employee.setActive(false);
                    oFRUpdate = entitySetupService.callEntityUpdateAction(employee, loggedUser);
                    if (oFRUpdate.getErrors().size() > 0) {
                        oFR.append(oFRUpdate);
                    }
                }
            }
            oFR.addSuccess(
                    userMessageServiceRemote.getUserMessage("CPEmployeesInactivated", systemUser));
            return oFR;
        } catch (Exception e) {
            OLog.logException(e, loggedUser, "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
        } finally {
        }
        return oFR;
    }

    @Override
    public OFunctionResult calculatedPeriodClosing(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR, oFRUpdate = null;
        oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            CalculatedPeriod calculatedPeriod = (CalculatedPeriod) odm.getData().get(0);
            if (calculatedPeriod.getClosed().equals("Y")
                    || !calculatedPeriod.getPaymentMethod().getUnit().equals("M")) {
                return oFR;
            }
            if (calculatedPeriod.getClosed().equals("N")) {
                if ("M".equals(calculatedPeriod.getPaymentMethod().getUnit())) {
                    oFRUpdate = inActivateEmployeeForPayrollClosing(odm, functionParms, loggedUser);
                    oFR.append(oFRUpdate);
                }
                //Closing Loan
                List loanData = new ArrayList();

                loanData.add(calculatedPeriod.getDbid());

                String webServiceCode = "WSF_ClosingLoan";
                webServiceUtility.callingWebService(webServiceCode, loanData, loggedUser);
                calculatedPeriod.setClosed("Y");
                oem.saveEntity(calculatedPeriod, loggedUser);
                //Reset Variable Element
                oFR.append(resetVariableSalaryElements(calculatedPeriod, loggedUser));
                //
                oFR.append(generateNewCalculatedPeriod(calculatedPeriod, loggedUser));
                //Close Related CPs
                oFR.append(closeRelatedCalculatedPeriod(calculatedPeriod, loggedUser));
            }
            return oFR;
        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser, "ODataMessage", odm);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;
        }
    }

    public OFunctionResult newCalculatedPeriodCreation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult(), oFRUpdate = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {

            CalculatedPeriod oldCalculatedPeriod;
            oldCalculatedPeriod = (CalculatedPeriod) odm.getData().get(0);
            generateNewCalculatedPeriod(oldCalculatedPeriod, loggedUser);
            return oFR;
        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser, "ODataMessage", odm);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;
        }
    }

    @Override
    public OFunctionResult validatePaymetMethod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        List<PaymentMethod> paymentMethods = (List<PaymentMethod>) odm.getData().get(1);
        List<PaymentMethod> compPaymentMethods = new ArrayList<PaymentMethod>(paymentMethods);

        Label1:
        for (int i = 0; i < paymentMethods.size(); i++) {
            PaymentMethod currentUpperMethod = paymentMethods.get(i);
            if (compPaymentMethods.size() != 0) {
                compPaymentMethods.remove(0);
            }
            for (int j = 0; j < compPaymentMethods.size(); j++) {
                PaymentMethod currentInnerMethod = compPaymentMethods.get(j);
                if (currentUpperMethod != currentInnerMethod
                        && currentUpperMethod.getCompany().getDbid() == currentInnerMethod.getCompany().getDbid()
                        && currentUpperMethod.getUnit().equals(currentInnerMethod.getUnit())
                        && (currentUpperMethod.getUnit().equalsIgnoreCase("M")
                        || currentUpperMethod.getUnit().equalsIgnoreCase("R")
                        || currentUpperMethod.getUnit().equalsIgnoreCase("A")
                        || currentUpperMethod.getUnit().equalsIgnoreCase("E")
                        || currentUpperMethod.getUnit().equalsIgnoreCase("W"))) {

                    String type = null;
                    if (currentUpperMethod.getUnit().equalsIgnoreCase("M")) {
                        type = "Monthly";
                    } else if (currentUpperMethod.getUnit().equalsIgnoreCase("R")) {
                        type = "Retroactive";
                    } else if (currentUpperMethod.getUnit().equalsIgnoreCase("A")) {
                        type = "Advanced";
                    } else if (currentUpperMethod.getUnit().equalsIgnoreCase("E")) {
                        type = "End Of Service";
                    } else if (currentUpperMethod.getUnit().equalsIgnoreCase("W")) {
                        type = "Weekly";
                    }

                    List<String> userMsgParams = new ArrayList<String>();
                    userMsgParams.add(type);
                    userMsgParams.add(currentUpperMethod.getCompany().getName());
                    oFR.addError(userMessageServiceRemote.getUserMessage("PayemtMethodTypeError", userMsgParams, loggedUser));
                    continue Label1;
                }
            }
        }

        return oFR;
    }

    @Override
    public OFunctionResult GetPaymetMethod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) odm.getData().get(0);

            Object newVal = odm.getData().get(2);
            SalaryElement salaryElement = (SalaryElement) newVal;
            long companyDbid = salaryElement.getCompany().getDbid();

            List<String> paymentMethodCond = new ArrayList<String>();
            paymentMethodCond.add("company.dbid = " + companyDbid);
            paymentMethodCond.add("inActive = false");
            paymentMethodCond.add("active = true");
            paymentMethodCond.add("unit = 'M'");
            List<PaymentMethod> paymentMethods = oem.loadEntityList(PaymentMethod.class.getSimpleName(),
                    paymentMethodCond, null, null, loggedUser);

            if (paymentMethods.size() > 0) {
                employeeSalaryElement.setPaymentMethod(paymentMethods.get(0));

                List<String> conds = new ArrayList<String>();
                conds.add("paymentMethod.dbid = " + paymentMethods.get(0).getDbid());
                List<PaymentPeriod> periods = oem.loadEntityList(PaymentPeriod.class.getSimpleName(), conds, null, null, loggedUser);
                if (periods != null && !periods.isEmpty()) {
                    employeeSalaryElement.setPaymentPeriod(periods.get(0));
                }
            }
            oFR.setReturnedDataMessage(odm);
            return oFR;

        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;

        } finally {
        }

    }

    @Override
    public OFunctionResult runPayrollCalcutaionWrapper(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            //get the calculated period
            PayrollCalculationWrapper payroll = (PayrollCalculationWrapper) odm.getData().get(0);
            CalculatedPeriod calcPeriod = (CalculatedPeriod) payroll.getPayrollCalculation().getCalculatedPeriod();
            oFR.append(validatePreviousCalculatedPeriodClosedOrNot(calcPeriod, loggedUser));
            if (!oFR.getErrors().isEmpty()) {
                return oFR;
            }
            oFR = payrollService.runPayCalcWarapperNewTx(odm, functionParms, loggedUser, "PCWS-001");
            OWebService ws = (OWebService) oFR.getReturnValues().get(0);
            ODataMessage wsODM = (ODataMessage) oFR.getReturnValues().get(1);
            oFR.append(functionService.runWebServiceFunction(ws, wsODM, functionParms, loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
            return oFR;
        }
        return oFR;
    }

    @Override
    public OFunctionResult getAccumlatedNetSal(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {

            PayrollCalculationWrapper calculationWrapper = (PayrollCalculationWrapper) odm.getData().get(0);
            if (calculationWrapper == null) {
                return oFR;
            }

            if (calculationWrapper.getPayrollCalculation() == null) {
                return oFR;
            }

            CalculatedPeriod calcPeriod = calculationWrapper.getPayrollCalculation().getCalculatedPeriod();

            int endMonth = calcPeriod.getMonth();

            BigDecimal netYearToDate = new BigDecimal(BigInteger.ZERO),
                    netSalary = new BigDecimal(BigInteger.ZERO);
            String updateStat = "",
                    selectEmployees = "select employee_dbid from employeedata where calculatedperiod_dbid = " + calcPeriod.getDbid(),
                    selectNetSal = "",
                    selectOutstandBal = "select outstandingBalance from employeepayroll where employee_dbid = ";

            Object outstandBalance = null;

            int year = calcPeriod.getYear();

            List<Integer> empDbids = (List<Integer>) oem.executeEntityListNativeQuery(selectEmployees, loggedUser);
            if (empDbids != null) {
                for (int i = 0; i < empDbids.size(); i++) {
                    netYearToDate = BigDecimal.ZERO;

                    for (int j = 1; j <= endMonth; j++) {
                        selectNetSal = "select netsalary from employeedata where calculatedperiod_dbid in "
                                + "(select dbid from calculatedperiod where paymentMethod_dbid = " + calcPeriod.getPaymentMethod().getDbid()
                                + " and payPeriod_dbid = " + calcPeriod.getPayPeriod().getDbid()
                                + " and month =  " + j + " and year = " + year + ")" + " and employee_dbid = " + empDbids.get(i);
                        if (oem.executeEntityNativeQuery(selectNetSal, loggedUser) == null) {
                            continue;
                        }
                        netSalary = new BigDecimal((oem.executeEntityNativeQuery(selectNetSal, loggedUser)).toString());
                        netYearToDate = netYearToDate.add(netSalary);
                    }

                    outstandBalance = oem.executeEntityNativeQuery(selectOutstandBal + empDbids.get(i), loggedUser);
                    if (outstandBalance != null) {
                        netYearToDate = netYearToDate.add(new BigDecimal(outstandBalance.toString()));
                    }

                    updateStat = "update employeedata set netYearToDate = " + (netYearToDate.setScale(5, RoundingMode.UP)).toString()
                            + " where calculatedperiod_dbid = " + calcPeriod.getDbid() + " and employee_dbid = " + empDbids.get(i);

                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                }
            }
        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        } finally {
            return oFR;
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult runPayCalcWarapperNewTx(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser, String wsCode) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            //WebServiceFunction wsFunction = PCWS-001
            PayrollCalculationWrapper calculationWrapper = (PayrollCalculationWrapper) odm.getData().get(0);
            WebServiceFunction wsFunction = (WebServiceFunction) oem.loadEntity(WebServiceFunction.class.getSimpleName(), Collections.singletonList("code = '" + wsCode + "'"), null, loggedUser);
            OWebService ws = wsFunction.getWebService();
            WebServiceFilter wsFilter = ws.getServiceFilter();
            for (int i = 0; i < wsFilter.getFilterFields().size(); i++) {
                if (wsFilter.getFilterFields().get(i).isInActive()) {
                    continue;
                }
                if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("personalInfo")) {
                    if (calculationWrapper.getEmployee() != null && calculationWrapper.getEmployee().getDbid() != 0) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getEmployee().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getEmployee().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                } else if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("payroll.location")) {
                    if (calculationWrapper.getLocation() != null && calculationWrapper.getLocation().getDbid() != 0) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getLocation().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getLocation().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                } else if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("employeeMainPosition.position.unit")
                        || wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("positionSimpleMode.unit")) {
                    if (calculationWrapper.getUnit() != null && calculationWrapper.getUnit().getDbid() != 0) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getUnit().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getUnit().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                } else if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("employeeMainPosition.position.job")
                        || wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("positionSimpleMode.job")) {
                    if (calculationWrapper.getJob() != null && calculationWrapper.getJob().getDbid() != 0) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getJob().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getJob().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                } else if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("hiring.hiringType")) {
                    if (calculationWrapper.getHiringType() != null) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getHiringType().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getHiringType().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                } else if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("employeeMainPosition.position.payGrade")
                        || wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("positionSimpleMode.payGrade")) {
                    if (calculationWrapper.getPayGrade() != null && calculationWrapper.getPayGrade().getDbid() != 0) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getPayGrade().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getPayGrade().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                } else if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("payroll.costCenter")) {
                    if (calculationWrapper.getCostCenter() != null && calculationWrapper.getCostCenter().getDbid() != 0) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getCostCenter().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getCostCenter().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                } else if (wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("employeeMainPosition.position.name")
                        || wsFilter.getFilterFields().get(i).getFieldExpression().startsWith("positionSimpleMode")) {
                    if (calculationWrapper.getPosition() != null && calculationWrapper.getPosition().getDbid() != 0) {
                        wsFilter.getFilterFields().get(i).setFieldValue(Long.toString(calculationWrapper.getPosition().getDbid()));
                        wsFilter.getFilterFields().get(i).setCustom2(Long.toString(calculationWrapper.getPosition().getDbid()));
                    } else {
                        wsFilter.getFilterFields().get(i).setFieldValue(null);
                        wsFilter.getFilterFields().get(i).setCustom2(null);
                    }
                }
            }
            ODataType odt = wsFunction.getOdataType();
            List dataList = new ArrayList();
            if (wsCode.equals("WSF2")) {
                dataList.add(calculationWrapper.getPayrollCalculation().getCalculatedPeriod().getDbid());
            } else {
                dataList.add(calculationWrapper.getPayrollCalculation().getDbid());
            }
            ODataMessage wsODM = new ODataMessage(odt, dataList);
            oFR.getReturnValues().add(ws);
            oFR.getReturnValues().add(wsODM);

        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser), e);
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult runPersonalClosingWrapper(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            PayrollCalculationWrapper payroll = (PayrollCalculationWrapper) odm.getData().get(0);
            CalculatedPeriod calcPeriod = (CalculatedPeriod) payroll.getPayrollCalculation().getCalculatedPeriod();
            oFR.append(validatePreviousCalculatedPeriodClosedOrNot(calcPeriod, loggedUser));
            if (oFR.getErrors().isEmpty()) {
                oFR = payrollService.runPayCalcWarapperNewTx(odm, functionParms, loggedUser, "WSF2");
                OWebService ws = (OWebService) oFR.getReturnValues().get(0);
                ODataMessage wsODM = (ODataMessage) oFR.getReturnValues().get(1);
                oFR.append(functionService.runWebServiceFunction(ws, wsODM, functionParms, loggedUser));
            }
        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
        }
        return oFR;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult runPersonalClosingWrapperNewTx(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        return runPersonalClosingWrapper(odm, functionParms, loggedUser);
    }

    @Override
    public OFunctionResult validateCashOrBank(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        EmployeePayroll employeePayroll = (EmployeePayroll) odm.getData().get(0);
        OUser systemUser = oem.getSystemUser(loggedUser);

        try {
            if (employeePayroll.getCashOrBank() != null) {
                Character cashOrBank = employeePayroll.getCashOrBank().charAt(0);

                if (cashOrBank.equals('C') || cashOrBank.equals('c')
                        || cashOrBank.equals('S') || cashOrBank.equals('s')) {
                    if (employeePayroll.getBank() != null && employeePayroll.getBranch() != null) {
                        ofr.addError(userMessageServiceRemote.getUserMessage(
                                "You chose cash, so there must not exist neither a bank nor bankbranch.", loggedUser));
                    } else if (employeePayroll.getBank() != null && employeePayroll.getBranch() == null) {
                        ofr.addError(userMessageServiceRemote.getUserMessage(
                                "You chose cash, so there must not exist a bank.", loggedUser));
                    } else if (employeePayroll.getBank() == null && employeePayroll.getBranch() != null) {
                        ofr.addError(userMessageServiceRemote.getUserMessage(
                                "You chose cash, so there must not exist a bankbranch.", loggedUser));
                    } else {
//                    ofr.addSuccess(userMessageServiceRemote.getUserMessage("Succesfully saved.", loggedUser));
                    }
                }

                if (cashOrBank.equals('B') || cashOrBank.equals('b')) {
                    if (employeePayroll.getBank() == null && employeePayroll.getBranch() == null) {
                        ofr.addError(userMessageServiceRemote.getUserMessage(
                                "Bank&BankBranchRequired", loggedUser));
                    } else if (employeePayroll.getBank() != null && employeePayroll.getBranch() == null) {
                        ofr.addError(userMessageServiceRemote.getUserMessage(
                                "BankBranchRequired", loggedUser));
                    } else if (employeePayroll.getBank() == null && employeePayroll.getBranch() != null) {
                        ofr.addError(userMessageServiceRemote.getUserMessage(
                                "BankRequired", loggedUser));
                    } else {
//                    ofr.addSuccess(userMessageServiceRemote.getUserMessage("Succesfully saved.", loggedUser));
                    }
                }
            }
        } catch (Exception e) {
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
        } finally {
        }

        return ofr;
    }

    @Override
    public OFunctionResult payrollChangeAction(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();

        CalculatedPeriod calcPeriod = (CalculatedPeriod) odm.getData().get(0);
        String fldExp = odm.getData().get(1).toString();
        Object newVal = odm.getData().get(2);

        int month;
        int year;

        month = fldExp.startsWith("month") ? Integer.parseInt(newVal.toString()) : calcPeriod.getMonth();
        year = fldExp.startsWith("year") ? Integer.parseInt(newVal.toString()) : calcPeriod.getYear();

        if (month >= 1 && month <= 12) {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            String endDay;

            String startDateStr = year + "-" + month + "-01";
            String endDateStr;

            Date startDate = null;
            Date endDate = null;

            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                endDay = "31";
            } else if (month == 2) {
                if (year % 4 == 0) {
                    endDay = "29";
                } else {
                    endDay = "28";
                }
            } else {
                endDay = "30";
            }

            endDateStr = year + "-" + month + "-" + endDay;

            try {
                startDate = format.parse(startDateStr);
                endDate = format.parse(endDateStr);
            } catch (ParseException ex) {
                OLog.logException(ex, loggedUser);
            }
            calcPeriod.setStartDate(startDate);
            calcPeriod.setEndDate(endDate);
        }

        ofr.setReturnedDataMessage(odm);
        return ofr;
    }

    @Override
    public OFunctionResult salaryElementInActive(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            SalaryElement salaryElement = (SalaryElement) odm.getData().get(0);
            if (salaryElement == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            if (salaryElement.isInActive()) {
                salaryElement.setActive(false);
                oem.saveEntity(salaryElement, loggedUser);
            } else if (!salaryElement.isInActive()) {
                salaryElement.setActive(true);
                oem.saveEntity(salaryElement, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logError("Error in Getting Salary Element", loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult payrollCalculationValidation(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            PayrollCalculation payrollCalculation = (PayrollCalculation) odm.getData().get(1);
            if (payrollCalculation == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            long calculatedPeriodDBID = payrollCalculation.getCalculatedPeriod().getDbid();
            List<String> conditions = new ArrayList<String>();
            conditions.add("calculatedPeriod.dbid =" + calculatedPeriodDBID);
            List<PayrollCalculation> payrollCalculations = oem.loadEntityList(PayrollCalculation.class.getSimpleName(), conditions, null, null, loggedUser);
            if (payrollCalculations.size() > 0) {
                oFR.addError(userMessageServiceRemote.getUserMessage("PayrollCalcValidation", loggedUser));
                return oFR;
            }
        } catch (Exception ex) {
            OLog.logError("Error in Getting Payroll Calculation", loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult salaryElementDescDD(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            SalaryElement salaryElement = (SalaryElement) odm.getData().get(0);
            if (salaryElement == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            String mappedColumn = salaryElement.getMappedColumn();
            String expression;
            mappedColumn = mappedColumn.substring(4);
            //Update Label & Header Of DD Of EmployeeSalary Entity
            if (salaryElement instanceof Income) {
                expression = "EmployeeSalary_salI" + mappedColumn;
            } else {
                expression = "EmployeeSalary_salD" + mappedColumn;
            }

            List<String> conditions = new ArrayList<String>();
            conditions.add("name  ='" + expression + "'");
            List<DD> dds = oem.loadEntityList(DD.class.getSimpleName(), conditions, null, null, loggedUser);
            for (int i = 0; i < dds.size(); i++) {
                dds.get(i).setLabelTranslated(salaryElement.getDescriptionTranslated());
                dds.get(i).setHeaderTranslated(salaryElement.getDescriptionTranslated());
                entitySetupService.callEntityUpdateAction(dds.get(i), loggedUser);
            }
        } catch (Exception ex) {
            OLog.logError("Error in Getting Salary Element", loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult mobileBillDeduction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeBill bill = (EmployeeBill) odm.getData().get(0);

            EmployeeBenifit benifit = (EmployeeBenifit) oem.loadEntity(EmployeeBenifit.class.getSimpleName(),
                    Collections.singletonList("employee.dbid = " + bill.getEmployee().getDbid()), null, loggedUser);

            if (benifit == null) {
                return oFR;
            }

            if (benifit.getMobileLimit() != null && bill.getMobileAmount() != null) {
                BigDecimal mobileDiff = bill.getMobileAmount().subtract(benifit.getMobileLimit());

                BaseEntity saElement = oem.loadEntity(Deduction.class.getSimpleName(),
                        Collections.singletonList("internalCode = 'EXTRA'"), null, loggedUser);

                if (saElement != null) {

                    List<String> conds = new ArrayList<String>();
                    conds.add("employee.dbid = " + bill.getEmployee().getDbid());
                    conds.add("salaryElement.dbid = " + saElement.getDbid());

                    EmployeeSalaryElement element = (EmployeeSalaryElement) oem.loadEntity(EmployeeSalaryElement.class.getSimpleName(), conds, null, loggedUser);
                    if (element != null) {
                        if (mobileDiff.compareTo(BigDecimal.ZERO) > 0) {
                            element.setValue(mobileDiff);
                        } else {
                            element.setValue(BigDecimal.ZERO);
                        }
                        oFR.append(entitySetupService.callEntityUpdateAction(element, loggedUser));
                    }
                }
            }

            if (benifit.getMobile1Limit() != null && bill.getMobile1Amount() != null) {
                BigDecimal mobile1Diff = bill.getMobile1Amount().subtract(benifit.getMobile1Limit());
                BaseEntity saElement = oem.loadEntity(Deduction.class.getSimpleName(),
                        Collections.singletonList("internalCode = 'EXTRA1'"), null, loggedUser);

                if (saElement != null) {

                    List<String> conds = new ArrayList<String>();
                    conds.add("employee.dbid = " + bill.getEmployee().getDbid());
                    conds.add("salaryElement.dbid = " + saElement.getDbid());

                    EmployeeSalaryElement element = (EmployeeSalaryElement) oem.loadEntity(EmployeeSalaryElement.class.getSimpleName(), conds, null, loggedUser);

                    if (element != null) {
                        if (mobile1Diff.compareTo(BigDecimal.ZERO) > 0) {
                            element.setValue(mobile1Diff);

                        } else {
                            element.setValue(BigDecimal.ZERO);
                        }
                        oFR.append(entitySetupService.callEntityUpdateAction(element, loggedUser));
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult employeeBillChangeAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeBill bill = (EmployeeBill) odm.getData().get(0);
            String fldExp = odm.getData().get(1).toString();
            Object newVal = odm.getData().get(2);

            BigDecimal mobile;
            BigDecimal mobile1;

            EmployeeBenifit benifit = (EmployeeBenifit) oem.loadEntity(EmployeeBenifit.class.getSimpleName(),
                    Collections.singletonList("employee.dbid = " + bill.getEmployee().getDbid()), null, loggedUser);

            if (benifit == null) {
                return oFR;
            }

            if (fldExp.startsWith("mobileAmount") || fldExp.startsWith("mobileAmountMask")) {
                mobile = (newVal == null) ? new BigDecimal(0) : new BigDecimal(newVal.toString());

                bill.setMobileBenifit(benifit.getMobileLimit());

                BigDecimal mobileDiff = mobile.subtract(benifit.getMobileLimit());

                if (mobileDiff.compareTo(BigDecimal.ZERO) > 0) {
                    bill.setMobileDifference(mobileDiff);
                    bill.setMobileDifferenceMask(mobileDiff);
                } else {
                    bill.setMobileDifference(BigDecimal.ZERO);
                    bill.setMobileDifferenceMask(BigDecimal.ZERO);
                }
            } else if (fldExp.startsWith("mobile1Amount") || fldExp.startsWith("mobile1AmountMask")) {
                mobile1 = (newVal == null) ? new BigDecimal(0) : new BigDecimal(newVal.toString());

                bill.setMobile1Benifit(benifit.getMobile1Limit());

                BigDecimal mobile1Diff = mobile1.subtract(benifit.getMobile1Limit());

                if (mobile1Diff.compareTo(BigDecimal.ZERO) > 0) {
                    bill.setMobile1Difference(mobile1Diff);
                    bill.setMobile1DifferenceMask(mobile1Diff);
                } else {
                    bill.setMobile1Difference(BigDecimal.ZERO);
                    bill.setMobile1DifferenceMask(BigDecimal.ZERO);
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }

        oFR.setReturnedDataMessage(odm);

        return oFR;
    }

    @Override
    public OFunctionResult createGradeSalaryElementsForPayGrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            PayGrade payGrade = (PayGrade) odm.getData().get(0);
            Long companyDBID = loggedUser.getUserPrefrence1();
            List conds = new ArrayList();
            if (companyDBID == null || companyDBID == 0L) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("UserHasNoCompany", loggedUser));
            } else {
                conds.add("company.dbid = " + companyDBID);
            }
            conds.add("fixedVariable = 'Y'");
            List<SalaryElement> fixedSE = oem.loadEntityList("SalaryElement",
                    conds, null, null, loggedUser);
            for (SalaryElement fixedElement : fixedSE) {
                GradeSalaryElement gradeSalaryElement = new GradeSalaryElement();
                gradeSalaryElement.setSalaryElement(fixedElement);
                gradeSalaryElement.setPayGrade(payGrade);
                oFR.append(entitySetupService.callEntityCreateAction(gradeSalaryElement, loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult createGradeSalaryElementsForSalaryElement(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<String> conds = new ArrayList<String>();
            conds.add("description = 'salaryElementsGrade'");
            PayrollParameter payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            if (payrollParameter != null && payrollParameter.getValueData() != null
                    && payrollParameter.getValueData().equalsIgnoreCase("y")) {
                SalaryElement salaryElement = (SalaryElement) odm.getData().get(0);
//            if ("N".equals(salaryElement.getFixedVariable())) {
//                return oFR;
//            }
                List<PayGrade> payGrades = oem.loadEntityList("PayGrade",
                        null, null, null, loggedUser);
                for (PayGrade payGrade : payGrades) {
                    GradeSalaryElement gradeSalaryElement = new GradeSalaryElement();
                    gradeSalaryElement.setSalaryElement(salaryElement);
                    gradeSalaryElement.setPayGrade(payGrade);
                    oFR.append(entitySetupService.callEntityCreateAction(gradeSalaryElement, loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult validateFixedSalaryElementMinMaxVal(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        try {
            EmployeeSalaryElement empSalaryElement = (EmployeeSalaryElement) odm.getData().get(0);
            SalaryElement salaryElement = empSalaryElement.getSalaryElement();
            if ("N".equals(salaryElement.getFixedVariable())) {
                return oFR;
            }
            EmployeePayroll payroll = empSalaryElement.getEmployee().getPayroll();
            if (payroll == null) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NoPayrollFound", loggedUser));
                return oFR;
            }
            PayGrade payGrade = payroll.getPayGrade();
            if (payGrade == null) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NoPayGradeFound", loggedUser));
                return oFR;
            }
            List conds = new ArrayList();
            conds.add("salaryElement.dbid = " + salaryElement.getDbid());
            conds.add("payGrade.dbid = " + payGrade.getDbid());
            List<GradeSalaryElement> gradeSalaryElements = oem.loadEntityList("GradeSalaryElement",
                    conds, null, null, loggedUser);
            if (gradeSalaryElements == null || gradeSalaryElements.isEmpty()) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NoGradeSEsFound", loggedUser));
                return oFR;
            }
            BigDecimal minVal = new BigDecimal(0), maxVal = new BigDecimal(0);
            minVal = gradeSalaryElements.get(0).getMinSalValue();
            maxVal = gradeSalaryElements.get(0).getMaxSalValue();
            if (minVal == null || minVal.compareTo(BigDecimal.ZERO) == 0) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NoMinValue", loggedUser));
                return oFR;
            }
            if (maxVal == null || maxVal.compareTo(BigDecimal.ZERO) == 0) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NoMaxValue", loggedUser));
                return oFR;
            }
            if (empSalaryElement.getValue().compareTo(minVal) == -1
                    || empSalaryElement.getValue().compareTo(maxVal) == 1) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("InvalidEmployeeSalaryElementValue", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult updateEmployeeSalaryElementsWithGradeMid(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {

            PayGrade payGrade = null;
            Employee employee = null;
            if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeePayroll")) {
                EmployeePayroll employeePayroll = (EmployeePayroll) odm.getData().get(0);
                payGrade = employeePayroll.getPayGrade();
                employee = employeePayroll.getEmployee();
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeProfileHistory")) {
                EmployeeProfileHistory profileHistory = (EmployeeProfileHistory) odm.getData().get(0);
                payGrade = profileHistory.getPayGrade();
                employee = profileHistory.getEmployee();
            }
            if (payGrade != null) {
                List<GradeSalaryElement> gradeSalaryElements = oem.loadEntityList("GradeSalaryElement",
                        Collections.singletonList("payGrade.dbid = " + payGrade.getDbid()),
                        null, null, loggedUser);
                List conds = new ArrayList();
                conds.add("employee.dbid = " + employee.getDbid());
                conds.add("salaryElement.fixedVariable = 'Y'");
                List<EmployeeSalaryElement> empSalaryElements = oem.loadEntityList("EmployeeSalaryElement",
                        conds, null, null, loggedUser);

                for (EmployeeSalaryElement employeeSalaryElement : empSalaryElements) {
                    for (GradeSalaryElement grade : gradeSalaryElements) {
                        // Update SalaryElement in GradeSalaryElements with same
                        // SalaryElement in EmployeeSalaryElement
                        if (grade.getSalaryElement().getDbid()
                                == employeeSalaryElement.getSalaryElement().getDbid()) {
                            if (grade.getMidSalValue() != null) {
                                // If mid value is ZERO or NULL, don't update the SalaryElement Value
                                if (grade.getMidSalValue().compareTo(BigDecimal.ZERO) != 0) {
                                    employeeSalaryElement.setValue(grade.getMidSalValue());
                                    oFR.append(entitySetupService.callEntityUpdateAction(
                                            employeeSalaryElement, loggedUser));
                                }
                            }
                            break;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult getESEOldValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeSalaryUpgrade salaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
            String fldExp = odm.getData().get(1).toString();
            Object newVal = odm.getData().get(2);
            Income income = fldExp.contains("income") && newVal instanceof Income ? (Income) newVal : salaryUpgrade.getIncome();

            Employee employee = fldExp.equalsIgnoreCase("employee") && newVal instanceof Employee ? (Employee) newVal : salaryUpgrade.getEmployee();
            if (salaryUpgrade == null || income == null || employee == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            List conditions = new ArrayList();
            conditions.add("employee.dbid = " + employee.getDbid());
            conditions.add("salaryElement.dbid = " + income.getDbid());
            EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
            if (employeeSalaryElement != null) {
                salaryUpgrade.setValueBeforeMask(employeeSalaryElement.getValue());
            }
            //SET VALUES IN ENTITY
            oFR.setReturnedDataMessage(odm);
            oFR.append(oFR);

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult runRetroCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            oFR = payrollService.runPayCalcWarapperNewTx(odm, functionParms, loggedUser, "WSF_RC0001");
            OWebService ws = (OWebService) oFR.getReturnValues().get(0);
            ODataMessage wsODM = (ODataMessage) oFR.getReturnValues().get(1);
            oFR.append(functionService.runWebServiceFunction(ws, wsODM, functionParms, loggedUser));
        } catch (Exception e) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult applySalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            List conditions = new ArrayList();
            EmployeeSalaryUpgrade salaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
            if (salaryUpgrade == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }

            if (salaryUpgrade.getCancelled().equals("Y")) {
                oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade001", loggedUser));
                return oFR;
            }
            if (salaryUpgrade.getApplied() != null) {
                if (salaryUpgrade.getApplied().equals("Y")) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade002", loggedUser));
                    return oFR;
                }
            }
            applySalaryUpgrade(salaryUpgrade, loggedUser);
            salaryUpgrade.setApplied("Y");
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("salaryUpgrade003", loggedUser));
            oem.saveEntity(salaryUpgrade, loggedUser);

            //___________________Apply Net To Gross Business________________________________
            String internalCode;
            internalCode = salaryUpgrade.getIncome().getInternalCode();
            if (internalCode.toLowerCase().contains("gross")
                    || internalCode.toLowerCase().contains("net")
                    || internalCode.toLowerCase().contains("july")) {
                conditions.clear();
                conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
                EmployeeSalaryElement ESE = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                if (ESE != null) {
                    entitySetupService.callEntityUpdateAction(ESE, loggedUser);
                }
            }
            //___________________Apply Net To Gross Business________________________________
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult importSalaryUpgradeValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String datePattern1 = "((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";

        DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
        String datePattern2 = "(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((19|20)\\d\\d)";

        ImportEmployeeSalaryUpgrade employeeSU = (ImportEmployeeSalaryUpgrade) odm.getData().get(0);

        if (employeeSU.getIncome() == null || employeeSU.getIncome().equals("")) {
            employeeSU.setIncome("NA");
        }

        if (employeeSU.getUpgradeReason() == null || employeeSU.getUpgradeReason().equals("")) {
            employeeSU.setUpgradeReason("NA");
        }

        if (employeeSU.getUpgradeReason() == null || employeeSU.getUpgradeReason().equals("")) {
            employeeSU.setUpgradeReason("NA");
        }

        if (employeeSU.getGivenBy() == null || employeeSU.getGivenBy().equals("")) {
            employeeSU.setGivenBy("NA");
        }

        if (employeeSU.getApplyDate() != null && !employeeSU.getApplyDate().equals("")) {
            String s = employeeSU.getApplyDate().replace("/", "-");
            if (s.matches(datePattern1)) {
                try {
                    s = dateFormat1.format(dateFormat1.parse(s));
                } catch (ParseException ex) {
                }
            } else if (s.matches(datePattern2)) {
                try {
                    s = dateFormat1.format(dateFormat2.parse(s));
                } catch (ParseException ex) {
                }
            } else {
                ofr.addError(userMessageServiceRemote.getUserMessage("ApplyDateIsManadatory", loggedUser));
            }
            employeeSU.setApplyDate(s);
        } else {
            ofr.addError(userMessageServiceRemote.getUserMessage("ApplyDateIsManadatory", loggedUser));
        }

        ofr.setReturnedDataMessage(odm);
        return ofr;
    }

    @Override
    public OFunctionResult importEmpSalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {

            ArrayList importEmpSalaryUpgradCond = new ArrayList();
            importEmpSalaryUpgradCond.add("done = false");

            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            if (!employeeDbidList.isEmpty()) {
                String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                String securityCond = "employee in ( select e.code from employee e where e.dbid in " + employeeDbidStr + ")";
                importEmpSalaryUpgradCond.add(securityCond);
            }
            List<ImportEmployeeSalaryUpgrade> impEmpSalUpgradeList = oem.loadEntityList(ImportEmployeeSalaryUpgrade.class.getSimpleName(),
                    importEmpSalaryUpgradCond, null, null, loggedUser);

            OFunctionResult tempOFR;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            for (int i = 0; i < impEmpSalUpgradeList.size(); i++) {
                ImportEmployeeSalaryUpgrade impEmpSalUpgrade = impEmpSalUpgradeList.get(i);

                oFR.append(fillSalaryUpgradeValues(ImportEmployeeSalaryUpgrade.class.getSimpleName(), impEmpSalUpgrade, loggedUser, null));
                //edited by Osama Samy 16-05-2017 after append we need to get the last object of the returned objects not the first.
                //   impEmpSalUpgrade = (ImportEmployeeSalaryUpgrade) oFR.getReturnValues().get(0);
                int lastElement = oFR.getReturnValues().isEmpty() ? 0 : oFR.getReturnValues().size() - 1;
                impEmpSalUpgrade = (ImportEmployeeSalaryUpgrade) oFR.getReturnValues().get(lastElement);
                oem.saveEntity(impEmpSalUpgrade, loggedUser);

                tempOFR = calculateValues(impEmpSalUpgrade, loggedUser);
                impEmpSalUpgrade = (ImportEmployeeSalaryUpgrade) tempOFR.getReturnValues().get(0);
                oFR.append(tempOFR);

                Employee employee = (Employee) oem.loadEntity(Employee.class.getSimpleName(),
                        Collections.singletonList("code = '" + impEmpSalUpgrade.getEmployee() + "'"), null, loggedUser);
                Income income = (Income) oem.loadEntity(Income.class.getSimpleName(),
                        Collections.singletonList("code = '" + impEmpSalUpgrade.getIncome() + "'"), null, loggedUser);
                if (employee == null || income == null) {
                    continue;
                }

                if (income.getFixedVariable().equals("N")) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("VarSENotAllowed", loggedUser));
                    continue;
                }

                Date applyDate = dateFormat.parse(impEmpSalUpgrade.getApplyDate());

                // get calculated period
                Integer cutOffStart = null;
                int calcPeriodMonth, calcPeriodYear, calcPeriodDay;

                SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
                SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
                SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

                calcPeriodDay = Integer.parseInt(dayFormat.format(applyDate));
                calcPeriodMonth = Integer.parseInt(monthFormat.format(applyDate));
                calcPeriodYear = Integer.parseInt(yearFormat.format(applyDate));

                List<String> cutOffConditions = new ArrayList<String>();
                cutOffConditions.add("company.dbid = " + employee.getPositionSimpleMode().getUnit().getCompany().getDbid());
                cutOffConditions.add("description = 'day_of_calculation'");

                PayrollParameter cutOffParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);

                cutOffStart = Integer.parseInt(cutOffParameter.getValueData()) + 1;
                if (calcPeriodDay >= cutOffStart) {
                    calcPeriodMonth = (calcPeriodMonth + 1) % 12;
                    if (calcPeriodMonth == 1) { // the original applyDate was in December(12)
                        calcPeriodYear++;
                    }
                }

                List<String> payrollParameterconditions = new ArrayList<String>();
                payrollParameterconditions.add("description = 'default_pay_method'");
                payrollParameterconditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
                PayrollParameter parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), payrollParameterconditions, null, loggedUser);

                List<String> consolidationConditions = new ArrayList<String>();
                consolidationConditions.add("month = " + calcPeriodMonth);
                consolidationConditions.add("year = " + calcPeriodYear);
                consolidationConditions.add("paymentMethod.dbid=" + parameter.getValueData());
                CalculatedPeriod calculatedPeriod = null;
                calculatedPeriod = (CalculatedPeriod) oem.loadEntity(CalculatedPeriod.class.getSimpleName(), consolidationConditions, null, loggedUser);

                boolean futureCalcPeriod = false;
                String selectFirstCalcPeriod = "select MIN(ENDDATE) from calculatedperiod "
                        + "where CLOSED = 'N' and paymentMethod_dbid = " + parameter.getValueData();
                Date selectRes = (Date) oem.executeEntityNativeQuery(selectFirstCalcPeriod, loggedUser);
                Calendar cal = Calendar.getInstance();
                cal.setTime(selectRes);
                int minYear = cal.get(Calendar.YEAR);
                int minMonth = cal.get(Calendar.MONTH) + 1;
                if (minYear <= calcPeriodYear) {
                    //futureCalcPeriod = true;
                    if (minMonth < calcPeriodMonth) {
                        futureCalcPeriod = true;
                    }
                }

                if (income != null && employee != null && calculatedPeriod != null) {

                    BigDecimal upgradeFactor = null,
                            valueAfter = null,
                            valueBefore = null;
                    if (impEmpSalUpgrade.getUpgradeFactor() != null) {
                        upgradeFactor = new BigDecimal(impEmpSalUpgrade.getUpgradeFactor());
                    }
                    if (impEmpSalUpgrade.getValueAfter() != null) {
                        valueAfter = new BigDecimal(impEmpSalUpgrade.getValueAfter());
                    }
                    if (impEmpSalUpgrade.getValueBefore() != null) {
                        valueBefore = new BigDecimal(impEmpSalUpgrade.getValueBefore());
                    }

                    // get upgrade reason : udc
                    List<String> conds = new ArrayList<String>();
                    conds.add("code = '" + impEmpSalUpgrade.getUpgradeReason() + "'");
                    conds.add("type.code = 'Pay_SUpR'");
                    UDC upgradeReason = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    if (upgradeReason == null) {
                        conds.clear();
                        conds.add("code = 'Pay_SUpR'");
                        UDCType type = (UDCType) oem.loadEntity(UDCType.class.getSimpleName(), conds, null, loggedUser);

                        upgradeReason = new UDC();
                        upgradeReason.setType(type);
                        upgradeReason.setCode(impEmpSalUpgrade.getUpgradeReason());
                        upgradeReason.setValue(impEmpSalUpgrade.getUpgradeReason());
                        upgradeReason.setValueTranslated(impEmpSalUpgrade.getUpgradeReason());
                        oem.saveEntity(upgradeReason, loggedUser);
                    }

                    EmployeeSalaryUpgrade empSalUpgrade = new EmployeeSalaryUpgrade();
                    empSalUpgrade.setEmployee(employee);
                    empSalUpgrade.setIncome(income);
                    empSalUpgrade.setFromCalculatedPeriod(calculatedPeriod);
                    empSalUpgrade.setApplyDate(applyDate);
                    empSalUpgrade.setValueAfter(valueAfter);
                    empSalUpgrade.setValueBefore(valueBefore);
                    empSalUpgrade.setUpgradeFactor(upgradeFactor);
                    empSalUpgrade.setGivenBy((Employee) oem.loadEntity(Employee.class.getSimpleName(),
                            Collections.singletonList("code = '" + impEmpSalUpgrade.getGivenBy() + "'"), null, loggedUser));
                    empSalUpgrade.setUpgradeReason(upgradeReason);
                    oem.saveEntity(empSalUpgrade, loggedUser);

                    if (futureCalcPeriod) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("FutureCalcPeriodEntered", loggedUser));
                        continue;
                    }

                    List<String> condition = new ArrayList<String>();
                    condition.add("name='" + EmployeeSalaryUpgrade.class.getSimpleName() + "'");
                    ODataType dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(), condition, null, loggedUser);
                    List salaryUpradeData = new ArrayList();
                    salaryUpradeData.add(empSalUpgrade);
                    ODataMessage salaryUpDataMessage = new ODataMessage(dataType, salaryUpradeData);
                    oFR.append(payrollService.applySalaryUpgrade(salaryUpDataMessage, functionParms, loggedUser));

                    impEmpSalUpgrade.setDone(true);
                    oem.saveEntity(impEmpSalUpgrade, loggedUser);
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    // [Loubna] -  - Calculating upFactor if the valAfter is given & vice versa, it works as both change action & validation - 21-08-2013
    @Override
    public OFunctionResult salaryUpgradePercentChangeAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            BigDecimal valueB4 = null,
                    upgradeFactor = null,
                    valueAfter = null,
                    difference = null;
            Date applyDate = null;

            if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeSalaryUpgrade")) {
                EmployeeSalaryUpgrade empSalaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);

                List<String> conds = new ArrayList<String>();
                conds.add("salaryElement.dbid = " + empSalaryUpgrade.getIncome().getDbid());
                conds.add("employee.dbid = " + empSalaryUpgrade.getEmployee().getDbid());

                EmployeeSalaryElement empSalaryElement = (EmployeeSalaryElement) oem.loadEntity(
                        EmployeeSalaryElement.class.getSimpleName(), conds, null, loggedUser);

                valueB4 = empSalaryElement.getValue();
                if (empSalaryUpgrade.getApplyDate() == null) {
                    applyDate = empSalaryUpgrade.getFromCalculatedPeriod().getStartDate();
                } else {
                    applyDate = empSalaryUpgrade.getApplyDate();
                }

                if (odm.getODataType().getName().equals("EntityChangeField")) {
                    String fldExp = odm.getData().get(1).toString();
                    Object newVal = odm.getData().get(2);
                    upgradeFactor = fldExp.startsWith("upgradeFactor") ? new BigDecimal(newVal.toString()) : null;
                    valueAfter = fldExp.startsWith("valueAfter") ? new BigDecimal(newVal.toString()) : null;
                    difference = fldExp.startsWith("difference") ? new BigDecimal(newVal.toString()) : null;
                } else {
                    upgradeFactor = empSalaryUpgrade.getUpgradeFactor();
                    valueAfter = empSalaryUpgrade.getValueAfter();
                    difference = empSalaryUpgrade.getDifference();
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("ImportEmployeeSalaryUpgrade")) {
                ImportEmployeeSalaryUpgrade impEmpSalaryUpgrade = (ImportEmployeeSalaryUpgrade) odm.getData().get(0);

                List<String> conds = new ArrayList<String>();
                conds.add("employee.code = '" + impEmpSalaryUpgrade.getEmployee() + "'");
                conds.add("salaryElement.code = '" + impEmpSalaryUpgrade.getIncome() + "'");
                EmployeeSalaryElement empSE = (EmployeeSalaryElement) oem.loadEntity(EmployeeSalaryElement.class.getSimpleName(), conds, null, loggedUser);
                if (empSE == null) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("NoEmpSalElementExists", loggedUser));
                    return oFR;
                }

                if (odm.getODataType().getName().equals("EntityChangeField")) {
                    String fldExp = odm.getData().get(1).toString();
                    Object newVal = odm.getData().get(2);
                    upgradeFactor = fldExp.startsWith("upgradeFactor") ? new BigDecimal(newVal.toString()) : null;
                    valueAfter = fldExp.startsWith("valueAfter") ? new BigDecimal(newVal.toString()) : null;
                    difference = fldExp.startsWith("difference") ? new BigDecimal(newVal.toString()) : null;
                } else {
                    upgradeFactor = impEmpSalaryUpgrade.getUpgradeFactor() != null ? new BigDecimal(impEmpSalaryUpgrade.getUpgradeFactor()) : null;
                    valueAfter = impEmpSalaryUpgrade.getValueAfter() != null ? new BigDecimal(impEmpSalaryUpgrade.getValueAfter()) : null;
                    difference = impEmpSalaryUpgrade.getDifference() != null ? new BigDecimal(impEmpSalaryUpgrade.getDifference()) : null;
                }

                valueB4 = empSE.getValue();
                impEmpSalaryUpgrade.setValueBefore(valueB4.toString());
            }

            // calculation
            if (valueB4 != null && valueB4.compareTo(BigDecimal.ZERO) == 0) {
                valueB4 = new BigDecimal(BigInteger.ZERO);
            }
            if (upgradeFactor != null && valueB4 != null && !valueB4.equals(BigDecimal.ZERO)) {
                if (valueAfter != null || difference != null) {
                    oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyFactorEntered", loggedUser));
                }
                BigDecimal affPercent = valueB4.multiply(upgradeFactor).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP);
                valueAfter = valueB4.add(affPercent);
                difference = valueAfter.subtract(valueB4);
            } else if (valueAfter != null && valueB4 != null) {
                if (upgradeFactor != null || difference != null) {
                    oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyValAfterEntered", loggedUser));
                }
                difference = valueAfter.subtract(valueB4);
                if (!valueB4.equals(BigDecimal.ZERO)) {
                    upgradeFactor = difference.multiply(new BigDecimal(100)).divide(valueB4, BigDecimal.ROUND_HALF_UP);
                }
            } else if (difference != null && difference != null) {
                if (valueAfter != null || upgradeFactor != null) {
                    oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyDiffEntered", loggedUser));
                }
                valueAfter = valueB4.add(difference);
                if (!valueB4.equals(BigDecimal.ZERO)) {
                    upgradeFactor = difference.multiply(new BigDecimal(100)).divide(valueB4, BigDecimal.ROUND_HALF_UP);
                }
            }

            // setting values in the entities
            if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeSalaryUpgrade")) {
                EmployeeSalaryUpgrade empSalaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
                empSalaryUpgrade.setUpgradeFactor(upgradeFactor);
                empSalaryUpgrade.setValueAfter(valueAfter);
                empSalaryUpgrade.setDifference(difference);
                empSalaryUpgrade.setValueBefore(valueB4);
                empSalaryUpgrade.setApplyDate(applyDate);
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("ImportEmployeeSalaryUpgrade")) {
                ImportEmployeeSalaryUpgrade impEmpSalaryUpgrade = (ImportEmployeeSalaryUpgrade) odm.getData().get(0);
                if (difference != null) {
                    impEmpSalaryUpgrade.setDifference(difference.toString());
                }
                if (upgradeFactor != null) {
                    impEmpSalaryUpgrade.setUpgradeFactor(upgradeFactor.toString());
                }
                if (valueAfter != null) {
                    impEmpSalaryUpgrade.setValueAfter(valueAfter.toString());
                }
            }

            oFR.setReturnedDataMessage(odm);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult salaryUpgradePercent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        BigDecimal affPercent;
        try {
            EmployeeSalaryUpgrade salaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
            if (!validateSalaryUpgradeIsCancelledOrIsApplied(salaryUpgrade, oFR, loggedUser)) {
                return oFR;
            }
            if (salaryUpgrade.getValueBefore() != null) {
                if ((salaryUpgrade.getUpgradeFactor() != null) && !(salaryUpgrade.getUpgradeFactor().equals(new BigDecimal(0)))) {
                    if ((salaryUpgrade.getValueAfter() == null) || (salaryUpgrade.getValueAfter().equals(new BigDecimal(0)))) {
                        affPercent = salaryUpgrade.getValueBefore().multiply(salaryUpgrade.getUpgradeFactor()).divide(new BigDecimal(100));
                        salaryUpgrade.setValueAfter(salaryUpgrade.getValueBefore().add(affPercent));
                    }
                }
            } else {
                if ((salaryUpgrade.getUpgradeFactor() != null) && !(salaryUpgrade.getUpgradeFactor().equals(new BigDecimal(0)))) {
                    salaryUpgrade.setValueAfter(new BigDecimal(0));
                }
                if ((salaryUpgrade.getValueAfter() == null) || (salaryUpgrade.getValueAfter().equals(new BigDecimal(0)))) {
                    salaryUpgrade.setValueAfter(salaryUpgrade.getValueAfter());
                }
            }
            oFR.setReturnedDataMessage(odm);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    //[Loubna] - Ticket:  - 16/07/13 - Modify Salary upgrade function was missing
    @Override
    public OFunctionResult modifySalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeSalaryUpgrade salaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
            oFR.append(entitySetupService.callEntityUpdateAction(salaryUpgrade, loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult cancelSalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeSalaryUpgrade salaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
            if (salaryUpgrade == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            if (salaryUpgrade.getCancelled().equals("Y")) {
                oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade004", loggedUser));
                return oFR;
            }
            if (salaryUpgrade.getApplied() != null && salaryUpgrade.getApplied().equals("Y")) {
                List conditions = new ArrayList();
                conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
                List<EmployeeSalaryElement> ese = oem.loadEntityList("EmployeeSalaryElement", conditions, null, null, loggedUser);
                if (ese.size() > 0) {
                    ese.get(0).setValue(salaryUpgrade.getValueBefore());
                    oem.saveEntity(ese.get(0), loggedUser);
                }
            }
            salaryUpgrade.setCancelled("Y");
            oem.saveEntity(salaryUpgrade, loggedUser);
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("salaryUpgrade005", loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult updateGradeSalaryElementsForSalaryElement(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            SalaryElement salaryElement = (SalaryElement) odm.getData().get(0);
            SalaryElement salaryElementDB = (SalaryElement) oem.loadEntity("SalaryElement",
                    salaryElement.getDbid(), null, null, loggedUser);
            // Loubna - 08/09/2013 - in case of activating an inactive SE, salaryElementDB is retreived as NULL, so it gave a NullPointerException
            if (salaryElementDB == null) {
                salaryElementDB = salaryElement;
            }

            String wasFixed = salaryElementDB.getFixedVariable();
            String nowFixed = salaryElement.getFixedVariable();
            // If it was not fixed and updated to be fixed
            // Create new GradeSalaryElements
            if ("N".equals(wasFixed) && "Y".equals(nowFixed)) {
                List<PayGrade> payGrades = oem.loadEntityList("PayGrade",
                        Collections.singletonList("company.dbid=" + salaryElement.getCompany().getDbid()), null, null, loggedUser);
                for (PayGrade payGrade : payGrades) {
                    GradeSalaryElement gradeSalaryElement = new GradeSalaryElement();
                    gradeSalaryElement.setActive(true);
                    gradeSalaryElement.setSalaryElement(salaryElement);
                    gradeSalaryElement.setPayGrade(payGrade);
                    oFR.append(entitySetupService.callEntityCreateAction(gradeSalaryElement, loggedUser));
                }
            }

            if ("Y".equals(wasFixed) && "N".equals(nowFixed)) {
                List<BaseEntity> gradeSalaryElements = oem.loadEntityList("GradeSalaryElement",
                        Arrays.asList("salaryElement.dbid = " + String.valueOf(salaryElement.getDbid()),
                                "salaryElement.company.dbid=" + salaryElement.getCompany().getDbid()),
                        null, null, loggedUser);
                oem.deleteEntityList(gradeSalaryElements, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult postDeleteSEFormula(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (odm.getData().get(0).getClass().getSimpleName().equals("SalaryElementFormula")) {
                SalaryElementFormula formula = (SalaryElementFormula) odm.getData().get(0);
                if (formula != null) {
                    List<String> conds = new ArrayList<String>();
                    conds.add("salaryElement.dbid = " + formula.getSalaryElement().getDbid());
                    List<EmployeeSalaryElement> empSEs = oem.loadEntityList(
                            EmployeeSalaryElement.class.getSimpleName(), conds, null, null, loggedUser);
                    if (empSEs != null) {
                        for (int i = 0; i < empSEs.size(); i++) {
                            empSEs.get(i).setValue(BigDecimal.ZERO);
                            oem.saveEntity(empSEs.get(i), loggedUser);
                        }
                    }
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeDependance")) {
                EmployeeDependance empDependance = (EmployeeDependance) odm.getData().get(0);
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
            return oFR;

        } finally {
            return oFR;
        }
    }

    /*
     * If the input is SEFormula ->
     * If the input is Employee Entry ->
     * If the input is EmpDep ->
     * If the input is EmpProfileHistory ->
     */
    @Override
    public OFunctionResult postSalaryElementFormula(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            if (odm.getData().get(0).getClass().getSimpleName().equals("SalaryElementFormula")) {
                SalaryElementFormula formula = (SalaryElementFormula) odm.getData().get(0);

                if (formula != null) {
                    if (formula.getType() != null && formula.getType().getCode().equals("equation")
                            && formula.getAttribute() != null && formula.getAttribute().getCode().equals("depNum")) {
                        BigDecimal value = formula.getValue();
                        SalaryElement salaryElement = formula.getSalaryElement();

                        if (value == null) {
                            OLog.logError("Error in ** postSalaryElementFormula **:"
                                    + "\nNo 'value' found in the salary element formula.", loggedUser);
                            // TODO: add user message
                            return ofr;
                        }
                        if (salaryElement == null) {
                            OLog.logError("Error in ** postSalaryElementFormula **: "
                                    + "\nNo 'salary element' found in the salary element formula.", loggedUser);
                            // TODO: add user message
                            return ofr;
                        }

                        if (value != null && salaryElement != null) {

                            List<Integer> emps = null;
                            try {
                                emps = (List<Integer>) oem.executeEntityListNativeQuery("Select dbid "
                                        + "From oemployee where formulaApplied is null or formulaApplied = 'Y'", loggedUser);
                            } catch (Exception e) {
                                OLog.logError("Error in ** postSalaryElementFormula **:"
                                        + "\nError encountered while loading the employees on which the "
                                        + " formula should be applied.", loggedUser);
                                // TODO: add user message
                                return ofr;
                            }

                            if (emps == null && emps.isEmpty()) {
                                OLog.logError("Error: No employees found.", loggedUser);
                                // TODO: add user message
                                return ofr;
                            }
                            for (int i = 0; i < emps.size(); i++) {
                                boolean appFormula = isSalaryElementFormulaApplied(emps.get(i), null, loggedUser);
                                if (!appFormula) {
                                    continue;
                                }

                                List<String> conds = new ArrayList<String>();
                                String empDbid = emps.get(i).toString();
                                conds.add("employee.dbid = " + empDbid);
                                conds.add("formulaApplied = 'Y'");

                                List<EmployeeDependance> dependances = null;
                                try {
                                    dependances = oem.loadEntityList(EmployeeDependance.class.getSimpleName(),
                                            conds, null, null, loggedUser);
                                } catch (Exception e) {
                                    OLog.logError("Error in ** postSalaryElementFormula **:"
                                            + "\nError encountered in loading the depenedences of the employee of"
                                            + " dbid : " + empDbid, loggedUser);
                                    OLog.logException(e, loggedUser);
                                    // TODO: add user message
                                    return ofr;
                                }

                                BigDecimal newValue = BigDecimal.ZERO;

                                int depNum = 1; // the employee himself is counted according to the law (Libya)

                                if (dependances != null && !dependances.isEmpty()) {
                                    depNum += dependances.size();
                                }

                                if (formula.getOperator().getCode().equals("*")) {
                                    newValue = value.multiply(new BigDecimal(depNum));
                                } else if (formula.getOperator().getCode().equals("+")) {
                                    newValue = value.add(new BigDecimal(depNum));
                                } else if (formula.getOperator().getCode().equals("-")) {
                                    newValue = value.subtract(new BigDecimal(depNum));
                                } else if (formula.getOperator().getCode().equals("/")) {
                                    newValue = value.divide(new BigDecimal(depNum));
                                }

                                if (newValue.compareTo(BigDecimal.ZERO) > 0) {
                                    List<EmployeeSalaryElement> empSEs = null;

                                    conds.clear();
                                    conds.add("salaryElement.dbid = " + salaryElement.getDbid());
                                    conds.add("employee.dbid = " + emps.get(i));
                                    empSEs = (List<EmployeeSalaryElement>) oem.loadEntityList(
                                            EmployeeSalaryElement.class.getSimpleName(), conds, null, null, loggedUser);
                                    if (empSEs != null && !empSEs.isEmpty()) {
                                        for (int j = 0; j < empSEs.size(); j++) {
                                            empSEs.get(j).setValue(newValue);
                                            try {
                                                oem.saveEntity(empSEs.get(j), loggedUser);
                                            } catch (Exception e) {
                                                OLog.logError("Error in ** postSalaryElementFormula **: "
                                                        + "\nError encountered in updating the element salary element "
                                                        + "on the employee: " + empSEs.get(j).getEmployee().getCode(), loggedUser);
                                                OLog.logException(e, loggedUser);
                                                // TODO: add user message
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        Job job = formula.getJob();
                        BigDecimal value = formula.getValue();
                        SalaryElement salaryElement = formula.getSalaryElement();

                        List<EmployeeSalaryElement> salaryElements = null;

                        List<String> seConds = new ArrayList<String>();

                        if (job != null && salaryElement != null) {
                            seConds.add("salaryElement.dbid = " + formula.getSalaryElement().getDbid());
                            seConds.add("employee.positionSimpleMode.job.dbid = " + job.getDbid());

                            salaryElements = (List<EmployeeSalaryElement>) oem.loadEntityList(
                                    EmployeeSalaryElement.class.getSimpleName(), seConds, null, null, loggedUser);
                            if (salaryElements != null && !salaryElements.isEmpty()) {
                                for (int i = 0; i < salaryElements.size(); i++) {
                                    salaryElements.get(i).setValue(value);
                                    try {
                                        oem.saveEntity(salaryElements.get(i), loggedUser);
                                    } catch (Exception e) {
                                        OLog.logError("Error in ** postSalaryElementFormula **: "
                                                + "\nError encountered in updating the element salary element "
                                                + "on the employee: " + salaryElements.get(i).getEmployee().getCode(), loggedUser);
                                        OLog.logException(e, loggedUser);
                                        // TODO: add user message
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("Employee")) {
                Employee employeeHiring = (Employee) odm.getData().get(0);

                if (employeeHiring != null) {

                    List<SalaryElementFormula> formulas = null;

                    // in case the formula is on a certain job
                    Job job = employeeHiring.getPositionSimpleMode().getJob();
                    if (job != null) {
                        List<String> conds = new ArrayList<String>();
                        conds.add("job.dbid = " + job.getDbid());
                        formulas = (List<SalaryElementFormula>) oem.loadEntityList(
                                SalaryElementFormula.class.getSimpleName(), conds, null, null, loggedUser);

                        for (int i = 0; i < formulas.size(); i++) {
                            EmployeeSalaryElement empSE = null;
                            if (formulas.get(i).getSalaryElement() != null) {
                                conds.clear();
                                conds.add("employee.dbid = " + employeeHiring.getDbid());
                                conds.add("salaryElement.dbid = " + formulas.get(i).getSalaryElement().getDbid());
                                empSE = (EmployeeSalaryElement) oem.loadEntity(
                                        EmployeeSalaryElement.class.getSimpleName(), conds, null, loggedUser);
                                if (empSE != null) {
                                    empSE.setValue(formulas.get(i).getValue());
                                    try {
                                        oem.saveEntity(empSE, loggedUser);
                                    } catch (Exception e) {
                                        OLog.logError("Error in ** postSalaryElementFormula **: "
                                                + "\nError encountered in updating the element salary element "
                                                + "on the employee: " + empSE.getEmployee().getCode(), loggedUser);
                                        OLog.logException(e, loggedUser);
                                        // TODO: add user message
                                    }
                                }
                            }
                        }
                    }

                    boolean appFormula = isSalaryElementFormulaApplied(null, employeeHiring, loggedUser);
                    if (appFormula) {

                        List<String> conds = new ArrayList<String>();
                        conds.add("employee.dbid = " + employeeHiring.getDbid());
                        conds.add("formulaApplied = 'Y'");
                        List<EmployeeDependance> dependances = oem.loadEntityList(EmployeeDependance.class.getSimpleName(),
                                conds, null, null, loggedUser);

                        int depNum = 1; // the employee himself is counted acc to the law

                        if (dependances != null && !dependances.isEmpty()) {
                            depNum += dependances.size();
                        }

                        conds.clear();
                        conds.add("type.code = 'equation'");
                        conds.add("attribute.code = 'depNum'");
                        formulas = (List<SalaryElementFormula>) oem.loadEntityList(
                                SalaryElementFormula.class.getSimpleName(), conds, null, null, loggedUser);
                        for (int i = 0; i < formulas.size(); i++) {
                            if (formulas.get(i).getSalaryElement() != null) {
                                if (formulas.get(i).getOperator() != null
                                        && formulas.get(i).getValue() != null) {

                                    SalaryElement salaryElement = formulas.get(i).getSalaryElement();

                                    BigDecimal value = formulas.get(i).getValue();
                                    BigDecimal newValue = new BigDecimal(BigInteger.ZERO);

                                    if (formulas.get(i).getOperator().getCode().equals("*")) {
                                        newValue = value.multiply(new BigDecimal(depNum));
                                    } else if (formulas.get(i).getOperator().getCode().equals("+")) {
                                        newValue = value.add(new BigDecimal(depNum));
                                    } else if (formulas.get(i).getOperator().getCode().equals("-")) {
                                        newValue = value.subtract(new BigDecimal(depNum));
                                    } else if (formulas.get(i).getOperator().getCode().equals("/")) {
                                        newValue = value.divide(new BigDecimal(depNum));
                                    }

                                    conds.clear();
                                    conds.add("employee.dbid = " + employeeHiring.getDbid());
                                    conds.add("salaryElement.dbid = " + salaryElement.getDbid());
                                    EmployeeSalaryElement empSE = (EmployeeSalaryElement) oem.loadEntity(
                                            EmployeeSalaryElement.class.getSimpleName(),
                                            conds, null, loggedUser);
                                    if (empSE != null) {
                                        empSE.setValue(newValue);
                                        try {
                                            oem.saveEntity(empSE, loggedUser);
                                        } catch (Exception e) {
                                            OLog.logError("Error in ** postSalaryElementFormula **: "
                                                    + "\nError encountered in updating the element salary element "
                                                    + "on the employee: " + empSE.getEmployee().getCode(), loggedUser);
                                            OLog.logException(e, loggedUser);
                                            // TODO: add user message
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        List<String> conds = new ArrayList<String>();
                        conds.add("type.code = 'equation'");
                        conds.add("attribute.code = 'depNum'");
                        formulas = (List<SalaryElementFormula>) oem.loadEntityList(
                                SalaryElementFormula.class.getSimpleName(), conds, null, null, loggedUser);
                        for (int i = 0; i < formulas.size(); i++) {
                            if (formulas.get(i).getSalaryElement() != null) {
                                if (formulas.get(i).getOperator() != null
                                        && formulas.get(i).getValue() != null) {
                                    SalaryElement salaryElement = formulas.get(i).getSalaryElement();
                                    conds.clear();
                                    conds.add("employee.dbid = " + employeeHiring.getDbid());
                                    conds.add("salaryElement.dbid = " + salaryElement.getDbid());
                                    EmployeeSalaryElement empSE = (EmployeeSalaryElement) oem.loadEntity(
                                            EmployeeSalaryElement.class.getSimpleName(),
                                            conds, null, loggedUser);
                                    if (empSE != null) {
                                        empSE.setValue(BigDecimal.ZERO);
                                        try {
                                            oem.saveEntity(empSE, loggedUser);
                                        } catch (Exception e) {
                                            OLog.logError("Error in ** postSalaryElementFormula **: "
                                                    + "\nError encountered in updating the element salary element "
                                                    + "on the employee: " + empSE.getEmployee().getCode(), loggedUser);
                                            OLog.logException(e, loggedUser);
                                            // TODO: add user message
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeProfileHistory")) {
                EmployeeProfileHistory profileHistory = (EmployeeProfileHistory) odm.getData().get(0);

                if (profileHistory != null && profileHistory.getPosition() != null) {
                    Job job = profileHistory.getPosition().getJob();
                    List<SalaryElementFormula> formulas = new ArrayList<SalaryElementFormula>();

                    if (job != null) {
                        List<String> conds = new ArrayList<String>();
                        conds.add("job.dbid = " + job.getDbid());
                        formulas = (List<SalaryElementFormula>) oem.loadEntityList(
                                SalaryElementFormula.class.getSimpleName(), conds, null, null, loggedUser);

                        for (int i = 0; i < formulas.size(); i++) {
                            EmployeeSalaryElement empSE = null;
                            if (formulas.get(i).getSalaryElement() != null) {
                                conds.clear();
                                conds.add("employee.dbid = " + profileHistory.getEmployee().getDbid());
                                conds.add("salaryElement.dbid = " + formulas.get(i).getSalaryElement().getDbid());
                                empSE = (EmployeeSalaryElement) oem.loadEntity(
                                        EmployeeSalaryElement.class.getSimpleName(), conds, null, loggedUser);
                                if (empSE != null) {
                                    empSE.setValue(formulas.get(i).getValue());
                                    try {
                                        oem.saveEntity(empSE, loggedUser);
                                    } catch (Exception e) {

                                        OLog.logError("Error in ** postSalaryElementFormula **: "
                                                + "\nError encountered in updating the element salary element "
                                                + "on the employee: " + empSE.getEmployee().getCode(), loggedUser);
                                        OLog.logException(e, loggedUser);
                                        // TODO: add user message
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeDependance")) {
                EmployeeDependance employeeDependance = (EmployeeDependance) odm.getData().get(0);

                List<SalaryElementFormula> formulas = null;
                List<EmployeeDependance> dependances = null;

                if (employeeDependance != null && employeeDependance.getEmployee() != null) {
                    Employee employee = employeeDependance.getEmployee();
                    boolean appFormula = isSalaryElementFormulaApplied(null, employee, loggedUser);
                    if (appFormula) {
                        List<String> conds = new ArrayList<String>();
                        conds.add("type.code = 'equation'");
                        conds.add("attribute.code = 'depNum'");
                        formulas = (List<SalaryElementFormula>) oem.loadEntityList(
                                SalaryElementFormula.class.getSimpleName(), conds, null, null, loggedUser);

                        conds.clear();
                        conds.add("employee.dbid = " + employee.getDbid());
                        conds.add("formulaApplied = 'Y'");
                        dependances = oem.loadEntityList(EmployeeDependance.class.getSimpleName(),
                                conds, null, null, loggedUser);

                        BigDecimal newValue = BigDecimal.ZERO;

                        int depNum = dependances.size() + 1; // 1 for the employee himself
                        for (int i = 0; i < formulas.size(); i++) {
                            if (formulas.get(i).getOperator() != null
                                    && formulas.get(i).getValue() != null) {

                                SalaryElement salaryElement = formulas.get(i).getSalaryElement();

                                BigDecimal value = formulas.get(i).getValue();

                                if (formulas.get(i).getOperator().getCode().equals("*")) {
                                    newValue = value.multiply(new BigDecimal(depNum));
                                } else if (formulas.get(i).getOperator().getCode().equals("+")) {
                                    newValue = value.add(new BigDecimal(depNum));
                                } else if (formulas.get(i).getOperator().getCode().equals("-")) {
                                    newValue = value.subtract(new BigDecimal(depNum));
                                } else if (formulas.get(i).getOperator().getCode().equals("/")) {
                                    newValue = value.divide(new BigDecimal(depNum));
                                }

                                conds.clear();
                                conds.add("employee.dbid = " + employee.getDbid());
                                conds.add("salaryElement.dbid = " + salaryElement.getDbid());
                                EmployeeSalaryElement empSE = (EmployeeSalaryElement) oem.loadEntity(
                                        EmployeeSalaryElement.class.getSimpleName(),
                                        conds, null, loggedUser);
                                if (empSE != null) {
                                    empSE.setValue(newValue);
                                    try {
                                        oem.saveEntity(empSE, loggedUser);
                                    } catch (Exception e) {
                                        OLog.logError("Error in ** postSalaryElementFormula **: "
                                                + "\nError encountered in updating the element salary element "
                                                + "on the employee: " + empSE.getEmployee().getCode(), loggedUser);
                                        OLog.logException(e, loggedUser);
                                        // TODO: add user message
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
            return ofr;

        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult applySalaryUpgradeServerJob(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            List dateConditions = new ArrayList();
            EmployeeSalaryUpgrade salaryUpgrade;
            Calendar todayCal = Calendar.getInstance();
            dateConditions.add("applyDate = '" + DATE_FOTMAT.format(todayCal.getTime()) + "'");
            List<EmployeeSalaryUpgrade> applySalaryUpgrade = oem.loadEntityList(EmployeeSalaryUpgrade.class.getSimpleName(), dateConditions, null, null, loggedUser);
            for (int i = 0; i < applySalaryUpgrade.size(); i++) {
                List conditions = new ArrayList();
                salaryUpgrade = applySalaryUpgrade.get(i);
                if (!validateSalaryUpgradeIsCancelledOrIsApplied(salaryUpgrade, oFR, loggedUser)) {
                    return oFR;
                }
                if (salaryUpgrade.getFromCalculatedPeriod() != null
                        && salaryUpgrade.getToCalculatedPeriod() != null) {
                    //Calling Salary Upgrade WS Case Retro
                    List salaryUpData = new ArrayList();

                    salaryUpData.add(salaryUpgrade.getEmployee().getDbid());
                    salaryUpData.add(salaryUpgrade.getIncome().getDbid());
                    if (salaryUpgrade.getValueAfter() == null) {
                        salaryUpData.add('0');
                    } else {
                        salaryUpData.add(salaryUpgrade.getValueAfter());
                    }
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    salaryUpData.add(dateFormat.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()));
                    salaryUpData.add(dateFormat.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()));
                    String webServiceCode = "WSF_ASU001";
                    webServiceUtility.callingWebService(webServiceCode, salaryUpData, loggedUser);

                } else {
                    conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                    conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
                    List<EmployeeSalaryElement> ese = oem.loadEntityList("EmployeeSalaryElement", conditions, null, null, loggedUser);
                    if (ese.size() > 0) {
                        ese.get(0).setValue(salaryUpgrade.getValueAfter());
                        oem.saveEntity(ese.get(0), loggedUser);
                    }
                }
                conditions.clear();
                conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                conditions.add("income.dbid = " + salaryUpgrade.getIncome().getDbid());
                List<EmployeeSalaryUpgrade> salaryUpgrades;
                salaryUpgrades = oem.loadEntityList("EmployeeSalaryUpgrade", conditions, null, null, loggedUser);
                salaryUpgrade.setTransactionNumber(new BigDecimal(salaryUpgrades.size()));
                salaryUpgrade.setApplied("Y");
                oFR.addSuccess(userMessageServiceRemote.getUserMessage("salaryUpgrade003", loggedUser));
                oem.saveEntity(salaryUpgrade, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult validateEmpSalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        List conditions = new ArrayList();
        List conditions2 = new ArrayList();
        try {
            EmployeeSalaryUpgrade salaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
            if (salaryUpgrade == null) {
                oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
                return oFR;
            }
            EmployeeSalaryUpgrade salaryUpgradeTrans = (EmployeeSalaryUpgrade) oem.loadEntity("EmployeeSalaryUpgrade",
                    salaryUpgrade.getDbid(), null, null, loggedUser);
            if (salaryUpgradeTrans == null) //new salary upgrade
            {
                if ((salaryUpgrade.getValueAfter() != null) && !(salaryUpgrade.getValueAfter().equals(new BigDecimal(0)))) {
                    if ((salaryUpgrade.getUpgradeFactor() != null) && !(salaryUpgrade.getUpgradeFactor().equals(new BigDecimal(0)))) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade006", loggedUser));
                        return oFR;
                    }
                }

                conditions.clear();
                conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                conditions.add("income.dbid = " + salaryUpgrade.getIncome().getDbid());
                conditions.add("cancelled = 'N'");
                conditions2.add("applyDate desc");
                List<EmployeeSalaryUpgrade> empSUs = oem.loadEntityList("EmployeeSalaryUpgrade", conditions, null, conditions2, loggedUser);
                if (empSUs != null && !empSUs.isEmpty()) {
                    for (int i = 0; i < empSUs.size(); i++) {
                        if (empSUs.get(i).getApplyDate().compareTo(salaryUpgrade.getApplyDate()) >= 0) {
                            oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade010", loggedUser));
                            return oFR;
                        }
                    }
                }
            } else //Modify old salary upgrade
            {
                // [Loubna] -  - 16/07/13
                if ((salaryUpgrade.getValueAfter() != null) && !(salaryUpgrade.getValueAfter().equals(new BigDecimal(0)))) {
                    if ((salaryUpgrade.getUpgradeFactor() != null) && !(salaryUpgrade.getUpgradeFactor().equals(new BigDecimal(0)))) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade006", loggedUser));
                        return oFR;
                    }
                }

                //Check if this record is already applied
                if (salaryUpgrade.getApplied() != null) {
                    if (salaryUpgrade.getApplied().equals("Y")) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade009", loggedUser));
                    }
                } else {
                    conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                    conditions.add("income.dbid = " + salaryUpgrade.getIncome().getDbid());
                    conditions2.add("applyDate desc");
                    List<EmployeeSalaryUpgrade> esu = oem.loadEntityList("EmployeeSalaryUpgrade", conditions, null, conditions2, loggedUser);

                    if (esu.size() > 0) {
                        if (esu.get(0).getDbid() == salaryUpgrade.getDbid())//Modified record is the last record
                        {
                            if (salaryUpgrade.getApplied() != null) {
                                if (salaryUpgrade.getApplied().equals("Y")) {
                                    oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade007", loggedUser));
                                    return oFR;
                                }
                            }
                        } else {
                            oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade008", loggedUser));
                            return oFR;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult validateSalaryUpgrade(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeSalaryUpgrade salaryUpgrade = (EmployeeSalaryUpgrade) odm.getData().get(0);
            if (salaryUpgrade != null) {
                Employee employee = salaryUpgrade.getEmployee();
                SalaryElement salaryElement = salaryUpgrade.getIncome();
                GradeSalaryElement gradeSE = null;
                PayGrade payGrade = null;

                BigDecimal oldVal = salaryUpgrade.getValueBefore();
                BigDecimal newVal = salaryUpgrade.getValueAfter();

                List<String> conds = new ArrayList<String>();
                conds.add("employee.dbid = " + employee.getDbid());
                EmployeePayroll employeePayroll = (EmployeePayroll) oem.loadEntity(EmployeePayroll.class.getSimpleName(), conds, null, loggedUser);

                if (employeePayroll != null) {
                    payGrade = employeePayroll.getPayGrade();
                    if (payGrade != null) {
                        conds.clear();;
                        conds.add("salaryElement.dbid = " + salaryElement.getDbid());
                        conds.add("payGrade.dbid = " + payGrade.getDbid());
                        gradeSE = (GradeSalaryElement) oem.loadEntity(GradeSalaryElement.class.getSimpleName(), conds, null, loggedUser);
                        if (gradeSE != null) {
                            BigDecimal minAllowedVal = getMinAlowedValue(oldVal, gradeSE);
                            if (minAllowedVal != null) {
                                if (newVal.compareTo(minAllowedVal) < 0) {
                                    ofr.addError(userMessageServiceRemote.getUserMessage("SalUpgradeMinVal", loggedUser));
                                }
                                if (newVal.compareTo(gradeSE.getMaxSalValue()) > 0) {
                                    ofr.addError(userMessageServiceRemote.getUserMessage("SalUpgradeMaxVal", loggedUser));
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
            return ofr;

        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult costCenterCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            PayrollCalculation payrollCalc = (PayrollCalculation) odm.getData().get(0);
            if (payrollCalc != null) {
                CalculatedPeriod currentCalcPeriod = payrollCalc.getCalculatedPeriod();
                PaymentPeriod paymentPeriod = currentCalcPeriod.getPayPeriod();
                List<EmployeeSalary> empSalaries = null;
                List<SalaryElement> salaryElements = null;

                BigDecimal hoursPerPeriod = paymentPeriod.getHoursPerDay();
                int daysPerPeriod = paymentPeriod.getDaysPerPeriod();

                List<String> conds = new ArrayList<String>();

                // get employee salary records on current calc period
                conds.clear();
                conds.add("calculatedPeriod_dbid = " + currentCalcPeriod.getDbid());

                List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
                String securityCondSQL = "";
                if (!employeeDbidList.isEmpty()) {
                    String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                    String securityCond = "employee_dbid in " + employeeDbidStr;
                    securityCondSQL = " AND employee_dbid in " + employeeDbidStr;
                    conds.add(securityCond);
                }

                empSalaries = oem.loadEntityList(EmployeeSalary.class.getSimpleName(),
                        conds, null, null, loggedUser);

                String deletedStatement = "delete from CostCenterCalculation where calculatedPeriod_dbid = " + currentCalcPeriod.getDbid() + securityCondSQL;
                oem.executeEntityUpdateNativeQuery(deletedStatement, loggedUser);

                for (int i = 0; i < empSalaries.size(); i++) {
                    EmployeeSalary empSalary = (EmployeeSalary) empSalaries.get(i);
                    Employee employee = null;
                    List<EmpCostCenterException> empCostCenterExcs = null;
                    List<EmployeeCostCenter> empCostCenters = null;

                    conds.clear();
                    conds.add("dbid = " + empSalary.getEmployee_dbid());
                    employee = (Employee) oem.loadEntity(Employee.class.getSimpleName(), conds, null, loggedUser);

                    conds.clear();
                    conds.add("employee.dbid = " + empSalary.getEmployee_dbid());
                    empCostCenterExcs = (List<EmpCostCenterException>) oem.loadEntityList(
                            EmpCostCenterException.class.getSimpleName(), conds, null, null, loggedUser);

                    StringBuilder typesDbids = new StringBuilder("");
                    boolean exceptionsExist = false;
                    if (empCostCenterExcs != null && !empCostCenterExcs.isEmpty()) {
                        exceptionsExist = true;
                        for (int j = 0; j < empCostCenterExcs.size(); j++) {
                            EmpCostCenterException ccException = empCostCenterExcs.get(j);
                            SalaryElementType salElementType = ccException.getSalaryElementType();
                            CostCenter costCenter = empCostCenterExcs.get(j).getCostCenter();

                            conds.clear();
                            conds.add("type.dbid = " + salElementType.getDbid());
                            conds.add("costCenterDistribution = true");
                            List<SalaryElement> exceptionSalElements = oem.loadEntityList(
                                    SalaryElement.class.getSimpleName(), conds, null, null, loggedUser);
                            for (int k = 0; k < exceptionSalElements.size(); k++) {
                                BigDecimal oldValue = null;
                                String salElementName = exceptionSalElements.get(k).getMappedColumn().replace("S", "s");
                                oldValue = new BigDecimal(BaseEntity.getValueFromEntity(empSalary, salElementName).toString());

                                CostCenterCalculation costCenterCalc = new CostCenterCalculation();
                                costCenterCalc.setCalculatedPeriod(currentCalcPeriod);
                                costCenterCalc.setSalaryElement(exceptionSalElements.get(k));
                                costCenterCalc.setEmployee(employee);
                                costCenterCalc.setCostCenter(costCenter);
                                costCenterCalc.setSalaryElementValue(oldValue);
                                costCenterCalc.setSalaryElementType(salElementType);
                                costCenterCalc = setCostCenterCalcValue(costCenterCalc, empCostCenterExcs.get(j).getDistributionPercentage(),
                                        empCostCenterExcs.get(j).getDistributionDays(), empCostCenterExcs.get(j).getDistributionHours(),
                                        oldValue, daysPerPeriod, hoursPerPeriod);
                                oem.saveEntity(costCenterCalc, loggedUser);
                            }
                            typesDbids.append(salElementType.getDbid() + ",");
                        }
                    }
                    conds.clear();
                    if (exceptionsExist) {
                        conds.clear();
                        conds.add("type.dbid not in (" + typesDbids.replace(typesDbids.length() - 1, typesDbids.length(), "") + ")"); //replace last , ??????????!!!!!????????
                    }
                    conds.add("costCenterDistribution = true");
                    salaryElements = (List<SalaryElement>) oem.loadEntityList(SalaryElement.class.getSimpleName(),
                            conds, null, null, loggedUser);

                    conds.clear();
                    conds.add("employee.dbid = " + empSalary.getEmployee_dbid());
                    empCostCenters = (List<EmployeeCostCenter>) oem.loadEntityList(
                            EmployeeCostCenter.class.getSimpleName(), conds, null, null, loggedUser);

                    if (empCostCenters != null) {
                        for (int j = 0; j < empCostCenters.size(); j++) {
                            CostCenter costCenter = empCostCenters.get(j).getCostCenter();
                            for (int k = 0; k < salaryElements.size(); k++) {

                                BigDecimal oldValue = null;
                                String salElementName = salaryElements.get(k).getMappedColumn().replace("S", "s");
                                oldValue = new BigDecimal(BaseEntity.getValueFromEntity(empSalary, salElementName).toString());

                                CostCenterCalculation costCenterCalc = new CostCenterCalculation();
                                costCenterCalc.setCalculatedPeriod(currentCalcPeriod);
                                costCenterCalc.setSalaryElement(salaryElements.get(k));
                                costCenterCalc.setEmployee(employee);
                                costCenterCalc.setCostCenter(costCenter);
                                costCenterCalc.setSalaryElementValue(oldValue);
                                costCenterCalc.setSalaryElementType(salaryElements.get(k).getType());
                                costCenterCalc = setCostCenterCalcValue(costCenterCalc, empCostCenters.get(j).getDistPercent(),
                                        empCostCenters.get(j).getDistDays(), empCostCenters.get(j).getDistHours(),
                                        oldValue, daysPerPeriod, hoursPerPeriod);
                                oem.saveEntity(costCenterCalc, loggedUser);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult setEmployeeCostCenter(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            Employee employee = (Employee) odm.getData().get(0);
            if (employee != null) {

                List<String> conds = new ArrayList<String>();
                conds.add("company.dbid = " + loggedUser.getUserPrefrence1());
                List<CompanyCostCenter> companyCostCenters = null;
                try {
                    companyCostCenters = (List<CompanyCostCenter>) oem.loadEntityList(CompanyCostCenter.class.getSimpleName(),
                            conds, null, null, loggedUser);
                } catch (Exception e) {
                    OLog.logError("Error in ** setEmployeeCostCenter **:\n"
                            + "Error encountered in loading Company Cost Centers.", loggedUser);
                    OLog.logException(e, loggedUser);
                    // TODO: add user message
                    return oFR;
                }

                if (companyCostCenters != null) {
                    for (int i = 0; i < companyCostCenters.size(); i++) {
                        EmployeeCostCenter empCostCenter = new EmployeeCostCenter();
                        empCostCenter.setEmployee(employee);
                        empCostCenter.setCostCenter(companyCostCenters.get(i).getCostCenter());
                        empCostCenter.setDistHours(BigDecimal.ZERO);
                        empCostCenter.setDistDays(BigDecimal.ZERO);
                        empCostCenter.setDistPercent(companyCostCenters.get(i).getDistPercent());
                        try {
                            oFR.append(entitySetupService.callEntityCreateAction(empCostCenter, loggedUser));
                        } catch (Exception e) {
                            OLog.logError("Error in ** setEmployeeCostCenter **: "
                                    + "\nError encountered in creating the employee cost center record.", loggedUser);
                            OLog.logException(e, loggedUser);
                            // TODO: add user message
                            return oFR;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult validateEmpCostCenter(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            BigDecimal distPercent = null,
                    distHours = null,
                    distDays = null;
            if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeCostCenter")) {
                EmployeeCostCenter empCC = (EmployeeCostCenter) odm.getData().get(0);
                distPercent = empCC.getDistPercent();
                distHours = empCC.getDistHours();
                distDays = empCC.getDistDays();
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmpCostCenterException")) {
                EmpCostCenterException empCCExc = (EmpCostCenterException) odm.getData().get(0);
                distPercent = empCCExc.getDistributionPercentage();
                distHours = empCCExc.getDistributionHours();
                distDays = empCCExc.getDistributionDays();
            }

            if (distPercent != null && !distPercent.toString().equals("0E-13") && distPercent.compareTo(BigDecimal.ZERO) > 0) {
                if ((distDays != null && !distDays.toString().equals("0E-13") && distDays.compareTo(BigDecimal.ZERO) > 0)
                        || (distHours != null && !distHours.toString().equals("0E-13") && distHours.compareTo(BigDecimal.ZERO) > 0)) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("EmpCC001", loggedUser));
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    @Override
    public OFunctionResult validateCCTotalDistPercentage(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            BigDecimal totalValue = BigDecimal.ZERO;
            boolean isPercentage = false,
                    isDays = false,
                    percentAndDays = false;

            if (((List) odm.getData().get(1)) == null || (((List) odm.getData().get(1)) != null && ((List) odm.getData().get(1)).isEmpty())) {
                return oFR;
            }
            if (((List) odm.getData().get(1)).get(0).getClass().getSimpleName().equals("EmployeeCostCenter")) {
                List<EmployeeCostCenter> empCCs = (List<EmployeeCostCenter>) odm.getData().get(1);

                for (int i = 0; i < empCCs.size(); i++) {
                    totalValue = totalValue.add(empCCs.get(i).getDistPercent() == null ? BigDecimal.ZERO : empCCs.get(i).getDistPercent());
                }

                for (int i = 0; i < empCCs.size(); i++) {
                    for (int j = 0; j < empCCs.size(); j++) {
                        if (empCCs.get(i).getCostCenter().equals(empCCs.get(j).getCostCenter()) && i != j) {
                            oFR.addError(userMessageServiceRemote.getUserMessage("CCNotUnique", loggedUser));
                            return oFR;
                        }
                    }
                }

                for (int i = 0; i < empCCs.size(); i++) {
                    if (empCCs.get(i).getDistPercent() != null && empCCs.get(i).getDistPercent().compareTo(BigDecimal.ZERO) != 0) {
                        isPercentage = true;
                    }

                    if ((empCCs.get(i).getDistDays() != null && empCCs.get(i).getDistDays().compareTo(BigDecimal.ZERO) != 0)
                            || (empCCs.get(i).getDistHours() != null && empCCs.get(i).getDistHours().compareTo(BigDecimal.ZERO) != 0)) {
                        isDays = true;
                    }
                    if (isDays && isPercentage) {
                        percentAndDays = true;
                        break;
                    }
                }
            }

            if (((List) odm.getData().get(1)).get(0).getClass().getSimpleName().equals("EmpCostCenterException")) {
                List<EmpCostCenterException> empCCs = (List<EmpCostCenterException>) odm.getData().get(1);

                Map<SalaryElementType, List<EmpCostCenterException>> empCCMap;
                empCCMap = new HashMap<SalaryElementType, List<EmpCostCenterException>>();
                for (int i = 0; i < empCCs.size(); i++) {
                    List<EmpCostCenterException> tempLSt = empCCMap.get(empCCs.get(i).getSalaryElementType());
                    if (tempLSt == null) {
                        tempLSt = new ArrayList<EmpCostCenterException>();
                    }
                    tempLSt.add(empCCs.get(i));
                    empCCMap.put(empCCs.get(i).getSalaryElementType(), tempLSt);
                }
                for (Map.Entry<SalaryElementType, List<EmpCostCenterException>> entry : empCCMap.entrySet()) {
                    if (percentAndDays) {
                        break;
                    }

                    isPercentage = false;
                    isDays = false;
                    totalValue = (BigDecimal.ZERO);

                    SalaryElementType salaryElementType = entry.getKey();
                    List<EmpCostCenterException> list = entry.getValue();
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getDistributionPercentage() != null && list.get(i).getDistributionPercentage().compareTo(BigDecimal.ZERO) != 0) {
                            isPercentage = true;
                        }

                        if ((list.get(i).getDistributionDays() != null && list.get(i).getDistributionDays().compareTo(BigDecimal.ZERO) != 0)
                                || (list.get(i).getDistributionHours() != null && list.get(i).getDistributionHours().compareTo(BigDecimal.ZERO) != 0)) {
                            isDays = true;
                        }
                        if (isDays && isPercentage) {
                            percentAndDays = true;
                            break;
                        }
                        for (int j = 0; j < list.size(); j++) {
                            if (list.get(i).getCostCenter().equals(list.get(j).getCostCenter()) && i != j) {
                                oFR.addError(userMessageServiceRemote.getUserMessage("CCNotUnique", loggedUser));
                                return oFR;
                            }
                        }
                        totalValue = totalValue.add(empCCs.get(i).getDistributionPercentage() == null ? BigDecimal.ZERO : empCCs.get(i).getDistributionPercentage());
                    }
                    if (!(totalValue.compareTo(BigDecimal.ZERO) == 0 || totalValue.compareTo(new BigDecimal(100)) == 0)) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("EmpCCValidation01", loggedUser));
                        return oFR;
                    }
                }
            }

            if (((List) odm.getData().get(1)).get(0).getClass().getSimpleName().equals("CompanyCostCenter")) {
                List<CompanyCostCenter> compCCs = (List<CompanyCostCenter>) odm.getData().get(1);

                for (int i = 0; i < compCCs.size(); i++) {
                    totalValue = totalValue.add(compCCs.get(i).getDistPercent() == null ? BigDecimal.ZERO : compCCs.get(i).getDistPercent());
                }

                for (int i = 0; i < compCCs.size(); i++) {
                    for (int j = 0; j < compCCs.size(); j++) {
                        if (compCCs.get(i).getCostCenter().equals(compCCs.get(j).getCostCenter()) && i != j) {
                            oFR.addError(userMessageServiceRemote.getUserMessage("CCNotUnique", loggedUser));
                            return oFR;
                        }
                    }
                }
            }

            if (!(totalValue.compareTo(BigDecimal.ZERO) == 0 || totalValue.compareTo(new BigDecimal(100)) == 0)) {
                oFR.addError(userMessageServiceRemote.getUserMessage("EmpCCValidation01", loggedUser));
                return oFR;
            }

            if (percentAndDays) {
                oFR.addError(userMessageServiceRemote.getUserMessage("PercentAndDaysEntered", loggedUser));
                return oFR;
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    private BigDecimal getMinAlowedValue(BigDecimal value, GradeSalaryElement gradeSE) {
        BigDecimal minAlowedValue = null;

        if (gradeSE != null) {
            BigDecimal minValue = gradeSE.getMinSalValue();
            BigDecimal maxVal = gradeSE.getMaxSalValue();

            if (minValue != null && maxVal != null) {
                BigDecimal firstMidVal = gradeSE.getMidSalValue1();
                BigDecimal secMidVal = gradeSE.getMidSalValue2();
                BigDecimal thirdMidVal = gradeSE.getMidSalValue3();

                if (firstMidVal != null && secMidVal != null && thirdMidVal != null) {
                    if (value.compareTo(minValue) <= 0) {
                        minAlowedValue = minValue;
                    } else if (value.compareTo(minValue) >= 0
                            && value.compareTo(firstMidVal) <= 0) {
                        minAlowedValue = firstMidVal;
                    } else if (value.compareTo(firstMidVal) >= 0
                            && value.compareTo(secMidVal) <= 0) {
                        minAlowedValue = secMidVal;
                    } else if (value.compareTo(secMidVal) >= 0
                            && value.compareTo(thirdMidVal) <= 0) {
                        minAlowedValue = thirdMidVal;
                    } else if (value.compareTo(thirdMidVal) >= 0
                            && value.compareTo(maxVal) <= 0) {
                        minAlowedValue = maxVal;
                    }
                }
            }
        }

        return minAlowedValue;
    }

    private CostCenterCalculation setCostCenterCalcValue(CostCenterCalculation costCenterCalc, BigDecimal percent,
            BigDecimal days, BigDecimal hours, BigDecimal oldValue, int daysPerPeriod, BigDecimal hoursPerPeriod) {
        BigDecimal newValue = null;
        if (percent != null
                && percent.compareTo(BigDecimal.ZERO) == 1) {
            BigDecimal precent = percent;
            newValue = oldValue.multiply(precent.divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP));

            costCenterCalc.setDistributionPercentage(precent);
        } else if (days != null
                && days.compareTo(BigDecimal.ZERO) == 1) {

            BigDecimal distDays = days;
            newValue = oldValue.multiply(distDays.divide(new BigDecimal(daysPerPeriod), BigDecimal.ROUND_HALF_UP));

            costCenterCalc.setDistributionDays(distDays);
        } else if (hours != null
                && hours.compareTo(BigDecimal.ZERO) == 1) {

            BigDecimal distHours = hours;
            newValue = oldValue.multiply(distHours.divide(hoursPerPeriod.multiply(new BigDecimal(daysPerPeriod)), BigDecimal.ROUND_HALF_UP));

            costCenterCalc.setDistributionHours(distHours);
        }

        costCenterCalc.setDistributionValue(newValue);
        return costCenterCalc;
    }

    @Override
    public OFunctionResult updateEmployeeDeductionsIncentiveVacation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        EmployeeVacationRequest employeeVactionRequest;
        try {
            employeeVactionRequest = (EmployeeVacationRequest) odm.getData().get(0);
            List<String> employeeDeductionsConditions = new ArrayList<String>();
            employeeDeductionsConditions.add("employee.dbid = " + employeeVactionRequest.getEmployee().getDbid());
            employeeDeductionsConditions.add("salaryElement.internalCode like  'IncOut%' ");
            List<EmployeeSalaryElement> employeeSalaryElements = oem.loadEntityList("EmployeeSalaryElement", employeeDeductionsConditions, null, null, loggedUser);
            if (employeeSalaryElements.isEmpty()) {
                return oFR;
            }
            BigDecimal vacationValue = employeeVactionRequest.getPeriod();
            List<String> vacationConditions = new ArrayList<String>();
            vacationConditions.add("vacation.dbid = " + employeeVactionRequest.getVacation().getDbid());
            vacationConditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
            List<IncentivesVacationSetup> incentivesVacation = oem.loadEntityList("IncentivesVacationSetup", vacationConditions, null, null, loggedUser);
            for (int i = 0; i < incentivesVacation.size(); i++) {
                if (incentivesVacation.get(i).getFromValue().doubleValue() <= vacationValue.doubleValue() && incentivesVacation.get(i).getToValue().doubleValue() >= vacationValue.doubleValue()) {
                    for (int j = 0; j < employeeSalaryElements.size(); j++) {
                        if (employeeVactionRequest.getCancelled().equals("Y")) {
                            employeeSalaryElements.get(j).setValue(employeeSalaryElements.get(j).getValue().subtract(incentivesVacation.get(i).getIncentiveValue()));
                            oem.saveEntity(employeeSalaryElements.get(j), loggedUser);
                        } else {
                            employeeSalaryElements.get(j).setValue(employeeSalaryElements.get(j).getValue().add(incentivesVacation.get(i).getIncentiveValue()));
                            oem.saveEntity(employeeSalaryElements.get(j), loggedUser);
                        }
                    }
                    break;
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
            return oFR;
        }
        return oFR;
    }

    @Override
    public OFunctionResult convertFromNetToGross(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        List<String> where = new ArrayList<String>();
        List<String> where2 = new ArrayList<String>();
        Employee employee;
        BigDecimal value;
        BigDecimal net = null;

        EmployeeSalaryElement empSalaryElement = (EmployeeSalaryElement) odm.getData().get(0);

        if (empSalaryElement.getSalaryElement().getInternalCode() != null && empSalaryElement.getSalaryElement().getInternalCode().equals("Net Salary")) {
            try {

                employee = empSalaryElement.getEmployee();
                value = empSalaryElement.getValue();

                where.add("salaryElement.internalCode like  'Gross Sala%' ");
                where.add("employee.dbid = " + employee.getDbid());

                where2.add("net = " + value);

                List<NetToGross> netGross = oem.loadEntityList("NetToGross", where2, null, null, loggedUser);

                if (netGross.size() > 0) {
                    net = netGross.get(0).getGross();
                }
                List<EmployeeSalaryElement> empSalElement = oem.loadEntityList("EmployeeSalaryElement",
                        where, null, null, loggedUser);

                if (empSalElement.size() > 0) {
                    empSalElement.get(0).setValue(net);
                    oem.saveEntity(empSalElement.get(0), loggedUser);
                }

            } catch (Exception ex) {
                OLog.logException(ex, loggedUser);
            }

        }

        return ofr;
    }

    @Override
    public OFunctionResult updateEmployeeDeductionsIncentivePenalty(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        EmployeePenaltyRequest employeePenaltyRequest;
        try {
            employeePenaltyRequest = (EmployeePenaltyRequest) odm.getData().get(0);
            List<String> employeeDeductionsConditions = new ArrayList<String>();
            employeeDeductionsConditions.add("employee.dbid = " + employeePenaltyRequest.getEmployee().getDbid());
            employeeDeductionsConditions.add("salaryElement.internalCode like  'IncOut%' ");
            List<EmployeeSalaryElement> employeeSalaryElements = oem.loadEntityList("EmployeeSalaryElement", employeeDeductionsConditions, null, null, loggedUser);
            if (employeeSalaryElements.isEmpty()) {
                return oFR;
            }
            BigDecimal penaltyValue = employeePenaltyRequest.getPenaltyValue();
            List<String> penaltyConditions = new ArrayList<String>();
            penaltyConditions.add("penalty.dbid = " + employeePenaltyRequest.getPenalty().getDbid());
            penaltyConditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
            List<IncentivesPenaltySetup> incentivesPenalty = oem.loadEntityList("IncentivesPenaltySetup", penaltyConditions, null, null, loggedUser);
            for (int i = 0; i < incentivesPenalty.size(); i++) {
                if (incentivesPenalty.get(i).getFromValue().doubleValue() <= penaltyValue.doubleValue() && incentivesPenalty.get(i).getToValue().doubleValue() >= penaltyValue.doubleValue()) {
                    for (int j = 0; j < employeeSalaryElements.size(); j++) {
                        if (employeePenaltyRequest.getCancelled().equals("Y")) {
                            employeeSalaryElements.get(j).setValue(employeeSalaryElements.get(j).getValue().subtract(incentivesPenalty.get(i).getIncentiveValue()));
                            oem.saveEntity(employeeSalaryElements.get(j), loggedUser);
                        } else {
                            employeeSalaryElements.get(j).setValue(employeeSalaryElements.get(j).getValue().add(incentivesPenalty.get(i).getIncentiveValue()));
                            oem.saveEntity(employeeSalaryElements.get(j), loggedUser);
                        }
                    }
                    break;
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
            return oFR;
        }
        return oFR;
    }

    @Override
    public OFunctionResult updateEmployeeDeductionsIncentiveAbsence(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        EmployeeAbsenceRequest employeeAbsenceRequest;
        try {
            employeeAbsenceRequest = (EmployeeAbsenceRequest) odm.getData().get(0);
            List<String> employeeDeductionsConditions = new ArrayList<String>();
            employeeDeductionsConditions.add("employee.dbid = " + employeeAbsenceRequest.getEmployee().getDbid());
            employeeDeductionsConditions.add("salaryElement.internalCode like  'IncOut%' ");
            List<EmployeeSalaryElement> employeeSalaryElements = oem.loadEntityList("EmployeeSalaryElement", employeeDeductionsConditions, null, null, loggedUser);
            if (employeeSalaryElements.isEmpty()) {
                return oFR;
            }
            BigDecimal absenceValue = employeeAbsenceRequest.getPeriod();
            List<String> absenceConditions = new ArrayList<String>();
            absenceConditions.add("absence.dbid = " + employeeAbsenceRequest.getAbsence().getDbid());
            absenceConditions.add(OEntityManager.CONFCOND_GET_ACTIVEANDINACTIVE);
            List<IncentivesAbsenceSetup> incentivesAbsence = oem.loadEntityList("IncentivesAbsenceSetup", absenceConditions, null, null, loggedUser);
            for (int i = 0; i < incentivesAbsence.size(); i++) {
                if (incentivesAbsence.get(i).getFromValue().doubleValue() <= absenceValue.doubleValue() && incentivesAbsence.get(i).getToValue().doubleValue() >= absenceValue.doubleValue()) {
                    for (int j = 0; j < employeeSalaryElements.size(); j++) {
                        if (employeeAbsenceRequest.getCancelled().equals("Y")) {
                            employeeSalaryElements.get(j).setValue(employeeSalaryElements.get(j).getValue().subtract(incentivesAbsence.get(i).getIncentiveValue()));
                            oem.saveEntity(employeeSalaryElements.get(j), loggedUser);
                        } else {
                            employeeSalaryElements.get(j).setValue(employeeSalaryElements.get(j).getValue().add(incentivesAbsence.get(i).getIncentiveValue()));
                            oem.saveEntity(employeeSalaryElements.get(j), loggedUser);
                        }
                    }
                    break;
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
            return oFR;
        }
        return oFR;
    }

    private boolean isSalaryElementFormulaApplied(Integer emp, Employee inEmp, OUser user) {
        try {
            Employee employee = null;
            if (inEmp != null) {
                employee = inEmp;
            } else {
                employee = (Employee) oem.loadEntity(Employee.class.getSimpleName(), emp, null, null, user);
            }
            if (employee != null) {
                if (employee.getFormulaApplied() != null) {
                    if (employee.getFormulaApplied().equals("Y")) {
                        return true;
                    }
                } else if (employee.getGender() != null && employee.getGender().getCode().equals("M")) {
                    if (employee.getMaritalStatus() != null && !employee.getMaritalStatus().getCode().equals("01")) {
                        return true;
                    }
                } else if (employee.getGender() != null && employee.getGender().getCode().equals("F")) {
                    if (employee.getMaritalStatus() != null && employee.getMaritalStatus().getCode().equals("01")) {
                        return true;
                    }
                }

            }
        } catch (Exception ex) {
            OLog.logException(ex, user);
        }
        return false;
    }

    @Override
    public OFunctionResult netToGrossCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeSalaryElement ese = (EmployeeSalaryElement) odm.getData().get(0);
            if (ese != null) {
                String internalCode;
                internalCode = ese.getSalaryElement().getInternalCode();
                List<String> conditions = new ArrayList<String>();
                if (internalCode.equalsIgnoreCase("net")) {
                    BigDecimal grossSalary = BigDecimal.ZERO;
                    BigDecimal oldGrossSalary = BigDecimal.ZERO;
                    BigDecimal oldRaiseTax = BigDecimal.ZERO;
                    BigDecimal oldRaiseNTax = BigDecimal.ZERO;
                    BigDecimal newRaise = BigDecimal.ZERO;
                    EmployeeSalaryElement employeeSalaryElement = null;
                    EmployeeSalaryElement ESE = null;
                    SalaryElement salaryElement = (SalaryElement) oem.loadEntity(SalaryElement.class.getSimpleName(), Collections.singletonList("dbid = "), null, loggedUser);
                    PayrollParameter payrollParameter = null;
                    conditions.add("employee.dbid = " + ese.getEmployee().getDbid());
                    conditions.add("salaryElement.internalCode = 'GROSS'");
                    employeeSalaryElement = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                    if (employeeSalaryElement != null) {
                        //Old Employee
                        String oldGrossSalarySQL;
                        // TODO: apply encryption
                        oldGrossSalarySQL = "select sum(value) from employeesalaryelement AA , salaryelement BB where AA.employee_dbid = "
                                + ese.getEmployee().getDbid() + " and AA.salaryelement_dbid = BB.dbid AND lower(BB.internalCode) like '%gross%'";
                        Object obj = oem.executeEntityNativeQuery(oldGrossSalarySQL, loggedUser);
                        if (obj != null) {
                            oldGrossSalary = new BigDecimal(obj.toString());
                        } else {
                            oldGrossSalary = employeeSalaryElement.getValue();
                        }

                        if (oldGrossSalary != null && oldGrossSalary.doubleValue() > 0) {
                            String oldRaiseSQL;
                            // TODO: apply encryption
                            oldRaiseSQL = "select sum(value) from employeesalaryelement AA , salaryelement BB where AA.employee_dbid = "
                                    + ese.getEmployee().getDbid() + " and AA.salaryelement_dbid = BB.dbid AND lower(BB.internalCode) like '%july%'"
                                    + " AND BB.dbid Not In (select income_dbid from TaxIncome where deleted = 0) ";
                            obj = oem.executeEntityNativeQuery(oldRaiseSQL, loggedUser);
                            if (obj != null) {
                                oldRaiseNTax = new BigDecimal(obj.toString());
                            }
                            grossSalary = netToGrossCal(ese.getEmployee(), ese.getValue().subtract(oldRaiseNTax), loggedUser);

                            oldRaiseSQL = "select sum(value) from employeesalaryelement AA , salaryelement BB where AA.employee_dbid = "
                                    + ese.getEmployee().getDbid() + " and AA.salaryelement_dbid = BB.dbid AND lower(BB.internalCode) like '%july%'"
                                    + " AND BB.dbid In (select income_dbid from TaxIncome where deleted = 0) ";
                            obj = oem.executeEntityNativeQuery(oldRaiseSQL, loggedUser);
                            if (obj != null) {
                                oldRaiseTax = new BigDecimal(obj.toString());
                            }
                            oldGrossSalary = oldGrossSalary.add(oldRaiseTax);
                            newRaise = grossSalary.subtract(oldGrossSalary);
                            //_________________Modifying Gross Salary By Raised Value(??????)_______________________
                            employeeSalaryElement.setValue(newRaise.add(employeeSalaryElement.getValue()));
                            oem.saveEntity(employeeSalaryElement, loggedUser);
                            //_______________________________________________________________________________
                            conditions.clear();
                            conditions.add("description = 'NewJulyRaise'");
                            conditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
                            payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conditions, null, loggedUser);
                            if (payrollParameter != null && payrollParameter.getValueData() != null) {
                                conditions.clear();
                                conditions.add("employee.dbid = " + ese.getEmployee().getDbid());
                                conditions.add("salaryElement.code = '" + payrollParameter.getValueData() + "'");
                                ESE = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                                if (ESE != null) {
                                    ESE.setValue(newRaise);
                                    oem.saveEntity(ESE, loggedUser);
                                }
                            }
                        } else {// Hiring Employee
                            grossSalary = netToGrossCal(ese.getEmployee(), ese.getValue(), loggedUser);
                            employeeSalaryElement.setValue(grossSalary);
                            oem.saveEntity(employeeSalaryElement, loggedUser);
                        }
                    }
                } else if ((internalCode.toLowerCase().contains("gross") && internalCode.length() > 5)
                        || (internalCode.toLowerCase().contains("july"))) {
                    EmployeeSalaryElement netESE;
                    conditions.clear();
                    conditions.add("employee.dbid = " + ese.getEmployee().getDbid());
                    conditions.add("salaryElement.internalCode = 'net'");
                    netESE = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                    if (netESE != null) {
                        conditions.clear();
                        conditions.add("description = 'NewAllowanceRaise'");
                        conditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
                        PayrollParameter payrollParameter;
                        payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conditions, null, loggedUser);
                        if (payrollParameter != null) {
                            BigDecimal raiseValue;
                            conditions.clear();
                            conditions.add("employee.dbid = " + ese.getEmployee().getDbid());
                            conditions.add("salaryElement.code = '" + payrollParameter.getValueData() + "'");
                            EmployeeSalaryElement raiseESE;
                            raiseESE = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                            if (raiseESE != null) {
                                raiseValue = raiseESE.getValue();
                                netESE.setValue(netESE.getValue().add(raiseValue));
                                entitySetupService.callEntityUpdateAction(netESE, loggedUser);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return oFR;
    }

    public BigDecimal netToGrossCal(Employee employee, BigDecimal netSalary, OUser loggedUser) {
        BigDecimal grossSalary = netSalary;
        BigDecimal taxableValue = BigDecimal.ZERO;
        BigDecimal calculatedTax = BigDecimal.ZERO;
        BigDecimal excemptionValue = BigDecimal.ZERO;
        BigDecimal factor1 = BigDecimal.ZERO;
        BigDecimal factor2 = BigDecimal.ZERO;
        BigDecimal minimumTaxValue = BigDecimal.ZERO;
        BigDecimal maximumTaxValue = BigDecimal.ZERO;
        BigDecimal previousTaxValue = BigDecimal.ZERO;
        BigDecimal taxPercent = BigDecimal.ZERO;
        BigDecimal insuranceValue = BigDecimal.ZERO;

        int category = 0, index = 0;

        List<String> conditions = new ArrayList<String>();
        List<String> orders = new ArrayList<String>();
        TaxGroup taxGroup = null;
        TaxGroupCalculation taxGroupCalculation = null;
        Tax tax = null;
        List<TaxBreak> taxBreaks = null;
        InsuranceCategory insuranceCategory = null;
        InsuranceGroup insuranceGroup = null;
        InsuranceGroupMember insuranceMember = null;
        Insurance insurance;
        EmployeePayroll employeePayroll = null;
        try {
            employeePayroll = employee.getPayroll();
            if (employeePayroll != null && employeePayroll.getDbid() != 0) {
                taxGroup = employeePayroll.getTaxGroup();
                if (taxGroup != null && taxGroup.getDbid() != 0) {
                    conditions.clear();
                    conditions.add("taxGroup.dbid = " + taxGroup.getDbid());
                    taxGroupCalculation = (TaxGroupCalculation) oem.loadEntity("TaxGroupCalculation", conditions, null, loggedUser);
                    if (taxGroupCalculation != null && taxGroupCalculation.getDbid() != 0) {
                        tax = taxGroupCalculation.getTax();
                        if (tax != null && tax.getDbid() != 0) {
                            conditions.clear();
                            conditions.add("tax.dbid = " + tax.getDbid());
                            orders.add("sortIndex");
                            taxBreaks = oem.loadEntityList("TaxBreak", conditions, null, orders, loggedUser);
                            if (taxBreaks != null && !taxBreaks.isEmpty()) {
                                excemptionValue = tax.getAnnualyExemptedValue();
                                taxableValue = netSalary.multiply(new BigDecimal(12));
                                taxableValue = taxableValue.subtract(excemptionValue);
                                if (taxableValue.doubleValue() > 0) {
                                    for (int i = 0; i < taxBreaks.size(); i++) {
                                        minimumTaxValue = taxBreaks.get(i).getMinValue();
                                        maximumTaxValue = taxBreaks.get(i).getMaximumValue();
                                        if (taxableValue.doubleValue() >= minimumTaxValue.doubleValue()
                                                && taxableValue.doubleValue() <= maximumTaxValue.doubleValue()) {
                                            category = taxBreaks.get(i).getSortIndex();
                                            index = i;
                                            previousTaxValue = taxBreaks.get(i).getPreviousBreakValue();
                                            factor1 = taxBreaks.get(i).getFactor1();
                                            factor2 = taxBreaks.get(i).getFactor2();
                                            taxPercent = taxBreaks.get(i).getTaxPercent();
                                            break;
                                        }
                                    }
                                    //Gross = (factor1 * Net) - factor2
                                    grossSalary = factor1.multiply(netSalary);
                                    grossSalary = grossSalary.subtract(factor2);
                                    if (!(grossSalary.multiply(new BigDecimal(12)).subtract(excemptionValue).doubleValue() >= minimumTaxValue.doubleValue()
                                            && grossSalary.multiply(new BigDecimal(12)).subtract(excemptionValue).doubleValue() <= maximumTaxValue.doubleValue())) {
                                        index++;
                                        if (index < taxBreaks.size()) {
                                            category = taxBreaks.get(index).getSortIndex();
                                            previousTaxValue = taxBreaks.get(index).getPreviousBreakValue();
                                            factor1 = taxBreaks.get(index).getFactor1();
                                            factor2 = taxBreaks.get(index).getFactor2();
                                            taxPercent = taxBreaks.get(index).getTaxPercent();

                                            grossSalary = factor1.multiply(netSalary);
                                            grossSalary = grossSalary.subtract(factor2);
                                        }
                                    }
                                    //Calculate The Tax Value
                                    //ldc_CalcTaxNetAmount = ((ldc_YearTaxBasicValue - (ldc_MinimumBreakTax*Idc_TaxCalcFactor))* ldc_BreakTaxPercent / 100) + (ldc_PreviousBreakTax*Idc_TaxCalcFactor)
                                    calculatedTax = (((taxableValue.subtract(minimumTaxValue)).multiply(taxPercent)).divide(new BigDecimal(100))).add(previousTaxValue);
                                    calculatedTax = calculatedTax.setScale(3);
                                } else {
                                    grossSalary = netSalary;
                                }
                                //________________________
                                insuranceGroup = employeePayroll.getInsuranceGroup();
                                if (insuranceGroup != null && insuranceGroup.getDbid() != 0) {
                                    conditions.clear();
                                    conditions.add("insuranceGroup.dbid = " + insuranceGroup.getDbid());
                                    insuranceMember = (InsuranceGroupMember) oem.loadEntity("InsuranceGroupMember", conditions, null, loggedUser);
                                    if (insuranceMember != null && insuranceMember.getDbid() != 0) {
                                        insurance = insuranceMember.getInsurance();
                                        if (insurance != null && insurance.getDbid() != 0) {
                                            conditions.clear();
                                            conditions.add("insurance.dbid = " + insurance.getDbid());
                                            conditions.add("basicOrVariable ='Y' ");
                                            insuranceCategory = (InsuranceCategory) oem.loadEntity("InsuranceCategory", conditions, null, loggedUser);
                                            BigDecimal basicIns = BigDecimal.ZERO;
                                            BigDecimal varIns = BigDecimal.ZERO;
                                            if (insuranceCategory != null && insuranceCategory.getDbid() != 0) {
                                                basicIns = (insuranceCategory.getEmployeePercent().multiply(employeePayroll.getBasicBasis())).divide(new BigDecimal(100));
                                            }
                                            //______________________
                                            conditions.clear();
                                            conditions.add("insurance.dbid = " + insurance.getDbid());
                                            conditions.add("basicOrVariable ='N' ");
                                            insuranceCategory = (InsuranceCategory) oem.loadEntity("InsuranceCategory", conditions, null, loggedUser);
                                            if (insuranceCategory != null && insuranceCategory.getDbid() != 0) {
                                                varIns = (insuranceCategory.getEmployeePercent().multiply(employeePayroll.getVariableBasis())).divide(new BigDecimal(100));
                                            }
                                            insuranceValue = basicIns.add(varIns);
                                            grossSalary = grossSalary.add(insuranceValue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return grossSalary;
    }

    @Override
    public OFunctionResult createEmpSEAudit(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeSalaryElement empSE = (EmployeeSalaryElement) odm.getData().get(0);
            Employee emp = empSE.getEmployee();
            SalaryElement salaryElement = empSE.getSalaryElement();

            boolean isCreate = false;
            if (empSE.getDbid() == 0) {
                isCreate = true;
            }

            List<String> conds = new ArrayList<String>();
            conds.add("dbid = " + loggedUser.getDbid());

            // TODO: apply encryption
            String selectOldVal = "select value from employeesalaryelement where dbid = " + empSE.getDbid();
            Object oldValue = oem.executeEntityNativeQuery(selectOldVal, loggedUser);
            String oldValStr = oldValue == null ? "0" : oldValue.toString(),
                    newValStr = null;

            if (oldValStr.toString().equals("0E-13")) {
                oldValStr = BigDecimal.ZERO.toString();
            }

            if ((empSE.getValue() != null && empSE.getValue().toString().equals("0E-13")) || empSE.getValue() == null) {
                newValStr = BigDecimal.ZERO.toString();
            } else {
                newValStr = empSE.getValue().toString();
            }

            if (oldValStr.equals(newValStr)) {
                return ofr;
            }

            AuditTrail auditTrail = new AuditTrail();
            auditTrail.setAuditDateTime(Calendar.getInstance().getTime());
            auditTrail.setUser(loggedUser);
            auditTrail.setEntityKey(emp.getName());

            if (!isCreate) {
                auditTrail.setOldValue(oldValStr);
                auditTrail.setOperationDone("Updating Employee Salary Element value");
            } else {
                auditTrail.setOldValue(null);
                auditTrail.setOperationDone("Inserting Employee Salary Element");
            }

            auditTrail.setNewValue(newValStr);

            auditTrail.setField(salaryElement.getDescriptionTranslated());
            auditTrail.setEntity("EmployeeSalaryElement");
            auditTrail.setScreen("Employee I and D");
            auditTrail.setEmployee(emp);

            oem.saveEntity(auditTrail, loggedUser);
        } catch (Exception exception) {
            OLog.logException(exception, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
            return ofr;
        }
        return ofr;
    }

    /**
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     *
     * This Function Used To Change The Payment Method & Payment Period of The
     * Employee Salary Elements
     * @since 09-07-2014
     */
    @Override
    public OFunctionResult changeESEPayMethod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeSEAssign request = (EmployeeSEAssign) odm.getData().get(0);
            if (request != null) {
                SalaryElement salaryElement = request.getSalaryElement();
                PaymentMethod paymentMethod = request.getPaymentMethod();
                PaymentPeriod paymentPeriod = request.getPaymentPeriod();
                BigDecimal value = request.getValue();
                Currency currency = request.getCurrency();

                if (salaryElement == null || (salaryElement != null && salaryElement.getDbid() == 0)
                        || paymentMethod == null || (paymentMethod != null && paymentMethod.getDbid() == 0)
                        || paymentPeriod == null || (paymentPeriod != null && paymentPeriod.getDbid() == 0)) {

                    ofr.addError(userMessageServiceRemote.getUserMessage("ESEA00001", loggedUser));
                    return ofr;
                }
                //_____________________________Filter____________________________________
                Map<String, Long> filterMap = new HashMap<String, Long>();
                filterMap.put("employee", request.getEmployee() == null ? 0 : request.getEmployee().getDbid());
                filterMap.put("costCenter", request.getCostCenter() == null ? 0 : request.getCostCenter().getDbid());
                filterMap.put("location", request.getLocation() == null ? 0 : request.getLocation().getDbid());
                filterMap.put("hiringType", request.getHiringType() == null ? 0 : request.getHiringType().getDbid());
                filterMap.put("position", request.getPosition() == null ? 0 : request.getPosition().getDbid());
                filterMap.put("unit", request.getUnit() == null ? 0 : request.getUnit().getDbid());
                filterMap.put("job", request.getJob() == null ? 0 : request.getJob().getDbid());
                filterMap.put("grade", request.getPayGrade() == null ? 0 : request.getPayGrade().getDbid());

                String whereSQL;
                whereSQL = FilterUtility.getFilter(filterMap, "employee_dbid");
                if (whereSQL == null) {
                    whereSQL = "";
                }
                //_____________________________Filter____________________________________
                String expSQL;
                List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
                String securityCond = "";
                if (!employeeDbidList.isEmpty()) {
                    String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                    securityCond = " AND employee_dbid in " + employeeDbidStr;
                }
                expSQL = "Update EmployeeSalaryElement "
                        + " set paymentmethod_dbid = " + paymentMethod.getDbid()
                        + " ,paymentperiod_dbid = " + paymentPeriod.getDbid()
                        + " Where salaryelement_dbid = " + salaryElement.getDbid()
                        + whereSQL
                        + securityCond;
                oem.executeEntityUpdateNativeQuery(expSQL, loggedUser);
                //_______________________Update value ________________________________
                if (value != null) {
                    String symmetricKey = dataEncryptorService.getSymmetricKey();
                    String ivKey = dataEncryptorService.getIvKey();

                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        applyEncryption = EncryptionUtility.checkApplyingEncryption(loggedUser);
                        if (applyEncryption) {
                            expSQL = "Update EmployeeSalaryElement "
                                    + " set valueenc =  encrypt(" + value + ",'" + symmetricKey + "','" + ivKey + "')"
                                    + " Where salaryelement_dbid = " + salaryElement.getDbid()
                                    + whereSQL
                                    + securityCond;
                        } else {
                            expSQL = "Update EmployeeSalaryElement "
                                    + " set value = " + value
                                    + " Where salaryelement_dbid = " + salaryElement.getDbid()
                                    + whereSQL
                                    + securityCond;
                        }
                    } else {
                        expSQL = "Update EmployeeSalaryElement "
                                + " set value = " + value
                                + " Where salaryelement_dbid = " + salaryElement.getDbid()
                                + whereSQL
                                + securityCond;
                    }
                    oem.executeEntityUpdateNativeQuery(expSQL, loggedUser);
                }
                //_______________________update Currency______________________________
                if (currency != null && currency.getDbid() != 0) {
                    expSQL = "Update EmployeeSalaryElement "
                            + " set currency_dbid = " + currency.getDbid()
                            + " Where salaryelement_dbid = " + salaryElement.getDbid()
                            + whereSQL
                            + securityCond;
                    oem.executeEntityUpdateNativeQuery(expSQL, loggedUser);
                }
                //Assigning This Element to All Employees

                whereSQL = FilterUtility.getFilter(filterMap, "dbid");
                if (whereSQL == null) {
                    whereSQL = "";
                }
                expSQL = "Select dbid from oemployee where active = 1 "
                        + " AND dbid not in (select Distinct ESE.employee_dbid from EmployeeSalaryElement ESE "
                        + " Where salaryElement_Dbid = " + salaryElement.getDbid() + " )" + whereSQL + securityCond.replace("employee_dbid", "dbid");
                List<Object> emps = oem.executeEntityListNativeQuery(expSQL, loggedUser);
                EmployeeSalaryElement employeeSalaryElement;
                Employee employee;
                if (value == null) {
                    value = BigDecimal.ZERO;
                }

                for (Object emp : emps) {

                    employee = (Employee) oem.loadEntity("Employee", Long.valueOf(emp.toString()), null, null, loggedUser);
                    if (employee != null) {
                        employeeSalaryElement = new EmployeeSalaryElement();
                        employeeSalaryElement.setEmployee(employee);
                        employeeSalaryElement.setPaymentMethod(paymentMethod);
                        employeeSalaryElement.setPaymentPeriod(paymentPeriod);
                        employeeSalaryElement.setSalaryElement(salaryElement);
                        employeeSalaryElement.setSelected(true);
                        //employeeSalaryElement.setValue(BigDecimal.ZERO);
                        employeeSalaryElement.setValue(value);
                        employeeSalaryElement.setMonthly(salaryElement.getMonthly());

                        if (currency != null && currency.getDbid() != 0) {
                            employeeSalaryElement.setCurrency(currency);
                        } else if (employee.getCurrency() == null) {
                            employeeSalaryElement.setCurrency(salaryElement.getCurrency());
                        } else {
                            employeeSalaryElement.setCurrency(employee.getCurrency());
                        }
                        employeeSalaryElement.setSortIndex(salaryElement.getSortIndex());
                        employeeSalaryElement.setSalaryElementType(salaryElement.getEntityDiscrminatorValue());

                        oem.saveEntity(employeeSalaryElement, loggedUser);
                    }
                }
                ofr.addSuccess(userMessageServiceRemote.getUserMessage("ESEA00002", loggedUser));

            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("ESEA00003", loggedUser), ex);
        }
        return ofr;
    }

    /*
     * a java function to run personal closing function
     * and payroll calculation function
     */
    @Override
    public OFunctionResult runPRSClosingAndPayrollCalcProcess(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofrPRSClosing = new OFunctionResult();
        OFunctionResult ofrPayrollCalc = new OFunctionResult();
        ofrPRSClosing = runPersonalClosingWrapperNewTx(odm, functionParms, loggedUser);
        ofrPayrollCalc = runPayrollCalcutaionWrapper(odm, functionParms, loggedUser);
        ofrPRSClosing.append(ofrPayrollCalc);
        return ofrPRSClosing;

    }

    private OFunctionResult generateNewCalculatedPeriod(CalculatedPeriod oldCalculatedPeriod, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            CalculatedPeriod newCalculatedPeriod;
            OFunctionResult oFRUpdate;
            CalculatedPeriod calculatedPeriod;

            calculatedPeriod = (CalculatedPeriod) oem.loadEntity("CalculatedPeriod", oldCalculatedPeriod.getDbid(), null, null, loggedUser);

            int month = calculatedPeriod.getMonth();
            int year = calculatedPeriod.getYear();
            month++;
            if (month == 13) {
                month = 1;
                year++;
            }
            Calendar calendar = new GregorianCalendar();
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);

            // check if the next calc period already exists
            List<String> conds = new ArrayList<String>();
            conds.add("month = " + month);
            conds.add("year = " + year);
            conds.add("payPeriod.dbid = " + calculatedPeriod.getPayPeriod().getDbid());
            conds.add("paymentMethod.dbid = " + calculatedPeriod.getPaymentMethod().getDbid());
            newCalculatedPeriod
                    = (CalculatedPeriod) oem.loadEntity(CalculatedPeriod.class
                            .getSimpleName(), conds, null, loggedUser);

            if (newCalculatedPeriod
                    == null) {
                newCalculatedPeriod = new CalculatedPeriod();
                newCalculatedPeriod.setClosed("N");
                newCalculatedPeriod.setInActive(false);
                newCalculatedPeriod.setDescription("Monthly " + month + " - " + year);
                newCalculatedPeriod.setMonth(month);
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String endDay, endDateStr;
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    endDay = "31";
                } else if (month == 2) {
                    if (year % 4 == 0) {
                        endDay = "29";
                    } else {
                        endDay = "28";
                    }
                } else {
                    endDay = "30";
                }

                endDateStr = endDay + "-" + month + "-" + year;

                Date tempDate = dateFormat.parse(endDateStr);
                newCalculatedPeriod.setEndDate(tempDate);
                tempDate = dateFormat.parse("1-" + month + "-" + year);

                newCalculatedPeriod.setStartDate(tempDate);
                newCalculatedPeriod.setYear(year);
                newCalculatedPeriod.setPayPeriod(calculatedPeriod.getPayPeriod());
                newCalculatedPeriod.setPaymentMethod(calculatedPeriod.getPaymentMethod());
                newCalculatedPeriod.setTaxSettlementEnabled(calculatedPeriod.getTaxSettlementEnabled());
                oFRUpdate = entitySetupService.callEntityCreateAction(newCalculatedPeriod, loggedUser);
                if (oFRUpdate.getErrors().isEmpty()) {
                    oFR.addSuccess(
                            userMessageServiceRemote.getUserMessage("CalculatedPeriodCreated", loggedUser));
                } else {
                    oFR.append(oFRUpdate);
                }
            }

            conds.clear();

            conds.add(
                    "calculatedPeriod.dbid = " + newCalculatedPeriod.getDbid());
            PayrollCalculation payrollCalculation;
            payrollCalculation = (PayrollCalculation) oem.loadEntity("PayrollCalculation", conds, null, loggedUser);
            if (payrollCalculation
                    == null) {
                payrollCalculation = new PayrollCalculation();
                payrollCalculation.setCalculatedPeriod(newCalculatedPeriod);
                // setting option to recalculate
                payrollCalculation.setRecalculateEmployees(true);
                payrollCalculation.setExcludePersonnelClose(false);
                payrollCalculation.setExcludeTerminatedEmployees(false);
                payrollCalculation.setExcludeTimeManagement(false);

                oem.saveEntity(payrollCalculation, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    private boolean validateSalaryUpgradeIsCancelledOrIsApplied(EmployeeSalaryUpgrade salaryUpgrade, OFunctionResult oFR, OUser loggedUser) {
        if (salaryUpgrade == null) {
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
            return false;
        }
        if (salaryUpgrade.getCancelled().equals("Y")) {
            oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade001", loggedUser));
            return false;
        }
        if (salaryUpgrade.getApplied() != null) {
            if (salaryUpgrade.getApplied().equals("Y")) {
                oFR.addError(userMessageServiceRemote.getUserMessage("salaryUpgrade002", loggedUser));
                return false;
            }
        }
        return true;
    }

    private OFunctionResult validatePreviousCalculatedPeriodClosedOrNot(CalculatedPeriod calcPeriod, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            //get the month and year for the calculated period
            int currentMonth = calcPeriod.getMonth();
            int currentYear = calcPeriod.getYear();
            int targetMonth, targetYear;

            //calendar, month field is zero based.
            Calendar calcPeriodCalendar = new GregorianCalendar(currentYear, (currentMonth - 1), 1);
            calcPeriodCalendar.add(Calendar.MONTH, -1);             //get the previous month
            targetMonth = calcPeriodCalendar.get(Calendar.MONTH) + 1;
            targetYear = calcPeriodCalendar.get(Calendar.YEAR);

            //check if the previous month is closed or not
            String query = "SELECT closed FROM calculatedPeriod WHERE code like 'M%' AND month=" + targetMonth + " AND year =" + targetYear;

            String closed = "";
            List<String> resultSet = (List<String>) oem.executeEntityListNativeQuery(query, loggedUser);
            if ((resultSet != null) && (resultSet.size() > 0)) {
                closed = resultSet.get(0);
            } else {
                closed = "NoCalPerFound";
            }
            if ((closed != null) && !("Y".equalsIgnoreCase(closed)) && !("NoCalPerFound".equalsIgnoreCase(closed))) {
                oFR.addError(userMessageServiceRemote.getUserMessage("EXPayCal000001", loggedUser));
                return oFR;
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
            return oFR;
        }
        return oFR;
    }

    @Override
    public OFunctionResult applySEMaintenance(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            DateTimeUtility dateTimeUtility = new DateTimeUtility();
            EmployeeSEMaintenance empSEMaintenance = (EmployeeSEMaintenance) odm.getData().get(0);

            Map<String, Long> filterMap = new HashMap<String, Long>();
            filterMap.put("employee", empSEMaintenance.getEmployee() == null ? 0 : empSEMaintenance.getEmployee().getDbid());
            filterMap.put("costCenter", empSEMaintenance.getCostCenter() == null ? 0 : empSEMaintenance.getCostCenter().getDbid());
            filterMap.put("location", empSEMaintenance.getLocation() == null ? 0 : empSEMaintenance.getLocation().getDbid());
            filterMap.put("hiringType", empSEMaintenance.getHiringType() == null ? 0 : empSEMaintenance.getHiringType().getDbid());
            filterMap.put("position", empSEMaintenance.getPosition() == null ? 0 : empSEMaintenance.getPosition().getDbid());
            filterMap.put("unit", empSEMaintenance.getUnit() == null ? 0 : empSEMaintenance.getUnit().getDbid());
            filterMap.put("job", empSEMaintenance.getJob() == null ? 0 : empSEMaintenance.getJob().getDbid());
            filterMap.put("grade", empSEMaintenance.getPayGrade() == null ? 0 : empSEMaintenance.getPayGrade().getDbid());

            String whereSQL;
            whereSQL = FilterUtility.getFilter(filterMap, "employee_dbid");
            if (whereSQL == null) {
                whereSQL = "";
            }

            Calendar calendar = Calendar.getInstance();
            Date currentDate = calendar.getTime();

            BigDecimal value = empSEMaintenance.getNewValue();
            SalaryElement salaryElement = empSEMaintenance.getSalaryElement();

            String expSQL;

            DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
            Connection connection = datasource.getConnection();

            String securityCond = "";

            if (empSEMaintenance.getEmployee() == null || empSEMaintenance.getEmployee().getDbid() == 0) {
                List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
                securityCond = " AND " + employeeDbidList.toString().replace("[", "(").replace("]", ")");
            }
            // TODO: apply encryption
            if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("oracle")) {
                expSQL = "Update EmployeeSalaryElement "
                        + " set value = " + value
                        + " , lastMaintDTime = to_date('" + dateTimeUtility.DATE_FORMAT.format(currentDate) + "','YYYY-MM-DD')"
                        + " , lastMaintUser_dbid = " + loggedUser.getDbid()
                        + " Where salaryelement_dbid = " + salaryElement.getDbid()
                        + whereSQL;
            } else {
                expSQL = "Update EmployeeSalaryElement "
                        + " set value = " + value
                        + " , lastMaintDTime = '" + dateTimeUtility.DATE_FORMAT.format(currentDate) + "'"
                        + " , lastMaintUser_dbid = " + loggedUser.getDbid()
                        + " Where salaryelement_dbid = " + salaryElement.getDbid()
                        + whereSQL;
            }
            expSQL += securityCond;
            if (oem.executeEntityUpdateNativeQuery(expSQL, loggedUser)) {
                ofr.addSuccess(userMessageServiceRemote.getUserMessage("ESEMaint00002", loggedUser));
            } else {
                ofr.addError(userMessageServiceRemote.getUserMessage("ESEMaint00003", loggedUser));
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult calculateValues(ImportEmployeeSalaryUpgrade impEmpSalaryUpgrade, OUser loggedUser) {
        BigDecimal upgradeFactor = impEmpSalaryUpgrade.getUpgradeFactor() != null ? new BigDecimal(impEmpSalaryUpgrade.getUpgradeFactor()) : null;
        BigDecimal valueAfter = impEmpSalaryUpgrade.getValueAfter() != null ? new BigDecimal(impEmpSalaryUpgrade.getValueAfter()) : null;
        BigDecimal difference = impEmpSalaryUpgrade.getDifference() != null ? new BigDecimal(impEmpSalaryUpgrade.getDifference()) : null;
        BigDecimal valueB4 = new BigDecimal(impEmpSalaryUpgrade.getValueBefore());

        OFunctionResult oFR = new OFunctionResult();

        // calculation
        if (valueB4.toString().equals("0E-13")) {
            valueB4 = new BigDecimal(BigInteger.ZERO);
        }
        if (upgradeFactor != null && valueB4 != null && !valueB4.equals(BigDecimal.ZERO)) {
            if (valueAfter != null || difference != null) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyFactorEntered", loggedUser));
            }
            BigDecimal affPercent = valueB4.multiply(upgradeFactor).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP);
            valueAfter = valueB4.add(affPercent);
            difference = valueAfter.subtract(valueB4);
        } else if (valueAfter != null && valueB4 != null) {
            if (upgradeFactor != null || difference != null) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyValAfterEntered", loggedUser));
            }
            difference = valueAfter.subtract(valueB4);
            if (!valueB4.equals(BigDecimal.ZERO)) {
                upgradeFactor = difference.multiply(new BigDecimal(100)).divide(valueB4, BigDecimal.ROUND_HALF_UP);
            }
        } else if (difference != null && difference != null) {
            if (valueAfter != null || upgradeFactor != null) {
                oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyDiffEntered", loggedUser));
            }
            valueAfter = valueB4.add(difference);
            if (!valueB4.equals(BigDecimal.ZERO)) {
                upgradeFactor = difference.multiply(new BigDecimal(100)).divide(valueB4, BigDecimal.ROUND_HALF_UP);
            }
        }
        impEmpSalaryUpgrade.setDifference(difference.toString());
        impEmpSalaryUpgrade.setValueAfter(valueAfter.toString());
        impEmpSalaryUpgrade.setValueBefore(valueB4.toString());
        impEmpSalaryUpgrade.setUpgradeFactor(upgradeFactor.toString());

        oFR.addReturnValue(impEmpSalaryUpgrade);
        return oFR;
    }

    @Override
    public OFunctionResult netToGrossCalculationPreSave(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            //EmployeeSalaryElement Entity ==> Call As A Validation
            List conditions = new ArrayList();
            if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeSalaryElement")) {
                EmployeeSalaryElement ese = (EmployeeSalaryElement) odm.getData().get(0);
                if (ese != null) {
                    String internalCode;
                    internalCode = ese.getSalaryElement().getInternalCode();
                    if (internalCode != null && ((internalCode.toLowerCase().contains("gross") && internalCode.length() > 5)
                            || (internalCode.toLowerCase().contains("july")))) {
                        //Get Old Value
                        String SQL;
                        BigDecimal oldValue;
                        BigDecimal raiseValue;
                        // TODO: apply encryption
                        String selectSQL = "select valueData from PayrollParameter where description = 'ApplyEncryption'";
                        Object applyEncResult = oem.executeEntityNativeQuery(selectSQL, loggedUser);
                        String applyEnc = applyEncResult == null ? "" : applyEncResult.toString();
                        if (applyEnc.equalsIgnoreCase("yes")) {
                            conditions.clear();
                            conditions.add("dbid = " + ese.getDbid());
                            EmployeeSalaryElement oldEmpSE = (EmployeeSalaryElement) oem.loadEntity(EmployeeSalaryElement.class.getSimpleName(), conditions, null, loggedUser);
                            if (oldEmpSE != null) {
                                oldValue = oldEmpSE.getValue() == null ? BigDecimal.ZERO : oldEmpSE.getValue();
                                raiseValue = ese.getValue().subtract(oldValue);
                            } else {
                                oldValue = ese.getValue();
                                raiseValue = oldValue;
                            }
                        } else {
                            SQL = "Select Value From EmployeeSalaryElement Where dbid = " + ese.getDbid();
                            Object obj = oem.executeEntityNativeQuery(SQL, loggedUser);
                            if (obj != null) {
                                oldValue = new BigDecimal(obj.toString());
                                raiseValue = ese.getValue().subtract(oldValue);
                            } else {
                                oldValue = ese.getValue();
                                raiseValue = oldValue;
                            }
                        }
                        //Get The Element The Should be Updated By Increased/Decreased Value
                        conditions.add("description = 'NewAllowanceRaise'");
                        conditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
                        PayrollParameter payrollParameter;
                        payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conditions, null, loggedUser);
                        if (payrollParameter != null && payrollParameter.getValueData() != null) {
                            // TODO: apply encryption
                            SQL = "Update EmployeeSalaryElement "
                                    + " Set value = " + raiseValue
                                    + " Where employee_dbid = " + ese.getEmployee().getDbid()
                                    + " AND salaryelement_dbid in (select dbid from SalaryElement "
                                    + " where code = '" + payrollParameter.getValueData() + "')";
                            oem.executeEntityUpdateNativeQuery(SQL, loggedUser);
                        }
                    }
                }
            } //EmployeePayroll Entity ==> Call As A Post Action
            else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeePayroll")) {
                EmployeePayroll employeePayroll = (EmployeePayroll) odm.getData().get(0);
                if (employeePayroll != null) {
                    //Should be Called Only when The Insurance Data has been changed
                    //Now We Have A problem in it.
                    EmployeeSalaryElement netESE;
                    conditions.clear();
                    conditions.add("employee.dbid = " + employeePayroll.getEmployee().getDbid());
                    conditions.add("salaryElement.internalCode = 'net'");
                    netESE = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                    if (netESE != null) {
                        entitySetupService.callEntityUpdateAction(netESE, loggedUser);
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return oFR;
    }

    @Override
    public OFunctionResult importEmployeeSalaryElementHistory(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String expSQL;
            List<Object[]> importESE;
            Long employeeDbid, salElemDbid, importDbid, cpDbid;
            String valueEnc = null;
            String value;
            String type;
            expSQL = "select AA.dbid,BB.dbid,CC.dbid,AA.employeeCode,AA.salaryElementCode,"
                    + " AA.salaryElementValueEnc,AA.elementType, DD.dbid, AA.salaryElementValue"
                    + " from ImportEmployeeSalaryElementH AA, Oemployee BB , SalaryElement CC, CalculatedPeriod DD"
                    + " where AA.done = 0  and "
                    + " AA.employeeCode = BB.code and "
                    + " AA.salaryElementCode = CC.code and "
                    + " lower(left(AA.elementType,1)) = lower(left(CC.stype,1)) and"
                    + " AA.cpcode = DD.code";
            importESE = oem.executeEntityListNativeQuery(expSQL, loggedUser);
            for (Object[] object : importESE) {
                importDbid = Long.parseLong(object[0].toString());
                employeeDbid = Long.parseLong(object[1].toString());
                salElemDbid = Long.parseLong(object[2].toString());
                if (object[5] != null) {
                    valueEnc = object[5].toString();
                }
                cpDbid = Long.parseLong(object[7].toString());
                value = object[8].toString();
                if (value.trim().length() <= 0) {
                    value = "0";
                }
                type = object[6].toString();

                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    applyEncryption = EncryptionUtility.checkApplyingEncryption(loggedUser);
                    if (applyEncryption) {
                        expSQL = "Update EMP_SAL_ELEMENTS_HISTORY "
                                + " Set element_valueenc = '" + valueEnc + "' "
                                + " , element_value = " + value
                                + " , monthly_element_value = 0"
                                + " Where employee = '" + employeeDbid + "'"
                                + " and salary_element_id = '" + salElemDbid + "'"
                                + " and calculated_period_id = '" + cpDbid + "'"
                                + " and income_or_deduction = '" + type + "'";
                    } else {
                        expSQL = "Update EMP_SAL_ELEMENTS_HISTORY "
                                + " Set element_value = " + value
                                + " , monthly_element_value = 0"
                                + " Where employee = '" + employeeDbid + "'"
                                + " and salary_element_id = '" + salElemDbid + "'"
                                + " and calculated_period_id = '" + cpDbid + "'"
                                + " and income_or_deduction = '" + type + "'";
                    }
                } else {
                    expSQL = "Update EMP_SAL_ELEMENTS_HISTORY "
                            + " Set element_value = " + value
                            + " , monthly_element_value = 0"
                            + " Where employee = '" + employeeDbid + "'"
                            + " and salary_element_id = '" + salElemDbid + "'"
                            + " and calculated_period_id = '" + cpDbid + "'"
                            + " and income_or_deduction = '" + type + "'";
                }
                oem.executeEntityUpdateNativeQuery(expSQL, loggedUser);

                expSQL = "update ImportEmployeeSalaryElementH set done = 1 where dbid = " + importDbid;
                oem.executeEntityUpdateNativeQuery(expSQL, loggedUser);
                ofr.addSuccess(userMessageServiceRemote.getUserMessage("UESEH-S001", loggedUser));
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("UESEH-E001", loggedUser));
        }
        return ofr;
    }

    private OFunctionResult closeRelatedCalculatedPeriod(CalculatedPeriod calculatedPeriod, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List conditions = new ArrayList();
            List<CalculatedPeriod> calculatedPeriods;

            conditions.add("month = " + calculatedPeriod.getMonth());
            conditions.add("year = " + calculatedPeriod.getYear());
            conditions.add("closed = 'N' ");
            conditions.add("paymentMethod.relatedPayMethod.dbid = " + calculatedPeriod.getPaymentMethod().getDbid());
            conditions.add("paymentMethod.relatedPayPeriod.dbid = " + calculatedPeriod.getPayPeriod().getDbid());

            calculatedPeriods = oem.loadEntityList("CalculatedPeriod", conditions, null, null, loggedUser);
            if (!calculatedPeriods.isEmpty()) {
                for (CalculatedPeriod cp : calculatedPeriods) {
                    cp.setClosed("Y");
                    oem.saveEntity(cp, loggedUser);
                    if (cp.getPaymentMethod().getUnit().equals("S")) {
                        closeRelatedCalculatedPeriod(cp, loggedUser);
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    @Override
    public OFunctionResult generateRelatedCalculatedPeriods(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            CalculatedPeriod calculatedPeriod = (CalculatedPeriod) odm.getData().get(0);
            if (calculatedPeriod != null) {
                PaymentMethod paymentMethod;
                paymentMethod = calculatedPeriod.getPaymentMethod();

                PaymentPeriod paymentPeriod;
                paymentPeriod = calculatedPeriod.getPayPeriod();

                if (paymentMethod != null && paymentPeriod != null && paymentMethod.getUnit().equals("M")) {
                    int month = calculatedPeriod.getMonth();
                    int year = calculatedPeriod.getYear();
                    Date startDate = calculatedPeriod.getStartDate();
                    Date endDate = calculatedPeriod.getEndDate();
                    ofr = generateRelatedCalculatedPeriods(paymentMethod, paymentPeriod, month, year, startDate, endDate, null, loggedUser);
                }
            }

        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("GCP-E001", loggedUser));
        }
        return ofr;
    }

    private OFunctionResult generateRelatedCalculatedPeriods(PaymentMethod paymentMethod, PaymentPeriod paymentPeriod, int month, int year,
            Date startDate, Date endDate, String code, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<PaymentMethod> methods;
            List Conditions = new ArrayList();
            if (paymentMethod != null && paymentPeriod != null) {
                Conditions.add("relatedPayMethod.dbid = " + paymentMethod.getDbid());
                Conditions.add("relatedPayPeriod.dbid = " + paymentPeriod.getDbid());
                methods = oem.loadEntityList("PaymentMethod", Conditions, null, null, loggedUser);
                if (!methods.isEmpty()) {
                    for (PaymentMethod method : methods) {
                        ofr = generateCalculatedPeriod(method, month, year, startDate, endDate, code, loggedUser);
                        if (method.getUnit().equals("S")) {
                            CalculatedPeriod calculatedPeriod = (CalculatedPeriod) ofr.getReturnValues().get(0);
                            String cpCode;
                            cpCode = "R".concat(calculatedPeriod.getCode());
                            generateRelatedCalculatedPeriods(method, method.getPeriods().get(0), month, year, startDate, endDate, cpCode, loggedUser);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult generateCalculatedPeriod(PaymentMethod method, int month, int year, Date startDate, Date endDate, String Code, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            CalculatedPeriod calculatedPeriod = new CalculatedPeriod();
            calculatedPeriod.setMonth(month);
            calculatedPeriod.setYear(year);
            calculatedPeriod.setClosed("N");
            calculatedPeriod.setInActive(false);
            calculatedPeriod.setDescription(method.getDescription() + " " + month + " - " + year);
            calculatedPeriod.setEndDate(endDate);
            calculatedPeriod.setStartDate(startDate);
            calculatedPeriod.setPayPeriod(method.getPeriods().get(0));
            calculatedPeriod.setPaymentMethod(method);
            calculatedPeriod.setTaxSettlementEnabled("N");
            calculatedPeriod.setCode(Code);
            ofr = entitySetupService.callEntityCreateAction(calculatedPeriod, loggedUser);
            List returnValues = new ArrayList();
            returnValues.add(calculatedPeriod);
            ofr.setReturnValues(returnValues);

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult resetVariableSalaryElements(CalculatedPeriod calculatedPeriod, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {

            if (calculatedPeriod != null) {

                DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                Connection connection = datasource.getConnection();
                String procedure = "{call reSetVariableSalaryElements()}";
                CallableStatement callableStatement = connection.prepareCall(procedure);

                callableStatement.executeUpdate();
                callableStatement.close();
                connection.close();

            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    private OFunctionResult applySalaryUpgrade(EmployeeSalaryUpgrade salaryUpgrade, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            DateTimeUtility dateTimeUtility = new DateTimeUtility();
            List conditions = new ArrayList();
            conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
            conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
            EmployeeSalaryElement ese = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
            if (ese != null) {
                ese.setValue(salaryUpgrade.getValueAfter());
                oem.saveEntity(ese, loggedUser);
            }
            if (salaryUpgrade.getFromCalculatedPeriod() != null
                    && salaryUpgrade.getToCalculatedPeriod() != null) {
                //________________________________
                String sql = "SELECT VALUEDATA FROM payrollparameter WHERE DESCRIPTION like 'ApplyEncryption'";
                Object encryptEnabled = oem.executeEntityNativeQuery(sql, loggedUser);
                String databaseConnection = EntityUtilities.getDBConnectionString(loggedUser).toLowerCase();
                boolean isMSSQL = true;
                if (databaseConnection.toLowerCase().contains("oracle")) {
                    isMSSQL = false;
                }
                if (encryptEnabled != null && encryptEnabled.toString().equals("Yes")) {
                    String symmetricKey = dataEncryptorService.getSymmetricKey();
                    String ivKey = dataEncryptorService.getIvKey();
                    if (isMSSQL) {
                        sql = "Update Emp_Sal_Elements_History "
                                + " Set Element_ValueEnc = dbo.encrypt('" + salaryUpgrade.getValueAfter().setScale(3, RoundingMode.UP) + "','" + symmetricKey + "','" + ivKey + "')"
                                + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                                + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                                + " and calculated_period_id in "
                                + " (select calculated_period_id from calculated_period,payment_method "
                                + " where  cast(start_date as date) >= cast('"
                                + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "' as date)"
                                + " AND cast(start_date as date) <= cast('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "' as date)"
                                + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                                + " AND payment_method.payment_unit_id = 'M' )";

                    } else {
                        sql = "Update Emp_Sal_Elements_History "
                                + " Set Element_ValueEnc = encrypt(" + salaryUpgrade.getValueAfter().setScale(3, RoundingMode.UP) + ",'" + symmetricKey + "'," + ivKey + ")"
                                + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                                + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                                + " and calculated_period_id in "
                                + " (select calculated_period_id from calculated_period,payment_method "
                                + " where  to_date(start_date) >= to_date('"
                                + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "','YYYY-MM-dd')"
                                + " AND to_date(start_date) <= to_date('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "','YYYY-MM-dd')"
                                + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                                + " AND payment_method.payment_unit_id = 'M' )";
                    }
                } else if (isMSSQL) {
                    sql = "Update Emp_Sal_Elements_History "
                            + " Set Element_Value = " + salaryUpgrade.getValueAfter().setScale(3, RoundingMode.UP)
                            + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                            + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                            + " and calculated_period_id in "
                            + " (select calculated_period_id from calculated_period,payment_method "
                            + " where start_date >='" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "'"
                            + " AND start_date <='" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "'"
                            + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                            + " AND payment_method.payment_unit_id = 'M' )";
                } else {
                    sql = "Update Emp_Sal_Elements_History "
                            + " Set Element_Value = " + salaryUpgrade.getValueAfter().setScale(3, RoundingMode.UP)
                            + " Where employee = '" + salaryUpgrade.getEmployee().getDbid() + "'"
                            + " AND salary_element_id = '" + salaryUpgrade.getIncome().getDbid() + "'"
                            + " and calculated_period_id in "
                            + " (select calculated_period_id from calculated_period,payment_method "
                            + " where (start_date) >=to_date('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getFromCalculatedPeriod().getStartDate()) + "', 'YYYY-MM-dd')"
                            + " AND (start_date) <=to_date('" + dateTimeUtility.DATE_FORMAT.format(salaryUpgrade.getToCalculatedPeriod().getEndDate()) + "', 'YYYY-MM-dd')"
                            + " AND calculated_period.payment_method_id = payment_method.payment_method_id "
                            + " AND payment_method.payment_unit_id = 'M' )";
                }
                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                //________________________________
            }
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("salaryUpgrade003", loggedUser));
            //___________________Apply Net To Gross Business________________________________
            String internalCode;
            internalCode = salaryUpgrade.getIncome().getInternalCode();
            if (internalCode != null && (internalCode.toLowerCase().contains("gross")
                    || internalCode.toLowerCase().contains("net")
                    || internalCode.toLowerCase().contains("july"))) {
                conditions.clear();
                conditions.add("employee.dbid = " + salaryUpgrade.getEmployee().getDbid());
                conditions.add("salaryElement.dbid = " + salaryUpgrade.getIncome().getDbid());
                EmployeeSalaryElement ESE = (EmployeeSalaryElement) oem.loadEntity("EmployeeSalaryElement", conditions, null, loggedUser);
                if (ESE != null) {
                    entitySetupService.callEntityUpdateAction(ESE, loggedUser);
                }
            }
            //___________________Apply Net To Gross Business________________________________
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return oFR;
    }

    private OFunctionResult fillSalaryUpgradeValues(String entityName, Object entity, OUser loggedUser, ODataMessage odm) {
        OFunctionResult oFR = new OFunctionResult();
        BigDecimal valueB4 = null,
                upgradeFactor = null,
                valueAfter = null,
                difference = null;
        String fixround = null;
        Date applyDate = null;
        try {
            if (entityName.equals("EmployeeSalaryUpgrade")) {
                EmployeeSalaryUpgrade empSalaryUpgrade = (EmployeeSalaryUpgrade) entity;
                List<String> conds = new ArrayList<String>();
                conds.add("salaryElement.dbid = " + empSalaryUpgrade.getIncome().getDbid());
                conds.add("employee.dbid = " + empSalaryUpgrade.getEmployee().getDbid());

                EmployeeSalaryElement empSalaryElement = (EmployeeSalaryElement) oem.loadEntity(
                        EmployeeSalaryElement.class
                        .getSimpleName(), conds, null, loggedUser);

                valueB4 = empSalaryElement.getValue();

                if (empSalaryUpgrade.getApplyDate()
                        == null) {
                    applyDate = empSalaryUpgrade.getFromCalculatedPeriod().getStartDate();
                } else {
                    applyDate = empSalaryUpgrade.getApplyDate();
                }

                if (odm
                        != null && odm.getODataType()
                        .getName().equals("EntityChangeField")) {
                    String fldExp = odm.getData().get(1).toString();
                    Object newVal = odm.getData().get(2);
                    upgradeFactor = fldExp.startsWith("upgradeFactor") ? new BigDecimal(newVal.toString()) : null;
                    valueAfter = fldExp.startsWith("valueAfter") ? new BigDecimal(newVal.toString()) : null;
                    difference = fldExp.startsWith("difference") ? new BigDecimal(newVal.toString()) : null;
                } else {
                    upgradeFactor = empSalaryUpgrade.getUpgradeFactor();
                    valueAfter = empSalaryUpgrade.getValueAfter();
                    difference = empSalaryUpgrade.getDifference();
                }
            } else if (entityName.equals("ImportEmployeeSalaryUpgrade")) {
                ImportEmployeeSalaryUpgrade impEmpSalaryUpgrade = (ImportEmployeeSalaryUpgrade) entity;
                List<String> conds = new ArrayList<>();
                conds.add("employee.code = '" + impEmpSalaryUpgrade.getEmployee() + "'");
                conds.add("salaryElement.code = '" + impEmpSalaryUpgrade.getIncome() + "'");
                conds.add("salaryElement.type.incomeOrDeduction = 'I'");

                EmployeeSalaryElement empSE = (EmployeeSalaryElement) oem.loadEntity(EmployeeSalaryElement.class
                        .getSimpleName(), conds, null, loggedUser);

                if (empSE
                        == null) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("NoEmpSalElementExists", loggedUser));
                    return oFR;
                }

                if (odm
                        != null && odm.getODataType()
                        .getName().equals("EntityChangeField")) {
                    String fldExp = odm.getData().get(1).toString();
                    Object newVal = odm.getData().get(2);
                    upgradeFactor = fldExp.startsWith("upgradeFactor") ? new BigDecimal(newVal.toString()) : null;
                    valueAfter = fldExp.startsWith("valueAfter") ? new BigDecimal(newVal.toString()) : null;
                    difference = fldExp.startsWith("difference") ? new BigDecimal(newVal.toString()) : null;
                } else {
                    upgradeFactor = impEmpSalaryUpgrade.getUpgradeFactor() != null ? new BigDecimal(impEmpSalaryUpgrade.getUpgradeFactor()) : null;
                    valueAfter = impEmpSalaryUpgrade.getValueAfter() != null ? new BigDecimal(impEmpSalaryUpgrade.getValueAfter()) : null;
                    difference = impEmpSalaryUpgrade.getDifference() != null ? new BigDecimal(impEmpSalaryUpgrade.getDifference()) : null;
                }

                valueB4 = empSE.getValue();

                impEmpSalaryUpgrade.setValueBefore(valueB4.toString());
            }

            // calculation
            if (valueB4 != null && valueB4.compareTo(BigDecimal.ZERO) == 0) {
                valueB4 = new BigDecimal(BigInteger.ZERO);
            }
            if (upgradeFactor != null && valueB4 != null && !valueB4.equals(BigDecimal.ZERO)) {
                fixround = valueB4.multiply(upgradeFactor).toString();
                BigDecimal affPercent = new BigDecimal(fixround).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP);
                valueAfter = valueB4.add(affPercent);
                difference = valueAfter.subtract(valueB4);
                //if (valueAfter != null || difference != null) {
                //    oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyFactorEntered", loggedUser));
                //}
            } else if (valueAfter != null && valueB4 != null) {
                if (upgradeFactor != null || difference != null) {
                    oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyValAfterEntered", loggedUser));
                }
                difference = valueAfter.subtract(valueB4);
                //edited by Osama Samy 16-05-2017 i set difference equal zero if the diff
                if (difference.toString().contains("E-")) {
                    difference = new BigDecimal(BigInteger.ZERO);

                    oFR.addWarning(userMessageServiceRemote.getUserMessage("Salary Upgrade of some employees has not been changed.", loggedUser));
                }
                if (!valueB4.equals(BigDecimal.ZERO)) {
                    fixround = difference.multiply(new BigDecimal(100)).toString();
                    upgradeFactor = new BigDecimal(fixround).divide(valueB4, BigDecimal.ROUND_HALF_UP);
                }
            } else if (difference != null && valueB4 != null) {
                if (valueAfter != null || upgradeFactor != null) {
                    oFR.addWarning(userMessageServiceRemote.getUserMessage("NotOnlyDiffEntered", loggedUser));
                }
                valueAfter = valueB4.add(difference);
                if (!valueB4.equals(BigDecimal.ZERO)) {
                    fixround = difference.multiply(new BigDecimal(100)).toString();
                    upgradeFactor = new BigDecimal(fixround).divide(valueB4, BigDecimal.ROUND_HALF_UP);
                }
            }

            // setting values in the entities
            switch (entityName) {
                case "EmployeeSalaryUpgrade":
                    EmployeeSalaryUpgrade empSalaryUpgrade = (EmployeeSalaryUpgrade) entity;
                    empSalaryUpgrade.setUpgradeFactor(upgradeFactor);
                    empSalaryUpgrade.setValueAfter(valueAfter);
                    empSalaryUpgrade.setDifference(difference);
                    empSalaryUpgrade.setValueBefore(valueB4);
                    empSalaryUpgrade.setApplyDate(applyDate);
                    break;
                case "ImportEmployeeSalaryUpgrade":
                    ImportEmployeeSalaryUpgrade impEmpSalaryUpgrade = (ImportEmployeeSalaryUpgrade) entity;
                    if (difference != null) {
                        impEmpSalaryUpgrade.setDifference(difference.toString());
                    }
                    if (upgradeFactor != null) {
                        impEmpSalaryUpgrade.setUpgradeFactor(upgradeFactor.toString());
                    }
                    if (valueAfter != null) {
                        impEmpSalaryUpgrade.setValueAfter(valueAfter.toString());
                    }
                    break;
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        oFR.addReturnValue(entity);
        return oFR;
    }
}
