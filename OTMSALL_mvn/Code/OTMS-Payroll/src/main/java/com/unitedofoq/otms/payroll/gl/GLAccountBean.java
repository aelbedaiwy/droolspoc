package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeePayroll;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.payroll.gl.GLAccountLocal",
        beanInterface = GLAccountLocal.class)
public class GLAccountBean implements GLAccountLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;

    @Override
    public OFunctionResult insertGLMappedAccount(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<String> conds = new ArrayList<String>();
            conds.add("dbid = " + loggedUser.getUserPrefrence1());
            Company company = (Company) oem.loadEntity(Company.class.getSimpleName(), conds, null, loggedUser);

            List mappedAccounts;
            long companyDBID = company.getDbid();
            String separator = null;
            List segmentDbids;

            //delete glmappedaccount table
            oem.executeEntityUpdateNativeQuery("delete from GLMappedAccount", loggedUser);

            //select all segmentDbids
            segmentDbids = oem.executeEntityListNativeQuery("select referencevalue_dbid " + ",constantvalue "
                    + "from GLAccountSegment where deleted = 0 order by segmentNumber", loggedUser);

            //select separator
            separator = oem.executeEntityNativeQuery("select description from systemlookupdetail where "
                    + "deleted = 0 and code = 'Separator' and SYSTEMLOOKUP_DBID in "
                    + "(select dbid from systemlookup where code = 'GLParameters')", loggedUser).toString();


            StringBuilder mappedAccInsert = new StringBuilder("insert into GLMappedAccount("),
                    mappedAccInsertValues = new StringBuilder(" values ");

            StringBuilder selectStat = new StringBuilder("select "),
                    fromStat = new StringBuilder(" from "),
                    whereStat = new StringBuilder(" where 1=1");

            Map<Integer, String> segments = new HashMap<Integer, String>();
            int segCounter = 0;
            int numMappedSeg = 0;

            for (int i = 0; i < segmentDbids.size(); i++) {
                Object[] segmentValues = (Object[]) segmentDbids.get(i);

                if (segmentValues[0] != null) {
                    String udcDbid = segmentValues[0].toString();
                    Object selectRefVal = oem.executeEntityNativeQuery("select udc.code from udc where dbid = " + udcDbid, loggedUser);

                    if (selectRefVal.equals("Unit")) {
                        selectStat.append("unit.dbid,");
                        fromStat.append("unit,");
                        whereStat.append(" and unit.company_dbid = " + companyDBID + " and unit.glaccount !='' AND  "
                                + "unit.glAccount is not null and unit.deleted = 0");

                        mappedAccInsert.append("unit_dbid,");
                        segments.put(segCounter, "unit");
                        segCounter++;
                        numMappedSeg++;
                    } else if (selectRefVal.equals("Employee")) {
                        selectStat.append("employeepayroll.employee_dbid,");
                        fromStat.append("employeepayroll,");
                        whereStat.append(" and employeepayroll.deleted = 0 and employeepayroll.glaccount !='' and "
                                + "employeepayroll.glaccount is not null and employeepayroll.employee_dbid in "
                                + "(select dbid from oemployee where position_dbid in "
                                + "(select dbid from position where unit_dbid in "
                                + "(select dbid from unit where company_dbid = " + companyDBID + ")))");

                        mappedAccInsert.append("employee_dbid,");
                        segments.put(segCounter, "employee");
                        segCounter++;
                    } else if (selectRefVal.equals("SalaryElement")) {
                        selectStat.append("salaryelement.dbid,");
                        fromStat.append("salaryelement,");
                        whereStat.append(" and salaryelement.company_dbid = ").append(companyDBID).append(" and (salaryelement.generalCreditAccount = 1 or "
                                + "salaryelement.generalDebitAccount = 1 or "
                                + "salaryelement.debitAccount is not null or "
                                + "salaryelement.creditAccount is not null) and salaryelement.deleted = 0");

                        mappedAccInsert.append("salaryelement_dbid,");
                        segments.put(segCounter, "salaryElement");
                        segCounter++;
                        numMappedSeg++;
                    } else if (selectRefVal.equals("CostCenter")) {
                        selectStat.append("costcenter.dbid,");
                        fromStat.append("costcenter,");
                        whereStat.append(" and costcenter.company_dbid = ").append(companyDBID).append(" and costcenter.glaccount !='' AND"
                                + "  costcenter.glAccount is not null and costcenter.deleted = 0");

                        mappedAccInsert.append("costcenter_dbid,");
                        segments.put(segCounter, "costCenter");
                        segCounter++;
                        numMappedSeg++;
                    } else if (selectRefVal.equals("PayGrade")) {
                        selectStat.append("paygrade.dbid,");
                        fromStat.append("paygrade,");
                        whereStat.append(" and paygrade.company_dbid = ").append(companyDBID).append(
                                " and paygrade.glaccount !='' AND paygrade.glAccount is not null and "
                                + "paygrade.deleted = 0");

                        mappedAccInsert.append("grade_dbid,");
                        segments.put(segCounter, "grade");
                        segCounter++;
                        numMappedSeg++;
                    } else if (selectRefVal.equals("LegalEntity")) {
                        selectStat.append("legalentity.dbid,");
                        fromStat.append("legalentity,");
                        whereStat.append(" and legalentity.company_dbid = ").append(companyDBID).append(
                                " and legalentity.glaccount !='' AND  "
                                + "legalentity.glAccount is not null and legalentity.deleted = 0");

                        mappedAccInsert.append("legalEntity_dbid,");
                        segments.put(segCounter, "legalEntity");
                        segCounter++;
                        numMappedSeg++;
                    } else if (selectRefVal.equals("Nationality")) {
                        selectStat.append("nat.businessudc_dbid ,");
                        fromStat.append("glaccountudc nat,");
                        whereStat.append(" and nat.businessudc_dbid in(select dbid from businessudc "
                                + "where parentcode = 'prs_013')");

                        mappedAccInsert.append("nationality_dbid,");
                        segments.put(segCounter, "nationality");
                        segCounter++;
                        numMappedSeg++;
                    } else if (selectRefVal.equals("HiringType")) {
                        selectStat.append("HiringType.businessudc_dbid,");
                        fromStat.append("glaccountudc HiringType,");
                        whereStat.append(" and HiringType.businessudc_dbid in(select dbid from businessudc "
                                + "where parentcode = 'prs_001') ");

                        mappedAccInsert.append("hiringType_dbid,");
                        segments.put(segCounter, "hiringType");
                        segCounter++;
                        numMappedSeg++;
                    } else if (selectRefVal.equals("Location")) {
                        selectStat.append("Loc.businessudc_dbid,");
                        fromStat.append("glaccountudc Loc,");
                        whereStat.append(" and Loc.businessudc_dbid in(select dbid from businessudc "
                                + "where parentcode = 'prs_loc')");

                        mappedAccInsert.append("location_dbid,");
                        segments.put(segCounter, "location");
                        segCounter++;
                        numMappedSeg++;
                    }
                } else if (segmentValues[1] != null) {
                    String constantValue = segmentValues[1].toString();
                    if (constantValue != null && !constantValue.equals("")) {
                        segments.put(segCounter, "constVal_" + constantValue);
                        segCounter++;
                    }
                }
            }

            selectStat.replace(selectStat.length() - 1, selectStat.length(), "");
            fromStat.replace(fromStat.length() - 1, fromStat.length(), "");

            String sqlSelectStat = (selectStat.append(fromStat).append(whereStat)).toString();

            mappedAccounts = oem.executeEntityListNativeQuery(sqlSelectStat, loggedUser);

            mappedAccInsert.replace(mappedAccInsert.length() - 1, mappedAccInsert.length(), ",company_dbid)");

            String currentMappedRec = null;
            Object[] mappedRecord;
            Long x;
            for (int i = 0; i < mappedAccounts.size(); i++) {
                mappedRecord = null;
                x = Long.valueOf("0");
                if (numMappedSeg == 1) {
                    x = (Long) mappedAccounts.get(i);
                    currentMappedRec = x.toString();
                } else if (numMappedSeg > 1) {
                    mappedRecord = (Object[]) mappedAccounts.get(i);
                    currentMappedRec = mappedRecord[0].toString();
                }

                mappedAccInsertValues.append("(").append(currentMappedRec);
                for (int j = 1; mappedRecord != null && j < mappedRecord.length; j++) {
                    mappedAccInsertValues.append(",").append(mappedRecord[j]);
                }
                mappedAccInsertValues.append("," + loggedUser.getUserPrefrence1() + "),");
            }
            mappedAccInsertValues.replace(mappedAccInsertValues.length() - 1, mappedAccInsertValues.length(), "");
            String SQL;
            SQL = mappedAccInsert.toString() + mappedAccInsertValues.toString();

            boolean updated = oem.executeEntityUpdateNativeQuery(SQL, loggedUser);
            if (updated) {
                ofr.addSuccess(userMessageServiceRemote.getUserMessage("RecordsInserted", loggedUser));
            } else {
                ofr.addError(userMessageServiceRemote.getUserMessage("RecordsNotInserted", loggedUser));
            }

            String selectSalaryElemets = "select distinct salaryelement_dbid from glmappedaccount";
            List<Long> salaryElemetsDbids = (List<Long>) oem.executeEntityListNativeQuery(selectSalaryElemets, loggedUser);

            //_______________________________
            class SalaryElementAcc {

                String crAccount;
                String dbAccount;
                boolean crSingleVal;
                boolean dbSingleVal;

                public String getCrAccount() {
                    return crAccount;
                }

                public void setCrAccount(String crAccount) {
                    this.crAccount = crAccount;
                }

                public String getDbAccount() {
                    return dbAccount;
                }

                public void setDbAccount(String dbAccount) {
                    this.dbAccount = dbAccount;
                }

                public boolean isCrSingleVal() {
                    return crSingleVal;
                }

                public void setCrSingleVal(boolean crSingleVal) {
                    this.crSingleVal = crSingleVal;
                }

                public boolean isDbSingleVal() {
                    return dbSingleVal;
                }

                public void setDbSingleVal(boolean dbSingleVal) {
                    this.dbSingleVal = dbSingleVal;
                }
            }

            if (salaryElemetsDbids != null) {
                Map<Long, SalaryElementAcc> salaryElementAccount = new HashMap<Long, SalaryElementAcc>();
                for (int i = 0; i < salaryElemetsDbids.size(); i++) {
                    String crAccount = "";
                    String dbAccount = "";
                    boolean crSingleVal = false;
                    boolean dbSingleVal = false;

                    SalaryElement salaryElement = (SalaryElement) oem.loadEntity(SalaryElement.class.getSimpleName(),
                            salaryElemetsDbids.get(i), null, null, loggedUser);

                    if (salaryElement != null) {
                        if (salaryElement.isGeneralCreditAccount()) {
                            if (salaryElement.getCreditAccount() != null
                                    && !salaryElement.getCreditAccount().equals("")) {
                                crAccount = salaryElement.getCreditAccount();
                                crSingleVal = true;
                            } else {
                                crAccount = "L";
                                crSingleVal = true;
                            }
                        } else {
                            if (salaryElement.getCreditAccount() != null
                                    && !salaryElement.getCreditAccount().equals("")) {
                                crAccount = salaryElement.getCreditAccount();
                            } else {
                                crAccount = "E";
                                crSingleVal = true;
                            }
                        }

                        // debit
                        if (salaryElement.isGeneralDebitAccount()) {
                            if (salaryElement.getDebitAccount() != null
                                    && !salaryElement.getDebitAccount().equals("")) {
                                dbAccount = salaryElement.getDebitAccount();
                                dbSingleVal = true;
                            } else {
                                dbAccount = "L";
                                dbSingleVal = true;
                            }
                        } else {
                            if (salaryElement.getDebitAccount() != null
                                    && !salaryElement.getDebitAccount().equals("")) {
                                dbAccount += salaryElement.getDebitAccount();
                            } else {
                                dbAccount = "E";
                                dbSingleVal = true;
                            }
                        }

                        SalaryElementAcc seAcc = new SalaryElementAcc();
                        seAcc.setCrAccount(crAccount);
                        seAcc.setDbAccount(dbAccount);
                        seAcc.setCrSingleVal(crSingleVal);
                        seAcc.setDbSingleVal(dbSingleVal);

                        salaryElementAccount.put(salaryElemetsDbids.get(i), seAcc);
                    }
                }

                conds.clear();
                List<GLMappedAccount> accounts = oem.loadEntityList(
                        GLMappedAccount.class.getSimpleName(), null, null, null, loggedUser);
                for (int i = 0; i < accounts.size(); i++) {
                    String cGlAccount = "";
                    String dGlAccount = "";
                    SalaryElementAcc seAcc = salaryElementAccount.get(accounts.get(i).getSalaryElement().getDbid());

                    if (seAcc != null) {
                        //credit
                        if (seAcc.isCrSingleVal()) {
                            cGlAccount = seAcc.getCrAccount();
                        } else {
                            for (int j = 0; j < segments.size(); j++) {
                                String currentSegmentField = segments.get(j);
                                if (currentSegmentField.startsWith("constVal_")) {
                                    cGlAccount += currentSegmentField.substring("constVal_".length());
                                } else {
                                    Object currentSegment = BaseEntity.getValueFromEntity(accounts.get(i), currentSegmentField);
                                    if (currentSegment instanceof Unit) {
                                        cGlAccount += ((Unit) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof Employee) {
                                        conds.clear();
                                        conds.add("employee.dbid = " + ((Employee) currentSegment).getDbid());
                                        EmployeePayroll employeePayroll = (EmployeePayroll) oem.loadEntity(
                                                EmployeePayroll.class.getSimpleName(), conds, null, loggedUser);
                                        if (employeePayroll != null) {
                                            cGlAccount += employeePayroll.getGlAccount();
                                        }
                                    } else if (currentSegment instanceof CostCenter) {
                                        cGlAccount += ((CostCenter) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof PayGrade) {
                                        cGlAccount += ((PayGrade) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof LegalEntity) {
                                        cGlAccount += ((LegalEntity) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof SalaryElement) {
                                        cGlAccount += ((SalaryElement) currentSegment).getCreditAccount();
                                    } else if (currentSegmentField.equals("nationality")) {
                                        List<String> udcConds = new ArrayList<String>();
                                        udcConds.add("businessUdc.dbid = " + ((UDC) currentSegment).getDbid());
                                        GLAccountUDC accountUDC = (GLAccountUDC) oem.loadEntity("GLAccountUDC", udcConds, null, loggedUser);
                                        if (accountUDC != null) {
                                            cGlAccount += accountUDC.getGlAccount();
                                        }
                                    } else if (currentSegmentField.equals("hiringType")) {
                                        List<String> udcConds = new ArrayList<String>();
                                        udcConds.add("businessUdc.dbid = " + ((UDC) currentSegment).getDbid());
                                        GLAccountUDC accountUDC = (GLAccountUDC) oem.loadEntity("GLAccountUDC", udcConds, null, loggedUser);
                                        if (accountUDC != null) {
                                            cGlAccount += accountUDC.getGlAccount();
                                        }
                                    } else if (currentSegmentField.equals("location")) {
                                        List<String> udcConds = new ArrayList<String>();
                                        udcConds.add("businessUdc.dbid = " + ((UDC) currentSegment).getDbid());
                                        GLAccountUDC accountUDC = (GLAccountUDC) oem.loadEntity("GLAccountUDC", udcConds, null, loggedUser);
                                        if (accountUDC != null) {
                                            cGlAccount += accountUDC.getGlAccount();
                                        }
                                    }
                                }

                                if (j != segments.size() - 1) {
                                    cGlAccount += separator;
                                }
                            }
                        }

                        //debit
                        if (seAcc.isDbSingleVal()) {
                            dGlAccount = seAcc.getDbAccount();
                        } else {
                            for (int j = 0; j < segments.size(); j++) {
                                String currentSegmentField = segments.get(j);
                                if (currentSegmentField.startsWith("constVal_")) {
                                    dGlAccount += currentSegmentField.substring("constVal_".length());
                                } else {
                                    Object currentSegment = BaseEntity.getValueFromEntity(accounts.get(i), currentSegmentField);
                                    if (currentSegment instanceof Unit) {
                                        dGlAccount += ((Unit) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof Employee) {
                                        conds.clear();
                                        conds.add("employee.dbid = " + ((Employee) currentSegment).getDbid());
                                        EmployeePayroll employeePayroll = (EmployeePayroll) oem.loadEntity(
                                                EmployeePayroll.class.getSimpleName(), conds, null, loggedUser);
                                        if (employeePayroll != null) {
                                            dGlAccount += employeePayroll.getGlAccount();
                                        }
                                    } else if (currentSegment instanceof CostCenter) {
                                        dGlAccount += ((CostCenter) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof PayGrade) {
                                        dGlAccount += ((PayGrade) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof LegalEntity) {
                                        dGlAccount += ((LegalEntity) currentSegment).getGlAccount();
                                    } else if (currentSegment instanceof SalaryElement) {
                                        dGlAccount += ((SalaryElement) currentSegment).getDebitAccount();
                                    } else if (currentSegmentField.equals("nationality")) {
                                        List<String> udcConds = new ArrayList<String>();
                                        udcConds.add("businessUdc.dbid = " + ((UDC) currentSegment).getDbid());
                                        GLAccountUDC accountUDC = (GLAccountUDC) oem.loadEntity("GLAccountUDC", udcConds, null, loggedUser);
                                        if (accountUDC != null) {
                                            dGlAccount += accountUDC.getGlAccount();
                                        }
                                    } else if (currentSegmentField.equals("hiringType")) {
                                        List<String> udcConds = new ArrayList<String>();
                                        udcConds.add("businessUdc.dbid = " + ((UDC) currentSegment).getDbid());
                                        GLAccountUDC accountUDC = (GLAccountUDC) oem.loadEntity("GLAccountUDC", udcConds, null, loggedUser);
                                        if (accountUDC != null) {
                                            dGlAccount += accountUDC.getGlAccount();
                                        }
                                    } else if (currentSegmentField.equals("location")) {
                                        List<String> udcConds = new ArrayList<String>();
                                        udcConds.add("businessUdc.dbid = " + ((UDC) currentSegment).getDbid());
                                        GLAccountUDC accountUDC = (GLAccountUDC) oem.loadEntity("GLAccountUDC", udcConds, null, loggedUser);
                                        if (accountUDC != null) {
                                            dGlAccount += accountUDC.getGlAccount();
                                        }
                                    }
                                }
                                if (j != segments.size() - 1) {
                                    dGlAccount += separator;
                                }
                            }
                        }
                        accounts.get(i).setCreditAccount(cGlAccount);
                        accounts.get(i).setDebitAccount(dGlAccount);
                        oem.saveEntity(accounts.get(i), loggedUser);
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }
}
