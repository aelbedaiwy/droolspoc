package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import com.unitedofoq.otms.utilities.filter.FilterUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.payroll.PayrollCalculationBeanLocal",
        beanInterface = PayrollCalculationBeanLocal.class)
public class PayrollCalculationBean implements PayrollCalculationBeanLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private DataTypeServiceRemote dataTypeService;
    @EJB
    private WSUtilityLocal webServiceUtility;
    @EJB
    private PayrollCalculationBeanLocal payrollService;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;

    @Override
    public OFunctionResult initializeMonthlyCalculatedPeriod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();

        try {
            CalculatedPeriod calculatedPeriod;

            List conditions = new ArrayList();
            conditions.clear();
            conditions.add("paymentMethod.unit = 'M'");
            conditions.add("closed = 'N'");
            calculatedPeriod = (CalculatedPeriod) oem.loadEntity("CalculatedPeriod", conditions, null, loggedUser);
            ODataMessage dm = new ODataMessage(dataTypeService.loadODataType("SingleObjectValue", loggedUser),
                    Collections.singletonList((Object) calculatedPeriod));
            ofr.setReturnedDataMessage(dm);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;

    }

    @Override
    public OFunctionResult initializeSeparateCalculatedPeriod(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();

        try {
            CalculatedPeriod calculatedPeriod;
            List conditions = new ArrayList();
            conditions.clear();
            conditions.add("paymentMethod.unit = 'S'");
            conditions.add("closed = 'N'");
            calculatedPeriod = (CalculatedPeriod) oem.loadEntity("CalculatedPeriod", conditions, null, loggedUser);
            ODataMessage dm = new ODataMessage(dataTypeService.loadODataType("SingleObjectValue", loggedUser),
                    Collections.singletonList((Object) calculatedPeriod));
            ofr.setReturnedDataMessage(dm);

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }

        return ofr;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult runSalaryCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            SalaryCalculation salaryCalculation = (SalaryCalculation) odm.getData().get(0);
            if (salaryCalculation != null) {
                String whereSQL = null;
                CalculatedPeriod calculatedPeriod = salaryCalculation.getCalculatedPeriod();
                if (calculatedPeriod != null) {
                    FilterBase request = salaryCalculation;
                    List wsData = new ArrayList();
                    Map<String, Long> filterMap = generateMapCondition(request);
                    whereSQL = FilterUtility.getFilterOHR(filterMap);
                    wsData.add(calculatedPeriod.getDbid());
                    wsData.add(whereSQL);
                    String webServiceCode = "WSF_SalCalc";
                    String wsResult = webServiceUtility.callingWebService(webServiceCode, wsData, loggedUser);
                    if (wsResult.equals("1")) {
                        ofr.addSuccess(userMessageServiceRemote.getUserMessage("O-00502", loggedUser));
                    } else {
                        ofr.addError(userMessageServiceRemote.getUserMessage("O-00503", loggedUser));
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }

        return ofr;
    }

    @Override
    public OFunctionResult runSeparateCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            SeparateCalculation separateCalculation = (SeparateCalculation) odm.getData().get(0);
            if (separateCalculation != null) {
                String whereSQL = null;
                CalculatedPeriod calculatedPeriod = separateCalculation.getCalculatedPeriod();
                if (calculatedPeriod != null) {
                    FilterBase request;
                    request = (FilterBase) separateCalculation;
                    List wsData = new ArrayList();
                    Map<String, Long> filterMap = generateMapCondition(request);
                    whereSQL = FilterUtility.getFilterOHR(filterMap);
                    wsData.add(calculatedPeriod.getDbid());
                    wsData.add(whereSQL);
                    String webServiceCode = "WSF_SalCalc";
                    String wsResult = webServiceUtility.callingWebService(webServiceCode, wsData, loggedUser);
                    if (wsResult.equals("1")) {
                        ofr.addSuccess(userMessageServiceRemote.getUserMessage("O-00504", loggedUser));
                    } else {
                        ofr.addError(userMessageServiceRemote.getUserMessage("O-00505", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }

        return ofr;
    }

    @Override
    public OFunctionResult runRetroCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            RetroActiveCalculation retroCalculation = (RetroActiveCalculation) odm.getData().get(0);
            if (retroCalculation != null) {
                String whereSQL = null;
                CalculatedPeriod fromCalculatedPeriod = retroCalculation.getFromCalculatedPeriod();
                CalculatedPeriod toCalculatedPeriod = retroCalculation.getToCalculatedPeriod();
                if (fromCalculatedPeriod != null && toCalculatedPeriod != null) {
                    FilterBase request = (FilterBase) retroCalculation;
                    List wsData = new ArrayList();
                    Map<String, Long> filterMap = generateMapCondition(request);
                    whereSQL = FilterUtility.getFilterOHR(filterMap);
                    wsData.add(fromCalculatedPeriod.getDbid());
                    wsData.add(toCalculatedPeriod.getDbid());
                    wsData.add(whereSQL);
                    String webServiceCode = "WSF_RetroCalc001";
                    String wsResult = webServiceUtility.callingWebService(webServiceCode, wsData, loggedUser);
                    if (wsResult.equals("1")) {
                        ofr.addSuccess(userMessageServiceRemote.getUserMessage("O-00506", loggedUser));
                    } else {
                        ofr.addError(userMessageServiceRemote.getUserMessage("O-00507", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }

        return ofr;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult runPersonnelClosing(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            SalaryCalculation salaryCalculation = (SalaryCalculation) odm.getData().get(0);
            if (salaryCalculation != null) {
                String whereSQL = null;
                CalculatedPeriod calculatedPeriod = salaryCalculation.getCalculatedPeriod();
                if (calculatedPeriod != null) {
                    FilterBase request = (FilterBase) salaryCalculation;
                    List wsData = new ArrayList();
                    Map<String, Long> filterMap = generateMapCondition(request);
                    whereSQL = FilterUtility.getFilterOHR(filterMap);
                    wsData.add(calculatedPeriod.getDbid());
                    wsData.add(whereSQL);
                    String webServiceCode = "WSF_PerClosing001";
                    String wsResult = webServiceUtility.callingWebService(webServiceCode, wsData, loggedUser);
                    if (wsResult.equals("1")) {
                        ofr.addSuccess(userMessageServiceRemote.getUserMessage("O-00500", loggedUser));
                    } else {
                        ofr.addError(userMessageServiceRemote.getUserMessage("O-00501", loggedUser));
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }

        return ofr;
    }

    @Override
    public OFunctionResult runPersonnelClosingAndSalaryCalculation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            ofr.append(payrollService.runPersonnelClosing(odm, functionParms, loggedUser));
            ofr.append(payrollService.runSalaryCalculation(odm, functionParms, loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }

        return ofr;
    }

    private Map<String, Long> generateMapCondition(FilterBase request) {
        Employee employee = request.getEmployee();
        CostCenter costCenter = request.getCostCenter();
        UDC location = request.getLocation();
        UDC hiringType = request.getHiringType();
        Position position = request.getPosition();
        Unit unit = request.getUnit();
        Job job = request.getJob();
        PayGrade grade = request.getPayGrade();
        Map<String, Long> filterMap = new HashMap<String, Long>();
        filterMap.put("employee", employee == null ? 0 : employee.getDbid());
        filterMap.put("costCenter", costCenter == null ? 0 : costCenter.getDbid());
        filterMap.put("location", location == null ? 0 : location.getDbid());
        filterMap.put("hiringType", hiringType == null ? 0 : hiringType.getDbid());
        filterMap.put("position", position == null ? 0 : position.getDbid());
        filterMap.put("unit", unit == null ? 0 : unit.getDbid());
        filterMap.put("job", job == null ? 0 : job.getDbid());
        filterMap.put("grade", grade == null ? 0 : grade.getDbid());
        return filterMap;
    }

    //We Need To Make New Service To Include Calculated Period Functions
    @Override
    public OFunctionResult closeMonth(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR, oFRUpdate = null;
        oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            SalaryCalculation salaryCalculation = (SalaryCalculation) odm.getData().get(0);
            if (salaryCalculation != null) {
                CalculatedPeriod newCalculatedPeriod = salaryCalculation.getCalculatedPeriod();
                if (newCalculatedPeriod.getClosed().equals("Y")) {
                    oFR.addError(userMessageServiceRemote.getUserMessage("O-00509", systemUser));
                    return oFR;
                }
                if (newCalculatedPeriod.getClosed().equals("N")) {
                    if ("M".equals(newCalculatedPeriod.getPaymentMethod().getUnit())) {
                        oFRUpdate = inActiveEmployeesHavingLeaveDate(newCalculatedPeriod, loggedUser);
                        oFR.append(oFRUpdate);
                    }
                    //Closing Loan
                    List loanData = new ArrayList();

                    loanData.add(newCalculatedPeriod.getDbid());

                    String webServiceCode = "WSF_ClosingLoan";
                    webServiceUtility.callingWebService(webServiceCode, loanData, loggedUser);
                    newCalculatedPeriod.setClosed("Y");
                    oem.saveEntity(newCalculatedPeriod, loggedUser);
                    oFR.addSuccess(userMessageServiceRemote.getUserMessage("O-00508", systemUser));
                    oFR.append(generateNewCalculatedPeriod(newCalculatedPeriod, loggedUser));
                }
            }
            return oFR;
        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser, "ODataMessage", odm);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;
        }
    }

    private OFunctionResult inActiveEmployeesHavingLeaveDate(CalculatedPeriod calculatedPeriod, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        Date cpStartDate;
        Date cpEndDate;
        try {
            if (calculatedPeriod.getClosed().equals("N")) {
                cpStartDate = calculatedPeriod.getStartDate();
                cpEndDate = calculatedPeriod.getEndDate();

                List<Employee> employees = oem.createListNamedQuery("getEmployeeForClosing", loggedUser,
                        "from", cpStartDate,
                        "to", cpEndDate);
                for (Employee employee : employees) {
                    employee.setActive(false);
                    ofr.append(entitySetupService.callEntityUpdateAction(employee, loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private OFunctionResult generateNewCalculatedPeriod(CalculatedPeriod oldCalculatedPeriod, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            CalculatedPeriod newCalculatedPeriod2;
            OFunctionResult oFRUpdate;
            CalculatedPeriod calculatedPeriod;

            calculatedPeriod = (CalculatedPeriod) oem.loadEntity("CalculatedPeriod", oldCalculatedPeriod.getDbid(), null, null, loggedUser);

            int month = calculatedPeriod.getMonth();
            int year = calculatedPeriod.getYear();
            month++;
            if (month == 13) {
                month = 1;
                year++;
            }
            Calendar calendar = new GregorianCalendar();
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);

            // check if the next calc period already exists
            List<String> conds = new ArrayList<String>();
            conds.add("month = " + month);
            conds.add("year = " + year);
            conds.add("payPeriod.dbid = " + calculatedPeriod.getPayPeriod().getDbid());
            conds.add("paymentMethod.dbid = " + calculatedPeriod.getPaymentMethod().getDbid());
            newCalculatedPeriod2 = (CalculatedPeriod) oem.loadEntity(CalculatedPeriod.class.getSimpleName(), conds, null, loggedUser);

            if (newCalculatedPeriod2 == null) {
                newCalculatedPeriod2 = new CalculatedPeriod();
                newCalculatedPeriod2.setClosed("N");
                newCalculatedPeriod2.setInActive(false);
                newCalculatedPeriod2.setDescription("Monthly " + month + " - " + year);
                newCalculatedPeriod2.setMonth(month);
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String endDay, endDateStr;
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                    endDay = "31";
                } else if (month == 2) {
                    if (year % 4 == 0) {
                        endDay = "29";
                    } else {
                        endDay = "28";
                    }
                } else {
                    endDay = "30";
                }

                endDateStr = endDay + "-" + month + "-" + year;

                Date tempDate = dateFormat.parse(endDateStr);
                newCalculatedPeriod2.setEndDate(tempDate);
                tempDate = dateFormat.parse("1-" + month + "-" + year);

                newCalculatedPeriod2.setStartDate(tempDate);
                newCalculatedPeriod2.setYear(year);
                newCalculatedPeriod2.setPayPeriod(calculatedPeriod.getPayPeriod());
                newCalculatedPeriod2.setPaymentMethod(calculatedPeriod.getPaymentMethod());
                newCalculatedPeriod2.setTaxSettlementEnabled(calculatedPeriod.getTaxSettlementEnabled());
                oFRUpdate = entitySetupService.callEntityCreateAction(newCalculatedPeriod2, loggedUser);
                if (oFRUpdate.getErrors().isEmpty()) {
                    List<String> userMsgParams = new ArrayList<String>();
                    userMsgParams.add(month + " - " + year);
                    oFR.addSuccess(
                            userMessageServiceRemote.getUserMessage("O-00510", userMsgParams, loggedUser));
                } else {
                    oFR.append(oFRUpdate);
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }
}
