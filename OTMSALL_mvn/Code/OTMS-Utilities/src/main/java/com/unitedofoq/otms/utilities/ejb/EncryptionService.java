package com.unitedofoq.otms.utilities.ejb;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import static com.unitedofoq.fabs.core.encryption.DataEncryptorService.loggedUser;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.otms.utilities.encryption.ColumnEncrypted;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.utilities.ejb.EncryptionServiceLocal", beanInterface = EncryptionServiceLocal.class)
public class EncryptionService implements EncryptionServiceLocal {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    UIFrameworkServiceRemote uIFrameworkService;
    @EJB
    DataTypeServiceRemote dataTypeService;
    @EJB
    DataEncryptorServiceLocal dataEncryptorService;

    String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

    @Override
    public Map<String, Map<String, ColumnEncrypted>> getEncryptedTablesColumnsMap(OUser loggedUser) {
        String sql;
        sql = "select originaltablename , originalcolumnname , encryptedcolumnname , columndatatype "
                + " from ENCRYPTEDCOLUMNS";
        List<Object[]> encryptedTablesCols = null;

        Map<String, List<ColumnEncrypted>> encryptedTables = new HashMap<String, List<ColumnEncrypted>>();
        Map<String, Map<String, ColumnEncrypted>> encryptedTablesColumns = new HashMap<String, Map<String, ColumnEncrypted>>();

        try {
            encryptedTablesCols = (List<Object[]>) oem.executeEntityListNativeQuery(sql, loggedUser);
        } catch (Exception ex) {
            OLog.logError(ex.toString(), loggedUser);
        }
        String tableName,
                columnName,
                encryptedColumn,
                columnType;
        ColumnEncrypted columnEncrypted = null;
        List<ColumnEncrypted> columnEncryptedList = null;
        Map<String, ColumnEncrypted> encryptedColumnMap = null;
        for (Object[] object : encryptedTablesCols) {
            columnEncryptedList = new ArrayList<ColumnEncrypted>();
            encryptedColumnMap = new HashMap<String, ColumnEncrypted>();
            tableName = object[0].toString();
            columnName = object[1].toString();
            encryptedColumn = object[2].toString();
            columnType = object[3].toString();
            columnEncrypted = new ColumnEncrypted();
            columnEncrypted.setColName(columnName);
            columnEncrypted.setEncColName(encryptedColumn);
            columnEncrypted.setColType(columnType);
            //check if table exist in Map or not
            if (encryptedTablesColumns.containsKey(tableName)) {
                //Exist
                encryptedColumnMap = encryptedTablesColumns.get(tableName);
                encryptedColumnMap.put(columnName, columnEncrypted);
                encryptedTablesColumns.remove(tableName);
                encryptedTablesColumns.put(tableName, encryptedColumnMap);
//                columnEncryptedList = encryptedTables.get(tableName);
//                columnEncryptedList.add(columnEncrypted);
//                encryptedTables.remove(tableName);
//                encryptedTables.put(tableName, columnEncryptedList);
            } else {
                //Not Exist
                encryptedColumnMap.put(columnName, columnEncrypted);
                encryptedTablesColumns.put(tableName, encryptedColumnMap);
//                columnEncryptedList.add(columnEncrypted);
//                encryptedTables.put(tableName, columnEncryptedList);
            }
        }
        return encryptedTablesColumns;
    }

    @Override
    public Map<String, ColumnEncrypted> getEncryptedColumnsMap(String tableName, OUser loggedUser) {
        String sql;
        sql = "select originalcolumnname , encryptedcolumnname , columndatatype, expression "
                + " from ENCRYPTEDCOLUMNS Where UPPER(originaltablename) = UPPER('" + tableName
                + "')";
        List<Object[]> encryptedTablesCols = null;
        try {
            encryptedTablesCols = (List<Object[]>) oem.executeEntityListNativeQuery(sql, loggedUser);
        } catch (Exception ex) {
            OLog.logError(ex.toString(), loggedUser);
        }

        if (encryptedTablesCols == null || encryptedTablesCols.isEmpty()) {
            return new HashMap<String, ColumnEncrypted>();
        }

        String columnName,
                encryptedColumn,
                columnType,
                expression;
        ColumnEncrypted columnEncrypted = null;
        Map<String, ColumnEncrypted> encryptedColumnMap = new HashMap<String, ColumnEncrypted>();
        for (Object[] object : encryptedTablesCols) {
            //tableName = object[0].toString();
            columnName = object[0] == null ? "" : object[0].toString();
            encryptedColumn = object[1] == null ? "" : object[1].toString();
            columnType = object[2] == null ? "" : object[2].toString();
            expression = object[3] == null ? "" : object[3].toString();

            columnEncrypted = new ColumnEncrypted();
            columnEncrypted.setColName(columnName);
            columnEncrypted.setEncColName(encryptedColumn);
            columnEncrypted.setColType(columnType);
            columnEncrypted.setExpression(expression);
            //TO BE RETRIEVED IN UPPER CASE
            encryptedColumnMap.put(columnName.toUpperCase(), columnEncrypted);

        }
        return encryptedColumnMap;
    }

    @Override
    public String getModifiedBirtReportStatement(String reportSelectStat, OUser loggedUser) {

        try {
            // split the statement on "select, from, order by, where"

            // get last "from"
            // 1 - get index of last "from"
            // 2 - get the table name after it
            reportSelectStat = reportSelectStat.replaceAll("\n", " ").replaceAll("\\s+", " ").replaceAll("[\\s]*,[\\s]*", ",");

            reportSelectStat = reportSelectStat.trim();
            int indexOfFromKeyword = reportSelectStat.toUpperCase().lastIndexOf(" FROM ");
            String tableName = reportSelectStat.substring(indexOfFromKeyword + 5);
            tableName = tableName.trim();
            if (tableName.contains(" ")) {
                tableName = tableName.substring(0, tableName.indexOf(" "));
            }

            // get enc columns
            Map<String, ColumnEncrypted> encColumns = getEncryptedColumnsMap(tableName, loggedUser);

            // get the two keys
            String symmetricKey = dataEncryptorService.getSymmetricKey();
            String ivKey = dataEncryptorService.getIvKey();
            
            String colName, colNameEnc;
            
            //23-3-2017--Khaled Zidane
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {

                for (String encCol : encColumns.keySet()) {
                    colName = encColumns.get(encCol).getColName();
                    colNameEnc = encColumns.get(encCol).getEncColName();

                    // do not cast in case of varchar types
                    if (!encColumns.get(encCol).getColType().toUpperCase().contains("VARCHAR")) {
                        reportSelectStat = reportSelectStat.replaceAll("(?i)" + tableName + "."
                                + colName + "(?![A-Za-z])",
                                "cast(dbo.decrypt(" + tableName + "." + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ") as "
                                + encColumns.get(encCol).getColType() + ")");

                        reportSelectStat = reportSelectStat.replaceAll("(?<!(?i)" + tableName + ".)(?<!(?i)as )(?i)"
                                + colName + "(?![A-Za-z])", "cast(dbo.decrypt(" + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ") as "
                                + encColumns.get(encCol).getColType() + ")");
                    } else {
                        reportSelectStat = reportSelectStat.replaceAll("(?i)" + tableName + "."
                                + colName + "(?![A-Za-z])",
                                "dbo.decrypt(" + tableName + "." + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ")");

                        reportSelectStat = reportSelectStat.replaceAll("(?<!(?i)" + tableName + ".)(?<!(?i)as )(?i)"
                                + colName + "(?![A-Za-z])", "dbo.decrypt(" + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ")");
                    }
                }
            }
            else{
                for (String encCol : encColumns.keySet()) {
                    colName = encColumns.get(encCol).getColName();
                    colNameEnc = encColumns.get(encCol).getEncColName();

                    // do not cast in case of varchar types
                    if (!encColumns.get(encCol).getColType().toUpperCase().contains("VARCHAR")) {
                        reportSelectStat = reportSelectStat.replaceAll("(?i)" + tableName + "."
                                + colName + "(?![A-Za-z])",
                                "cast(decrypt(" + tableName + "." + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ") as "
                                + encColumns.get(encCol).getColType() + ")");

                        reportSelectStat = reportSelectStat.replaceAll("(?<!(?i)" + tableName + ".)(?<!(?i)as )(?i)"
                                + colName + "(?![A-Za-z])", "cast(decrypt(" + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ") as "
                                + encColumns.get(encCol).getColType() + ")");
                    } else {
                        reportSelectStat = reportSelectStat.replaceAll("(?i)" + tableName + "."
                                + colName + "(?![A-Za-z])",
                                "decrypt(" + tableName + "." + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ")");

                        reportSelectStat = reportSelectStat.replaceAll("(?<!(?i)" + tableName + ".)(?<!(?i)as )(?i)"
                                + colName + "(?![A-Za-z])", "decrypt(" + colNameEnc
                                + ", '" + symmetricKey + "' , " + ivKey + ")");
                    }
                }
            }
            // <editor-fold defaultstate="collapsed" desc="">
            /*// get the part of the original select stat after the "from tableName" till the end,
             // including order by, group by and where



             String whereStat = "", groupByStat = "",orderByStat = "";

             if(reportSelectStat.toUpperCase().contains("WHERE")) {
             whereStat = reportSelectStat.substring(reportSelectStat.toUpperCase().lastIndexOf("WHERE"));
             if(reportSelectStat.toUpperCase().contains("GROUP BY")) {
             whereStat = reportSelectStat.substring(reportSelectStat.toUpperCase().lastIndexOf("WHERE"),
             reportSelectStat.toUpperCase().lastIndexOf("GROUP BY"));
             } else if(reportSelectStat.toUpperCase().contains("ORDER BY")) {
             whereStat = reportSelectStat.substring(reportSelectStat.toUpperCase().lastIndexOf("WHERE"),
             reportSelectStat.toUpperCase().lastIndexOf("ORDER BY"));
             }
             }
             if (reportSelectStat.toUpperCase().contains("GROUP BY")) {
             if(reportSelectStat.toUpperCase().contains("ORDER BY")) {
             groupByStat = reportSelectStat.substring(reportSelectStat.toUpperCase().lastIndexOf("GROUP BY"),
             reportSelectStat.toUpperCase().lastIndexOf("ORDER BY"));
             } else {
             groupByStat = reportSelectStat.substring(reportSelectStat.toUpperCase().lastIndexOf("GROUP BY"));
             }
             }
             if(reportSelectStat.toUpperCase().contains("ORDER BY")) {
             orderByStat = reportSelectStat.substring(reportSelectStat.toUpperCase().lastIndexOf("ORDER BY"));
             }

             // get the encrypted columns on this table
             Map<String, ColumnEncrypted> encColumns = getEncryptedColumnsMap(tableName, loggedUser);

             // cut the report select statement
             int index = reportSelectStat.toUpperCase().indexOf("SELECT");
             String selectStat = reportSelectStat.substring(index + 6, indexOfFromKeyword);

             // check if the select stat has "distinct"
             boolean distinct = false;
             if (selectStat.trim().toUpperCase().startsWith("DISTINCT")) {
             distinct = true;
             index = selectStat.toUpperCase().indexOf("DISTINCT");
             selectStat = selectStat.substring(index + 8);
             }

             // replace each column with its modified form with decryption
             selectStat = splitSelectStatString(selectStat, encColumns);

             // replace in where, groupby & orderby stat
             whereStat = splitSelectStatString(whereStat, encColumns);
             groupByStat = splitSelectStatString(groupByStat, encColumns);
             orderByStat = splitSelectStatString(orderByStat, encColumns);

             if(distinct) {
             selectStat = " distinct " + selectStat;
             }
             modifiedStat = "SELECT " + selectStat
             + " FROM " + tableName + " "
             + whereStat + " "
             + groupByStat + " "
             + orderByStat;
             */
            // </editor-fold>
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return reportSelectStat;
    }

    /*
     private String splitSelectStatString(String origStr, Map<String, ColumnEncrypted> encColumns) {

     String columns[] = origStr.split(",");
     String columnSplittedOnDot[] = new String[2];
     String columnDec = "";
     String tableName = "";

     for (String column : columns) {


     //            if(column.toUpperCase().contains("SELECT ")) {
     //                column = column.substring(column.toUpperCase().indexOf("SELECT ")+7);
     //            } else if(column.toUpperCase().contains("GROUP BY ")) {
     //                column = column.substring(column.toUpperCase().indexOf("GROUP BY ")+9);
     //            } else if(column.toUpperCase().contains("ORDER BY ")) {
     //                column = column.substring(column.toUpperCase().indexOf("GROUP BY ")+9);
     //            }

     if(column.toUpperCase().contains(" AS ")) {
     column = column.toUpperCase().split(" AS ")[0];
     }

     if (column.contains(".")) {
     columnSplittedOnDot = column.split("\\.");
     tableName = columnSplittedOnDot[0] + ".";
     columnDec = columnSplittedOnDot[1];
     } else {
     columnDec = column;
     }
     if (encColumns.containsKey(columnDec.toUpperCase().trim())) {
     origStr = getDecryptionStatement(encColumns.get(columnDec.toUpperCase().trim()), origStr, tableName, column);
     }
     }
     return origStr;
     }

     private String splitStatPart2(String origStr, Map<String, ColumnEncrypted> encColumns) {
     // get the stat beside the group by then order by
     // split on "," then replace them
     String columnSplittedOnDot[] = new String[2];
     String columnDec = "";
     String tableName = "";

     origStr = origStr.toUpperCase();
     for (String key : encColumns.keySet()) {

     if (origStr.contains(key)) {
     columnSplittedOnDot = key.split(".");
     tableName = columnSplittedOnDot[0] + ".";
     columnDec = columnSplittedOnDot[1];

     if (encColumns.containsKey(columnDec)) {
     // origStr = getDecryptionStatement(encColumns.get(columnDec.toUpperCase()), origStr, tableName, column);
     }
     }
     }
     return origStr;
     }

     private String getDecryptionStatement(ColumnEncrypted encColumns, String origStr,
     String tableName, String column) {
     if (encColumns != null) {
     // check if this column is in the statement
     // then, replace this column with the decrypted
     String modifiedKey;
     modifiedKey =
     "cast(decrypt(to_char(" + tableName + encColumns.getEncColName()
     + "), '669RXFgQd3HD9vDr',66657489213487653421348765432783) as "
     + encColumns.getColType() +")";
     origStr = origStr.substring(0,origStr.toUpperCase().indexOf(column)) + modifiedKey + origStr.substring(origStr.toUpperCase().indexOf(column)+column.length());
     }
     return origStr;
     }
     */
    @Override
    public OFunctionResult birtReportsUserExit(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String selectSTMT = (String) odm.getData().get(0);
            String repName = (String) odm.getData().get(1);
            String dataSetName = (String) odm.getData().get(2);

            String newSelect = getModifiedBirtReportStatement(selectSTMT, loggedUser);

            ODataType screenDataLoadingUEDTRet = dataTypeService.loadODataType("ScreenDataLoadingUERet", loggedUser);
            ODataMessage returnedODM = new ODataMessage();
            returnedODM.setODataType(screenDataLoadingUEDTRet);
            ArrayList<Object> odmData = new ArrayList<Object>(1);

            odmData.add(newSelect);
            returnedODM.setData(odmData);
            ofr.setReturnedDataMessage(returnedODM);
            return ofr;

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

}
