package com.unitedofoq.otms.utilities.encryption;

public class ColumnEncrypted {

    String colName;
    String encColName;
    String colType;
    String expression;

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getEncColName() {
        return encColName;
    }

    public void setEncColName(String encColName) {
        this.encColName = encColName;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }
};
