package com.unitedofoq.otms.utilities.ejb;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.utilities.encryption.ColumnEncrypted;
import java.util.Map;
import javax.ejb.Local;

@Local
public interface EncryptionServiceLocal {

    public Map<String, Map<String, ColumnEncrypted>> getEncryptedTablesColumnsMap(OUser loggedUser);

    public Map<String, ColumnEncrypted> getEncryptedColumnsMap(String tableName, OUser loggedUser);

    public String getModifiedBirtReportStatement(String selectStat, OUser loggedUser);

    public OFunctionResult birtReportsUserExit(ODataMessage odm, OFunctionParms params, OUser loggedUser);
}
