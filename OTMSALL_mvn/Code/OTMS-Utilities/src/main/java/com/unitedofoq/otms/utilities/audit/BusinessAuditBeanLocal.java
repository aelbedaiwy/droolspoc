package com.unitedofoq.otms.utilities.audit;

import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.PayrollMatrix;
import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.Local;

/**
 *
 * @author Aboelnour
 */
@Local
public interface BusinessAuditBeanLocal {

    public OFunctionResult log(PayrollMatrix payrollMatrix, Employee employee, Boolean salaryElementAssigned,
            BigDecimal empSalEleValue, Boolean applied, Date logDate, String message, String methodName, OUser loggedUser);
}
