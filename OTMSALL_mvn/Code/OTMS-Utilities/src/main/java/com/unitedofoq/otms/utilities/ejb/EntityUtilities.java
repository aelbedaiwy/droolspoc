package com.unitedofoq.otms.utilities.ejb;

import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.sql.Connection;
import javax.naming.InitialContext;
import javax.persistence.Table;
import javax.sql.DataSource;

public class EntityUtilities {

    public static String getTableName(String entityName) {
        String tableName = "";
        try {
            Class cls = Class.forName(entityName);
            Table clsTble = (Table) cls.getAnnotation(Table.class);
            tableName = clsTble == null ? cls.getSimpleName() : clsTble.name();
        } catch (Exception e) {
            OLog.logException(e, null);
        }
        return tableName;
    }

    public static String getDBConnectionString(OUser loggedUser) {
        String connectionString = "";
        try {
            DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
            Connection connection = datasource.getConnection();
            if (connection != null
                    && connection.getMetaData() != null) {
                connectionString = connection.getMetaData().getDatabaseProductName();
                connection.close();
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return connectionString;
    }
}
