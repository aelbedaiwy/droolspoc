package com.unitedofoq.otms.utilities.audit;

import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.PayrollMatrix;
import com.unitedofoq.otms.payroll.PayrollParameter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aboelnour
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.foundation.utilities.BusinessAuditBeanLocal",
        beanInterface = BusinessAuditBeanLocal.class)

public class BusinessAuditBean implements BusinessAuditBeanLocal {

    @EJB
    private OEntityManagerRemote oem;

    @Override
    public OFunctionResult log(PayrollMatrix payrollMatrix, Employee employee, Boolean salaryElementAssigned,
            BigDecimal empSalEleValue, Boolean applied, Date logDate, String message, String methodName, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            List<String> conds = new ArrayList<String>();
            conds.add("description = 'BusinessAuditLog'");
            PayrollParameter payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            if (payrollParameter != null && payrollParameter.getValueData() != null
                    && payrollParameter.getValueData().equalsIgnoreCase("y")) {
                PayrollMatrixBA businessAudit = new PayrollMatrixBA();
                businessAudit.setPayrollMatrix(payrollMatrix);
                businessAudit.setEmployee(employee);
                businessAudit.setSalaryElementAssigned(salaryElementAssigned);
                businessAudit.setSalaryElementValue(empSalEleValue);
                businessAudit.setApplied(applied);
                businessAudit.setLogDate(logDate);
                businessAudit.setMessage(message);
                businessAudit.setMethodName(methodName);
                businessAudit.setLoggedUser(loggedUser.getLoginName());
                oem.saveEntity(businessAudit, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return oFR;
    }
}
