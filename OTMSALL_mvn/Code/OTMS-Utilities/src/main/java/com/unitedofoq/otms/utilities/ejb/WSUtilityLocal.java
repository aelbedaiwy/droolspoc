/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.utilities.ejb;

import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;
import javax.ejb.Local;



@Local
public interface WSUtilityLocal {
    public String callingWebService(String webServiceCode, List webServiceData, OUser loggedUser);
}
