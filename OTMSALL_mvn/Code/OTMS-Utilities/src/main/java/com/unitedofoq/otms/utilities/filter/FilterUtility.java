package com.unitedofoq.otms.utilities.filter;

import java.util.Map;

public class FilterUtility {

    public static String getFilter(Map<String, Long> filter, String field) {
        String whereSQL = "";

        long employeeDbid = filter.get("employee");
        long costCenterDbid = filter.get("costCenter");
        long locationDbid = filter.get("location");
        long hiringTypeDbid = filter.get("hiringType");
        long positionDbid = filter.get("position");
        long unitDbid = filter.get("unit");
        long jobDbid = filter.get("job");

        //_____________________________Filter____________________________________
        if (employeeDbid != 0) {
            whereSQL = " AND " + field + " = " + employeeDbid;
        }
        if (costCenterDbid != 0) {
            whereSQL += " AND " + field + " in (select employee_dbid from employeepayroll where costcenter_dbid = " + costCenterDbid + ") ";
        }
        if (locationDbid != 0) {
            whereSQL += " AND " + field + " in (select employee_dbid from employeepayroll where location_dbid = " + locationDbid + ") ";
        }
        if (hiringTypeDbid != 0) {
            whereSQL += " AND " + field + " in (select employee_dbid from employeepayroll where hiringtype_dbid = " + hiringTypeDbid + ") ";
        }
        if (positionDbid != 0) {
            whereSQL += " AND " + field + " in (select dbid from oemployee where position_dbid = " + positionDbid + ") ";
        }
        if (unitDbid != 0) {
            whereSQL += " AND " + field
                    + " in (select dbid from oemployee where position_dbid in (select dbid from position where unit_dbid ="
                    + unitDbid + ") )";
        }
        if (jobDbid != 0) {
            whereSQL += " AND " + field
                    + " in (select dbid from oemployee where position_dbid in (select dbid from position where job_dbid ="
                    + jobDbid + ") )";
        }

        return whereSQL;
    }

    public static String getFilterOHR(Map<String, Long> filter) {
        String whereSQL = "";

        long employeeDbid = filter.get("employee");
        long costCenterDbid = filter.get("costCenter");
        long locationDbid = filter.get("location");
        long hiringTypeDbid = filter.get("hiringType");
        long positionDbid = filter.get("position");
        long unitDbid = filter.get("unit");
        long jobDbid = filter.get("job");
        long payGradeDbid = filter.get("grade");

        //_____________________________Filter____________________________________
        if (employeeDbid != 0) {
            whereSQL = " employee.employee = '" + employeeDbid + "'";
        }
        if (costCenterDbid != 0) {
            if (!whereSQL.equals("")) {
                whereSQL += " AND ";
            }
            whereSQL += " employee.cost_center = '" + costCenterDbid + "' ";
        }
        if (locationDbid != 0) {
            if (!whereSQL.equals("")) {
                whereSQL += " AND ";
            }
            whereSQL += " employee.department =  '" + locationDbid + "' ";
        }
        if (hiringTypeDbid != 0) {
            if (!whereSQL.equals("")) {
                whereSQL += " AND ";
            }
            whereSQL += " employee.hiring_type = '" + hiringTypeDbid + "' ";
        }
        if (positionDbid != 0) {
            if (!whereSQL.equals("")) {
                whereSQL += " AND ";
            }
            whereSQL += " employee.position_id = '" + positionDbid + "' ";
        }
        if (unitDbid != 0) {
            if (!whereSQL.equals("")) {
                whereSQL += " AND ";
            }
            whereSQL += "  employee.company  ='" + unitDbid + "'";
        }
        if (jobDbid != 0) {
            if (!whereSQL.equals("")) {
                whereSQL += " AND ";
            }
            whereSQL += " employee.job_id = '" + jobDbid + "'";
        }
        if (payGradeDbid != 0) {
            if (!whereSQL.equals("")) {
                whereSQL += " AND ";
            }
            whereSQL += " employee.pay_grade_id = '" + payGradeDbid + "'";
        }
        return whereSQL;
    }
}
