package com.unitedofoq.otms.utilities.encryption;

import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.security.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.commons.codec.binary.Hex;

public class EncryptionUtility {

    private static final String ALGO = "AES/CBC/PKCS5Padding";
    private static final byte[] keyValue = "anskdgfiandkndka".getBytes();
    private static Key key;
    private static Cipher decryptor;
    private static Cipher encryptor;

    public static void init() throws Exception {

        key = generateKey();
        encryptor = Cipher.getInstance(ALGO);
        IvParameterSpec iv = new IvParameterSpec(Hex.decodeHex("53647382936253647352635273546354".toCharArray()));
        decryptor = Cipher.getInstance(ALGO);
        encryptor.init(Cipher.ENCRYPT_MODE, key, iv);
        decryptor.init(Cipher.DECRYPT_MODE, key, iv);
    }

//    public static String encrypt(String Data) throws Exception {
//        init();
//        String encryptedValue = "";
//        if (Data != null) {
//            byte[] encVal = encryptor.doFinal(Data.getBytes());
//            encryptedValue = Hex.encodeHexString(encVal);
//        }
//        //String encryptedValue = new BASE64Encoder().encode(encVal);
//        return encryptedValue;
//    }
//
//    public static String decrypt(String encryptedData) throws Exception {
//        init();
//        String decryptedValue = "";
//        //byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
//        if (encryptedData != null) {
//            byte[] decordedValue = Hex.decodeHex(encryptedData.toCharArray());
//            byte[] decValue = decryptor.doFinal(decordedValue);
//            decryptedValue = new String(decValue);
//        }
//        return decryptedValue;
//    }

    public static boolean checkApplyingEncryption(OUser user) throws Exception {
        boolean returnedValue = false;
        try {
            String value = "", jdbcName;
            jdbcName = user.getTenant().getPersistenceUnitName();
            if (jdbcName != null) {
                DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + jdbcName));
                Connection connection = datasource.getConnection();
                Statement statment = connection.createStatement();
                ResultSet result = statment.executeQuery("select valueData from PayrollParameter where description = 'ApplyEncryption'");
                if (result.next()) {
                    value = result.getString("valueData");
                }
                try {
                    connection.close();
                } catch (Exception ex) {
                    OLog.logError("****&&&&Exception Error****&&&&"
                            + "\n Function  : checkApplyingEncryption"
                            + "\n Exception :Closing Connection To Get Apply Encryption From Payroll Parameter", user);
                }
            }
            if (value.equalsIgnoreCase("yes")) {
                returnedValue = true;
            }
        } catch (Exception ex) {
            OLog.logError("****&&&&Exception Error****&&&&"
                    + "\n Function  : checkApplyingEncryption", user);
        }

        return returnedValue;
    }

    private static Key generateKey() throws Exception {
        if (key == null) {
            key = new SecretKeySpec(keyValue, "AES");
        }
        return key;
    }
//    public static void main(String[] args) throws Exception {
//
//        init();
//        String password = "rehab2015azza2015";
//        String passwordEnc = EncryptionUtility.encrypt(password);
//        String passwordDec = EncryptionUtility.decrypt(passwordEnc);
//
//
//        System.out.println("Plain Text : " + password);
//        System.out.println("Encrypted Text : " + passwordEnc);
//        System.out.println("Decrypted Text : " + passwordDec);
//    }
}
