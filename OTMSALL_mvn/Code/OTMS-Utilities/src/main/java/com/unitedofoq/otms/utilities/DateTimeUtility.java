package com.unitedofoq.otms.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Years;

public final class DateTimeUtility {

    public final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public final DateFormat YEAR_FORMAT = new SimpleDateFormat("yyyy");
    public final DateFormat MONTH_FORMAT = new SimpleDateFormat("MM");
    public final DateFormat DAY_FORMAT = new SimpleDateFormat("dd");
    public final DateFormat DAYMONTH_FORMAT = new SimpleDateFormat("MM-dd");
    public final DateFormat DAY_NAME_FORMAT = new SimpleDateFormat("EEEE");
    public static final DateFormat DATETIME_FOTMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    public final String TIME24HOURS_PATTERN
            = "(([01]?[0-9]|2[0-3]):[0-5][0-9])|(([01]?[0-9]|2[0-3]).[0-5][0-9])";
    public final DateTimeZone dateTimeZone = DateTimeZone.forID("EET");

    public enum DateFieldType {

        DAY, MONTH, YEAR
    };

    public boolean validateTimeFormat(String time) {
        Pattern pattern = Pattern.compile(TIME24HOURS_PATTERN);
        Matcher matcher = pattern.matcher(time);
        return matcher.matches();
    }

    public String subtractTime(String time1, String time2) {
        try {

            if (time1 == null || time2 == null) {
                return "00:00";
            } else if (time1.equals("") || time2.equals("")) {
                return "00:00";
            }

            if (time1.contains(".")) {
                time1 = time1.replace(".", ":");
            }

            if (time2.contains(".")) {
                time2 = time2.replace(".", ":");
            }

            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("GMT"));

            Date date1 = df.parse(time1 + ":00");
            Date date2 = df.parse(time2 + ":00");

            long remainder = date2.getTime() - date1.getTime();

            String result = df.format(remainder);
            result = result.substring(0, result.lastIndexOf(":"));

            return result;

        } catch (Exception ex) {
            return null;
        }

    }

    public String addTime(String time1, String time2) {

        if (time1 == null || time2 == null) {
            return "00:00";
        } else if (time1.equals("") || time2.equals("")) {
            return "00:00";
        }

        if (time1.contains(".")) {
            time1 = time1.replace(".", ":");
        }

        if (time2.contains(".")) {
            time2 = time2.replace(".", ":");
        }

        String time1Hours = time1.substring(0, time1.indexOf(":"));
        String time1Mins = time1.substring(time1.indexOf(":") + 1);

        String time2Hours = time2.substring(0, time2.indexOf(":"));
        String time2Mins = time2.substring(time2.indexOf(":") + 1);

        Integer hoursSum = new Integer(time1Hours) + new Integer(time2Hours);
        Integer minsSum = new Integer(time1Mins) + new Integer(time2Mins);

        String hoursSumStr;
        String minsSumStr;

        if (minsSum > 59) {
            minsSum = minsSum - 60;
            hoursSum += 1;
        }

        if (hoursSum < 10) {
            hoursSumStr = "0" + hoursSum;
        } else {
            hoursSumStr = Integer.toString(hoursSum);
        }

        if (minsSum < 10) {
            minsSumStr = "0" + minsSum;
        } else {
            minsSumStr = Integer.toString(minsSum);
        }

        return hoursSumStr + ":" + minsSumStr;
    }

    public int compareTime(String time1, String time2) {

        Integer time1Int = Integer.parseInt(time1.replaceAll(":", "").replaceAll("\\.", ""));
        Integer time2Int = Integer.parseInt(time2.replaceAll(":", "").replaceAll("\\.", ""));

        if (time1Int > time2Int) {
            return 1;
        } else if (time1Int < time2Int) {
            return -1;
        } else {
            return 0;
        }
    }

    public String convertMinutesToTime(double doubleValue) {
        String time = "00:00";

        Double hours = 0.0, minutes = doubleValue;

        Integer intHours, intMins;

        if (doubleValue >= 60) {
            hours = doubleValue / 60;
            minutes = minutes - (hours * 60);
        }

        intHours = hours.intValue();
        intMins = minutes.intValue();

        String hoursStr = intHours.toString();
        String minStr = intMins.toString();

        if (hours < 10) {
            hoursStr = "0" + hoursStr;
        }
        if (minutes < 10) {
            minStr = "0" + minStr;
        }

        time = hoursStr + ":" + minStr;
        return time;
    }

    public Double convertToNumber(String time) {
        time = time.replaceAll("\\.", ":");

        String hours = time.substring(0, time.indexOf(":"));
        String mins = time.substring(time.indexOf(":") + 1);

        Double dMins = (Double.parseDouble(mins) / 60);

        mins = Double.toString(dMins);

        mins = mins.substring(mins.indexOf(".") + 1);

        Double timeInt = Double.parseDouble(hours + "." + mins);

        return timeInt;
    }

    public String convertToTime(Double time) {

        String timeStr = time.toString();

        String hours = timeStr.substring(0, timeStr.indexOf("."));
        String mins = timeStr.substring(timeStr.indexOf(".") + 1);

        if (Integer.parseInt(hours) < 10) {
            hours = "0" + hours;
        }

        mins = Integer.toString(new Integer(mins) * 60 / 100);

        if (new Integer(mins) < 10) {
            mins = "0" + mins;
        }

        return hours + ":" + mins;

    }

    public String round24(String date) {
        return addTime(date, "24:00");
    }

    public Integer getDaysBetween(Date date1, Date date2) {
        int totalNumDays = 0;

        DateTime fromDT = new DateTime(date1, dateTimeZone);
        DateTime toDT = new DateTime(date2, dateTimeZone);

        int result = date1.compareTo(date2);
        if (result <= 0) {
            totalNumDays = Days.daysBetween(fromDT, toDT).getDays();
        }
        if (result > 0) {
            totalNumDays = Days.daysBetween(toDT, fromDT).getDays();
        }

        return totalNumDays;
    }

    public int getLastDayInMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        return maxDay;
    }

    public int getLastDayInMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();

        int day = 1;
        month--;

        calendar.set(year, month, day);

        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        return maxDay;
    }

    /**
     * Add/Subtract a value to/from a date; either day, month or year.
     *
     * @param date
     * @param amount for subtraction let it be a -ve value, otherwise +ve.
     * @param type type of the amount to be either DAY, MOTNH or YEAR.
     * @return a date after modification.
     */
    public Date addValueToDate(Date date, int amount, DateFieldType type) {
        Date newDate = date;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newDate);
        if (type.equals(DateFieldType.DAY)) {
            calendar.add(Calendar.DAY_OF_MONTH, amount);
        } else if (type.equals(DateFieldType.MONTH)) {
            calendar.add(Calendar.MONTH, amount);
        } else if (type.equals(DateFieldType.YEAR)) {
            calendar.add(Calendar.YEAR, amount);
        }
        newDate = calendar.getTime();
        return newDate;
    }

    /**
     * Retrieves the difference between two dates. This difference can be either
     * in the form of number of days, months or years.
     *
     * @param date1
     * @param date2
     * @param type type of the amount to be either DAY, MOTNH or YEAR.
     * @return the difference value
     */
    public int getDifferenceBetweenDates(Date date1, Date date2, DateFieldType type) {
        int difference = 0;
        DateTime fromDT = new DateTime(date1, dateTimeZone);
        DateTime toDT = new DateTime(date2, dateTimeZone);

        int result = date1.compareTo(date2);
        if (result <= 0) {
            if (type.equals(DateFieldType.DAY)) {
                difference = Days.daysBetween(fromDT, toDT).getDays();
            } else if (type.equals(DateFieldType.DAY)) {
                difference = Months.monthsBetween(fromDT, toDT).getMonths();
            } else if (type.equals(DateFieldType.DAY)) {
                difference = Years.yearsBetween(fromDT, toDT).getYears();
            }
        }
        if (result > 0) {
            if (type.equals(DateFieldType.DAY)) {
                difference = Days.daysBetween(toDT, fromDT).getDays();
            } else if (type.equals(DateFieldType.DAY)) {
                difference = Months.monthsBetween(toDT, fromDT).getMonths();
            } else if (type.equals(DateFieldType.DAY)) {
                difference = Years.yearsBetween(toDT, fromDT).getYears();
            }
        }
        return difference;
    }

    public String dateTimeFormatForOracle(String dateStr) {
        return "To_Date('" + dateStr + "','YYYY-MM-DD')";
    }

    public boolean isInterval(String time1, String time2, String time3) {
        if (compareTime(time1, time3) <= 0 && compareTime(time2, time3) >= 0) {
            return true;
        }
        return false;
    }
}
