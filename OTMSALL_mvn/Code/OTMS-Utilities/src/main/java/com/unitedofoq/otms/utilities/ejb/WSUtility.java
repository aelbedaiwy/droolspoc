package com.unitedofoq.otms.utilities.ejb;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.function.WebServiceFunction;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.webservice.OWebService;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.utilities.ejb.WSUtilityLocal",
        beanInterface = WSUtilityLocal.class)
public class WSUtility implements WSUtilityLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private OFunctionServiceRemote functionService;

    @Override
    public String callingWebService(String webServiceCode, List webServiceData, OUser loggedUser) {
        String wsResult = "";
        try {
            WebServiceFunction wsFunction = (WebServiceFunction) oem.loadEntity(WebServiceFunction.class.getSimpleName(),
                    Collections.singletonList("code = '" + webServiceCode + "'"), null, loggedUser);
            if (wsFunction != null) {
                ODataType wsDT = wsFunction.getOdataType();
                ODataMessage wsDM = new ODataMessage(wsDT, webServiceData);

                OWebService webService = wsFunction.getWebService();
                OFunctionResult returnedOFR = functionService.runWebServiceFunction(webService, wsDM, null, loggedUser);
                ODataMessage returnedDM = returnedOFR.getReturnedDataMessage();
                if (returnedDM != null && returnedDM.getData()
                        != null && !returnedDM.getData().isEmpty() && returnedDM.getData().get(0) != null) {
                    wsResult = returnedDM.getData().get(0).toString();
                }
            } else {
                wsResult = "InValid Web Service Function";
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return wsResult;
    }
}
