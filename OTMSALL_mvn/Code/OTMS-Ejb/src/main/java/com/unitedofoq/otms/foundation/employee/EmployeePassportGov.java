/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"employee"})
@ChildEntity(fields = {"employeeResidencies"})
public class EmployeePassportGov extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeePassportGov_employee";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="number">
    @Column(nullable=false,name="passportGovNum")
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumberDD() {
        return "EmployeePassportGov_number";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDate;

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewalDateDD() {
        return "EmployeePassportGov_renewalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDateHijri;

    public Date getRenewalDateHijri() {
        return renewalDateHijri;
    }

    public void setRenewalDateHijri(Date renewalDateHijri) {
        this.renewalDateHijri = renewalDateHijri;
    }

    public String getRenewalDateHijriDD() {
        return "EmployeePassportGov_renewalDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cardDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardDate;

    public Date getCardDate() {
        return cardDate;
    }

    public void setCardDate(Date cardDate) {
        this.cardDate = cardDate;
    }

    public String getCardDateDD() {
        return "EmployeePassportGov_cardDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cardDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardDateHijri;

    public Date getCardDateHijri() {
        return cardDateHijri;
    }

    public void setCardDateHijri(Date cardDateHijri) {
        this.cardDateHijri = cardDateHijri;
    }

    public String getCardDateHijriDD() {
        return "EmployeePassportGov_cardDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entranceDate;

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public String getEntranceDateDD() {
        return "EmployeePassportGov_entranceDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entranceDateHijri;

    public Date getEntranceDateHijri() {
        return entranceDateHijri;
    }

    public void setEntranceDateHijri(Date entranceDateHijri) {
        this.entranceDateHijri = entranceDateHijri;
    }

    public String getEntranceDateHijriDD() {
        return "EmployeePassportGov_entranceDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entrancePort">
    @Column
    private String entrancePort;

    public String getEntrancePort() {
        return entrancePort;
    }

    public void setEntrancePort(String entrancePort) {
        this.entrancePort = entrancePort;
    }

    public String getEntrancePortDD() {
        return "EmployeePassportGov_entrancePort";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issuePlace">
    @Column
    private String issuePlace;

    public String getIssuePlace() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }

    public String getIssuePlaceDD() {
        return "EmployeePassportGov_issuePlace";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorNo">
    @Column
    private String guarantorNo;

    public String getGuarantorNo() {
        return guarantorNo;
    }

    public void setGuarantorNo(String guarantorNo) {
        this.guarantorNo = guarantorNo;
    }

    public String getGuarantorNoDD() {
        return "EmployeePassportGov_guarantorNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorName">
    @Column
    private String guarantorName;

    public String getGuarantorName() {
        return guarantorName;
    }

    public void setGuarantorName(String guarantorName) {
        this.guarantorName = guarantorName;
    }

    public String getGuarantorNameDD() {
        return "EmployeePassportGov_guarantorName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorAddress">
    @Column
    private String guarantorAddress;

    public String getGuarantorAddress() {
        return guarantorAddress;
    }

    public void setGuarantorAddress(String guarantorAddress) {
        this.guarantorAddress = guarantorAddress;
    }

    public String getGuarantorAddressDD() {
        return "EmployeePassportGov_guarantorAddress";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="guarantorTelephoneNo">
    @Column
    private String guarantorTelephoneNo;

    public String getGuarantorTelephoneNo() {
        return guarantorTelephoneNo;
    }

    public void setGuarantorTelephoneNo(String guarantorTelephoneNo) {
        this.guarantorTelephoneNo = guarantorTelephoneNo;
    }

    public String getGuarantorTelephoneNoDD() {
        return "EmployeePassportGov_guarantorTelephoneNo";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="employeeResidencies">
    @OneToMany(mappedBy="passport")
    List<EmployeeResidencyGov> employeeResidencies;

    public List<EmployeeResidencyGov> getEmployeeResidencies() {
        return employeeResidencies;
    }

    public void setEmployeeResidencies(List<EmployeeResidencyGov> employeeResidencies) {
        this.employeeResidencies = employeeResidencies;
    }

    public String getEmployeeResidenciesDD() {
        return "EmployeePassportGov_employeeResidencies";
    }
    //</editor-fold >
}
