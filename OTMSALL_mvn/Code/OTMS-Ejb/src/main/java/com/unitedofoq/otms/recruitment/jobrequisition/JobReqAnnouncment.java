package com.unitedofoq.otms.recruitment.jobrequisition;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields={"jobRequisition"})
public class JobReqAnnouncment extends BaseEntity{

    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
	private Date announcmentDate;
    public String getAnnouncmentDateDD(){
        return "JobReqAnnouncment_announcmentDate";
    }
    @Column(nullable=false)
	private String description;
    public String getDescriptionDD(){
        return "JobReqAnnouncment_description";
    }
    @Column(nullable=false)
	private double cost;
    public String getCostDD(){
        return "JobReqAnnouncment_cost";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private UDC type;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Employee orderedBy;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobRequisition_DBID", nullable=false)
    private JobRequisition jobRequisition;

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public Date getAnnouncmentDate() {
        return announcmentDate;
    }

    public void setAnnouncmentDate(Date announcmentDate) {
        this.announcmentDate = announcmentDate;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Employee getOrderedBy() {
        return orderedBy;
    }

    public void setOrderedBy(Employee orderedBy) {
        this.orderedBy = orderedBy;
    }

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }
}