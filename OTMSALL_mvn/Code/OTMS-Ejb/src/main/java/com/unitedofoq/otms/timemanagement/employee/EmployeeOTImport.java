package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@Entity
public class EmployeeOTImport extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeOTImport_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public String getCalculatedPeriodDD() {
        return "EmployeeOTImport_calculatedPeriod";
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestedDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestedDate;

    public Date getRequestedDate() {
        return requestedDate;
    }

    public String getRequestedDateDD() {
        return "EmployeeOTImport_requestedDate";
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="otValue">
    private BigDecimal otValue;

    public BigDecimal getOtValue() {
        return otValue;
    }

    public String getOtValueDD() {
        return "EmployeeOTImport_otValue";
    }

    public void setOtValue(BigDecimal otValue) {
        this.otValue = otValue;
    }
    @Transient
    private BigDecimal otValueMask;

    public BigDecimal getOtValueMask() {
        otValueMask = otValue;
        return otValueMask;
    }

    public String getOtValueMaskDD() {
        return "EmployeeOTImport_otValueMask";
    }

    public void setOtValueMask(BigDecimal otValueMask) {
        updateDecimalValue("otValue", otValueMask);
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public String getCommentsDD() {
        return "EmployeeOTImport_comments";
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public String getDoneDD() {
        return "EmployeeOTImport_done";
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    // </editor-fold >
}
