package com.unitedofoq.otms.recruitment.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tbadran
 */
@Entity
@ParentEntity(fields = {"employeeHiringDocument"})
public class EmployeeHiringDocumentDetails extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employeeHiringDocument">

    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private EmployeeHiringDocument employeeHiringDocument;

    public EmployeeHiringDocument getEmployeeHiringDocument() {
        return employeeHiringDocument;
    }

    public void setEmployeeHiringDocument(EmployeeHiringDocument employeeHiringDocument) {
        this.employeeHiringDocument = employeeHiringDocument;
    }

    public String getEmployeeHiringDocumentDD() {
        return "EmployeeHiringDocumentDetails_employeeHiringDocument";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statusType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private UDC statusType;

    public UDC getStatusType() {
        return statusType;
    }

    public void setStatusType(UDC statusType) {
        this.statusType = statusType;
    }

    public String getStatusTypeDD() {
        return "EmployeeHiringDocumentDetails_statusType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statusDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusDate;

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusDateDD() {
        return "EmployeeHiringDocumentDetails_statusDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="owner">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Employee owner;

    public Employee getOwner() {
        return owner;
    }

    public void setOwner(Employee owner) {
        this.owner = owner;
    }

    public String getOwnerDD() {
        return "EmployeeHiringDocumentDetails_owner";
    }
    // </editor-fold>
}
