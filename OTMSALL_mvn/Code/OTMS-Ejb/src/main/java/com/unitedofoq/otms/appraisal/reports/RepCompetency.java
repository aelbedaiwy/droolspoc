/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepCompetency extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepCompetency_dsDbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="competencyDescription">
    @Column
    @Translatable(translationField = "competencyDescriptionTranslated")
    private String competencyDescription;

    public void setCompetencyDescription(String competencyDescription) {
        this.competencyDescription = competencyDescription;
    }

    public String getCompetencyDescription() {
        return competencyDescription;
    }

    public String getCompetencyDescriptionDD() {
        return "RepCompetency_competencyDescription";
    }
    
    @Transient
    @Translation(originalField = "competencyDescription")
    private String competencyDescriptionTranslated;

    public String getCompetencyDescriptionTranslated() {
        return competencyDescriptionTranslated;
    }

    public void setCompetencyDescriptionTranslated(String competencyDescriptionTranslated) {
        this.competencyDescriptionTranslated = competencyDescriptionTranslated;
    }
    
    public String getCompetencyDescriptionTranslatedDD() {
        return "RepCompetency_competencyDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="competencyName">
    @Column
    @Translatable(translationField = "competencyNameTranslated")
    private String competencyName;

    public String getCompetencyName() {
        return competencyName;
    }

    public void setCompetencyName(String competencyName) {
        this.competencyName = competencyName;
    }
    
    public String getCompetencyNameDD() {
        return "RepCompetency_competencyName";
    }
    
    @Transient
    @Translation(originalField = "competencyName")
    private String competencyNameTranslated;

    public String getCompetencyNameTranslated() {
        return competencyNameTranslated;
    }

    public void setCompetencyNameTranslated(String competencyNameTranslated) {
        this.competencyNameTranslated = competencyNameTranslated;
    }
    
    public String getCompetencyNameTranslatedDD() {
        return "RepCompetency_competencyName";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="competencyGroupName">
    @Translatable(translationField = "competencyGroupNameTranslated")
    private String competencyGroupName;

    public String getCompetencyGroupName() {
        return competencyGroupName;
    }

    public void setCompetencyGroupName(String competencyGroupName) {
        this.competencyGroupName = competencyGroupName;
    }
    
    public String getCompetencyGroupNameDD() {
        return "RepCompetency_competencyGroupName";
    }
    
    @Transient
    @Translation(originalField = "competencyGroupName")
    private String competencyGroupNameTranslated;

    public String getCompetencyGroupNameTranslated() {
        return competencyGroupNameTranslated;
    }

    public void setCompetencyGroupNameTranslated(String competencyGroupNameTranslated) {
        this.competencyGroupNameTranslated = competencyGroupNameTranslated;
    }
    
    public String getCompetencyGroupNameTranslatedDD() {
        return "RepCompetency_competencyGroupName";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ratingSchema">
    @Translatable(translationField="ratingSchemaTranslated")
    private String ratingSchema;

    public String getRatingSchema() {
        return ratingSchema;
    }

    public void setRatingSchema(String ratingSchema) {
        this.ratingSchema = ratingSchema;
    }
    
    public String getRatingSchemaDD() {
        return "RepCompetency_ratingSchema";
    }
    @Transient
    @Translation(originalField="ratingSchema")
    private String ratingSchemaTranslated;

    public String getRatingSchemaTranslated() {
        return ratingSchemaTranslated;
    }

    public void setRatingSchemaTranslated(String ratingSchemaTranslated) {
        this.ratingSchemaTranslated = ratingSchemaTranslated;
    }
    
    public String getRatingSchemaTranslatedDD() {
        return "RepCompetency_ratingSchema";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyIDDD() {
        return "RepCompetency_companyID";
    }
    // </editor-fold>
}