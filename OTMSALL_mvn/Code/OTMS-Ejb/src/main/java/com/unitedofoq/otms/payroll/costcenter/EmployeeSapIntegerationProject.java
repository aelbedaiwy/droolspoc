/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author A.Alaa
 */
@Entity
@ParentEntity (fields="employee")
public class EmployeeSapIntegerationProject extends BaseEntity{

    //<editor-fold defaultstate="collapse" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;
    
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD() {
        return "EmployeeSapIntegerationProject_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="sapProject">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY) 
    private SapProject sapProject;

    public SapProject getSapProject() {
        return sapProject;
    }

    public void setSapProject(SapProject sapProject) {
        this.sapProject = sapProject;
    }
    public String getSapProjectDD() {
        return "EmployeeSapIntegerationProject_sapProject";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="precentage">
    @Column(precision=25, scale=13)
    private BigDecimal precentage;

    public BigDecimal getPrecentage() {
        return precentage;
    }

    public void setPrecentage(BigDecimal precentage) {
        this.precentage = precentage;
    }
    public String getPrecentageDD() {
        return "EmployeeSapIntegerationProject_precentage";
    }
    
    
    @Transient
    private BigDecimal precentageMask;
   
    public BigDecimal getPrecentageMask() {
        precentageMask = precentage ;
        return precentageMask;
    }

    public void setPrecentageMask(BigDecimal precentageMask) {
        updateDecimalValue("precentage",precentageMask);
    }
    public String getPrecentageMaskDD() {
        return "EmployeeSapIntegerationProject_precentageMask";
    }
    
    
     //</editor-fold>
    
    //<editor-fold defaultstate="collapse" desc="wbs">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Wbs wbs;

    public Wbs getWbs() {
        return wbs;
    }

    public void setWbs(Wbs wbs) {
        this.wbs = wbs;
    }
     public String getWbsDD() {
        return "EmployeeSapIntegerationProject_wbs";
    }
     //</editor-fold>
     
}
