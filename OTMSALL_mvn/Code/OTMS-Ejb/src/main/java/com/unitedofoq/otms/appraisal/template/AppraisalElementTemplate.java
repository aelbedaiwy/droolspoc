package com.unitedofoq.otms.appraisal.template;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = "appraisalTemplate")
public class AppraisalElementTemplate extends AppraisalElementBase {

    //<editor-fold defaultstate="collapsed" desc="templateElement">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplate templateElement;

    public AppraisalTemplate getTemplateElement() {
        return templateElement;
    }

    public String getTemplateElementDD() {
        return "AppraisalElementTemplate_templateElement";
    }

    public void setTemplateElement(AppraisalTemplate templateElement) {
        this.templateElement = templateElement;
    }
    //</editor-fold >

}
