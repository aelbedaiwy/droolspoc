package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepUnitPositionTranslation extends RepCompanyLevelBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="parentPositionName">
    @Column
    private String parentPositionName;

    public void setParentPositionName(String parentPositionName) {
        this.parentPositionName = parentPositionName;
    }

    public String getParentPositionName() {
        return parentPositionName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    @Column
    private String costCenterGroupDescription;

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeStepDescription">
    @Column
    private String payGradeStepDescription;

    public void setPayGradeStepDescription(String payGradeStepDescription) {
        this.payGradeStepDescription = payGradeStepDescription;
    }

    public String getPayGradeStepDescription() {
        return payGradeStepDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="parentPositionEmployee">
    @Column
    private String parentPositionEmployee;

    public void setParentPositionEmployee(String parentPositionEmployee) {
        this.parentPositionEmployee = parentPositionEmployee;
    }

    public String getParentPositionEmployee() {
        return parentPositionEmployee;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee">
    @Column
    private String employee;

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getEmployee() {
        return employee;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
    // </editor-fold>

}
