package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.dayoff.EmployeeDayOffRequest;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("VACATION")
@ParentEntity(fields = {"employee"})
public class EmployeeVacationRequest extends EmployeeDayOffRequest {
    // <editor-fold defaultstate="collapsed" desc="vacation">

    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "EmployeeVacationRequest_vacation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="visaRequest">
    @Column(length = 1)
    private String visaRequest;

    public String getVisaRequest() {
        return visaRequest;
    }

    public void setVisaRequest(String visaRequest) {
        this.visaRequest = visaRequest;
    }

    public String getVisaRequestDD() {
        return "EmployeeVacationRequest_visaRequest";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ticket">
    @Column(length = 1)
    private String ticket;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getTicketDD() {
        return "EmployeeVacationRequest_ticket";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exits">
    @Column(length = 1)
    private String exits;

    public String getExits() {
        return exits;
    }

    public void setExits(String exits) {
        this.exits = exits;
    }

    public String getExitsDD() {
        return "EmployeeVacationRequest_exits";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="internationalTrip">
    @Column(length = 1)
    private String internationalTrip;

    public String getInternationalTrip() {
        return internationalTrip;
    }

    public void setInternationalTrip(String internationalTrip) {
        this.internationalTrip = internationalTrip;
    }

    public String getInternationalTripDD() {
        return "EmployeeVacationRequest_internationalTrip";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationEffect">
    @Column(precision = 18, scale = 0)
    private BigDecimal vacationEffect;//(name = "vacation_Effect")

    public BigDecimal getVacationEffect() {
        return vacationEffect;
    }

    public void setVacationEffect(BigDecimal vacationEffect) {
        this.vacationEffect = vacationEffect;
    }

    public String getVacationEffectDD() {
        return "EmployeeVacationRequest_vacationEffect";
    }
    @Transient
    private BigDecimal vacationEffectMask;

    public BigDecimal getVacationEffectMask() {
        vacationEffectMask = vacationEffect;
        return vacationEffectMask;
    }

    public void setVacationEffectMask(BigDecimal vacationEffectMask) {
        updateDecimalValue("vacationEffect", vacationEffectMask);
    }

    public String getVacationEffectMaskDD() {
        return "EmployeeVacationRequest_vacationEffectMask";
    }
    // </editor-fold>

    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeVacationRequest_m2mCheck";
    }

    @PostLoad
    public void postLoad() {
        setM2mCheck(true);
    }
    // <editor-fold defaultstate="collapsed" desc="daysOrHours">
    private boolean daysOrHours;

    public boolean isDaysOrHours() {
        return daysOrHours;
    }

    public void setDaysOrHours(boolean daysOrHours) {
        this.daysOrHours = daysOrHours;
    }

    public String getDaysOrHoursDD() {
        return "EmployeeVacationRequest_daysOrHours";
    }

    // </editor-fold >
}
