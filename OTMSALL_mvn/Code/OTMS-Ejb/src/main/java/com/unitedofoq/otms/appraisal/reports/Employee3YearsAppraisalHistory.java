package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Employee3YearsAppraisalHistory extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "employeeDbid")
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "Employee3YearsAppraisalHistory_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="finalRateCurrent">

    @Column(name = "finalRateCurrentDbid")
    private BigDecimal finalRateCurrent;

    public BigDecimal getFinalRateCurrent() {
        return finalRateCurrent;
    }

    public void setFinalRateCurrent(BigDecimal finalRateCurrent) {
        this.finalRateCurrent = finalRateCurrent;
    }

    public String getFinalRateCurrentDD() {
        return "Employee3YearsAppraisalHistory_finalRateCurrent";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="finalRatePreviousYear">

    @Column(name = "finalRatePreviousYearDbid")
    private BigDecimal finalRatePreviousYear;

    public BigDecimal getFinalRatePreviousYear() {
        return finalRatePreviousYear;
    }

    public void setFinalRatePreviousYear(BigDecimal finalRatePreviousYear) {
        this.finalRatePreviousYear = finalRatePreviousYear;
    }

    public String getFinalRatePreviousYearDD() {
        return "Employee3YearsAppraisalHistory_finalRatePreviousYear";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="finalRateYearBeforePrevious">

    @Column(name = "finrateyearbefpredbid")
    private BigDecimal finalRateYearBeforePrevious;

    public BigDecimal getFinalRateYearBeforePrevious() {
        return finalRateYearBeforePrevious;
    }

    public void setFinalRateYearBeforePrevious(BigDecimal finalRateYearBeforePrevious) {
        this.finalRateYearBeforePrevious = finalRateYearBeforePrevious;
    }

    public String getFinalRateYearBeforePreviousDD() {
        return "Employee3YearsAppraisalHistory_finalRateYearBeforePrevious";
    }
    //</editor-fold>

}
