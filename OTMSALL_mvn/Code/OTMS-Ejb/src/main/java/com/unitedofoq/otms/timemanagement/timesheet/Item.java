/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author 3dly
 */
@Entity
@ParentEntity(fields = "company")
public class Item extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="id">
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDD() {
        return "Item_id";
    }
    //</editor-fold>
    
    /*
     * description
     */
    @Translatable(translationField = "descriptionTranslated")
    @Column
    private String description;
    @Translation(originalField = "description")
    @Transient
    private String descriptionTranslated;

    //<editor-fold defaultstate="collapsed" desc="Setters">
    public void setDescription(String description) {
        this.description = description;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters">   
    public String getDescription() {
        return description;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public String getTemplateDescriptionTranslatedDD() {
        return "Item_description";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "ItemTranslation_company";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="item">
//    @OneToMany
//    private List<Item> item;
//
//    public List<Item> getItem() {
//        return item;
//    }
//
//    public void setItem(List<Item> item) {
//        this.item = item;
//    }
//    
//    public String getItemDD() {
//        return "Item_item";
//    }
    //</editor-fold>
}
