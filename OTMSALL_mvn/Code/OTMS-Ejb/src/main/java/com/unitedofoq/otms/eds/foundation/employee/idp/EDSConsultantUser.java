package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("CONSULTANT")
@ParentEntity(fields={"consultant"})
public class EDSConsultantUser extends OUser {
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="consultant_DBID")
	private EDSConsultant consultant;

    public EDSConsultant getConsultant() {
        return consultant;
    }

    public void setConsultant(EDSConsultant consultant) {
        this.consultant = consultant;
    }
    
}
