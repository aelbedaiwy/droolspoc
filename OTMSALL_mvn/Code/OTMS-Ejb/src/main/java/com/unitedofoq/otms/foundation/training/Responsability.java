/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@MappedSuperclass
public class  Responsability extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;

    public String getDescription() {
        return description;
    }
    public String getDescriptionTranslatedDD() {
        return "Responsability_description";
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="weight">
    @Column(precision=18, scale=3)
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
    public String getWeightDD() {
        return "Responsability_weight";
    }
    @Transient
    private BigDecimal weightMask;

    public BigDecimal getWeightMask() {
        weightMask = weight;
        return weightMask;
    }

    public void setWeightMask(BigDecimal weightMask) {
        updateDecimalValue("weight",weightMask);
    }
    public String getWeightMaskDD() {
        return "Responsability_weightMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxScore">
    @Column(precision=18, scale=3)
    private BigDecimal maxScore;

    public BigDecimal getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(BigDecimal maxScore) {
        this.maxScore = maxScore;
    }
    public String getMaxScoreDD() {
        return "Responsability_maxScore";
    }
    @Transient
    private BigDecimal maxScoreMask;

    public BigDecimal getMaxScoreMask() {
        maxScoreMask = maxScore ;
        return maxScoreMask;
    }

    public void setMaxScoreMask(BigDecimal maxScoreMask) {
        updateDecimalValue("maxScore",maxScoreMask);
    }
    public String getMaxScoreMaskDD() {
        return "Responsability_maxScoreMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minScore">
    @Column(precision=18, scale=3)
    private BigDecimal minScore;

    public BigDecimal getMinScore() {
        return minScore;
    }

    public void setMinScore(BigDecimal minScore) {
        this.minScore = minScore;
    }
    public String getMinScoreDD() {
        return "Responsability_minScore";
    }
    @Transient
    private BigDecimal minScoreMask;

    public BigDecimal getMinScoreMask() {
        minScoreMask = minScore;
        return minScoreMask;
    }

    public void setMinScoreMask(BigDecimal minScoreMask) {
        updateDecimalValue("minScore",minScoreMask);
    }
    public String getMinScoreMaskDD() {
        return "Responsability_minScoreMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="isAppraisalUse">
    @Column(length=1)
    private String isAppraisalUse ;

    public String getIsAppraisalUse() {
        return isAppraisalUse;
    }

    public void setIsAppraisalUse(String isAppraisalUse) {
        this.isAppraisalUse = isAppraisalUse;
    }

   
    public String getIsAppraisalUseDD() {
        return "Responsability_isAppraisalUse";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="isQualtitativeorQuantitative">
    @Column(length=1)
    private String isQualtitativeorQuantitative ;

    public String getIsQualtitativeorQuantitative() {
        return isQualtitativeorQuantitative;
    }

    public void setIsQualtitativeorQuantitative(String isQualtitativeorQuantitative) {
        this.isQualtitativeorQuantitative = isQualtitativeorQuantitative;
    }

    public String getIsQualtitativeorQuantitativeDD() {
        return "Responsability_isQualtitativeorQuantitative";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="templateScale">
    //Quality Or Quantity Or Comment
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC templateScale;

    public UDC getTemplateScale() {
        return templateScale;
    }

    public void setTemplateScale(UDC templateScale) {
        this.templateScale = templateScale;
    }

    public String getTemplateScaleDD() {
        return "Responsability_templateScale";
    }
    // </editor-fold>    
}
