/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.measure;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.job.Job;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
/**
 *
 * @author lahmed
 */
@Entity
public class QuantitativeGroup extends BusinessObjectBaseEntity {
    //<editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionDD() {
        return "QuantativeGroup_description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="weight">
    @Column(precision = 25, scale = 13)
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
    
    public String getWeightDD() {
        return "QuantativeGroup_weight";
    }
    
    @Transient
    private BigDecimal weightMasked;

    public BigDecimal getWeightMasked() {
        weightMasked = weight;
        return weightMasked;
    }

    public void setWeightMasked(BigDecimal weightMasked) {
        updateDecimalValue("weight",weightMasked);
    }

    public String getWeightMaskedDD() {
        return "QuantativeGroup_weightMasked";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="kPAGroup">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private QuantitativeGroup group;
    
    public QuantitativeGroup getGroup() {
        return group;
    }

    public void setGroup(QuantitativeGroup group) {
        this.group = group;
    }
    
    public String getGroupDD() {
        return "QuantativeGroup_group";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="kpa">
   /* @OneToMany
    private List<KPA> kpas;

    public List<KPA> getKpas() {
        return kpas;
    }

    public void setKpas(List<KPA> kpas) {
        this.kpas = kpas;
    }
    public String getKpasDD() {
        return "QuantativeGroup_kpas";
    
    }*/
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="kpaView">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private KPAView kpaView;

    public KPAView getKpaView() {
        return kpaView;
    }

    public void setKpaView(KPAView kpaView) {
        this.kpaView = kpaView;
    }

    
    public String getKpaViewDD() {
        return "QuantativeGroup_kpaView";
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job job;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
    
    public String getJobDD() {
        return "QuantativeGroup_job";
    }
    //</editor-fold>
}
