package com.unitedofoq.otms.recruitment.jobrequisition;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccupationElementRatingBase;
import com.unitedofoq.otms.foundation.occupation.Knowledge;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobRequisition"})
public class JobReqKnowledge extends OccupationElementRatingBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="knowledge_DBID", nullable=false)
    private Knowledge knowledge;
    public String getKnowledgeDD(){
        return "JobReqKnowledge_knowledge";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobRequisition_DBID", nullable=false)
    private JobRequisition jobRequisition;

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public Knowledge getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(Knowledge knowledge) {
        this.knowledge = knowledge;
    }

}