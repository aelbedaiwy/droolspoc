/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;

/**
 *
 * @author mohamed
 */
@Entity
public class EmployeeOTRImport extends BaseEntity {

    private String employeeCode;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dailyDate;

    private String timeFrom;

    private String timeTo;

    private boolean done;
    @Column(name = "comments")
    private String comment;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "EmployeeOTRImport_employeeCode";
    }

    public Date getDailyDate() {
        return dailyDate;
    }

    public void setDailyDate(Date dailyDate) {
        this.dailyDate = dailyDate;
    }

    public String getDailyDateDD() {
        return "EmployeeOTRImport_dailyDate";
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeFromDD() {
        return "EmployeeOTRImport_timeFrom";
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getTimeToDD() {
        return "EmployeeOTRImport_timeTo";
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "EmployeeOTRImport_done";
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentDD() {
        return "EmployeeOTRImport_comment";
    }
}
