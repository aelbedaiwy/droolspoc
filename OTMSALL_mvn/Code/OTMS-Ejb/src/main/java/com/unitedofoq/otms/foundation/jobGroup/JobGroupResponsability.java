/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.jobGroup;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.training.Responsability;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"jobGroup"})
public class JobGroupResponsability extends Responsability {
// <editor-fold defaultstate="collapsed" desc="jobGroup">
	@JoinColumn(nullable=false)
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private JobGroup jobGroup;
    public JobGroup getJobGroup() {
        return jobGroup;
    }
    public void setJobGroup(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }
    public String getJobGroupDD() {
        return "JobGroupResponsability_jobGroup";
    }
    // </editor-fold>
}