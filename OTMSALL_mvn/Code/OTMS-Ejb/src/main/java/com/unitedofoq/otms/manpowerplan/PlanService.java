/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.fabs.core.security.user.ORole;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.RoleUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.foundation.unit.UnitLevel;
import com.unitedofoq.otms.foundation.unit.UnitLevelIndex;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name="java:global/ofoq/com.unitedofoq.otms.manpowerplan.PlanServiceLocal",
    beanInterface=PlanServiceLocal.class)
public class PlanService implements PlanServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote userMessageService;
    @EJB
    protected EntityServiceLocal entityServiceLocal;

    @Override
    public OFunctionResult postCreatePlanInstance (ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
//            oem.getEM(loggedUser);
            
            PlanInstance planInstance = (PlanInstance)odm.getData().get(0);
            
            Company company = null;
            
            List<UnitPlan> unitPlans = new ArrayList<UnitPlan>();
            
            if(planInstance!=null){
                company = planInstance.getCompany();
                if(company!=null){
                    //<editor-fold defaultstate="collapsed" desc="1. Company Plan">
                    String planPrefix = company.getPrefix() + "Plan";
                    
                    CompanyPlan companyPlan = new CompanyPlan();
                    
                    companyPlan.setName(planInstance.getDescription());
                    companyPlan.setOriginalCompany_dbid(company.getDbid());
                    companyPlan.setPrefix(planPrefix);
                    companyPlan.setEmail(company.getEmail());
                    companyPlan.setCode(planInstance.getDescription());
                    companyPlan.setInstance(planInstance.getDbid());
                    
                    List<String> udcConds = new ArrayList<String>();
                    udcConds.add("type.code = 'CompanyType'");
                    udcConds.add("code = 'Plan'");
                    UDC planCompany = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConds, null, loggedUser);
                    companyPlan.setCompanyOrPlan(planCompany);
                    
                    entitySetupService.callEntityCreateAction(companyPlan, loggedUser);
                    //</editor-fold>
                    
                    //<editor-fold defaultstate="collapsed" desc="2. Unit Level Index">
                    List<String> conds = new ArrayList<String>();
                    conds.add("company.dbid = " + company.getDbid());
                    List<UnitLevelIndex> levelIndecies = (List<UnitLevelIndex>)oem.loadEntityList(
                            UnitLevelIndex.class.getSimpleName(), conds, null, null, loggedUser);
                    
                    if(levelIndecies!=null){
                        for (int i = 0; i < levelIndecies.size(); i++) {
                            UnitLevelIndexPlan levelIndexPlan = new UnitLevelIndexPlan();

                            levelIndexPlan.setOriginalLevelIndex_dbid(levelIndecies.get(i).getDbid());
                            levelIndexPlan.setCompany_dbid(companyPlan.getDbid());
                            levelIndexPlan.setLevelIndex(levelIndecies.get(i).getLevelIndex());
                            levelIndexPlan.setDescription(levelIndecies.get(i).getDescription());

                            entitySetupService.callEntityCreateAction(levelIndexPlan, loggedUser);
                        }
                    }
                    //</editor-fold>
                    
                    //<editor-fold defaultstate="collapsed" desc="3. Unit Level">
                    conds.clear();
                    conds.add("company.dbid = " + company.getDbid());
                    List<UnitLevel> unitLevels = (List<UnitLevel>)oem.loadEntityList(
                            UnitLevel.class.getSimpleName(), conds, null, null, loggedUser);
                    if(unitLevels!=null){
                        for (int i = 0; i < unitLevels.size(); i++) {
                            UnitLevelPlan levelPlan = new UnitLevelPlan();

                            levelPlan.setOriginalLevel_dbid(unitLevels.get(i).getDbid());
                            levelPlan.setCompany_dbid(companyPlan.getDbid());
                            levelPlan.setLevelId(unitLevels.get(i).getLevelId());
                            levelPlan.setDescription(unitLevels.get(i).getDescription());

                            entitySetupService.callEntityCreateAction(levelPlan, loggedUser);
                        }
                    }
                    //</editor-fold>
                    
                    //<editor-fold defaultstate="collapsed" desc="4. Unit">
                    conds.clear();
                    conds.add("company.dbid = " + company.getDbid());
                    List<String> sort = new ArrayList<String>();
                    sort.add("unitLevelindex.levelIndex");
                    List<Unit> units = (List<Unit>)oem.loadEntityList(
                            Unit.class.getSimpleName(), conds, null, sort, loggedUser);
                    if(units!=null){
                        for (int i = 0; i < units.size(); i++) {
                            UnitPlan unitPlan = new UnitPlan();
                            
                            unitPlan.setCompany_dbid(companyPlan.getDbid());
                            unitPlan.setName(units.get(i).getName());
                            unitPlan.setOriginalUnit_dbid(units.get(i).getDbid());
                            
                            // get paret unit
                            if(units.get(i).getParentUnit()!=null)
                            {
                                Long originalParentUnit_dbid = units.get(i).getParentUnit().getDbid();
                                long newParentUnit_dbid = Long.valueOf(""+oem.executeEntityNativeQuery(
                                        "Select dbid From Unit where originalUnit_dbid = " + 
                                        originalParentUnit_dbid +" and company_dbid = " + companyPlan.getDbid(), loggedUser));
                                unitPlan.setParentUnit_dbid(newParentUnit_dbid);
                            }
                            
                            //unitPlan.setParentUnit_dbid
                            
                            if(units.get(i).getHierarchycode()!=null)
                            {
                                String hierarchyCode = units.get(i).getHierarchycode().replace(company.getPrefix(), companyPlan.getPrefix());
                                unitPlan.setHierarchycode(hierarchyCode);
                            }
                            
                            // get new levelIndex
                            if(units.get(i).getUnitLevelindex()!=null){
                                Long originalLevelIndex = units.get(i).getUnitLevelindex().getDbid();
                                Long newLevelIndex_dbid = Long.valueOf(""+oem.executeEntityNativeQuery(
                                        "Select dbid From UnitLevelIndex where originalLevelIndex_dbid = " + 
                                        originalLevelIndex +" and company_dbid = " + companyPlan.getDbid(), loggedUser));
                                unitPlan.setUnitLevelIndex_dbid(newLevelIndex_dbid);
                            }
                            
                            // get new level
                            if(units.get(i).getUnitLevel()!=null){
                                Long originalLevel = units.get(i).getUnitLevel().getDbid();
                                Long newLevel_dbid = Long.valueOf(oem.executeEntityNativeQuery(
                                        "Select dbid From UnitLevel where originalLevel_dbid = " + 
                                        originalLevel+" and company_dbid = " + companyPlan.getDbid(), loggedUser).toString());
                                
                                unitPlan.setUnitLevel_dbid(newLevel_dbid);
                            }
                            unitPlans.add(unitPlan);
                            
                            entitySetupService.callEntityCreateAction(unitPlan, loggedUser);
                        }
                    }
                    //</editor-fold>
                    
                    //<editor-fold defaultstate="collapsed" desc="5. Position">
                    //if(unitPlans!=null){
                        //for (int i = 0; i < unitPlans.size(); i++) {
                            List<String> positionConds = new ArrayList<String>();
                            positionConds.add("unit.company.dbid = " + company.getDbid());
                            
                            sort.clear();
                            sort.add("unit.unitLevelindex.levelIndex");
                            List<Position> positions = oem.loadEntityList(Position.class.getSimpleName(), positionConds, 
                                    null, null, loggedUser);
                            
                            //<editor-fold defaultstate="collapsed" desc="sort by hierarchy code">
                            //mergeSort_srt(positions, 0, positions.size());
                            
                            class HierarchyCodeComparable implements Comparator<Object> {

                                @Override
                                public int compare(Object o1, Object o2) {
                                    if (((Position) o1).getHierarchycode().replaceAll("[^.]", "").length()
                                            <= ((Position) o2).getHierarchycode().replaceAll("[^.]", "").length()) {
                                        return 0;
                                    }
                                    return 1;
                                }
                            }
                            Collections.sort(positions,new HierarchyCodeComparable());
//                            sortedPositions.set(0, positions.get(0));
//                            for (int i = 1; i < positions.size(); i++) {
//                                String hierarchyCode = positions.get(i).getHierarchycode();
//                                Integer numberOfPositions = hierarchyCode.replaceAll("[^.]", "").length();
//                                for (int j = 0; j < sortedPositions.size(); j++) {
//                                    if(numberOfPositions<=sortedPositions.get(j).getHierarchycode().replaceAll("[^.]", "").length())
//                                    {
//                                        sortedPositions.add(j, positions.get(i));
//                                        break;
//                                    }
//                                }
//                            }
                            //</editor-fold>
                            if(positions!=null){                
                            List<String>codes = new ArrayList<String>();
                            for (int i = 0; i < positions.size(); i++) {
                                //if(positions.get(i).getName().equals("Quality Assuarance Manager"))
                                    codes.add(i + "  " +positions.get(i).getHierarchycode());
                            }
                            
                            
                                for (int j = 0; j < positions.size(); j++) {
                                    PositionPlan positionPlan = new PositionPlan();
                                    
                                    positionPlan.setName(positions.get(j).getName());
                                    positionPlan.setOriginalPosition_dbid(positions.get(j).getDbid());
                                    
                                    long originalUnit_dbid = Long.valueOf("" + oem.executeEntityNativeQuery(
                                            "select position.unit_dbid from position "
                                            + "where dbid = " + positions.get(j).getDbid()
                                            + "", loggedUser));
                                    long unit_dbid = Long.valueOf("" + oem.executeEntityNativeQuery(
                                            "select dbid from unit "
                                            + "where originalUnit_dbid = " + originalUnit_dbid
                                            + " and company_dbid = " + companyPlan.getDbid(), loggedUser));
                                    
                                    positionPlan.setUnit_dbid(unit_dbid);
                                    positionPlan.setHeadcount(positions.get(j).getHeadcount());
                                    
                                    // get paret unit
                                    if(positions.get(j).getParentPositionSimpleMode()!=null)
                                    {
                                        Long originalParentPosition_dbid = positions.get(j).getParentPositionSimpleMode().getDbid();
                                        long newParentPosition_dbid = Long.valueOf(""+oem.executeEntityNativeQuery(
                                                "select position.dbid from position "
                                                + "where originalPosition_dbid = " + originalParentPosition_dbid
                                                + " and unit_dbid in (select dbid from unit where company_dbid = " +
                                                companyPlan.getDbid() + ")", loggedUser));
                                        /*List<String> tempConds = new ArrayList<String>();
                                        tempConds.add("originalPosition_dbid = " + originalParentPosition_dbid);
                                        tempConds.add("unit_dbid =");
                                        PositionPlan plan = (PositionPlan)oem.loadEntity(PositionPlan.class.getSimpleName(), tempConds, null, loggedUser);
                                        newParentPosition_dbid = plan.getDbid();*/
                                        positionPlan.setParentPosition_dbid(newParentPosition_dbid);
                                    }
                                    
                                    if(positions.get(j).getHierarchycode()!=null){
                                        String hierarchyCode = positions.get(j).getHierarchycode().replace(company.getPrefix(), companyPlan.getPrefix());
                                        positionPlan.setHierarchycode(hierarchyCode);
                                    }
                                    
                                    entitySetupService.callEntityCreateAction(positionPlan, loggedUser);
                                }
                            }
                        //}
                    //}
                    //</editor-fold>
                }
            }
            
        }catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult applyPlanInstance (ODataMessage odm, OFunctionParms params, OUser loggedUser){
        OFunctionResult ofr = new OFunctionResult();        
        try {
//            oem.getEM(loggedUser);
            PlanInstance planInstance = (PlanInstance)odm.getData().get(0);
            
            if(planInstance != null){
                Company originalCompany = planInstance.getCompany();
                
                if(originalCompany!=null){
                    Long originalCompany_dbid = originalCompany.getDbid();
                    
                    List<String> conds = new ArrayList<String>();
                    conds.add("originalCompany_dbid = " + originalCompany_dbid);
                    conds.add("instance = " + planInstance.getDbid());
                    CompanyPlan companyPlan = (CompanyPlan)oem.loadEntity(CompanyPlan.class.getSimpleName(), conds, null, loggedUser);
                    
                    if(companyPlan!=null)
                    {
                        Long companyPlan_dbid = companyPlan.getDbid();
                        //<editor-fold defaultstate="collapsed" desc="1. Unit Level Index">
                        List<String> levelIndexConds = new ArrayList<String>();
                        levelIndexConds.add("company_dbid = " + companyPlan_dbid);
                        List<UnitLevelIndexPlan> levelIndexPlans = 
                                oem.loadEntityList(UnitLevelIndexPlan.class.getSimpleName(), levelIndexConds, null, null, loggedUser);
                        
                        levelIndexConds = new ArrayList<String>();
                        levelIndexConds.add("company_dbid = " + originalCompany_dbid);
                        List<UnitLevelIndexPlan> originalLevelIndex = 
                                oem.loadEntityList(UnitLevelIndexPlan.class.getSimpleName(), levelIndexConds, null, null, loggedUser);
                        
                        if(levelIndexPlans!=null){

                            List<UnitLevelIndexPlan> toBeRemovedindexPlans =new ArrayList<UnitLevelIndexPlan>();
                            
                            //To be removed
                            for (int i = 0; i < originalLevelIndex.size(); i++) {
                                boolean exist =false;
                                for (int j = 0; j < levelIndexPlans.size(); j++) {                                    
                                    if(levelIndexPlans.get(j).getOriginalLevelIndex_dbid()!=null &&
                                            originalLevelIndex.get(i).getDbid() == levelIndexPlans.get(j).getOriginalLevelIndex_dbid()){
                                        exist = true;
                                        break;
                                    }
                                }
                                if(!exist){
                                    toBeRemovedindexPlans.add(originalLevelIndex.get(i));
                                }
                            }
                            
                            //Adding New
                            for (int i = 0; i < levelIndexPlans.size(); i++) {
                                if(levelIndexPlans.get(i).getOriginalLevelIndex_dbid()==null){ //New
                                    UnitLevelIndexPlan newLevelIndexPlan = new UnitLevelIndexPlan();
                                    newLevelIndexPlan.setCompany_dbid(originalCompany.getDbid());
                                    newLevelIndexPlan.setDTYPE(levelIndexPlans.get(i).getDTYPE());
                                    newLevelIndexPlan.setDescription(levelIndexPlans.get(i).getDescription());
                                    newLevelIndexPlan.setLevelIndex(levelIndexPlans.get(i).getLevelIndex());                             
                                    entitySetupService.callEntityCreateAction(newLevelIndexPlan, loggedUser);
                                    levelIndexPlans.get(i).setOriginalLevelIndex_dbid(newLevelIndexPlan.getDbid());
                                    entitySetupService.callEntityUpdateAction(levelIndexPlans.get(i), loggedUser);
                                }
                            }
                                                        
                            //Refreshing
                            levelIndexConds = new ArrayList<String>();
                            levelIndexConds.add("company_dbid = " + companyPlan_dbid);
                            levelIndexPlans = 
                                    oem.loadEntityList(UnitLevelIndexPlan.class.getSimpleName(), levelIndexConds, null, null, loggedUser);
                                                        
                            //Updating
                            for (int i = 0; i < levelIndexPlans.size(); i++) {
                                UnitLevelIndexPlan originalIndex = (UnitLevelIndexPlan) oem.loadEntity(UnitLevelIndexPlan.class.getSimpleName()
                                        ,levelIndexPlans.get(i).getOriginalLevelIndex_dbid(), null, null, loggedUser);
                                
                                    originalIndex.setCompany_dbid(originalCompany.getDbid());
                                    originalIndex.setDTYPE(levelIndexPlans.get(i).getDTYPE());
                                    originalIndex.setDescription(levelIndexPlans.get(i).getDescription());
                                    originalIndex.setLevelIndex(levelIndexPlans.get(i).getLevelIndex());                             
                                    entitySetupService.callEntityCreateAction(originalIndex, loggedUser);
                            }
                                                        
                            //Deleting
                            for (int i = 0; i < toBeRemovedindexPlans.size(); i++) {
                                oem.deleteEntity(toBeRemovedindexPlans.get(i), loggedUser);
                            }
                            
                            /////////////////////////Unit Level Index Finished////////////////////////////////////
                            
                        }
                        //</editor-fold>
                        
                        //<editor-fold defaultstate="collapsed" desc="2. Unit Level">
                        List<String> levelConds = new ArrayList<String>();
                        levelConds.add("company_dbid = " + companyPlan_dbid);
                        List<UnitLevelPlan> levelPlans = 
                                oem.loadEntityList(UnitLevelPlan.class.getSimpleName(), levelConds, null, null, loggedUser);
                        
                        levelConds = new ArrayList<String>();
                        levelConds.add("company_dbid = " + originalCompany_dbid);
                        List<UnitLevelPlan> originalLevels = 
                                oem.loadEntityList(UnitLevelPlan.class.getSimpleName(), levelConds, null, null, loggedUser);
                        
                        if(levelPlans!=null){

                            List<UnitLevelPlan> toBeRemovedLevelPlans =new ArrayList<UnitLevelPlan>();
                            
                            //To be removed
                            for (int i = 0; i < originalLevels.size(); i++) {
                                boolean exist =false;
                                for (int j = 0; j < levelPlans.size(); j++) {                                    
                                    if(levelPlans.get(j).getOriginalLevel_dbid()!=null && originalLevels.get(i).getDbid() == levelPlans.get(j).getOriginalLevel_dbid()){
                                        exist = true;
                                        break;
                                    }
                                }
                                if(!exist){
                                    toBeRemovedLevelPlans.add(originalLevels.get(i));
                                }
                            }
                            
                            //Adding New
                            for (int i = 0; i < levelPlans.size(); i++) {
                                if(levelPlans.get(i).getOriginalLevel_dbid()==null){ //New
                                    UnitLevelPlan newLevelPlan = new UnitLevelPlan();
                                    newLevelPlan.setCompany_dbid(originalCompany.getDbid());
                                    newLevelPlan.setDTYPE(levelPlans.get(i).getDTYPE());
                                    newLevelPlan.setDescription(levelPlans.get(i).getDescription());
                                    newLevelPlan.setLevelId(levelPlans.get(i).getLevelId());                             
                                    entitySetupService.callEntityCreateAction(newLevelPlan, loggedUser);
                                    levelPlans.get(i).setOriginalLevel_dbid(newLevelPlan.getDbid());
                                    entitySetupService.callEntityUpdateAction(levelPlans.get(i), loggedUser);
                                }
                            }
                                                        
                            //Refreshing
                            levelConds = new ArrayList<String>();
                            levelConds.add("company_dbid = " + companyPlan_dbid);
                            levelPlans = 
                                    oem.loadEntityList(UnitLevelPlan.class.getSimpleName(), levelConds, null, null, loggedUser);
                                                        
                            //Updating
                            for (int i = 0; i < levelPlans.size(); i++) {
                                UnitLevelPlan originalLevel = (UnitLevelPlan) oem.loadEntity(UnitLevelPlan.class.getSimpleName()
                                        ,levelPlans.get(i).getOriginalLevel_dbid(), null, null, loggedUser);
                                
                                    originalLevel.setCompany_dbid(originalCompany.getDbid());
                                    originalLevel.setDTYPE(levelPlans.get(i).getDTYPE());
                                    originalLevel.setDescription(levelPlans.get(i).getDescription());
                                    originalLevel.setLevelId(levelPlans.get(i).getLevelId());                             
                                    entitySetupService.callEntityCreateAction(originalLevel, loggedUser);
                            }
                                                        
                            //Deleting those which are to be removed
                            for (int i = 0; i < toBeRemovedLevelPlans.size(); i++) {
                                oem.deleteEntity(toBeRemovedLevelPlans.get(i), loggedUser);
                            }
                        }
                        //</editor-fold>
                        
                        //<editor-fold defaultstate="collapsed" desc="3. Unit">
                        List<String> unitConds = new ArrayList<String>();
                        unitConds.add("company_dbid = " + companyPlan_dbid);
                        List<UnitPlan> unitPlans = 
                                oem.loadEntityList(UnitPlan.class.getSimpleName(), unitConds, null, null, loggedUser);
                        
                        unitConds = new ArrayList<String>();
                        unitConds.add("company_dbid = " + originalCompany_dbid);
                        List<UnitPlan> originalUnits = 
                                oem.loadEntityList(UnitPlan.class.getSimpleName(), unitConds, null, null, loggedUser);
                        
                        if(unitPlans!=null){

                            List<UnitPlan> toBeRemovedUnitPlans =new ArrayList<UnitPlan>();
                            
                            //To be removed
                            for (int i = 0; i < originalUnits.size(); i++) {
                                boolean exist =false;
                                for (int j = 0; j < unitPlans.size(); j++) {                                    
                                    if(unitPlans.get(j).getOriginalUnit_dbid()!=null && originalUnits.get(i).getDbid() == unitPlans.get(j).getOriginalUnit_dbid()){
                                        exist = true;
                                        break;
                                    }
                                }
                                if(!exist){
                                    toBeRemovedUnitPlans.add(originalUnits.get(i));
                                }
                            }
                            
                            //Adding New
                            for (int i = 0; i < unitPlans.size(); i++) {
                                if(unitPlans.get(i).getOriginalUnit_dbid()==null){ //New
                                    UnitPlan newPlan = new UnitPlan();
                                    newPlan.setCompany_dbid(originalCompany.getDbid());
                                    newPlan.setDTYPE(unitPlans.get(i).getDTYPE());
                                    newPlan.setName(unitPlans.get(i).getName());
                                    
                                    UnitPlan parentUnitPlan = (UnitPlan) oem.loadEntity(UnitPlan.class.getSimpleName(), unitPlans.get(i).getParentUnit_dbid(),null, null, loggedUser);
                                    newPlan.setParentUnit_dbid(parentUnitPlan.getOriginalUnit_dbid());
                                    
                                    if(unitPlans.get(i).getHierarchycode()!=null){
                                        String hierarchyCode = unitPlans.get(i).getHierarchycode().replace(companyPlan.getPrefix(), originalCompany.getPrefix());
                                        newPlan.setHierarchycode(hierarchyCode);
                                    }
                                    
                                    if(unitPlans.get(i).getUnitLevelIndex_dbid()!=null){
                                        UnitLevelIndexPlan indexPlan = (UnitLevelIndexPlan) oem.loadEntity(UnitLevelIndexPlan.class.getSimpleName(), unitPlans.get(i).getUnitLevelIndex_dbid(),null, null, loggedUser);
                                        newPlan.setUnitLevelIndex_dbid(indexPlan.getOriginalLevelIndex_dbid());
                                    }
                                    
                                    if(unitPlans.get(i).getUnitLevel_dbid()!=null){
                                        UnitLevelPlan levelPlan = (UnitLevelPlan) oem.loadEntity(UnitLevelPlan.class.getSimpleName(), unitPlans.get(i).getUnitLevel_dbid(),null, null, loggedUser);
                                        newPlan.setUnitLevel_dbid(levelPlan.getOriginalLevel_dbid());
                                    }
                                    
                                    entitySetupService.callEntityCreateAction(newPlan, loggedUser);
                                    unitPlans.get(i).setOriginalUnit_dbid(newPlan.getDbid());
                                    entitySetupService.callEntityUpdateAction(unitPlans.get(i), loggedUser);
                                }
                            }
                                                        
                            //Refreshing
                            unitConds = new ArrayList<String>();
                            unitConds.add("company_dbid = " + companyPlan_dbid);
                            unitPlans = 
                                    oem.loadEntityList(UnitPlan.class.getSimpleName(), unitConds, null, null, loggedUser);
                                                        
                            //Updating
                            for (int i = 0; i < unitPlans.size(); i++) {
                                UnitPlan originalUnit = (UnitPlan) oem.loadEntity(UnitPlan.class.getSimpleName()
                                        ,unitPlans.get(i).getOriginalUnit_dbid(), null, null, loggedUser);
                                
                                    originalUnit.setCompany_dbid(originalCompany.getDbid());
                                    originalUnit.setDTYPE(unitPlans.get(i).getDTYPE());
                                    originalUnit.setName(unitPlans.get(i).getName());  
                                    
                                    if(unitPlans.get(i).getParentUnit_dbid()!=null){
                                        UnitPlan parentUnitPlan = (UnitPlan) oem.loadEntity(UnitPlan.class.getSimpleName(), unitPlans.get(i).getParentUnit_dbid(),null, null, loggedUser);
                                        originalUnit.setParentUnit_dbid(parentUnitPlan.getOriginalUnit_dbid());
                                    }
                                    
                                    if(unitPlans.get(i).getHierarchycode()!=null){
                                        String hierarchyCode = unitPlans.get(i).getHierarchycode().replace(companyPlan.getPrefix(), originalCompany.getPrefix());
                                        originalUnit.setHierarchycode(hierarchyCode);
                                    }
                                    
                                    if(unitPlans.get(i).getUnitLevelIndex_dbid() != null){
                                        UnitLevelIndexPlan indexPlan = (UnitLevelIndexPlan) oem.loadEntity(UnitLevelIndexPlan.class.getSimpleName(), unitPlans.get(i).getUnitLevelIndex_dbid(),null, null, loggedUser);
                                        originalUnit.setUnitLevelIndex_dbid(indexPlan.getOriginalLevelIndex_dbid());
                                    }
                                    
                                    if(unitPlans.get(i).getUnitLevel_dbid()!=null){
                                        UnitLevelPlan levelPlan = (UnitLevelPlan) oem.loadEntity(UnitLevelPlan.class.getSimpleName(), unitPlans.get(i).getUnitLevel_dbid(),null, null, loggedUser);
                                        originalUnit.setUnitLevel_dbid(levelPlan.getOriginalLevel_dbid());
                                    }
                                    
                                    entitySetupService.callEntityCreateAction(originalUnit, loggedUser);
                            }
                                                        
                            //Deleting those which are toBeRemoved
                            for (int i = 0; i < toBeRemovedUnitPlans.size(); i++) {
                                oem.deleteEntity(toBeRemovedUnitPlans.get(i), loggedUser);
                            }
                        }
                        //</editor-fold>
                        
                        //<editor-fold defaultstate="collapsed" desc="4. Position">
                        List<PositionPlan> positionPlans = new ArrayList<PositionPlan>();
                        List<PositionPlan> originalPositions = new ArrayList<PositionPlan>();
                        List<String> positionConds = new ArrayList<String>();
                        
                        for (int i = 0; i < unitPlans.size(); i++) {
                            positionConds.clear();
                            positionConds.add("unit_dbid = " + unitPlans.get(i).getDbid());
                            positionPlans.addAll((List<PositionPlan>)oem.loadEntityList(PositionPlan.class.getSimpleName(), positionConds, null, null, loggedUser));
                        }
                        
                        for (int i = 0; i < originalUnits.size(); i++) {
                            positionConds.clear();
                            positionConds.add("unit_dbid = " + originalUnits.get(i).getDbid());
                            originalPositions.addAll((List<PositionPlan>)oem.loadEntityList(PositionPlan.class.getSimpleName(), positionConds, null, null, loggedUser));
                        }

                            List<PositionPlan> toBeRemovedPositionPlans =new ArrayList<PositionPlan>();
                            
                            //To be removed
                            for (int i = 0; i < originalPositions.size(); i++) {
                                boolean exist =false;
                                for (int j = 0; j < positionPlans.size(); j++) {                                    
                                    if(positionPlans.get(j).getOriginalPosition_dbid()!=null && originalPositions.get(i).getDbid() == positionPlans.get(j).getOriginalPosition_dbid()){
                                        exist = true;
                                        break;
                                    }
                                }
                                if(!exist){
                                    toBeRemovedPositionPlans.add(originalPositions.get(i));
                                }
                            }
                            
                            //Adding New
                            for (int i = 0; i < positionPlans.size(); i++) {
                                if(positionPlans.get(i).getOriginalPosition_dbid()==null){ //New
                                    PositionPlan newPlan = new PositionPlan();
                                    
                                    UnitPlan positionUnit = (UnitPlan) oem.loadEntity(UnitPlan.class.getSimpleName(), positionPlans.get(i).getUnit_dbid(),null, null, loggedUser);
                                    
                                    newPlan.setUnit_dbid(positionUnit.getOriginalUnit_dbid());
                                    newPlan.setDTYPE(positionPlans.get(i).getDTYPE());
                                    newPlan.setName(positionPlans.get(i).getName());
                                    
                                    PositionPlan parentPositionPlan = (PositionPlan) oem.loadEntity(PositionPlan.class.getSimpleName(), positionPlans.get(i).getParentPosition_dbid(),null, null, loggedUser);
                                    newPlan.setParentPosition_dbid(parentPositionPlan.getOriginalPosition_dbid());
                                    
                                    if(positionPlans.get(i).getHierarchycode()!=null){
                                        String hierarchyCode = positionPlans.get(i).getHierarchycode().replace(companyPlan.getPrefix(), originalCompany.getPrefix());
                                        newPlan.setHierarchycode(hierarchyCode);
                                    }
                                    
                                    entitySetupService.callEntityCreateAction(newPlan, loggedUser);
                                    positionPlans.get(i).setOriginalPosition_dbid(newPlan.getDbid());
                                    entitySetupService.callEntityUpdateAction(positionPlans.get(i), loggedUser);
                                }
                            }
                                                        
                            //Refreshing
                            positionPlans = new ArrayList<PositionPlan>();
                            for (int i = 0; i < unitPlans.size(); i++) {
                                positionConds.clear();
                                positionConds.add("unit_dbid = " + unitPlans.get(i).getDbid());
                                positionPlans.addAll((List<PositionPlan>)oem.loadEntityList(PositionPlan.class.getSimpleName(), positionConds, null, null, loggedUser));
                            }
                            
                            //Updating
                            for (int i = 0; i < positionPlans.size(); i++) {
                                PositionPlan originalPositionPlan = (PositionPlan) oem.loadEntity(PositionPlan.class.getSimpleName()
                                        ,positionPlans.get(i).getOriginalPosition_dbid(), null, null, loggedUser);
                                
                                    UnitPlan positionUnit = (UnitPlan) oem.loadEntity(UnitPlan.class.getSimpleName(), positionPlans.get(i).getUnit_dbid(),null, null, loggedUser);
                                    
                                    originalPositionPlan.setUnit_dbid(positionUnit.getOriginalUnit_dbid());
                                    originalPositionPlan.setDTYPE(positionPlans.get(i).getDTYPE());
                                    originalPositionPlan.setName(positionPlans.get(i).getName());
                                    
                                    PositionPlan parentPositionPlan = null;
                                    if(positionPlans.get(i).getParentPosition_dbid()!=null)
                                    {
                                        parentPositionPlan = (PositionPlan) oem.loadEntity(PositionPlan.class.getSimpleName(), positionPlans.get(i).getParentPosition_dbid(),null, null, loggedUser);
                                        originalPositionPlan.setParentPosition_dbid(parentPositionPlan.getOriginalPosition_dbid());
                                    }
                                    
                                    if(positionPlans.get(i).getHierarchycode()!=null){
                                        String hierarchyCode = positionPlans.get(i).getHierarchycode().replace(companyPlan.getPrefix(), originalCompany.getPrefix());
                                        originalPositionPlan.setHierarchycode(hierarchyCode);
                                    }
                                    
                                    entitySetupService.callEntityCreateAction(originalPositionPlan, loggedUser);
                            }
                                                        
                            //Deleting those which are toBeRemoved
                            for (int i = 0; i < toBeRemovedPositionPlans.size(); i++) {
                                oem.deleteEntity(toBeRemovedPositionPlans.get(i), loggedUser);
                            }
                        //</editor-fold>
                    }
                }
                
//                if(ofr.getErrors()!=null && ofr.getErrors().isEmpty()){
//                    ofr.addSuccess(userMessageService.getUserMessage("InstanceApplied", loggedUser));
//                }
//                else if(ofr.getErrors() ==null){
//                    ofr.addSuccess(userMessageService.getUserMessage("InstanceApplied", loggedUser));
//                }
                
            }
         }
        catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult addCompanyPlanUser(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();        
        CompanyPlan company = (CompanyPlan) odm.getData().get(0);
        try {
//            oem.getEM(loggedUser);
            OUser user = new OUser();
            user.setUserPrefrence1(company.getDbid());
            user.setLoginName(company.getName().trim().replaceAll("[^a-zA-Z0-9]+","").toLowerCase()+"admin");
            
            user.setEmail(company.getName().trim().replaceAll("[^a-zA-Z0-9]+","").toLowerCase()+"admin@"+company.getName().trim().replaceAll("[^a-zA-Z0-9]+","").toLowerCase()+".com" );
            
            
            UDC englishLang = (UDC) oem.loadEntity(UDC.class.getSimpleName(), 23, null, null, loggedUser);
                user.setFirstLanguage(englishLang);
            
            UDC arabicLang = (UDC) oem.loadEntity(UDC.class.getSimpleName(), 24, null, null, loggedUser);
                user.setSecondLanguage(arabicLang);
                
            
            user.setPassword("123456");
            
            OModule foundationModule = (OModule) oem.loadEntity(OModule.class.getSimpleName(),
                        Collections.singletonList("moduleTitle = 'FOUNDATION'"), null, loggedUser);
                
            user.setOmodule(foundationModule);
            
            OFunctionResult userOFR = entitySetupService.callEntityCreateAction(user, loggedUser);
            oFR.append(userOFR);
            
            
            if(oFR.getErrors().size()>0){
                return oFR;
            }
            
            String role1Name = "OTMSUser4E";
            String role2Name = "OTMSUser4M";
            String role3Name = "Admin";
            
            ORole role1 = (ORole) oem.loadEntity(ORole.class.getSimpleName(), Collections.singletonList("name ='"+role1Name+"'"), null, loggedUser);
            ORole role2 = (ORole) oem.loadEntity(ORole.class.getSimpleName(), Collections.singletonList("name ='"+role2Name+"'"), null, loggedUser);
            ORole role3 = (ORole) oem.loadEntity(ORole.class.getSimpleName(), Collections.singletonList("name ='"+role3Name+"'"), null, loggedUser);
            
            
            if(role1!=null){
                RoleUser roleUser = new RoleUser();
                roleUser.setOuser(user);
                roleUser.setOrole(role1);
                oFR.append(entitySetupService.callEntityCreateAction(roleUser, loggedUser));
            }
            if(role2!=null){
                RoleUser roleUser = new RoleUser();
                roleUser.setOuser(user);
                roleUser.setOrole(role2);
                oFR.append(entitySetupService.callEntityCreateAction(roleUser, loggedUser));
            }
            if(role3!=null){
                RoleUser roleUser = new RoleUser();
                roleUser.setOuser(user);
                roleUser.setOrole(role3);
                oFR.append(entitySetupService.callEntityCreateAction(roleUser, loggedUser));
            }
            
            List<String> params = new ArrayList<String>();
            params.add(user.getLoginName());
            params.add(user.getPassword());
            
            oFR.addSuccess(userMessageService.getUserMessage("UserCreation", params, loggedUser));
            
            
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
//            oem.closeEM(loggedUser);
        }

        return oFR;
    }
    
    }
