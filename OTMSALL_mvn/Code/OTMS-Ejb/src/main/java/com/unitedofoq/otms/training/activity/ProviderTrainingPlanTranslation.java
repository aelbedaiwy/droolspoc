/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author lap2
 */
@Entity
@Table(name= "provtrainplantranslation")
public class ProviderTrainingPlanTranslation extends BaseEntityTranslation{
    //<editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    //</editor-fold>    
}