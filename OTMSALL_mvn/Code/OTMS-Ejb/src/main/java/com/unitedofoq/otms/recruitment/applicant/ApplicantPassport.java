package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantPassport extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantPassport_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="passportNum">
    private String passportNum;

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getPassportNumDD() {
        return "ApplicantPassport_passportNum";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDate;

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewalDateDD() {
        return "ApplicantPassport_renewalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDateHijri;

    public Date getRenewalDateHijri() {
        return renewalDateHijri;
    }

    public void setRenewalDateHijri(Date renewalDateHijri) {
        this.renewalDateHijri = renewalDateHijri;
    }

    public String getRenewalDateHijriDD() {
        return "ApplicantPassport_renewalDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cardDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardDate;

    public Date getCardDate() {
        return cardDate;
    }

    public void setCardDate(Date cardDate) {
        this.cardDate = cardDate;
    }

    public String getCardDateDD() {
        return "ApplicantPassport_cardDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cardDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardDateHijri;

    public Date getCardDateHijri() {
        return cardDateHijri;
    }

    public void setCardDateHijri(Date cardDateHijri) {
        this.cardDateHijri = cardDateHijri;
    }

    public String getCardDateHijriDD() {
        return "ApplicantPassport_cardDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entranceDate;

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public String getEntranceDateDD() {
        return "ApplicantPassport_entranceDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entranceDateHijri;

    public Date getEntranceDateHijri() {
        return entranceDateHijri;
    }

    public void setEntranceDateHijri(Date entranceDateHijri) {
        this.entranceDateHijri = entranceDateHijri;
    }

    public String getEntranceDateHijriDD() {
        return "ApplicantPassport_entranceDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entrancePort">
    @Column
    private String entrancePort;

    public String getEntrancePort() {
        return entrancePort;
    }

    public void setEntrancePort(String entrancePort) {
        this.entrancePort = entrancePort;
    }

    public String getEntrancePortDD() {
        return "ApplicantPassport_entrancePort";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issuePlace">
    @Column
    private String issuePlace;

    public String getIssuePlace() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }

    public String getIssuePlaceDD() {
        return "ApplicantPassport_issuePlace";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorNo">
    @Column
    private String guarantorNo;

    public String getGuarantorNo() {
        return guarantorNo;
    }

    public void setGuarantorNo(String guarantorNo) {
        this.guarantorNo = guarantorNo;
    }

    public String getGuarantorNoDD() {
        return "ApplicantPassport_guarantorNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorName">
    @Column
    private String guarantorName;

    public String getGuarantorName() {
        return guarantorName;
    }

    public void setGuarantorName(String guarantorName) {
        this.guarantorName = guarantorName;
    }

    public String getGuarantorNameDD() {
        return "ApplicantPassport_guarantorName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorAddress">
    @Column
    private String guarantorAddress;

    public String getGuarantorAddress() {
        return guarantorAddress;
    }

    public void setGuarantorAddress(String guarantorAddress) {
        this.guarantorAddress = guarantorAddress;
    }

    public String getGuarantorAddressDD() {
        return "ApplicantPassport_guarantorAddress";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="guarantorTelephoneNo">
    @Column
    private String guarantorTelephoneNo;

    public String getGuarantorTelephoneNo() {
        return guarantorTelephoneNo;
    }

    public void setGuarantorTelephoneNo(String guarantorTelephoneNo) {
        this.guarantorTelephoneNo = guarantorTelephoneNo;
    }

    public String getGuarantorTelephoneNoDD() {
        return "ApplicantPassport_guarantorTelephoneNo";
    }
    // </editor-fold >
}