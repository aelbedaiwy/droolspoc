/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.unit;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mmohamed
 */
@Entity
@ReadOnly
public class UserAuthorizedUnits implements Serializable {
    @Column(name="OUSER_DBID")
    private Long user_DBID ;
    
    
    @Id
    @Column(name="UNIT_DBID")
    private Long unit_DBID ;

    public Long getUnit_DBID() {
        return unit_DBID;
    }

    public void setUnit_DBID(Long unit_DBID) {
        this.unit_DBID = unit_DBID;
    }    
        
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="Unit_DBID", updatable=false, insertable=false)
    private Unit unit ;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Long getUser_DBID() {
        return user_DBID;
    }

    public void setUser_DBID(Long user_DBID) {
        this.user_DBID = user_DBID;
    }
    
    
}
