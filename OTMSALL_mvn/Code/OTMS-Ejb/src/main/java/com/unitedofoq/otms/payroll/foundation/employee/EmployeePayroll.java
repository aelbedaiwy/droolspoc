package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.*;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeePayroll extends EmployeePayrollBase
{
    // <editor-fold defaultstate="collapsed" desc="employee">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;
    public Employee getEmployee() {
        return employee;
    }
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeePayroll_employee";
    }
    // </editor-fold>

    // new fields for check boxes (insurance/tax groups)
    // <editor-fold defaultstate="collapsed" desc="insuranceGrpCheck">
    private boolean insuranceGrpCheck;


    public boolean isInsuranceGrpCheck() {
        return insuranceGrpCheck;
    }

    public void setInsuranceGrpCheck(boolean insuranceGrpCheck) {
        this.insuranceGrpCheck = insuranceGrpCheck;
    }

    public String getInsuranceGrpCheckDD() {
        return "EmployeePayroll_insuranceGrpCheck";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxGroupCheck">

    private boolean taxGroupCheck;


    public boolean isTaxGroupCheck() {
        return taxGroupCheck;
    }

    public void setTaxGroupCheck(boolean taxGroupCheck) {
        this.taxGroupCheck = taxGroupCheck;
    }
    public String getTaxGroupCheckDD() {
        return "EmployeePayroll_taxGroupCheck";
    }

    // </editor-fold>
}
