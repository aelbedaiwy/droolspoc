/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElementType;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmpCostCenterException extends BaseEntity{
    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD() {
        return "EmpCostCenterException_employee";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;
    
    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }
    
    public String getCostCenterDD() {
        return "EmpCostCenterException_costCenter";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="distributionPercentage">
    @Column(precision=25, scale=13)
    private  BigDecimal distributionPercentage;

    public BigDecimal getDistributionPercentage() {
        return distributionPercentage;
    }

    public void setDistributionPercentage(BigDecimal distributionPercentage) {
        this.distributionPercentage = distributionPercentage;
    }
    
    public String getDistributionPercentageDD() {
        return "EmpCostCenterException_distributionPercentage";
    }
    @Transient
    private BigDecimal distributionPercentageMask;
    public BigDecimal getDistributionPercentageMask() {
        distributionPercentageMask = distributionPercentage ;
        return distributionPercentageMask;
    }
    public void setDistributionPercentageMask(BigDecimal distributionPercentageMask) {
        updateDecimalValue("distributionPercentage",distributionPercentageMask);
    }
    public String getDistributionPercentageMaskDD() {
        return "EmpCostCenterException_distributionPercentageMask";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="distributionDays">
    @Column(precision=25, scale=13)
    private  BigDecimal distributionDays;

    public BigDecimal getDistributionDays() {
        return distributionDays;
    }

    public void setDistributionDays(BigDecimal distributionDays) {
        this.distributionDays = distributionDays;
    }
    
    public String getDistributionDaysDD() {
        return "EmpCostCenterException_distributionDays";
    }
    @Transient
    private BigDecimal distributionDaysMask;
    public BigDecimal getDistributionDaysMask() {
        distributionDaysMask = distributionDays ;
        return distributionDaysMask;
    }
    public void setDistributionDaysMask(BigDecimal distributionDaysMask) {
        updateDecimalValue("distributionDays",distributionDaysMask);
    }
    public String getDistributionDaysMaskDD() {
        return "EmpCostCenterException_distributionDaysMask";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="distributionHours">
    @Column(precision=25, scale=13)
    private  BigDecimal distributionHours;

    public BigDecimal getDistributionHours() {
        return distributionHours;
    }

    public void setDistributionHours(BigDecimal distributionHours) {
        this.distributionHours = distributionHours;
    }
    
    public String getDistributionHoursDD() {
        return "EmpCostCenterException_distributionHours";
    }
    @Transient
    private BigDecimal distributionHoursMask;
    public BigDecimal getDistributionHoursMask() {
        distributionHoursMask = distributionHours ;
        return distributionHoursMask;
    }
    public void setDistributionHoursMask(BigDecimal distributionHoursMask) {
        updateDecimalValue("distributionHours",distributionHoursMask);
    }
    public String getDistributionHoursMaskDD() {
        return "EmpCostCenterException_distributionHoursMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="salaryElementType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElementType salaryElementType;

    public SalaryElementType getSalaryElementType() {
        return salaryElementType;
    }

    public void setSalaryElementType(SalaryElementType salaryElementType) {
        this.salaryElementType = salaryElementType;
    }
    
    public String getSalaryElementTypeDD() {
        return "EmpCostCenterException_salaryElementType";
    }
    //</editor-fold>
}