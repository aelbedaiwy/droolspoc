package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.ManyToOne;
import javax.persistence.Column;


/**
 * The persistent class for the tax_group_detail database table.
 * 
 */
@Entity
@ParentEntity(fields={"tax"})
public class TaxGroupCalculation extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
	@Column(nullable=false)
	private int sortIndex;
    public int getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "TaxGroupCalculation_sortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tax">
	
	@JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Tax tax;
    public Tax getTax() {
        return tax;
    }
    public void setTax(Tax tax) {
        this.tax = tax;
    }
    public String getTaxDD() {
        return "TaxGroupCalculation_tax";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxGroup">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TaxGroup taxGroup;
    public TaxGroup getTaxGroup() {
        return taxGroup;
    }
    public void setTaxGroup(TaxGroup taxGroup) {
        this.taxGroup = taxGroup;
    }
    public String getTaxGroupDD() {
        return "TaxGroupCalculation_taxGroup";
    }
    // </editor-fold>
}