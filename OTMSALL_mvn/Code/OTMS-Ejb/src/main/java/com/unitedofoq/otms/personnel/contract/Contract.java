
package com.unitedofoq.otms.personnel.contract;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"company"})
public class Contract extends BaseEntity  {

  // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "Contract_code";
    }
    // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField="descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "Contract_description";
    }
    // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "Contract_description";
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
     // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public String getCommentsDD() {
        return "Contract_comments";
    }
    // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public void setType(UDC type) {
        this.type = type;
    }

    public UDC getType() {
        return type;
    }

    public String getTypeDD() {
        return "Contract_type";
    }
    // </editor-fold>
    
  // <editor-fold defaultstate="collapsed" desc="Company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Company company;

    public Company getCompany() {
        return company;
    }
    
    public String getCompanyDD() {
        return "Contract_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }
     // </editor-fold>
 
  // <editor-fold defaultstate="collapsed" desc="Period">
    private Integer period;

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }
   
    public String getPeriodDD() {
        return "Contract_period";
    } 

    // </editor-fold>
    
  //<editor-fold defaultstate="collapsed" desc="probationPeriod">
    @Column(precision=25,scale=13)
    private BigDecimal probationPeriod; // days

    public BigDecimal getProbationPeriod() {
        return probationPeriod;
    }

    public void setProbationPeriod(BigDecimal probationPeriod) {
        this.probationPeriod = probationPeriod;
    }
    
    public String getProbationPeriodDD() {
        return "Contract_probationPeriod";
    }
     // </editor-fold>
  
  // <editor-fold defaultstate="collapsed" desc="working hours">
   @Column(precision=25, scale=13)
    private BigDecimal workingHours;

    public BigDecimal getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(BigDecimal workingHours) {
        this.workingHours = workingHours;
    }
    
    public String getWorkingHoursDD() {
        return "Contract_workingHours";
    }
        // </editor-fold>
}