/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="selectedApplicant")
public class SelectedApplicantCompetency extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="selectedApplicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private RequisitionSelectedApplicant selectedApplicant;

    public RequisitionSelectedApplicant getSelectedApplicant() {
        return selectedApplicant;
    }

    public void setSelectedApplicant(RequisitionSelectedApplicant selectedApplicant) {
        this.selectedApplicant = selectedApplicant;
    }
    
    public String getSelectedApplicantDD(){
        return "SelectedApplicantCompetency_selectedApplicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    
    public String getCompetenciesDD(){
        return "SelectedApplicantCompetency_competency";
    }
    // </editor-fold>
}
