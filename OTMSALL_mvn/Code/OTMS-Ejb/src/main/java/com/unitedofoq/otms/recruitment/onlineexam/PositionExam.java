package com.unitedofoq.otms.recruitment.onlineexam;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.position.Position;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class PositionExam extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
    public String getPositionDD() {
        return "PositionExam_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exam">
    @ManyToOne(fetch = FetchType.LAZY)
    private Exam exam;
    
    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }
    
    public String getExamDD() {
        return "PositionExam_exam";
    }
    // </editor-fold>
}
