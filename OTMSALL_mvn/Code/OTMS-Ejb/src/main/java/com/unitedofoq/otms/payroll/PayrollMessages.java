/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.*;


/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields="payrollCalculation")
public class PayrollMessages extends BusinessObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="payrollcalculation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    //DBID will be used as a sort index for messages
    private PayrollCalculation payrollCalculation;

    public PayrollCalculation getPayrollCalculation() {
        return payrollCalculation;
    }
    public void setPayrollCalculation(PayrollCalculation Payrollcalculation) {
        this.payrollCalculation = Payrollcalculation;
    }
    public String getPayrollCalculationDD() {
        return "PayrollMessages_payrollCalculation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="userMessage">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UserMessage userMessage;
    public UserMessage getUserMessage() {
        return userMessage;
    }
    public void setUserMessage(UserMessage UserMessage) {
        this.userMessage = UserMessage;
    }
    public String getUserMessageDD() {
        return "PayrollMessages_userMessage";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="title">
    @Column
    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitleDD() {
        return "PayrollMessages_title";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="text">
    @Column
    private String text;
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public String getTextDD() {
        return "PayrollMessages_text";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "PayrollMessages_employee";
    }
    // </editor-fold>

}
