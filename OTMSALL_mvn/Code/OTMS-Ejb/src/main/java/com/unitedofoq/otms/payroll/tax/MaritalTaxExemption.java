package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.math.BigDecimal;
import javax.persistence.*;


/**
 * @author mragab
 * 
 */
@Entity
@ParentEntity(fields={"tax"})
public class MaritalTaxExemption extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="tax">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Tax tax;
    public Tax getTax() {
        return tax;
    }
    public void setTax(Tax tax) {
        this.tax = tax;
    }
    public String getTaxDD() {
        return "MaritalTaxExemption_tax";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gender">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC gender;

    public UDC getGender() {
        return gender;
    }

    public void setGender(UDC gender) {
        this.gender = gender;
    }
    
    public String getGenderDD() {
        return "MaritalTaxExemption_gender";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maritalStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC maritalStatus;

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
    
    public String getMaritalStatusDD() {
        return "MaritalTaxExemption_maritalStatus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="yearValue">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal yearValue;

    public BigDecimal getYearValue() {
        return yearValue;
    }

    public void setYearValue(BigDecimal yearValue) {
        this.yearValue = yearValue;
    }
    
    public String getYearValueDD() {
        return "MaritalTaxExemption_yearValue";
    }
    @Transient
    private BigDecimal yearValueMask;

    public BigDecimal getYearValueMask() {
        yearValueMask = yearValue;
        return yearValueMask;
    }

    public void setYearValueMask(BigDecimal yearValueMask) {
        updateDecimalValue("yearValue",yearValueMask);
    }
    
    public String getYearValueMaskDD() {
        return "MaritalTaxExemption_yearValueMask";
    }
    // </editor-fold>
}