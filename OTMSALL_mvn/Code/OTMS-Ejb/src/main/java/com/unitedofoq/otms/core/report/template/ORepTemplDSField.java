package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.dd.DD;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"fieldGroup"})
public class ORepTemplDSField extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="columnName">
    @Column(nullable = false)
    /**
     * Column name in the Data Table assigned in {@link ORepTempl#tableName}
     */
    private String columnName;

    /**
     * @return the columnName
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * @param columnName the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * @return the columnName DD
     */
    public String getColumnNameDD() {
        return "ORepTemplDSField_columnName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dd">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private DD dd;

    /**
     * @return the dd
     */
    public DD getDd() {
        return dd;
    }

    /**
     * @param dd the dd to set
     */
    public void setDd(DD dd) {
        this.dd = dd;
    }

    /**
     * @return the dd DD
     */
    public String getDdDD() {
        return "ORepTemplDSField_dd";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Translation(originalField = "description")
    @Transient
    private String descriptionTranslated;

    /**
     * @return the descriptionTranslated
     */
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    /**
     * @param descriptionTranslated the descriptionTranslated to set
     */
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    /**
     * @return the descriptionTranslated DD
     */
    public String getDescriptionTranslatedDD() {
        return "ORepTemplDSField_descriptionTranslated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    @Column
    /**
     * Should allow null to allow adding non-entity-column (e.g. Any of the
     * salary elements)
     */
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "ORepTemplDSField_fieldExpression";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oactOnEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    /**
     * Entity of the {@link #fieldExpression} Should allow null to allow adding
     * non-entity-column (e.g. Any of the salary elements)
     */
    protected OEntity oactOnEntity;

    public OEntity getOactOnEntity() {
        return oactOnEntity;
    }

    public void setOactOnEntity(OEntity oactOnEntity) {
        this.oactOnEntity = oactOnEntity;
    }

    public String getOactOnEntityDD() {
        return "ORepTemplDSField_oactOnEntity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="orepTemplDS">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORepTemplDS orepTemplDS;

    /**
     * @return the orepTemplDS
     */
    public ORepTemplDS getOrepTemplDS() {
        return orepTemplDS;
    }

    /**
     * @param orepTemplDS the orepTemplDS to set
     */
    public void setOrepTemplDS(ORepTemplDS orepTemplDS) {
        this.orepTemplDS = orepTemplDS;
    }

    /**
     * @return the orepTemplDSDD
     */
    public String getOrepTemplDSDD() {
        return "ORepTemplDSField_orepTemplDS";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldGroup">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private ORepTemplDSFieldGroup fieldGroup;

    /**
     * @return the fieldGroup
     */
    public ORepTemplDSFieldGroup getFieldGroup() {
        return fieldGroup;
    }

    /**
     * @param fieldGroup the fieldGroup to set
     */
    public void setFieldGroup(ORepTemplDSFieldGroup fieldGroup) {
        this.fieldGroup = fieldGroup;
    }

    /**
     * @return the fieldGroupDD
     */
    public String getFieldGroupDD() {
        return "ORepTemplDSField_fieldGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="usedAsFilter">
    @Column(nullable = true)
    private boolean usedAsFilter;

    /**
     * @return the usedAsFilter
     */
    public boolean isUsedAsFilter() {
        return usedAsFilter;
    }

    /**
     * @param usedAsFilter the usedAsFilter to set
     */
    public void setUsedAsFilter(boolean usedAsFilter) {
        this.usedAsFilter = usedAsFilter;
    }

    /**
     * @return the usedAsFilter
     */
    public String getUsedAsFilterDD() {
        return "ORepTemplDSField_usedAsFilter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="udcType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDCType udcType;

    /**
     * @return the udcType
     */
    public UDCType getUdcType() {
        return udcType;
    }

    /**
     * @param udcType the udcType to set
     */
    public void setUdcType(UDCType udcType) {
        this.udcType = udcType;
    }

    /**
     * @return the udcTypeDD
     */
    public String getUdcTypeDD() {
        return "ORepTemplDSField_udcType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyExpression">
    @Column
    private String companyExpression;

    /**
     * @return the companyExpression
     */
    public String getCompanyExpression() {
        return companyExpression;
    }

    /**
     * @param companyExpression the companyExpression to set
     */
    public void setCompanyExpression(String companyExpression) {
        this.companyExpression = companyExpression;
    }

    /**
     * @return the companyExpressionDD
     */
    public String getCompanyExpressionDD() {
        return "ORepTemplDSField_companyExpression";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="distinctFilterList">
    @Column
    private boolean distinctFilterList;

    /**
     * @return the distinctFilterList
     */
    public boolean isDistinctFilterList() {
        return distinctFilterList;
    }

    /**
     * @param distinctFilterList the distinctFilterList to set
     */
    public void setDistinctFilterList(boolean distinctFilterList) {
        this.distinctFilterList = distinctFilterList;
    }

    /**
     * @return the distinctFilterListDD
     */
    public String getDistinctFilterListDD() {
        return "ORepTemplDSField_distinctFilterList";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calendarFormat">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC calendarFormat;

    /**
     * @return the calendarFormat
     */
    public UDC getCalendarFormat() {
        return calendarFormat;
    }

    /**
     * @param calendarFormat the calendarFormat to set
     */
    public void setCalendarFormat(UDC calendarFormat) {
        this.calendarFormat = calendarFormat;
    }

    /**
     * @return the calendarFormatDD
     */
    public String getCalendarFormatDD() {
        return "ORepTemplDSField_calendarFormat";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column
    private int sortIndex;

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * @return the sortIndexDD
     */
    public String getSortIndexDD() {
        return "ORepTemplDSField_sortIndex";
    }
    // </editor-fold>
}
