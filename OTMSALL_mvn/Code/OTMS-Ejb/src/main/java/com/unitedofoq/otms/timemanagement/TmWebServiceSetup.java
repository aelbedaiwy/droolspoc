/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.payroll.LegalEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author dev
 */
@Entity
public class TmWebServiceSetup extends BaseEntity {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    LegalEntity legalEntity;
    private String url;
    private String userName;
    private String password;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public String getLegalEntityDD() {
        return "TmWebServiceSetup_legalEntity";
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlDD() {
        return "TmWebServiceSetup_url";
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserNameDD() {
        return "TmWebServiceSetup_userName";
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public String getPasswordDD() {
        return "TmWebServiceSetup_password";
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
