package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.otms.payroll.FilterBase;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class EmployeeVacationAdjustmentAssign extends FilterBase {

    @ManyToOne(fetch = FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "EmployeeVacationAdjustmentAssign_vacation";
    }
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDateDD() {
        return "EmployeeVacationAdjustmentAssign_creationDate";
    }
    private BigDecimal adjustmentValue;

    public BigDecimal getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(BigDecimal adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    public String getAdjustmentValueDD() {
        return "EmployeeVacationAdjustmentAssign_adjustmentValuel";
    }
    
        // <editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "EmployeeVacationAdjustment_comments";
    }
    // </editor-fold>
}
