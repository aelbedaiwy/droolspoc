/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.occupation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author arezk
 */

@MappedSuperclass
public class OccuElementGradeBase extends BaseEntity{

    @Column(nullable=false)
    protected int grade;
    public String getGradeDD(){
        return "OccuElementGradeBase_grade";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected OccupationElementScale scale;

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public OccupationElementScale getScale() {
        return scale;
    }

    public void setScale(OccupationElementScale scale) {
        this.scale = scale;
    }
}
