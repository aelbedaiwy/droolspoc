/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.*;

/**
 *
 * @author lahmed
 */
@Entity
public class BankBranchesTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>
}
