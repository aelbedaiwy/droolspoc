
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.Date;
import javax.persistence.*;

@Entity
//@ParentEntity(fields={"provider"})
@ParentEntity(fields="company")
public class ProviderTrainingPlan extends BaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "ProviderTrainingPlan_company";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="planYear">
    @Column
    private int planYear;

    public void setPlanYear(int planYear) {
        this.planYear = planYear;
    }

    public int getPlanYear() {
        return planYear;
    }

    public String getPlanYearDD() {
        return "ProviderTrainingPlan_planYear";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "ProviderTrainingPlan_startDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getEndDateDD() {
        return "ProviderTrainingPlan_endDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "ProviderTrainingPlan_description";
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "ProviderTrainingPlan_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "ProviderTrainingPlan_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="provider">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Provider getProvider() {
        return provider;
    }

    public String getProviderDD() {
        return "ProviderTrainingPlan_provider";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public String getNotesDD() {
        return "ProviderTrainingPlan_notes";
    }
    // </editor-fold>
 
    // <editor-fold defaultstate="collapsed" desc="planDurationUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC planDurationUnit;

    public UDC getPlanDurationUnit() {
        return planDurationUnit;
    }

    public void setPlanDurationUnit(UDC periodUnit) {
        this.planDurationUnit = periodUnit;
    }
    
    public String getPlanDurationUnitDD() {
        return "ProviderTrainingPlan_planDurationUnit";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="planDuration">
    //@MeasurableField(name = "planDurationUnit.value")
    private Integer planDuration;

    public Integer getPlanDuration() {
        return planDuration;
    }

    public void setPlanDuration(Integer planDuration) {
        this.planDuration = planDuration;
    }
    
    public String getPlanDurationDD() {
        return "ProviderTrainingPlan_planDuration";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="preferedDisplay">
    @Column
    private Integer preferedDisplay;

    public Integer getPreferedDisplay() {
        return preferedDisplay;
    }

    public void setPreferedDisplay(Integer preferedDisplay) {
        this.preferedDisplay = preferedDisplay;
    }
    
    public String getPreferedDisplayDD() {
        return "ProviderTrainingPlan_preferedDisplay";
    }
    //</editor-fold>
}
