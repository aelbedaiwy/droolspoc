package com.unitedofoq.otms.foundation.competency;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.activity.Activity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="competency")
public class CompetencyTrainingCourse extends BaseEntity{

    
    // <editor-fold defaultstate="collapsed" desc="Competency">
   // @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Competency competency;
    public Competency getCompetency() {
        return competency;
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
     public String getCompetencyDD() {
        return "CompetencyTrainingCourse_competency";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="trainingActivity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private Activity trainingActivity;
    public String getTrainingActivityDD()      {    return "CompetencyTrainingCourse_trainingActivity";  }
   
    public Activity getTrainingActivity() {
        return trainingActivity;
    }

    public void setTrainingActivity(Activity trainingActivity) {
        this.trainingActivity = trainingActivity;
    }
    // </editor-fold>
}