package com.unitedofoq.otms.recruitment;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class SelectionCriteria extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "SelectionCriteria_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastYearScaleType">
    @ManyToOne(fetch = FetchType.LAZY)
    private ScaleType lastYearScaleType;

    public ScaleType getLastYearScaleType() {
        return lastYearScaleType;
    }

    public void setLastYearScaleType(ScaleType lastYearScaleType) {
        this.lastYearScaleType = lastYearScaleType;
    }

    public String getLastYearScaleTypeDD() {
        return "SelectionCriteria_lastYearScaleType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastYearScaleTypeRule">
    @ManyToOne(fetch = FetchType.LAZY)
    private ScaleTypeRule lastYearScaleTypeRule;

    public ScaleTypeRule getLastYearScaleTypeRule() {
        return lastYearScaleTypeRule;
    }

    public void setLastYearScaleTypeRule(ScaleTypeRule scaleTypeRule) {
        this.lastYearScaleTypeRule = scaleTypeRule;
    }

    public String getLastYearScaleTypeRuleDD() {
        return "SelectionCriteria_lastYearScaleTypeRule";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastYearOperator">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC lastYearOperator;

    public UDC getLastYearOperator() {
        return lastYearOperator;
    }

    public void setLastYearOperator(UDC lastYearOperator) {
        this.lastYearOperator = lastYearOperator;
    }

    public String getLastYearOperatorDD() {
        return "SelectionCriteria_lastYearOperator";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prevYearScaleType">
    @ManyToOne(fetch = FetchType.LAZY)
    private ScaleType prevYearScaleType;

    public ScaleType getPrevYearScaleType() {
        return prevYearScaleType;
    }

    public void setPrevYearScaleType(ScaleType prevYearScaleType) {
        this.prevYearScaleType = prevYearScaleType;
    }

    public String getPrevYearScaleTypeDD() {
        return "SelectionCriteria_prevYearScaleType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prevYearScaleTypeRule">
    @ManyToOne(fetch = FetchType.LAZY)
    private ScaleTypeRule prevYearScaleTypeRule;

    public ScaleTypeRule getPrevYearScaleTypeRule() {
        return prevYearScaleTypeRule;
    }

    public void setPrevYearScaleTypeRule(ScaleTypeRule scaleTypeRule) {
        this.prevYearScaleTypeRule = scaleTypeRule;
    }

    public String getPrevYearScaleTypeRuleDD() {
        return "SelectionCriteria_prevYearScaleTypeRule";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prevYearOperator">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC prevYearOperator;

    public UDC getPrevYearOperator() {
        return prevYearOperator;
    }

    public void setPrevYearOperator(UDC prevYearOperator) {
        this.prevYearOperator = prevYearOperator;
    }

    public String getPrevYearOperatorDD() {
        return "SelectionCriteria_prevYearOperator";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="serviceYears">
    private double serviceYears;

    public double getServiceYears() {
        return serviceYears;
    }

    public void setServiceYears(double serviceYears) {
        this.serviceYears = serviceYears;
    }

    public String getServiceYearsDD() {
        return "SelectionCriteria_serviceYears";
    }
    // </editor-fold>
}
