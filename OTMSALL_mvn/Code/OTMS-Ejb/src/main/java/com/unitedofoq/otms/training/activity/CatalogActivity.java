package com.unitedofoq.otms.training.activity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CATALOG")
public class CatalogActivity extends Activity {
}