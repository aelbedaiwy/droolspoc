package com.unitedofoq.otms.recruitment.jobapplicant;

import com.unitedofoq.fabs.core.security.user.OUser;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("JOBAPP")
public class JobAppUser extends OUser {

    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
	private JobApplicant jobApplicant;

	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant theJobApplicant) {
		jobApplicant = theJobApplicant;
	}
}
