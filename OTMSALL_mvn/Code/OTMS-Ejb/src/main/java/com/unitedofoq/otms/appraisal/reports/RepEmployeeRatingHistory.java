/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepEmployeeRatingHistory extends RepEmployeeBase  {
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeRatingHistory_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="rating1">
    @Column
    private BigDecimal rating1;

    public void setRating1(BigDecimal rating1) {
        this.rating1 = rating1;
    }

    public BigDecimal getRating1() {
        return rating1;
    }

    public String getRating1DD() {
        return "RepEmployeeRatingHistory_rating1";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="rating2">
    @Column
    private BigDecimal rating2;

    public void setRating2(BigDecimal rating2) {
        this.rating2 = rating2;
    }

    public BigDecimal getRating2() {
        return rating2;
    }

    public String getRating2DD() {
        return "RepEmployeeRatingHistory_rating2";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="rating3">
    @Column
    private BigDecimal rating3;

    public void setRating3(BigDecimal rating3) {
        this.rating3 = rating3;
    }

    public BigDecimal getRating3() {
        return rating3;
    }

    public String getRating3DD() {
        return "RepEmployeeRatingHistory_rating3";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    @Translatable(translationField = "templateTranslated")
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public String getTemplateDD() {
        return "RepEmployeeRatingHistory_template";
    }
    
    @Transient
    @Translation(originalField = "template")
    private String templateTranslated;

    public String getTemplateTranslated() {
        return templateTranslated;
    }

    public void setTemplateTranslated(String templateTranslated) {
        this.templateTranslated = templateTranslated;
    }
    
    public String getTemplateTranslatedDD() {
        return "RepEmployeeRatingHistory_template";
    }
    // </editor-fold>
     
    // <editor-fold defaultstate="collapsed" desc="year">
    @Column
    private String year;

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public String getYearDD() {
        return "RepEmployeeRatingHistory_year";
    }
    // </editor-fold>
}