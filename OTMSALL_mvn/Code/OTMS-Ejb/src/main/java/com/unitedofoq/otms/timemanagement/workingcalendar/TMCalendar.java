package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import com.unitedofoq.otms.timemanagement.Rule;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"company"})
@ChildEntity(fields = {"calendars", "rules"})
public class TMCalendar extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "TMCalendar_code";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "TMCalendar_name";
    }

    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "TMCalendar_name";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="includeHolidays">
    private boolean includeHolidays;

    public boolean isIncludeHolidays() {
        return includeHolidays;
    }

    public void setIncludeHolidays(boolean includeHolidays) {
        this.includeHolidays = includeHolidays;
    }

    public String getIncludeHolidaysDD() {
        return "TMCalendar_includeHolidays";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="rotateIfHolidayIncluded">
    private boolean rotateIfHolidayIncluded;

    public boolean isRotateIfHolidayIncluded() {
        return rotateIfHolidayIncluded;
    }

    public void setRotateIfHolidayIncluded(boolean rotateIfHolidayIncluded) {
        this.rotateIfHolidayIncluded = rotateIfHolidayIncluded;
    }

    public String isRotateIfHolidayIncludedDD() {
        return "TMCalendar_rotateIfHolidayIncluded";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "TMCalendar_type";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="referenceDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date referenceDate;

    public Date getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(Date referenceDate) {
        this.referenceDate = referenceDate;
    }

    public String getReferenceDateDD() {
        return "referenceDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "TMCalendar_company";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="calendars">
    @OneToMany(mappedBy = "calendar")
    List<NormalWorkingCalendar> calendars;

    public List<NormalWorkingCalendar> getCalendars() {
        return calendars;
    }

    public void setCalendars(List<NormalWorkingCalendar> calendars) {
        this.calendars = calendars;
    }

    public String getCalendarsDD() {
        return "TMCalendar_calendars";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="numberOfHours">
    private BigDecimal numberOfHours;

    public BigDecimal getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(BigDecimal numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public String getNumberOfHoursDD() {
        return "TMCalendar_numberOfHours";
    }

    @Transient
    private BigDecimal numberOfHoursMask;

    public BigDecimal getNumberOfHoursMask() {
        numberOfHoursMask = numberOfHours;
        return numberOfHoursMask;
    }

    public void setNumberOfHoursMask(BigDecimal numberOfHoursMask) {
        updateDecimalValue("numberOfHours", numberOfHoursMask);
    }

    public String getNumberOfHoursMaskDD() {
        return "TMCalendar_numberOfHoursMask";
    }
    //</editor-fold>

    //Loubna - 08/09/2013 - for rotation & shift cases (monthly consolidation)
    //<editor-fold defaultstate="collapsed" desc="rules">
    @OneToMany(mappedBy = "tmCalendar")
    private List<Rule> rules;

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public String getRulesDD() {
        return "TMCalendar_NormalWorkingCalendar_rules";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualWorkingDays">
    private boolean actualWorkingDays;

    public boolean isActualWorkingDays() {
        return actualWorkingDays;
    }

    public void setActualWorkingDays(boolean actualWorkingDays) {
        this.actualWorkingDays = actualWorkingDays;
    }

    public String getActualWorkingDaysDD() {
        return "TMCalendar_actualWorkingDays";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public String getLegalEntityDD() {
        return "TMCalendar_legalEntity";
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="weekEndOT">
    private String weekEndOT;

    public String getWeekEndOT() {
        return weekEndOT;
    }

    public String getWeekEndOTDD() {
        return "TMCalendar_weekEndOT";
    }

    public void setWeekEndOT(String weekEndOT) {
        this.weekEndOT = weekEndOT;
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="holidayOT">
    private String holidayOT;

    public String getHolidayOT() {
        return holidayOT;
    }

    public String getHolidayOTDD() {
        return "TMCalendar_holidayOT";
    }

    public void setHolidayOT(String holidayOT) {
        this.holidayOT = holidayOT;
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="maximumWeekEndOT">
    private String maximumWeekEndOT;

    public String getMaximumWeekEndOT() {
        return maximumWeekEndOT;
    }

    public String getMaximumWeekEndOTDD() {
        return "TMCalendar_maximumWeekEndOT";
    }

    public void setMaximumWeekEndOT(String maximumWeekEndOT) {

        this.maximumWeekEndOT = maximumWeekEndOT;
    }

    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="maximumHolidayOT">
    private String maximumHolidayOT;

    public String getMaximumHolidayOT() {

        return maximumHolidayOT;

    }

    public String getMaximumHolidayOTDD() {

        return "TMCalendar_maximumHolidayOT";

    }

    public void setMaximumHolidayOT(String maximumHolidayOT) {

        this.maximumHolidayOT = maximumHolidayOT;

    }

    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "TMCalendar_legalEntityGroup";
    }
    //</editor-fold>
}
