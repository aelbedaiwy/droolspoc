/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="repemployeeassessmentNotes")
public class RepEmployeeAssessmentNotes extends RepEmployeeBase {
    
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeAssessmentNotes_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeAssessmentNotes_genderDescription";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }
    
    public String isEmployeeActiveDD() {
        return "RepEmployeeAssessmentNotes_employeeActive";
    }
    
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeeAssessmentNotes_employeeActive";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="assessmentNotes">
     @Column
     private String assessmentNotes;

    public String getAssessmentNotes() {
        return assessmentNotes;
    }

    public void setAssessmentNotes(String assessmentNotes) {
        this.assessmentNotes = assessmentNotes;
    }

    public String getAssessmentNotesDD() {
        return "RepEmployeeAssessmentNotes_assessmentNotes";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="assessmentDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date assessmentDate;

    public Date getAssessmentDate() {
        return assessmentDate;
    }

    public void setAssessmentDate(Date assessmentDate) {
        this.assessmentDate = assessmentDate;
    }

    public String getAssessmentDateDD() {
        return "RepEmployeeAssessmentNotes_assessmentDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="makerName">
    @Column
    @Translatable(translationField = "makerNameTranslated")
    private String makerName;

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public String getMakerNameDD() {
        return "RepEmployeeAssessmentNotes_makerName";
    }
    
    @Transient
    @Translation(originalField = "makerName")
    private String makerNameTranslated;

    public String getMakerNameTranslated() {
        return makerNameTranslated;
    }

    public void setMakerNameTranslated(String makerNameTranslated) {
        this.makerNameTranslated = makerNameTranslated;
    }
    
    public String getMakerNameTranslatedDD() {
        return "RepEmployeeAssessmentNotes_makerName";
    }
    //</editor-fold>
}
