/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.data.Person;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.employee.idp.EDSConsultant;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author nkhalil
 */
@Entity
@DiscriminatorValue("Consult")
@ParentEntity(fields={"consultant"})
public class ConsultantPerson extends Person {
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private EDSConsultant consultant;

    public EDSConsultant getConsultant() {
        return consultant;
    }

    public void setConsultant(EDSConsultant consultant) {
        this.consultant = consultant;
    }

  
    
}
