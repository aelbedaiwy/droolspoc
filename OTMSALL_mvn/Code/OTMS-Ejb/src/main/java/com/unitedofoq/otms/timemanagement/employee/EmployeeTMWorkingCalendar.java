/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author lap2
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeTMWorkingCalendar extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeTMWorkingCalendar_employee";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="workingCalendar">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private NormalWorkingCalendar workingCalendar;

    public NormalWorkingCalendar getWorkingCalendar() {
        return workingCalendar;
    }

    public void setWorkingCalendar(NormalWorkingCalendar workingCalendar) {
        this.workingCalendar = workingCalendar;
    }

    public String getWorkingCalendarDD() {
        return "EmployeeTMWorkingCalendar_workingCalendar";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="dateFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }
    
    public String getDateFromDD() {
        return "EmployeeTMWorkingCalendar_dateFrom";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="dateTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateToDD() {
        return "EmployeeTMWorkingCalendar_dateTo";
    }
    // </editor-fold >
}