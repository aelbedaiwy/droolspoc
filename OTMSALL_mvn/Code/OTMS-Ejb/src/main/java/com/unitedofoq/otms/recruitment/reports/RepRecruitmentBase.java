package com.unitedofoq.otms.recruitment.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

@MappedSuperclass
public class RepRecruitmentBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="dsDbid">

    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepRecruitmentBase_dsDbid";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applicantFullName">
    @Column
    private String applicantFullName;

    public void setApplicantFullName(String applicantFullName) {
        this.applicantFullName = applicantFullName;
    }

    public String getApplicantFullName() {
        return applicantFullName;
    }

    public String getApplicantFullNameDD() {
        return "RepRecruitmentBase_applicantFullName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requisition">
    @Column
    private String requisition;

    public void setRequisition(String requisition) {
        this.requisition = requisition;
    }

    public String getRequisition() {
        return requisition;
    }

    public String getRequisitionDD() {
        return "RepRecruitmentBase_requisition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @Column
    private String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "RepRecruitmentBase_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statusResult">
    @Column
    private String statusResult;

    public void setStatusResult(String statusResult) {
        this.statusResult = statusResult;
    }

    public String getStatusResult() {
        return statusResult;
    }

    public String getStatusResultDD() {
        return "RepRecruitmentBase_statusResult";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date statusDate;

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public String getStatusDateDD() {
        return "RepRecruitmentBase_statusDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comment">
    @Column
    private String comment;

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public String getCommentDD() {
        return "RepRecruitmentBase_comment";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="what">
    @Column
    private String what;

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhat() {
        return what;
    }

    public String getWhatDD() {
        return "RepRecruitmentBase_what";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="byWhoName">
    @Column
    private String byWhoName;

    public void setByWhoName(String byWhoName) {
        this.byWhoName = byWhoName;
    }

    public String getByWhoName() {
        return byWhoName;
    }

    public String getByWhoNameDD() {
        return "RepRecruitmentBase_byWhoName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="position">
    @Column
    private String position;

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "RepRecruitmentBase_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getCompanyIDDD() {
        return "RepRecruitmentBase_companyID";
    }
    // </editor-fold>
}
