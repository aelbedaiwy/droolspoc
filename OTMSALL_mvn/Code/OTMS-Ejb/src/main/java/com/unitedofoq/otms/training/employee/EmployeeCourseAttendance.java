
package com.unitedofoq.otms.training.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"employeeCourse"})
public class EmployeeCourseAttendance extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public String getNotesDD() {
        return "EmployeeCourseAttendance_notes";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="breakFrom">
    @Column
    private String breakFrom;

    public void setBreakFrom(String breakFrom) {
        this.breakFrom = breakFrom;
    }

    public String getBreakFrom() {
        return breakFrom;
    }

    public String getBreakFromDD() {
        return "EmployeeCourseAttendance_breakFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="breakTo">
    @Column
    private String breakTo;

    public void setBreakTo(String breakTo) {
        this.breakTo = breakTo;
    }

    public String getBreakTo() {
        return breakTo;
    }

    public String getBreakToDD() {
        return "EmployeeCourseAttendance_breakTo";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="attendanceDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date attendanceDate;

    public void setAttendanceDate(Date attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public Date getAttendanceDate() {
        return attendanceDate;
    }

    public String getAttendanceDateDD() {
        return "EmployeeCourseAttendance_attendanceDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dayStatus;

    public void setDayStatus(UDC dayStatus) {
        this.dayStatus = dayStatus;
    }

    public UDC getDayStatus() {
        return dayStatus;
    }

    public String getDayStatusDD() {
        return "EmployeeCourseAttendance_dayStatus";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeFrom">
    @Column
    private String timeFrom;

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public String getTimeFromDD() {
        return "EmployeeCourseAttendance_timeFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeTo">
    @Column
    private String timeTo;

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public String getTimeToDD() {
        return "EmployeeCourseAttendance_timeTo";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeCourse">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeCourse employeeCourse;

    public EmployeeCourse getEmployeeCourse() {
        return employeeCourse;
    }

    public void setEmployeeCourse(EmployeeCourse employeeCourse) {
        this.employeeCourse = employeeCourse;
    }
    
    public String getEmployeeCourseDD() {
        return "EmployeeCourseAttendance_employeeCourse";
    }
    //</editor-fold>
}