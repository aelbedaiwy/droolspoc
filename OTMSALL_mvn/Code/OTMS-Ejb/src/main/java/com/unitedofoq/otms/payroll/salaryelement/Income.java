package com.unitedofoq.otms.payroll.salaryelement;

import javax.persistence.*;
import javax.persistence.DiscriminatorValue;

/**
 * Entity implementation class for Entity: Income
 *
 */
@Entity
@DiscriminatorValue("INCOME")
public class Income extends SalaryElement {
   
}
