package com.unitedofoq.otms.nursery;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeDependance;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeNursery extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeNursery_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeDependance">
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private EmployeeDependance employeeDependance;

    public EmployeeDependance getEmployeeDependance() {
        return employeeDependance;
    }

    public void setEmployeeDependance(EmployeeDependance employeeDependance) {
        this.employeeDependance = employeeDependance;
    }

    public String getEmployeeDependanceDD() {
        return "EmployeeNursery_employeeDependance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "EmployeeNursery_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "EmployeeNursery_endDate";
    }
    // </editor-fold>
}
