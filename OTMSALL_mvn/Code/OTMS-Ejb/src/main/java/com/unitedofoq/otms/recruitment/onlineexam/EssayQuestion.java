package com.unitedofoq.otms.recruitment.onlineexam;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;

@Entity
@ParentEntity(fields = {"exam"})
public class EssayQuestion extends QuestionBase {

    // <editor-fold defaultstate="collapsed" desc="modelAnswer">
    private String modelAnswer;

    public String getModelAnswer() {
        return modelAnswer;
    }

    public void setModelAnswer(String modelAnswer) {
        this.modelAnswer = modelAnswer;
    }

    public String getModelAnswerDD() {
        return "EssayQuestion_modelAnswer";
    }
    // </editor-fold>
}
