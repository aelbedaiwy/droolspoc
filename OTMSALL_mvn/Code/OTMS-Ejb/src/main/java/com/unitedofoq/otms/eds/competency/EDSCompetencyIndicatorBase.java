package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="ITYPE")
public class EDSCompetencyIndicatorBase extends BaseEntity{
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslatedDD(){   return "EDSCompetencyIndicatorBase_description";  }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
}
