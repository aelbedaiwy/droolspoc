package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccuElementGradeBase;
import com.unitedofoq.otms.foundation.occupation.Skill;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobAppSkill extends OccuElementGradeBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
	private JobApplicant jobApplicant;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Skill skill;
	public String getSkillDD(){
        return "JobAppSkill_skill";
    }
	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant theJobApplicant) {
		jobApplicant = theJobApplicant;
	}

	public Skill getSkill() {
		return skill;
	}

	public void setSkill(Skill theSkill) {
		skill = theSkill;
	}
}
