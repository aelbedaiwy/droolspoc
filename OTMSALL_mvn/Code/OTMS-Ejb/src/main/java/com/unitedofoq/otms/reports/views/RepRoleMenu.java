
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepRoleMenu extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private String dsDBID;

    public void setDsDBID(String dsDBID) {
        this.dsDBID = dsDBID;
    }

    public String getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepRoleMenu_dsDBID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="role">
    @Column
    private String role;

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public String getRoleDD() {
        return "RepRoleMenu_role";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="menu1">
    @Column
    @Translatable(translationField = "menu1Translated")
    private String menu1;

    public void setMenu1(String menu1) {
        this.menu1 = menu1;
    }

    public String getMenu1() {
        return menu1;
    }

    public String getMenu1DD() {
        return "RepRoleMenu_menu1";
    }
    
    @Transient
    @Translation(originalField = "menu1")
    private String menu1Translated;

    public String getMenu1Translated() {
        return menu1Translated;
    }

    public void setMenu1Translated(String menu1Translated) {
        this.menu1Translated = menu1Translated;
    }
    
    public String getMenu1TranslatedDD() {
        return "RepRoleMenu_menu1";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="menu2">
    @Column
    @Translatable(translationField = "menu2Translated")
    private String menu2;

    public void setMenu2(String menu2) {
        this.menu2 = menu2;
    }

    public String getMenu2() {
        return menu2;
    }

    public String getMenu2DD() {
        return "RepRoleMenu_menu2";
    }
    
    @Transient
    @Translation(originalField = "menu2")
    private String menu2Translated;

    public String getMenu2Translated() {
        return menu2Translated;
    }

    public void setMenu2Translated(String menu2Translated) {
        this.menu2Translated = menu2Translated;
    }
    
    public String getMenu2TranslatedDD() {
        return "RepRoleMenu_menu2";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="menu3">
    @Column
    @Translatable(translationField = "menu3Translated")
    private String menu3;

    public void setMenu3(String menu3) {
        this.menu3 = menu3;
    }

    public String getMenu3() {
        return menu3;
    }

    public String getMenu3DD() {
        return "RepRoleMenu_menu3";
    }
    
    @Transient
    @Translation(originalField = "menu3")
    private String menu3Translated;

    public String getMenu3Translated() {
        return menu3Translated;
    }

    public void setMenu3Translated(String menu3Translated) {
        this.menu3Translated = menu3Translated;
    }
    
    public String getMenu3TranslatedDD() {
        return "RepRoleMenu_menu3";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="functionName">
    @Column
    @Translatable(translationField = "functionNameTranslated")
    private String functionName;

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getFunctionNameDD() {
        return "RepRoleMenu_functionName";
    }
    
    @Transient
    @Translation(originalField = "functionName")
    private String functionNameTranslated;

    public String getFunctionNameTranslated() {
        return functionNameTranslated;
    }

    public void setFunctionNameTranslated(String functionNameTranslated) {
        this.functionNameTranslated = functionNameTranslated;
    }
    
    public String getFunctionNameTranslatedDD() {
        return "RepRoleMenu_functionName";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="companyID">
    private String companyID;  

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }
    
    public String getCompanyIDDD() {
        return "RepRoleMenu_companyID";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="functionCode">
    private String functionCode;

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }
    
    public String getFunctionCodeDD() {
        return "RepRoleMenu_functionCode";
    }

    //</editor-fold>

}
