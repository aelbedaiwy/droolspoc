/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.comunication.EMailSenderLocal;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.udc.UDCServiceRemote;
import com.unitedofoq.fabs.core.udc.UDCType;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.budget.BudgetInstance;
import com.unitedofoq.otms.budget.EmployeeBudgetElement;
import com.unitedofoq.otms.eds.foundation.employee.EDSEmployeeUser;
import com.unitedofoq.otms.eds.foundation.employee.idp.EDSIDPActionForBusinessOutcome;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeUser;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.system.SystemLookUpDetail;
import com.unitedofoq.otms.timemanagement.employee.EmployeeAnnualPlanner;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendance;
import com.unitedofoq.otms.training.activity.Provider;
import com.unitedofoq.otms.training.activity.ProviderCourse;
import com.unitedofoq.otms.training.activity.ProviderCourseClass;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import com.unitedofoq.otms.training.activity.ProviderTrainingPlan;
import com.unitedofoq.otms.training.employee.EmployeeCourse;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author arezk
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.foundation.training.TrainingServiceRemote",
beanInterface = TrainingServiceRemote.class)
public class TrainingService implements TrainingServiceRemote{
    
    @EJB
    UIFrameworkServiceRemote uIFrameworkService;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    OFunctionServiceRemote functionService;
    @EJB
    UDCServiceRemote udcService;
    @EJB
    EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    EMailSenderLocal mailSender;
    
    public static final String TIME24HOURS_PATTERN =
            "(([01]?[0-9]|2[0-3]):[0-5][0-9])|(([01]?[0-9]|2[0-3]).[0-5][0-9])";
    public static final DateFormat DATE_FOTMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat MONTH_FOTMAT = new SimpleDateFormat("MM");

    @Override
    public OFunctionResult finish(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) 
    {
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ProviderTrainingPlan.class) ;
        if (inputValidationErrors != null) return inputValidationErrors ;
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        try {
//            oem.getEM(loggedUser);
            ProviderTrainingPlan provider = (ProviderTrainingPlan) odm.getData().get(0);
            UDCType TrainingPlan = (UDCType) oem.loadEntity("UDCType", Collections.singletonList("value = 'TrainingPlan'"), 
                                            null, loggedUser);
            List<UDC> trainingPlanUDCs = udcService.getUDCs(TrainingPlan.getDbid(), loggedUser);
            for (UDC tp : trainingPlanUDCs) {
                if(tp.getCode().equals("02")){ // Finished
                    provider.setStatus(tp);
                    entitySetupService.callEntityUpdateAction(provider, loggedUser);
                    break;
                }
            }
        } catch (Exception ex){
            OLog.logException(ex, loggedUser);
        } finally {
//            oem.closeEM(loggedUser);
        }
        return oFR;
    }
    
    @Override
    public OFunctionResult announce(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) 
    {
        // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
        OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                odm, loggedUser, ProviderTrainingPlan.class) ;
        if (inputValidationErrors != null) return inputValidationErrors ;
        // </editor-fold>

        OFunctionResult oFR = new OFunctionResult();
        try {
//            oem.getEM(loggedUser);
            ProviderTrainingPlan provider = (ProviderTrainingPlan) odm.getData().get(0);
            UDCType TrainingPlan = (UDCType) oem.loadEntity("UDCType", Collections.singletonList("value = 'TrainingPlan'"), 
                                            null, loggedUser);
            List<UDC> trainingPlanUDCs = udcService.getUDCs(TrainingPlan.getDbid(), loggedUser);
            for (UDC tp : trainingPlanUDCs) {
                if(tp.getCode().equals("03")){ // Announce
                    provider.setStatus(tp);
                    entitySetupService.callEntityUpdateAction(provider, loggedUser);
                    break;
                }
            }
        } catch (Exception ex){
            OLog.logException(ex, loggedUser);
        } finally {
//            oem.closeEM(loggedUser);
        }
        return oFR;
    }
    
    @Override
    public OFunctionResult postCreateProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderTrainingPlan providerTrainingPlan = (ProviderTrainingPlan) odm.getData().get(0);
            
            //validation part
            Date startDate = providerTrainingPlan.getStartDate();
            Date endDate = providerTrainingPlan.getEndDate();
            if(startDate.compareTo(endDate) > 0){
                ofr.addError(usrMsgService.getUserMessage("StartDateGreaterThanEndDate", loggedUser));
                return ofr;
            }
            
            oem.saveEntity(providerTrainingPlan, loggedUser);
            
                List<String> statusConds = new ArrayList<String>();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Created'");
                UDC createdStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                
                providerTrainingPlan.setStatus(createdStatus);
                
                oem.saveEntity(providerTrainingPlan, loggedUser);
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult setCoursesCreated(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            //ProviderTrainingPlan providerTrainingPlan = (ProviderTrainingPlan) odm.getData().get(0);
            //List<String> reservedConds =new ArrayList<String>();
            //conds.add("trainingPlan.dbid = " + providerTrainingPlan.getDbid());
            //List<ProviderCourseInformation> courseInformations = (List<ProviderCourseInformation>)oem.loadEntityList(
            //        ProviderCourseInformation.class.getSimpleName(), reservedConds, null, null, loggedUser);
            
//            ProviderCourseInformation courseInformation = (ProviderCourseInformation) odm.getData().get(0);
//            
//            if(courseInformation!=null){
//                List<String> statusConds = new ArrayList<String>();
//                statusConds.add("type.code = 'CourseStatus'");
//                statusConds.add("code = 'Created'");
//                UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
//                courseInformation.setStatus(status);
//                oem.saveEntity(courseInformation, loggedUser);
//            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult closeProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderTrainingPlan providerTrainingPlan = (ProviderTrainingPlan) odm.getData().get(0);
            if(providerTrainingPlan!=null){
                List<String> statusConds = new ArrayList<String>();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Closed'");
                UDC closedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                
                providerTrainingPlan.setStatus(closedStatus);
                
                entitySetupService.callEntityUpdateAction(providerTrainingPlan, loggedUser);
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult announceProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        OUser systemUser = loggedUser;
        try{
//            oem.getEM(systemUser);
            ProviderTrainingPlan providerTrainingPlan = (ProviderTrainingPlan) odm.getData().get(0);
            if(providerTrainingPlan!=null){
                List<String> conds = new ArrayList<String>();
            conds.add("trainingPlan.dbid  = " + providerTrainingPlan.getDbid());
            List<ProviderCourseInformation> courseInformations = oem.loadEntityList(ProviderCourseInformation.class.getSimpleName(),
                    conds, null, null, loggedUser);
            if(courseInformations!=null)
            {
                for (int i = 0;i < courseInformations.size(); i++) {
                    if(courseInformations.get(i).getStartDate() == null || courseInformations.get(i).getEndDate() == null)
                    {
                        ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce011", loggedUser));
                        return ofr;
                    }
                }
            }
            else
            {
                ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce012", loggedUser));
                return ofr;
            }
                
            if ((providerTrainingPlan.getStartDate() != null) && (providerTrainingPlan.getEndDate() != null)
                        && courseInformations.size() > 0)
                {
                    List<String> statusConds = new ArrayList<String>();
                    statusConds.add("type.code = 'TrainingPlan'");
                    statusConds.add("code = 'Announced'");
                    UDC announcedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);

                    providerTrainingPlan.setStatus(announcedStatus);

                    entitySetupService.callEntityUpdateAction(providerTrainingPlan, loggedUser);

                    //<editor-fold defaultstate="collapsed" desc="send e-mail">
                    //Send Email

                    String trainingPlanName = providerTrainingPlan.getDescription();
                    String startDate = ""; 
                    if(providerTrainingPlan.getStartDate()!=null)
                        startDate = DATE_FOTMAT.format(providerTrainingPlan.getStartDate());
                    String endDate = "";
                    if(providerTrainingPlan.getEndDate()!=null)
                        endDate = DATE_FOTMAT.format(providerTrainingPlan.getEndDate());
                    String from = "system@unitedofoq.com";

                    if(loggedUser!=null){
                        Company company = null;
                        List<String> companyConditions = new ArrayList<String>();
                        companyConditions.add("dbid = " + loggedUser.getUserPrefrence1());
                        company = (Company) oem.loadEntity(Company.class.getSimpleName(), 
                                companyConditions, null, loggedUser);
                        if(company!=null){
                            List<String> conditions = new ArrayList<String>();
                            conditions.add("positionSimpleMode.unit.company.dbid = " + 
                                    company.getDbid());
                            List<Employee> employees = null;
                            employees = oem.loadEntityList(Employee.class.getSimpleName(), 
                                    conditions, null, null, loggedUser);
                            if(employees!=null)
                            {
                                for (int i = 0; i < employees.size(); i++) {
                                    EmployeeUser employeeUser = employees.get(i).getEmployeeUser();
                                    if(employeeUser == null){
                                        //Salema[Emloyee New Design] 
//                                        OLog.logError("Employee Doesn't Have EmployeeUser", 
//                                                loggedUser, employees.get(i).getPersonalInfo().getName());
                                        OLog.logError("Employee Doesn't Have EmployeeUser", 
                                                loggedUser, employees.get(i).getName());
                                        continue;
                                    }
                                    String email = employeeUser.getEmail();
                                    //Salema[Emloyee New Design] 
//                                    String employeeName = employees.get(i).getPersonalInfo().getFirstName();
                                    String employeeName = employees.get(i).getFirstName();
    //                                String to = employees.get(i).getPersonalInfo().getEmail();

                                    if(email == null){
                                        //Salema[Emloyee New Design] 
//                                        OLog.logError("EmployeeUser Doesn't Have Email", 
//                                                loggedUser, employees.get(i).getPersonalInfo().getName());
                                        OLog.logError("EmployeeUser Doesn't Have Email", 
                                                loggedUser, employees.get(i).getName());
                                        continue;
                                    }

                                    String body = "Dear " + employeeName + "\nThe training plan " + 
                                            trainingPlanName + " will start at " + startDate +
                                    ", end at " + endDate + " and is availabe for request.";
                                    try {
                                        mailSender.sendEMail(from, email, "New Training Plan Announcement", body);
                                    } catch (Exception ex) {
                                        OLog.logException(ex, systemUser);
                                    }
                                }
                            }
                        }
                    }
    //                String from = "system@unitedofoq.com";
    //                String to = "loubna.abdelhamid@gmail.com";            
    //                String body = "Dear "; //employee name , the training plan + name + will start @ start date
    //                // and end @ end date and is availabe for request
    //                try {
    //                    mailSender.sendEMail(from, to, "New Training Plan Announcement", body);
    //                } catch (Exception ex) {
    //                    OLog.logException(ex, systemUser);
    //                }
                    //</editor-fold> 
                }
                else
                {
                    ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce001", loggedUser));
                }
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult setPlanDuration(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderTrainingPlan providerTrainingPlan = (ProviderTrainingPlan) odm.getData().get(0);
            
            Date startDate = providerTrainingPlan.getStartDate();
            Date endDate = providerTrainingPlan.getEndDate();
            
            long months = subtractDates(startDate, endDate);
            providerTrainingPlan.setPlanDuration((int) months);
            //entitySetupService.callEntityUpdateAction(providerTrainingPlan, loggedUser);
            //oem.saveEntity(providerTrainingPlan, loggedUser);
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult finishProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderTrainingPlan providerTrainingPlan = (ProviderTrainingPlan) odm.getData().get(0);
            if(providerTrainingPlan!=null){
                List<String> statusConds = new ArrayList<String>();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Finished'");
                UDC finishedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                
                providerTrainingPlan.setStatus(finishedStatus);
                
                entitySetupService.callEntityUpdateAction(providerTrainingPlan, loggedUser);
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult scheduledJobClosePlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            
            Calendar today = Calendar.getInstance();
            Date todayDate = today.getTime();
            
            List<String> conds = new ArrayList<String>();
            conds.add("endDate <= '" + DATE_FOTMAT.format(todayDate) + "'");
            List<ProviderTrainingPlan> plans = (List<ProviderTrainingPlan>)oem.loadEntityList(
                    ProviderTrainingPlan.class.getSimpleName(), conds, null, null, loggedUser);
            
            for (int j = 0; j < plans.size(); j++) {
                conds.clear();
                conds.add("type.code = 'TrainingPlan'");
                conds.add("code = 'Closed'");
                UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                
                plans.get(j).setStatus(status);
                oem.saveEntity(plans.get(j), loggedUser);
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult scheduledJobExpireCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            
            Calendar today = Calendar.getInstance();
            Date todayDate = today.getTime();
            
            List<String> conds = new ArrayList<String>();
            conds.add("status.code = 'Created'");
            conds.add("startDate <= '" + DATE_FOTMAT.format(todayDate) + "'");
            List<ProviderCourseInformation> courses = (List<ProviderCourseInformation>)oem.loadEntityList(
                    ProviderCourseInformation.class.getSimpleName(), conds, null, null, loggedUser);
            
            for (int j = 0; j < courses.size(); j++) {
                conds.clear();
                conds.add("type.code = 'CourseStatus'");
                conds.add("code = 'Expired'");
                UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                
                courses.get(j).setStatus(status);
                
                oem.saveEntity(courses.get(j), loggedUser);
                
                //<editor-fold defaultstate="collapsed" desc="send an e-mail">
                
                String from = "system@unitedofoq.com";
                OUser systemUser = loggedUser;

                conds.clear();
                conds.add("name = 'HR Admin'");
                Position hrAdminPosition = (Position) oem.loadEntity(Position.class.getSimpleName(), conds, null, loggedUser);
                if(hrAdminPosition!=null){
                    conds.clear();
                    conds.add("positionSimpleMode.dbid =" + hrAdminPosition.getDbid());
                    Employee hrAdmin = (Employee) oem.loadEntity(Employee.class.getSimpleName(), conds, null, loggedUser);
                    if(hrAdmin!=null)
                    {
                        String email = hrAdmin.getEmployeeUser().getEmail();
                        //Salema[Emloyee New Design] 
//                        String hrAdminName = hrAdmin.getPersonalInfo().getFirstName();
                        String hrAdminName = hrAdmin.getFirstName();
                        if (email == null) {
                             //Salema[Emloyee New Design]
//                            OLog.logError("EmployeeUser Doesn't Have Email",
//                                    loggedUser, hrAdmin.getPersonalInfo().getName());
                             OLog.logError("EmployeeUser Doesn't Have Email",
                                    loggedUser, hrAdmin.getName());
                        }

                        String body = "Dear " + hrAdminName + "\n  Kindly be informed that the course "
                                + courses.get(j).getCourse().getDescription() + " has been expired.";
                        try {
                            mailSender.sendEMail(from, email, "A Course has been Expired", body);
                        } catch (Exception ex) {
                            OLog.logException(ex, systemUser);
                        }
                    }
                }
                
                conds.clear();
                conds.add("name = 'HR Manager'");
                Position hrManagerPosition = (Position) oem.loadEntity(Position.class.getSimpleName(), conds, null, loggedUser);
                if(hrManagerPosition!=null){
                    conds.clear();
                    conds.add("positionSimpleMode.dbid =" + hrManagerPosition.getDbid());
                    Employee hrManager = (Employee) oem.loadEntity(Employee.class.getSimpleName(), conds, null, loggedUser);                

                    if(hrManager!=null)
                    {
                        String email2 = hrManager.getEmployeeUser().getEmail();
                         //Salema[Emloyee New Design]
//                        String hrManagerName = hrManager.getPersonalInfo().getFirstName();
                        String hrManagerName = hrManager.getFirstName();

                        String body = "Dear " + hrManagerName + "\n  Kindly be informed that the course "
                                + courses.get(j).getCourse().getDescription() + " has been expired.";
                        try {
                            mailSender.sendEMail(from, email2, "A Course has been Expired", body);
                        } catch (Exception ex) {
                            OLog.logException(ex, systemUser);
                        }
                    }
                }
                
                conds.clear();
                conds.add("courseInformation.dbid = " + courses.get(j).getDbid());
                List<EmployeeCourse> requestors = (List<EmployeeCourse>)oem.loadEntityList(EmployeeCourse.class.getSimpleName(), conds, null, null, loggedUser);
                for (int i = 0; i < requestors.size(); i++) {
                    if(requestors.get(i).getEmployee()!=null){
                        String employeeMail = requestors.get(i).getEmployee().getEmployeeUser().getEmail();
                        //Salema[Emloyee New Design]
//                        String employeeName = requestors.get(i).getEmployee().getPersonalInfo().getFirstName();
                          String employeeName = requestors.get(i).getEmployee().getFirstName();

                        if (employeeMail == null) {
                            //Salema[Emloyee New Design]
//                            OLog.logError("EmployeeUser Doesn't Have Email",
//                                    loggedUser, requestors.get(i).getEmployee().getPersonalInfo().getName());
                            OLog.logError("EmployeeUser Doesn't Have Email",
                                    loggedUser, requestors.get(i).getEmployee().getName());
                        }

                        String body = "Dear " + employeeName + "\n  Kindly be informed that the course "
                            + courses.get(j).getCourse().getDescription() + " has been expired and is no longer available;"
                                + "as it wasn't planned by the HR.\n"
                                + "Thank you..";
                        try {
                            mailSender.sendEMail(from, employeeMail, "A Course has been Expired", body);
                        } catch (Exception ex) {
                            OLog.logException(ex, systemUser);
                        }
                    }
                }
                //</editor-fold>
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult scheduledJobFinishCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            
            Calendar today = Calendar.getInstance();
            Date todayDate = today.getTime();
            
            List<String> conds = new ArrayList<String>();
            conds.add("status.code = 'Running'");
            conds.add("endDate <= '" + DATE_FOTMAT.format(todayDate) + "'");
            List<ProviderCourseInformation> courses = (List<ProviderCourseInformation>)oem.loadEntityList(
                    ProviderCourseInformation.class.getSimpleName(), conds, null, null, loggedUser);
            
            for (int j = 0; j < courses.size(); j++) {
                conds.clear();
                conds.add("type.code = 'CourseStatus'");
                conds.add("code = 'Finished'");
                UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                
                courses.get(j).setStatus(status);
                oem.saveEntity(courses.get(j), loggedUser);
                
                //<editor-fold defaultstate="collapsed" desc="send an e-mail">
                conds.clear();
                conds.add("name = 'HR Admin'");
                Position hrAdminPosition = (Position) oem.loadEntity(Position.class.getSimpleName(), conds, null, loggedUser);
                if(hrAdminPosition!=null)
                {
                    conds.clear();
                    conds.add("positionSimpleMode.dbid =" + hrAdminPosition.getDbid());
                    Employee hrAdmin = (Employee) oem.loadEntity(Employee.class.getSimpleName(), conds, null, loggedUser);
                    if(hrAdmin!=null)
                    {
                        String from = "system@unitedofoq.com";
                        String email = hrAdmin.getEmployeeUser().getEmail();
                        String employeeName = hrAdmin.getFirstName();

                        if (email == null) {
                            OLog.logError("EmployeeUser Doesn't Have Email",
                                    loggedUser, hrAdmin.getName());
                        }

                        OUser systemUser = loggedUser;

                        String body = "Dear " + employeeName + "\n Kindly be informed that the course "
                                + courses.get(j).getCourse().getDescription() + " has been finished.";
                        try {
                            mailSender.sendEMail(from, email, "A Course has been Finished", body);
                        } catch (Exception ex) {
                            OLog.logException(ex, systemUser);
                        }
                    }
                }
                //</editor-fold>
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    
    @Override
    public OFunctionResult scheduledJobReservedCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            //ProviderCourseInformation courseInformation = (ProviderCourseInformation)odm.getData().get(0);
            
            Calendar today = Calendar.getInstance();
            Date todayDate = today.getTime();
            
            List<String> conds = new ArrayList<String>();
            conds.add("status.code = 'Reserved'");
            conds.add("startDate <= '" + DATE_FOTMAT.format(todayDate) + "'");
            List<ProviderCourseInformation> courses = (List<ProviderCourseInformation>)oem.loadEntityList(
                    ProviderCourseInformation.class.getSimpleName(), conds, null, null, loggedUser);
            
            for (int j = 0; j < courses.size(); j++) {
                conds.clear();
                conds.add("type.code = 'CourseStatus'");
                conds.add("code = 'Running'");
                UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);

                courses.get(j).setStatus(status);

                oem.saveEntity(courses.get(j), loggedUser);

                conds.clear();
                conds.add("type.code = 'EmployeeCourse'");
                conds.add("code = 'Enrolled'");
                status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);

                conds.clear();
                conds.add("courseInformation.dbid = " + courses.get(j).getDbid());
                conds.add("status.dbid = " + status.getDbid());
                List<EmployeeCourse> employeeCourses = (List<EmployeeCourse>) oem.loadEntityList(EmployeeCourse.class.getSimpleName(),
                        conds, null, null, loggedUser);
                if (employeeCourses != null) {
                    List<String> empCourseConds = new ArrayList<String>();
                    empCourseConds.add("type.code = 'EmployeeCourse'");
                    empCourseConds.add("code = 'Attended'");
                    UDC enrolledStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), empCourseConds, null, loggedUser);
                    for (int i = 0; i < employeeCourses.size(); i++) {
                        employeeCourses.get(i).setStatus(enrolledStatus);
                        oem.saveEntity(employeeCourses.get(i), loggedUser);
                    }
                }

            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult reserveCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderCourseInformation courseInformation = (ProviderCourseInformation) odm.getData().get(0);
            
            //plannedNum vs selectedNum
            if (courseInformation.getPlannedAttendeesNumber() > courseInformation.getAcceptedNumber()) {
                ofr.addWarning(usrMsgService.getUserMessage("PlannedNumExceedsAcceptedNum", loggedUser));
            } else if (courseInformation.getPlannedAttendeesNumber() < courseInformation.getAcceptedNumber()) {
                ofr.addWarning(usrMsgService.getUserMessage("AcceptedNumExceedsPlannedNum", loggedUser));
            }
            
            List<String> conds = new ArrayList<String>();
            conds.add("type.code = 'CourseStatus'");
            conds.add("code = 'Reserved'");
            UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);

            courseInformation.setStatus(status);
            
            oem.saveEntity(courseInformation, loggedUser);
            
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        } 
            return ofr;
    }
    @Override
    public OFunctionResult validateUpdateProviderCourseInfo(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
//            oem.getEM(loggedUser);
            ProviderCourseInformation courseInformation = (ProviderCourseInformation) odm.getData().get(0);

            List<String> conds = new ArrayList<String>();
            conds.add("dbid = " + courseInformation.getDbid());
            ProviderCourseInformation loadedCourseInformation = (ProviderCourseInformation) oem.loadEntity(
                    ProviderCourseInformation.class.getSimpleName(), conds, null, loggedUser);

            if (courseInformation.getTrainingPlan().getStatus().getCode().equals("Announced")) {
                if (loadedCourseInformation != null) {
                    if (loadedCourseInformation.getStartDate().compareTo(courseInformation.getStartDate()) != 0
                            || loadedCourseInformation.getEndDate().compareTo(courseInformation.getEndDate()) != 0
                            || !loadedCourseInformation.getSessionStartTime().equals(courseInformation.getSessionStartTime())
                            || !loadedCourseInformation.getSessionEndTime().equals(courseInformation.getSessionEndTime())) {
                        ofr.addError(usrMsgService.getUserMessage("PlanIsAnnounced", loggedUser));
                        return ofr;
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        } finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult validateProviderCourseInfo(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderCourseInformation courseInformation = (ProviderCourseInformation) odm.getData().get(0);
            
            
            if (courseInformation != null) {
                
                List<String> conds = new ArrayList<String>();
                conds.add("dbid = " + courseInformation.getDbid());
                ProviderCourseInformation loadedCourseInformation = (ProviderCourseInformation) oem.loadEntity(
                        ProviderCourseInformation.class.getSimpleName(), conds, null, loggedUser);

                if (courseInformation.getTrainingPlan().getStatus().getCode().equals("Announced")) {
                    if(loadedCourseInformation!=null){
                        if(loadedCourseInformation.getStartDate().compareTo(courseInformation.getStartDate()) != 0 || 
                                loadedCourseInformation.getEndDate().compareTo(courseInformation.getEndDate()) != 0 ||
                                !loadedCourseInformation.getSessionStartTime().equals(courseInformation.getSessionStartTime()) ||
                                !loadedCourseInformation.getSessionEndTime().equals(courseInformation.getSessionEndTime())){
                            ofr.addError(usrMsgService.getUserMessage("PlanIsAnnounced", loggedUser));
                            return ofr;
                        }
                    }
                }
                
                // if plan is announced, then do not add a new course
                //if (!courseInformation.getTrainingPlan().getStatus().getCode().equals("Announced")) {
                    // Date part
                    Date startDate = courseInformation.getStartDate();
                    Date endDate = courseInformation.getEndDate();
                    if (startDate != null && endDate != null) {
                        if (startDate.compareTo(endDate) > 0) {
                            ofr.addError(usrMsgService.getUserMessage("StartDateGreaterThanEndDate", loggedUser));
                            return ofr;
                        }

                        ProviderTrainingPlan trainingPlan = courseInformation.getTrainingPlan();
                        Date planStartDate = trainingPlan.getStartDate();
                        Date planEndDate = trainingPlan.getEndDate();

                        if (startDate.compareTo(planStartDate) < 0) {
                            ofr.addError(usrMsgService.getUserMessage("CourseOutOfPlanBoundaries", loggedUser));
                            return ofr;
                        }
                        if (startDate.compareTo(planEndDate) > 0) {
                            ofr.addError(usrMsgService.getUserMessage("CourseOutOfPlanBoundaries", loggedUser));
                            return ofr;
                        }
                        if (endDate.compareTo(planStartDate) < 0) {
                            ofr.addError(usrMsgService.getUserMessage("CourseOutOfPlanBoundaries", loggedUser));
                            return ofr;
                        }
                    }

                    // Time part
                    String startTime = courseInformation.getSessionStartTime();
                    String endTime = courseInformation.getSessionEndTime();

                    if (startTime != null && !startTime.equals("")) {
                        if (!validateTimeFormat(startTime)) {
                            ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Start Time"), loggedUser));
                            return ofr;
                        }
                    }

                    if (endTime != null && !endTime.equals("")) {
                        if (!validateTimeFormat(endTime)) {
                            ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("End Time"), loggedUser));
                            return ofr;
                        }
                    }

                    // ****                
                    if (startTime != null && endTime != null) {
                        startTime = unifyTimeValue(startTime);
                        endTime = unifyTimeValue(endTime);

                        if(!endTime.equals("") && !startTime.equals("")) {
                            if (compareTime(startTime, endTime) > 0) {
                                ofr.addError(usrMsgService.getUserMessage("StartTimeGreaterThanEndTime", loggedUser));
                                return ofr;
                            }
                        }
                    }

                    //if(loadedCourseInformation == null){
                        List<String> statusConds = new ArrayList<String>();
                        statusConds.add("type.code = 'CourseStatus'");
                        statusConds.add("code = 'Created'");
                        UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        courseInformation.setStatus(status);

                        courseInformation.setSessionStartTime(startTime);
                        courseInformation.setSessionEndTime(endTime);
                    //}
                //}
                //else{
               //     ofr.addError(usrMsgService.getUserMessage("PlanIsAnnounced", loggedUser));
                //}
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult cancelEmployeeCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            EmployeeCourse employeeCourse = (EmployeeCourse) odm.getData().get(0);
            
            List<String> conds = new ArrayList<String>();
            conds.add("type.code = 'Cr_Status'");
            conds.add("code = 'Cancelled'");
            UDC cancelledStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
            
            if (employeeCourse.getStatus().getValue().equals(cancelledStatus.getValue())) 
            {
                //<editor-fold defaultstate="collapsed" desc="Effect on Time Management">
                SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
                String startYear = yearFormat.format(employeeCourse.getCourseInformation().getStartDate());
                String endYear = yearFormat.format(employeeCourse.getCourseInformation().getEndDate());

                UDC dayNature = null;
                List<String> udcConditions = new ArrayList<String>();

                if (endYear.equals(startYear)) {
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("employee.dbid = " + employeeCourse.getEmployee().getDbid());
                    conditions.add("plannerYear = " + startYear);
                    EmployeeAnnualPlanner annualPlanner = (EmployeeAnnualPlanner) oem.loadEntity(EmployeeAnnualPlanner.class.getSimpleName(), conditions, null, loggedUser);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(employeeCourse.getCourseInformation().getStartDate());

                    EmployeeDailyAttendance dailyAttendance;
                    List<String> empDAConditions = new ArrayList<String>();
                    do {
                        SimpleDateFormat monthFormat = new SimpleDateFormat("M");
                        String month = monthFormat.format(calendar.getTime());

                        SimpleDateFormat dayFormat = new SimpleDateFormat("d");
                        String day = dayFormat.format(calendar.getTime());

                        Integer.parseInt(day);

                        //daily attendance part
                        DateFormat DATE_FOTMAT = new SimpleDateFormat("yyyy-MM-dd");

                        empDAConditions = new ArrayList<String>();
                        empDAConditions.add("employee.dbid = " + employeeCourse.getEmployee().getDbid());
                        empDAConditions.add("dailyDate = '" + DATE_FOTMAT.format(calendar.getTime()) + "'");

                        dailyAttendance = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(),
                                empDAConditions, null, loggedUser);

                        if (dailyAttendance != null) {
                            if (dailyAttendance.getTimeIn() == null || dailyAttendance.getTimeIn().equals("")) {
                                udcConditions = new ArrayList<String>();
                                udcConditions.add("type.code='900'");
                                udcConditions.add("code = 'Absence'");
                                dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(),
                                        udcConditions, null, loggedUser);

                                dailyAttendance.setDayNature(dayNature);

                                BaseEntity.setValueInEntity(annualPlanner, "m" + month + "D" + Integer.parseInt(day), "A");
                            }
                        }
                        calendar.add(Calendar.DAY_OF_MONTH, 1);

                    } while (calendar.getTime().compareTo(employeeCourse.getCourseInformation().getEndDate()) <= 0);

                }
               //</editor-fold>
            }
        }
        catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
   @Override
    public OFunctionResult validateEmployeeCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            EmployeeCourse employeeCourse = (EmployeeCourse)odm.getData().get(0);
            
            if(employeeCourse!=null)
            {
                List<String> statusConds = new ArrayList<String>();
                statusConds.add("type.code='EmployeeCourse'");
                statusConds.add("code='0.A'");
                UDC status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                employeeCourse.setStatus(status);
                
                if(employeeCourse.getCourseInformation()!=null){
                    ProviderCourseInformation courseInformation = employeeCourse.getCourseInformation();
                    courseInformation.setRequestersNumber(
                            courseInformation.getRequestersNumber() + 1);
                     entitySetupService.callEntityUpdateAction(courseInformation, loggedUser);
                }
                
        ProviderCourseInformation provCourseInfo;
        provCourseInfo = employeeCourse.getCourseInformation();
        Date startDate;
        Date endDate ;
        String notes;
        String courseAddress ;
        startDate = provCourseInfo.getStartDate();
        endDate = provCourseInfo.getEndDate();
        notes = provCourseInfo.getNotes();
        courseAddress = provCourseInfo.getCourseAddress();
        
        ProviderCourse  proCourse ; 
        proCourse = provCourseInfo.getCourse();
        String courseName ;
        courseName = proCourse.getDescription();
        
        UDC learningMethod;
        String learningMethod_value;
        
        learningMethod = proCourse.getLearningMethod();
        learningMethod_value = learningMethod.getValue() ;     
        
        ProviderCourseClass proCourseClass ;
        proCourseClass = proCourse.getCourseClass();
        
        Provider provider ;
        provider = proCourseClass.getProvider() ;
        String providerName ;
        providerName = provider.getDescription();
        
        ///getDescription()

            if (providerName != null)
            {
                employeeCourse.setProviderName(providerName);
            }
            if(courseName != null)
            {
                employeeCourse.setCourseName(courseName);
            }
            if(startDate != null)
            {
                employeeCourse.setStartDate(startDate);
            }
            if(endDate != null)
            {
                employeeCourse.setEndDate(endDate);
            }
            
            if(notes != null)
            {
                employeeCourse.setNotes(notes);
            }
            
            if(courseAddress != null)
            {
                employeeCourse.setCourseAddress(courseAddress);
            }
            
            if(learningMethod_value != null)
            {
                employeeCourse.setLearningMethod(learningMethod_value);
            }
                
                ofr.setReturnedDataMessage(odm);
            }
        }
        catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult postEmployeeCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
//            oem.getEM(loggedUser);
            EmployeeCourse employeeCourse = (EmployeeCourse) odm.getData().get(0);

            if (employeeCourse != null) {
                
                //load old employeeCourse
                List<String> conds = new ArrayList<String>();
                conds.add("dbid = "+ employeeCourse.getDbid());
                EmployeeCourse loadedEmployeeCourse = (EmployeeCourse) oem.loadEntity(EmployeeCourse.class.getSimpleName(), 
                        conds, null, loggedUser);
                
                Calendar today = Calendar.getInstance();
                Date todayDate = today.getTime();

                Employee loggedEmployee = null;
                OUser ouser = loggedUser;
                if (ouser instanceof EmployeeUser || ouser instanceof EDSEmployeeUser) {
                    long dbid = 0;
                    try {
                        dbid = (Long) BaseEntity.getValueFromEntity(ouser, "employee.dbid");
                    } catch (Exception ex) {
                        Logger.getLogger(TrainingService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        loggedEmployee = (Employee) oem.loadEntity(Employee.class.getSimpleName(), dbid, null, null, ouser);
                    } catch (Exception ex) {
                        Logger.getLogger(TrainingService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                UDC currentStatus = employeeCourse.getStatus();
                String statusStr = currentStatus.getCode().substring(0, (currentStatus.getCode().length() - 2));
                int currentStatusID = Integer.parseInt(statusStr);

                UDC status = null;
                List<String> statusConds = new ArrayList<String>();
                //17-1. when request is approved, status is approved, if not then status: not approved
                if (currentStatusID <= 1 && employeeCourse.getApproved() != null) {
                    if (employeeCourse.getApproved().getCode() == null) {
                        employeeCourse.setApprovalDate(null);
                        employeeCourse.setApprovedByWhom(null);
                    }
                    if (employeeCourse.getApproved().getCode().equals("Yes")) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='1.A'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setApprovalDate(todayDate);
                        employeeCourse.setApprovedByWhom(loggedEmployee);
                    } else if (employeeCourse.getApproved().getCode().equals("No")) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='1.B'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setApprovedByWhom(loggedEmployee);
                    }
                }
                //17.2. when request is selected, status is (HR planning), if not selected, then status is (not selected by HR)
                if (currentStatusID <= 2 && employeeCourse.getCourseSelected() != null) {
                    if (employeeCourse.getCourseSelected().getCode() == null) {
                        employeeCourse.setSelectionDate(null);
                        employeeCourse.setSelectedByWhom(null);
                    } else if (employeeCourse.getCourseSelected().getCode().equals("Yes") &&
                            ((loadedEmployeeCourse.getCourseSelected()!=null &&
                                !loadedEmployeeCourse.getCourseSelected().getCode().equals("Yes")) 
                            || loadedEmployeeCourse.getCourseSelected()==null)) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='2.A'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setSelectionDate(todayDate);
                        employeeCourse.setSelectedByWhom(loggedEmployee);
                        // increment selectedNum in courseInfo
                        ProviderCourseInformation courseInformation = employeeCourse.getCourseInformation();
                        if (courseInformation != null) {
                            courseInformation.setSelectedNumber(courseInformation.getSelectedNumber() + 1);

                            // set course -> underPlanning
                            if (courseInformation.getStatus()!=null &&
                                    !courseInformation.getStatus().getCode().equals("UnderPlanning")) {
                                statusConds.clear();
                                statusConds.add("type.code='CourseStatus'");
                                statusConds.add("code='UnderPlanning'");
                                status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                                courseInformation.setStatus(status);
                            }
                            oem.saveEntity(courseInformation, loggedUser);
                        }
                    } else if (employeeCourse.getCourseSelected().getCode().equals("No") &&
                            ((loadedEmployeeCourse.getCourseSelected()!=null &&
                                !loadedEmployeeCourse.getCourseSelected().getCode().equals("No")) 
                            || loadedEmployeeCourse.getCourseSelected()==null)) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='2.B'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setSelectionDate(todayDate);
                        employeeCourse.setSelectedByWhom(loggedEmployee);
                        
                        ProviderCourseInformation courseInformation = employeeCourse.getCourseInformation();
                        if (courseInformation != null) {
                            courseInformation.setSelectedNumber(courseInformation.getSelectedNumber() - 1);
                            oem.saveEntity(courseInformation, loggedUser);
                        }
                    }
                }
                //17.3. when request is accepted, status is (employee acceptance), if not (employee does not accept)
                if (currentStatusID <= 3 && employeeCourse.getAccepted() != null) {
                    if (employeeCourse.getAccepted().getCode() == null) {
                        employeeCourse.setAcceptanceDate(null);
                        employeeCourse.setAcceptedByWhom(null);
                    } else if (employeeCourse.getAccepted().getCode().equals("Yes")&&
                            ((loadedEmployeeCourse.getAccepted()!=null &&
                                !loadedEmployeeCourse.getAccepted().getCode().equals("Yes")) 
                            || loadedEmployeeCourse.getAccepted()==null)) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='3.A'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setAcceptanceDate(todayDate);
                        employeeCourse.setAcceptedByWhom(loggedEmployee);
                        // increment acceptedNum in courseInfo
                        ProviderCourseInformation courseInformation = employeeCourse.getCourseInformation();
                        if (courseInformation != null) {
                            courseInformation.setAcceptedNumber(courseInformation.getAcceptedNumber() + 1);
                            oem.saveEntity(courseInformation, loggedUser);
                        }
                        
                        //<editor-fold defaultstate="collapsed" desc="budget element affect">
                        EmployeeBudgetElement empBE = null;
                        BigDecimal coursePrice = courseInformation.getCoursePricePerAttendee();
                        
                        // get month of acceptance
                        String actualMonth = "actual";
                        if(coursePrice!=null && employeeCourse.getAcceptanceDate()!=null){                            
                            String accMonth = MONTH_FOTMAT.format(employeeCourse.getAcceptanceDate());
                            if(accMonth.equals("01")){
                                actualMonth += "JanValue";
                            } else if(accMonth.equals("02")){
                                actualMonth += "FebValue";
                            } else if(accMonth.equals("03")){
                                actualMonth += "MarValue";
                            } else if(accMonth.equals("04")){
                                actualMonth += "AprValue";
                            } else if(accMonth.equals("05")){
                                actualMonth += "MayValue";
                            } else if(accMonth.equals("06")){
                                actualMonth += "JunValue";
                            } else if(accMonth.equals("07")){
                                actualMonth += "JulValue";
                            } else if(accMonth.equals("08")){
                                actualMonth += "AugValue";
                            } else if(accMonth.equals("09")){
                                actualMonth += "SepValue";
                            } else if(accMonth.equals("10")){
                                actualMonth += "OctValue";
                            } else if(accMonth.equals("11")){
                                actualMonth += "NovValue";
                            } else if(accMonth.equals("12")){
                                actualMonth += "DecValue";
                            } 

                            String budgetElCode = "";
                            
                            SystemLookUpDetail sysLkUpDetail = null;
                            List<String> lkUpDetailConds = new ArrayList<String>();
                            lkUpDetailConds.add("systemLookUp.code = 'TrainingSetup'");
                            lkUpDetailConds.add("code = 'TrBudgetElement'");
                            sysLkUpDetail = (SystemLookUpDetail) oem.loadEntity(SystemLookUpDetail.class.getSimpleName(), 
                                    lkUpDetailConds, null, loggedUser);
                            
                            budgetElCode = sysLkUpDetail.getDescription();
                            
                            List<String> sort = new ArrayList<String>();
                            sort.add("instanceYear");
                            List<BudgetInstance> instances = oem.loadEntityList(BudgetInstance.class.getSimpleName(), 
                                    null, null, sort, loggedUser);
                            
                            BudgetInstance budgetInstance = null;
                            if(instances!=null && !instances.isEmpty()){
                                budgetInstance = instances.get(instances.size()-1);
                            }
                            
                            List<String> empBEConds = new ArrayList<String>();
                            empBEConds.add("employee.dbid = " + employeeCourse.getEmployee().getDbid());
                            empBEConds.add("budgetElement.code = '" + budgetElCode + "'");
                            empBEConds.add("budgetInstance.dbid = " + budgetInstance.getDbid());
                            //empBEConds.add("budgetElement.internalType.code = 'Training001'");
                            //empBEConds.add("budgetElement.internalType.systemLookUp.code = 'TrainingBE'");
                            empBE = (EmployeeBudgetElement)oem.loadEntity(EmployeeBudgetElement.class.getSimpleName(), empBEConds, null, loggedUser);

                            if(empBE!=null){
                                BigDecimal oldVal = (BigDecimal)BaseEntity.getValueFromEntity(empBE, actualMonth);
                                BigDecimal newVal = coursePrice.add(oldVal);
                                BaseEntity.setValueInEntity(empBE, actualMonth, newVal);
                                OFunctionResult empBEOFR = entitySetupService.callEntityUpdateAction(empBE, loggedUser);
                                if(empBEOFR!=null && empBEOFR.getErrors().size() > 1){
                                    ofr.addError(usrMsgService.getUserMessage("TrEmpBE", loggedUser));
                                    OLog.logError("Updating EmployeeBudgetElement - Error Encountered", loggedUser,
                                            "EmployeeBudgetElement", empBE);
                                }
                            }
                        }
                        else if(coursePrice==null){
                            ofr.addError(usrMsgService.getUserMessage("NoCoursePrice", loggedUser));
                        } else if(employeeCourse.getAcceptanceDate()==null){
                            ofr.addError(usrMsgService.getUserMessage("NoAcceptanceDate", loggedUser));
                        }
                        //</editor-fold>
                    } else if (employeeCourse.getAccepted().getCode().equals("No")&&
                            ((loadedEmployeeCourse.getAccepted()!=null &&
                                !loadedEmployeeCourse.getAccepted().getCode().equals("No")) 
                            || loadedEmployeeCourse.getAccepted()==null)) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='3.B'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setAcceptedByWhom(loggedEmployee);
                        // decrement acceptedNum in courseInfo
                        ProviderCourseInformation courseInformation = employeeCourse.getCourseInformation();
                        if (courseInformation != null) {
                            courseInformation.setAcceptedNumber(courseInformation.getAcceptedNumber() - 1);
                            oem.saveEntity(courseInformation, loggedUser);
                        }
                    }
                }
                //17.4. when request is enrolled, status is (course enrollment), if not (course not enrolled)
                if (currentStatusID <= 4 && employeeCourse.getEnrolled() != null) {
                    if (employeeCourse.getEnrolled().getCode() == null) {
                        employeeCourse.setEnrollementDate(null);
                        employeeCourse.setEnrolledBy(null);
                    } else if (employeeCourse.getEnrolled().getCode().equals("Yes")) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='4.A'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setEnrollementDate(todayDate);
                        employeeCourse.setEnrolledBy(loggedEmployee);
                    } else if (employeeCourse.getEnrolled().getCode().equals("No")) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='4.B'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setEnrolledBy(loggedEmployee);
                    }
                }

                //17.5. when employee attends, status is (employee attended), if not (employee absence)
                if (currentStatusID <= 5 && employeeCourse.getAttended() != null) {
                    if (employeeCourse.getAttended().getCode() == null) {
                        employeeCourse.setAttendenceDate(null);
                    } else if (employeeCourse.getAttended().getCode().equals("Yes") &&
                            ((loadedEmployeeCourse.getAttended()!=null &&
                                !loadedEmployeeCourse.getAttended().getCode().equals("Yes")) 
                            || loadedEmployeeCourse.getAttended()==null)) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='5.A'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setAttendenceDate(todayDate);
                        // increment attendeesNum in courseInfo
                        ProviderCourseInformation courseInformation = employeeCourse.getCourseInformation();
                        if (courseInformation != null) {
                            courseInformation.setAttendeesNumber(courseInformation.getAttendeesNumber() + 1);
                            oem.saveEntity(courseInformation, loggedUser);
                        }                                                                        
                        EDSIDPActionForBusinessOutcome action = employeeCourse.getAction();
                        
                        action.setActualCompletProg(100);
                        action.setActualStartDate(action.getPlannedStartDate());
                        action.setActualFinishedDate(action.getPlannedEndDate());
                        action.setCompletionDate(action.getPlannedEndDate());
                        
                        
                        entitySetupService.callEntityUpdateAction(action, ouser);
                                                        
                    } else if (employeeCourse.getAttended().getCode().equals("No") &&
                            ((loadedEmployeeCourse.getAttended()!=null &&
                                !loadedEmployeeCourse.getAttended().getCode().equals("No")) 
                            || loadedEmployeeCourse.getAttended()==null)) {
                        statusConds.clear();
                        statusConds.add("type.code='EmployeeCourse'");
                        statusConds.add("code='5.B'");
                        status = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                        employeeCourse.setStatus(status);
                        employeeCourse.setStatusDate(todayDate);
                        employeeCourse.setAttendenceDate(todayDate);
                        // decrement attendeesNum in courseInfo
                        ProviderCourseInformation courseInformation = employeeCourse.getCourseInformation();
                        if (courseInformation != null) {
                            courseInformation.setAttendeesNumber(courseInformation.getAttendeesNumber() - 1);
                            oem.saveEntity(courseInformation, loggedUser);
                        }
                    }

                }
                //oem.saveEntity(employeeCourse, loggedUser);

                ofr.setReturnedDataMessage(odm);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        } finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    private int compareTime(String time1, String time2) {

        Integer time1Int = Integer.parseInt(time1.replaceAll(":", "").replaceAll("\\.", ""));
        Integer time2Int = Integer.parseInt(time2.replaceAll(":", "").replaceAll("\\.", ""));

        if (time1Int > time2Int) {
            return 1;
        } else if (time1Int < time2Int) {
            return -1;
        } else {
            return 0;
        }
    }
    
    private boolean validateTimeFormat(String time) {
        Pattern pattern = Pattern.compile(TIME24HOURS_PATTERN);
        Matcher matcher = pattern.matcher(time);
        return matcher.matches();
    }
    
    private String unifyTimeValue(String time) {
        String unifiedTime = time;
        if(time!=null && !time.isEmpty() && !time.contains(":")){
            if(time.contains(".")){
               String[] tempTime = time.split(".");
               unifiedTime = tempTime[0] + ":" + tempTime[1];
            }
            else {
               String [] tempTime = new String[2];
               tempTime[0] = time.substring(0, 2);
               tempTime[1] = time.substring(2, 4);
               unifiedTime = tempTime[0] + ":" + tempTime[1];
            }
        }
        return unifiedTime;
    }
    
    @Override
    public OFunctionResult validateProviderTrainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
            ProviderTrainingPlan providerTrainingPlan = (ProviderTrainingPlan) odm.getData().get(0);
            if(providerTrainingPlan!=null){
                List<String> statusConds = new ArrayList<String>();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Announced'");
                UDC announcedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                statusConds.clear();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Closed'");
                UDC closedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                statusConds.clear();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Finished'");
                UDC finishedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                long closedDbid=0;
                long announcedDbid=0;
                long finishedDbid=0;
                if(finishedStatus != null)
                {
                    finishedDbid = finishedStatus.getDbid();
                }
                if(closedStatus != null)
                {
                    closedDbid = closedStatus.getDbid();
                }
                if(announcedStatus != null)
                {
                    announcedDbid = announcedStatus.getDbid();
                }
                ProviderTrainingPlan savedPlan;
                UDC planStatus;
                savedPlan = (ProviderTrainingPlan)oem.loadEntity("ProviderTrainingPlan", providerTrainingPlan.getDbid(), null, null, loggedUser);
                if (savedPlan !=null)
                {
                    planStatus = savedPlan.getStatus();
                    if ((planStatus != null) )
                    {
                        if((planStatus.getDbid() == announcedDbid)||
                                (planStatus.getDbid() == closedDbid) ||
                                (planStatus.getDbid() == finishedDbid))
                        {
                            //Plan Announced
                            //Name Validation
                            String planName;
                            String savedPlanName;
                            planName = providerTrainingPlan.getDescription();
                            savedPlanName = savedPlan.getDescription();
                            if ((planName == null ? savedPlanName != null : !planName.equals(savedPlanName)))
                            {
                                ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce002", loggedUser));
                                return ofr;
                            }
                            //Date Validation
                            Date valDate;
                            Date savedValDate;
                            valDate = providerTrainingPlan.getStartDate();
                            savedValDate = savedPlan.getStartDate();
                            if ((valDate == null ? savedValDate != null : !valDate.equals(savedValDate)))
                            {
                                ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce003", loggedUser));
                                return ofr;
                            }
                            valDate = providerTrainingPlan.getEndDate();
                            savedValDate = savedPlan.getEndDate();
                            if ((valDate == null ? savedValDate != null : !valDate.equals(savedValDate)))
                            {
                                ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce004", loggedUser));
                                return ofr;
                            }
                            //Plan Duration
                            Integer planDuration;
                            Integer savedPlanDuration;
                            planDuration = providerTrainingPlan.getPlanDuration();
                            savedPlanDuration = savedPlan.getPlanDuration();
                            if ((planDuration == null ? savedPlanDuration != null : !planDuration.equals(savedPlanDuration)))
                            {
                                ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce005", loggedUser));
                                return ofr;
                            }
                        }
                    }
                }
                
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
        } 
        finally {
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult validateNewProviderCourseInfo(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderCourseInformation courseInformation = (ProviderCourseInformation) odm.getData().get(0);
            if(courseInformation!=null){
                
                ProviderTrainingPlan trainingPlan = courseInformation.getTrainingPlan();
                UDC planStatus;
                planStatus = trainingPlan.getStatus();
                List<String> statusConds = new ArrayList<String>();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Announced'");
                UDC announcedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                statusConds.clear();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Closed'");
                UDC closedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                statusConds.clear();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Finished'");
                UDC finishedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                long closedDbid=0;
                long announcedDbid=0;
                long finishedDbid=0;
                if(finishedStatus != null)
                {
                    finishedDbid = finishedStatus.getDbid();
                }
                if(closedStatus != null)
                {
                    closedDbid = closedStatus.getDbid();
                }
                if(announcedStatus != null)
                {
                    announcedDbid = announcedStatus.getDbid();
                }
                if(planStatus!=null)
                {
                    if((planStatus.getDbid() == announcedDbid)||
                                (planStatus.getDbid() == closedDbid) ||
                                (planStatus.getDbid() == finishedDbid))
                        {
                            ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce006", loggedUser));
                            return ofr;
                        }
                }
                
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    @Override
    public OFunctionResult validatePlannedPCI(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult ofr = new OFunctionResult();
        try{
//            oem.getEM(loggedUser);
            ProviderCourseInformation courseInformation = (ProviderCourseInformation) odm.getData().get(0);
            if(courseInformation!=null){
                
                ProviderTrainingPlan trainingPlan = courseInformation.getTrainingPlan();
                UDC planStatus;
                planStatus = trainingPlan.getStatus();
                List<String> statusConds = new ArrayList<String>();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Announced'");
                UDC announcedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                statusConds.clear();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Closed'");
                UDC closedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                statusConds.clear();
                statusConds.add("type.code = 'TrainingPlan'");
                statusConds.add("code = 'Finished'");
                UDC finishedStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), statusConds, null, loggedUser);
                long closedDbid=0;
                long announcedDbid=0;
                long finishedDbid=0;
                if(finishedStatus != null)
                {
                    finishedDbid = finishedStatus.getDbid();
                }
                if(closedStatus != null)
                {
                    closedDbid = closedStatus.getDbid();
                }
                if(announcedStatus != null)
                {
                    announcedDbid = announcedStatus.getDbid();
                }
                if(planStatus!=null)
                {
                    if((planStatus.getDbid() == announcedDbid)||
                                (planStatus.getDbid() == closedDbid) ||
                                (planStatus.getDbid() == finishedDbid))
                        {
                            ProviderCourseInformation savedCourseInfo = (ProviderCourseInformation)oem.loadEntity("ProviderCourseInformation", courseInformation.getDbid(), null, null, loggedUser);
                            if (savedCourseInfo != null)
                            {
                                //Date Validation
                                Date valDate;
                                Date savedValDate;
                                valDate = courseInformation.getStartDate();
                                savedValDate = savedCourseInfo.getStartDate();
                                if ((valDate == null ? savedValDate != null : !valDate.equals(savedValDate)))
                                {
                                    ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce007", loggedUser));
                                    return ofr;
                                }
                                valDate = courseInformation.getEndDate();
                                savedValDate = savedCourseInfo.getEndDate();
                                if ((valDate == null ? savedValDate != null : !valDate.equals(savedValDate)))
                                {
                                    ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce008", loggedUser));
                                    return ofr;
                                }
                                //Time
                                String valTime;
                                valTime = courseInformation.getSessionStartTime();
                                if ((valTime == null ? savedValDate != null : !valTime.equals(savedValDate)))
                                {
                                    ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce009", loggedUser));
                                    return ofr;
                                }
                                valTime = courseInformation.getSessionEndTime();
                                if ((valTime == null ? savedValDate != null : !valTime.equals(savedValDate)))
                                {
                                    ofr.addError(usrMsgService.getUserMessage("TrainingAnnounce010", loggedUser));
                                    return ofr;
                                }
                            }
                        }
                }
                
            }
        }
        catch(Exception ex){
            OLog.logException(ex, loggedUser);
        } 
        finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }
    
    private long subtractDates(Date startDate, Date endDate) {
        //int numberOfDays = Days.daysBetween(fromDT, toDT).getDays();
        
        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        
        long timeInMillis = end.getTimeInMillis() - start.getTimeInMillis();
        long days = (timeInMillis/(1000 * 60 * 60 * 24));
        
        long months = days / 30;
        
        return months;
    }
    
    @Override
    public OFunctionResult employeeProviderUE(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser)
    {
        OFunctionResult oFR = new OFunctionResult();
        List<String> finalConds = new ArrayList<String>();
        
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate Input & Return Error If Found">
               OFunctionResult inputValidationErrors = functionService.validateJavaFunctionODM(
                       odm, loggedUser, true, List.class, BaseEntity.class, ODataMessage.class, OScreen.class) ;
               if (inputValidationErrors != null) return inputValidationErrors ;
               // </editor-fold>
               
               
               List<String> conds = new ArrayList<String>();
               
               if(loggedUser instanceof EmployeeUser){
                    conds = Collections.singletonList("employee.dbid ="+Long.toString(((EmployeeUser) loggedUser).getEmployee().getDbid())) ;
                }else if (loggedUser instanceof EDSEmployeeUser){
                    conds = Collections.singletonList("employee.dbid ="+Long.toString(((EDSEmployeeUser) loggedUser).getEmployee().getDbid())) ;
                }
               
               List<EmployeeCourse> courses = null;
               if(conds.size()>0){
                 courses = oem.loadEntityList(EmployeeCourse.class.getSimpleName(),
                          conds ,null, null, loggedUser);
               }
               
               String con = "";
               
               if (courses==null || courses.size() > 0) {
                   con = "dbid in (";
                for (int i = 0; i < courses.size(); i++) {
                    if(i>0){
                        con += " ,";
                    }                    
                    con += courses.get(i).getCourseInformation().getTrainingPlan().getProvider().getDbid();
                }
                con+= ")";
            } else {
                con = "dbid =0";
            }
               finalConds.add(con);
        } catch (Exception ex) {
           OLog.logException(ex, loggedUser);
           finalConds.add("dbid = 0");
        }finally{
            OFunctionResult constructFR = uIFrameworkService.constructScreenDataLoadingUEConditionsOFR(finalConds, loggedUser) ;
            oFR.append(constructFR) ;
            oFR.setReturnedDataMessage(constructFR.getReturnedDataMessage());
            return oFR ;
        }
        
        
    }
}