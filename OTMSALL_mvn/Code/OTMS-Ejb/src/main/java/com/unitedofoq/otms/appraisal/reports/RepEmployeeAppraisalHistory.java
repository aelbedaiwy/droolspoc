
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.util.Date;
import javax.persistence.*;
import java.math.BigDecimal;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeAppraisalHistory extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeAppraisalHistory_dsDbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="year">
    @Column
    private String year;

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public String getYearDD() {
        return "RepEmployeeAppraisalHistory_year";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "RepEmployeeAppraisalHistory_startDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="finishDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finishDate;

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public String getFinishDateDD() {
        return "RepEmployeeAppraisalHistory_finishDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="duration">
    @Column
    private String duration;

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public String getDurationDD() {
        return "RepEmployeeAppraisalHistory_duration";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="purpose">
    @Column
    private String purpose;

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getPurposeDD() {
        return "RepEmployeeAppraisalHistory_purpose";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    @Translatable(translationField = "templateTranslated")
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public String getTemplateDD() {
        return "RepEmployeeAppraisalHistory_template";
    }
    
    @Transient
    @Translation(originalField = "template")
    private String templateTranslated;

    public String getTemplateTranslated() {
        return templateTranslated;
    }

    public void setTemplateTranslated(String templateTranslated) {
        this.templateTranslated = templateTranslated;
    }
    
    public String getTemplateTranslatedDD() {
        return "RepEmployeeAppraisalHistory_template";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="totalScore">
    @Column
    private BigDecimal totalScore;

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public String getTotalScoreDD() {
        return "RepEmployeeAppraisalHistory_totalScore";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="averageScore">
    private BigDecimal averageScore;
    
    public BigDecimal getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(BigDecimal averageScore) {
        this.averageScore = averageScore;
    }
    
    public String getAverageScoreDD() {
        return "RepEmployeeAppraisalHistory_averageScore";
    } 
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="finalRating">
    @Column
    private String finalRating;

    public String getFinalRating() {
        return finalRating;
    }

    public void setFinalRating(String finalRating) {
        this.finalRating = finalRating;
    }
    
    public String getFinalRatingDD() {
        return "RepEmployeeAppraisalHistory_finalRating";
    }
    // </editor-fold>
}