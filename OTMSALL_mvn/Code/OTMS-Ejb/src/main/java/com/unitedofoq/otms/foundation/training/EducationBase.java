/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@MappedSuperclass
public class EducationBase extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="scientificDegree">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private UDC scientificDegree;

    public UDC getScientificDegree() {
        return scientificDegree;
    }

    public String getScientificDegreeDD() {
        return "EducationBase_scientificDegree";
    }

    public void setScientificDegree(UDC scientificDegree) {
        this.scientificDegree = scientificDegree;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="speciality">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private UDC speciality;

    public UDC getSpeciality() {
        return speciality;
    }

    public String getSpecialityDD() {
        return "EducationBase_speciality";
    }

    public void setSpeciality(UDC speciality) {
        this.speciality = speciality;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="university">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private University university;

    public University getUniversity() {
        return university;
    }

    public String getUniversityDD() {
        return "EducationBase_university";
    }

    public void setUniversity(University university) {
        this.university = university;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="faculty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Faculty faculty;

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public String getFacultyDD() {
        return "EducationBase_faculty";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="facultyDepartment">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private FacultyDepartment facultyDepartment;

    public FacultyDepartment getFacultyDepartment() {
        return facultyDepartment;
    }

    public void setFacultyDepartment(FacultyDepartment facultyDepartment) {
        this.facultyDepartment = facultyDepartment;
    }

    public String getFacultyDepartmentDD() {
        return "facultyDepartment_facultyDepartment";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="degree">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC degree;

    public UDC getDegree() {
        return degree;
    }

    public String getDegreeDD() {
        return "EducationBase_degree";
    }

    public void setDegree(UDC degree) {
        this.degree = degree;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="major">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC major;

    public UDC getMajor() {
        return major;
    }

    public void setMajor(UDC major) {
        this.major = major;
    }

    public String getMajorDD() {
        return "EducationBase_major";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="school">
    private boolean school;

    public boolean isSchool() {
        return school;
    }

    public void setSchool(boolean school) {
        this.school = school;
    }

    public String getSchoolDD() {
        return "EducationBase_school";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="schDescription">
    @Column
    @Translatable(translationField = "schDescriptionTranslated")
    private String schDescription;
    @Transient
    @Translation(originalField = "schDescription")
    private String schDescriptionTranslated;

    public String getSchDescription() {
        return schDescription;
    }

    public void setSchDescription(String schDescription) {
        this.schDescription = schDescription;
    }

    public String getSchDescriptionTranslatedDD() {
        return "EducationBase_description";
    }

    public String getSchDescriptionTranslated() {
        return schDescriptionTranslated;
    }

    public void setDescriptionTranslated(String schDescriptionTranslated) {
        this.schDescriptionTranslated = schDescriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="schDegree">
    @Column
    private String schDegree;

    public String getSchDegree() {
        return schDegree;
    }

    public void setSchDegree(String schDegree) {
        this.schDegree = schDegree;
    }

    public String getSchDegreeDD() {
        return "EducationBase_schDegree";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="schGrade">
    @Column
    private String schGrade;

    public String getSchGrade() {
        return schGrade;
    }

    public void setSchGrade(String schGrade) {
        this.schGrade = schGrade;
    }

    public String getSchGradeDD() {
        return "EducationBase_schGrade";
    }
    // </editor-fold >

}
