/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author lap2
 */
@Entity
@ParentEntity(fields="company")
public class GLAccountUDC extends BusinessObjectBaseEntity 
{
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "GLAccountUDC_company";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="businessUdc">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private BusinessUDC businessUdc;

    public BusinessUDC getBusinessUdc() {
        return businessUdc;
    }

    public void setBusinessUdc(BusinessUDC businessUdc) {
        this.businessUdc = businessUdc;
    }
    
    public String getBusinessUdcDD() {
        return "GLAccountUDC_businessUdc";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="glUdc">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }
    
    public String getGlAccountDD() {
        return "GLAccountUDC_glAccount";
    }
    //</editor-fold>

}
