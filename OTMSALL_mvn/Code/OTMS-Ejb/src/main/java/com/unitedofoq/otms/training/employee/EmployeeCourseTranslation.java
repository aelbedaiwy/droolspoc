/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author lap2
 */
@Entity
public class EmployeeCourseTranslation extends BaseEntityTranslation{
    //<editor-fold defaultstate="collapsed" desc="employeeComments">
    private String employeeComments;

    public void setEmployeeComments(String employeeComments) {
        this.employeeComments = employeeComments;
    }

    public String getEmployeeComments() {
        return employeeComments;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="commentsOnEmployee">
    private String commentsOnEmployee;

    public void setCommentsOnEmployee(String commentsOnEmployee) {
        this.commentsOnEmployee = commentsOnEmployee;
    }

    public String getCommentsOnEmployee() {
        return commentsOnEmployee;
    }
    //</editor-fold>
    
}
