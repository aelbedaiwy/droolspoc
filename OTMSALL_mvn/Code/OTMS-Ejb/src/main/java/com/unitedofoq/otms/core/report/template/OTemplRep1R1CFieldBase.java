package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.report.OReportField;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "FTYPE")
//@DiscriminatorColumn Don't allow instantiation
public class OTemplRep1R1CFieldBase extends OReportField {

    // <editor-fold defaultstate="collapsed" desc="orderSequesnce">
    private Long orderSequesnce;

    public Long getOrderSequesnce() {
        return orderSequesnce;
    }

    public void setOrderSequesnce(Long orderSequesnce) {
        this.orderSequesnce = orderSequesnce;
    }

    /**
     * @return the orderSequesnce
     */
    public String getOrderSequesnceDD() {
        return "OTemplRep1R1CFieldBase_orderSequesnce";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="groupedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC groupedBy;

    public UDC getGroupedBy() {
        return groupedBy;
    }

    public void setGroupedBy(UDC groupedBy) {
        this.groupedBy = groupedBy;
    }

    /**
     * @return the sortedBy
     */
    public String getGroupedByDD() {
        return "OTemplRep1R1CFieldBase_groupedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="orepTemplDSField">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORepTemplDSField orepTemplDSField;

    /**
     * @return the oRepTemplDSField
     */
    public ORepTemplDSField getOrepTemplDSField() {
        return orepTemplDSField;
    }

    /**
     * @param oRepTemplDSField the oRepTemplDSField to set
     */
    public void setOrepTemplDSField(ORepTemplDSField oRepTemplDSField) {
        this.orepTemplDSField = oRepTemplDSField;
    }

    /**
     * @return the oRepTemplDSFieldDD
     */
    public String getOrepTemplDSFieldDD() {
        return "OTemplRep1R1CFieldBase_oRepTemplDSField";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="partType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC partType;

    /**
     * @return the partType
     */
    public UDC getPartType() {
        return partType;
    }

    /**
     * @param partType the partType to set
     */
    public void setPartType(UDC partType) {
        this.partType = partType;
    }

    /**
     * @return the partTypeDD
     */
    public String getPartTypeDD() {
        return "OTemplRep1R1CFieldBase_partType";
    }
    // </editor-fold>
}
