package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.payroll.*;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@ParentEntity(fields = "calculatedPeriod")
public class FinancialIntegrationSummary extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="amount">

    @Column(precision = 25, scale = 13)
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAmountDD() {
        return "FinancialIntegrationSummary_amount";
    }
    @Transient
    private BigDecimal amountMask;

    public BigDecimal getAmountMask() {
        amountMask = amount;
        return amountMask;
    }

    public void setAmountMask(BigDecimal amountMask) {
        updateDecimalValue("amount", amountMask);
    }

    public String getAmountMaskDD() {
        return "FinancialIntegrationSummary_amountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }

    public String getCalculatedPeriodDD() {
        return "FinancialIntegrationSummary_calculatedPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;  //Copy of the salary element currency

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "FinancialIntegrationSummary_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "FinancialIntegrationSummary_salaryElement";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="creditOrDebit">
    @Column(length = 1)
    private String creditOrDebit;

    public String getCreditOrDebit() {
        return creditOrDebit;
    }

    public void setCreditOrDebit(String creditOrDebit) {
        this.creditOrDebit = creditOrDebit;
    }

    public String getCreditOrDebitDD() {
        return "FinancialIntegrationSummary_creditOrDebit";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="accountNumber">
    @Column
    private String accountNumber;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumberDD() {
        return "FinancialIntegrationSummary_accountNumber";
    }
    //</editor-fold >
}
