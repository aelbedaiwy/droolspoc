/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields="jobGap")
@Table(name= "edsidpbusoutforjobgapaac")
public class EDSIDPBusinessOutcomeForJobGapAAC extends BaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSIDPBusinessOutcomeForJobGap jobGap;

    public String getJobGapDD() {
        return "EDSIDPBusinessOutcomeForJobGapAAC_jobGap";
    }

    public EDSIDPBusinessOutcomeForJobGap getJobGap() {
        return jobGap;
    }

    public void setJobGap(EDSIDPBusinessOutcomeForJobGap jobGap) {
        this.jobGap = jobGap;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private EDSIDPAccountability accountability;

    public String getAccountabilityDD() {
        return "EDSIDPBusinessOutcomeForJobGapAAC_accountability";
    }

    public EDSIDPAccountability getAccountability() {
        return accountability;
    }

    public void setAccountability(EDSIDPAccountability accountability) {
        this.accountability = accountability;
    }


}