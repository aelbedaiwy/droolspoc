/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll.salaryelement;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;


/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields="company")
public class SalaryElementType extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "SalaryElementType_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "SalaryElementType_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="incomeOrDeduction">
    @Column(nullable=false)
    private String incomeOrDeduction;

    public String getIncomeOrDeduction() {
        return incomeOrDeduction;
    }

    public void setIncomeOrDeduction(String incomeOrDeduction) {
        this.incomeOrDeduction = incomeOrDeduction;
    }
    public String getIncomeOrDeductionDD() {
        return "SalaryElementType_incomeOrDeduction";
    }
    // </editor-fold>
    
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
      public String getCompanyDD() {
        return "SalaryElementType_company";
    }
}
