package com.unitedofoq.otms.appraisal.goal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplateSequence;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class DepartmentGoalFilter extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="sequence">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplateSequence sequence;

    public AppraisalTemplateSequence getSequence() {
        return sequence;
    }

    public void setSequence(AppraisalTemplateSequence sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD() {
        return "DepartmentGoalFilter_sequence";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="unit">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }

    public String getUnitDD() {
        return "DepartmentGoalFilter_unit";
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="position">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "DepartmentGoalFilter_position";
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    //</editor-fold >
}
