package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

@Entity
public class ImportEmployeeSalaryElement extends BaseEntity {
    boolean done;
    public boolean isDone() {
        return done;
    }

    public String getDoneDD() {
        return "ImportEmployeeSalaryElement_done";
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    String elementType;
    public String getElementType() {
        return elementType;
    }

    public String getElementTypeDD() {
        return "ImportEmployeeSalaryElement_elementType";
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    String employeeCode;
    public String getEmployeeCode() {
        return employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "ImportEmployeeSalaryElement_employeeCode";
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    String salaryElementDescription;
    public String getSalaryElementDescription() {
        return salaryElementDescription;
    }

    public String getSalaryElementDescriptionDD() {
        return "ImportEmployeeSalaryElement_salaryElementDescription";
    }

    public void setSalaryElementDescription(String salaryElementDescription) {
        this.salaryElementDescription = salaryElementDescription;
    }

    String salaryElementValue;
    public String getSalaryElementValue() {
        return salaryElementValue;
    }

    public String getSalaryElementValueDD() {
        return "ImportEmployeeSalaryElement_salaryElementValue";
    }

    public void setSalaryElementValue(String salaryElementValue) {
        this.salaryElementValue = salaryElementValue;
    }

    // <editor-fold defaultstate="collapsed" desc="salaryElementValueEnc">
    private String salaryElementValueEnc;

    public String getSalaryElementValueEnc() {
        return salaryElementValueEnc;
    }

    public void setSalaryElementValueEnc(String salaryElementValueEnc) {
        this.salaryElementValueEnc = salaryElementValueEnc;
    }

    public String getSalaryElementValueEncDD() {
        return "ImportEmployeeSalaryElement_salaryElementValueEnc";
    }
    // </editor-fold>
}
