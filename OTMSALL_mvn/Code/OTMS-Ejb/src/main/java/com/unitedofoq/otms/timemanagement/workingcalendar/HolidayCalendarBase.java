package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class HolidayCalendarBase extends BusinessObjectBaseEntity {
    //<editor-fold defaultstate="collapsed" desc="holidayName">

    @Translatable(translationField = "holidayNameTranslated")
    private String holidayName;

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public String getHolidayNameDD() {
        return "HolidayCalendarBase_holidayName";
    }
    @Transient
    @Translation(originalField = "holidayName")
    private String holidayNameTranslated;

    public String getHolidayNameTranslated() {
        return holidayNameTranslated;
    }

    public void setHolidayNameTranslated(String holidayNameTranslated) {
        this.holidayNameTranslated = holidayNameTranslated;
    }

    public String getHolidayNameTranslatedDD() {
        return "HolidayCalendarBase_holidayName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dateFrom">
    @Column(unique = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateFromDD() {
        return "HolidayCalendarBase_dateFrom";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dateTo">
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateToDD() {
        return "HolidayCalendarBase_dateTo";
    }
    //</editor-fold>
}
