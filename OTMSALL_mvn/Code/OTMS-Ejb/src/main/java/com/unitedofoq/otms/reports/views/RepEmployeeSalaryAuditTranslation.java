package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name= "repempsalaudtranslation")
public class RepEmployeeSalaryAuditTranslation extends RepCompanyLevelBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="fixedOrVariable">
    @Column
    private String fixedOrVariable;

    public void setFixedOrVariable(String fixedOrVariable) {
        this.fixedOrVariable = fixedOrVariable;
    }

    public String getFixedOrVariable() {
        return fixedOrVariable;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nature">
    @Column
    private String nature;

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getNature() {
        return nature;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeOrCompany">
    @Column
    private String employeeOrCompany;

    public void setEmployeeOrCompany(String employeeOrCompany) {
        this.employeeOrCompany = employeeOrCompany;
    }

    public String getEmployeeOrCompany() {
        return employeeOrCompany;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    private String genderDescription;

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescription() {
        return genderDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeactive">
    @Column
    private String employeeactive;

    public void setEmployeeactive(String employeeactive) {
        this.employeeactive = employeeactive;
    }

    public String getEmployeeactive() {
        return employeeactive;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElementType">
    @Column
    private String salaryElementType;

    public void setSalaryElementType(String salaryElementType) {
        this.salaryElementType = salaryElementType;
    }

    public String getSalaryElementType() {
        return salaryElementType;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="incomeOrDeduction">
    @Column
    private String incomeOrDeduction;

    public void setIncomeOrDeduction(String incomeOrDeduction) {
        this.incomeOrDeduction = incomeOrDeduction;
    }

    public String getIncomeOrDeduction() {
        return incomeOrDeduction;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentperiod">
    @Column
    private String paymentperiod;

    public void setPaymentperiod(String paymentperiod) {
        this.paymentperiod = paymentperiod;
    }

    public String getPaymentperiod() {
        return paymentperiod;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentmethod">
    @Column
    private String paymentmethod;

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    private String firstName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    private String middleName;

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    private String lastName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    private String fullName;

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    private String locationDescription;

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescription() {
        return locationDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    private String hiringType;

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringType() {
        return hiringType;
    }
    // </editor-fold>
}