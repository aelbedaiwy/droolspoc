/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="requisition")
public class RequisitionCompetency extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="requisition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Requisition requisition;

    public Requisition getRequisition() {
        return requisition;
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }
    
    public String getRequisitionDD(){
        return "RequisitionCompetency_requisition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    
    public String getCompetenciesDD(){
        return "RequisitionCompetency_competency";
    }
    // </editor-fold>
}
