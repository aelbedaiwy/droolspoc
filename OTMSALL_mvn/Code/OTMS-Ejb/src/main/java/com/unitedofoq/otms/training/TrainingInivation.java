/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;

import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mragab
 */
@Entity
public class TrainingInivation extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="trainingRequest">
    @OneToMany
    private List<TrainingRequest> trainingRequest;

    public List<TrainingRequest> getTrainingRequest() {
        return trainingRequest;
    }

    public void setTrainingRequest(List<TrainingRequest> trainingRequest) {
        this.trainingRequest = trainingRequest;
    }

    public String getTrainingRequestDD() {
        return "TrainingInivation_trainingRequest";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="subject">
    @Column
    private String subject;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubjectDD() {
        return "TrainingInivation_subject";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    @Column
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getLocationDD() {
        return "TrainingInivation_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="organizer">
    @Column
    private String organizer;

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }
    
    public String getOrganizerDD() {
        return "TrainingInivation_organizer";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendees">
    @Column
    private String attendees;

    public String getAttendees() {
        return attendees;
    }

    public void setAttendees(String attendees) {
        this.attendees = attendees;
    }
    
    public String getAttendeesDD() {
        return "TrainingInivation_attendees";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="body">
    @Column
    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
    
    public String getBodyDD() {
        return "TrainingInivation_body";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reminderSet">
    @Column
    private boolean reminderSet;

    public boolean isReminderSet() {
        return reminderSet;
    }

    public void setReminderSet(boolean reminderSet) {
        this.reminderSet = reminderSet;
    }

    public String getReminderSetDD() {
        return "TrainingInivation_reminderSet";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sendStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC sendStatus;

    public UDC getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(UDC sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getSendStatusDD() {
        return "TrainingInivation_sendStatus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reminderMinutesBeforeStart">
    @Column
    private Integer reminderMinutesBeforeStart;

    public Integer getReminderMinutesBeforeStart() {
        return reminderMinutesBeforeStart;
    }

    public void setReminderMinutesBeforeStart(Integer reminderMinutesBeforeStart) {
        this.reminderMinutesBeforeStart = reminderMinutesBeforeStart;
    }
    
    public String getReminderMinutesBeforeStartDD() {
        return "TrainingInivation_reminderMinutesBeforeStart";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reminderType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC reminderType;

    public UDC getReminderType() {
        return reminderType;
    }

    public void setReminderType(UDC reminderType) {
        this.reminderType = reminderType;
    }

    public String getReminderTypeDD() {
        return "TrainingInivation_reminderType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getStartDateDD() {
        return "TrainingInivation_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public String getEndDateDD() {
        return "TrainingInivation_endDate";
    }
    // </editor-fold>
}
