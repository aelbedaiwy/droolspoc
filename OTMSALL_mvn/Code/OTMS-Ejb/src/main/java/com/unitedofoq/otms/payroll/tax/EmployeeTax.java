package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class EmployeeTax extends RepPayrollData {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private Integer dsDbid;

    public Integer getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(Integer dsDbid) {
        this.dsDbid = dsDbid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentCalculatedTaxIncomes">
    @Column(precision = 25, scale = 13)
    private BigDecimal currentCalculatedTaxIncomes;

    public BigDecimal getCurrentCalculatedTaxIncomes() {
        return currentCalculatedTaxIncomes;
    }

    public void setCurrentCalculatedTaxIncomes(BigDecimal currentCalculatedTaxIncomes) {
        this.currentCalculatedTaxIncomes = currentCalculatedTaxIncomes;
    }

    public String getCurrentCalculatedTaxIncomesDD() {
        return "EmployeeTax_currentCalculatedTaxIncomes";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currCalculatedTaxIncomesEnc">

    @Column

    private String currCalculatedTaxIncomesEnc;

    public String getCurrCalculatedTaxIncomesEnc() {
        return currCalculatedTaxIncomesEnc;
    }

    public void setCurrCalculatedTaxIncomesEnc(String currCalculatedTaxIncomesEnc) {
        this.currCalculatedTaxIncomesEnc = currCalculatedTaxIncomesEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="otherCalculatedTaxIncomes">
    @Column(precision = 25, scale = 13)
    private BigDecimal otherCalculatedTaxIncomes;

    public BigDecimal getOtherCalculatedTaxIncomes() {
        return otherCalculatedTaxIncomes;
    }

    public void setOtherCalculatedTaxIncomes(BigDecimal otherCalculatedTaxIncomes) {
        this.otherCalculatedTaxIncomes = otherCalculatedTaxIncomes;
    }

    public String getOtherCalculatedTaxIncomesDD() {
        return "EmployeeTax_otherCalculatedTaxIncomes";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="otherCalcuTaxIncomesEnc">
    @Column
    private String otherCalcuTaxIncomesEnc;

    public String getOtherCalcuTaxIncomesEnc() {
        return otherCalcuTaxIncomesEnc;
    }

    public void setOtherCalcuTaxIncomesEnc(String otherCalcuTaxIncomesEnc) {
        this.otherCalcuTaxIncomesEnc = otherCalcuTaxIncomesEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="retroCalculatedTaxIncomes">
    @Column(precision = 25, scale = 13)
    private BigDecimal retroCalculatedTaxIncomes;

    public BigDecimal getRetroCalculatedTaxIncomes() {
        return retroCalculatedTaxIncomes;
    }

    public void setRetroCalculatedTaxIncomes(BigDecimal retroCalculatedTaxIncomes) {
        this.retroCalculatedTaxIncomes = retroCalculatedTaxIncomes;
    }

    public String getRetroCalculatedTaxIncomesDD() {
        return "EmployeeTax_retroCalculatedTaxIncomes";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="retroCalcTaxIncomesEnc">
    @Column
    private String retroCalcTaxIncomesEnc;

    public String getRetroCalcTaxIncomesEnc() {
        return retroCalcTaxIncomesEnc;
    }

    public void setRetroCalcTaxIncomesEnc(String retroCalcTaxIncomesEnc) {
        this.retroCalcTaxIncomesEnc = retroCalcTaxIncomesEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentCalculatedTaxDeductions">
    @Column(precision = 25, scale = 13)
    private BigDecimal currentCalculatedTaxDeductions;

    public BigDecimal getCurrentCalculatedTaxDeductions() {
        return currentCalculatedTaxDeductions;
    }

    public void setCurrentCalculatedTaxDeductions(BigDecimal currentCalculatedTaxDeductions) {
        this.currentCalculatedTaxDeductions = currentCalculatedTaxDeductions;
    }

    public String getCurrentCalculatedTaxDeductionsDD() {
        return "EmployeeTax_currentCalculatedTaxDeductions";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currCalcTaxDeductionsEnc">
    @Column
    private String currCalcTaxDeductionsEnc;

    public String getCurrCalcTaxDeductionsEnc() {
        return currCalcTaxDeductionsEnc;
    }

    public void setCurrCalcTaxDeductionsEnc(String currCalcTaxDeductionsEnc) {
        this.currCalcTaxDeductionsEnc = currCalcTaxDeductionsEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="otherCalculatedTaxDeductions">
    @Column(precision = 25, scale = 13)
    private BigDecimal otherCalculatedTaxDeductions;

    public BigDecimal getOtherCalculatedTaxDeductions() {
        return otherCalculatedTaxDeductions;
    }

    public void setOtherCalculatedTaxDeductions(BigDecimal otherCalculatedTaxDeductions) {
        this.otherCalculatedTaxDeductions = otherCalculatedTaxDeductions;
    }

    public String getOtherCalculatedTaxDeductionsDD() {
        return "EmployeeTax_otherCalculatedTaxDeductions";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="otherCalcTaxDeductionsEnc">
    @Column
    private String otherCalcTaxDeductionsEnc;

    public String getOtherCalcTaxDeductionsEnc() {
        return otherCalcTaxDeductionsEnc;
    }

    public void setOtherCalcTaxDeductionsEnc(String otherCalcTaxDeductionsEnc) {
        this.otherCalcTaxDeductionsEnc = otherCalcTaxDeductionsEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="retroCalculatedTaxDeductions">
    @Column(precision = 25, scale = 13)
    private BigDecimal retroCalculatedTaxDeductions;

    public BigDecimal getRetroCalculatedTaxDeductions() {
        return retroCalculatedTaxDeductions;
    }

    public void setRetroCalculatedTaxDeductions(BigDecimal retroCalculatedTaxDeductions) {
        this.retroCalculatedTaxDeductions = retroCalculatedTaxDeductions;
    }

    public String getRetroCalculatedTaxDeductionsDD() {
        return "EmployeeTax_retroCalculatedTaxDeductions";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="retroCalcTaxDeductionsEnc">
    @Column
    private String retroCalcTaxDeductionsEnc;

    public String getRetroCalcTaxDeductionsEnc() {
        return retroCalcTaxDeductionsEnc;
    }

    public void setRetroCalcTaxDeductionsEnc(String retroCalcTaxDeductionsEnc) {
        this.retroCalcTaxDeductionsEnc = retroCalcTaxDeductionsEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calcTaxExemption">
    @Column(precision = 25, scale = 13)
    private BigDecimal calcTaxExemption;

    public BigDecimal getCalcTaxExemption() {
        return calcTaxExemption;
    }

    public void setCalcTaxExemption(BigDecimal calcTaxExemption) {
        this.calcTaxExemption = calcTaxExemption;
    }

    public String getCalcTaxExemptionDD() {
        return "EmployeeTax_calcTaxExemption";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calcTaxExemptionEnc">
    @Column
    private String calcTaxExemptionEnc;

    public String getCalcTaxExemptionEnc() {
        return calcTaxExemptionEnc;
    }

    public void setCalcTaxExemptionEnc(String calcTaxExemptionEnc) {
        this.calcTaxExemptionEnc = calcTaxExemptionEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedEmployeeExemption">
    @Column(precision = 25, scale = 13)
    private BigDecimal calculatedEmployeeExemption;

    public BigDecimal getCalculatedEmployeeExemption() {
        return calculatedEmployeeExemption;
    }

    public void setCalculatedEmployeeExemption(BigDecimal calculatedEmployeeExemption) {
        this.calculatedEmployeeExemption = calculatedEmployeeExemption;
    }

    public String getCalculatedEmployeeExemptionDD() {
        return "EmployeeTax_calculatedEmployeeExemption";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedEmployeeExemptionEnc">
    @Column
    private String calculatedEmployeeExemptionEnc;

    public String getCalculatedEmployeeExemptionEnc() {
        return calculatedEmployeeExemptionEnc;
    }

    public void setCalculatedEmployeeExemptionEnc(String calculatedEmployeeExemptionEnc) {
        this.calculatedEmployeeExemptionEnc = calculatedEmployeeExemptionEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedMaritalExemption">
    @Column(precision = 25, scale = 13)
    private BigDecimal calculatedMaritalExemption;

    public BigDecimal getCalculatedMaritalExemption() {
        return calculatedMaritalExemption;
    }

    public void setCalculatedMaritalExemption(BigDecimal calculatedMaritalExemption) {
        this.calculatedMaritalExemption = calculatedMaritalExemption;
    }

    public String getCalculatedMaritalExemptionDD() {
        return "EmployeeTax_calculatedMaritalExemption";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedMaritalExemptionEnc">
    @Column
    private String calculatedMaritalExemptionEnc;

    public String getCalculatedMaritalExemptionEnc() {
        return calculatedMaritalExemptionEnc;
    }

    public void setCalculatedMaritalExemptionEnc(String calculatedMaritalExemptionEnc) {
        this.calculatedMaritalExemptionEnc = calculatedMaritalExemptionEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedPercentExemption">
    @Column(precision = 25, scale = 13)
    private BigDecimal calculatedPercentExemption;

    public BigDecimal getCalculatedPercentExemption() {
        return calculatedPercentExemption;
    }

    public void setCalculatedPercentExemption(BigDecimal calculatedPercentExemption) {
        this.calculatedPercentExemption = calculatedPercentExemption;
    }

    public String getCalculatedPercentExemptionDD() {
        return "EmployeeTax_calculatedPercentExemption";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedPercentExemptionEnc">
    @Column
    private String calculatedPercentExemptionEnc;

    public String getCalculatedPercentExemptionEnc() {
        return calculatedPercentExemptionEnc;
    }

    public void setCalculatedPercentExemptionEnc(String calculatedPercentExemptionEnc) {
        this.calculatedPercentExemptionEnc = calculatedPercentExemptionEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedExemption">
    @Column(precision = 25, scale = 13)
    private BigDecimal calculatedExemption;

    public BigDecimal getCalculatedExemption() {
        return calculatedExemption;
    }

    public void setCalculatedExemption(BigDecimal calculatedExemption) {
        this.calculatedExemption = calculatedExemption;
    }

    public String getCalculatedExemptionDD() {
        return "EmployeeTax_calculatedExemption";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedExemptionEnc">
    @Column
    private String calculatedExemptionEnc;

    public String getCalculatedExemptionEnc() {
        return calculatedExemptionEnc;
    }

    public void setCalculatedExemptionEnc(String calculatedExemptionEnc) {
        this.calculatedExemptionEnc = calculatedExemptionEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedTaxBasicValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal calculatedTaxBasicValue;

    public BigDecimal getCalculatedTaxBasicValue() {
        return calculatedTaxBasicValue;
    }

    public void setCalculatedTaxBasicValue(BigDecimal calculatedTaxBasicValue) {
        this.calculatedTaxBasicValue = calculatedTaxBasicValue;
    }

    public String getCalculatedTaxBasicValueDD() {
        return "EmployeeTax_calculatedTaxBasicValue";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedTaxBasicValueEnc">
    @Column
    private String calculatedTaxBasicValueEnc;

    public String getCalculatedTaxBasicValueEnc() {
        return calculatedTaxBasicValueEnc;
    }

    public void setCalculatedTaxBasicValueEnc(String calculatedTaxBasicValueEnc) {
        this.calculatedTaxBasicValueEnc = calculatedTaxBasicValueEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentMonthTaxDeduction">
    @Column
    private String currentMonthTaxDeduction;

    public String getCurrentMonthTaxDeduction() {
        return currentMonthTaxDeduction;
    }

    public void setCurrentMonthTaxDeduction(String currentMonthTaxDeduction) {
        this.currentMonthTaxDeduction = currentMonthTaxDeduction;
    }

    public String getCurrentMonthTaxDeductionDD() {
        return "EmployeeTax_currentMonthTaxDeduction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentMonthPaidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal currentMonthPaidAmount;

    public BigDecimal getCurrentMonthPaidAmount() {
        return currentMonthPaidAmount;
    }

    public void setCurrentMonthPaidAmount(BigDecimal currentMonthPaidAmount) {
        this.currentMonthPaidAmount = currentMonthPaidAmount;
    }

    public String getCurrentMonthPaidAmountDD() {
        return "EmployeeTax_currentMonthPaidAmount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentMonthPaidAmountEnc">
    @Column

    private String currentMonthPaidAmountEnc;

    public String getCurrentMonthPaidAmountEnc() {
        return currentMonthPaidAmountEnc;
    }

    public void setCurrentMonthPaidAmountEnc(String currentMonthPaidAmountEnc) {
        this.currentMonthPaidAmountEnc = currentMonthPaidAmountEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedTaxNetAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal calculatedTaxNetAmount;

    public BigDecimal getCalculatedTaxNetAmount() {
        return calculatedTaxNetAmount;
    }

    public void setCalculatedTaxNetAmount(BigDecimal calculatedTaxNetAmount) {
        this.calculatedTaxNetAmount = calculatedTaxNetAmount;
    }

    public String getCalculatedTaxNetAmountDD() {
        return "EmployeeTax_calculatedTaxNetAmount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedTaxNetAmountEnc">
    @Column
    private String calculatedTaxNetAmountEnc;

    public String getCalculatedTaxNetAmountEnc() {
        return calculatedTaxNetAmountEnc;
    }

    public void setCalculatedTaxNetAmountEnc(String calculatedTaxNetAmountEnc) {
        this.calculatedTaxNetAmountEnc = calculatedTaxNetAmountEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="taxDeduction">
    @Column
    private String taxDeduction;

    public String getTaxDeduction() {
        return taxDeduction;
    }

    public void setTaxDeduction(String taxDeduction) {
        this.taxDeduction = taxDeduction;
    }

    public String getTaxDeductionDD() {
        return "EmployeeTax_taxDeduction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="taxSettlement">
    @Column(length = 1)
    private String taxSettlement;

    public String getTaxSettlement() {
        return taxSettlement;
    }

    public void setTaxSettlement(String taxSettlement) {
        this.taxSettlement = taxSettlement;
    }

    public String getTaxSettlementDD() {
        return "EmployeeTax_taxSettlement";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxIncomes">
    @Column(precision = 25, scale = 13)
    private BigDecimal yearTaxIncomes;

    public BigDecimal getYearTaxIncomes() {
        return yearTaxIncomes;
    }

    public void setYearTaxIncomes(BigDecimal yearTaxIncomes) {
        this.yearTaxIncomes = yearTaxIncomes;
    }

    public String getYearTaxIncomesDD() {
        return "EmployeeTax_yearTaxIncomes";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxIncomesEnc">
    @Column
    private String yearTaxIncomesEnc;

    public String getYearTaxIncomesEnc() {
        return yearTaxIncomesEnc;
    }

    public void setYearTaxIncomesEnc(String yearTaxIncomesEnc) {
        this.yearTaxIncomesEnc = yearTaxIncomesEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxDeductions">
    @Column(precision = 25, scale = 13)
    private BigDecimal yearTaxDeductions;

    public BigDecimal getYearTaxDeductions() {
        return yearTaxDeductions;
    }

    public void setYearTaxDeductions(BigDecimal yearTaxDeductions) {
        this.yearTaxDeductions = yearTaxDeductions;
    }

    public String getYearTaxDeductionsDD() {
        return "EmployeeTax_yearTaxDeductions";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxDeductionsEnc">
    @Column
    private String yearTaxDeductionsEnc;

    public String getYearTaxDeductionsEnc() {
        return yearTaxDeductionsEnc;
    }

    public void setYearTaxDeductionsEnc(String yearTaxDeductionsEnc) {
        this.yearTaxDeductionsEnc = yearTaxDeductionsEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxExemption">
    @Column(precision = 25, scale = 13)
    private BigDecimal yearTaxExemption;

    public BigDecimal getYearTaxExemption() {
        return yearTaxExemption;
    }

    public void setYearTaxExemption(BigDecimal yearTaxExemption) {
        this.yearTaxExemption = yearTaxExemption;
    }

    public String getYearTaxExemptionDD() {
        return "EmployeeTax_yearTaxExemption";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxExemptionEnc">
    @Column
    private String yearTaxExemptionEnc;

    public String getYearTaxExemptionEnc() {
        return yearTaxExemptionEnc;
    }

    public void setYearTaxExemptionEnc(String yearTaxExemptionEnc) {
        this.yearTaxExemptionEnc = yearTaxExemptionEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxBasicValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal yearTaxBasicValue;

    public BigDecimal getYearTaxBasicValue() {
        return yearTaxBasicValue;
    }

    public void setYearTaxBasicValue(BigDecimal yearTaxBasicValue) {
        this.yearTaxBasicValue = yearTaxBasicValue;
    }

    public String getYearTaxBasicValueDD() {
        return "EmployeeTax_yearTaxBasicValue";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxBasicValueEnc">
    private String yearTaxBasicValueEnc;

    public String getYearTaxBasicValueEnc() {
        return yearTaxBasicValueEnc;
    }

    public void setYearTaxBasicValueEnc(String yearTaxBasicValueEnc) {
        this.yearTaxBasicValueEnc = yearTaxBasicValueEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxPaidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal yearTaxPaidAmount;

    public BigDecimal getYearTaxPaidAmount() {
        return yearTaxPaidAmount;
    }

    public void setYearTaxPaidAmount(BigDecimal yearTaxPaidAmount) {
        this.yearTaxPaidAmount = yearTaxPaidAmount;
    }

    public String getYearTaxPaidAmountDD() {
        return "EmployeeTax_yearTaxPaidAmount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxPaidAmountEnc">
    @Column

    private String yearTaxPaidAmountEnc;

    public String getYearTaxPaidAmountEnc() {
        return yearTaxPaidAmountEnc;
    }

    public void setYearTaxPaidAmountEnc(String yearTaxPaidAmountEnc) {
        this.yearTaxPaidAmountEnc = yearTaxPaidAmountEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxNetAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal yearTaxNetAmount;

    public BigDecimal getYearTaxNetAmount() {
        return yearTaxNetAmount;
    }

    public void setYearTaxNetAmount(BigDecimal yearTaxNetAmount) {
        this.yearTaxNetAmount = yearTaxNetAmount;
    }

    public String getYearTaxNetAmountDD() {
        return "EmployeeTax_yearTaxNetAmount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="yearTaxNetAmountEnc">
    @Column
    private String yearTaxNetAmountEnc;

    public String getYearTaxNetAmountEnc() {
        return yearTaxNetAmountEnc;
    }

    public void setYearTaxNetAmountEnc(String yearTaxNetAmountEnc) {
        this.yearTaxNetAmountEnc = yearTaxNetAmountEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="settledIncorded">
    @Column
    private String settledIncorded;

    public String getSettledIncorded() {
        return settledIncorded;
    }

    public void setSettledIncorded(String settledIncorded) {
        this.settledIncorded = settledIncorded;
    }

    public String getSettledIncordedDD() {
        return "EmployeeTax_settledIncorded";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="settledSalaryElementID">
    @Column
    private String settledSalaryElementID;

    public String getSettledSalaryElementID() {
        return settledSalaryElementID;
    }

    public void setSettledSalaryElementID(String settledSalaryElementID) {
        this.settledSalaryElementID = settledSalaryElementID;
    }

    public String getSettledSalaryElementIDDD() {
        return "EmployeeTax_settledSalaryElementID";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EmployeeTax_code";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fullname">
    @Column
    @Translatable(translationField = "fullnameTranslated")
    private String fullname;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameDD() {
        return "EmployeeTax_fullname";
    }
    @Transient
    @Translation(originalField = "fullname")
    private String fullnameTranslated;

    public String getFullnameTranslated() {
        return fullnameTranslated;
    }

    public void setFullnameTranslated(String fullnameTranslated) {
        this.fullnameTranslated = fullnameTranslated;
    }

    public String getFullnameTranslatedDD() {
        return "EmployeeTax_fullname";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    @Translatable(translationField = "positionNameTranslated")
    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionNameDD() {
        return "EmployeeTax_positionName";
    }
    @Transient
    @Translation(originalField = "positionName")
    private String positionNameTranslated;

    public String getPositionNameTranslated() {
        return positionNameTranslated;
    }

    public void setPositionNameTranslated(String positionNameTranslated) {
        this.positionNameTranslated = positionNameTranslated;
    }

    public String getPositionNameTranslatedDD() {
        return positionNameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    @Translatable(translationField = "locationDescriptionTranslated")
    private String locationDescription;

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescriptionDD() {
        return "EmployeeTax_locationDescription";
    }
    @Transient
    @Translation(originalField = "locationDescription")
    private String locationDescriptionTranslated;

    public String getLocationDescriptionTranslated() {
        return locationDescriptionTranslated;
    }

    public void setLocationDescriptionTranslated(String locationDescriptionTranslated) {
        this.locationDescriptionTranslated = locationDescriptionTranslated;
    }

    public String getLocationDescriptionTranslatedDD() {
        return "EmployeeTax_locationDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    @Translatable(translationField = "costCenterDescriptionTranslated")
    private String costCenterDescription;

    public String getCostCenterDescription() {
        return costCenterDescription;
    }

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescriptionDD() {
        return "EmployeeTax_costCenterDescription";
    }
    @Transient
    @Translation(originalField = "costCenterDescription")
    private String costCenterDescriptionTranslated;

    public String getCostCenterDescriptionTranslated() {
        return costCenterDescriptionTranslated;
    }

    public void setCostCenterDescriptionTranslated(String costCenterDescriptionTranslated) {
        this.costCenterDescriptionTranslated = costCenterDescriptionTranslated;
    }

    public String getCostCenterDescriptionTranslatedDD() {
        return "EmployeeTax_costCenterDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    @Translatable(translationField = "payGradeDescriptionTranslated")
    private String payGradeDescription;

    public String getPayGradeDescription() {
        return payGradeDescription;
    }

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescriptionDD() {
        return "EmployeeTax_payGradeDescription";
    }
    @Transient
    @Translation(originalField = "payGradeDescription")
    private String payGradeDescriptionTranslated;

    public String getPayGradeDescriptionTranslated() {
        return payGradeDescriptionTranslated;
    }

    public void setPayGradeDescriptionTranslated(String payGradeDescriptionTranslated) {
        this.payGradeDescriptionTranslated = payGradeDescriptionTranslated;
    }

    public String getPayGradeDescriptionTranslatedDD() {
        return "EmployeeTax_payGradeDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringTypeDescription">
    @Column
    @Translatable(translationField = "hiringTypeDescriptionTranslated")
    private String hiringTypeDescription;

    public String getHiringTypeDescription() {
        return hiringTypeDescription;
    }

    public void setHiringTypeDescription(String hiringTypeDescription) {
        this.hiringTypeDescription = hiringTypeDescription;
    }

    public String getHiringTypeDescriptionDD() {
        return "EmployeeTax_hiringTypeDescription";
    }
    @Transient
    @Translation(originalField = "hiringTypeDescription")
    private String hiringTypeDescriptionTranslated;

    public String getHiringTypeDescriptionTranslated() {
        return hiringTypeDescriptionTranslated;
    }

    public void setHiringTypeDescriptionTranslated(String hiringTypeDescriptionTranslated) {
        this.hiringTypeDescriptionTranslated = hiringTypeDescriptionTranslated;
    }

    public String getHiringTypeDescriptionTranslatedDD() {
        return "EmployeeTax_hiringTypeDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    @Translatable(translationField = "unitNameTranslated")
    private String unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitNameDD() {
        return "EmployeeTax_unitName";
    }
    @Transient
    @Translation(originalField = "unitName")
    private String unitNameTranslated;

    public String getUnitNameTranslated() {
        return unitNameTranslated;
    }

    public void setUnitNameTranslated(String unitNameTranslated) {
        this.unitNameTranslated = unitNameTranslated;
    }

    public String getUnitNameTranslatedDD() {
        return "EmployeeTax_unitName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    @Translatable(translationField = "jobNameTranslated")
    private String jobName;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobNameDD() {
        return "EmployeeTax_jobName";
    }
    @Transient
    @Translation(originalField = "jobName")
    private String jobNameTranslated;

    public String getJobNameTranslated() {
        return jobNameTranslated;
    }

    public void setJobNameTranslated(String jobNameTranslated) {
        this.jobNameTranslated = jobNameTranslated;
    }

    public String getJobNameTranslatedDD() {
        return "EmployeeTax_jobName";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="branchDescription">
    @Column
    private String branchDescription;

    public String getBranchDescription() {
        return branchDescription;
    }

    public void setBranchDescription(String branchDescription) {
        this.branchDescription = branchDescription;
    }

    public String getBranchDescriptionDD() {
        return "EmployeeTax_branchDescription";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeeTax_notes";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empCodeName">
    @Column
    private String empCodeName;

    public String getEmpCodeName() {
        return empCodeName;
    }

    public void setEmpCodeName(String empCodeName) {
        this.empCodeName = empCodeName;
    }

    public String getEmpCodeNameDD() {
        return "EmployeeTax_empCodeName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empNameCode">
    @Column
    private String empNameCode;

    public String getEmpNameCode() {
        return empNameCode;
    }

    public void setEmpNameCode(String empNameCode) {
        this.empNameCode = empNameCode;
    }

    public String getEmpNameCodeDD() {
        return "EmployeeTax_empNameCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="variableInsurance">
    @Column(precision = 25, scale = 13)
    private BigDecimal variableInsurance;

    public BigDecimal getVariableInsurance() {
        return variableInsurance;
    }

    public void setVariableInsurance(BigDecimal variableInsurance) {
        this.variableInsurance = variableInsurance;
    }

    public String getVariableInsuranceDD() {
        return "EmployeeTax_variableInsurance";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="fixedInsurance">
    @Column(precision = 25, scale = 13)
    private BigDecimal fixedInsurance;

    public BigDecimal getFixedInsurance() {
        return fixedInsurance;
    }

    public void setFixedInsurance(BigDecimal fixedInsurance) {
        this.fixedInsurance = fixedInsurance;
    }

    public String getFixedInsuranceDD() {
        return "EmployeeTax_fixedInsurance";
    }
    //</editor-fold>
}
