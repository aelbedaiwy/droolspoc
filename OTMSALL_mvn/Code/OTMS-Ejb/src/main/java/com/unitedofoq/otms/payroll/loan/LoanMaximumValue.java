
package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"loan"})
public class LoanMaximumValue extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="fromYears">
    @Column
    private double fromYears;

    public double getFromYears() {
        return fromYears;
    }

    public void setFromYears(double fromYears) {
        this.fromYears = fromYears;
    }
    
    public String getFromYearsDD() {
        return "LoanMaximumValue_fromYears";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="toYears">
    @Column
    private double toYears;

    public void setToYears(double toYears) {
        this.toYears = toYears;
    }

    public double getToYears() {
        return toYears;
    }

    public String getToYearsDD() {
        return "LoanMaximumValue_toYears";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="maximumAsAmount">
    private BigDecimal maximumAsAmount;

    public BigDecimal getMaximumAsAmount() {
        return maximumAsAmount;
    }

    public void setMaximumAsAmount(BigDecimal maximumAsAmount) {
        this.maximumAsAmount = maximumAsAmount;
    }
    
    public String getMaximumAsAmountDD() {
        return "LoanMaximumValue_maximumAsAmount";
    }
    
    @Transient
    private BigDecimal maximumAsAmountMask;

    public BigDecimal getMaximumAsAmountMask() {
        maximumAsAmountMask = maximumAsAmount;
        return maximumAsAmountMask;
    }

    public void setMaximumAsAmountMask(BigDecimal maximumAsAmountMask) {
        updateDecimalValue("maximumAsAmount",maximumAsAmountMask);
    }
    
    public String getMaximumAsAmountMaskDD() {
        return "LoanMaximumValue_maximumAsAmountMask";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="savingBoxPercentage">
    @Column
    private BigDecimal savingBoxPercentage;

    public void setSavingBoxPercentage(BigDecimal savingBoxPercentage) {
        this.savingBoxPercentage = savingBoxPercentage;
    }

    public BigDecimal getSavingBoxPercentage() {
        return savingBoxPercentage;
    }

    public String getSavingBoxPercentageDD() {
        return "LoanMaximumValue_savingBoxPercentage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="basicSalaryPercentage">
    @Column
    private BigDecimal basicSalaryPercentage;

    public void setBasicSalaryPercentage(BigDecimal basicSalaryPercentage) {
        this.basicSalaryPercentage = basicSalaryPercentage;
    }

    public BigDecimal getBasicSalaryPercentage() {
        return basicSalaryPercentage;
    }

    public String getBasicSalaryPercentageDD() {
        return "LoanMaximumValue_basicSalaryPercentage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="loan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Loan loan;

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
    
    public String getLoanDD() {
        return "LoanMaximumValue_loan";
    }
    // </editor-fold>
    

}