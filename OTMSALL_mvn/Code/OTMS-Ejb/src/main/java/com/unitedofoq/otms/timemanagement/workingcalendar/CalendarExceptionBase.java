/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author lap2
 */
@MappedSuperclass
public class CalendarExceptionBase extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="name">

    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "CalendarExceptionBase_name";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="timeIn">
    //@Column
    //@Temporal(TemporalType.TIMESTAMP)
    private String timeIn;

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeInDD() {
        return "CalendarExceptionBase_timeIn";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="timeOut">
//    @Column
//    @Temporal(TemporalType.TIMESTAMP)
    private String timeOut;

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getTimeOutDD() {
        return "CalendarExceptionBase_timeOut";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="breakFrom">
//    @Column
//    @Temporal(TemporalType.TIMESTAMP)
    private String breakFrom;

    public String getBreakFrom() {
        return breakFrom;
    }

    public void setBreakFrom(String breakFrom) {
        this.breakFrom = breakFrom;
    }

    public String getBreakFromDD() {
        return "CalendarExceptionBase_breakFrom";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="breakTo">
    //@Temporal(TemporalType.TIMESTAMP)
    private String breakTo;

    public String getBreakTo() {
        return breakTo;
    }

    public void setBreakTo(String breakTo) {
        this.breakTo = breakTo;
    }

    public String getBreakToDD() {
        return "CalendarExceptionBase_breakTo";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="activeFrom">
    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date activeFrom;

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public String getActiveFromDD() {
        return "CalendarExceptionBase_activeFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="activeTo">
    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date activeTo;

    public void setActiveTo(Date activeTo) {
        this.activeTo = activeTo;
    }

    public Date getActiveTo() {
        return activeTo;
    }

    public String getActiveToDD() {
        return "CalendarExceptionBase_activeTo";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="numberOfHours">
    private BigDecimal numberOfHours;

    public BigDecimal getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(BigDecimal numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public String getNumberOfHoursDD() {
        return "CalendarExceptionBase_numberOfHours";
    }

    @Transient
    private BigDecimal numberOfHoursMask;

    public BigDecimal getNumberOfHoursMask() {
        numberOfHoursMask = numberOfHours;
        return numberOfHoursMask;
    }

    public void setNumberOfHoursMask(BigDecimal numberOfHoursMask) {
        updateDecimalValue("numberOfHours", numberOfHoursMask);
    }

    public String getNumberOfHoursMaskDD() {
        return "CalendarExceptionBase_numberOfHoursMask";
    }
    //</editor-fold>
}
