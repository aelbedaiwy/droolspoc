
package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepCostCenterTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    @Column
    private String costCenterGroupDescription;

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="legalEntityDescription">
    @Column
    private String legalEntityDescription;

    public void setLegalEntityDescription(String legalEntityDescription) {
        this.legalEntityDescription = legalEntityDescription;
    }

    public String getLegalEntityDescription() {
        return legalEntityDescription;
    }
    // </editor-fold>

}
