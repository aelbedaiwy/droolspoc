package com.unitedofoq.otms.payroll.custody;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@ParentEntity(fields = {"type"})
public class CustodyDetail extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "CustodyDetail_name";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "CustodyDetail_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "CustodyDetail_description";
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comment">
    @Column(name = "comments")
    private String comment;

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public String getCommentDD() {
        return "CustodyDetail_comment";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private CustodyType type;

    public void setType(CustodyType type) {
        this.type = type;
    }

    public CustodyType getType() {
        return type;
    }

    public String getTypeDD() {
        return "CustodyDetail_type";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currStock">
    private int currStock;

    public int getCurrStock() {
        return currStock;
    }

    public void setCurrStock(int currStock) {
        this.currStock = currStock;
    }

    public String getCurrStockDD() {
        return "CustodyDetail_currStock";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="orgStock">
    private int orgStock;

    public int getOrgStock() {
        return orgStock;
    }

    public void setOrgStock(int orgStock) {
        this.orgStock = orgStock;
    }

    public String getOrgStockDD() {
        return "CustodyDetail_orgStock";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="itemPrice">
    @Column(precision = 25, scale = 13)
    private BigDecimal itemPrice;

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemPriceDD() {
        return "CustodyDetail_itemPrice";
    }
    @Transient
    private BigDecimal itemPriceMask;

    public BigDecimal getItemPriceMask() {
        itemPriceMask = itemPrice;
        return itemPriceMask;
    }

    public void setItemPriceMask(BigDecimal itemPriceMask) {
        updateDecimalValue("itemPrice", itemPriceMask);
    }

    public String getItemPriceMaskDD() {
        return "EmployeeCustody_itemPriceMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="deduction">
    @ManyToOne
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }

    public String getDeductionDD() {
        return "CustodyDetail_deduction";
    }
    //</editor-fold>
}
