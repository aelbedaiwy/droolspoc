package com.unitedofoq.otms.core;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.security.user.ORole;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class RoleDataSecurity extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="role">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORole role;

    public ORole getRole() {
        return role;
    }

    public String getRoleDD() {
        return "RoleDataSecurity_role";
    }

    public void setRole(ORole role) {
        this.role = role;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="oentity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity oentity;

    public OEntity getOentity() {
        return oentity;
    }

    public String getOentityDD() {
        return "RoleDataSecurity_oentity";
    }

    public void setOentity(OEntity oentity) {
        this.oentity = oentity;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "RoleDataSecurity_sortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startCondition">
    private String startCondition;

    public String getStartCondition() {
        return startCondition;
    }

    public String getStartConditionDD() {
        return "RoleDataSecurity_startCondition";
    }

    public void setStartCondition(String startCondition) {
        this.startCondition = startCondition;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    private String fieldExpression;

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }

    public String getFieldExpressionDD() {
        return "RoleDataSecurity_fieldExpression";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="conditionalOperator">
    private String conditionalOperator;

    public String getConditionalOperator() {
        return conditionalOperator;
    }

    public String getConditionalOperatorDD() {
        return "RoleDataSecurity_conditionalOperator";
    }

    public void setConditionalOperator(String conditionalOperator) {
        this.conditionalOperator = conditionalOperator;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="value">
    private String value;

    public String getValue() {
        return value;
    }

    public String getValueDD() {
        return "RoleDataSecurity_value";
    }

    public void setValue(String value) {
        this.value = value;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="logicalOperator">
    private String logicalOperator;

    public String getLogicalOperator() {
        return logicalOperator;
    }

    public String getLogicalOperatorDD() {
        return "RoleDataSecurity_logicalOperator";
    }

    public void setLogicalOperator(String logicalOperator) {
        this.logicalOperator = logicalOperator;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endCondition">
    private String endCondition;

    public String getEndCondition() {
        return endCondition;
    }

    public String getEndConditionDD() {
        return "RoleDataSecurity_endCondition";
    }

    public void setEndCondition(String endCondition) {
        this.endCondition = endCondition;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="includeRelatedEntities">
    private boolean includeRelatedEntities;

    public boolean isIncludeRelatedEntities() {
        return includeRelatedEntities;
    }

    public String isIncludeRelatedEntitiesDD() {
        return "RoleDataSecurity_includeRelatedEntities";
    }

    public void setIncludeRelatedEntities(boolean includeRelatedEntities) {
        this.includeRelatedEntities = includeRelatedEntities;
    }
    // </editor-fold>
}
