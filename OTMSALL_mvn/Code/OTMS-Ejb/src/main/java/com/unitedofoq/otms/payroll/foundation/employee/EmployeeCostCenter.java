/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeCostCenter extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;
    public Employee getEmployee() {
        return employee;
    }
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeCostCenter_employee";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }
    
    public String getCostCenterDD() {
        return "EmployeeCostCenter_costCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="distPercent">
    @Column(precision=25, scale=13)
    private BigDecimal distPercent;

    public BigDecimal getDistPercent() {
        return distPercent;
    }

    public void setDistPercent(BigDecimal distPercent) {
        this.distPercent = distPercent;
    }
    
    public String getDistPercentDD() {
        return "EmployeeCostCenter_distPercent";
    }
    
    @Transient
    private BigDecimal distPercentMask;

    public BigDecimal getDistPercentMask() {
        distPercentMask = distPercent;
        return distPercentMask;
    }

    public void setDistPercentMask(BigDecimal distPercentMask) {
        updateDecimalValue("distPercent", distPercentMask);
    }
    
    public String getDistPercentMaskDD() {
        return "EmployeeCostCenter_distPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="distDays">
    @Column(precision=25, scale=13)
    private BigDecimal distDays;

    public BigDecimal getDistDays() {
        return distDays;
    }

    public void setDistDays(BigDecimal distDays) {
        this.distDays = distDays;
    }
    
    public String getDistDaysDD() {
        return "EmployeeCostCenter_distDays";
    }
    
    @Transient
    private BigDecimal distDaysMask;

    public BigDecimal getDistDaysMask() {
        distDaysMask = distDays;
        return distDaysMask;
    }

    public void setDistDaysMask(BigDecimal distDaysMask) {
        updateDecimalValue("distDays", distDaysMask);
    }
    
    public String getDistDaysMaskDD() {
        return "EmployeeCostCenter_distDaysMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="distHours">
    @Column(precision=25, scale=13)
    private BigDecimal distHours;

    public BigDecimal getDistHours() {
        return distHours;
    }

    public void setDistHours(BigDecimal distHours) {
        this.distHours = distHours;
    }
    
    public String getDistHoursDD() {
        return "EmployeeCostCenter_distHours";
    }
    
    @Transient
    private BigDecimal distHoursMask;

    public BigDecimal getDistHoursMask() {
        distHoursMask = distHours;
        return distHoursMask;
    }

    public void setDistHoursMask(BigDecimal distHoursMask) {
        updateDecimalValue("distHours", distHoursMask);
    }
    
    public String getDistHoursMaskDD() {
        return "EmployeeCostCenter_distHoursMask";
    }
    // </editor-fold>
}