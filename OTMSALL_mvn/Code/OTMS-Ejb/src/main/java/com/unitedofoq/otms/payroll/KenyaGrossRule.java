package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class KenyaGrossRule extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch= FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "KenyaGrossRule_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromValue">
    @Column(precision=25, scale=13)
    private BigDecimal fromValue;

    public BigDecimal getFromValue() {
        return fromValue;
    }

    public void setFromValue(BigDecimal fromValue) {
        this.fromValue = fromValue;
    }

    public String getFromValueDD() {
        return "KenyaGrossRule_fromValue";
    }

    @Transient
    private BigDecimal fromValueMask;

    public BigDecimal getFromValueMask() {
        fromValueMask = fromValue;
        return fromValueMask;
    }

    public String getFromValueMaskDD() {
        return "KenyaGrossRule_fromValueMask";
    }

    public void setFromValueMask(BigDecimal fromValueMask) {
        updateDecimalValue("fromValue", fromValueMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toValue">
    @Column(precision=25, scale=13)
    private BigDecimal toValue;

    public BigDecimal getToValue() {
        return toValue;
    }

    public void setToValue(BigDecimal toValue) {
        this.toValue = toValue;
    }

    public String getToValueDD() {
        return "KenyaGrossRule_toValue";
    }

    @Transient
    private BigDecimal toValueMask;

    public BigDecimal getToValueMask() {
        toValueMask = toValue;
        return toValueMask;
    }

    public String getToValueMaskDD() {
        return "KenyaGrossRule_toValueMask";
    }

    public void setToValueMask(BigDecimal toValueMask) {
        updateDecimalValue("toValue", toValueMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="value">
    @Column(precision=25, scale=13)
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getValueDD() {
        return "KenyaGrossRule_value";
    }

    @Transient
    private BigDecimal valueMask;

    public BigDecimal getValueMask() {
        valueMask = value;
        return valueMask;
    }

    public String getValueMaskDD() {
        return "KenyaGrossRule_valueMask";
    }

    public void setValueMask(BigDecimal valueMask) {
        updateDecimalValue("value", valueMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch= FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "KenyaGrossRule_salaryElement";
    }
    // </editor-fold>
}