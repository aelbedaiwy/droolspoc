/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.company;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"company"})
public class CompanyCostCenter extends BaseEntity
{
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "CompanyCostCenter_company";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }
    
    public String getCostCenterDD() {
        return "CompanyCostCenter_costCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="distPercent">
    @Column(precision=25, scale=13)
    private BigDecimal distPercent;

    public BigDecimal getDistPercent() {
        return distPercent;
    }

    public void setDistPercent(BigDecimal distPercent) {
        this.distPercent = distPercent;
    }
    
    public String getDistPercentDD() {
        return "CompanyCostCenter_distPercent";
    }
    
    @Transient
    private BigDecimal distPercentMask;

    public BigDecimal getDistPercentMask() {
        distPercentMask = distPercent;
        return distPercentMask;
    }

    public void setDistPercentMask(BigDecimal distPercentMask) {
        updateDecimalValue("distPercent", distPercentMask);
    }
    
    public String getDistPercentMaskDD() {
        return "CompanyCostCenter_distPercentMask";
    }
    // </editor-fold>
}