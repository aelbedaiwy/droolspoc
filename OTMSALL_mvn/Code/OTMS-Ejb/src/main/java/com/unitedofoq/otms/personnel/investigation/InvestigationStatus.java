/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.investigation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
@ParentEntity(fields={"company"})
public class InvestigationStatus extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD() {
        return "InvestigationStatus_code";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "InvestigationStatus_description";
    }

    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
     // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="salaryEffect">
    @Column(precision=25, scale=3)
    private BigDecimal salaryEffect;

    public BigDecimal getSalaryEffect() {
        return salaryEffect;
    }
    public BigDecimal getSalaryEffectDD() {
        return salaryEffect;
    }

    public void setSalaryEffect(BigDecimal salaryEffect) {
        this.salaryEffect = salaryEffect;
    }
    
    @Transient
    private BigDecimal salaryEffectMask;

    public BigDecimal getSalaryEffectMask() {
        salaryEffectMask = salaryEffect;
        return salaryEffectMask;
    }
    public String getSalaryEffectMaskDD() {
        return "InvestigationStatus_salaryEffectMask";
    }

    public void setSalaryEffectMask(BigDecimal salaryEffectMask) {
        updateDecimalValue("salaryEffect", salaryEffectMask);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="penalty">
    @ManyToOne(fetch= FetchType.LAZY)
    private Penalty penalty;

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }
    
    public String getPenaltyDD() {
        return "InvestigationStatus_penalty";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "InvestigationStatus_company";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="income">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;
    public Income getIncome() {
        return income;
    }
    public void setIncome(Income income) {
        this.income = income;
    }
    public String getIncomeDD() {
        return "InvestigationStatus_income";
    }
    // </editor-fold>
}