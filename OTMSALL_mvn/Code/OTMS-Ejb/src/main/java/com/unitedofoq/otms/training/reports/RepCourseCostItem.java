package com.unitedofoq.otms.training.reports;


import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.recruitment.reports.RepReqCostItemBase;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepCourseCostItem extends RepReqCostItemBase {

    //<editor-fold defaultstate="collapsed" desc="course">
    @Translatable(translationField = "courseTranslated")
    private String course;

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourseDD() {
        return "RepCourseCostItem_course";
    }
    @Transient
    @Translation(originalField = "course")
    private String courseTranslated;

    public String getCourseTranslated() {
        return courseTranslated;
    }

    public void setCourseTranslated(String courseTranslated) {
        this.courseTranslated = courseTranslated;
    }

    public String getCourseTranslatedDD() {
        return "RepCourseCostItem_course";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="provider">
    @Translatable(translationField = "providerTranslated")
    private String provider;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "RepCourseCostItem_provider";
    }
    @Transient
    @Translation(originalField = "provider")
    private String providerTranslated;

    public String getProviderTranslated() {
        return providerTranslated;
    }

    public void setProviderTranslated(String providerTranslated) {
        this.providerTranslated = providerTranslated;
    }

    public String getProviderTranslatedDD() {
        return "RepCourseCostItem_provider";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="courseClass">
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public String getCourseClass() {
        return courseClass;
    }

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClassDD() {
        return "RepCourseCostItem_courseClass";
    }
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;

    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }

    public String getCourseClassTranslatedDD() {
        return "RepCourseCostItem_courseClass";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="numAttendee">
    @Column
    private int numAttendee;

    public void setNumAttendee(int numAttendee) {
        this.numAttendee = numAttendee;
    }

    public int getNumAttendee() {
        return numAttendee;
    }

    public String getNumAttendeeDD() {
        return "RepCourseCostItem_numAttendee";
    }
    // </editor-fold>
}
