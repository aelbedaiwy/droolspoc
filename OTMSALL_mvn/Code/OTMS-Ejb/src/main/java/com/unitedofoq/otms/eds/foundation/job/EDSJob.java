/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.job;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.employee.EDSEmployee;
import com.unitedofoq.otms.eds.foundation.unit.EDSUnit;
import com.unitedofoq.otms.eds.paygrade.EDSPayGrade;
import com.unitedofoq.otms.foundation.position.PositionBase;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields={"unit"})
@DiscriminatorValue("MASTER")
@ChildEntity(fields={"behaviors", "values", "jobCompetencies"})
@FABSEntitySpecs(hideInheritedFields={"payGradeStep","effectiveFrom","effectiveTo","statusDate","occupation","creatationDate"})
@Table(name="Position")
public class EDSJob extends PositionBase{

    // <editor-fold defaultstate="collapsed" desc="behaviors">

    @OneToMany(mappedBy="job")
    private List<EDSJobBehavior> behaviors;
    public List<EDSJobBehavior> getBehaviors() {
        return behaviors;
    }

    public void setBehaviors(List<EDSJobBehavior> behaviors) {
        this.behaviors = behaviors;
    }
     public String getBehaviorsDD() {
        return "EDSJob_behaviors";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="values">
    @OneToMany(mappedBy="job")
    private List<EDSJobValue> values;
    public List<EDSJobValue> getValues() {
        return values;
    }

    public void setValues(List<EDSJobValue> values) {
        this.values = values;
    }

     public String getValuesDD() {
        return "EDSJob_values";
    }
    // </editor-fold>
     
    // <editor-fold defaultstate="collapsed" desc="jobCompetencies">
    @OneToMany(mappedBy="job")
     private List<EDSJobCompetency> jobCompetencies;
     public List<EDSJobCompetency> getJobCompetencies() {
        return jobCompetencies;
    }

    public void setJobCompetencies(List<EDSJobCompetency> jobCompetencies) {
        this.jobCompetencies = jobCompetencies;
    }
    public String getJobCompetenciesDD() {
        return "EDSJob_jobCompetencies";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employees">
    @OneToMany(mappedBy="job")
    private List<EDSEmployee> employees;
     public List<EDSEmployee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EDSEmployee> employees) {
        this.employees = employees;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="parentPosition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="parentPosition_dbid")
	private EDSJob parentJob;

    public EDSJob getParentJob() {
        return parentJob;
    }

    public String getParentJobDD() {
        return "EDSJob_parentJob";
    }

    public void setParentJob(EDSJob parentJob) {
        this.parentJob = parentJob;
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
	private EDSUnit unit;
    public String getUnitDD() {
        return "Position_unit";
    }
     public EDSUnit getUnit() {
        return unit;
    }

    public void setUnit(EDSUnit unit) {
        this.unit = unit;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="job_dbid")
	private EDSJobFamily jobFamily;

    public EDSJobFamily getJobFamily() {
        return jobFamily;
    }

    public void setJobFamily(EDSJobFamily jobFamily) {
        this.jobFamily = jobFamily;
    }

    public String getJobFamilyDD() {
        return "Position_job";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSPayGrade payGrade;

    public EDSPayGrade getPayGrade() {
        return payGrade;
    }

    public void setPayGrade(EDSPayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public String getPayGradeDD() {
        return "Position_payGrade";
    }
   // </editor-fold>
}