/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields="university")
@ChildEntity(fields="facultyDepartments")
public class Faculty extends BaseEntity {
     // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "Faculty_code";
    }
// </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
     public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNameTranslatedDD() {
         return "Faculty_nameTranslated";
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="university">
  
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private University university;

    public University getUniversity() {
        return university;
    }
    public String getUniversityDD() {
        return "Faculty_university";
    }
    public void setUniversity(University university) {
        this.university = university;
    }
 // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="facultyDepartments">
  @OneToMany(mappedBy = "faculty")
    private List<FacultyDepartment> facultyDepartments;

    public List<FacultyDepartment> getFacultyDepartments() {
        return facultyDepartments;
    }

    public void setFacultyDepartments(List<FacultyDepartment> facultyDepartments) {
        this.facultyDepartments = facultyDepartments;
    }

       public String  getFacultyDepartmentsDD() {

        return "Faculty_facultyDepartments";
    }


// </editor-fold>

}
