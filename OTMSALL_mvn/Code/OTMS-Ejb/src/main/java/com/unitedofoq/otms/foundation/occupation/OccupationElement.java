package com.unitedofoq.otms.foundation.occupation;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class OccupationElement extends BusinessObjectBaseEntity {
    @Column(nullable= false, unique = true)
    protected String id;
    protected boolean onet;

    public boolean isOnet() {
        return onet;
    }

    public void setOnet(boolean onet) {
        this.onet = onet;
    }

      
    public String getOnetDD(){
        return "OccupationElement_oNet";
    }

    @Translatable(translationField="nameTranslated")
    protected String name;
    public String getNameTranslatedDD(){
        return "OccupationElement_name";
    }
    
    public String getIdDD(){
        return "OccupationElement_id";
    }

    @Translatable(translationField="descriptionTranslated")
    protected String description;
    public String getDescriptionTranslatedDD(){
        return "OccupationElement_description";
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String elementName) {
        this.name = elementName;
    }

    
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    
}
