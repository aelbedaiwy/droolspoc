/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 *
 * @author mostafa
 */
@Entity
public class TMOpenedCalcPeriod extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch= FetchType.LAZY)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }
    
    public String getCalculatedPeriodDD() {
        return "TMOpenedCalcPeriod_calculatedPeriod";
    }
    // </editor-fold>
}