
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Transient;


@MappedSuperclass
public class RepEmployeeQuestionareBase extends RepEmployeeBase {
        // <editor-fold defaultstate="collapsed" desc="dsDbid">

    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeQuestionaireBase_dsDbid";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="year">
    @Column
    private String year;

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public String getYearDD() {
        return "RepEmployeeQuestionaireBase_year";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "RepEmployeeQuestionaireBase_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="finishDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finishDate;

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public String getFinishDateDD() {
        return "RepEmployeeQuestionaireBase_finishDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="duration">
    @Column
    private String duration;

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public String getDurationDD() {
        return "RepEmployeeQuestionaireBase_duration";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="purpose">
    @Column
    private String purpose;

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getPurposeDD() {
        return "RepEmployeeQuestionaireBase_purpose";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    @Translatable(translationField = "templateTranslated")
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public String getTemplateDD() {
        return "RepEmployeeQuestionaireBase_template";
    }
    @Transient
    @Translation(originalField = "template")
    private String templateTranslated;

    public String getTemplateTranslated() {
        return templateTranslated;
    }

    public void setTemplateTranslated(String templateTranslated) {
        this.templateTranslated = templateTranslated;
    }

    public String getTemplateTranslatedDD() {
        return "RepEmployeeQuestionaireBase_template";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="question">
    private String question;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionDD() {
        return "RepEmployeeQuestionaireBase_question";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="answer">
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerDD() {
        return "RepEmployeeQuestionaireBase_answer";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="type">
    @Translatable(translationField = "typeTranslated")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "RepEmployeeQuestionaireBase_type";
    }
    @Transient
    @Translation(originalField = "type")
    private String typeTranslated;

    public String getTypeTranslated() {
        return typeTranslated;
    }

    public void setTypeTranslated(String typeTranslated) {
        this.typeTranslated = typeTranslated;
    }

    public String getTypeTranslatedDD() {
        return "RepEmployeeQuestionaireBase_type";
    }
    //</editor-fold>
}
