package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class RepCompanyLevelBaseTranslation extends BaseEntityTranslation {
    // <editor-fold defaultstate="collapsed" desc="level0Description">

    @Column
    private String level0Description;

    public void setLevel0Description(String level0Description) {
        this.level0Description = level0Description;
    }

    public String getLevel0Description() {
        return level0Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level1Description">
    @Column
    private String level1Description;

    public void setLevel1Description(String level1Description) {
        this.level1Description = level1Description;
    }

    public String getLevel1Description() {
        return level1Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level2Description">
    @Column
    private String level2Description;

    public void setLevel2Description(String level2Description) {
        this.level2Description = level2Description;
    }

    public String getLevel2Description() {
        return level2Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level3Description">
    @Column
    private String level3Description;

    public void setLevel3Description(String level3Description) {
        this.level3Description = level3Description;
    }

    public String getLevel3Description() {
        return level3Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level4Description">
    @Column
    private String level4Description;

    public void setLevel4Description(String level4Description) {
        this.level4Description = level4Description;
    }

    public String getLevel4Description() {
        return level4Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level5Description">
    @Column
    private String level5Description;

    public void setLevel5Description(String level5Description) {
        this.level5Description = level5Description;
    }

    public String getLevel5Description() {
        return level5Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level6Description">
    @Column
    private String level6Description;

    public void setLevel6Description(String level6Description) {
        this.level6Description = level6Description;
    }

    public String getLevel6Description() {
        return level6Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level7Description">
    @Column
    private String level7Description;

    public void setLevel7Description(String level7Description) {
        this.level7Description = level7Description;
    }

    public String getLevel7Description() {
        return level7Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level8Description">
    @Column
    private String level8Description;

    public void setLevel8Description(String level8Description) {
        this.level8Description = level8Description;
    }

    public String getLevel8Description() {
        return level8Description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="level9Description">
    @Column
    private String level9Description;

    public void setLevel9Description(String level9Description) {
        this.level9Description = level9Description;
    }

    public String getLevel9Description() {
        return level9Description;
    }
    // </editor-fold>
}
