package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.*;

@Entity
public class ORepTemplDSTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold>
}
