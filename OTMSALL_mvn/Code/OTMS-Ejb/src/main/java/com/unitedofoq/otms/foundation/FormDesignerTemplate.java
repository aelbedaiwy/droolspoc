/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

/**
 *
 * @author arezk
 */
@Entity
public class FormDesignerTemplate extends BaseEntity {
    
    // <editor-fold defaultstate="collapsed" desc="text">
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    public String getTextDD(){
        return "FormDesignerTemplate_text";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="title">
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getTitleDD(){
        return "FormDesignerTemplate_title";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD(){
        return "FormDesignerTemplate_code";
    }
    // </editor-fold>
}
