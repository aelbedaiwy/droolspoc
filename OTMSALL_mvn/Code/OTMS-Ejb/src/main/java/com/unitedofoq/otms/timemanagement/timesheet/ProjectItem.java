/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author 3dly
 */
@Entity
@ParentEntity(fields="projectID")
public class ProjectItem extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="projectID">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Project projectID;

    public Project getProjectID() {
        return projectID;
    }

    public void setProjectID(Project projectID) {
        this.projectID = projectID;
    }

    public String getProjectIDDD() {
        return "ProjectItem_projectID";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="itemID">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Item itemID;

    public Item getItemID() {
        return itemID;
    }

    public void setItemID(Item itemID) {
        this.itemID = itemID;
    }
    
    public String getItemIDDD(){
        return "ProjectItem_itemID";
    }
    //</editor-fold>
}
