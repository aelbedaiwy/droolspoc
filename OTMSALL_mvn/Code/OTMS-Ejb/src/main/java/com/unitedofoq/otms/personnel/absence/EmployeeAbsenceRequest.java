package com.unitedofoq.otms.personnel.absence;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.dayoff.EmployeeDayOffRequest;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("ABSENCE")
@ParentEntity(fields = {"employee"})
public class EmployeeAbsenceRequest extends EmployeeDayOffRequest {

    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence absence;

    public Absence getAbsence() {
        return absence;
    }

    public void setAbsence(Absence absence) {
        this.absence = absence;
    }

    public String getAbsenceDD() {
        return "EmployeeAbsenceRequest_absence";
    }
}
