package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplateSequence;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class AppraisalHistoryBase extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="description">

    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "AppraisalHistoryBase_description";
    }
     //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "AppraisalHistoryBase_comments";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="totalScore">
    @Column(precision = 25, scale = 13)
    private BigDecimal totalScore = new BigDecimal(0);

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public String getTotalScoreDD() {
        return "AppraisalHistoryBase_totalScore";
    }

    @Transient
    private BigDecimal totalScoreMask;

    public BigDecimal getTotalScoreMask() {
        totalScoreMask = totalScore;
        return totalScoreMask;
    }

    public void setTotalScoreMask(BigDecimal totalScoreMask) {
        updateDecimalValue("totalScore", totalScoreMask);
    }

    public String getTotalScoreMaskDD() {
        return "AppraisalHistoryBase_totalScoreMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="averageScore">
    //the final score after formula calculation
    @Column(precision = 25, scale = 13)
    private BigDecimal averageScore = new BigDecimal(0);

    public BigDecimal getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(BigDecimal averageScore) {
        this.averageScore = averageScore;
    }

    public String getAverageScoreDD() {
        return "AppraisalHistoryBase_averageScore";
    }

    @Transient
    private BigDecimal averageScoreMask;

    public BigDecimal getAverageScoreMask() {
        averageScoreMask = averageScore;
        return averageScoreMask;
    }

    public void setAverageScoreMask(BigDecimal averageScoreMask) {
        updateDecimalValue("averageScore", averageScoreMask);
    }

    public String getAverageScoreMaskDD() {
        return "AppraisalHistoryBase_averageScoreMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "AppraisalHistoryBase_employee";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="sequence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplateSequence sequence;

    public AppraisalTemplateSequence getSequence() {
        return sequence;
    }

    public void setSequence(AppraisalTemplateSequence sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD() {
        return "AppraisalHistoryBase_sequence";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="confirmed">
    private boolean confirmed;

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getConfirmedDD() {
        return "AppraisalHistoryBase_confirmed";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="rating">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule rating;

    public ScaleTypeRule getRating() {
        return rating;
    }

    public String getRatingDD() {
        return "AppraisalHistoryBase_rating";
    }

    public void setRating(ScaleTypeRule rating) {
        this.rating = rating;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="appraiser">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee appraiser;

    public Employee getAppraiser() {
        return appraiser;
    }

    public String getAppraiserDD() {
        return "AppraisalHistoryBase_appraiser";
    }

    public void setAppraiser(Employee appraiser) {
        this.appraiser = appraiser;
    }
    //</editor-fold>
}
