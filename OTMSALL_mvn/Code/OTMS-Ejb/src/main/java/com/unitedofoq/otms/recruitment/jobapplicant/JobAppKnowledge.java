
package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccuElementGradeBase;
import com.unitedofoq.otms.foundation.occupation.Knowledge;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobAppKnowledge extends OccuElementGradeBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
	private JobApplicant jobApplicant;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Knowledge knowledge;
    public String getKnowledgeDD(){
        return "JobAppKnowledge_knowledge";
    }
	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant theJobApplicant) {
		jobApplicant = theJobApplicant;
	}
	
	public Knowledge getKnowledge() {
		return knowledge;
	}

	public void setKnowledge(Knowledge theKnowledge) {
		knowledge = theKnowledge;
	}
}
