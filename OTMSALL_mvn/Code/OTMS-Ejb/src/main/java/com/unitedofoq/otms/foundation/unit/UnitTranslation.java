package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.*;

@Entity
public class UnitTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>
}
