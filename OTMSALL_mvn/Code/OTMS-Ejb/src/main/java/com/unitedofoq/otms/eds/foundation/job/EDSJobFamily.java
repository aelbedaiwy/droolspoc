/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.job;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import com.unitedofoq.otms.foundation.job.JobBase;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 *
 * @author mmohamed
 */
@Entity
@Table(name = "Job")
@DiscriminatorValue("MASTER")
@ParentEntity(fields="company")

public class EDSJobFamily extends JobBase {
    // <editor-fold defaultstate="collapsed" desc="positions">
    @OneToMany(mappedBy="jobFamily")
	private List<EDSJob> jobs;

    public List<EDSJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<EDSJob> jobs) {
        this.jobs = jobs;
    }

    public String getJobsDD() {
        return "Job_jobs";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private EDSCompany company;
    public EDSCompany getCompany() {
        return company;
    }
    public void setCompany(EDSCompany company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "EDSJobFamily_company";
    }
    // </editor-fold>
}
