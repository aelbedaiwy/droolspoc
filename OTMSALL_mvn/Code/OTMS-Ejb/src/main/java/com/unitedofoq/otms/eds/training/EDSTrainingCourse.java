/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.eds.competency.EDSCompetencyTrainingCourse;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
public class EDSTrainingCourse extends BusinessObjectBaseEntity{
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;

    public String getDescriptionTranslatedDD() {
        return "EDSTrainingCourse_description";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String prerequisite;
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSChannel channel;
    @OneToMany(mappedBy="courses")
    private List<EDSTrainingProviderCourse> trainingProviderCourses;
    @OneToMany(mappedBy="trainingCourse")
    private List<EDSCompetencyTrainingCourse> competencyTrainingCourses;

    public String getPrerequisiteDD(){  return "EDSTrainingCourse_prerequisite";  }
    public String getNameTranslatedDD()        {  return "EDSTrainingCourse_name";  }
    public String getTrainingProviderCoursesDD()        {  return "EDSTrainingCourse_trainingProviderCourses";  }
    public String getCompetencyTrainingCoursesDD()        {  return "EDSTrainingCourse_competencyTrainingCourses";  }

    public EDSChannel getChannel() {
        return channel;
    }

    public void setChannel(EDSChannel channel) {
        this.channel = channel;
    }

    public List<EDSCompetencyTrainingCourse> getCompetencyTrainingCourses() {
        return competencyTrainingCourses;
    }

    public void setCompetencyTrainingCourses(List<EDSCompetencyTrainingCourse> competencyTrainingCourses) {
        this.competencyTrainingCourses = competencyTrainingCourses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public List<EDSTrainingProviderCourse> getTrainingProviderCourses() {
        return trainingProviderCourses;
    }

    public void setTrainingProviderCourses(List<EDSTrainingProviderCourse> trainingProviderCourses) {
        this.trainingProviderCourses = trainingProviderCourses;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    
}
