package com.unitedofoq.otms.appraisal.training;

import com.unitedofoq.otms.appraisal.employee.AppraisalHistoryBase;
import com.unitedofoq.otms.training.activity.Provider;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ProviderAppraisalHistory extends AppraisalHistoryBase {

    //<editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "ProviderAppraisalHistory_provider";
    }
    //</editor-fold>

}
