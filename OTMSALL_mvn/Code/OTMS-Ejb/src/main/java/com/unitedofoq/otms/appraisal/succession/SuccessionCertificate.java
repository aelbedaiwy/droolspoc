package com.unitedofoq.otms.appraisal.succession;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
@ParentEntity(fields={"plan"})
public class SuccessionCertificate extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="plan">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch= FetchType.LAZY)
    private SuccessionPlan plan;

    public void setPlan(SuccessionPlan plan) {
        this.plan = plan;
    }

    public SuccessionPlan getPlan() {
        return plan;
    }

    public String getPlanDD() {
        return "SuccessionCertificate_plan";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certificate">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC certificate;

    public UDC getCertificate() {
        return certificate;
    }
    public String getCertificateDD() {
        return "SuccessionCertificate_certificate";
    }

    public void setCertificate(UDC certificate) {
        this.certificate = certificate;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "SuccessionCertificate_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="plannedDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date plannedDate;

    public Date getPlannedDate() {
        return plannedDate;
    }
    public String getPlannedDateDD() {
        return "SuccessionCertificate_plannedDate";
    }

    public void setPlannedDate(Date plannedDate) {
        this.plannedDate = plannedDate;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actualDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date actualDate;

    public Date getActualDate() {
        return actualDate;
    }
    public String getActualDateDD() {
        return "SuccessionCertificate_actualDate";
    }

    public void setActualDate(Date actualDate) {
        this.actualDate = actualDate;
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="comment">
    @Column(name="comments")
    private String comment;
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public String getCommentDD() {
        return "SuccessionCourse_comment";
    }
    //</editor-fold>
}
