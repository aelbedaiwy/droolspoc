package com.unitedofoq.otms.foundation.currency;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.*;


/**
 * The persistent class for the currency database table.
 * 
 */
@Entity
@Table(name="OCurrency")
public class Currency extends BusinessObjectBaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="code">
	@Column
	private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Currency_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
	@Column(nullable=false,length=40)
	private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "Currency_description";
    }
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
   
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Islocal">
	@Column(length=1)
	private String islocal;

    public String getIslocal() {
        return islocal;
    }

    public void setIslocal(String islocal) {
        this.islocal = islocal;
    }

    
    public String getIslocalDD() {
        return "Currency_islocal";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="digits">
    @Column(precision=10, scale=0)
    private BigDecimal digits;
    public BigDecimal getDigits() {
        return digits;
    }

    public void setDigits(BigDecimal digits) {
        this.digits = digits;
    }

    public String getDigitsDD() {
        return "Currency_digits";
    }
    @Transient
    private BigDecimal digitsMask;
    public BigDecimal getDigitsMask() {
        digitsMask= digits;
        return digitsMask;
    }

    public void setDigitsMask(BigDecimal digitsMask) {
        updateDecimalValue("digits",digitsMask);
    }

    public String getDigitsMaskDD() {
        return "Currency_digitsMask";
    }
    // </editor-fold>
   

}