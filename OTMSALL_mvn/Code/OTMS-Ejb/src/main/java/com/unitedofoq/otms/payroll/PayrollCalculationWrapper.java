package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.ManyToOne;

@Entity
public class PayrollCalculationWrapper extends BaseEntity {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayrollCalculation payrollCalculation;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job job;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade payGrade;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC hiringType;

    public UDC getHiringType() {
        return hiringType;
    }

    public void setHiringType(UDC hiringType) {
        this.hiringType = hiringType;
    }

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public String getCostCenterDD() {
        return "PayrollCalculationWrapper_costCenter";
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "PayrollCalculationWrapper_employee";
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getHiringTypeDD() {
        return "PayrollCalculationWrapper_hiringType";
    }

    public Job getJob() {
        return job;
    }

    public String getJobDD() {
        return "PayrollCalculationWrapper_job";
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public UDC getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "PayrollCalculationWrapper_location";
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public PayGrade getPayGrade() {
        return payGrade;
    }

    public String getPayGradeDD() {
        return "PayrollCalculationWrapper_payGrade";
    }

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public PayrollCalculation getPayrollCalculation() {
        return payrollCalculation;
    }

    public String getPayrollCalculationDD() {
        return "PayrollCalculationWrapper_payrollCalculation";
    }

    public void setPayrollCalculation(PayrollCalculation payrollCalculation) {
        this.payrollCalculation = payrollCalculation;
    }

    public Position getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "PayrollCalculationWrapper_position";
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Unit getUnit() {
        return unit;
    }

    public String getUnitDD() {
        return "PayrollCalculationWrapper_unit";
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
    /*
     @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
     @JoinColumn(name="retro_dbid")
     private RetroPayrollCalculation retroPayrollCalculation;

     public RetroPayrollCalculation getRetroPayrollCalculation() {
     return retroPayrollCalculation;
     }

     public void setRetroPayrollCalculation(RetroPayrollCalculation retroPayrollCalculation) {
     this.retroPayrollCalculation = retroPayrollCalculation;
     }

     public String getRetroPayrollCalculationDD() {
     return "PayrollCalculationWrapper_retroPayrollCalculation";
     }
     * */
}
