
package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.*;

@Entity
@Table(name = "position")
public class PositionPlan extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "PositionPlan_name";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "PositionPlan_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unit_dbid">
    @Column
    private Long unit_dbid;

    public void setUnit_dbid(Long unit_dbid) {
        this.unit_dbid = unit_dbid;
    }

    public Long getUnit_dbid() {
        return unit_dbid;
    }

    public String getUnit_dbidDD() {
        return "PositionPlan_unit_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="originalPosition_dbid">
    @Column
    private Long originalPosition_dbid;

    public void setOriginalPosition_dbid(Long originalPosition_dbid) {
        this.originalPosition_dbid = originalPosition_dbid;
    }

    public Long getOriginalPosition_dbid() {
        return originalPosition_dbid;
    }

    public String getOriginalPosition_dbidDD() {
        return "PositionPlan_originalPosition_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="headcount">
    @Column
    private int headcount;

    public void setHeadcount(int headcount) {
        this.headcount = headcount;
    }

    public int getHeadcount() {
        return headcount;
    }

    public String getHeadcountDD() {
        return "PositionPlan_headcount";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hierarchycode">
    @Column
    private String hierarchycode;

    public void setHierarchycode(String hierarchycode) {
        this.hierarchycode = hierarchycode;
    }

    public String getHierarchycode() {
        return hierarchycode;
    }

    public String getHierarchycodeDD() {
        return "PositionPlan_hierarchycode";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="DTYPE">
    private String DTYPE = "MASTER";

    public String getDTYPE() {
        return DTYPE;
    }

    public void setDTYPE(String DTYPE) {
        this.DTYPE = DTYPE;
    }
    
    public String getDTYPEDD() {
        return "PositionPlan_DTYPE";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="parentPositionSimpleMode">
    private Long parentPosition_dbid;

    public Long getParentPosition_dbid() {
        return parentPosition_dbid;
    }

    public void setParentPosition_dbid(Long parentPosition_dbid) {
        this.parentPosition_dbid = parentPosition_dbid;
    }
    
    public String getParentPosition_dbidDD() {
        return "PositionPlan_parentPosition_dbid";
    }
    // </editor-fold >
}