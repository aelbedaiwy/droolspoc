/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.jobGroup.JobGroup;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author lap
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="VTYPE")
public class VisaBase extends BusinessObjectBaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column(nullable=false)
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
 public String getCodeDD() {
        return "VisaBase_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nationality">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nationality;

    public UDC getNationality() {
        return nationality;
    }

    public void setNationality(UDC nationality) {
        this.nationality = nationality;
    }
public String getNationalityDD() {
        return "VisaBase_nationality";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobGroup">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private JobGroup jobGroup;

    public JobGroup getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }
public String getJobGroupDD() {
        return "VisaBase_jobGroup";
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="validPeriod">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC validPeriod;

    public UDC getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(UDC validPeriod) {
        this.validPeriod = validPeriod;
    }
     public String getValidPeriodDD() {
        return "VisaBase_validPeriod";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastRenewalDate">
    @Column(nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRenewalDate;

    public Date getLastRenewalDate() {
        return lastRenewalDate;
    }

    public void setLastRenewalDate(Date lastRenewalDate) {
        this.lastRenewalDate = lastRenewalDate;
    }
     public String getLastRenewalDateDD() {
        return "VisaBase_lastRenewalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="value">

    @Column(precision=25, scale=13)
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
    public String getValueDD() {
        return "VisaBase_value";
    }
    @Transient
    private BigDecimal valueMask;

    public BigDecimal getValueMask() {
        valueMask = value;
        return valueMask;
    }

    public void setValueMask(BigDecimal valueMask) {
        updateDecimalValue("value",valueMask);
    }
    public String getValueMaskDD() {
        return "VisaBase_valueMask";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueFrom">
   
    @Column
    private String issueFrom;

    public String getIssueFrom() {
        return issueFrom;
    }

    public void setIssueFrom(String issueFrom) {
        this.issueFrom = issueFrom;
    }
    public String getIssueFromDD() {
        return "VisaBase_issueFrom";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getNotesDD() {
        return "VisaBase_notes";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entryDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }
    public String getEntryDateDD() {
        return "VisaBase_entryDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="used">
    @Column
    private String used;

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }
 public String getUsedDD() {
        return "VisaBase_used";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="visaIssueDate">
   
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date visaIssueDate;

    public Date getVisaIssueDate() {
        return visaIssueDate;
    }

    public void setVisaIssueDate(Date visaIssueDate) {
        this.visaIssueDate = visaIssueDate;
    }
public String getVisaIssueDateDD() {
        return "VisaBase_visaIssueDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
  
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "VisaBase_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="changeDate">
     @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date changeDate;

    public Date getChangeDate() {
        return changeDate;
    }
    
    public String getChangeDateDD() {
        return "VisaBase_changeDate";
    }
    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expirDate">
    @Column(nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirDate;

    public Date getExpirDate() {
        return expirDate;
    }

    public void setExpirDate(Date expirDate) {
        this.expirDate = expirDate;
    }
    public String getExpirDateDD() {
        return "VisaBase_expirDate";
    }
// </editor-fold>
  
}
