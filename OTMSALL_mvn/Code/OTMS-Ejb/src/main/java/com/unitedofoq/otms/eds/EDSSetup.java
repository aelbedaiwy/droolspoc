/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds;

import com.unitedofoq.otms.eds.foundation.job.EDSJob;
import com.unitedofoq.otms.eds.training.EDSChannel;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.OneToOne;

/**
 *
 * @author mmohamed
 */
@Entity
public class EDSSetup extends BaseEntity{

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    EDSJob hrJob;

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    EDSChannel trainingChannel;

    public EDSJob getHrJob() {
        return hrJob;
    }

    public String getHrJobDD() {
        return "EDSSetup_hrJob";
    }

    public void setHrJob(EDSJob hrJob) {
        this.hrJob = hrJob;
    }

    public EDSChannel getTrainingChannel() {
        return trainingChannel;
    }

    public String getTrainingChannelDD() {
        return "EDSSetup_trainingChannel";
    }

    public void setTrainingChannel(EDSChannel trainingChannel) {
        this.trainingChannel = trainingChannel;
    }
    
    
}
