package com.unitedofoq.otms.personnel.investigation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
@ParentEntity(fields = {"employee"})
public class EmpInvestigation extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="employee">

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmpInvestigation_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="investigation">
    @ManyToOne(fetch = FetchType.LAZY)
    private InvestigationSetup investigation;

    public InvestigationSetup getInvestigation() {
        return investigation;
    }

    public void setInvestigation(InvestigationSetup investigation) {
        this.investigation = investigation;
    }

    public String getInvestigationDD() {
        return "EmpInvestigation_investigation";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = FetchType.LAZY)
    private InvestigationStatus status;

    public InvestigationStatus getStatus() {
        return status;
    }

    public void setStatus(InvestigationStatus status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "EmpInvestigation_status";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="oldStatus">
    @ManyToOne(fetch = FetchType.LAZY)
    private InvestigationStatus oldStatus;

    public InvestigationStatus getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(InvestigationStatus oldStatus) {
        this.oldStatus = oldStatus;
    }

    public String getOldStatusDD() {
        return "EmpInvestigation_oldStatus";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="reason">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC reason;

    public UDC getReason() {
        return reason;
    }

    public void setReason(UDC reason) {
        this.reason = reason;
    }

    public String getReasonDD() {
        return "EmpInvestigation_reason";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="invDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date invDate;

    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    public String getInvDateDD() {
        return "EmpInvestigation_invDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="approvedBy">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee approvedBy;

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "EmpInvestigation_approvedBy";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="enteredBy">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee enteredBy;

    public Employee getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(Employee enteredBy) {
        this.enteredBy = enteredBy;
    }

    public String getEnteredByDD() {
        return "EmpInvestigation_enteredBy";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="raisedBy">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee raisedBy;

    public Employee getRaisedBy() {
        return raisedBy;
    }

    public void setRaisedBy(Employee raisedBy) {
        this.raisedBy = raisedBy;
    }

    public String getRaisedByDD() {
        return "EmpInvestigation_raisedBy";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes">
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmpInvestigation_notes";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="invActive">
    private boolean invActive;

    public boolean isInvActive() {
        return invActive;
    }

    public void setInvActive(boolean invActive) {
        this.invActive = invActive;
    }

    public String getInvActiveDD() {
        return "EmpInvestigation_invActive";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empInvestigation">
    @ManyToOne(fetch = FetchType.LAZY)
    private EmpInvestigation empInvestigation;

    public EmpInvestigation getEmpInvestigation() {
        return empInvestigation;
    }

    public String getEmpInvestigationDD() {
        return "EmpInvestigation_empInvestigation";
    }

    public void setEmpInvestigation(EmpInvestigation empInvestigation) {
        this.empInvestigation = empInvestigation;
    }
    //</editor-fold >
    // <editor-fold defaultstate="collapsed" desc="suspensionCancelled">
    @Column(length = 1)
    private String suspensionCancelled;

    public String getSuspensionCancelled() {
        return suspensionCancelled;
    }

    public void setSuspensionCancelled(String suspensionCancelled) {
        this.suspensionCancelled = suspensionCancelled;
    }

    public String getSuspensionCancelledDD() {
        return "EmpInvestigation_suspensionCancelled";
    }
    // </editor-fold>
}
