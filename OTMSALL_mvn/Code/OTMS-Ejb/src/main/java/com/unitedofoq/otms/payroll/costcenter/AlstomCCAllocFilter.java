package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.FilterBase;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class AlstomCCAllocFilter extends FilterBase {
    // <editor-fold defaultstate="collapsed" desc="calcPeriod">
    @ManyToOne(fetch = FetchType.LAZY)
    private CalculatedPeriod calcPeriod;

    public CalculatedPeriod getCalcPeriod() {
        return calcPeriod;
    }

    public void setCalcPeriod(CalculatedPeriod calcPeriod) {
        this.calcPeriod = calcPeriod;
    }

    public String getCalcPeriodDD() {
        return "AlstomCCAllocFilter_calcPeriod";
    }
    // </editor-fold>
}