package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import java.util.List;
import javax.persistence.*;

@Entity
@ChildEntity(fields = "messages")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PTYPE")
@DiscriminatorValue("MASTER")
public class PayrollCalculation extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }

    public String getCalculatedPeriodDD() {
        return "PayrollCalculation_calculatedPeriod";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="recalculateEmployees">
    @Column(nullable = false)
    boolean recalculateEmployees = false;

    public boolean isRecalculateEmployees() {
        return recalculateEmployees;
    }

    public void setRecalculateEmployees(boolean recalculateEmployees) {
        this.recalculateEmployees = recalculateEmployees;
    }

    public String getRecalculateEmployeesDD() {
        return "PayrollCalculation_recalculateEmployees";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="excludeTerminatedEmployees">
    @Column(nullable = false)
    boolean excludeTerminatedEmployees = true;

    public boolean isExcludeTerminatedEmployees() {
        return excludeTerminatedEmployees;
    }

    public void setExcludeTerminatedEmployees(boolean excludeTerminatedEmployees) {
        this.excludeTerminatedEmployees = excludeTerminatedEmployees;
    }

    public String getExcludeTerminatedEmployeesDD() {
        return "PayrollCalculation_excludeTerminatedEmployees";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="excludePersonnelClose">
    @Column(nullable = false)
    boolean excludePersonnelClose = true;//Exclude Who hasn't Personnel Close

    public boolean isExcludePersonnelClose() {
        return excludePersonnelClose;
    }

    public void setExcludePersonnelClose(boolean excludePersonnelClose) {
        this.excludePersonnelClose = excludePersonnelClose;
    }

    public String getExcludePersonnelCloseDD() {
        return "PayrollCalculation_excludePersonnelClose";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="excludeTimeManagement">
    @Column(nullable = false)
    boolean excludeTimeManagement = false;       //Exclude Who hasn't Time Management

    public boolean isExcludeTimeManagement() {
        return excludeTimeManagement;
    }

    public void setExcludeTimeManagement(boolean excludeTimeManagement) {
        this.excludeTimeManagement = excludeTimeManagement;
    }

    public String getExcludeTimeManagementDD() {
        return "PayrollCalculation_excludeTimeManagement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="messages">
    @OneToMany(mappedBy = "payrollCalculation")
    private List<PayrollMessages> messages;

    public List<PayrollMessages> getMessages() {
        return messages;
    }

    public void setMessages(List<PayrollMessages> messages) {
        this.messages = messages;
    }

    public String getMessagesDD() {
        return "PayrollCalculation_messages";
    }
    // </editor-fold>
}
