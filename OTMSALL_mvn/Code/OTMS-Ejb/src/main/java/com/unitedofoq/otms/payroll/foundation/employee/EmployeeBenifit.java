
package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"employee"})
public class EmployeeBenifit extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeBenifit_employee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobileLimit">
    @Column
    private BigDecimal mobileLimit;

    public void setMobileLimit(BigDecimal mobileLimit) {
        this.mobileLimit = mobileLimit;
    }

    public BigDecimal getMobileLimit() {
        return mobileLimit;
    }

    public String getMobileLimitDD() {
        return "EmployeeBenifit_mobileLimit";
    }
        
    @Transient
    private BigDecimal mobileLimitMask;
    public BigDecimal getMobileLimitMask() {
        mobileLimitMask = mobileLimit ;
        return mobileLimitMask;
    }
    public void setMobileLimitMask(BigDecimal mobileLimitMask) {
        updateDecimalValue("mobileLimit",mobileLimitMask);
    }
    public String getMobileLimitMaskDD() {
        return "EmployeeBenifit_mobileLimitMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobile1Limit">
    @Column
    private BigDecimal mobile1Limit;

    public void setMobile1Limit(BigDecimal mobile1Limit) {
        this.mobile1Limit = mobile1Limit;
    }

    public BigDecimal getMobile1Limit() {
        return mobile1Limit;
    }

    public String getMobile1LimitDD() {
        return "EmployeeBenifit_mobile1Limit";
    }
    
    @Transient
    private BigDecimal mobile1LimitMask;
    public BigDecimal getMobile1LimitMask() {
        mobile1LimitMask = mobile1Limit ;
        return mobile1LimitMask;
    }
    public void setMobile1LimitMask(BigDecimal mobile1LimitMask) {
        updateDecimalValue("mobile1Limit",mobile1LimitMask);
    }
    public String getMobile1LimitMaskDD() {
        return "EmployeeBenifit_mobile1LimitMask";
    }
    // </editor-fold>

    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }
    public String getM2mCheckDD() {
        return "EmployeeBenifit_m2mCheck";
    }

    @Override
    public void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
    
}
