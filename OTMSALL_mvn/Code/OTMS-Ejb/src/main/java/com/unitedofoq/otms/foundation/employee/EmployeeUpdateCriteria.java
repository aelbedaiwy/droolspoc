package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.Transient;


@Entity
public class EmployeeUpdateCriteria extends BaseEntity{

  // <editor-fold defaultstate="collapsed" desc="Employee Code">
    private String empCode;

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }
    
    public String getEmpCodeDD() {
        return "EmployeeUpdateCriteria_empCode";
    }
 
// </editor-fold>    
  // <editor-fold defaultstate="collapsed" desc="Field Value">  
    private String fieldValue;

    
    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
    
    public String getFieldValueDD() {
        return "EmployeeUpdateCriteria_fieldValue";
    }
// </editor-fold>
  // <editor-fold defaultstate="collapsed" desc="Done"> 
  private Boolean done;

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
  
    public String getDoneDD() {
        return "EmployeeUpdateCriteria_done";
    }
// </editor-fold>
    
  @Transient
  
  private String fieldname;

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }
  
    public String getFieldnameDD() {
        return "EmployeeUpdateCriteria_fieldname";
    }
    
}

