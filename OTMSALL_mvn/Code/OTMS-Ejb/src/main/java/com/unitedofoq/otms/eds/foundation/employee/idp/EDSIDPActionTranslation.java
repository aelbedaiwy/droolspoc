
package com.unitedofoq.otms.eds.foundation.employee.idp;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class EDSIDPActionTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="achievementComment">
    @Column
    private String achievementComment;

    public void setAchievementComment(String achievementComment) {
        this.achievementComment = achievementComment;
    }

    public String getAchievementComment() {
        return achievementComment;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

}
