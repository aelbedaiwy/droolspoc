/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="repemployeemilitarydata")
public class RepEmployeeMilitaryData extends RepEmployeeBase {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeMilitaryData_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeMilitaryData_genderDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    //@Column(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepEmployeeMilitaryData_employeeActive";
    }
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeeMilitaryData_employeeActive";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="fromDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fromDate;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDateDD() {
        return "RepEmployeeMilitaryData_fromDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date toDate;

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
    
    public String getToDateDD() {
        return "RepEmployeeMilitaryData_toDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="issueOffice">
    @Column
    @Translatable(translationField = "issueOfficeTranslated")
    private String issueOffice;

    public String getIssueOffice() {
        return issueOffice;
    }

    public void setIssueOffice(String issueOffice) {
        this.issueOffice = issueOffice;
    }

    public String getIssueOfficeDD() {
        return "RepEmployeeMilitaryData_issueOffice";
    }

    @Transient
    @Translation(originalField = "issueOffice")
    private String issueOfficeTranslated;

    public String getIssueOfficeTranslated() {
        return issueOfficeTranslated;
    }

    public void setIssueOfficeTranslated(String issueOfficeTranslated) {
        this.issueOfficeTranslated = issueOfficeTranslated;
    }
    
    public String getIssueOfficeTranslatedDD() {
        return "RepEmployeeMilitaryData_issueOffice";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="reserveDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date reserveDate;

    public Date getReserveDate() {
        return reserveDate;
    }

    public void setReserveDate(Date reserveDate) {
        this.reserveDate = reserveDate;
    }

    public String getReserveDateDD() {
        return "RepEmployeeMilitaryData_reserveDate";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="status">
    @Column
    @Translatable(translationField = "statusTranslated")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "RepEmployeeMilitaryData_status";
    }
    
    @Transient
    @Translation(originalField = "status")
    private String statusTranslated;

    public String getStatusTranslated() {
        return statusTranslated;
    }

    public void setStatusTranslated(String statusTranslated) {
        this.statusTranslated = statusTranslated;
    }
    
    public String getStatusTranslatedDD() {
        return "RepEmployeeMilitaryData_status";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="formType">
    @Column
    @Translatable(translationField = "formTypeTranslated")
    private String formType;

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormTypeDD() {
        return "RepEmployeeMilitaryData_formType";
    }
    
    @Transient
    @Translation(originalField = "formType")
    private String formTypeTranslated;

    public String getFormTypeTranslated() {
        return formTypeTranslated;
    }

    public void setFormTypeTranslated(String formTypeTranslated) {
        this.formTypeTranslated = formTypeTranslated;
    }
    
    public String getFormTypeTranslatedDD() {
        return "RepEmployeeMilitaryData_formType";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="examinationDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date examinationDate;

    public Date getExaminationDate() {
        return examinationDate;
    }

    public void setExaminationDate(Date examinationDate) {
        this.examinationDate = examinationDate;
    }

    public String getExaminationDateDD() {
        return "RepEmployeeMilitaryData_examinationDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="compeletionDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date compeletionDate;

    public Date getCompeletionDate() {
        return compeletionDate;
    }

    public void setCompeletionDate(Date compeletionDate) {
        this.compeletionDate = compeletionDate;
    }

    public String getCompeletionDateDD() {
        return "RepEmployeeMilitaryData_compeletionDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="addToServicePeriod">
    @Column
    private String addToServicePeriod;

    public String getAddToServicePeriod() {
        return addToServicePeriod;
    }

    public void setAddToServicePeriod(String addToServicePeriod) {
        this.addToServicePeriod = addToServicePeriod;
    }

    public String getAddToServicePeriodDD() {
        return "RepEmployeeMilitaryData_addToServicePeriod";
    }
    //</editor-fold>
}
