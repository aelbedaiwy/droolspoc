package com.unitedofoq.otms.recruitment.reports;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name = "repreqselapphis")
public class RepRequisitionSelectedApplicantHistory extends RepRecruitmentBase {

}