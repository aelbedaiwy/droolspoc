package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.otms.recruitment.vacancy.Vacancy;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "VACANCY")
public class AppliedApplicantOnVacancy extends AppliedApplicant {

    // <editor-fold defaultstate="collapsed" desc="vacancy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacancy vacancy;

    public Vacancy getVacancy() {
        return vacancy;
    }

    public String getVacancyDD() {
        return "AppliedApplicantOnVacancy_vacancy";
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }
    // </editor-fold >
}
