package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.unit.Unit;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"unit"})
public class DepartmentGoal extends GoalBase {

    //<editor-fold defaultstate="collapsed" desc="unit">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitDD() {
        return "DepartmentGoal_unit";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="goal">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public String getGoalDD() {
        return "DepartmentGoal_goal";
    }
    //</editor-fold>
}
