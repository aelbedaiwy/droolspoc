package com.unitedofoq.otms.security;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Remote;

@Remote
public interface CustomReportDataSecurityServiceLocal {

    public OFunctionResult birtReportApplySecurity(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public String getQuery(String query, OUser loggedUser);

    public String applyReportSecuirty(String selectSQL, OUser loggedUser);
}
