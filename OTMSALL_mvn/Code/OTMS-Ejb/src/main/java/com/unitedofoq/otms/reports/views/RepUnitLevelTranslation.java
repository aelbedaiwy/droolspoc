package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepUnitLevelTranslation extends RepCompanyLevelBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="unit">
    @Column
    private String unit;

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="parentUnit">
    @Column
    private String parentUnit;

    public void setParentUnit(String parentUnit) {
        this.parentUnit = parentUnit;
    }

    public String getParentUnit() {
        return parentUnit;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitLevelIndex">
    @Column
    private String unitLevelIndex;

    public void setUnitLevelIndex(String unitLevelIndex) {
        this.unitLevelIndex = unitLevelIndex;
    }

    public String getUnitLevelIndex() {
        return unitLevelIndex;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @Column
    private String costCenter;

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenter() {
        return costCenter;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="position">
    @Column
    private String position;

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="managerName">
    @Column
    private String managerName;

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerName() {
        return managerName;
    }
    // </editor-fold>

}
