package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantSkill extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantSkill_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldOfExpertise">
    private String fieldOfExpertise;

    public String getFieldOfExpertise() {
        return fieldOfExpertise;
    }

    public void setFieldOfExpertise(String fieldOfExpertise) {
        this.fieldOfExpertise = fieldOfExpertise;
    }

    public String getFieldOfExpertiseDD() {
        return "ApplicantSkill_fieldOfExpertise";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="professiency">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC professiency;

    public UDC getProfessiency() {
        return professiency;
    }

    public void setProfessiency(UDC professiency) {
        this.professiency = professiency;
    }

    public String getProfessiencyDD() {
        return "ApplicantSkill_professiency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="interest">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC interest;

    public UDC getInterest() {
        return interest;
    }

    public void setInterest(UDC interest) {
        this.interest = interest;
    }

    public String getInterestDD() {
        return "ApplicantSkill_interest";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="experienceYears">
    private int experienceYears;

    public int getExperienceYears() {
        return experienceYears;
    }

    public void setExperienceYears(int experienceYears) {
        this.experienceYears = experienceYears;
    }

    public String getExperienceYearsDD() {
        return "ApplicantSkill_experienceYears";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workJustification">
    private String workJustification;

    public String getWorkJustification() {
        return workJustification;
    }

    public void setWorkJustification(String workJustification) {
        this.workJustification = workJustification;
    }

    public String getWorkJustificationDD() {
        return "ApplicantSkill_workJustification";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workSample">
    private String workSample;

    public String getWorkSample() {
        return workSample;
    }

    public void setWorkSample(String workSample) {
        this.workSample = workSample;
    }

    public String getWorkSampleDD() {
        return "ApplicantSkill_workSample";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certificate">
    private String certificate;

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getCertificateDD() {
        return "ApplicantSkill_certificate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="eduOrTraining">
    private String eduOrTraining;

    public String getEduOrTraining() {
        return eduOrTraining;
    }

    public void setEduOrTraining(String eduOrTraining) {
        this.eduOrTraining = eduOrTraining;
    }

    public String getEduOrTrainingDD() {
        return "ApplicantSkill_eduOrTraining";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="readAboutIt">
    private String readAboutIt;

    public String getReadAboutIt() {
        return readAboutIt;
    }

    public void setReadAboutIt(String readAboutIt) {
        this.readAboutIt = readAboutIt;
    }

    public String getReadAboutItDD() {
        return "ApplicantSkill_readAboutIt";
    }
    // </editor-fold>
}