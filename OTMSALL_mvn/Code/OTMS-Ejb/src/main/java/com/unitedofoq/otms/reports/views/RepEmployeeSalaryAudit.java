package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeSalaryAudit extends RepEmployeeBase {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private long dsDBID;

    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }

    public long getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepEmployeeSalaryAudit_dsDBID";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="fixedOrVariable">
    @Translatable(translationField = "fixedOrVariableTranslated")
    private String fixedOrVariable;

    public String getFixedOrVariable() {
        return fixedOrVariable;
    }

    public void setFixedOrVariable(String fixedOrVariable) {
        this.fixedOrVariable = fixedOrVariable;
    }

    public String getFixedOrVariableDD() {
        return "RepEmployeeSalaryAudit_fixedOrVariable";
    }

    @Transient
    @Translation(originalField = "fixedOrVariable")
    private String fixedOrVariableTranslated;

    public String getFixedOrVariableTranslated() {
        return fixedOrVariableTranslated;
    }

    public void setFixedOrVariableTranslated(String fixedOrVariableTranslated) {
        this.fixedOrVariableTranslated = fixedOrVariableTranslated;
    }

    public String getFixedOrVariableTranslatedDD() {
        return "RepEmployeeSalaryAudit_fixedOrVariable";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="nature">
    @Translatable(translationField = "natureTranslated")
    private String nature;

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getNatureDD() {
        return "RepEmployeeSalaryAudit_nature";
    }

    @Transient
    @Translation(originalField = "nature")
    private String natureTranslated;

    public String getNatureTranslated() {
        return natureTranslated;
    }

    public void setNatureTranslated(String natureTranslated) {
        this.natureTranslated = natureTranslated;
    }

    public String getNatureTranslatedDD() {
        return "RepEmployeeSalaryAudit_nature";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeOrCompany">
    @Translatable(translationField = "employeeOrCompanyTranslated")
    private String employeeOrCompany;

    public String getEmployeeOrCompany() {
        return employeeOrCompany;
    }

    public void setEmployeeOrCompany(String employeeOrCompany) {
        this.employeeOrCompany = employeeOrCompany;
    }

    public String getEmployeeOrCompanyDD() {
        return "RepEmployeeSalaryAudit_employeeOrCompany";
    }

    @Transient
    @Translation(originalField = "employeeOrCompany")
    private String employeeOrCompanyTranslated;

    public String getEmployeeOrCompanyTranslated() {
        return employeeOrCompanyTranslated;
    }

    public void setEmployeeOrCompanyTranslated(String employeeOrCompanyTranslated) {
        this.employeeOrCompanyTranslated = employeeOrCompanyTranslated;
    }

    public String getEmployeeOrCompanyTranslatedDD() {
        return "RepEmployeeSalaryAudit_employeeOrCompany";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeSalaryAudit_genderDescription";
    }

    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }

    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeSalaryAudit_genderDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employeeactive">
    @Column
    @Translatable(translationField = "employeeactiveTranslated")
    private String employeeactive;

    public void setEmployeeactive(String employeeactive) {
        this.employeeactive = employeeactive;
    }

    public String getEmployeeactive() {
        return employeeactive;
    }

    public String getEmployeeactiveDD() {
        return "RepEmployeeSalaryAudit_employeeactive";
    }

    @Transient
    @Translation(originalField = "employeeactive")
    private String employeeactiveTranslated;

    public String getEmployeeactiveTranslated() {
        return employeeactiveTranslated;
    }

    public void setEmployeeactiveTranslated(String employeeactiveTranslated) {
        this.employeeactiveTranslated = employeeactiveTranslated;
    }

    public String getEmployeeactiveTranslatedDD() {
        return "RepEmployeeSalaryAudit_employeeactive";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "RepEmployeeSalaryAudit_description";
    }

    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "RepEmployeeSalaryAudit_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="salaryElementCode">
    @Column
    private String salaryElementCode;

    public void setSalaryElementCode(String salaryElementCode) {
        this.salaryElementCode = salaryElementCode;
    }

    public String getSalaryElementCode() {
        return salaryElementCode;
    }

    public String getSalaryElementCodeDD() {
        return "RepEmployeeSalaryAudit_salaryElementCode";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="value">
    @Column
    private BigDecimal value;

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getValueDD() {
        return "RepEmployeeSalaryAudit_value";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="valueEnc">
    @Column
    private String valueEnc;

    public String getValueEnc() {
        return valueEnc;
    }

    public void setValueEnc(String valueEnc) {
        this.valueEnc = valueEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElementType">
    @Column
    @Translatable(translationField = "salaryElementTypeTranslated")
    private String salaryElementType;

    public void setSalaryElementType(String salaryElementType) {
        this.salaryElementType = salaryElementType;
    }

    public String getSalaryElementType() {
        return salaryElementType;
    }

    public String getSalaryElementTypeDD() {
        return "RepEmployeeSalaryAudit_salaryElementType";
    }

    @Transient
    @Translation(originalField = "salaryElementType")
    private String salaryElementTypeTranslated;

    public String getSalaryElementTypeTranslated() {
        return salaryElementTypeTranslated;
    }

    public void setSalaryElementTypeTranslated(String salaryElementTypeTranslated) {
        this.salaryElementTypeTranslated = salaryElementTypeTranslated;
    }

    public String getSalaryElementTypeTranslatedDD() {
        return "RepEmployeeSalaryAudit_salaryElementType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="incomeOrDeduction">
    @Column
    @Translatable(translationField = "incomeOrDeductionTranslated")
    private String incomeOrDeduction;

    public void setIncomeOrDeduction(String incomeOrDeduction) {
        this.incomeOrDeduction = incomeOrDeduction;
    }

    public String getIncomeOrDeduction() {
        return incomeOrDeduction;
    }

    public String getIncomeOrDeductionDD() {
        return "RepEmployeeSalaryAudit_incomeOrDeduction";
    }

    @Transient
    @Translation(originalField = "incomeOrDeduction")
    private String incomeOrDeductionTranslated;

    public String getIncomeOrDeductionTranslated() {
        return incomeOrDeductionTranslated;
    }

    public void setIncomeOrDeductionTranslated(String incomeOrDeductionTranslated) {
        this.incomeOrDeductionTranslated = incomeOrDeductionTranslated;
    }

    public String getIncomeOrDeductionTranslatedDD() {
        return "RepEmployeeSalaryAudit_incomeOrDeduction";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="paymentperiod">
    @Column
    @Translatable(translationField = "paymentperiodTranslated")
    private String paymentperiod;

    public void setPaymentperiod(String paymentperiod) {
        this.paymentperiod = paymentperiod;
    }

    public String getPaymentperiod() {
        return paymentperiod;
    }

    public String getPaymentperiodDD() {
        return "RepEmployeeSalaryAudit_paymentperiod";
    }

    @Transient
    @Translation(originalField = "paymentperiod")
    private String paymentperiodTranslated;

    public String getPaymentperiodTranslated() {
        return paymentperiodTranslated;
    }

    public void setPaymentperiodTranslated(String paymentperiodTranslated) {
        this.paymentperiodTranslated = paymentperiodTranslated;
    }

    public String getPaymentperiodTranslatedDD() {
        return "RepEmployeeSalaryAudit_paymentperiod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="paymentmethod">
    @Column
    @Translatable(translationField = "paymentmethodTranslated")
    private String paymentmethod;

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public String getPaymentmethodDD() {
        return "RepEmployeeSalaryAudit_paymentmethod";
    }

    @Transient
    @Translation(originalField = "paymentmethod")
    private String paymentmethodTranslated;

    public String getPaymentmethodTranslated() {
        return paymentmethodTranslated;
    }

    public void setPaymentmethodTranslated(String paymentmethodTranslated) {
        this.paymentmethodTranslated = paymentmethodTranslated;
    }

    public String getPaymentmethodTranslatedDD() {
        return "RepEmployeeSalaryAudit_paymentmethod";
    }
    // </editor-fold>
}
