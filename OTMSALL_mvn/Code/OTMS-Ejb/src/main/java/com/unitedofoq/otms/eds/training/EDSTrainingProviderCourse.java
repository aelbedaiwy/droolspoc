/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.training;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.List;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="provider")
@ChildEntity(fields={"schedules"})
public class EDSTrainingProviderCourse extends BaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSTrainingProvider provider;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSTrainingCourse courses;
    @OneToMany(mappedBy="trainingProviderCourse")
    private List<EDSTrainingProviderCourseSchedule> schedules;

    public EDSTrainingCourse getCourses() {
        return courses;
    }

    public void setCourses(EDSTrainingCourse courses) {
        this.courses = courses;
    }

    public EDSTrainingProvider getProvider() {
        return provider;
    }

    public void setProvider(EDSTrainingProvider provider) {
        this.provider = provider;
    }

    public List<EDSTrainingProviderCourseSchedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<EDSTrainingProviderCourseSchedule> schedules) {
        this.schedules = schedules;
    }
}
