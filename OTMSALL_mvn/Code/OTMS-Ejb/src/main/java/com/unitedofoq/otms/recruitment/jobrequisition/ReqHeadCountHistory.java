package com.unitedofoq.otms.recruitment.jobrequisition;

import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class ReqHeadCountHistory extends BaseEntity {

    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
    private Date setDate;
    @Column(nullable=false)
	private int oldValue;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Employee setBy;

    public int getOldValue() {
        return oldValue;
    }

    public void setOldValue(int oldValue) {
        this.oldValue = oldValue;
    }

    public Employee getSetBy() {
        return setBy;
    }

    public void setSetBy(Employee setBy) {
        this.setBy = setBy;
    }

    public Date getSetDate() {
        return setDate;
    }

    public void setSetDate(Date setDate) {
        this.setDate = setDate;
    }

    
}