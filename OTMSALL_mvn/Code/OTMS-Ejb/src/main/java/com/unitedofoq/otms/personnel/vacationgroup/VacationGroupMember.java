package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"vacationGroup"})
public class VacationGroupMember extends VacationMemberBase {
    // <editor-fold defaultstate="collapsed" desc="vacationGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private VacationGroup vacationGroup;

    public VacationGroup getVacationGroup() {
        return vacationGroup;
    }

    public void setVacationGroup(VacationGroup vacationGroup) {
        this.vacationGroup = vacationGroup;
    }

    public String getVacationGroupDD() {
        return "VacationGroupMember_vacationGroup";
    }
    // </editor-fold>

}
