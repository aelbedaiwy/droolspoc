package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplateSequence;
import java.util.Date;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class GoalBase extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="weight">
    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getWeightDD() {
        return "GoalBase_weight";

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="valueTo">
    private double valueTo;

    public double getValueTo() {
        return valueTo;
    }

    public void setValueTo(double valueTo) {
        this.valueTo = valueTo;
    }

    public String getValueToDD() {
        return "GoalBase_valueTo";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="valueFrom">
    private double valueFrom;

    public double getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(double valueFrom) {
        this.valueFrom = valueFrom;
    }

    public String getValueFromDD() {
        return "GoalBase_valueFrom";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dueDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date dueDate;

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getDueDateDD() {
        return "GoalBase_dueDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "Goal_name";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nameTranslated">
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "Goal_nameTranslated";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="totalTaken">
    private double totalTaken;

    public double getTotalTaken() {
        return totalTaken;
    }

    public void setTotalTaken(double totalTaken) {
        this.totalTaken = totalTaken;
    }

    public String getTotalTakenDD() {
        return "GoalBase_totalTaken";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sequence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplateSequence sequence;

    public AppraisalTemplateSequence getSequence() {
        return sequence;
    }

    public void setSequence(AppraisalTemplateSequence sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD() {
        return "GoalBase_sequence";
    }
    //</editor-fold>
}
