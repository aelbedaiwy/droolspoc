package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import javax.persistence.*;

@Entity
public class RepTrainingPlan extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepTrainingPlan_dsDbid";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="companyID">
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyIDDD() {
        return "RepTrainingPlan_companyID";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="planDesription">
    @Column
    @Translatable(translationField = "planDescriptionTranslated")
    private String planDescription;

    public void setPlanDesription(String planDescription) {
        this.planDescription = planDescription;
    }

    public String getPlanDescription() {
        return planDescription;
    }

    public String getPlanDescriptionDD() {
        return "RepTrainingPlan_planDescription";
    }
    @Transient
    @Translation(originalField = "planDescription")
    private String planDescriptionTranslated;

    public String getPlanDescriptionTranslated() {
        return planDescriptionTranslated;
    }

    public void setPlanDescriptionTranslated(String planDescriptionTranslated) {
        this.planDescriptionTranslated = planDescriptionTranslated;
    }

    public String getPlanDescriptionTranslatedDD() {
        return "RepTrainingPlan_planDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }

    public String getCourseClassDD() {
        return "RepTrainingPlan_courseClass";
    }
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;

    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }

    public String getCourseClassTranslatedDD() {
        return "RepTrainingPlan_courseClass";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="courseCode">
    @Column
    private String courseCode;

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public String getCourseCodeDD() {
        return "RepTrainingPlan_courseCode";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="courseName">
    @Column
    @Translatable(translationField = "courseNameTranslated")
    private String courseName;

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getCourseNameDD() {
        return "RepTrainingPlan_courseName";
    }
    @Transient
    @Translation(originalField = "courseName")
    private String courseNameTranslated;

    public String getCourseNameTranslated() {
        return courseNameTranslated;
    }

    public void setCourseNameTranslated(String courseNameTranslated) {
        this.courseNameTranslated = courseNameTranslated;
    }

    public String getCourseNameTranslatedDD() {
        return "RepTrainingPlan_courseName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    @Translatable(translationField = "typeTranslated")
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getTypeDD() {
        return "RepTrainingPlan_type";
    }
    @Transient
    @Translation(originalField = "type")
    private String typeTranslated;

    public String getTypeTranslated() {
        return typeTranslated;
    }

    public void setTypeTranslated(String typeTranslated) {
        this.typeTranslated = typeTranslated;
    }

    public String getTypeTranslatedDD() {
        return "RepTrainingPlan_type";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date toDate;

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public String getToDateDD() {
        return "RepTrainingPlan_toDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fromDate;

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public String getFromDateDD() {
        return "RepTrainingPlan_fromDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromTime">
    @Column
    private String fromTime;

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getFromTime() {
        return fromTime;
    }

    public String getFromTimeDD() {
        return "RepTrainingPlan_fromTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toTime">
    @Column
    private String toTime;

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getToTime() {
        return toTime;
    }

    public String getToTimeDD() {
        return "RepTrainingPlan_toTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="numAttendee">
    @Column
    private int numAttendee;

    public void setNumAttendee(int numAttendee) {
        this.numAttendee = numAttendee;
    }

    public int getNumAttendee() {
        return numAttendee;
    }

    public String getNumAttendeeDD() {
        return "RepTrainingPlan_numAttendee";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="planYear">
    private int planYear;

    public int getPlanYear() {
        return planYear;
    }

    public void setPlanYear(int planYear) {
        this.planYear = planYear;
    }

    public String getPlanYearDD() {
        return "RepTrainingPlan_planYear";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="planStatus">
    @Column
    @Translatable(translationField = "planStatusTranslated")
    private String planStatus;

    public String getPlanStatus() {
        return planStatus;
    }

    public void setPlanStatus(String planStatus) {
        this.planStatus = planStatus;
    }

    public String getPlanStatusDD() {
        return "RepTrainingPlan_planStatus";
    }
    @Transient
    @Translation(originalField = "planStatus")
    private String planStatusTranslated;

    public String getPlanStatusTranslated() {
        return planStatusTranslated;
    }

    public void setPlanStatusTranslated(String planStatusTranslated) {
        this.planStatusTranslated = planStatusTranslated;
    }

    public String getPlanStatusTranslatedDD() {
        return "RepTrainingPlan_planStatus";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="provider">
    @Translatable(translationField = "providerTranslated")
    private String provider;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "RepTraningPlan_provider";
    }
    @Transient
    @Translation(originalField = "provider")
    private String providerTranslated;

    public String getProviderTranslated() {
        return providerTranslated;
    }

    public void setProviderTranslated(String providerTranslated) {
        this.providerTranslated = providerTranslated;
    }

    public String getProviderTranslatedDD() {
        return "RepTraningPlan_providerTranslated";
    }
    //</editor-fold >
}