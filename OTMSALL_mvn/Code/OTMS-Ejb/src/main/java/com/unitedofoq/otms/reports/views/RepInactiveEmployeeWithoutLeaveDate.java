/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="repinactempwolvdat")
public class RepInactiveEmployeeWithoutLeaveDate extends RepEmployeeBase {
    //<editor-fold defaultstate="collapsed" desc="dsDid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_genderDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_employeeActive";
    }
    
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_employeeActive";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="leaveDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date leaveDate;

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }
     public String getLeaveDateDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_leaveDate";
    }
 // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="leaveReason">
    @Column
    @Translatable(translationField = "leaveReasonTranslated")
    private String leaveReason;

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }

    public String getLeaveReasonDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_leaveReason";
    }
    @Transient
    @Translation(originalField = "leaveReason")
    private String leaveReasonTranslated;

    public String getLeaveReasonTranslated() {
        return leaveReasonTranslated;
    }

    public void setLeaveReasonTranslated(String leaveReasonTranslated) {
        this.leaveReasonTranslated = leaveReasonTranslated;
    }

    public String getLeaveReasonTranslatedDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_leaveReason";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="terminationDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date terminationDate;

    public Date getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }
     public String getTerminationDateDD() {
        return "RepInactiveEmployeeWithoutLeaveDate_terminationDate";
    }
 // </editor-fold>
}