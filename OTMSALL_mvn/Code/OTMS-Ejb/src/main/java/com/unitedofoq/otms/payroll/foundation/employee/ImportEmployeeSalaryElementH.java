package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

@Entity
public class ImportEmployeeSalaryElementH extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employeeCode">
    private String employeeCode;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "ImportEmployeeSalaryElementH_employeeCode";
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="salaryElementCode">
    private String salaryElementCode;

    public String getSalaryElementCode() {
        return salaryElementCode;
    }

    public String getSalaryElementCodeDD() {
        return "ImportEmployeeSalaryElementH_salaryElementCode";
    }

    public void setSalaryElementCode(String salaryElementCode) {
        this.salaryElementCode = salaryElementCode;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="elementType">
    private String elementType;

    public String getElementType() {
        return elementType;
    }

    public String getElementTypeDD() {
        return "ImportEmployeeSalaryElementH_elementType";
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="salaryElementValue">
    private String salaryElementValue;

    public String getSalaryElementValue() {
        return salaryElementValue;
    }

    public String getSalaryElementValueDD() {
        return "ImportEmployeeSalaryElementH_salaryElementValue";
    }

    public void setSalaryElementValue(String salaryElementValue) {
        this.salaryElementValue = salaryElementValue;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="salaryElementValueEnc">
    private String salaryElementValueEnc;

    public String getSalaryElementValueEnc() {
        return salaryElementValueEnc;
    }

    public void setSalaryElementValueEnc(String salaryElementValueEnc) {
        this.salaryElementValueEnc = salaryElementValueEnc;
    }

    public String getSalaryElementValueEncDD() {
        return "ImportEmployeeSalaryElementH_salaryElementValueEnc";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cpCode">
    private String cpCode;

    public String getCpCode() {
        return cpCode;
    }

    public String getCpCodeDD() {
        return "ImportEmployeeSalaryElementH_cpCode";
    }

    public void setCpCode(String cpCode) {
        this.cpCode = cpCode;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public String getDoneDD() {
        return "ImportEmployeeSalaryElementH_done";
    }

    public void setDone(boolean done) {
        this.done = done;
    }
    // </editor-fold >

}
