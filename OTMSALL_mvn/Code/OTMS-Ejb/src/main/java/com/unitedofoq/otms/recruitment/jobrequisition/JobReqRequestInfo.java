 
package com.unitedofoq.otms.recruitment.jobrequisition;

import com.unitedofoq.fabs.core.validation.DateFromToValidation;
import com.unitedofoq.otms.foundation.baseentities.RequestInfo;
import javax.persistence.Entity;

@Entity
@DateFromToValidation (from={"requestDate"}, to={"approvalDate"})
public class JobReqRequestInfo extends RequestInfo {
}