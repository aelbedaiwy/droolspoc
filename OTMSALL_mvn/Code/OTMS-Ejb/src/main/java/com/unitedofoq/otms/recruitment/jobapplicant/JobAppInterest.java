
package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccuElementGradeBase;
import com.unitedofoq.otms.foundation.occupation.Interest;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobAppInterest extends OccuElementGradeBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
	private JobApplicant jobApplicant;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Interest interest;
	public String getInterestDD(){
        return "JobAppInterest_interest";
    }
	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant theJobApplicant) {
		jobApplicant = theJobApplicant;
	}

	public Interest getInterest() {
		return interest;
	}
	
	public void setInterest(Interest theInterest) {
		interest = theInterest;
	}
}
