
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"calendar"})
@ChildEntity(fields={"dayExcs"})
public class DaysCalendarException extends CalendarExceptionBase  {
    //<editor-fold defaultstate="collapsed" desc="calendar">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private NormalWorkingCalendar calendar;

    public NormalWorkingCalendar getCalendar() {
        return calendar;
    }

    public void setCalendar(NormalWorkingCalendar calendar) {
        this.calendar = calendar;
    }
    
    public String getCalendarDD() {
        return "DaysCalendarException_calendar";
    }
    //</editor-fold>    

    // <editor-fold defaultstate="collapsed" desc="day">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC day;

    public void setDay(UDC day) {
        this.day = day;
    }

    public UDC getDay() {
        return day;
    }

    public String getDayDD() {
        return "DaysCalendarException_day";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dayExcs">
    @OneToMany(mappedBy = "dayException")
    private List<OTDayExc> dayExcs;

    public List<OTDayExc> getDayExcs() {
        return dayExcs;
    }
    public String getDayExcsDD() {
        return "DaysCalendarException_dayExcs";
    }

    public void setDayExcs(List<OTDayExc> dayExcs) {
        this.dayExcs = dayExcs;
    }
    //</editor-fold >
}
