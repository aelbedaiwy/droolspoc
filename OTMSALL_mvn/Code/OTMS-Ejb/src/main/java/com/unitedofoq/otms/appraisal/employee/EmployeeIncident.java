package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tbadran
 */
@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeIncident extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeIncident_employee";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="incidentDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date incidentDate;

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public String getIncidentDateDD() {
        return "EmployeeIncident_incidentDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="incident">
    private String incident;

    public String getIncident() {
        return incident;
    }

    public void setIncident(String incident) {
        this.incident = incident;
    }

    public String getIncidentDD() {
        return "EmployeeIncident_incident";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="participants">
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Employee participant;

    public Employee getParticipant() {
        return participant;
    }

    public void setParticipant(Employee participant) {
        this.participant = participant;
    }

    public String getParticipantDD() {
        return "EmployeeIncident_participant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="content">
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentDD() {
        return "EmployeeIncident_content";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="consequences">
    private String consequences;

    public String getConsequences() {
        return consequences;
    }

    public void setConsequences(String consequences) {
        this.consequences = consequences;
    }

    public String getConsequencesDD() {
        return "EmployeeIncident_consequences";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="decisions">
    private String decisions;

    public String getDecisions() {
        return decisions;
    }

    public void setDecisions(String decisions) {
        this.decisions = decisions;
    }

    public String getDecisionsDD() {
        return "EmployeeIncident_decisions";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "EmployeeIncident_status";
    }
    //</editor-fold>
}
