
package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepPenaltyTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="penalty">
    @Column
    private String penalty;

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getPenalty() {
        return penalty;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="factorOrAmount">
    @Column
    private String factorOrAmount;

    public void setFactorOrAmount(String factorOrAmount) {
        this.factorOrAmount = factorOrAmount;
    }

    public String getFactorOrAmount() {
        return factorOrAmount;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="refreshUnitMeasure">
    @Column
    private String refreshUnitMeasure;

    public void setRefreshUnitMeasure(String refreshUnitMeasure) {
        this.refreshUnitMeasure = refreshUnitMeasure;
    }

    public String getRefreshUnitMeasure() {
        return refreshUnitMeasure;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitMeasure">
    @Column
    private String unitMeasure;

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="refreshMethod">
    @Column
    private String refreshMethod;

    public void setRefreshMethod(String refreshMethod) {
        this.refreshMethod = refreshMethod;
    }

    public String getRefreshMethod() {
        return refreshMethod;
    }
    // </editor-fold>

}
