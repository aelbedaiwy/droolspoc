
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;
import java.math.BigDecimal;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeBill extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeBill_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeBill_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeBill_genderDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="month">
    @Column
    private int month;

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMonth() {
        return month;
    }

    public String getMonthDD() {
        return "RepEmployeeBill_month";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobileAmount">
    @Column
    private BigDecimal mobileAmount;

    public void setMobileAmount(BigDecimal mobileAmount) {
        this.mobileAmount = mobileAmount;
    }

    public BigDecimal getMobileAmount() {
        return mobileAmount;
    }

    public String getMobileAmountDD() {
        return "RepEmployeeBill_mobileAmount";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobileBenifit">
    @Column
    private BigDecimal mobileBenifit;

    public void setMobileBenifit(BigDecimal mobileBenifit) {
        this.mobileBenifit = mobileBenifit;
    }

    public BigDecimal getMobileBenifit() {
        return mobileBenifit;
    }

    public String getMobileBenifitDD() {
        return "RepEmployeeBill_mobileBenifit";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobileDifference">
    @Column
    private BigDecimal mobileDifference;

    public void setMobileDifference(BigDecimal mobileDifference) {
        this.mobileDifference = mobileDifference;
    }

    public BigDecimal getMobileDifference() {
        return mobileDifference;
    }

    public String getMobileDifferenceDD() {
        return "RepEmployeeBill_mobileDifference";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobile1Amount">
    @Column
    private BigDecimal mobile1Amount;

    public void setMobile1Amount(BigDecimal mobile1Amount) {
        this.mobile1Amount = mobile1Amount;
    }

    public BigDecimal getMobile1Amount() {
        return mobile1Amount;
    }

    public String getMobile1AmountDD() {
        return "RepEmployeeBill_mobile1Amount";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobile1Benifit">
    @Column
    private BigDecimal mobile1Benifit;

    public void setMobile1Benifit(BigDecimal mobile1Benifit) {
        this.mobile1Benifit = mobile1Benifit;
    }

    public BigDecimal getMobile1Benifit() {
        return mobile1Benifit;
    }

    public String getMobile1BenifitDD() {
        return "RepEmployeeBill_mobile1Benifit";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobile1Difference">
    @Column
    private BigDecimal mobile1Difference;

    public void setMobile1Difference(BigDecimal mobile1Difference) {
        this.mobile1Difference = mobile1Difference;
    }

    public BigDecimal getMobile1Difference() {
        return mobile1Difference;
    }

    public String getMobile1DifferenceDD() {
        return "RepEmployeeBill_mobile1Difference";
    }
    // </editor-fold>

}