package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.otms.payroll.FilterBase;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class EmployeeSEMaintenance extends FilterBase {
    // <editor-fold defaultstate="collapsed" desc="salaryElement">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public String getSalaryElementDD() {
        return "EmployeeSEAssign_salaryElement";
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="newValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal newValue;

    public BigDecimal getNewValue() {
        return newValue;
    }

    public void setNewValue(BigDecimal newValue) {
        this.newValue = newValue;
    }

    public String getNewValueDD() {
        return "EmployeeSEMaintenance_newValue";
    }
    @Transient
    private BigDecimal newValueMask;

    public BigDecimal getNewValueMask() {
        newValueMask = newValue;
        return newValueMask;
    }

    public void setNewValueMask(BigDecimal newValueMask) {
        updateDecimalValue("newValue", newValueMask);
    }

    public String getNewValueMaskDD() {
        return "EmployeeSEMaintenance_newValueMask";
    }
    // </editor-fold>
}
