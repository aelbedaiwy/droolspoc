package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.otms.payroll.FilterBase;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ManualVacationPosting extends FilterBase {
    // <editor-fold defaultstate="collapsed" desc="vacation">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "ManualVacationPosting_vacation";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="postedYear">
    private String postedYear;

    public String getPostedYear() {
        return postedYear;
    }

    public String getPostedYearDD() {
        return "ManualVacationPosting_postedYear";
    }

    public void setPostedYear(String postedYear) {
        this.postedYear = postedYear;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="secondPosting">
    private boolean secondPosting;

    public boolean isSecondPosting() {
        return secondPosting;
    }

    public String getSecondPostingDD() {
        return "ManualVacationPosting_secondPosting";
    }

    public void setSecondPosting(boolean secondPosting) {
        this.secondPosting = secondPosting;
    }
    // </editor-fold >
}
