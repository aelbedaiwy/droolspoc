
package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class RepGradeSalElementTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="grade">
    @Column
    private String grade;

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="paymentMethod">
    @Column
    private String paymentMethod;

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @Column
    private String salaryElement;

    public void setSalaryElement(String salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElement() {
        return salaryElement;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextGrade">
    @Column
    private String nextGrade;

    public void setNextGrade(String nextGrade) {
        this.nextGrade = nextGrade;
    }

    public String getNextGrade() {
        return nextGrade;
    }
    // </editor-fold>

}
