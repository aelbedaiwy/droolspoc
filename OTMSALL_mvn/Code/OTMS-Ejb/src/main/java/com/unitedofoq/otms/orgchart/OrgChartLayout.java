package com.unitedofoq.otms.orgchart;

public class OrgChartLayout {
    /*
     * represents the current object DBID
     */

    private long id;

    /*
     * represents the current object parent DBID
     */
    private long parentID;

    /*
     * represents what to be displayed in minimized mode
     */
    private String minimizedTitle;
    /*
     * represents what to be displayed in normal mode
     */
    private String normalTitle;

    public String getMinimizedTitle() {
        return minimizedTitle;
    }

    public void setMinimizedTitle(String minimizedTitle) {
        this.minimizedTitle = minimizedTitle;
    }

    public String getNormalTitle() {
        return normalTitle;
    }

    public void setNormalTitle(String normalTitle) {
        this.normalTitle = normalTitle;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParentID() {
        return parentID;
    }

    public void setParentID(long parentID) {
        this.parentID = parentID;
    }
}
