/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.occupation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

/**
 *
 * @author abdullahm
 */
@MappedSuperclass
public class Education extends BaseEntity{

    @Column(nullable= false)
    @Temporal(javax.persistence.TemporalType.DATE)
    protected Date fromDate;

    public UDC getFaculty() {
        return faculty;
    }

    public void setFaculty(UDC faculty) {
        this.faculty = faculty;
    }

    public UDC getUniversity() {
        return university;
    }

    public void setUniversity(UDC university) {
        this.university = university;
    }
    public String getFromDateDD(){
        return "Education_fromDate";
    }
    @Column(nullable= false)
    protected String grade;
    public String getGradeDD(){
        return "Education_grade";
    }
    protected String major;
    public String getMajorDD(){
        return "Education_major";
    }
    @Column(nullable= false)
    @Temporal(javax.persistence.TemporalType.DATE)
    protected Date toDate;
    public String getToDateDD(){
        return "Education_toDate";
    }
    //attributes from direct asso
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    protected UDC degree;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    protected UDC faculty;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected UDC university;

    public UDC getDegree() {
        return degree;
    }

    public void setDegree(UDC degree) {
        this.degree = degree;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}