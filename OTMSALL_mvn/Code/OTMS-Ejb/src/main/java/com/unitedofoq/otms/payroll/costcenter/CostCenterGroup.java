package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import javax.persistence.*;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.List;

/**
 * 
 */
@Entity
@ParentEntity(fields="company")
public class CostCenterGroup extends BusinessObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="code">
	@Column
	private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "CostCenterGroup_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "CostCenterGroup_description";
    }
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="company">
	
	@JoinColumn(nullable=false)
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "CostCenterGroup_company";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenters">
    @OneToMany(mappedBy="costCenterGroup")
    private List<CostCenter> costCenters ;
    public List<CostCenter> getCostCenters() {
        return costCenters;
    }
    public void setCostCenters(List<CostCenter> costCenters) {
        this.costCenters = costCenters;
    }
    public String getCostCentersDD() {
        return "CostCenterGroup_costCenters";
    }
    // </editor-fold>
}