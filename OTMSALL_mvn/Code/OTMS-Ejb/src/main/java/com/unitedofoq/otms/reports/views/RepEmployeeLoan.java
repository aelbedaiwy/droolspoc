/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author arezk
 */
@Table(name = "RepEmployeeLoan")
@Entity
@ReadOnly
public class RepEmployeeLoan extends RepEmployeeBase {

    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }
    //</editor-fold>
    // EmployeeLoan

    // <editor-fold defaultstate="collapsed" desc="loanDescription">
    @Translatable(translationField = "loanDescriptionTranslated")
    private String loanDescription;

    public String getLoanDescription() {
        return loanDescription;
    }

    public void setLoanDescription(String loanDescription) {
        this.loanDescription = loanDescription;
    }

    public String getLoanDescriptionDD() {
        return "RepEmployeeLoan_loanDescription";
    }

    @Transient
    @Translation(originalField = "loanDescription")
    private String loanDescriptionTranslated;

    public String getLoanDescriptionTranslated() {
        return loanDescriptionTranslated;
    }

    public void setLoanDescriptionTranslated(String loanDescriptionTranslated) {
        this.loanDescriptionTranslated = loanDescriptionTranslated;
    }

    public String getLoanDescriptionTranslatedDD() {
        return "RepEmployeeLoan_loanDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvedByName">
    @Translatable(translationField = "approvedByNameTranslated")
    private String approvedByName;

    public String getApprovedByName() {
        return approvedByName;
    }

    public void setApprovedByName(String approvedByName) {
        this.approvedByName = approvedByName;
    }

    public String getApprovedByDD() {
        return "RepEmployeeLoan_approvedByName";
    }

    @Transient
    @Translation(originalField = "approvedByName")
    private String approvedByNameTranslated;

    public String getApprovedByNameTranslated() {
        return approvedByNameTranslated;
    }

    public void setApprovedByNameTranslated(String approvedByNameTranslated) {
        this.approvedByNameTranslated = approvedByNameTranslated;
    }

    public String getApprovedByNameTranslatedDD() {
        return "RepEmployeeLoan_approvedByName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startYear">
    private Integer startYear;

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public String getStartYearDD() {
        return "RepEmployeeLoan_startYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startMonth">
    private Integer startMonth;

    public Integer getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartMonthDD() {
        return "RepEmployeeLoan_startMonth";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="installmentNumber">
    private Integer installmentNumber;

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public String getInstallmentNumberDD() {
        return "RepEmployeeLoan_installmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal monthValue;

    public BigDecimal getMonthValue() {
        return monthValue;
    }

    public void setMonthValue(BigDecimal monthValue) {
        this.monthValue = monthValue;
    }

    public String getMonthValueDD() {
        return "RepEmployeeLoan_monthValue";
    }
//    @Transient
//    private BigDecimal monthValueMask;
//
//    public BigDecimal getMonthValueMask() {
//        monthValueMask = monthValue;
//        return monthValueMask;
//    }
//
//    public void setMonthValueMask(BigDecimal monthValueMask) {
//        updateDecimalValue("monthValue", monthValueMask);
//    }
//
//    public String getMonthValueMaskDD() {
//        return "RepEmployeeLoan_monthValueMask";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="suspendedToMonth">
    private Integer suspendedToMonth;

    public Integer getSuspendedToMonth() {
        return suspendedToMonth;
    }

    public void setSuspendedToMonth(Integer suspendedToMonth) {
        this.suspendedToMonth = suspendedToMonth;
    }

    public String getSuspendedToMonthDD() {
        return "RepEmployeeLoan_suspendedToMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="suspendedToYear">
    private Integer suspendedToYear;

    public Integer getSuspendedToYear() {
        return suspendedToYear;
    }

    public void setSuspendedToYear(Integer suspendedToYear) {
        this.suspendedToYear = suspendedToYear;
    }

    public String getSuspendedToYearDD() {
        return "RepEmployeeLoan_suspendedToYear";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="comment">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "RepEmployeeLoan_comments";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValueEnc">

    @Column

    private String monthValueEnc;

    public String getMonthValueEnc() {
        return monthValueEnc;
    }

    public void setMonthValueEnc(String monthValueEnc) {
        this.monthValueEnc = monthValueEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal loanValue;

    public BigDecimal getLoanValue() {
        return loanValue;
    }

    public void setLoanValue(BigDecimal loanValue) {
        this.loanValue = loanValue;
    }

    public String getLoanValueDD() {
        return "RepEmployeeLoan_loanValue";
    }
//    @Transient
//    private BigDecimal loanValueMask;
//
//    public BigDecimal getLoanValueMask() {
//        loanValueMask = loanValue;
//        return loanValueMask;
//    }
//
//    public void setLoanValueMask(BigDecimal loanValueMask) {
//        updateDecimalValue("loanValue", loanValueMask);
//    }
//
//    public String getLoanValueMaskDD() {
//        return "RepEmployeeLoan_loanValueMask";
//    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="loanValueEnc">
    @Column
    private String loanValueEnc;

    public String getLoanValueEnc() {
        return loanValueEnc;
    }

    public void setLoanValueEnc(String loanValueEnc) {
        this.loanValueEnc = loanValueEnc;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="status">
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "RepEmployeeLoan_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal paidAmount;

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaidAmountDD() {
        return "RepEmployeeLoan_paidAmount";
    }
//    @Transient
//    private BigDecimal paidAmountMask;
//
//    public BigDecimal getPaidAmountMask() {
//        paidAmountMask = paidAmount;
//        return paidAmountMask;
//    }
//
//    public void setPaidAmountMask(BigDecimal paidAmountMask) {
//        updateDecimalValue("paidAmount", paidAmountMask);
//    }
//
//    public String getPaidAmountMaskDD() {
//        return "RepEmployeeLoan_paidAmountMask";
//    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="paidAmountEnc">
    @Column

    private String paidAmountEnc;

    public String getPaidAmountEnc() {
        return paidAmountEnc;
    }

    public void setPaidAmountEnc(String paidAmountEnc) {
        this.paidAmountEnc = paidAmountEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidInstallment">
    @Column
    private Integer paidInstallment;

    public Integer getPaidInstallment() {
        return paidInstallment;
    }

    public void setPaidInstallment(Integer paidInstallment) {
        this.paidInstallment = paidInstallment;
    }

    public String getPaidInstallmentDD() {
        return "RepEmployeeLoan_paidInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="lastMonth">
    private Integer lastMonth;

    public Integer getLastMonth() {
        return lastMonth;
    }

    public void setLastMonth(Integer lastMonth) {
        this.lastMonth = lastMonth;
    }

    public String getLastMonthDD() {
        return "RepEmployeeLoan_lastMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastYear">
    private Integer lastYear;

    public Integer getLastYear() {
        return lastYear;
    }

    public void setLastYear(Integer lastYear) {
        this.lastYear = lastYear;
    }

    public String getLastYearDD() {
        return "RepEmployeeLoan_lastYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastPaidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal lastPaidAmount;

    public BigDecimal getLastPaidAmount() {
        return lastPaidAmount;
    }

    public void setLastPaidAmount(BigDecimal lastPaidAmount) {
        this.lastPaidAmount = lastPaidAmount;
    }

    public String getLastPaidAmountDD() {
        return "RepEmployeeLoan_lastPaidAmount";
    }
//    @Transient
//    private BigDecimal lastPaidAmountMask;
//
//    public BigDecimal getLastPaidAmountMask() {
//        lastPaidAmountMask = lastPaidAmount;
//        return lastPaidAmountMask;
//    }
//
//    public void setLastPaidAmountMask(BigDecimal lastPaidAmountMask) {
//        updateDecimalValue("lastPaidAmount", lastPaidAmountMask);
//    }
//
//    public String getLastPaidAmountMaskDD() {
//        return "RepEmployeeLoan_lastPaidAmountMask";
//    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="lastPaidAmountEnc">
    @Column

    private String lastPaidAmountEnc;

    public String getLastPaidAmountEnc() {
        return lastPaidAmountEnc;
    }

    public void setLastPaidAmountEnc(String lastPaidAmountEnc) {
        this.lastPaidAmountEnc = lastPaidAmountEnc;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="loanOrInstallment">
    @Column(length = 1)
    private String loanOrInstallment = "L";

    public String getLoanOrInstallment() {
        return loanOrInstallment;
    }

    public void setLoanOrInstallment(String loanOrInstallment) {
        this.loanOrInstallment = loanOrInstallment;
    }

    public String getLoanOrInstallmentDD() {
        return "RepEmployeeLoan_loanOrInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="deducedMonth">
    private Integer deducedMonth;

    public Integer getDeducedMonth() {
        return deducedMonth;
    }

    public void setDeducedMonth(Integer deducedMonth) {
        this.deducedMonth = deducedMonth;
    }

    public String getDeducedMonthDD() {
        return "RepEmployeeLoan_deducedMonth";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="deducedYear">
    private Integer deducedYear;

    public Integer getDeducedYear() {
        return deducedYear;
    }

    public void setDeducedYear(Integer deducedYear) {
        this.deducedYear = deducedYear;
    }

    public String getDeducedYearDD() {
        return "RepEmployeeLoan_deducedYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deducedPercentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal deducedPercentage;

    public BigDecimal getDeducedPercentage() {
        return deducedPercentage;
    }

    public void setDeducedPercentage(BigDecimal deducedPercentage) {
        this.deducedPercentage = deducedPercentage;
    }

    public String getDeducedPercentageDD() {
        return "RepEmployeeLoan_deducedPercentage";
    }
//    @Transient
//    private BigDecimal deducedPercentageMask;
//
//    public BigDecimal getDeducedPercentageMask() {
//        deducedPercentageMask = deducedPercentage;
//        return deducedPercentageMask;
//    }
//
//    public void setDeducedPercentageMask(BigDecimal deducedPercentageMask) {
//        updateDecimalValue("deducedPercentage", deducedPercentageMask);
//    }
//
//    public String getDeducedPercentageMaskDD() {
//        return "RepEmployeeLoan_deducedPercentageMask";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fixedInstallmentsNo">
    @Column(precision = 25, scale = 13)
    private BigDecimal fixedInstallmentsNo;

    public BigDecimal getFixedInstallmentsNo() {
        return fixedInstallmentsNo;
    }

    public void setFixedInstallmentsNo(BigDecimal fixedInstallmentsNo) {
        this.fixedInstallmentsNo = fixedInstallmentsNo;
    }

    public String getFixedInstallmentsNoDD() {
        return "RepEmployeeLoan_fixedInstallmentsNo";
    }
//    @Transient
//    private BigDecimal fixedInstallmentsNoMask;
//
//    public BigDecimal getFixedInstallmentsNoMask() {
//        fixedInstallmentsNoMask = fixedInstallmentsNo;
//        return fixedInstallmentsNoMask;
//    }
//
//    public void setFixedInstallmentsNoMask(BigDecimal fixedInstallmentsNoMask) {
//        updateDecimalValue("fixedInstallmentsNo", fixedInstallmentsNoMask);
//    }
//
//    public String getFixedInstallmentsNoMaskDD() {
//        return "RepEmployeeLoan_fixedInstallmentsNoMask";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "RepEmployeeLoan_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "RepEmployeeLoan_maximumValue";
    }
//    @Transient
//    private BigDecimal maximumValueMask;
//
//    public BigDecimal getMaximumValueMask() {
//        maximumValueMask = maximumValue;
//        return maximumValueMask;
//    }
//
//    public void setMaximumValueMask(BigDecimal maximumValueMask) {
//        updateDecimalValue("maximumValue", maximumValueMask);
//    }
//
//    public String getMaximumValueMaskDD() {
//        return "RepEmployeeLoan_maximumValueMask";
//    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="minimumValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal minimumValue;

    public BigDecimal getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(BigDecimal minimumValue) {
        this.minimumValue = minimumValue;
    }

    public String getMinimumValueDD() {
        return "RepEmployeeLoan_minimumValue";
    }

//    @Transient
//    private BigDecimal minimumValueMask;
//
//    public BigDecimal getMinimumValueMask() {
//        minimumValueMask = minimumValue;
//        return minimumValueMask;
//    }
//
//    public void setMinimumValueMask(BigDecimal minimumValueMask) {
//        updateDecimalValue("minimumValue", minimumValueMask);
//    }
//
//    public String getMinimumValueMaskDD() {
//        return "RepEmployeeLoan_minimumValueMask";
//    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="settInstallmentNumber">
    private Integer settInstallmentNumber;

    public Integer getSettInstallmentNumber() {
        return settInstallmentNumber;
    }

    public void setSettInstallmentNumber(Integer settInstallmentNumber) {
        this.settInstallmentNumber = settInstallmentNumber;
    }

    public String getSettInstallmentNumberDD() {
        return "RepEmployeeLoan_settInstallmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="settLoanValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal settLoanValue;

    public BigDecimal getSettLoanValue() {
        return settLoanValue;
    }

    public void setSettLoanValue(BigDecimal settLoanValue) {
        this.settLoanValue = settLoanValue;
    }

    public String getSettLoanValueDD() {
        return "RepEmployeeLoan_settLoanValue";
    }

//    @Transient
//    private BigDecimal settLoanValueMask;
//
//    public BigDecimal getSettLoanValueMask() {
//        settLoanValueMask = settLoanValue;
//        return settLoanValueMask;
//    }
//
//    public void setSettLoanValueMask(BigDecimal settLoanValueMask) {
//        updateDecimalValue("settLoanValue", settLoanValueMask);
//    }
//
//    public String getSettLoanValueMaskDD() {
//        return "RepEmployeeLoan_settLoanValueMask";
//    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="settDeductionDescription">
    @Translatable(translationField = "settDeductionDescriptionTranslated")
    private String settDeductionDescription;

    public String getSettDeductionDescription() {
        return settDeductionDescription;
    }

    public void setSettDeductionDescription(String settDeductionDescription) {
        this.settDeductionDescription = settDeductionDescription;
    }

    public String getSettDeductionDD() {
        return "RepEmployeeLoan_settDeductionDescription";
    }

    @Transient
    @Translation(originalField = "settDeductionDescription")
    private String settDeductionDescriptionTranslated;

    public String getSettDeductionDescriptionTranslated() {
        return settDeductionDescriptionTranslated;
    }

    public void setSettDeductionDescriptionTranslated(String settDeductionDescriptionTranslated) {
        this.settDeductionDescriptionTranslated = settDeductionDescriptionTranslated;
    }

    public String getSettDeductionDescriptionTranslatedDD() {
        return "RepEmployeeLoan_settDeductionDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="equallyDistributed">
    @Column(length = 1)
    private String equallyDistributed = "Y";

    public String getEquallyDistributed() {
        return equallyDistributed;
    }

    public void setEquallyDistributed(String equallyDistributed) {
        this.equallyDistributed = equallyDistributed;
    }

    public String getEquallyDistributedDD() {
        return "RepEmployeeLoan_equallyDistributed";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="remainingLoanValue">
    private BigDecimal remainingLoanValue;

    public BigDecimal getRemainingLoanValue() {
        return remainingLoanValue;
    }

    public void setRemainingLoanValue(BigDecimal remainingLoanValue) {
        this.remainingLoanValue = remainingLoanValue;
    }

    public String getRemainingLoanValueDD() {
        return "RepEmployeeLoan_remainingLoanValue";
    }
//    @Transient
//    private BigDecimal remainingLoanValueMask;
//
//    public BigDecimal getRemainingLoanValueMask() {
//        remainingLoanValueMask = remainingLoanValue;
//        return remainingLoanValueMask;
//    }
//
//    public void setRemainingLoanValueMask(BigDecimal remainingLoanValueMask) {
//        updateDecimalValue("remainingLoanValue", remainingLoanValueMask);
//    }
//
//    public String getRemainingLoanValueMaskDD() {
//        return "RepEmployeeLoan_remainingLoanValueMask";
//    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="orgMonthValue">
    @Transient
    private BigDecimal orgMonthValue;

    public BigDecimal getOrgMonthValue() {
        orgMonthValue = monthValue;
        return orgMonthValue;
    }

    public void setOrgMonthValue(BigDecimal orgMonthValue) {
        this.orgMonthValue = orgMonthValue;
    }

    public String getOrgMonthValueDD() {
        return "RepEmployeeLoan_orgMonthValue";
    }
//    @Transient
//    private BigDecimal orgMonthValueMask;
//
//    public BigDecimal getOrgMonthValueMask() {
//        orgMonthValueMask = orgMonthValue;
//        return orgMonthValueMask;
//    }
//
//    public void setOrgMonthValueMask(BigDecimal orgMonthValueMask) {
//        updateDecimalValue("orgMonthValue", orgMonthValueMask);
//    }
//
//    public String getOrgMonthValueMaskDD() {
//        return "RepEmployeeLoan_orgMonthValueMask";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="remainingIns">
    private Integer remainingIns;

    public Integer getRemainingIns() {
        return remainingIns;
    }

    public void setRemainingIns(Integer remainingIns) {
        this.remainingIns = remainingIns;
    }

    public String getRemainingInsDD() {
        return "RepEmployeeLoan_remainingIns";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="orgInstallment">
    @Transient
    private Integer orgInstallment;

    public Integer getOrgInstallment() {
        orgInstallment = installmentNumber;
        return orgInstallment;
    }

    public void setOrgInstallment(Integer orgInstallment) {
        this.orgInstallment = orgInstallment;
    }

    public String getOrgInstallmentDD() {
        return "RepEmployeeLoan_orgInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="cancelledByDescription">
    @Translatable(translationField = "cancelledByNameTranslated")
    private String cancelledByName;

    public String getCancelledByName() {
        return cancelledByName;
    }

    public void setCancelledByName(String cancelledByDescription) {
        this.cancelledByName = cancelledByDescription;
    }

    public String getCancelledByNameDD() {
        return "RepEmployeeLoan_cancelledByName";
    }

    @Transient
    @Translation(originalField = "cancelledByName")
    private String cancelledByNameTranslated;

    public String getCancelledByNameTranslated() {
        return cancelledByNameTranslated;
    }

    public void setCancelledByNameTranslated(String cancelledByNameTranslated) {
        this.cancelledByNameTranslated = cancelledByNameTranslated;
    }

    public String getCancelledByNameTranslatedDD() {
        return "RepEmployeeLoan_cancelledByName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledComment">
    private String cancelledComment;

    public String getCancelledComment() {
        return cancelledComment;
    }

    public void setCancelledComment(String cancelledComment) {
        this.cancelledComment = cancelledComment;
    }

    public String getCancelledCommentDD() {
        return "RepEmployeeLoan_cancelledComment";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelledDate;

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getCancelledDateDD() {
        return "RepEmployeeLoan_cancelledDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="suspendOrCancel">
    private String suspendOrCancel;

    public String getSuspendOrCancel() {
        return suspendOrCancel;
    }

    public void setSuspendOrCancel(String suspendOrCancel) {
        this.suspendOrCancel = suspendOrCancel;
    }

    public String getSuspendOrCancelDD() {
        return "RepEmployeeLoan_suspendOrCancel";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="resInstallmentNumber">
    private Integer resInstallmentNumber;

    public Integer getResInstallmentNumber() {
        return resInstallmentNumber;
    }

    public void setResInstallmentNumber(Integer resInstallmentNumber) {
        this.resInstallmentNumber = resInstallmentNumber;
    }

    public String getResInstallmentNumberDD() {
        return "RepEmployeeLoan_resInstallmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="resMonthValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal resMonthValue;

    public BigDecimal getResMonthValue() {
        return resMonthValue;
    }

    public void setResMonthValue(BigDecimal resMonthValue) {
        this.resMonthValue = resMonthValue;
    }

    public String getResMonthValueDD() {
        return "RepEmployeeLoan_resMonthValue";
    }
//    @Transient
//    private BigDecimal resMonthValueMask;
//
//    public BigDecimal getResMonthValueMask() {
//        resMonthValueMask = resMonthValue;
//        return resMonthValueMask;
//    }
//
//    public void setResMonthValueMask(BigDecimal resMonthValueMask) {
//        updateDecimalValue("resMonthValue", resMonthValueMask);
//    }
//
//    public String getResMonthValueMaskDD() {
//        return "RepEmployeeLoan_resMonthValueMask";
//    }
    // </editor-fold>
}
