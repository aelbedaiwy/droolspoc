package com.unitedofoq.otms.recruitment.onlineexam;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@ChildEntity(fields = {"answers"})
@ParentEntity(fields = {"exam"})
public class MCQuestion extends QuestionBase {

    // <editor-fold defaultstate="collapsed" desc="answers">
    @OneToMany(mappedBy = "question")
    private List<Answer> answers;

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
    // </editor-fold>
}
