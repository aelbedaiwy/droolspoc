package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 * The persistent class for the legal_entity database table.
 *
 */
@Entity
@ParentEntity(fields = {"company"})
public class LegalEntity extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="description">

    @Column(nullable = false, unique = true)
    @Translatable(translationField = "descriptionTranslated")
    private String description;
    @Translation(originalField = "description")
    @Transient
    private String descriptionTranslated;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "LegalEntity_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="address">
    @Column
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDD() {
        return "LegalEntity_address";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fax">
    @Column
    private String fax;

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFaxDD() {
        return "LegalEntity_fax";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="number">
    @Column(name = "entityNumber")
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumberDD() {
        return "LegalEntity_number";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="telephone">
    @Column
    private String telephone;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephoneDD() {
        return "LegalEntity_telephone";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "LegalEntity_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="localCurrency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Currency localCurrency;

    public Currency getLocalCurrency() {
        return localCurrency;
    }

    public void setLocalCurrency(Currency localCurrency) {
        this.localCurrency = localCurrency;
    }

    public String getLocalCurrencyDD() {
        return "LegalEntity_localCurrency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="shortDescription">
    @Column
    @Translatable(translationField = "shortDescriptionTranslated")
    private String shortDescription;
    @Transient
    @Translation(originalField = "shortDescription")
    private String shortDescriptionTranslated;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescriptionTranslatedDD() {
        return "LegalEntity_shortDescriptionTranslated";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getShortDescriptionTranslated() {
        return shortDescriptionTranslated;
    }

    public void setShortDescriptionTranslated(String shortDescriptionTranslated) {
        this.shortDescriptionTranslated = shortDescriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "LegalEntity_company";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="glAccount">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }

    public String getGlAccountDD() {
        return "LegalEntity_glAccount";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "LegalEntity_name";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="owner">
    @Column
    private String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerDD() {
        return "LegalEntity_owner";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalView">
    @Column
    private String legalView;

    public String getLegalView() {
        return legalView;
    }

    public void setLegalView(String legalView) {
        this.legalView = legalView;
    }

    public String getLegalViewDD() {
        return "LegalEntity_legalView";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyNumber">
    @Column
    private String companyNumber;

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    public String getCompanyNumberDD() {
        return "LegalEntity_companyNumber";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "LegalEntity_legalEntityGroup";
    }
    //</editor-fold>

    @Column
    private BigDecimal maxWeekendOT;

    public void setMaxWeekendOT(BigDecimal maxWeekendOT) {
        this.maxWeekendOT = maxWeekendOT;
    }

    public BigDecimal getMaxWeekendOT() {
        return maxWeekendOT;
    }

    public String getMaxWeekendOTDD() {
        return "LegalEntity_maxWeekendOT";
    }
}
