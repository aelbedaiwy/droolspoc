/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields = {"employee"})
public class VacationSettlement extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate; //(name = "request_date")

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDateDD() {
        return "VacationSettlement_requestDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="originalVacation">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    
    private Vacation originalVacation;//name = "original_vacation"

    public Vacation getOriginalVacation() {
        return originalVacation;
    }

    public void setOriginalVacation(Vacation originalVacation) {
        this.originalVacation = originalVacation;
    }

    public String getOriginalVacationDD() {
        return "VacationSettlement_originalVacation";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="originalDays">
    @Column(precision = 18, scale = 3)
    private BigDecimal originalDays;//(name = "original_days")

    public BigDecimal getOriginalDays() {
        return originalDays;
    }

    public void setOriginalDays(BigDecimal originalDays) {
        this.originalDays = originalDays;
    }

    public String getOriginalDaysDD() {
        return "VacationSettlement_originalDays";
    }
    @Transient
    private BigDecimal originalDaysMask;

    public BigDecimal getOriginalDaysMask() {
        originalDaysMask = originalDays;
        return originalDaysMask;
    }

    public void setOriginalDaysMask(BigDecimal originalDaysMask) {
        updateDecimalValue("originalDays", originalDaysMask);
    }

    public String getOriginalDaysMaskDD() {
        return "VacationSettlement_originalDaysMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="target">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation target;//(name = "target_vacation")

    public Vacation getTarget() {
        return target;
    }

    public void setTarget(Vacation target) {
        this.target = target;
    }

    public String getTargetDD() {
        return "VacationSettlement_target";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="targetDays">
    @Column(precision = 18, scale = 3)
    private BigDecimal targetDays;//(name = "target_days")

    public BigDecimal getTargetDays() {
        return targetDays;
    }

    public void setTargetDays(BigDecimal targetDays) {
        this.targetDays = targetDays;
    }

    public String getTargetDaysDD() {
        return "VacationSettlement_targetDays";
    }
    @Transient
    private BigDecimal targetDaysMask;

    public BigDecimal getTargetDaysMask() {
        targetDaysMask = targetDays;
        return targetDaysMask;
    }

    public void setTargetDaysMask(BigDecimal targetDaysMask) {
        updateDecimalValue("targetDays", targetDaysMask);
    }

    public String getTargetDaysMaskDD() {
        return "VacationSettlement_targetDaysMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryelement">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryelement;//(name = "Salary_element")

    public SalaryElement getSalaryelement() {
        return salaryelement;
    }

    public void setSalaryelement(SalaryElement salaryelement) {
        this.salaryelement = salaryelement;
    }

    public String getSalaryelementDD() {
        return "VacationSettlement_salaryelement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="value">
    @Column(precision = 18, scale = 3)
    private BigDecimal value;//(name = "vacation_value")

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getValueDD() {
        return "VacationSettlement_value";
    }
    @Transient
    private BigDecimal valueMask;

    public BigDecimal getValueMask() {
        valueMask = value;
        return valueMask;
    }

    public void setValueMask(BigDecimal valueMask) {
        updateDecimalValue("value", valueMask);
    }

    public String getValueMaskDD() {
        return "VacationSettlement_valueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "VacationSettlement_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="originalTransaction">
    @Column
    private BigDecimal originalTransaction;//(name = "original_transaction_no")

    public BigDecimal getOriginalTransaction() {
        return originalTransaction;
    }

    public void setOriginalTransaction(BigDecimal originalTransaction) {
        this.originalTransaction = originalTransaction;
    }

    public String getOriginalTransactionDD() {
        return "VacationSettlement_originalTransaction";
    }
    @Transient
    private BigDecimal originalTransactionMask;

    public BigDecimal getOriginalTransactionMask() {
        originalTransactionMask = originalTransaction;
        return originalTransactionMask;
    }

    public void setOriginalTransactionMask(BigDecimal originalTransactionMask) {
        updateDecimalValue("originalTransaction", originalTransactionMask);
    }

    public String getOriginalTransactionMaskDD() {
        return "VacationSettlement_originalTransactionMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="targetTransactionNo">
    @Column
    private BigDecimal targetTransactionNo;//(name = "target_transaction_no")

    public BigDecimal getTargetTransactionNo() {
        return targetTransactionNo;
    }

    public void setTargetTransactionNo(BigDecimal targetTransactionNo) {
        this.targetTransactionNo = targetTransactionNo;
    }

    public String getTargetTransactionNoDD() {
        return "VacationSettlement_targetTransactionNo";
    }
    @Transient
    private BigDecimal targetTransactionNoMask;

    public BigDecimal getTargetTransactionNoMask() {
        targetTransactionNoMask = targetTransactionNo;
        return targetTransactionNoMask;
    }

    public void setTargetTransactionNoMask(BigDecimal targetTransactionNoMask) {
        updateDecimalValue("targetTransactionNoMask", targetTransactionNoMask);
    }

    public String getTargetTransactionNoMaskDD() {
        return "VacationSettlement_targetTransactionNoMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    private String payrollMonthly;//(name = "payroll_monthly")

    public String getPayrollMonthly() {
        return payrollMonthly;
    }

    public void setPayrollMonthly(String payrollMonthly) {
        this.payrollMonthly = payrollMonthly;
    }

    public String getPayrollMonthlyDD() {
        return "VacationSettlement_payrollMonthly";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="originalBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal originalBalance;//(name = "original_balance")

    public BigDecimal getOriginalBalance() {
        return originalBalance;
    }

    public void setOriginalBalance(BigDecimal originalBalance) {
        this.originalBalance = originalBalance;
    }

    public String getOriginalBalanceDD() {
        return "VacationSettlement_originalBalance";
    }
    @Transient
    private BigDecimal originalBalanceMask;

    public BigDecimal getOriginalBalanceMask() {
        originalBalanceMask = originalBalance;
        return originalBalanceMask;
    }

    public void setOriginalBalanceMask(BigDecimal originalBalanceMask) {
        updateDecimalValue("originalBalance", originalBalanceMask);
    }

    public String getOriginalBalanceMaskDD() {
        return "VacationSettlement_originalBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="targetBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal targetBalance;//(name = "target_balance")

    public BigDecimal getTargetBalance() {
        return targetBalance;
    }

    public void setTargetBalance(BigDecimal targetBalance) {
        this.targetBalance = targetBalance;
    }

    public String getTargetBalanceDD() {
        return "VacationSettlement_targetBalance";
    }
    @Transient
    private BigDecimal targetBalanceMask;

    public BigDecimal getTargetBalanceMask() {
        targetBalanceMask = targetBalance;
        return targetBalanceMask;
    }

    public void setTargetBalanceMask(BigDecimal targetBalanceMask) {
        updateDecimalValue("targetBalance", targetBalanceMask);
    }

    public String getTargetBalanceMaskDD() {
        return "VacationSettlement_targetBalanceMask";
    }
// </editor-fold>
    //<editor-fold defaultstate="collapse" desc="income">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }

    public String getIncomeDD() {
        return "VacationSettlement_income";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="deduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }

    public String getDeductionDD() {
        return "VacationSettlement_deduction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "VacationSettlement_done";
    }
    //</editor-fold>
}
