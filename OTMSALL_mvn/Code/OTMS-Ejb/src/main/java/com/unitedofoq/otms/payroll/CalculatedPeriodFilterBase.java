package com.unitedofoq.otms.payroll;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class CalculatedPeriodFilterBase extends FilterBase {

    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }

    public String getCalculatedPeriodDD() {
        return "CalculatedPeriodFilterBase_calculatedPeriod";
    }
    // </editor-fold >
}
