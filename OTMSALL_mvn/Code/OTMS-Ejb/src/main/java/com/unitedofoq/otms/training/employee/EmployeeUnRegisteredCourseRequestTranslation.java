/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author lap2
 */
@Entity
@Table(name= "empunregcoureqtranslation")
public class EmployeeUnRegisteredCourseRequestTranslation extends BaseEntityTranslation{
    //<editor-fold defaultstate="collapsed" desc="courseTitle">
    private String courseTitle;

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="providerName">
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
    //</editor-fold>
}