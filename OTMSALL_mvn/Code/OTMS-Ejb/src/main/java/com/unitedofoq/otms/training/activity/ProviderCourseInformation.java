
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.*;
import java.math.BigDecimal;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.training.employee.EmployeeCourseRequest;

@Entity
public class ProviderCourseInformation extends BaseEntity  {

    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD() {
        return "ProviderCourseInformation_code";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "ProviderCourseInformation_startDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getEndDateDD() {
        return "ProviderCourseInformation_endDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseBranch">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit courseBranch;

    public void setCourseBranch(Unit courseBranch) {
        this.courseBranch = courseBranch;
    }

    public Unit getCourseBranch() {
        return courseBranch;
    }

    public String getCourseBranchDD() {
        return "ProviderCourseInformation_courseBranch";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseAddress">
    @Column
    private String courseAddress;

    public void setCourseAddress(String courseAddress) {
        this.courseAddress = courseAddress;
    }

    public String getCourseAddress() {
        return courseAddress;
    }

    public String getCourseAddressDD() {
        return "ProviderCourseInformation_courseAddress";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="instructorName">
    @Column
    @Translatable(translationField = "instructorNameTranslated")
    private String instructorName;

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public String getInstructorNameDD() {
        return "ProviderCourseInformation_instructorName";
    }    
    @Transient
    @Translation(originalField = "instructorName")
    private String instructorNameTranslated;

    public String getInstructorNameTranslated() {
        return instructorNameTranslated;
    }

    public void setInstructorNameTranslated(String instructorNameTranslated) {
        this.instructorNameTranslated = instructorNameTranslated;
    }
    
    public String getInstructorNameTranslatedDD() {
        return "ProviderCourseInformation_instructorName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="instructor">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee instructor;

    public void setInstructor(Employee instructor) {
        this.instructor = instructor;
    }

    public Employee getInstructor() {
        return instructor;
    }

    public String getInstructorDD() {
        return "ProviderCourseInformation_instructor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="daysPerWeek">
    @Column
    private BigDecimal daysPerWeek;

    public void setDaysPerWeek(BigDecimal daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public BigDecimal getDaysPerWeek() {
        return daysPerWeek;
    }

    public String getDaysPerWeekDD() {
        return "ProviderCourseInformation_daysPerWeek";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hoursPerSession">
    @Column
    private BigDecimal hoursPerSession;

    public void setHoursPerSession(BigDecimal hoursPerSession) {
        this.hoursPerSession = hoursPerSession;
    }

    public BigDecimal getHoursPerSession() {
        return hoursPerSession;
    }

    public String getHoursPerSessionDD() {
        return "ProviderCourseInformation_hoursPerSession";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sessionStartTime">
    @Column
    private String sessionStartTime;

    public void setSessionStartTime(String sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public String getSessionStartTime() {
        return sessionStartTime;
    }

    public String getSessionStartTimeDD() {
        return "ProviderCourseInformation_sessionStartTime";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sessionEndTime">
    @Column
    private String sessionEndTime;

    public void setSessionEndTime(String sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public String getSessionEndTime() {
        return sessionEndTime;
    }

    public String getSessionEndTimeDD() {
        return "ProviderCourseInformation_sessionEndTime";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="plannedAttendeesNumber">
    @Column
    private int plannedAttendeesNumber;

    public void setPlannedAttendeesNumber(int plannedAttendeesNumber) {
        this.plannedAttendeesNumber = plannedAttendeesNumber;
    }

    public int getPlannedAttendeesNumber() {
        return plannedAttendeesNumber;
    }

    public String getPlannedAttendeesNumberDD() {
        return "ProviderCourseInformation_plannedAttendeesNumber";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="acceptedNumber">
    @Column
    private int acceptedNumber;

    public int getAcceptedNumber() {
        return acceptedNumber;
    }

    public void setAcceptedNumber(int acceptedNumber) {
        this.acceptedNumber = acceptedNumber;
    }

    public String getAcceptedNumberDD() {
        return "ProviderCourseInformation_acceptedNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "ProviderCourseInformation_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public String getNotesDD() {
        return "ProviderCourseInformation_notes";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestersNumber">
    @Column
    private int requestersNumber;

    public void setRequestersNumber(int requestersNumber) {
        this.requestersNumber = requestersNumber;
    }

    public int getRequestersNumber() {
        return requestersNumber;
    }

    public String getRequestersNumberDD() {
        return "ProviderCourseInformation_requestersNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="selectedNumber">
    @Column
    private int selectedNumber;

    public void setSelectedNumber(int selectedNumber) {
        this.selectedNumber = selectedNumber;
    }

    public int getSelectedNumber() {
        return selectedNumber;
    }

    public String getSelectedNumberDD() {
        return "ProviderCourseInformation_selectedNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="attendeesNumber">
    @Column
    private int attendeesNumber;

    public void setAttendeesNumber(int attendeesNumber) {
        this.attendeesNumber = attendeesNumber;
    }

    public int getAttendeesNumber() {
        return attendeesNumber;
    }

    public String getAttendeesNumberDD() {
        return "ProviderCourseInformation_attendeesNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="totalCost">
    @Column
    private BigDecimal totalCost;

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public String getTotalCostDD() {
        return "ProviderCourseInformation_totalCost";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="totalPrice">
    @Column
    private BigDecimal totalPrice;

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public String getTotalPriceDD() {
        return "ProviderCourseInformation_totalPrice";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="coursePricePerAttendee">
    @Column
    private BigDecimal coursePricePerAttendee;

    public void setCoursePricePerAttendee(BigDecimal coursePricePerAttendee) {
        this.coursePricePerAttendee = coursePricePerAttendee;
    }

    public BigDecimal getCoursePricePerAttendee() {
        return coursePricePerAttendee;
    }

    public String getCoursePricePerAttendeeDD() {
        return "ProviderCourseInformation_coursePricePerAttendee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseCostPerAttendee">
    @Column
    private BigDecimal courseCostPerAttendee;

    public void setCourseCostPerAttendee(BigDecimal courseCostPerAttendee) {
        this.courseCostPerAttendee = courseCostPerAttendee;
    }

    public BigDecimal getCourseCostPerAttendee() {
        return courseCostPerAttendee;
    }

    public String getCourseCostPerAttendeeDD() {
        return "ProviderCourseInformation_courseCostPerAttendee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCompanyShareAsPercentage">
    @Column
    private BigDecimal costCompanyShareAsPercentage;

    public void setCostCompanyShareAsPercentage(BigDecimal costCompanyShareAsPercentage) {
        this.costCompanyShareAsPercentage = costCompanyShareAsPercentage;
    }

    public BigDecimal getCostCompanyShareAsPercentage() {
        return costCompanyShareAsPercentage;
    }

    public String getCostCompanyShareAsPercentageDD() {
        return "ProviderCourseInformation_costCompanyShareAsPercentage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCompanyShareAsValue">
    @Column
    private BigDecimal costCompanyShareAsValue;

    public void setCostCompanyShareAsValue(BigDecimal costCompanyShareAsValue) {
        this.costCompanyShareAsValue = costCompanyShareAsValue;
    }

    public BigDecimal getCostCompanyShareAsValue() {
        return costCompanyShareAsValue;
    }

    public String getCostCompanyShareAsValueDD() {
        return "ProviderCourseInformation_costCompanyShareAsValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costEmployeeShareAsPercentage">
    @Column
    private BigDecimal costEmployeeShareAsPercentage;

    public void setCostEmployeeShareAsPercentage(BigDecimal costEmployeeShareAsPercentage) {
        this.costEmployeeShareAsPercentage = costEmployeeShareAsPercentage;
    }

    public BigDecimal getCostEmployeeShareAsPercentage() {
        return costEmployeeShareAsPercentage;
    }

    public String getCostEmployeeShareAsPercentageDD() {
        return "ProviderCourseInformation_costEmployeeShareAsPercentage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costEmployeeShareAsValue">
    @Column
    private BigDecimal costEmployeeShareAsValue;

    public void setCostEmployeeShareAsValue(BigDecimal costEmployeeShareAsValue) {
        this.costEmployeeShareAsValue = costEmployeeShareAsValue;
    }

    public BigDecimal getCostEmployeeShareAsValue() {
        return costEmployeeShareAsValue;
    }

    public String getCostEmployeeShareAsValueDD() {
        return "ProviderCourseInformation_costEmployeeShareAsValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companySharePerDayAsValue">
    @Column
    private BigDecimal companySharePerDayAsValue;

    public void setCompanySharePerDayAsValue(BigDecimal companySharePerDayAsValue) {
        this.companySharePerDayAsValue = companySharePerDayAsValue;
    }

    public BigDecimal getCompanySharePerDayAsValue() {
        return companySharePerDayAsValue;
    }

    public String getCompanySharePerDayAsValueDD() {
        return "ProviderCourseInformation_companySharePerDayAsValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companySharePerAttendeeAsValue">
    @Column
    private BigDecimal companySharePerAttendeeAsValue;

    public void setCompanySharePerAttendeeAsValue(BigDecimal companySharePerAttendeeAsValue) {
        this.companySharePerAttendeeAsValue = companySharePerAttendeeAsValue;
    }

    public BigDecimal getCompanySharePerAttendeeAsValue() {
        return companySharePerAttendeeAsValue;
    }

    public String getCompanySharePerAttendeeAsValueDD() {
        return "ProviderCourseInformation_companySharePerAttendeeAsValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseDurationUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC courseDurationUnit;

    public UDC getCourseDurationUnit() {
        return courseDurationUnit;
    }

    public void setCourseDurationUnit(UDC periodUnit) {
        this.courseDurationUnit = periodUnit;
    }
    
    public String getCourseDurationUnitDD() {
        return "ProviderCourseInformation_courseDurationUnit";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="courseDuration">
    //@MeasurableField(name = "courseDurationUnit.value")
    private Integer courseDuration;

    public Integer getCourseDuration() {
        return courseDuration;
    }

    public void setCourseDuration(Integer courseDuration) {
        this.courseDuration = courseDuration;
    }
    
    public String getCourseDurationDD() {
        return "ProviderCourseInformation_courseDuration";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="trainingPlan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderTrainingPlan trainingPlan;

    public ProviderTrainingPlan getTrainingPlan() {
        return trainingPlan;
    }

    public void setTrainingPlan(ProviderTrainingPlan trainingPlan) {
        this.trainingPlan = trainingPlan;
    }
    
    public String getTrainingPlanDD() {
        return "ProviderCourseInformation_trainingPlan";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="course">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourse course;

    public ProviderCourse getCourse() {
        return course;
    }

    public void setCourse(ProviderCourse course) {
        this.course = course;
    }
    
    public String getCourseDD() {
        return "ProviderCourseInformation_course";
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="employeeRequest">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeCourseRequest employeeRequest;

    public EmployeeCourseRequest getEmployeeRequest() {
        return employeeRequest;
    }

    public void setEmployeeRequest(EmployeeCourseRequest employeeRequest) {
        this.employeeRequest = employeeRequest;
    }
    
    public String getEmployeeRequestDD() {
        return "ProviderCourseInformation_employeeRequest";
    }
    //</editor-fold>
    
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }
    
    @Override
    public void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
}
