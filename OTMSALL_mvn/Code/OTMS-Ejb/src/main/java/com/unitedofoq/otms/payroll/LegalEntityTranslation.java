
package com.unitedofoq.otms.payroll;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class LegalEntityTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="shortDescription">
    @Column
    private String shortDescription;

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescription() {
        return shortDescription;
    }
    // </editor-fold>

}
