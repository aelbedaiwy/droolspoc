package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@ParentEntity(fields={"position"})
@DiscriminatorValue("DIRECT")
public class PositionDirectParent extends PositionParentBase {
// <editor-fold defaultstate="collapsed" desc="positionType">
    @Column(name="EPTYPE",nullable=false)
    private int parentPositionType;
    public int getParentPositionType() {
    	parentPositionType=3001;
        return parentPositionType;
    }
    public String getParentPositionTypeDD() {
        return "PositionDirectParent_parentPositionType";
    }
    public void setParentPositionType(int parentPositionType) {
    	parentPositionType = 3001;
        this.parentPositionType = parentPositionType ;
    }
    // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="position">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position ;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    public String getPositionDD() {
        return "PositionDirectParent_position";
    }


    // </editor-fold>
}
