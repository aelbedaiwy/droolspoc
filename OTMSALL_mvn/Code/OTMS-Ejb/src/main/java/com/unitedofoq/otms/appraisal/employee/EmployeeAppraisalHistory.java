package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeAppraisalHistory extends AppraisalHistoryBase {

    //<editor-fold defaultstate="collapsed" desc="yearSalary">
    private BigDecimal yearSalary = new BigDecimal(0);

    public BigDecimal getYearSalary() {
        return yearSalary;
    }

    public void setYearSalary(BigDecimal yearSalary) {
        this.yearSalary = yearSalary;
    }

    public String getYearSalaryDD() {
        return "EmployeeAppraisalHistory_yearSalary";
    }

    @Transient
    private BigDecimal yearSalaryMask;

    public BigDecimal getYearSalaryMask() {
        yearSalaryMask = yearSalary;
        return yearSalaryMask;
    }

    public void setYearSalaryMask(BigDecimal yearSalaryMask) {
        updateDecimalValue("yearSalary", yearSalaryMask);
    }

    public String getYearSalaryMaskDD() {
        return "EmployeeAppraisalHistory_yearSalaryMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="newSalary">
    private BigDecimal newSalary = new BigDecimal(0);

    public BigDecimal getNewSalary() {
        return newSalary;
    }

    public void setNewSalary(BigDecimal newSalary) {
        this.newSalary = newSalary;
    }

    public String getNewSalaryDD() {
        return "EmployeeAppraisalHistory_newSalary";
    }

    @Transient
    private BigDecimal newSalaryMask;

    public BigDecimal getNewSalaryMask() {
        newSalaryMask = newSalary;
        return newSalaryMask;
    }

    public void setNewSalaryMask(BigDecimal newSalaryMask) {
        updateDecimalValue("newSalary", newSalaryMask);
    }

    public String getNewSalaryMaskDD() {
        return "EmployeeAppraisalHistory_newSalaryMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="meritIncreasePercent">
    private BigDecimal meritIncreasePercent = new BigDecimal(0);

    public BigDecimal getMeritIncreasePercent() {
        return meritIncreasePercent;
    }

    public void setMeritIncreasePercent(BigDecimal meritIncreasePercent) {
        this.meritIncreasePercent = meritIncreasePercent;
    }

    public String getMeritIncreasePercentDD() {
        return "EmployeeAppraisalHistory_meritIncreasePercent";
    }

    @Transient
    private BigDecimal meritIncreasePercentMask;

    public BigDecimal getMeritIncreasePercentMask() {
        meritIncreasePercentMask = meritIncreasePercent;
        return meritIncreasePercentMask;
    }

    public void setMeritIncreasePercentMask(BigDecimal meritIncreasePercentMask) {
        updateDecimalValue("meritIncreasePercent", meritIncreasePercentMask);
    }

    public String getMeritIncreasePercentMaskDD() {
        return "EmployeeAppraisalHistory_meritIncreasePercentMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="meritIncreaseVal">
    private BigDecimal meritIncreaseVal = new BigDecimal(0);

    public BigDecimal getMeritIncreaseVal() {
        return meritIncreaseVal;
    }

    public void setMeritIncreaseVal(BigDecimal meritIncreaseVal) {
        this.meritIncreaseVal = meritIncreaseVal;
    }

    public String getMeritIncreaseValDD() {
        return "EmployeeAppraisalHistory_meritIncreaseVal";
    }

    @Transient
    private BigDecimal meritIncreaseValMask;

    public BigDecimal getMeritIncreaseValMask() {
        meritIncreaseValMask = meritIncreaseVal;
        return meritIncreaseValMask;
    }

    public void setMeritIncreaseValMask(BigDecimal meritIncreaseValMask) {
        updateDecimalValue("meritIncreaseVal", meritIncreaseValMask);
    }

    public String getMeritIncreaseValMaskDD() {
        return "EmployeeAppraisalHistory_meritIncreaseValMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="oldGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade oldGrade;

    public PayGrade getOldGrade() {
        return oldGrade;
    }

    public void setOldGrade(PayGrade oldGrade) {
        this.oldGrade = oldGrade;
    }

    public String getOldGradeDD() {
        return "EmployeeAppraisalHistory_oldGrade";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="newGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade newGrade;

    public PayGrade getNewGrade() {
        return newGrade;
    }

    public void setNewGrade(PayGrade newGrade) {
        this.newGrade = newGrade;
    }

    public String getNewGradeDD() {
        return "EmployeeAppraisalHistory_newGrade";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="bonus">
    private BigDecimal bonus = new BigDecimal(0);

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public String getBonusDD() {
        return "EmployeeAppraisalHistory_bonus";
    }

    @Transient
    private BigDecimal bonusMask;

    public BigDecimal getBonusMask() {
        bonusMask = bonus;
        return bonusMask;
    }

    public void setBonusMask(BigDecimal bonusMask) {
        updateDecimalValue("bonus", bonusMask);
    }

    public String getBonusMaskDD() {
        return "EmployeeAppraisalHistory_bonusMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="oldTitle">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position oldTitle;

    public Position getOldTitle() {
        return oldTitle;
    }

    public void setOldTitle(Position oldTitle) {
        this.oldTitle = oldTitle;
    }

    public String getOldTitleDD() {
        return "EmployeeAppraisalHistory_oldTitle";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="newTitle">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position newTitle;

    public Position getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(Position newTitle) {
        this.newTitle = newTitle;
    }

    public String getNewTitleDD() {
        return "EmployeeAppraisalHistory_newTitle";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="finalRate">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule finalRate;

    public ScaleTypeRule getFinalRate() {
        return finalRate;
    }

    public void setFinalRate(ScaleTypeRule finalRate) {
        this.finalRate = finalRate;
    }

    public String getFinalRateDD() {
        return "EmployeeAppraisalHistory_finalRate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="finalSalary">
    private BigDecimal finalSalary = new BigDecimal(0);

    public BigDecimal getFinalSalary() {
        return finalSalary;
    }

    public void setFinalSalary(BigDecimal finalSalary) {
        this.finalSalary = finalSalary;
    }

    public String getFinalSalaryDD() {
        return "EmployeeAppraisalHistory_finalSalary";
    }

    @Transient
    private BigDecimal finalSalaryMask;

    public BigDecimal getFinalSalaryMask() {
        finalSalaryMask = finalSalary;
        return finalSalaryMask;
    }

    public void setFinalSalaryMask(BigDecimal finalSalaryMask) {
        updateDecimalValue("finalSalary", finalSalaryMask);
    }

    public String getFinalSalaryMaskDD() {
        return "EmployeeAppraisalHistory_finalSalaryMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="salaryAdjustment">
    @Column(precision = 25, scale = 13)
    private BigDecimal salaryAdjustment = new BigDecimal(0);

    public BigDecimal getSalaryAdjustment() {
        return salaryAdjustment;
    }

    public void setSalaryAdjustment(BigDecimal salaryAdjustment) {
        this.salaryAdjustment = salaryAdjustment;
    }

    public String getSalaryAdjustmentDD() {
        return "EmployeeAppraisalHistory_salaryAdjustment";
    }

    @Transient
    private BigDecimal salaryAdjustmentMask;

    public BigDecimal getSalaryAdjustmentMask() {
        salaryAdjustmentMask = salaryAdjustment;
        return salaryAdjustmentMask;
    }

    public void setSalaryAdjustmentMask(BigDecimal salaryAdjustmentMask) {
        updateDecimalValue("salaryAdjustment", salaryAdjustmentMask);
    }

    public String getSalaryAdjustmentMaskDD() {
        return "EmployeeAppraisalHistory_salaryAdjustmentMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="finalRateNew">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule finalRateNew;

    public ScaleTypeRule getFinalRateNew() {
        return finalRateNew;
    }

    public void setFinalRateNew(ScaleTypeRule finalRateNew) {
        this.finalRateNew = finalRateNew;
    }

    public String getFinalRateNewDD() {
        return "EmployeeAppraisalHistory_finalRateNew";
    }
    //</editor-fold>

    //for validation of salary review
    @Transient
    private ScaleTypeRule oldRating;

    public ScaleTypeRule getOldRating() {
        return oldRating;
    }

    public void setOldRating(ScaleTypeRule oldRating) {
        this.oldRating = oldRating;
    }

    public String getOldRatingDD() {
        return "EmployeeAppraisalHistory_oldRating";
    }

    // for recruitment:
    // =================
    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "EmployeeAppraisalHistory_applicant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeAgree">
    private boolean employeeAgree;

    public boolean isEmployeeAgree() {
        return employeeAgree;
    }

    public String getEmployeeAgreeDD() {
        return "EmployeeAppraisalHistory_employeeAgree";
    }

    public void setEmployeeAgree(boolean employeeAgree) {
        this.employeeAgree = employeeAgree;
    }
    //</editor-fold >

    // <editor-fold defaultstate="collapsed" desc="employeeAgreement">
    private String employeeAgreement; // drop down: Agree-Disagree-Empty

    public String getEmployeeAgreement() {
        return employeeAgreement;
    }

    public void setEmployeeAgreement(String employeeAgreement) {
        this.employeeAgreement = employeeAgreement;
    }

    public String getEmployeeAgreementDD() {
        return "EmployeeGoal_employeeAgreement";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeComments">
    private String employeeComments;

    public String getEmployeeComments() {
        return employeeComments;
    }

    public String getEmployeeCommentsDD() {
        return "EmployeeAppraisalHistory_employeeComments";
    }

    public void setEmployeeComments(String employeeComments) {
        this.employeeComments = employeeComments;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="managerConfirmation">
    private boolean managerConfirmation;

    public boolean isManagerConfirmation() {
        return managerConfirmation;
    }

    public String getManagerConfirmationDD() {
        return "EmployeeAppraisalHistory_managerConfirmation";
    }

    public void setManagerConfirmation(boolean managerConfirmation) {
        this.managerConfirmation = managerConfirmation;
    }
    //</editor-fold>
}
