/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.holiday;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author dev
 */
@Entity
public class HolidayAssign extends BaseEntity {

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    HolidayDates holidayDates;

    String includeOrExclude;

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    UDC location;

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    Position postion;

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    Unit unit;

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    Employee employee;

    public HolidayDates getHolidayDates() {
        return holidayDates;
    }

    public String getHolidayDatesDD() {
        return "HolidayAssign_holidayDates";
    }

    public void setHolidayDates(HolidayDates holidayDates) {
        this.holidayDates = holidayDates;
    }

    public String getIncludeOrExclude() {
        return includeOrExclude;
    }

    public String getIncludeOrExcludeDD() {
        return "HolidayAssign_includeOrExclude";
    }

    public void setIncludeOrExclude(String includeOrExclude) {
        this.includeOrExclude = includeOrExclude;
    }

    public UDC getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "HolidayAssign_location";
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public Position getPostion() {
        return postion;
    }

    public String getPostionDD() {
        return "HolidayAssign_postion";
    }

    public void setPostion(Position postion) {
        this.postion = postion;
    }

    public Unit getUnit() {
        return unit;
    }

    public String getUnitDD() {
        return "HolidayAssign_unit";
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "HolidayAssign_employee";
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

}
