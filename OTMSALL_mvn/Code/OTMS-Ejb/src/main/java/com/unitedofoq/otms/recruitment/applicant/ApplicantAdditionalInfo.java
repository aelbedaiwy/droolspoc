/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.AdditionalInfo;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantAdditionalInfo extends AdditionalInfo {
    // <editor-fold defaultstate="collapsed" desc="Applicant">
    @JoinColumn(nullable=false)
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "ApplicantAdditionalInfo_applicant";
    }
    // </editor-fold>
}
