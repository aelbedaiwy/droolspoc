/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author arezk
 */
@Entity
@ChildEntity(fields={"trainingProviderCourses"})
public class EDSTrainingProvider extends BusinessObjectBaseEntity{
    @Column(nullable=false)
    private String contact;
    @Column(nullable=false)
    private String address;
    @Column(nullable=false)
    private String name;
    @Column(nullable=false)
    private String type;
    @OneToMany(mappedBy="provider")
    private List<EDSTrainingProviderCourse> trainingProviderCourses;

    public String getNameDD(){  return "EDSTrainingProvider_name";  }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EDSTrainingProviderCourse> getTrainingProviderCourses() {
        return trainingProviderCourses;
    }

    public void setTrainingProviderCourses(List<EDSTrainingProviderCourse> trainingProviderCourses) {
        this.trainingProviderCourses = trainingProviderCourses;
    }
}
