package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.*;
import javax.persistence.*;

/**
 * 
 */
@Entity
@ParentEntity(fields="company")
public class CostCenter extends BusinessObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column(unique=true)
    private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "CostCenter_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterGroup">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private CostCenterGroup costCenterGroup;
    public CostCenterGroup getCostCenterGroup() {
        return costCenterGroup;
    }
    public void setCostCenterGroup(CostCenterGroup costCenterGroup) {
        this.costCenterGroup = costCenterGroup;
    }
    public String getCostCenterGroupDD() {
        return "CostCenter_costCenterGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false,unique=true)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "CostCenter_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private LegalEntity legalEntity;
    public LegalEntity getLegalEntity() {
        return legalEntity;
    }
    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }
    public String getLegalEntityDD() {
        return "CostCenter_legalEntity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
	
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "CostCenter_company";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="glAccount">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }
    
    public String getGlAccountDD() {
        return "CostCenter_glAccount";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment1">
    @Column
    private String segment1;

    public String getSegment1() {
        return segment1;
    }

    public void setSegment1(String segment1) {
        this.segment1 = segment1;
    }
    
    public String getSegment1DD() {
        return "CostCenter_segment1";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment2">
    @Column
    private String segment2;

    public String getSegment2() {
        return segment2;
    }

    public void setSegment2(String segment2) {
        this.segment2 = segment2;
    }
    
    public String getSegment2DD() {
        return "CostCenter_segment2";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment3">
    @Column
    private String segment3;

    public String getSegment3() {
        return segment3;
    }

    public void setSegment3(String segment3) {
        this.segment3 = segment3;
    }
    
    public String getSegment3DD() {
        return "CostCenter_segment3";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment4">
    @Column
    private String segment4;

    public String getSegment4() {
        return segment4;
    }

    public void setSegment4(String segment4) {
        this.segment4 = segment4;
    }
    
    public String getSegment4DD() {
        return "CostCenter_segment4";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment5">
    @Column
    private String segment5;

    public String getSegment5() {
        return segment5;
    }

    public void setSegment5(String segment5) {
        this.segment5 = segment5;
    }
    
    public String getSegment5DD() {
        return "CostCenter_segment5";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment6">
    @Column
    private String segment6;

    public String getSegment6() {
        return segment6;
    }

    public void setSegment6(String segment6) {
        this.segment6 = segment6;
    }
    
    public String getSegment6DD() {
        return "CostCenter_segment6";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment7">
    @Column
    private String segment7;

    public String getSegment7() {
        return segment7;
    }

    public void setSegment7(String segment7) {
        this.segment7 = segment7;
    }
    
    public String getSegment7DD() {
        return "CostCenter_segment7";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment8">
    @Column
    private String segment8;

    public String getSegment8() {
        return segment8;
    }

    public void setSegment8(String segment8) {
        this.segment8 = segment8;
    }
    
    public String getSegment8DD() {
        return "CostCenter_segment8";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment9">
    @Column
    private String segment9;

    public String getSegment9() {
        return segment9;
    }

    public void setSegment9(String segment9) {
        this.segment9 = segment9;
    }
    
    public String getSegment9DD() {
        return "CostCenter_segment9";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="segment10">
    @Column
    private String segment10;

    public String getSegment10() {
        return segment10;
    }

    public void setSegment10(String segment10) {
        this.segment10 = segment10;
    }
    
    public String getSegment10DD() {
        return "CostCenter_segment10";
    }
    // </editor-fold>
}
