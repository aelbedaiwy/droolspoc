
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepJobGroupJob extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private long dsDBID;

    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }

    public long getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepJobGroupJob_dsDBID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    @Translatable(translationField = "jobNameTranslated")
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }

    public String getJobNameDD() {
        return "RepJobGroupJob_jobName";
    }
    
    @Transient
    @Translation(originalField = "jobName")
    private String jobNameTranslated;

    public String getJobNameTranslated() {
        return jobNameTranslated;
    }

    public void setJobNameTranslated(String jobNameTranslated) {
        this.jobNameTranslated = jobNameTranslated;
    }
    
    public String getJobNameTranslatedDD() {
        return "RepJobGroupJob_jobName";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobGroupDescription">
    @Column
    @Translatable(translationField = "jobGroupDescriptionTranslated")
    private String jobGroupDescription;

    public void setJobGroupDescription(String jobGroupDescription) {
        this.jobGroupDescription = jobGroupDescription;
    }

    public String getJobGroupDescription() {
        return jobGroupDescription;
    }

    public String getJobGroupDescriptionDD() {
        return "RepJobGroupJob_jobGroupDescription";
    }
    
    @Transient
    @Translation(originalField = "jobGroupDescription")
    private String jobGroupDescriptionTranslated;

    public String getJobGroupDescriptionTranslated() {
        return jobGroupDescriptionTranslated;
    }

    public void setJobGroupDescriptionTranslated(String jobGroupDescriptionTranslated) {
        this.jobGroupDescriptionTranslated = jobGroupDescriptionTranslated;
    }
    
    public String getJobGroupDescriptionTranslatedDD() {
        return "RepJobGroupJob_jobGroupDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    @Translatable(translationField = "payGradeDescriptionTranslated")
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }

    public String getPayGradeDescriptionDD() {
        return "RepJobGroupJob_payGradeDescription";
    }
    
    @Transient
    @Translation(originalField = "payGradeDescription")
    private String payGradeDescriptionTranslated;

    public String getPayGradeDescriptionTranslated() {
        return payGradeDescriptionTranslated;
    }

    public void setPayGradeDescriptionTranslated(String payGradeDescriptionTranslated) {
        this.payGradeDescriptionTranslated = payGradeDescriptionTranslated;
    }
    
    public String getPayGradeDescriptionTranslatedDD() {
        return "RepJobGroupJob_payGradeDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="minimumWorkDuration">
    @Column
    private BigDecimal minimumWorkDuration;

    public void setMinimumWorkDuration(BigDecimal minimumWorkDuration) {
        this.minimumWorkDuration = minimumWorkDuration;
    }

    public BigDecimal getMinimumWorkDuration() {
        return minimumWorkDuration;
    }

    public String getMinimumWorkDurationDD() {
        return "RepJobGroupJob_minimumWorkDuration";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="maximumWorkDuration">
    @Column
    private BigDecimal maximumWorkDuration;

    public void setMaximumWorkDuration(BigDecimal maximumWorkDuration) {
        this.maximumWorkDuration = maximumWorkDuration;
    }

    public BigDecimal getMaximumWorkDuration() {
        return maximumWorkDuration;
    }

    public String getMaximumWorkDurationDD() {
        return "RepJobGroupJob_maximumWorkDuration";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="workDurationMeasureUnit">
    @Column
    @Translatable(translationField = "workDurationMeasureUnitTranslated")
    private String workDurationMeasureUnit;

    public void setWorkDurationMeasureUnit(String workDurationMeasureUnit) {
        this.workDurationMeasureUnit = workDurationMeasureUnit;
    }

    public String getWorkDurationMeasureUnit() {
        return workDurationMeasureUnit;
    }

    public String getWorkDurationMeasureUnitDD() {
        return "RepJobGroupJob_workDurationMeasureUnit";
    }
    
    @Transient
    @Translation(originalField = "workDurationMeasureUnit")
    private String workDurationMeasureUnitTranslated;

    public String getWorkDurationMeasureUnitTranslated() {
        return workDurationMeasureUnitTranslated;
    }

    public void setWorkDurationMeasureUnitTranslated(String workDurationMeasureUnitTranslated) {
        this.workDurationMeasureUnitTranslated = workDurationMeasureUnitTranslated;
    }
    
    public String getWorkDurationMeasureUnitTranslatedDD() {
        return "RepJobGroupJob_workDurationMeasureUnit";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="minimumExperience">
    @Column
    private BigDecimal minimumExperience;

    public void setMinimumExperience(BigDecimal minimumExperience) {
        this.minimumExperience = minimumExperience;
    }

    public BigDecimal getMinimumExperience() {
        return minimumExperience;
    }

    public String getMinimumExperienceDD() {
        return "RepJobGroupJob_minimumExperience";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="maximumExperience">
    @Column
    private BigDecimal maximumExperience;

    public void setMaximumExperience(BigDecimal maximumExperience) {
        this.maximumExperience = maximumExperience;
    }

    public BigDecimal getMaximumExperience() {
        return maximumExperience;
    }

    public String getMaximumExperienceDD() {
        return "RepJobGroupJob_maximumExperience";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="experienceMeasureUnit">
    @Column
    @Translatable(translationField = "experienceMeasureUnitTranslated")
    private String experienceMeasureUnit;

    public void setExperienceMeasureUnit(String experienceMeasureUnit) {
        this.experienceMeasureUnit = experienceMeasureUnit;
    }

    public String getExperienceMeasureUnit() {
        return experienceMeasureUnit;
    }

    public String getExperienceMeasureUnitDD() {
        return "RepJobGroupJob_experienceMeasureUnit";
    }
    
    @Transient
    @Translation(originalField = "experienceMeasureUnit")
    private String experienceMeasureUnitTranslated;

    public String getExperienceMeasureUnitTranslated() {
        return experienceMeasureUnitTranslated;
    }

    public void setExperienceMeasureUnitTranslated(String experienceMeasureUnitTranslated) {
        this.experienceMeasureUnitTranslated = experienceMeasureUnitTranslated;
    }
    
    public String getExperienceMeasureUnitTranslatedDD() {
        return "RepJobGroupJob_experienceMeasureUnit";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getCompanyIDDD() {
        return "RepJobGroupJob_companyID";
    }
    // </editor-fold>

}
