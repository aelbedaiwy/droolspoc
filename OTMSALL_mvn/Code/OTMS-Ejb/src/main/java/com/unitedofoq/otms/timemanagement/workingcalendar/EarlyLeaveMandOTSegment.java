/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields={"calendar"})
public class EarlyLeaveMandOTSegment extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="fromTime">
    private String fromTime;

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }
    
    public String getFromTimeDD() {
        return "EarlyLeaveMandOTSegment_fromTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toTime">
    private String toTime;

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }
    
    public String getToTimeDD() {
        return "EarlyLeaveMandOTSegment_toTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="otPeriod">
    private String otPeriod;

    public String getOtPeriod() {
        return otPeriod;
    }

    public void setOtPeriod(String otPeriod) {
        this.otPeriod = otPeriod;
    }
    
    public String getOtPeriodDD() {
        return "EarlyLeaveMandOTSegment_otPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calendar">
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(nullable=false)
    private NormalWorkingCalendar calendar;

    public NormalWorkingCalendar getCalendar() {
        return calendar;
    }

    public void setCalendar(NormalWorkingCalendar calendar) {
        this.calendar = calendar;
    }
    
    public String getCalendarDD() {
        return "EarlyLeaveMandOTSegment_calendar";
    }
    // </editor-fold>
}
