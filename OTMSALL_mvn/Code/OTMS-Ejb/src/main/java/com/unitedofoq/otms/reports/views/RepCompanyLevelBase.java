package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class RepCompanyLevelBase extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="companyID">

    @Column
    private int companyID;

    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public String getCompanyIDDD() {
        return "RepCompanyLevelBase_companyID";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level0Description">
    @Column
    @Translatable(translationField = "level0DescriptionTranslated")
    private String level0Description;

    public String getLevel0Description() {
        return level0Description;
    }

    public void setLevel0Description(String level0Description) {
        this.level0Description = level0Description;
    }

    public String getLevel0DescriptionDD() {
        return "RepCompanyLevelBase_level0Description";
    }
    @Transient
    @Translation(originalField = "level0Description")
    private String level0DescriptionTranslated;

    public String getLevel0DescriptionTranslated() {
        return level0DescriptionTranslated;
    }

    public void setLevel0DescriptionTranslated(String level0DescriptionTranslated) {
        this.level0DescriptionTranslated = level0DescriptionTranslated;
    }

    public String getLevel0DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level0Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level1Description">
    @Column
    @Translatable(translationField = "level1DescriptionTranslated")
    private String level1Description;

    public String getLevel1Description() {
        return level1Description;
    }

    public void setLevel1Description(String level1Description) {
        this.level1Description = level1Description;
    }

    public String getLevel1DescriptionDD() {
        return "RepCompanyLevelBase_level1Description";
    }
    @Transient
    @Translation(originalField = "level1Description")
    private String level1DescriptionTranslated;

    public String getLevel1DescriptionTranslated() {
        return level1DescriptionTranslated;
    }

    public void setLevel1DescriptionTranslated(String level1DescriptionTranslated) {
        this.level1DescriptionTranslated = level1DescriptionTranslated;
    }

    public String getLevel1DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level1Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level2Description">
    @Column
    @Translatable(translationField = "level2DescriptionTranslated")
    private String level2Description;

    public String getLevel2Description() {
        return level2Description;
    }

    public void setLevel2Description(String level2Description) {
        this.level2Description = level2Description;
    }

    public String getLevel2DescriptionDD() {
        return "RepCompanyLevelBase_level2Description";
    }
    @Transient
    @Translation(originalField = "level2Description")
    private String level2DescriptionTranslated;

    public String getLevel2DescriptionTranslated() {
        return level2DescriptionTranslated;
    }

    public void setLevel2DescriptionTranslated(String level2DescriptionTranslated) {
        this.level2DescriptionTranslated = level2DescriptionTranslated;
    }

    public String getLevel2DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level2Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level3Description">
    @Column
    @Translatable(translationField = "level3DescriptionTranslated")
    private String level3Description;

    public String getLevel3Description() {
        return level3Description;
    }

    public void setLevel3Description(String level3Description) {
        this.level3Description = level3Description;
    }

    public String getLevel3DescriptionDD() {
        return "RepCompanyLevelBase_level3Description";
    }
    @Transient
    @Translation(originalField = "level3Description")
    private String level3DescriptionTranslated;

    public String getLevel3DescriptionTranslated() {
        return level3DescriptionTranslated;
    }

    public void setLevel3DescriptionTranslated(String level3DescriptionTranslated) {
        this.level3DescriptionTranslated = level3DescriptionTranslated;
    }

    public String getLevel3DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level3Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level4Description">
    @Column
    @Translatable(translationField = "level4DescriptionTranslated")
    private String level4Description;

    public String getLevel4Description() {
        return level4Description;
    }

    public void setLevel4Description(String level4Description) {
        this.level4Description = level4Description;
    }

    public String getLevel4DescriptionDD() {
        return "RepCompanyLevelBase_level4Description";
    }
    @Transient
    @Translation(originalField = "level4Description")
    private String level4DescriptionTranslated;

    public String getLevel4DescriptionTranslated() {
        return level4DescriptionTranslated;
    }

    public void setLevel4DescriptionTranslated(String level4DescriptionTranslated) {
        this.level4DescriptionTranslated = level4DescriptionTranslated;
    }

    public String getLevel4DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level4Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level5Description">
    @Column
    @Translatable(translationField = "level5DescriptionTranslated")
    private String level5Description;

    public String getLevel5Description() {
        return level5Description;
    }

    public void setLevel5Description(String level5Description) {
        this.level5Description = level5Description;
    }

    public String getLevel5DescriptionDD() {
        return "RepCompanyLevelBase_level5Description";
    }
    @Transient
    @Translation(originalField = "level5Description")
    private String level5DescriptionTranslated;

    public String getLevel5DescriptionTranslated() {
        return level5DescriptionTranslated;
    }

    public void setLevel5DescriptionTranslated(String level5DescriptionTranslated) {
        this.level5DescriptionTranslated = level5DescriptionTranslated;
    }

    public String getLevel5DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level5Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level6Description">
    @Column
    @Translatable(translationField = "level6DescriptionTranslated")
    private String level6Description;

    public String getLevel6Description() {
        return level6Description;
    }

    public void setLevel6Description(String level6Description) {
        this.level6Description = level6Description;
    }

    public String getLevel6DescriptionDD() {
        return "RepCompanyLevelBase_level6Description";
    }
    @Transient
    @Translation(originalField = "level6Description")
    private String level6DescriptionTranslated;

    public String getLevel6DescriptionTranslated() {
        return level6DescriptionTranslated;
    }

    public void setLevel6DescriptionTranslated(String level6DescriptionTranslated) {
        this.level6DescriptionTranslated = level6DescriptionTranslated;
    }

    public String getLevel6DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level6Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level7Description">
    @Column
    @Translatable(translationField = "level7DescriptionTranslated")
    private String level7Description;

    public String getLevel7Description() {
        return level7Description;
    }

    public void setLevel7Description(String level7Description) {
        this.level7Description = level7Description;
    }

    public String getLevel7DescriptionDD() {
        return "RepCompanyLevelBase_level7Description";
    }
    @Transient
    @Translation(originalField = "level7Description")
    private String level7DescriptionTranslated;

    public String getLevel7DescriptionTranslated() {
        return level7DescriptionTranslated;
    }

    public void setLevel7DescriptionTranslated(String level7DescriptionTranslated) {
        this.level7DescriptionTranslated = level7DescriptionTranslated;
    }

    public String getLevel7DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level7Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level8Description">
    @Column
    @Translatable(translationField = "level8DescriptionTranslated")
    private String level8Description;

    public String getLevel8Description() {
        return level8Description;
    }

    public void setLevel8Description(String level8Description) {
        this.level8Description = level8Description;
    }

    public String getLevel8DescriptionDD() {
        return "RepCompanyLevelBase_level8Description";
    }
    @Transient
    @Translation(originalField = "level8Description")
    private String level8DescriptionTranslated;

    public String getLevel8DescriptionTranslated() {
        return level8DescriptionTranslated;
    }

    public void setLevel8DescriptionTranslated(String level8DescriptionTranslated) {
        this.level8DescriptionTranslated = level8DescriptionTranslated;
    }

    public String getLevel8DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level8Description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="level9Description">
    @Column
    @Translatable(translationField = "level9DescriptionTranslated")
    private String level9Description;

    public String getLevel9Description() {
        return level9Description;
    }

    public void setLevel9Description(String level9Description) {
        this.level9Description = level9Description;
    }

    public String getLevel9DescriptionDD() {
        return "RepCompanyLevelBase_level9Description";
    }
    @Transient
    @Translation(originalField = "level9Description")
    private String level9DescriptionTranslated;

    public String getLevel9DescriptionTranslated() {
        return level9DescriptionTranslated;
    }

    public void setLevel9DescriptionTranslated(String level9DescriptionTranslated) {
        this.level9DescriptionTranslated = level9DescriptionTranslated;
    }

    public String getLevel9DescriptionTranslatedDD() {
        return "RepCompanyLevelBase_level9Description";
    }
    //</editor-fold>
}
