/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import com.unitedofoq.otms.foundation.unit.UnitLevelBase;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields="company")
@DiscriminatorValue("MASTER")
@Table(name="UnitLevel")
public class EDSUnitLevel extends UnitLevelBase {
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private EDSCompany company;
    public EDSCompany getCompany() {
        return company;
    }
    public void setCompany(EDSCompany company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "UnitLevel_company";
    }
    // </editor-fold>
}
