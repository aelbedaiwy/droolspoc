/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepEmployeesWithoutPunchIn extends RepEmployeeBase {
    
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private Long dsDbid;

    public Long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(Long dsDbid) {
        this.dsDbid = dsDbid;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="dailyDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dailyDate;

    public void setDailyDate(Date dailyDate) {
        this.dailyDate = dailyDate;
    }

    public Date getDailyDate() {
        return dailyDate;
    }

    public String getDailyDateDD() {
        return "RepEmployeesWithoutPunchIn_dailyDate";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="dayNature">
    @Column
    @Translatable(translationField = "dayNatureTranslated")
    private String dayNature;

    public void setDayNature(String dayNature) {
        this.dayNature = dayNature;
    }

    public String getDayNature() {
        return dayNature;
    }

    public String getDayNatureDD() {
        return "RepEmployeesWithoutPunchIn_dayNature";
    }
    
    @Transient
    @Translation(originalField = "dayNature")
    private String dayNatureTranslated;

    public String getDayNatureTranslated() {
        return dayNatureTranslated;
    }

    public void setDayNatureTranslated(String dayNatureTranslated) {
        this.dayNatureTranslated = dayNatureTranslated;
    }
    
    public String getDayNatureTranslatedDD() {
        return "RepEmployeesWithoutPunchIn_dayNature";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="machineDescription">
    private String machineDescription;

    public String getMachineDescription() {
        return machineDescription;
    }

    public void setMachineDescription(String machineDescription) {
        this.machineDescription = machineDescription;
    }
    
    public String getMachineDescriptionDD() {
        return "RepEmployeesWithoutPunchIn_machineDescription";
    }
    //</editor-fold>
}
