package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.payroll.tax.RepPayrollData;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class EmployeeSalary extends RepPayrollData {
    // <editor-fold defaultstate="collapsed" desc="dsDbid">

    @Column
    private Integer dsDbid;

    public Integer getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(Integer dsDbid) {
        this.dsDbid = dsDbid;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee_dbid">
    @Column
    private String employee_dbid;

    public String getEmployee_dbid() {
        return employee_dbid;
    }

    public void setEmployee_dbid(String employee_dbid) {
        this.employee_dbid = employee_dbid;
    }

    public String getEmployee_dbidDD() {
        return "EmployeeSalary_employee_dbid";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod_dbid">
    @Column
    private String calculatedPeriod_dbid;

    public String getCalculatedPeriod_dbid() {
        return calculatedPeriod_dbid;
    }

    public void setCalculatedPeriod_dbid(String calculatedPeriod_dbid) {
        this.calculatedPeriod_dbid = calculatedPeriod_dbid;
    }

    public String getCalculatedPeriod_dbidDD() {
        return "EmployeeSalary_calculatedPeriod_dbid";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EmployeeSalary_code";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String getEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }

    public String getEmployeeActiveTranslatedDD() {
        return "EmployeeSalary_employeeActive";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fullname">
    @Column
    @Translatable(translationField = "fullnameTranslated")
    private String fullname;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameDD() {
        return "EmployeeSalary_fullname";
    }
    @Transient
    @Translation(originalField = "fullname")
    private String fullnameTranslated;

    public String getFullnameTranslated() {
        return fullnameTranslated;
    }

    public void setFullnameTranslated(String fullnameTranslated) {
        this.fullnameTranslated = fullnameTranslated;
    }

    public String getFullnameTranslatedDD() {
        return "EmployeeSalary_fullname";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    @Translatable(translationField = "positionNameTranslated")
    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionNameDD() {
        return "EmployeeSalary_positionName";
    }
    @Transient
    @Translation(originalField = "positionName")
    private String positionNameTranslated;

    public String getPositionNameTranslated() {
        return positionNameTranslated;
    }

    public void setPositionNameTranslated(String positionNameTranslated) {
        this.positionNameTranslated = positionNameTranslated;
    }

    public String getPositionNameTranslatedDD() {
        return positionNameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    @Translatable(translationField = "locationDescriptionTranslated")
    private String locationDescription;

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescriptionDD() {
        return "EmployeeSalary_locationDescription";
    }
    @Transient
    @Translation(originalField = "locationDescription")
    private String locationDescriptionTranslated;

    public String getLocationDescriptionTranslated() {
        return locationDescriptionTranslated;
    }

    public void setLocationDescriptionTranslated(String locationDescriptionTranslated) {
        this.locationDescriptionTranslated = locationDescriptionTranslated;
    }

    public String getLocationDescriptionTranslatedDD() {
        return "EmployeeSalary_locationDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    @Translatable(translationField = "costCenterDescriptionTranslated")
    private String costCenterDescription;

    public String getCostCenterDescription() {
        return costCenterDescription;
    }

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescriptionDD() {
        return "EmployeeSalary_costCenterDescription";
    }
    @Transient
    @Translation(originalField = "costCenterDescription")
    private String costCenterDescriptionTranslated;

    public String getCostCenterDescriptionTranslated() {
        return costCenterDescriptionTranslated;
    }

    public void setCostCenterDescriptionTranslated(String costCenterDescriptionTranslated) {
        this.costCenterDescriptionTranslated = costCenterDescriptionTranslated;
    }

    public String getCostCenterDescriptionTranslatedDD() {
        return "EmployeeSalary_costCenterDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    @Translatable(translationField = "payGradeDescriptionTranslated")
    private String payGradeDescription;

    public String getPayGradeDescription() {
        return payGradeDescription;
    }

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescriptionDD() {
        return "EmployeeSalary_payGradeDescription";
    }
    @Transient
    @Translation(originalField = "payGradeDescription")
    private String payGradeDescriptionTranslated;

    public String getPayGradeDescriptionTranslated() {
        return payGradeDescriptionTranslated;
    }

    public void setPayGradeDescriptionTranslated(String payGradeDescriptionTranslated) {
        this.payGradeDescriptionTranslated = payGradeDescriptionTranslated;
    }

    public String getPayGradeDescriptionTranslatedDD() {
        return "EmployeeSalary_payGradeDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringTypeDescription">
    @Column
    @Translatable(translationField = "hiringTypeDescriptionTranslated")
    private String hiringTypeDescription;

    public String getHiringTypeDescription() {
        return hiringTypeDescription;
    }

    public void setHiringTypeDescription(String hiringTypeDescription) {
        this.hiringTypeDescription = hiringTypeDescription;
    }

    public String getHiringTypeDescriptionDD() {
        return "EmployeeSalary_hiringTypeDescription";
    }
    @Transient
    @Translation(originalField = "hiringTypeDescription")
    private String hiringTypeDescriptionTranslated;

    public String getHiringTypeDescriptionTranslated() {
        return hiringTypeDescriptionTranslated;
    }

    public void setHiringTypeDescriptionTranslated(String hiringTypeDescriptionTranslated) {
        this.hiringTypeDescriptionTranslated = hiringTypeDescriptionTranslated;
    }

    public String getHiringTypeDescriptionTranslatedDD() {
        return "EmployeeSalary_hiringTypeDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    @Translatable(translationField = "unitNameTranslated")
    private String unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitNameDD() {
        return "EmployeeSalary_unitName";
    }
    @Transient
    @Translation(originalField = "unitName")
    private String unitNameTranslated;

    public String getUnitNameTranslated() {
        return unitNameTranslated;
    }

    public void setUnitNameTranslated(String unitNameTranslated) {
        this.unitNameTranslated = unitNameTranslated;
    }

    public String getUnitNameTranslatedDD() {
        return "EmployeeSalary_unitName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    @Translatable(translationField = "jobNameTranslated")
    private String jobName;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobNameDD() {
        return "EmployeeSalary_jobName";
    }
    @Transient
    @Translation(originalField = "jobName")
    private String jobNameTranslated;

    public String getJobNameTranslated() {
        return jobNameTranslated;
    }

    public void setJobNameTranslated(String jobNameTranslated) {
        this.jobNameTranslated = jobNameTranslated;
    }

    public String getJobNameTranslatedDD() {
        return "EmployeeSalary_jobName";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="companyName">
    @Column
    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNameDD() {
        return "EmployeeSalary_companyName";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="bankCode">
    @Column
    private String bankCode;

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankCodeDD() {
        return "EmployeeSalary_bankCode";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeeSalary_notes";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI1">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI1;

    public BigDecimal getSalI1() {
        return salI1;
    }

    public void setSalI1(BigDecimal salI1) {
        this.salI1 = salI1;
    }

    public String getSalI1DD() {
        return "EmployeeSalary_salI1";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI2">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI2;

    public BigDecimal getSalI2() {
        return salI2;
    }

    public void setSalI2(BigDecimal salI2) {
        this.salI2 = salI2;
    }

    public String getSalI2DD() {
        return "EmployeeSalary_salI2";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI3">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI3;

    public BigDecimal getSalI3() {
        return salI3;
    }

    public void setSalI3(BigDecimal salI3) {
        this.salI3 = salI3;
    }

    public String getSalI3DD() {
        return "EmployeeSalary_salI3";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI4">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI4;

    public BigDecimal getSalI4() {
        return salI4;
    }

    public void setSalI4(BigDecimal salI4) {
        this.salI4 = salI4;
    }

    public String getSalI4DD() {
        return "EmployeeSalary_salI4";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI5">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI5;

    public BigDecimal getSalI5() {
        return salI5;
    }

    public void setSalI5(BigDecimal salI5) {
        this.salI5 = salI5;
    }

    public String getSalI5DD() {
        return "EmployeeSalary_salI5";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI6">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI6;

    public BigDecimal getSalI6() {
        return salI6;
    }

    public void setSalI6(BigDecimal salI6) {
        this.salI6 = salI6;
    }

    public String getSalI6DD() {
        return "EmployeeSalary_salI6";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI7">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI7;

    public BigDecimal getSalI7() {
        return salI7;
    }

    public void setSalI7(BigDecimal salI7) {
        this.salI7 = salI7;
    }

    public String getSalI7DD() {
        return "EmployeeSalary_salI7";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI8">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI8;

    public BigDecimal getSalI8() {
        return salI8;
    }

    public void setSalI8(BigDecimal salI8) {
        this.salI8 = salI8;
    }

    public String getSalI8DD() {
        return "EmployeeSalary_salI8";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI9">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI9;

    public BigDecimal getSalI9() {
        return salI9;
    }

    public void setSalI9(BigDecimal salI9) {
        this.salI9 = salI9;
    }

    public String getSalI9DD() {
        return "EmployeeSalary_salI9";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI10">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI10;

    public BigDecimal getSalI10() {
        return salI10;
    }

    public void setSalI10(BigDecimal salI10) {
        this.salI10 = salI10;
    }

    public String getSalI10DD() {
        return "EmployeeSalary_salI10";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI11">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI11;

    public BigDecimal getSalI11() {
        return salI11;
    }

    public void setSalI11(BigDecimal salI11) {
        this.salI11 = salI11;
    }

    public String getSalI11DD() {
        return "EmployeeSalary_salI11";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI12">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI12;

    public BigDecimal getSalI12() {
        return salI12;
    }

    public void setSalI12(BigDecimal salI12) {
        this.salI12 = salI12;
    }

    public String getSalI12DD() {
        return "EmployeeSalary_salI12";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI13">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI13;

    public BigDecimal getSalI13() {
        return salI13;
    }

    public void setSalI13(BigDecimal salI13) {
        this.salI13 = salI13;
    }

    public String getSalI13DD() {
        return "EmployeeSalary_salI13";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI14">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI14;

    public BigDecimal getSalI14() {
        return salI14;
    }

    public void setSalI14(BigDecimal salI14) {
        this.salI14 = salI14;
    }

    public String getSalI14DD() {
        return "EmployeeSalary_salI14";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI15">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI15;

    public BigDecimal getSalI15() {
        return salI15;
    }

    public void setSalI15(BigDecimal salI15) {
        this.salI15 = salI15;
    }

    public String getSalI15DD() {
        return "EmployeeSalary_salI15";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI16">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI16;

    public BigDecimal getSalI16() {
        return salI16;
    }

    public void setSalI16(BigDecimal salI16) {
        this.salI16 = salI16;
    }

    public String getSalI16DD() {
        return "EmployeeSalary_salI16";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI17">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI17;

    public BigDecimal getSalI17() {
        return salI17;
    }

    public void setSalI17(BigDecimal salI17) {
        this.salI17 = salI17;
    }

    public String getSalI17DD() {
        return "EmployeeSalary_salI17";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI18">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI18;

    public BigDecimal getSalI18() {
        return salI18;
    }

    public void setSalI18(BigDecimal salI18) {
        this.salI18 = salI18;
    }

    public String getSalI18DD() {
        return "EmployeeSalary_salI18";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI19">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI19;

    public BigDecimal getSalI19() {
        return salI19;
    }

    public void setSalI19(BigDecimal salI19) {
        this.salI19 = salI19;
    }

    public String getSalI19DD() {
        return "EmployeeSalary_salI19";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI20">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI20;

    public BigDecimal getSalI20() {
        return salI20;
    }

    public void setSalI20(BigDecimal salI20) {
        this.salI20 = salI20;
    }

    public String getSalI20DD() {
        return "EmployeeSalary_salI20";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI21">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI21;

    public BigDecimal getSalI21() {
        return salI21;
    }

    public void setSalI21(BigDecimal salI21) {
        this.salI21 = salI21;
    }

    public String getSalI21DD() {
        return "EmployeeSalary_salI21";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI22">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI22;

    public BigDecimal getSalI22() {
        return salI22;
    }

    public void setSalI22(BigDecimal salI22) {
        this.salI22 = salI22;
    }

    public String getSalI22DD() {
        return "EmployeeSalary_salI22";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI23">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI23;

    public BigDecimal getSalI23() {
        return salI23;
    }

    public void setSalI23(BigDecimal salI23) {
        this.salI23 = salI23;
    }

    public String getSalI23DD() {
        return "EmployeeSalary_salI23";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI24">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI24;

    public BigDecimal getSalI24() {
        return salI24;
    }

    public void setSalI24(BigDecimal salI24) {
        this.salI24 = salI24;
    }

    public String getSalI24DD() {
        return "EmployeeSalary_salI24";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI25">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI25;

    public BigDecimal getSalI25() {
        return salI25;
    }

    public void setSalI25(BigDecimal salI25) {
        this.salI25 = salI25;
    }

    public String getSalI25DD() {
        return "EmployeeSalary_salI25";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI26">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI26;

    public BigDecimal getSalI26() {
        return salI26;
    }

    public void setSalI26(BigDecimal salI26) {
        this.salI26 = salI26;
    }

    public String getSalI26DD() {
        return "EmployeeSalary_salI26";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI27">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI27;

    public BigDecimal getSalI27() {
        return salI27;
    }

    public void setSalI27(BigDecimal salI27) {
        this.salI27 = salI27;
    }

    public String getSalI27DD() {
        return "EmployeeSalary_salI27";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI28">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI28;

    public BigDecimal getSalI28() {
        return salI28;
    }

    public void setSalI28(BigDecimal salI28) {
        this.salI28 = salI28;
    }

    public String getSalI28DD() {
        return "EmployeeSalary_salI28";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI29">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI29;

    public BigDecimal getSalI29() {
        return salI29;
    }

    public void setSalI29(BigDecimal salI29) {
        this.salI29 = salI29;
    }

    public String getSalI29DD() {
        return "EmployeeSalary_salI29";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI30">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI30;

    public BigDecimal getSalI30() {
        return salI30;
    }

    public void setSalI30(BigDecimal salI30) {
        this.salI30 = salI30;
    }

    public String getSalI30DD() {
        return "EmployeeSalary_salI30";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI31">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI31;

    public BigDecimal getSalI31() {
        return salI31;
    }

    public void setSalI31(BigDecimal salI31) {
        this.salI31 = salI31;
    }

    public String getSalI31DD() {
        return "EmployeeSalary_salI31";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI32">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI32;

    public BigDecimal getSalI32() {
        return salI32;
    }

    public void setSalI32(BigDecimal salI32) {
        this.salI32 = salI32;
    }

    public String getSalI32DD() {
        return "EmployeeSalary_salI32";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI33">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI33;

    public BigDecimal getSalI33() {
        return salI33;
    }

    public void setSalI33(BigDecimal salI33) {
        this.salI33 = salI33;
    }

    public String getSalI33DD() {
        return "EmployeeSalary_salI33";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI34">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI34;

    public BigDecimal getSalI34() {
        return salI34;
    }

    public void setSalI34(BigDecimal salI34) {
        this.salI34 = salI34;
    }

    public String getSalI34DD() {
        return "EmployeeSalary_salI34";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI35">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI35;

    public BigDecimal getSalI35() {
        return salI35;
    }

    public void setSalI35(BigDecimal salI35) {
        this.salI35 = salI35;
    }

    public String getSalI35DD() {
        return "EmployeeSalary_salI35";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI36">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI36;

    public BigDecimal getSalI36() {
        return salI36;
    }

    public void setSalI36(BigDecimal salI36) {
        this.salI36 = salI36;
    }

    public String getSalI36DD() {
        return "EmployeeSalary_salI36";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI37">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI37;

    public BigDecimal getSalI37() {
        return salI37;
    }

    public void setSalI37(BigDecimal salI37) {
        this.salI37 = salI37;
    }

    public String getSalI37DD() {
        return "EmployeeSalary_salI37";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI38">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI38;

    public BigDecimal getSalI38() {
        return salI38;
    }

    public void setSalI38(BigDecimal salI38) {
        this.salI38 = salI38;
    }

    public String getSalI38DD() {
        return "EmployeeSalary_salI38";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI39">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI39;

    public BigDecimal getSalI39() {
        return salI39;
    }

    public void setSalI39(BigDecimal salI39) {
        this.salI39 = salI39;
    }

    public String getSalI39DD() {
        return "EmployeeSalary_salI39";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI40">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI40;

    public BigDecimal getSalI40() {
        return salI40;
    }

    public void setSalI40(BigDecimal salI40) {
        this.salI40 = salI40;
    }

    public String getSalI40DD() {
        return "EmployeeSalary_salI40";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI41">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI41;

    public BigDecimal getSalI41() {
        return salI41;
    }

    public void setSalI41(BigDecimal salI41) {
        this.salI41 = salI41;
    }

    public String getSalI41DD() {
        return "EmployeeSalary_salI41";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI42">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI42;

    public BigDecimal getSalI42() {
        return salI42;
    }

    public void setSalI42(BigDecimal salI42) {
        this.salI42 = salI42;
    }

    public String getSalI42DD() {
        return "EmployeeSalary_salI42";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI43">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI43;

    public BigDecimal getSalI43() {
        return salI43;
    }

    public void setSalI43(BigDecimal salI43) {
        this.salI43 = salI43;
    }

    public String getSalI43DD() {
        return "EmployeeSalary_salI43";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI44">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI44;

    public BigDecimal getSalI44() {
        return salI44;
    }

    public void setSalI44(BigDecimal salI44) {
        this.salI44 = salI44;
    }

    public String getSalI44DD() {
        return "EmployeeSalary_salI44";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI45">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI45;

    public BigDecimal getSalI45() {
        return salI45;
    }

    public void setSalI45(BigDecimal salI45) {
        this.salI45 = salI45;
    }

    public String getSalI45DD() {
        return "EmployeeSalary_salI45";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI46">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI46;

    public BigDecimal getSalI46() {
        return salI46;
    }

    public void setSalI46(BigDecimal salI46) {
        this.salI46 = salI46;
    }

    public String getSalI46DD() {
        return "EmployeeSalary_salI46";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI47">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI47;

    public BigDecimal getSalI47() {
        return salI47;
    }

    public void setSalI47(BigDecimal salI47) {
        this.salI47 = salI47;
    }

    public String getSalI47DD() {
        return "EmployeeSalary_salI47";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI48">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI48;

    public BigDecimal getSalI48() {
        return salI48;
    }

    public void setSalI48(BigDecimal salI48) {
        this.salI48 = salI48;
    }

    public String getSalI48DD() {
        return "EmployeeSalary_salI48";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI49">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI49;

    public BigDecimal getSalI49() {
        return salI49;
    }

    public void setSalI49(BigDecimal salI49) {
        this.salI49 = salI49;
    }

    public String getSalI49DD() {
        return "EmployeeSalary_salI49";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI50">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI50;

    public BigDecimal getSalI50() {
        return salI50;
    }

    public void setSalI50(BigDecimal salI50) {
        this.salI50 = salI50;
    }

    public String getSalI50DD() {
        return "EmployeeSalary_salI50";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI51">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI51;

    public BigDecimal getSalI51() {
        return salI51;
    }

    public void setSalI51(BigDecimal salI51) {
        this.salI51 = salI51;
    }

    public String getSalI51DD() {
        return "EmployeeSalary_salI51";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI52">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI52;

    public BigDecimal getSalI52() {
        return salI52;
    }

    public void setSalI52(BigDecimal salI52) {
        this.salI52 = salI52;
    }

    public String getSalI52DD() {
        return "EmployeeSalary_salI52";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI53">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI53;

    public BigDecimal getSalI53() {
        return salI53;
    }

    public void setSalI53(BigDecimal salI53) {
        this.salI53 = salI53;
    }

    public String getSalI53DD() {
        return "EmployeeSalary_salI53";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI54">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI54;

    public BigDecimal getSalI54() {
        return salI54;
    }

    public void setSalI54(BigDecimal salI54) {
        this.salI54 = salI54;
    }

    public String getSalI54DD() {
        return "EmployeeSalary_salI54";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI55">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI55;

    public BigDecimal getSalI55() {
        return salI55;
    }

    public void setSalI55(BigDecimal salI55) {
        this.salI55 = salI55;
    }

    public String getSalI55DD() {
        return "EmployeeSalary_salI55";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI56">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI56;

    public BigDecimal getSalI56() {
        return salI56;
    }

    public void setSalI56(BigDecimal salI56) {
        this.salI56 = salI56;
    }

    public String getSalI56DD() {
        return "EmployeeSalary_salI56";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI57">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI57;

    public BigDecimal getSalI57() {
        return salI57;
    }

    public void setSalI57(BigDecimal salI57) {
        this.salI57 = salI57;
    }

    public String getSalI57DD() {
        return "EmployeeSalary_salI57";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI58">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI58;

    public BigDecimal getSalI58() {
        return salI58;
    }

    public void setSalI58(BigDecimal salI58) {
        this.salI58 = salI58;
    }

    public String getSalI58DD() {
        return "EmployeeSalary_salI58";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI59">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI59;

    public BigDecimal getSalI59() {
        return salI59;
    }

    public void setSalI59(BigDecimal salI59) {
        this.salI59 = salI59;
    }

    public String getSalI59DD() {
        return "EmployeeSalary_salI59";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI60">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI60;

    public BigDecimal getSalI60() {
        return salI60;
    }

    public void setSalI60(BigDecimal salI60) {
        this.salI60 = salI60;
    }

    public String getSalI60DD() {
        return "EmployeeSalary_salI60";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI61">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI61;

    public BigDecimal getSalI61() {
        return salI61;
    }

    public void setSalI61(BigDecimal salI61) {
        this.salI61 = salI61;
    }

    public String getSalI61DD() {
        return "EmployeeSalary_salI61";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI62">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI62;

    public BigDecimal getSalI62() {
        return salI62;
    }

    public void setSalI62(BigDecimal salI62) {
        this.salI62 = salI62;
    }

    public String getSalI62DD() {
        return "EmployeeSalary_salI62";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI63">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI63;

    public BigDecimal getSalI63() {
        return salI63;
    }

    public void setSalI63(BigDecimal salI63) {
        this.salI63 = salI63;
    }

    public String getSalI63DD() {
        return "EmployeeSalary_salI63";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI64">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI64;

    public BigDecimal getSalI64() {
        return salI64;
    }

    public void setSalI64(BigDecimal salI64) {
        this.salI64 = salI64;
    }

    public String getSalI64DD() {
        return "EmployeeSalary_salI64";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI65">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI65;

    public BigDecimal getSalI65() {
        return salI65;
    }

    public void setSalI65(BigDecimal salI65) {
        this.salI65 = salI65;
    }

    public String getSalI65DD() {
        return "EmployeeSalary_salI65";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI66">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI66;

    public BigDecimal getSalI66() {
        return salI66;
    }

    public void setSalI66(BigDecimal salI66) {
        this.salI66 = salI66;
    }

    public String getSalI66DD() {
        return "EmployeeSalary_salI66";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI67">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI67;

    public BigDecimal getSalI67() {
        return salI67;
    }

    public void setSalI67(BigDecimal salI67) {
        this.salI67 = salI67;
    }

    public String getSalI67DD() {
        return "EmployeeSalary_salI67";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI68">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI68;

    public BigDecimal getSalI68() {
        return salI68;
    }

    public void setSalI68(BigDecimal salI68) {
        this.salI68 = salI68;
    }

    public String getSalI68DD() {
        return "EmployeeSalary_salI68";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI69">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI69;

    public BigDecimal getSalI69() {
        return salI69;
    }

    public void setSalI69(BigDecimal salI69) {
        this.salI69 = salI69;
    }

    public String getSalI69DD() {
        return "EmployeeSalary_salI69";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI70">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI70;

    public BigDecimal getSalI70() {
        return salI70;
    }

    public void setSalI70(BigDecimal salI70) {
        this.salI70 = salI70;
    }

    public String getSalI70DD() {
        return "EmployeeSalary_salI70";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI71">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI71;

    public BigDecimal getSalI71() {
        return salI71;
    }

    public void setSalI71(BigDecimal salI71) {
        this.salI71 = salI71;
    }

    public String getSalI71DD() {
        return "EmployeeSalary_salI71";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI72">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI72;

    public BigDecimal getSalI72() {
        return salI72;
    }

    public void setSalI72(BigDecimal salI72) {
        this.salI72 = salI72;
    }

    public String getSalI72DD() {
        return "EmployeeSalary_salI72";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI73">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI73;

    public BigDecimal getSalI73() {
        return salI73;
    }

    public void setSalI73(BigDecimal salI73) {
        this.salI73 = salI73;
    }

    public String getSalI73DD() {
        return "EmployeeSalary_salI73";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI74">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI74;

    public BigDecimal getSalI74() {
        return salI74;
    }

    public void setSalI74(BigDecimal salI74) {
        this.salI74 = salI74;
    }

    public String getSalI74DD() {
        return "EmployeeSalary_salI74";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI75">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI75;

    public BigDecimal getSalI75() {
        return salI75;
    }

    public void setSalI75(BigDecimal salI75) {
        this.salI75 = salI75;
    }

    public String getSalI75DD() {
        return "EmployeeSalary_salI75";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI76">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI76;

    public BigDecimal getSalI76() {
        return salI76;
    }

    public void setSalI76(BigDecimal salI76) {
        this.salI76 = salI76;
    }

    public String getSalI76DD() {
        return "EmployeeSalary_salI76";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI77">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI77;

    public BigDecimal getSalI77() {
        return salI77;
    }

    public void setSalI77(BigDecimal salI77) {
        this.salI77 = salI77;
    }

    public String getSalI77DD() {
        return "EmployeeSalary_salI77";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI78">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI78;

    public BigDecimal getSalI78() {
        return salI78;
    }

    public void setSalI78(BigDecimal salI78) {
        this.salI78 = salI78;
    }

    public String getSalI78DD() {
        return "EmployeeSalary_salI78";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI79">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI79;

    public BigDecimal getSalI79() {
        return salI79;
    }

    public void setSalI79(BigDecimal salI79) {
        this.salI79 = salI79;
    }

    public String getSalI79DD() {
        return "EmployeeSalary_salI79";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI80">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI80;

    public BigDecimal getSalI80() {
        return salI80;
    }

    public void setSalI80(BigDecimal salI80) {
        this.salI80 = salI80;
    }

    public String getSalI80DD() {
        return "EmployeeSalary_salI80";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI81">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI81;

    public BigDecimal getSalI81() {
        return salI81;
    }

    public void setSalI81(BigDecimal salI81) {
        this.salI81 = salI81;
    }

    public String getSalI81DD() {
        return "EmployeeSalary_salI81";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI82">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI82;

    public BigDecimal getSalI82() {
        return salI82;
    }

    public void setSalI82(BigDecimal salI82) {
        this.salI82 = salI82;
    }

    public String getSalI82DD() {
        return "EmployeeSalary_salI82";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI83">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI83;

    public BigDecimal getSalI83() {
        return salI83;
    }

    public void setSalI83(BigDecimal salI83) {
        this.salI83 = salI83;
    }

    public String getSalI83DD() {
        return "EmployeeSalary_salI83";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI84">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI84;

    public BigDecimal getSalI84() {
        return salI84;
    }

    public void setSalI84(BigDecimal salI84) {
        this.salI84 = salI84;
    }

    public String getSalI84DD() {
        return "EmployeeSalary_salI84";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI85">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI85;

    public BigDecimal getSalI85() {
        return salI85;
    }

    public void setSalI85(BigDecimal salI85) {
        this.salI85 = salI85;
    }

    public String getSalI85DD() {
        return "EmployeeSalary_salI85";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI86">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI86;

    public BigDecimal getSalI86() {
        return salI86;
    }

    public void setSalI86(BigDecimal salI86) {
        this.salI86 = salI86;
    }

    public String getSalI86DD() {
        return "EmployeeSalary_salI86";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI87">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI87;

    public BigDecimal getSalI87() {
        return salI87;
    }

    public void setSalI87(BigDecimal salI87) {
        this.salI87 = salI87;
    }

    public String getSalI87DD() {
        return "EmployeeSalary_salI87";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI88">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI88;

    public BigDecimal getSalI88() {
        return salI88;
    }

    public void setSalI88(BigDecimal salI88) {
        this.salI88 = salI88;
    }

    public String getSalI88DD() {
        return "EmployeeSalary_salI88";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI89">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI89;

    public BigDecimal getSalI89() {
        return salI89;
    }

    public void setSalI89(BigDecimal salI89) {
        this.salI89 = salI89;
    }

    public String getSalI89DD() {
        return "EmployeeSalary_salI89";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI90">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI90;

    public BigDecimal getSalI90() {
        return salI90;
    }

    public void setSalI90(BigDecimal salI90) {
        this.salI90 = salI90;
    }

    public String getSalI90DD() {
        return "EmployeeSalary_salI90";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI91">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI91;

    public BigDecimal getSalI91() {
        return salI91;
    }

    public void setSalI91(BigDecimal salI91) {
        this.salI91 = salI91;
    }

    public String getSalI91DD() {
        return "EmployeeSalary_salI91";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI92">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI92;

    public BigDecimal getSalI92() {
        return salI92;
    }

    public void setSalI92(BigDecimal salI92) {
        this.salI92 = salI92;
    }

    public String getSalI92DD() {
        return "EmployeeSalary_salI92";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI93">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI93;

    public BigDecimal getSalI93() {
        return salI93;
    }

    public void setSalI93(BigDecimal salI93) {
        this.salI93 = salI93;
    }

    public String getSalI93DD() {
        return "EmployeeSalary_salI93";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI94">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI94;

    public BigDecimal getSalI94() {
        return salI94;
    }

    public void setSalI94(BigDecimal salI94) {
        this.salI94 = salI94;
    }

    public String getSalI94DD() {
        return "EmployeeSalary_salI94";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI95">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI95;

    public BigDecimal getSalI95() {
        return salI95;
    }

    public void setSalI95(BigDecimal salI95) {
        this.salI95 = salI95;
    }

    public String getSalI95DD() {
        return "EmployeeSalary_salI95";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI96">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI96;

    public BigDecimal getSalI96() {
        return salI96;
    }

    public void setSalI96(BigDecimal salI96) {
        this.salI96 = salI96;
    }

    public String getSalI96DD() {
        return "EmployeeSalary_salI96";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI97">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI97;

    public BigDecimal getSalI97() {
        return salI97;
    }

    public void setSalI97(BigDecimal salI97) {
        this.salI97 = salI97;
    }

    public String getSalI97DD() {
        return "EmployeeSalary_salI97";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI98">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI98;

    public BigDecimal getSalI98() {
        return salI98;
    }

    public void setSalI98(BigDecimal salI98) {
        this.salI98 = salI98;
    }

    public String getSalI98DD() {
        return "EmployeeSalary_salI98";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI99">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI99;

    public BigDecimal getSalI99() {
        return salI99;
    }

    public void setSalI99(BigDecimal salI99) {
        this.salI99 = salI99;
    }

    public String getSalI99DD() {
        return "EmployeeSalary_salI99";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI100">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI100;

    public BigDecimal getSalI100() {
        return salI100;
    }

    public void setSalI100(BigDecimal salI100) {
        this.salI100 = salI100;
    }

    public String getSalI100DD() {
        return "EmployeeSalary_salI100";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI101">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI101;

    public BigDecimal getSalI101() {
        return salI101;
    }

    public void setSalI101(BigDecimal salI101) {
        this.salI101 = salI101;
    }

    public String getSalI101DD() {
        return "EmployeeSalary_salI101";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI102">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI102;

    public BigDecimal getSalI102() {
        return salI102;
    }

    public void setSalI102(BigDecimal salI102) {
        this.salI102 = salI102;
    }

    public String getSalI102DD() {
        return "EmployeeSalary_salI102";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI103">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI103;

    public BigDecimal getSalI103() {
        return salI103;
    }

    public void setSalI103(BigDecimal salI103) {
        this.salI103 = salI103;
    }

    public String getSalI103DD() {
        return "EmployeeSalary_salI103";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI104">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI104;

    public BigDecimal getSalI104() {
        return salI104;
    }

    public void setSalI104(BigDecimal salI104) {
        this.salI104 = salI104;
    }

    public String getSalI104DD() {
        return "EmployeeSalary_salI104";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI105">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI105;

    public BigDecimal getSalI105() {
        return salI105;
    }

    public void setSalI105(BigDecimal salI105) {
        this.salI105 = salI105;
    }

    public String getSalI105DD() {
        return "EmployeeSalary_salI105";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI106">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI106;

    public BigDecimal getSalI106() {
        return salI106;
    }

    public void setSalI106(BigDecimal salI106) {
        this.salI106 = salI106;
    }

    public String getSalI106DD() {
        return "EmployeeSalary_salI106";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI107">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI107;

    public BigDecimal getSalI107() {
        return salI107;
    }

    public void setSalI107(BigDecimal salI107) {
        this.salI107 = salI107;
    }

    public String getSalI107DD() {
        return "EmployeeSalary_salI107";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI108">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI108;

    public BigDecimal getSalI108() {
        return salI108;
    }

    public void setSalI108(BigDecimal salI108) {
        this.salI108 = salI108;
    }

    public String getSalI108DD() {
        return "EmployeeSalary_salI108";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI109">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI109;

    public BigDecimal getSalI109() {
        return salI109;
    }

    public void setSalI109(BigDecimal salI109) {
        this.salI109 = salI109;
    }

    public String getSalI109DD() {
        return "EmployeeSalary_salI109";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI110">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI110;

    public BigDecimal getSalI110() {
        return salI110;
    }

    public void setSalI110(BigDecimal salI110) {
        this.salI110 = salI110;
    }

    public String getSalI110DD() {
        return "EmployeeSalary_salI110";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI111">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI111;

    public BigDecimal getSalI111() {
        return salI111;
    }

    public void setSalI111(BigDecimal salI111) {
        this.salI111 = salI111;
    }

    public String getSalI111DD() {
        return "EmployeeSalary_salI111";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI112">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI112;

    public BigDecimal getSalI112() {
        return salI112;
    }

    public void setSalI112(BigDecimal salI112) {
        this.salI112 = salI112;
    }

    public String getSalI112DD() {
        return "EmployeeSalary_salI112";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI113">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI113;

    public BigDecimal getSalI113() {
        return salI113;
    }

    public void setSalI113(BigDecimal salI113) {
        this.salI113 = salI113;
    }

    public String getSalI113DD() {
        return "EmployeeSalary_salI113";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI114">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI114;

    public BigDecimal getSalI114() {
        return salI114;
    }

    public void setSalI114(BigDecimal salI114) {
        this.salI114 = salI114;
    }

    public String getSalI114DD() {
        return "EmployeeSalary_salI114";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI115">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI115;

    public BigDecimal getSalI115() {
        return salI115;
    }

    public void setSalI115(BigDecimal salI115) {
        this.salI115 = salI115;
    }

    public String getSalI115DD() {
        return "EmployeeSalary_salI115";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI116">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI116;

    public BigDecimal getSalI116() {
        return salI116;
    }

    public void setSalI116(BigDecimal salI116) {
        this.salI116 = salI116;
    }

    public String getSalI116DD() {
        return "EmployeeSalary_salI116";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI117">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI117;

    public BigDecimal getSalI117() {
        return salI117;
    }

    public void setSalI117(BigDecimal salI117) {
        this.salI117 = salI117;
    }

    public String getSalI117DD() {
        return "EmployeeSalary_salI117";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI118">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI118;

    public BigDecimal getSalI118() {
        return salI118;
    }

    public void setSalI118(BigDecimal salI118) {
        this.salI118 = salI118;
    }

    public String getSalI118DD() {
        return "EmployeeSalary_salI118";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI119">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI119;

    public BigDecimal getSalI119() {
        return salI119;
    }

    public void setSalI119(BigDecimal salI119) {
        this.salI119 = salI119;
    }

    public String getSalI119DD() {
        return "EmployeeSalary_salI119";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI120">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI120;

    public BigDecimal getSalI120() {
        return salI120;
    }

    public void setSalI120(BigDecimal salI120) {
        this.salI120 = salI120;
    }

    public String getSalI120DD() {
        return "EmployeeSalary_salI120";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI121">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI121;

    public BigDecimal getSalI121() {
        return salI121;
    }

    public void setSalI121(BigDecimal salI121) {
        this.salI121 = salI121;
    }

    public String getSalI121DD() {
        return "EmployeeSalary_salI121";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI122">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI122;

    public BigDecimal getSalI122() {
        return salI122;
    }

    public void setSalI122(BigDecimal salI122) {
        this.salI122 = salI122;
    }

    public String getSalI122DD() {
        return "EmployeeSalary_salI122";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI123">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI123;

    public BigDecimal getSalI123() {
        return salI123;
    }

    public void setSalI123(BigDecimal salI123) {
        this.salI123 = salI123;
    }

    public String getSalI123DD() {
        return "EmployeeSalary_salI123";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI124">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI124;

    public BigDecimal getSalI124() {
        return salI124;
    }

    public void setSalI124(BigDecimal salI124) {
        this.salI124 = salI124;
    }

    public String getSalI124DD() {
        return "EmployeeSalary_salI124";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI125">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI125;

    public BigDecimal getSalI125() {
        return salI125;
    }

    public void setSalI125(BigDecimal salI125) {
        this.salI125 = salI125;
    }

    public String getSalI125DD() {
        return "EmployeeSalary_salI125";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI126">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI126;

    public BigDecimal getSalI126() {
        return salI126;
    }

    public void setSalI126(BigDecimal salI126) {
        this.salI126 = salI126;
    }

    public String getSalI126DD() {
        return "EmployeeSalary_salI126";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI127">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI127;

    public BigDecimal getSalI127() {
        return salI127;
    }

    public void setSalI127(BigDecimal salI127) {
        this.salI127 = salI127;
    }

    public String getSalI127DD() {
        return "EmployeeSalary_salI127";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI128">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI128;

    public BigDecimal getSalI128() {
        return salI128;
    }

    public void setSalI128(BigDecimal salI128) {
        this.salI128 = salI128;
    }

    public String getSalI128DD() {
        return "EmployeeSalary_salI128";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI129">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI129;

    public BigDecimal getSalI129() {
        return salI129;
    }

    public void setSalI129(BigDecimal salI129) {
        this.salI129 = salI129;
    }

    public String getSalI129DD() {
        return "EmployeeSalary_salI129";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI130">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI130;

    public BigDecimal getSalI130() {
        return salI130;
    }

    public void setSalI130(BigDecimal salI130) {
        this.salI130 = salI130;
    }

    public String getSalI130DD() {
        return "EmployeeSalary_salI130";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI131">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI131;

    public BigDecimal getSalI131() {
        return salI131;
    }

    public void setSalI131(BigDecimal salI131) {
        this.salI131 = salI131;
    }

    public String getSalI131DD() {
        return "EmployeeSalary_salI131";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI132">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI132;

    public BigDecimal getSalI132() {
        return salI132;
    }

    public void setSalI132(BigDecimal salI132) {
        this.salI132 = salI132;
    }

    public String getSalI132DD() {
        return "EmployeeSalary_salI132";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI133">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI133;

    public BigDecimal getSalI133() {
        return salI133;
    }

    public void setSalI133(BigDecimal salI133) {
        this.salI133 = salI133;
    }

    public String getSalI133DD() {
        return "EmployeeSalary_salI133";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI134">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI134;

    public BigDecimal getSalI134() {
        return salI134;
    }

    public void setSalI134(BigDecimal salI134) {
        this.salI134 = salI134;
    }

    public String getSalI134DD() {
        return "EmployeeSalary_salI134";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI135">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI135;

    public BigDecimal getSalI135() {
        return salI135;
    }

    public void setSalI135(BigDecimal salI135) {
        this.salI135 = salI135;
    }

    public String getSalI135DD() {
        return "EmployeeSalary_salI135";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI136">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI136;

    public BigDecimal getSalI136() {
        return salI136;
    }

    public void setSalI136(BigDecimal salI136) {
        this.salI136 = salI136;
    }

    public String getSalI136DD() {
        return "EmployeeSalary_salI136";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI137">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI137;

    public BigDecimal getSalI137() {
        return salI137;
    }

    public void setSalI137(BigDecimal salI137) {
        this.salI137 = salI137;
    }

    public String getSalI137DD() {
        return "EmployeeSalary_salI137";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI138">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI138;

    public BigDecimal getSalI138() {
        return salI138;
    }

    public void setSalI138(BigDecimal salI138) {
        this.salI138 = salI138;
    }

    public String getSalI138DD() {
        return "EmployeeSalary_salI138";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI139">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI139;

    public BigDecimal getSalI139() {
        return salI139;
    }

    public void setSalI139(BigDecimal salI139) {
        this.salI139 = salI139;
    }

    public String getSalI139DD() {
        return "EmployeeSalary_salI139";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI140">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI140;

    public BigDecimal getSalI140() {
        return salI140;
    }

    public void setSalI140(BigDecimal salI140) {
        this.salI140 = salI140;
    }

    public String getSalI140DD() {
        return "EmployeeSalary_salI140";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI141">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI141;

    public BigDecimal getSalI141() {
        return salI141;
    }

    public void setSalI141(BigDecimal salI141) {
        this.salI141 = salI141;
    }

    public String getSalI141DD() {
        return "EmployeeSalary_salI141";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI142">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI142;

    public BigDecimal getSalI142() {
        return salI142;
    }

    public void setSalI142(BigDecimal salI142) {
        this.salI142 = salI142;
    }

    public String getSalI142DD() {
        return "EmployeeSalary_salI142";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI143">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI143;

    public BigDecimal getSalI143() {
        return salI143;
    }

    public void setSalI143(BigDecimal salI143) {
        this.salI143 = salI143;
    }

    public String getSalI143DD() {
        return "EmployeeSalary_salI143";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI144">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI144;

    public BigDecimal getSalI144() {
        return salI144;
    }

    public void setSalI144(BigDecimal salI144) {
        this.salI144 = salI144;
    }

    public String getSalI144DD() {
        return "EmployeeSalary_salI144";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI145">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI145;

    public BigDecimal getSalI145() {
        return salI145;
    }

    public void setSalI145(BigDecimal salI145) {
        this.salI145 = salI145;
    }

    public String getSalI145DD() {
        return "EmployeeSalary_salI145";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI146">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI146;

    public BigDecimal getSalI146() {
        return salI146;
    }

    public void setSalI146(BigDecimal salI146) {
        this.salI146 = salI146;
    }

    public String getSalI146DD() {
        return "EmployeeSalary_salI146";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI147">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI147;

    public BigDecimal getSalI147() {
        return salI147;
    }

    public void setSalI147(BigDecimal salI147) {
        this.salI147 = salI147;
    }

    public String getSalI147DD() {
        return "EmployeeSalary_salI147";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI148">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI148;

    public BigDecimal getSalI148() {
        return salI148;
    }

    public void setSalI148(BigDecimal salI148) {
        this.salI148 = salI148;
    }

    public String getSalI148DD() {
        return "EmployeeSalary_salI148";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI149">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI149;

    public BigDecimal getSalI149() {
        return salI149;
    }

    public void setSalI149(BigDecimal salI149) {
        this.salI149 = salI149;
    }

    public String getSalI149DD() {
        return "EmployeeSalary_salI149";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salI150">
    @Column(precision = 25, scale = 13)
    private BigDecimal salI150;

    public BigDecimal getSalI150() {
        return salI150;
    }

    public void setSalI150(BigDecimal salI150) {
        this.salI150 = salI150;
    }

    public String getSalI150DD() {
        return "EmployeeSalary_salI150";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD1">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD1;

    public BigDecimal getSalD1() {
        return salD1;
    }

    public void setSalD1(BigDecimal salD1) {
        this.salD1 = salD1;
    }

    public String getSalD1DD() {
        return "EmployeeSalary_salD1";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD2">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD2;

    public BigDecimal getSalD2() {
        return salD2;
    }

    public void setSalD2(BigDecimal salD2) {
        this.salD2 = salD2;
    }

    public String getSalD2DD() {
        return "EmployeeSalary_salD2";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD3">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD3;

    public BigDecimal getSalD3() {
        return salD3;
    }

    public void setSalD3(BigDecimal salD3) {
        this.salD3 = salD3;
    }

    public String getSalD3DD() {
        return "EmployeeSalary_salD3";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD4">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD4;

    public BigDecimal getSalD4() {
        return salD4;
    }

    public void setSalD4(BigDecimal salD4) {
        this.salD4 = salD4;
    }

    public String getSalD4DD() {
        return "EmployeeSalary_salD4";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD5">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD5;

    public BigDecimal getSalD5() {
        return salD5;
    }

    public void setSalD5(BigDecimal salD5) {
        this.salD5 = salD5;
    }

    public String getSalD5DD() {
        return "EmployeeSalary_salD5";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD6">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD6;

    public BigDecimal getSalD6() {
        return salD6;
    }

    public void setSalD6(BigDecimal salD6) {
        this.salD6 = salD6;
    }

    public String getSalD6DD() {
        return "EmployeeSalary_salD6";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD7">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD7;

    public BigDecimal getSalD7() {
        return salD7;
    }

    public void setSalD7(BigDecimal salD7) {
        this.salD7 = salD7;
    }

    public String getSalD7DD() {
        return "EmployeeSalary_salD7";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD8">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD8;

    public BigDecimal getSalD8() {
        return salD8;
    }

    public void setSalD8(BigDecimal salD8) {
        this.salD8 = salD8;
    }

    public String getSalD8DD() {
        return "EmployeeSalary_salD8";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD9">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD9;

    public BigDecimal getSalD9() {
        return salD9;
    }

    public void setSalD9(BigDecimal salD9) {
        this.salD9 = salD9;
    }

    public String getSalD9DD() {
        return "EmployeeSalary_salD9";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD10">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD10;

    public BigDecimal getSalD10() {
        return salD10;
    }

    public void setSalD10(BigDecimal salD10) {
        this.salD10 = salD10;
    }

    public String getSalD10DD() {
        return "EmployeeSalary_salD10";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD11">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD11;

    public BigDecimal getSalD11() {
        return salD11;
    }

    public void setSalD11(BigDecimal salD11) {
        this.salD11 = salD11;
    }

    public String getSalD11DD() {
        return "EmployeeSalary_salD11";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD12">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD12;

    public BigDecimal getSalD12() {
        return salD12;
    }

    public void setSalD12(BigDecimal salD12) {
        this.salD12 = salD12;
    }

    public String getSalD12DD() {
        return "EmployeeSalary_salD12";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD13">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD13;

    public BigDecimal getSalD13() {
        return salD13;
    }

    public void setSalD13(BigDecimal salD13) {
        this.salD13 = salD13;
    }

    public String getSalD13DD() {
        return "EmployeeSalary_salD13";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD14">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD14;

    public BigDecimal getSalD14() {
        return salD14;
    }

    public void setSalD14(BigDecimal salD14) {
        this.salD14 = salD14;
    }

    public String getSalD14DD() {
        return "EmployeeSalary_salD14";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD15">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD15;

    public BigDecimal getSalD15() {
        return salD15;
    }

    public void setSalD15(BigDecimal salD15) {
        this.salD15 = salD15;
    }

    public String getSalD15DD() {
        return "EmployeeSalary_salD15";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD16">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD16;

    public BigDecimal getSalD16() {
        return salD16;
    }

    public void setSalD16(BigDecimal salD16) {
        this.salD16 = salD16;
    }

    public String getSalD16DD() {
        return "EmployeeSalary_salD16";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD17">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD17;

    public BigDecimal getSalD17() {
        return salD17;
    }

    public void setSalD17(BigDecimal salD17) {
        this.salD17 = salD17;
    }

    public String getSalD17DD() {
        return "EmployeeSalary_salD17";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD18">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD18;

    public BigDecimal getSalD18() {
        return salD18;
    }

    public void setSalD18(BigDecimal salD18) {
        this.salD18 = salD18;
    }

    public String getSalD18DD() {
        return "EmployeeSalary_salD18";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD19">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD19;

    public BigDecimal getSalD19() {
        return salD19;
    }

    public void setSalD19(BigDecimal salD19) {
        this.salD19 = salD19;
    }

    public String getSalD19DD() {
        return "EmployeeSalary_salD19";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD20">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD20;

    public BigDecimal getSalD20() {
        return salD20;
    }

    public void setSalD20(BigDecimal salD20) {
        this.salD20 = salD20;
    }

    public String getSalD20DD() {
        return "EmployeeSalary_salD20";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD21">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD21;

    public BigDecimal getSalD21() {
        return salD21;
    }

    public void setSalD21(BigDecimal salD21) {
        this.salD21 = salD21;
    }

    public String getSalD21DD() {
        return "EmployeeSalary_salD21";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD22">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD22;

    public BigDecimal getSalD22() {
        return salD22;
    }

    public void setSalD22(BigDecimal salD22) {
        this.salD22 = salD22;
    }

    public String getSalD22DD() {
        return "EmployeeSalary_salD22";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD23">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD23;

    public BigDecimal getSalD23() {
        return salD23;
    }

    public void setSalD23(BigDecimal salD23) {
        this.salD23 = salD23;
    }

    public String getSalD23DD() {
        return "EmployeeSalary_salD23";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD24">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD24;

    public BigDecimal getSalD24() {
        return salD24;
    }

    public void setSalD24(BigDecimal salD24) {
        this.salD24 = salD24;
    }

    public String getSalD24DD() {
        return "EmployeeSalary_salD24";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD25">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD25;

    public BigDecimal getSalD25() {
        return salD25;
    }

    public void setSalD25(BigDecimal salD25) {
        this.salD25 = salD25;
    }

    public String getSalD25DD() {
        return "EmployeeSalary_salD25";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD26">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD26;

    public BigDecimal getSalD26() {
        return salD26;
    }

    public void setSalD26(BigDecimal salD26) {
        this.salD26 = salD26;
    }

    public String getSalD26DD() {
        return "EmployeeSalary_salD26";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD27">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD27;

    public BigDecimal getSalD27() {
        return salD27;
    }

    public void setSalD27(BigDecimal salD27) {
        this.salD27 = salD27;
    }

    public String getSalD27DD() {
        return "EmployeeSalary_salD27";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD28">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD28;

    public BigDecimal getSalD28() {
        return salD28;
    }

    public void setSalD28(BigDecimal salD28) {
        this.salD28 = salD28;
    }

    public String getSalD28DD() {
        return "EmployeeSalary_salD28";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD29">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD29;

    public BigDecimal getSalD29() {
        return salD29;
    }

    public void setSalD29(BigDecimal salD29) {
        this.salD29 = salD29;
    }

    public String getSalD29DD() {
        return "EmployeeSalary_salD29";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD30">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD30;

    public BigDecimal getSalD30() {
        return salD30;
    }

    public void setSalD30(BigDecimal salD30) {
        this.salD30 = salD30;
    }

    public String getSalD30DD() {
        return "EmployeeSalary_salD30";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD31">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD31;

    public BigDecimal getSalD31() {
        return salD31;
    }

    public void setSalD31(BigDecimal salD31) {
        this.salD31 = salD31;
    }

    public String getSalD31DD() {
        return "EmployeeSalary_salD31";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD32">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD32;

    public BigDecimal getSalD32() {
        return salD32;
    }

    public void setSalD32(BigDecimal salD32) {
        this.salD32 = salD32;
    }

    public String getSalD32DD() {
        return "EmployeeSalary_salD32";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD33">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD33;

    public BigDecimal getSalD33() {
        return salD33;
    }

    public void setSalD33(BigDecimal salD33) {
        this.salD33 = salD33;
    }

    public String getSalD33DD() {
        return "EmployeeSalary_salD33";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD34">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD34;

    public BigDecimal getSalD34() {
        return salD34;
    }

    public void setSalD34(BigDecimal salD34) {
        this.salD34 = salD34;
    }

    public String getSalD34DD() {
        return "EmployeeSalary_salD34";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD35">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD35;

    public BigDecimal getSalD35() {
        return salD35;
    }

    public void setSalD35(BigDecimal salD35) {
        this.salD35 = salD35;
    }

    public String getSalD35DD() {
        return "EmployeeSalary_salD35";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD36">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD36;

    public BigDecimal getSalD36() {
        return salD36;
    }

    public void setSalD36(BigDecimal salD36) {
        this.salD36 = salD36;
    }

    public String getSalD36DD() {
        return "EmployeeSalary_salD36";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD37">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD37;

    public BigDecimal getSalD37() {
        return salD37;
    }

    public void setSalD37(BigDecimal salD37) {
        this.salD37 = salD37;
    }

    public String getSalD37DD() {
        return "EmployeeSalary_salD37";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD38">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD38;

    public BigDecimal getSalD38() {
        return salD38;
    }

    public void setSalD38(BigDecimal salD38) {
        this.salD38 = salD38;
    }

    public String getSalD38DD() {
        return "EmployeeSalary_salD38";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD39">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD39;

    public BigDecimal getSalD39() {
        return salD39;
    }

    public void setSalD39(BigDecimal salD39) {
        this.salD39 = salD39;
    }

    public String getSalD39DD() {
        return "EmployeeSalary_salD39";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD40">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD40;

    public BigDecimal getSalD40() {
        return salD40;
    }

    public void setSalD40(BigDecimal salD40) {
        this.salD40 = salD40;
    }

    public String getSalD40DD() {
        return "EmployeeSalary_salD40";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD41">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD41;

    public BigDecimal getSalD41() {
        return salD41;
    }

    public void setSalD41(BigDecimal salD41) {
        this.salD41 = salD41;
    }

    public String getSalD41DD() {
        return "EmployeeSalary_salD41";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD42">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD42;

    public BigDecimal getSalD42() {
        return salD42;
    }

    public void setSalD42(BigDecimal salD42) {
        this.salD42 = salD42;
    }

    public String getSalD42DD() {
        return "EmployeeSalary_salD42";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD43">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD43;

    public BigDecimal getSalD43() {
        return salD43;
    }

    public void setSalD43(BigDecimal salD43) {
        this.salD43 = salD43;
    }

    public String getSalD43DD() {
        return "EmployeeSalary_salD43";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD44">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD44;

    public BigDecimal getSalD44() {
        return salD44;
    }

    public void setSalD44(BigDecimal salD44) {
        this.salD44 = salD44;
    }

    public String getSalD44DD() {
        return "EmployeeSalary_salD44";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD45">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD45;

    public BigDecimal getSalD45() {
        return salD45;
    }

    public void setSalD45(BigDecimal salD45) {
        this.salD45 = salD45;
    }

    public String getSalD45DD() {
        return "EmployeeSalary_salD45";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD46">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD46;

    public BigDecimal getSalD46() {
        return salD46;
    }

    public void setSalD46(BigDecimal salD46) {
        this.salD46 = salD46;
    }

    public String getSalD46DD() {
        return "EmployeeSalary_salD46";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD47">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD47;

    public BigDecimal getSalD47() {
        return salD47;
    }

    public void setSalD47(BigDecimal salD47) {
        this.salD47 = salD47;
    }

    public String getSalD47DD() {
        return "EmployeeSalary_salD47";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD48">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD48;

    public BigDecimal getSalD48() {
        return salD48;
    }

    public void setSalD48(BigDecimal salD48) {
        this.salD48 = salD48;
    }

    public String getSalD48DD() {
        return "EmployeeSalary_salD48";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD49">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD49;

    public BigDecimal getSalD49() {
        return salD49;
    }

    public void setSalD49(BigDecimal salD49) {
        this.salD49 = salD49;
    }

    public String getSalD49DD() {
        return "EmployeeSalary_salD49";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD50">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD50;

    public BigDecimal getSalD50() {
        return salD50;
    }

    public void setSalD50(BigDecimal salD50) {
        this.salD50 = salD50;
    }

    public String getSalD50DD() {
        return "EmployeeSalary_salD50";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD51">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD51;

    public BigDecimal getSalD51() {
        return salD51;
    }

    public void setSalD51(BigDecimal salD51) {
        this.salD51 = salD51;
    }

    public String getSalD51DD() {
        return "EmployeeSalary_salD51";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD52">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD52;

    public BigDecimal getSalD52() {
        return salD52;
    }

    public void setSalD52(BigDecimal salD52) {
        this.salD52 = salD52;
    }

    public String getSalD52DD() {
        return "EmployeeSalary_salD52";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD53">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD53;

    public BigDecimal getSalD53() {
        return salD53;
    }

    public void setSalD53(BigDecimal salD53) {
        this.salD53 = salD53;
    }

    public String getSalD53DD() {
        return "EmployeeSalary_salD53";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD54">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD54;

    public BigDecimal getSalD54() {
        return salD54;
    }

    public void setSalD54(BigDecimal salD54) {
        this.salD54 = salD54;
    }

    public String getSalD54DD() {
        return "EmployeeSalary_salD54";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD55">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD55;

    public BigDecimal getSalD55() {
        return salD55;
    }

    public void setSalD55(BigDecimal salD55) {
        this.salD55 = salD55;
    }

    public String getSalD55DD() {
        return "EmployeeSalary_salD55";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD56">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD56;

    public BigDecimal getSalD56() {
        return salD56;
    }

    public void setSalD56(BigDecimal salD56) {
        this.salD56 = salD56;
    }

    public String getSalD56DD() {
        return "EmployeeSalary_salD56";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD57">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD57;

    public BigDecimal getSalD57() {
        return salD57;
    }

    public void setSalD57(BigDecimal salD57) {
        this.salD57 = salD57;
    }

    public String getSalD57DD() {
        return "EmployeeSalary_salD57";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD58">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD58;

    public BigDecimal getSalD58() {
        return salD58;
    }

    public void setSalD58(BigDecimal salD58) {
        this.salD58 = salD58;
    }

    public String getSalD58DD() {
        return "EmployeeSalary_salD58";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD59">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD59;

    public BigDecimal getSalD59() {
        return salD59;
    }

    public void setSalD59(BigDecimal salD59) {
        this.salD59 = salD59;
    }

    public String getSalD59DD() {
        return "EmployeeSalary_salD59";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD60">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD60;

    public BigDecimal getSalD60() {
        return salD60;
    }

    public void setSalD60(BigDecimal salD60) {
        this.salD60 = salD60;
    }

    public String getSalD60DD() {
        return "EmployeeSalary_salD60";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD61">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD61;

    public BigDecimal getSalD61() {
        return salD61;
    }

    public void setSalD61(BigDecimal salD61) {
        this.salD61 = salD61;
    }

    public String getSalD61DD() {
        return "EmployeeSalary_salD61";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD62">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD62;

    public BigDecimal getSalD62() {
        return salD62;
    }

    public void setSalD62(BigDecimal salD62) {
        this.salD62 = salD62;
    }

    public String getSalD62DD() {
        return "EmployeeSalary_salD62";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD63">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD63;

    public BigDecimal getSalD63() {
        return salD63;
    }

    public void setSalD63(BigDecimal salD63) {
        this.salD63 = salD63;
    }

    public String getSalD63DD() {
        return "EmployeeSalary_salD63";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD64">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD64;

    public BigDecimal getSalD64() {
        return salD64;
    }

    public void setSalD64(BigDecimal salD64) {
        this.salD64 = salD64;
    }

    public String getSalD64DD() {
        return "EmployeeSalary_salD64";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD65">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD65;

    public BigDecimal getSalD65() {
        return salD65;
    }

    public void setSalD65(BigDecimal salD65) {
        this.salD65 = salD65;
    }

    public String getSalD65DD() {
        return "EmployeeSalary_salD65";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD66">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD66;

    public BigDecimal getSalD66() {
        return salD66;
    }

    public void setSalD66(BigDecimal salD66) {
        this.salD66 = salD66;
    }

    public String getSalD66DD() {
        return "EmployeeSalary_salD66";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD67">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD67;

    public BigDecimal getSalD67() {
        return salD67;
    }

    public void setSalD67(BigDecimal salD67) {
        this.salD67 = salD67;
    }

    public String getSalD67DD() {
        return "EmployeeSalary_salD67";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD68">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD68;

    public BigDecimal getSalD68() {
        return salD68;
    }

    public void setSalD68(BigDecimal salD68) {
        this.salD68 = salD68;
    }

    public String getSalD68DD() {
        return "EmployeeSalary_salD68";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD69">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD69;

    public BigDecimal getSalD69() {
        return salD69;
    }

    public void setSalD69(BigDecimal salD69) {
        this.salD69 = salD69;
    }

    public String getSalD69DD() {
        return "EmployeeSalary_salD69";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD70">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD70;

    public BigDecimal getSalD70() {
        return salD70;
    }

    public void setSalD70(BigDecimal salD70) {
        this.salD70 = salD70;
    }

    public String getSalD70DD() {
        return "EmployeeSalary_salD70";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD71">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD71;

    public BigDecimal getSalD71() {
        return salD71;
    }

    public void setSalD71(BigDecimal salD71) {
        this.salD71 = salD71;
    }

    public String getSalD71DD() {
        return "EmployeeSalary_salD71";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD72">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD72;

    public BigDecimal getSalD72() {
        return salD72;
    }

    public void setSalD72(BigDecimal salD72) {
        this.salD72 = salD72;
    }

    public String getSalD72DD() {
        return "EmployeeSalary_salD72";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD73">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD73;

    public BigDecimal getSalD73() {
        return salD73;
    }

    public void setSalD73(BigDecimal salD73) {
        this.salD73 = salD73;
    }

    public String getSalD73DD() {
        return "EmployeeSalary_salD73";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD74">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD74;

    public BigDecimal getSalD74() {
        return salD74;
    }

    public void setSalD74(BigDecimal salD74) {
        this.salD74 = salD74;
    }

    public String getSalD74DD() {
        return "EmployeeSalary_salD74";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD75">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD75;

    public BigDecimal getSalD75() {
        return salD75;
    }

    public void setSalD75(BigDecimal salD75) {
        this.salD75 = salD75;
    }

    public String getSalD75DD() {
        return "EmployeeSalary_salD75";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD76">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD76;

    public BigDecimal getSalD76() {
        return salD76;
    }

    public void setSalD76(BigDecimal salD76) {
        this.salD76 = salD76;
    }

    public String getSalD76DD() {
        return "EmployeeSalary_salD76";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD77">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD77;

    public BigDecimal getSalD77() {
        return salD77;
    }

    public void setSalD77(BigDecimal salD77) {
        this.salD77 = salD77;
    }

    public String getSalD77DD() {
        return "EmployeeSalary_salD77";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD78">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD78;

    public BigDecimal getSalD78() {
        return salD78;
    }

    public void setSalD78(BigDecimal salD78) {
        this.salD78 = salD78;
    }

    public String getSalD78DD() {
        return "EmployeeSalary_salD78";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD79">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD79;

    public BigDecimal getSalD79() {
        return salD79;
    }

    public void setSalD79(BigDecimal salD79) {
        this.salD79 = salD79;
    }

    public String getSalD79DD() {
        return "EmployeeSalary_salD79";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD80">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD80;

    public BigDecimal getSalD80() {
        return salD80;
    }

    public void setSalD80(BigDecimal salD80) {
        this.salD80 = salD80;
    }

    public String getSalD80DD() {
        return "EmployeeSalary_salD80";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD81">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD81;

    public BigDecimal getSalD81() {
        return salD81;
    }

    public void setSalD81(BigDecimal salD81) {
        this.salD81 = salD81;
    }

    public String getSalD81DD() {
        return "EmployeeSalary_salD81";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD82">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD82;

    public BigDecimal getSalD82() {
        return salD82;
    }

    public void setSalD82(BigDecimal salD82) {
        this.salD82 = salD82;
    }

    public String getSalD82DD() {
        return "EmployeeSalary_salD82";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD83">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD83;

    public BigDecimal getSalD83() {
        return salD83;
    }

    public void setSalD83(BigDecimal salD83) {
        this.salD83 = salD83;
    }

    public String getSalD83DD() {
        return "EmployeeSalary_salD83";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD84">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD84;

    public BigDecimal getSalD84() {
        return salD84;
    }

    public void setSalD84(BigDecimal salD84) {
        this.salD84 = salD84;
    }

    public String getSalD84DD() {
        return "EmployeeSalary_salD84";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="salD85">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD85;

    public BigDecimal getSalD85() {
        return salD85;
    }

    public void setSalD85(BigDecimal salD85) {
        this.salD85 = salD85;
    }

    public String getSalD85DD() {
        return "EmployeeSalary_salD85";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD86">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD86;

    public BigDecimal getSalD86() {
        return salD86;
    }

    public void setSalD86(BigDecimal salD86) {
        this.salD86 = salD86;
    }

    public String getSalD86DD() {
        return "EmployeeSalary_salD86";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD87">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD87;

    public BigDecimal getSalD87() {
        return salD87;
    }

    public void setSalD87(BigDecimal salD87) {
        this.salD87 = salD87;
    }

    public String getSalD87DD() {
        return "EmployeeSalary_salD87";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD88">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD88;

    public BigDecimal getSalD88() {
        return salD88;
    }

    public void setSalD88(BigDecimal salD88) {
        this.salD88 = salD88;
    }

    public String getSalD88DD() {
        return "EmployeeSalary_salD88";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD89">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD89;

    public BigDecimal getSalD89() {
        return salD89;
    }

    public void setSalD89(BigDecimal salD89) {
        this.salD89 = salD89;
    }

    public String getSalD89DD() {
        return "EmployeeSalary_salD89";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD90">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD90;

    public BigDecimal getSalD90() {
        return salD90;
    }

    public void setSalD90(BigDecimal salD90) {
        this.salD90 = salD90;
    }

    public String getSalD90DD() {
        return "EmployeeSalary_salD90";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD91">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD91;

    public BigDecimal getSalD91() {
        return salD91;
    }

    public void setSalD91(BigDecimal salD91) {
        this.salD91 = salD91;
    }

    public String getSalD91DD() {
        return "EmployeeSalary_salD91";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD92">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD92;

    public BigDecimal getSalD92() {
        return salD92;
    }

    public void setSalD92(BigDecimal salD92) {
        this.salD92 = salD92;
    }

    public String getSalD92DD() {
        return "EmployeeSalary_salD92";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD93">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD93;

    public BigDecimal getSalD93() {
        return salD93;
    }

    public void setSalD93(BigDecimal salD93) {
        this.salD93 = salD93;
    }

    public String getSalD93DD() {
        return "EmployeeSalary_salD93";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD94">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD94;

    public BigDecimal getSalD94() {
        return salD94;
    }

    public void setSalD94(BigDecimal salD94) {
        this.salD94 = salD94;
    }

    public String getSalD94DD() {
        return "EmployeeSalary_salD94";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD95">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD95;

    public BigDecimal getSalD95() {
        return salD95;
    }

    public void setSalD95(BigDecimal salD95) {
        this.salD95 = salD95;
    }

    public String getSalD95DD() {
        return "EmployeeSalary_salD95";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD96">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD96;

    public BigDecimal getSalD96() {
        return salD96;
    }

    public void setSalD96(BigDecimal salD96) {
        this.salD96 = salD96;
    }

    public String getSalD96DD() {
        return "EmployeeSalary_salD96";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD97">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD97;

    public BigDecimal getSalD97() {
        return salD97;
    }

    public void setSalD97(BigDecimal salD97) {
        this.salD97 = salD97;
    }

    public String getSalD97DD() {
        return "EmployeeSalary_salD97";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD98">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD98;

    public BigDecimal getSalD98() {
        return salD98;
    }

    public void setSalD98(BigDecimal salD98) {
        this.salD98 = salD98;
    }

    public String getSalD98DD() {
        return "EmployeeSalary_salD98";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD99">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD99;

    public BigDecimal getSalD99() {
        return salD99;
    }

    public void setSalD99(BigDecimal salD99) {
        this.salD99 = salD99;
    }

    public String getSalD99DD() {
        return "EmployeeSalary_salD99";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD100">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD100;

    public BigDecimal getSalD100() {
        return salD100;
    }

    public void setSalD100(BigDecimal salD100) {
        this.salD100 = salD100;
    }

    public String getSalD100DD() {
        return "EmployeeSalary_salD100";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD101">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD101;

    public BigDecimal getSalD101() {
        return salD101;
    }

    public void setSalD101(BigDecimal salD101) {
        this.salD101 = salD101;
    }

    public String getSalD101DD() {
        return "EmployeeSalary_salD101";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD102">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD102;

    public BigDecimal getSalD102() {
        return salD102;
    }

    public void setSalD102(BigDecimal salD102) {
        this.salD102 = salD102;
    }

    public String getSalD102DD() {
        return "EmployeeSalary_salD102";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD103">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD103;

    public BigDecimal getSalD103() {
        return salD103;
    }

    public void setSalD103(BigDecimal salD103) {
        this.salD103 = salD103;
    }

    public String getSalD103DD() {
        return "EmployeeSalary_salD103";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD104">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD104;

    public BigDecimal getSalD104() {
        return salD104;
    }

    public void setSalD104(BigDecimal salD104) {
        this.salD104 = salD104;
    }

    public String getSalD104DD() {
        return "EmployeeSalary_salD104";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD105">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD105;

    public BigDecimal getSalD105() {
        return salD105;
    }

    public void setSalD105(BigDecimal salD105) {
        this.salD105 = salD105;
    }

    public String getSalD105DD() {
        return "EmployeeSalary_salD105";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD106">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD106;

    public BigDecimal getSalD106() {
        return salD106;
    }

    public void setSalD106(BigDecimal salD106) {
        this.salD106 = salD106;
    }

    public String getSalD106DD() {
        return "EmployeeSalary_salD106";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD107">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD107;

    public BigDecimal getSalD107() {
        return salD107;
    }

    public void setSalD107(BigDecimal salD107) {
        this.salD107 = salD107;
    }

    public String getSalD107DD() {
        return "EmployeeSalary_salD107";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD108">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD108;

    public BigDecimal getSalD108() {
        return salD108;
    }

    public void setSalD108(BigDecimal salD108) {
        this.salD108 = salD108;
    }

    public String getSalD108DD() {
        return "EmployeeSalary_salD108";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD109">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD109;

    public BigDecimal getSalD109() {
        return salD109;
    }

    public void setSalD109(BigDecimal salD109) {
        this.salD109 = salD109;
    }

    public String getSalD109DD() {
        return "EmployeeSalary_salD109";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD110">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD110;

    public BigDecimal getSalD110() {
        return salD110;
    }

    public void setSalD110(BigDecimal salD110) {
        this.salD110 = salD110;
    }

    public String getSalD110DD() {
        return "EmployeeSalary_salD110";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD111">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD111;

    public BigDecimal getSalD111() {
        return salD111;
    }

    public void setSalD111(BigDecimal salD111) {
        this.salD111 = salD111;
    }

    public String getSalD111D() {
        return "EmployeeSalary_salD111";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD112">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD112;

    public BigDecimal getSalD112() {
        return salD112;
    }

    public void setSalD112(BigDecimal salD112) {
        this.salD112 = salD112;
    }

    public String getSalD112DD() {
        return "EmployeeSalary_salD112";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD113">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD113;

    public BigDecimal getSalD113() {
        return salD113;
    }

    public void setSalD113(BigDecimal salD113) {
        this.salD113 = salD113;
    }

    public String getSalD113DD() {
        return "EmployeeSalary_salD113";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD114">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD114;

    public BigDecimal getSalD114() {
        return salD114;
    }

    public void setSalD114(BigDecimal salD114) {
        this.salD114 = salD114;
    }

    public String getSalD114DD() {
        return "EmployeeSalary_salD114";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD115">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD115;

    public BigDecimal getSalD115() {
        return salD115;
    }

    public void setSalD115(BigDecimal salD115) {
        this.salD115 = salD115;
    }

    public String getSalD115DD() {
        return "EmployeeSalary_salD115";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD116">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD116;

    public BigDecimal getSalD116() {
        return salD116;
    }

    public void setSalD116(BigDecimal salD116) {
        this.salD116 = salD116;
    }

    public String getSalD116DD() {
        return "EmployeeSalary_salD116";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD117">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD117;

    public BigDecimal getSalD117() {
        return salD117;
    }

    public void setSalD117(BigDecimal salD117) {
        this.salD117 = salD117;
    }

    public String getSalD117DD() {
        return "EmployeeSalary_salD117";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD118">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD118;

    public BigDecimal getSalD118() {
        return salD118;
    }

    public void setSalD118(BigDecimal salD118) {
        this.salD118 = salD118;
    }

    public String getSalD118DD() {
        return "EmployeeSalary_salD118";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD119">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD119;

    public BigDecimal getSalD119() {
        return salD119;
    }

    public void setSalD119(BigDecimal salD119) {
        this.salD119 = salD119;
    }

    public String getSalD119DD() {
        return "EmployeeSalary_salD119";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD120">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD120;

    public BigDecimal getSalD120() {
        return salD120;
    }

    public void setSalD120(BigDecimal salD120) {
        this.salD120 = salD120;
    }

    public String getSalD120DD() {
        return "EmployeeSalary_salD120";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD121">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD121;

    public BigDecimal getSalD121() {
        return salD121;
    }

    public void setSalD121(BigDecimal salD121) {
        this.salD121 = salD121;
    }

    public String getSalD121DD() {
        return "EmployeeSalary_salD121";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD122">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD122;

    public BigDecimal getSalD122() {
        return salD122;
    }

    public void setSalD122(BigDecimal salD122) {
        this.salD122 = salD122;
    }

    public String getSalD122DD() {
        return "EmployeeSalary_salD122";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD123">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD123;

    public BigDecimal getSalD123() {
        return salD123;
    }

    public void setSalD123(BigDecimal salD123) {
        this.salD123 = salD123;
    }

    public String getSalD123DD() {
        return "EmployeeSalary_salD123";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD124">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD124;

    public BigDecimal getSalD124() {
        return salD124;
    }

    public void setSalD124(BigDecimal salD124) {
        this.salD124 = salD124;
    }

    public String getSalD124DD() {
        return "EmployeeSalary_salD124";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD125">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD125;

    public BigDecimal getSalD125() {
        return salD125;
    }

    public void setSalD125(BigDecimal salD125) {
        this.salD125 = salD125;
    }

    public String getSalD125DD() {
        return "EmployeeSalary_salD125";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD126">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD126;

    public BigDecimal getSalD126() {
        return salD126;
    }

    public void setSalD126(BigDecimal salD126) {
        this.salD126 = salD126;
    }

    public String getSalD126DD() {
        return "EmployeeSalary_salD126";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD127">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD127;

    public BigDecimal getSalD127() {
        return salD127;
    }

    public void setSalD127(BigDecimal salD127) {
        this.salD127 = salD127;
    }

    public String getSalD127DD() {
        return "EmployeeSalary_salD127";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD128">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD128;

    public BigDecimal getSalD128() {
        return salD128;
    }

    public void setSalD128(BigDecimal salD128) {
        this.salD128 = salD128;
    }

    public String getSalD128DD() {
        return "EmployeeSalary_salD128";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD129">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD129;

    public BigDecimal getSalD129() {
        return salD129;
    }

    public void setSalD129(BigDecimal salD129) {
        this.salD129 = salD129;
    }

    public String getSalD129DD() {
        return "EmployeeSalary_salD129";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD130">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD130;

    public BigDecimal getSalD130() {
        return salD130;
    }

    public void setSalD130(BigDecimal salD130) {
        this.salD130 = salD130;
    }

    public String getSalD130DD() {
        return "EmployeeSalary_salD130";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD131">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD131;

    public BigDecimal getSalD131() {
        return salD131;
    }

    public void setSalD131(BigDecimal salD131) {
        this.salD131 = salD131;
    }

    public String getSalD131DD() {
        return "EmployeeSalary_salD131";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD132">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD132;

    public BigDecimal getSalD132() {
        return salD132;
    }

    public void setSalD132(BigDecimal salD132) {
        this.salD132 = salD132;
    }

    public String getSalD132DD() {
        return "EmployeeSalary_salD132";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD133">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD133;

    public BigDecimal getSalD133() {
        return salD133;
    }

    public void setSalD133(BigDecimal salD133) {
        this.salD133 = salD133;
    }

    public String getSalD133DD() {
        return "EmployeeSalary_salD133";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD134">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD134;

    public BigDecimal getSalD134() {
        return salD134;
    }

    public void setSalD134(BigDecimal salD134) {
        this.salD134 = salD134;
    }

    public String getSalD134DD() {
        return "EmployeeSalary_salD134";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD135">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD135;

    public BigDecimal getSalD135() {
        return salD135;
    }

    public void setSalD135(BigDecimal salD135) {
        this.salD135 = salD135;
    }

    public String getSalD135DD() {
        return "EmployeeSalary_salD135";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD136">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD136;

    public BigDecimal getSalD136() {
        return salD136;
    }

    public void setSalD136(BigDecimal salD136) {
        this.salD136 = salD136;
    }

    public String getSalD136DD() {
        return "EmployeeSalary_salD136";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD137">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD137;

    public BigDecimal getSalD137() {
        return salD137;
    }

    public void setSalD137(BigDecimal salD137) {
        this.salD137 = salD137;
    }

    public String getSalD137DD() {
        return "EmployeeSalary_salD137";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD138">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD138;

    public BigDecimal getSalD138() {
        return salD138;
    }

    public void setSalD138(BigDecimal salD138) {
        this.salD138 = salD138;
    }

    public String getSalD138DD() {
        return "EmployeeSalary_salD138";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD139">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD139;

    public BigDecimal getSalD139() {
        return salD139;
    }

    public void setSalD139(BigDecimal salD139) {
        this.salD139 = salD139;
    }

    public String getSalD139DD() {
        return "EmployeeSalary_salD139";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD140">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD140;

    public BigDecimal getSalD140() {
        return salD140;
    }

    public void setSalD140(BigDecimal salD140) {
        this.salD140 = salD140;
    }

    public String getSalD140DD() {
        return "EmployeeSalary_salD140";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD141">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD141;

    public BigDecimal getSalD141() {
        return salD141;
    }

    public void setSalD141(BigDecimal salD141) {
        this.salD141 = salD141;
    }

    public String getSalD141DD() {
        return "EmployeeSalary_salD141";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD142">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD142;

    public BigDecimal getSalD142() {
        return salD142;
    }

    public void setSalD142(BigDecimal salD142) {
        this.salD142 = salD142;
    }

    public String getSalD142DD() {
        return "EmployeeSalary_salD142";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD143">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD143;

    public BigDecimal getSalD143() {
        return salD143;
    }

    public void setSalD143(BigDecimal salD143) {
        this.salD143 = salD143;
    }

    public String getSalD143DD() {
        return "EmployeeSalary_salD143";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD144">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD144;

    public BigDecimal getSalD144() {
        return salD144;
    }

    public void setSalD144(BigDecimal salD144) {
        this.salD144 = salD144;
    }

    public String getSalD144DD() {
        return "EmployeeSalary_salD144";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD145">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD145;

    public BigDecimal getSalD145() {
        return salD145;
    }

    public void setSalD145(BigDecimal salD145) {
        this.salD145 = salD145;
    }

    public String getSalD145DD() {
        return "EmployeeSalary_salD145";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD146">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD146;

    public BigDecimal getSalD146() {
        return salD146;
    }

    public void setSalD146(BigDecimal salD146) {
        this.salD146 = salD146;
    }

    public String getSalD146DD() {
        return "EmployeeSalary_salD146";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD147">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD147;

    public BigDecimal getSalD147() {
        return salD147;
    }

    public void setSalD147(BigDecimal salD147) {
        this.salD147 = salD147;
    }

    public String getSalD147DD() {
        return "EmployeeSalary_salD147";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD148">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD148;

    public BigDecimal getSalD148() {
        return salD148;
    }

    public void setSalD148(BigDecimal salD148) {
        this.salD148 = salD148;
    }

    public String getSalD148DD() {
        return "EmployeeSalary_salD148";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD149">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD149;

    public BigDecimal getSalD149() {
        return salD149;
    }

    public void setSalD149(BigDecimal salD149) {
        this.salD149 = salD149;
    }

    public String getSalD149DD() {
        return "EmployeeSalary_salD149";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salD150">
    @Column(precision = 25, scale = 13)
    private BigDecimal salD150;

    public BigDecimal getSalD150() {
        return salD150;
    }

    public void setSalD150(BigDecimal salD150) {
        this.salD150 = salD150;
    }

    public String getSalD150DD() {
        return "EmployeeSalary_salD150";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="branchCode">
    @Column
    private String branchCode;

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchCodeDD() {
        return "EmployeeSalary_branchCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empCodeName">
    @Column
    private String empCodeName;

    public String getEmpCodeName() {
        return empCodeName;
    }

    public void setEmpCodeName(String empCodeName) {
        this.empCodeName = empCodeName;
    }

    public String getEmpCodeNameDD() {
        return "EmployeeSalary_empCodeName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empNameCode">
    @Column
    private String empNameCode;

    public String getEmpNameCode() {
        return empNameCode;
    }

    public void setEmpNameCode(String empNameCode) {
        this.empNameCode = empNameCode;
    }

    public String getEmpNameCodeDD() {
        return "EmployeeSalary_empNameCode";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "EmployeeSalary_endDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "EmployeeSalary_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="savingBox">
    @Column(precision = 25, scale = 13)
    private BigDecimal savingBox;

    public BigDecimal getSavingBox() {
        return savingBox;
    }

    public void setSavingBox(BigDecimal savingBox) {
        this.savingBox = savingBox;
    }

    public String getSavingBoxDD() {
        return "EmployeeSalary_savingBox";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="currencyDescription">
    @Column
    @Translatable(translationField = "currencyDescriptionTranslated")
    private String currencyDescription;

    public String getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(String currencyDescription) {
        this.currencyDescription = currencyDescription;
    }

    public String getCurrencyDescriptionDD() {
        return "EmployeeSalary_currencyDescription";
    }
    @Transient
    @Translation(originalField = "currencyDescription")
    private String currencyDescriptionTranslated;

    public String getCurrencyDescriptionTranslated() {
        return currencyDescriptionTranslated;
    }

    public void setCurrencyDescriptionTranslated(String currencyDescriptionTranslated) {
        this.currencyDescriptionTranslated = currencyDescriptionTranslated;
    }

    public String getCurrencyDescriptionTranslatedDD() {
        return "EmployeeSalary_currencyDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="totalDed">
    @Column(precision = 25, scale = 13)
    private BigDecimal totalDed;

    public BigDecimal getTotalDed() {
        return totalDed;
    }

    public void setTotalDed(BigDecimal totalDed) {
        this.totalDed = totalDed;
    }

    public String getTotalDedDD() {
        return "EmployeeSalary_totalDed";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="beneficiaryNarration">
    private String beneficiaryNarration;

    public String getBeneficiaryNarration() {
        return beneficiaryNarration;
    }

    public void setBeneficiaryNarration(String beneficiaryNarration) {
        this.beneficiaryNarration = beneficiaryNarration;
    }

    public String getBeneficiaryNarrationDD() {
        return "EmployeeSalary_beneficiaryNarration";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="natID">
    private String natID;

    public String getNatID() {
        return natID;
    }

    public void setNatID(String natID) {
        this.natID = natID;
    }

    public String getNatIDDD() {
        return "EmployeeSalary_natID";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="netYearToDate">
    @Column(precision = 25, scale = 13)
    private BigDecimal netYearToDate;

    public BigDecimal getNetYearToDate() {
        return netYearToDate;
    }

    public void setNetYearToDate(BigDecimal netYearToDate) {
        this.netYearToDate = netYearToDate;
    }

    public String getNetYearToDateDD() {
        return "EmployeeSalary_netYearToDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="city_dbid">
    private String city_dbid;

    public String getCity_dbid() {
        return city_dbid;
    }

    public void setCity_dbid(String city_dbid) {
        this.city_dbid = city_dbid;
    }

    public String getCity_dbidDD() {
        return "EmployeeSalary_city_dbid";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="accountNo2">
    private String accountNo2;

    public String getAccountNo2() {
        return accountNo2;
    }

    public void setAccountNo2(String accountNo2) {
        this.accountNo2 = accountNo2;
    }

    public String getAccountNo2DD() {
        return "EmployeeSalary_accountNo2";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cityDescription">
    @Column
    @Translatable(translationField = "cityDescriptionTranslated")
    private String cityDescription;

    public String getCityDescription() {
        return cityDescription;
    }

    public void setCityDescription(String cityDescription) {
        this.cityDescription = cityDescription;
    }

    public String getCityDescriptionDD() {
        return "EmployeeSalary_cityDescription";
    }
    @Transient
    @Translation(originalField = "cityDescription")
    private String cityDescriptionTranslated;

    public String getCityDescriptionTranslated() {
        return cityDescriptionTranslated;
    }

    public void setCityDescriptionTranslated(String cityDescriptionTranslated) {
        this.cityDescriptionTranslated = cityDescriptionTranslated;
    }

    public String getCityDescriptionTranslatedDD() {
        return "EmployeeSalary_cityDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="arabicName">
    private String arabicName;

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getArabicNameDD() {
        return "EmployeeSalary_arabicName";
    }
    //</editor-fold>
}
