/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.employee.PositionGoal;
import com.unitedofoq.otms.foundation.PositionColor;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeIndirectPosition;
import com.unitedofoq.otms.foundation.employee.EmployeeMainPosition;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="positionInDirectParents">
// </editor-fold>
/**
 *
 * @author arezk
 */
@Entity
@DiscriminatorValue("MASTER")
@ParentEntity(fields = {"unit"})
@ChildEntity(fields = {"responsibilities", "tasks", "workflowExceptions", "trainings", "certificates",
    "educations", "competencies", "beheviours", "values", "positionInDirectParents", "positionDirectParent", "positionCertificatesNew", "positionGoal"})
@FABSEntitySpecs(customCascadeFields = {"positionDirectParent"})
public class Position extends PositionBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position dottedPosition;
    private int levelColor;
    // <editor-fold defaultstate="collapsed" desc="positionGoal">
    @OneToMany(mappedBy = "position")
    private List<PositionGoal> positionGoal;

    public List<PositionGoal> getPositionGoal() {
        return positionGoal;
    }

    public void setPositionGoal(List<PositionGoal> positionGoal) {
        this.positionGoal = positionGoal;
    }

    public String getPositionGoalDD() {
        return "Position_positionGoal";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="indirectEmployees">
    @OneToMany(mappedBy = "position")
    @JoinColumn
    private List<EmployeeIndirectPosition> indirectEmployees;

    public List<EmployeeIndirectPosition> getIndirectEmployees() {
        return indirectEmployees;
    }

    public void setIndirectEmployees(List<EmployeeIndirectPosition> indirectEmployees) {
        this.indirectEmployees = indirectEmployees;
    }

    public String getIndirectEmployeesDD() {
        return "Position_indirectEmployees";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mainEmployees">
    @OneToMany(mappedBy = "position")
    private List<EmployeeMainPosition> mainEmployees;

    public List<EmployeeMainPosition> getMainEmployees() {
        return mainEmployees;
    }

    public void setMainEmployees(List<EmployeeMainPosition> mainEmployees) {
        this.mainEmployees = mainEmployees;
    }

    public String getMainEmployeesDD() {
        return "Position_mainEmployees";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    private Unit unit;

    public String getUnitDD() {
        return "Position_unit";
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private PayGrade payGrade;

    public PayGrade getPayGrade() {
        return payGrade;
    }

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public String getPayGradeDD() {
        return "Position_payGrade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Job job;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getJobDD() {
        return "Position_job";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="shortName">
    @Column
    private String shortName;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getShortNameDD() {
        return "Position_shortName";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="systemCode">
    @Column
    private String systemCode;

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getSystemCodeDD() {
        return "Position_systemCode";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacancy">
    @Column
    private Integer vacancy;

    public Integer getVacancy() {
        return vacancy;
    }

    public void setVacancy(Integer vacancy) {
        this.vacancy = vacancy;
    }

    public String getVacancyDD() {
        return "Position_vacancy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="category">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC category;

    public UDC getCategory() {
        return category;
    }

    public void setCategory(UDC category) {
        this.category = category;
    }

    public String getCategoryDD() {
        return "Position_category";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tasks">
    @OneToMany(mappedBy = "position")
    private List<PositionTask> tasks;

    public List<PositionTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<PositionTask> tasks) {
        this.tasks = tasks;
    }

    public String setTasksDD() {
        return "Position_tasks";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workflowExceptions">
    @OneToMany(mappedBy = "position")
    private List<PositionWfException> workflowExceptions;

    public List<PositionWfException> getWorkflowExceptions() {
        return workflowExceptions;
    }

    public void setWorkflowExceptions(List<PositionWfException> workflowExceptions) {
        this.workflowExceptions = workflowExceptions;
    }

    public String getWorkflowExceptionsDD() {
        return "Position_workflowExceptions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "Position_currency";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="manager">
    private boolean manager;

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }

    public String getManagerDD() {
        return "Position_manager";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacant">
    private boolean vacant;

    public boolean isVacant() {
        return vacant;
    }

    public void setVacant(boolean vacant) {
        this.vacant = vacant;
    }

    public String IsVacantDD() {
        return "Position_vacant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="chartViewable">
    private boolean chartViewable;

    public boolean isChartViewable() {
        return chartViewable;
    }

    public void setChartViewable(boolean chartViewable) {
        this.chartViewable = chartViewable;
    }

    public String getChartViewableDD() {
        return "Position_chartViewable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="createdBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee createdBy;

    public Employee getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByDD() {
        return "Position_createdBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="updatedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee updatedBy;

    public Employee getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Employee updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedByDD() {
        return "Position_updatedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="updateDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateDateDD() {
        return "Position_updateDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionKey">
    @Column
    private boolean positionKey;

    public boolean isPositionKey() {
        return positionKey;
    }

    public void setPositionKey(boolean positionKey) {
        this.positionKey = positionKey;
    }

    public String getPositionKeyDD() {
        return "Position_positionKey";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionDirectParent">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "position")
    @JoinColumn
    private PositionDirectParent positionDirectParent;

    public PositionDirectParent getPositionDirectParent() {
        return positionDirectParent;
    }

    public void setPositionDirectParent(PositionDirectParent positionDirectParent) {
        this.positionDirectParent = positionDirectParent;
    }

    public String getPositionDirectParentDD() {
        return "Position_positionDirectParent";
    }

    @Override
    public void PrePersist() {
        super.PrePersist();
        if (positionDirectParent != null) {
            if (positionDirectParent.getPosition() == null) {
                positionDirectParent.setPosition(this);
            }
        }

        if (getLevelZeroParent() == null || getLevelZeroParent() == 0) {
            setLevelZeroParent(getDbid());
        } else if (getLevelOneParent() == null || getLevelOneParent() == 0) {
            setLevelOneParent(getDbid());
        } else if (getLevelTwoParent() == null || getLevelTwoParent() == 0) {
            setLevelTwoParent(getDbid());
        } else if (getLevelThreeParent() == null || getLevelThreeParent() == 0) {
            setLevelThreeParent(getDbid());
        } else if (getLevelFourParent() == null || getLevelFourParent() == 0) {
            setLevelFourParent(getDbid());
        } else if (getLevelFiveParent() == null || getLevelFiveParent() == 0) {
            setLevelFiveParent(getDbid());
        } else if (getLevelSixParent() == null || getLevelSixParent() == 0) {
            setLevelSixParent(getDbid());
        } else if (getLevelSevenParent() == null || getLevelSevenParent() == 0) {
            setLevelSevenParent(getDbid());
        } else if (getLevelEightParent() == null || getLevelEightParent() == 0) {
            setLevelEightParent(getDbid());
        } else if (getLevelNineParent() == null || getLevelNineParent() == 0) {
            setLevelNineParent(getDbid());
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionInDirectParents">
    @OneToMany(mappedBy = "position")
    @JoinColumn
    private List<PositionInDirectParent> positionInDirectParents;

    public List<PositionInDirectParent> getPositionInDirectParents() {
        return positionInDirectParents;
    }

    public String getPositionInDirectParentsDD() {
        return "Position_positionInDirectParents";
    }

    public void setPositionInDirectParents(List<PositionInDirectParent> positionInDirectParents) {
        this.positionInDirectParents = positionInDirectParents;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PositionParentAll">
    @OneToMany(mappedBy = "position")
    @JoinColumn
    private List<PositionParentAll> positionParentAll;

    public List<PositionParentAll> getPositionParentAll() {
        return positionParentAll;
    }

    public void setPositionParentAll(List<PositionParentAll> positionParentAll) {
        this.positionParentAll = positionParentAll;
    }

    public String getPositionParentAllDD() {
        return "Position_positionParentAll";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="childPositions">
    @OneToMany(mappedBy = "parentPosition")
    @JoinColumn
    private List<PositionParentAll> childPositions;

    public List<PositionParentAll> getChildPositions() {
        return childPositions;
    }

    public void setChildPositions(List<PositionParentAll> childPositions) {
        this.childPositions = childPositions;
    }

    public String getChildPositionsDD() {
        return "Position_childPositions";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumWorkDuration">
    @Column(precision = 25, scale = 13)
    private BigDecimal minimumWorkDuration;

    public BigDecimal getMinimumWorkDuration() {
        return minimumWorkDuration;
    }

    public void setMinimumWorkDuration(BigDecimal minimumWorkDuration) {
        this.minimumWorkDuration = minimumWorkDuration;
    }

    public String getMinimumWorkDurationDD() {
        return "Position_minimumWorkDuration";
    }
    @Transient
    private BigDecimal minimumWorkDurationMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMinimumWorkDurationMask() {
        minimumWorkDurationMask = minimumWorkDuration;
        return minimumWorkDurationMask;
    }

    public void setMinimumWorkDurationMask(BigDecimal minimumWorkDurationMask) {
        updateDecimalValue("minimumWorkDuration", minimumWorkDurationMask);
    }

    public String getMinimumWorkDurationMaskDD() {
        return "Position_minimumWorkDurationMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumWorkDuration">
    @Column(precision = 25, scale = 13)
    private BigDecimal maximumWorkDuration;

    public BigDecimal getMaximumWorkDuration() {
        return maximumWorkDuration;
    }

    public void setMaximumWorkDuration(BigDecimal maximumWorkDuration) {
        this.maximumWorkDuration = maximumWorkDuration;
    }

    public String getMaximumWorkDurationDD() {
        return "Position_maximumWorkDuration";
    }

    @Transient
    private BigDecimal maximumWorkDurationMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMaximumWorkDurationMask() {
        maximumWorkDurationMask = maximumWorkDuration;
        return maximumWorkDurationMask;
    }

    public void setMaximumWorkDurationMask(BigDecimal maximumWorkDurationMask) {
        updateDecimalValue("maximumWorkDuration", maximumWorkDurationMask);
    }

    public String getMaximumWorkDurationMaskDD() {
        return "Position_maximumWorkDurationMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workDurationMeasuerUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC workDurationMeasuerUnit;

    public String getWorkDurationMeasuerUnitDD() {
        return "Position_workDurationMeasuerUnit";
    }

    public UDC getWorkDurationMeasuerUnit() {
        return workDurationMeasuerUnit;
    }

    public void setWorkDurationMeasuerUnit(UDC workDurationMeasuerUnit) {
        this.workDurationMeasuerUnit = workDurationMeasuerUnit;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainings">
    @OneToMany(mappedBy = "position")
    private List<PositionTraining> trainings;

    public List<PositionTraining> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<PositionTraining> trainings) {
        this.trainings = trainings;
    }

    public String getTrainingsDD() {
        return "Position_trainings";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certificates">
    @OneToMany(mappedBy = "position")
    private List<PositionCertificate> certificates;

    public List<PositionCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<PositionCertificate> certificates) {
        this.certificates = certificates;
    }

    public String getCertificatesDD() {
        return "Position_certificates";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="educations">
    @OneToMany(mappedBy = "position")
    private List<PositionEducation> educations;

    public List<PositionEducation> getEducations() {
        return educations;
    }

    public void setEducations(List<PositionEducation> educations) {
        this.educations = educations;
    }

    public String getEducationsDD() {
        return "Position_educations";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competencies">
    @OneToMany(mappedBy = "position")
    private List<PositionCompetency> competencies;

    public List<PositionCompetency> getCompetencies() {
        return competencies;
    }

    public void setCompetencies(List<PositionCompetency> competencies) {
        this.competencies = competencies;
    }

    public String getCompetenciesDD() {
        return "Position_competencies";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="beheviours">
    @OneToMany(mappedBy = "position")
    private List<PositionBehaviour> beheviours;

    public List<PositionBehaviour> getBeheviours() {
        return beheviours;
    }

    public void setBeheviours(List<PositionBehaviour> beheviours) {
        this.beheviours = beheviours;
    }

    public String getBehevioursDD() {
        return "Position_beheviours";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="values">
    @OneToMany(mappedBy = "position")
    private List<PositionValue> values;

    public List<PositionValue> getValues() {
        return values;
    }

    public void setValues(List<PositionValue> values) {
        this.values = values;
    }

    public String getValuesDD() {
        return "Position_values";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumExperience">
    @Column(precision = 25, scale = 13)
    private BigDecimal minimumExperience;

    public BigDecimal getMinimumExperience() {
        return minimumExperience;
    }

    public void setMinimumExperience(BigDecimal minimumExperience) {
        this.minimumExperience = minimumExperience;
    }

    public String getMinimumExperienceDD() {
        return "Position_minimumExperience";
    }

    @Transient
    private BigDecimal minimumExperienceMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMinimumExperienceMask() {
        minimumExperienceMask = minimumExperience;
        return minimumExperienceMask;
    }

    public void setMinimumExperienceMask(BigDecimal minimumExperienceMask) {
        updateDecimalValue("minimumExperience", minimumExperienceMask);
    }

    public String getMinimumExperienceMaskDD() {
        return "Position_minimumExperienceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumExperience">
    @Column(precision = 25, scale = 13)
    private BigDecimal maximumExperience;

    public BigDecimal getMaximumExperience() {
        return maximumExperience;
    }

    public void setMaximumExperience(BigDecimal maximumExperience) {
        this.maximumExperience = maximumExperience;
    }

    public String getMaximumExperienceDD() {
        return "Position_maximumExperience";
    }

    @Transient
    private BigDecimal maximumExperienceMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMaximumExperienceMask() {
        maximumExperienceMask = maximumExperience;
        return maximumExperienceMask;
    }

    public void setMaximumExperienceMask(BigDecimal maximumExperienceMask) {
        updateDecimalValue("maximumExperience", maximumExperienceMask);
    }

    public String getMaximumExperienceMaskDD() {
        return "Position_maximumExperienceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="experienceMeasureUnit">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC experienceMeasureUnit;

    public UDC getExperienceMeasureUnit() {
        return experienceMeasureUnit;
    }

    public void setExperienceMeasureUnit(UDC experienceMeasureUnit) {
        this.experienceMeasureUnit = experienceMeasureUnit;
    }

    public String getExperienceMeasureUnitDD() {
        return "Position_experienceMeasureUnit";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costcenter">
    //ibr 10/10/2011 12:05
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterDD() {
        return "Position_costCenter";
    }
    //ibr 10/10/2011 12:10
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitDefaultPosition">
    @Column
    private boolean unitDefaultPosition;

    public boolean isUnitDefaultPosition() {
        return unitDefaultPosition;
    }

    public void setUnitDefaultPosition(boolean unitDefaultPosition) {
        this.unitDefaultPosition = unitDefaultPosition;
    }

    public String getUnitDefaultPositionDD() {
        return "Position_unitDefaultPosition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="responsibilities">
    @OneToMany(mappedBy = "position")
    private List<PositionResponsability> responsibilities;

    public List<PositionResponsability> getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(List<PositionResponsability> responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getResponsibilitiesDD() {
        return "Position_responsibilities";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="parentPositionSimpleMode">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "parentPosition_dbid")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position parentPositionSimpleMode;

    public Position getParentPositionSimpleMode() {
        return parentPositionSimpleMode;
    }

    public void setParentPositionSimpleMode(Position parentPositionSimpleMode) {
        this.parentPositionSimpleMode = parentPositionSimpleMode;
    }

    public String getParentPositionSimpleModeDD() {
        return "Position_parentPositionSimpleMode";
    }

// </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="childPositionsSimpleMode">
    @OneToMany(mappedBy = "parentPositionSimpleMode")
    private List<Position> childPositionsSimpleMode;

    public List<Position> getChildPositionsSimpleMode() {
        return childPositionsSimpleMode;
    }

    public void setChildPositionsSimpleMode(List<Position> childPositionsSimpleMode) {
        this.childPositionsSimpleMode = childPositionsSimpleMode;
    }

    public String getChildPositionsSimpleModeDD() {
        return "Position_childPositionsSimpleMode";
    }

// </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="employeesSimpleMode">
    @OneToMany(mappedBy = "positionSimpleMode")
    private List<Employee> employeesSimpleMode;

    public List<Employee> getEmployeesSimpleMode() {
        return employeesSimpleMode;
    }

    public void setEmployeesSimpleMode(List<Employee> employeesSimpleMode) {
        this.employeesSimpleMode = employeesSimpleMode;
    }

    public String getEmployeesSimpleModeDD() {
        return "Position_employeesSimpleMode";
    }

    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="positionCertificatesNew">
    @OneToMany(mappedBy = "position")
    private List<PositionCertificateNew> positionCertificatesNew;

    public List<PositionCertificateNew> getPositionCertificatesNew() {
        return positionCertificatesNew;
    }

    public void setPositionCertificatesNew(List<PositionCertificateNew> positionCertificatesNew) {
        this.positionCertificatesNew = positionCertificatesNew;
    }

    public String getPositionCertificatesNewDD() {
        return "Position_positionCertificatesNew";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="TempByLAhmed">
    //<editor-fold defaultstate="collapsed" desc="TempEmployeePostionUnits">
    /*@OneToMany(mappedBy="position")
     private List<TempEmployeePostionUnits> tempEmployeePostionUnits;

     public List<TempEmployeePostionUnits> getTempEmployeePostionUnits() {
     return tempEmployeePostionUnits;
     }

     public void setTempEmployeePostionUnits(List<TempEmployeePostionUnits> tempEmployeePostionUnits) {
     this.tempEmployeePostionUnits = tempEmployeePostionUnits;
     }

     public String getTempEmployeePostionUnitsDD() {
     return "Position_tempEmployeePostionUnits";
     }
     //</editor-fold>
     //</editor-fold>
     */
    // <editor-fold defaultstate="collapsed" desc="Levels Parents">
    @Column(name = "levelZeroParent_DBID")
    private Long levelZeroParent;

    @Column(name = "levelOneParent_DBID")
    private Long levelOneParent;

    @Column(name = "levelTwoParent_DBID")
    private Long levelTwoParent;

    @Column(name = "levelThreeParent_DBID")
    private Long levelThreeParent;

    @Column(name = "levelFourParent_DBID")
    private Long levelFourParent;

    @Column(name = "levelFiveParent_DBID")
    private Long levelFiveParent;

    @Column(name = "levelSixParent_DBID")
    private Long levelSixParent;

    @Column(name = "levelSevenParent_DBID")
    private Long levelSevenParent;

    @Column(name = "levelEightParent_DBID")
    private Long levelEightParent;

    @Column(name = "levelNineParent_DBID")
    private Long levelNineParent;

    public Long getLevelEightParent() {
        return levelEightParent;
    }

    public void setLevelEightParent(Long levelEightParent) {
        this.levelEightParent = levelEightParent;
    }

    public Long getLevelFiveParent() {
        return levelFiveParent;
    }

    public void setLevelFiveParent(Long levelFiveParent) {
        this.levelFiveParent = levelFiveParent;
    }

    public Long getLevelFourParent() {
        return levelFourParent;
    }

    public void setLevelFourParent(Long levelFourParent) {
        this.levelFourParent = levelFourParent;
    }

    public Long getLevelNineParent() {
        return levelNineParent;
    }

    public void setLevelNineParent(Long levelNineParent) {
        this.levelNineParent = levelNineParent;
    }

    public Long getLevelOneParent() {
        return levelOneParent;
    }

    public void setLevelOneParent(Long levelOneParent) {
        this.levelOneParent = levelOneParent;
    }

    public Long getLevelSevenParent() {
        return levelSevenParent;
    }

    public void setLevelSevenParent(Long levelSevenParent) {
        this.levelSevenParent = levelSevenParent;
    }

    public Long getLevelSixParent() {
        return levelSixParent;
    }

    public void setLevelSixParent(Long levelSixParent) {
        this.levelSixParent = levelSixParent;
    }

    public Long getLevelThreeParent() {
        return levelThreeParent;
    }

    public void setLevelThreeParent(Long levelThreeParent) {
        this.levelThreeParent = levelThreeParent;
    }

    public Long getLevelTwoParent() {
        return levelTwoParent;
    }

    public void setLevelTwoParent(Long levelTwoParent) {
        this.levelTwoParent = levelTwoParent;
    }

    public Long getLevelZeroParent() {
        return levelZeroParent;
    }

    public void setLevelZeroParent(Long levelZeroParent) {
        this.levelZeroParent = levelZeroParent;
    }

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="hiringDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date hiringDate;

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getHiringDateDD() {
        return "Position_hiringDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="plannedHeadCnt">
    /*private int plannedHeadCnt;

     public int getPlannedHeadCnt() {
     return plannedHeadCnt;
     }

     public void setPlannedHeadCnt(int plannedHeadCnt) {
     this.plannedHeadCnt = plannedHeadCnt;
     }

     public String getPlannedHeadCntDD() {
     return "Position_plannedHeadCnt";
     }*/
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="critical">
    private boolean critical;

    public boolean isCritical() {
        return critical;
    }

    public String getCriticalDD() {
        return "Position_critical";
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
    // </editor-fold >

    private boolean calculateDayLessWork;

    public boolean isCalculateDayLessWork() {
        return calculateDayLessWork;
    }

    public void setCalculateDayLessWork(boolean calculateDayLessWork) {
        this.calculateDayLessWork = calculateDayLessWork;
    }

    public String getCalculateDayLessWorkDD() {
        return "Position_calculateDayLessWork";
    }

    // <editor-fold defaultstate="collapsed" desc="Blue Collar">
    private boolean blueCollar;

    public boolean isBlueCollar() {
        return blueCollar;
    }

    public void setBlueCollar(boolean blueCollar) {
        this.blueCollar = blueCollar;
    }

    public String getBlueCollarDD() {
        return "Position_blueCollar";
    }

    // </editor-fold >
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "Position_legalEntity";
    }

    public int getLevelColor() {
        return levelColor;
    }

    public void setLevelColor(int levelColor) {
        this.levelColor = levelColor;
    }

    public String getLevelColorDD() {
        return "Position_levelColor";
    }

    public Position getDottedPosition() {
        return dottedPosition;
    }

    public void setDottedPosition(Position dottedPosition) {
        this.dottedPosition = dottedPosition;
    }

    public String getDottedPositionDD() {
        return "Position_dottedPosition";
    }
    // <editor-fold defaultstate="collapsed" desc="nodeLevelDescription">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PositionColor nodeLevel;

    public PositionColor getNodeLevel() {
        return nodeLevel;
    }

    public void setNodeLevel(PositionColor nodeLevel) {
        this.nodeLevel = nodeLevel;
    }

    public String getNodeLevelDD() {
        return "Unit_nodeLevel";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="eligibleDangerDays">
    private boolean eligibleDangerDays;

    public boolean isEligibleDangerDays() {
        return eligibleDangerDays;
    }

    public void setEligibleDangerDays(boolean eligibleDangerDays) {
        this.eligibleDangerDays = eligibleDangerDays;
    }

    public String getEligibleDangerDaysDD() {
        return "Position_eligibleDangerDays";
    }

    // </editor-fold >
}
