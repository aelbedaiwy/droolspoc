/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.applicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;


/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantCompetency extends PersonnelCompetencyBase {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "ApplicantCompetency_applicant";
    }
   // </editor-fold>
    
        @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }
    
    public String getM2mCheckDD() {
        return "EmployeeKPA_m2mCheck";
    }
           
    @Override
    public void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
    
}
