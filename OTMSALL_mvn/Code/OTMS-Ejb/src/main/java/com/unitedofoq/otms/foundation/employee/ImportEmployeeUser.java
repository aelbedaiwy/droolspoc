package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ImportEmployeeUser extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="employeeCode">
    @Column
    private String employeeCode;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "ImportEmployeeUser_employeeCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="loginName">
    @Column
    private String loginName;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginNameDD() {
        return "ImportEmployeeUser_loginName";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="password">
    @Column(name = "userpassword")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordDD() {
        return "ImportEmployeeUser_password";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="role">
    @Column(name = "userrole")
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleDD() {
        return "ImportEmployeeUser_role";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="done">
    @Column
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "ImportEmployeeUser_done";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "ImportEmployeeUser_notes";
    }
//</editor-fold>
}
