package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.competency.EDSCompetencyLevelBehavior;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields="businessOutcome")
public class EDSIDPBusinessOutcomeBehavior extends BaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSIDPBusinessOutcome businessOutcome;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name= "edscomlvlind_dbid")
    
    private EDSCompetencyLevelBehavior edsCompetencyLevelIndicator;

    private String customBehavior;

    public String getBusinessOutcomeDD()                {   return "EDSIDPBusinessOutcomeBehavior_businessOutcome";  }
    public String getEdsCompetencyLevelIndicatorDD()    {   return "EDSIDPBusinessOutcomeBehavior_edsCompetencyLevelIndicator";  }
    public String getCustomBehaviorDD()                 {   return "EDSIDPBusinessOutcomeBehavior_customBehavior";  }

    public String getCustomBehavior() {
        return customBehavior;
    }

    public void setCustomBehavior(String customBehavior) {
        this.customBehavior = customBehavior;
    }
  

    public EDSIDPBusinessOutcome getBusinessOutcome() {
        return businessOutcome;
    }

    public void setBusinessOutcome(EDSIDPBusinessOutcome businessOutcome) {
        this.businessOutcome = businessOutcome;
    }

    public EDSCompetencyLevelBehavior getEdsCompetencyLevelIndicator() {
        return edsCompetencyLevelIndicator;
    }

    public void setEdsCompetencyLevelIndicator(EDSCompetencyLevelBehavior edsCompetencyLevelIndicators) {
        this.edsCompetencyLevelIndicator = edsCompetencyLevelIndicators;
    }
}