package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;

@Entity
public class RepEmployeeOthersTranslation extends RepAppraisalSheetTranslationBase {
    // <editor-fold defaultstate="collapsed" desc="description">

    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>
}
