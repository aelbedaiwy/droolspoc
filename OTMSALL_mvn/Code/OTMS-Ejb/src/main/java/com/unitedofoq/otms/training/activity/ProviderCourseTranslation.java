/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author lap2
 */
@Entity
public class ProviderCourseTranslation extends BaseEntityTranslation{
    //<editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="preRequisites">
    private String preRequisites;

    public void setPreRequisites(String preRequisites) {
        this.preRequisites = preRequisites;
    }

    public String getPreRequisites() {
        return preRequisites;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="content">
    private String content;

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
    //</editor-fold>
}
