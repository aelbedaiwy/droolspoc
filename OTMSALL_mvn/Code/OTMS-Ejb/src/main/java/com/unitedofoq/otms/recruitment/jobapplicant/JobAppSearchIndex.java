package com.unitedofoq.otms.recruitment.jobapplicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class JobAppSearchIndex extends BaseEntity {

    @Column(nullable=false)
	private String searchIndex;
	private String cvSearchIndex;
	
	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String theSearchIndex) {
		searchIndex = theSearchIndex;
	}

    public String getSearchIndexDD() {
		return "JobAppSearchIndex_searchIndex";
	}
	

	
	public String getCvSearchIndex() {
		return cvSearchIndex;
	}

	public void setCvSearchIndex(String theCvSearchIndex) {
		cvSearchIndex = theCvSearchIndex;
	}

    public String getCvSearchIndexDD() {
		return "JobAppSearchIndex_cvSearchIndex";
	}
	 @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private Applicant applicant;

    /**
     * @return the applicant
     */
    public Applicant getApplicant() {
        return applicant;
    }

    /**
     * @param applicant the applicant to set
     */
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
     /**
     * @return the applicantDD
     */
     public String getApplicantDD() {
        return "JobAppSearchIndex_applicant";
    }
}
