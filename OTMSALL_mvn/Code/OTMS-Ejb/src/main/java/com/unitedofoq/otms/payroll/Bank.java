/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
@ParentEntity(fields = "company")
@ChildEntity(fields = "bankBranches")
public class Bank extends BusinessObjectBaseEntity {
    //<editor-fold defaultstate="collapsed" desc="company">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Bank_company";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="bankBraches">
    @OneToMany(mappedBy = "bank")
    private List<BankBranches> bankBranches;

    public List<BankBranches> getBankBranches() {
        return bankBranches;
    }

    public void setBankBraches(List<BankBranches> bankBranches) {
        this.bankBranches = bankBranches;
    }

    public String getBankBranchesDD() {
        return "Bank_bankBranches";
    }
    //</editor-fold>

    /*
     * BankDescription
     */
    @Translatable(translationField = "bankDescriptionTranslated")
    @Column(name = "Description")
    private String bankDescription;
    @Translation(originalField = "bankDescription")
    @Transient
    private String bankDescriptionTranslated;

    //<editor-fold defaultstate="collapsed" desc="Setters">
    public void setBankDescription(String bankDescription) {
        this.bankDescription = bankDescription;
    }

    public void setBankDescriptionTranslated(String bankDescriptionTranslated) {
        this.bankDescriptionTranslated = bankDescriptionTranslated;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Getters">   
    public String getBankDescription() {
        return bankDescription;
    }

    public String getBankDescriptionTranslated() {
        return bankDescriptionTranslated;
    }

    public String getBankDescriptionDD() {
        return "Bank_bankDescription";
    }

    public String getBankDescriptionTranslatedDD() {
        return "Bank_descriptionTranslated";
    }
    //</editor-fold>
    /*//<editor-fold defaultstate="collapsed" desc="employeePayrolls">
     @OneToMany(mappedBy="bank")
     private List<EmployeePayrollBase> employeePayrolls;

     public List<EmployeePayrollBase> getEmployeePayrolls() {
     return employeePayrolls;
     }

     public void setEmployeePayrolls(List<EmployeePayrollBase> employeePayrolls) {
     this.employeePayrolls = employeePayrolls;
     }

     public String getEmployeePayrollsDD() {
     return "Bank_employeePayrolls";
     }
     //</editor-fold>
     *
     */
}
