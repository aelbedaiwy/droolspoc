/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author lap2
 */
@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeTrainingHistory extends BaseEntity {
        // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeDayOffRequest_employee";
    }
// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="courseName">
      
        @Column
       private String courseName;
       public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
        public String getCourseNameDD() {
        return "EmployeeTrainingHistory_courseName";
    }
        
           // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="courseDetail">
    
        @Column
       private String courseDetail;

    public String getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(String courseDetail) {
        this.courseDetail = courseDetail;
    }
    public String getCourseDetailDD() {
        return "EmployeeDayOffRequest_courseDetail";
    }           
    // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="provider">
       
        @Column
       private String provider;
       
    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
    public String getProviderDD() {
        return "EmployeeDayOffRequest_provider";
    }
               // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="startDate"> 
         @Temporal(TemporalType.TIMESTAMP)
        private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
         
       public String getStartDateDD() {
        return "EmployeeTrainingHistory_startDate";
    } 

     // </editor-fold> 
        // <editor-fold defaultstate="collapsed" desc="endDate"> 
         @Temporal(TemporalType.TIMESTAMP)
        private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "EmployeeTrainingHistory_endDate";
    }

     // </editor-fold> 
        // <editor-fold defaultstate="collapsed" desc="degree">
      
        @Column
       private String degree;
    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }
    public String getDegreeDD() {
        return "EmployeeTrainingHistory_degree";
    }
     // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="location">
    
        @Column
       private String location;
  
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public String getLocationDD() {
        return "EmployeeTrainingHistory_location";
    }
     // </editor-fold>

}
