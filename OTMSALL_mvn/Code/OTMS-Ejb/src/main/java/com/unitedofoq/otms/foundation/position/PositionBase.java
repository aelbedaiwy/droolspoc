package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.payroll.paygrade.PayGradeStep;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "Position")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name = "DTYPE", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("MASTER")
public class PositionBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="lastchildCode">

    @Column
    private int lastChildCode;

    public int getLastChildCode() {
        return lastChildCode;
    }

    public void setLastChildCode(int lasChildCode) {
        this.lastChildCode = lasChildCode;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable = false)
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslatedDD() {

        return "PositionBase_name";
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "PositionBase_code";
    }
    @Column
    private String hierarchycode;

    public String getHierarchycode() {
        return hierarchycode;
    }

    public String getHierarchycodeDD() {
        return "PositionBase_hierarchycode";
    }

    public void setHierarchycode(String hierarchycode) {
        this.hierarchycode = hierarchycode;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeStep">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private PayGradeStep payGradeStep;

    public PayGradeStep getPayGradeStep() {
        return payGradeStep;
    }

    public void setPayGradeStep(PayGradeStep payGradeStep) {
        this.payGradeStep = payGradeStep;
    }

    public String getPayGradeStepDD() {
        return "PositionBase_payGradeStep";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="effectiveFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveFrom;

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public String getEffectiveFromDD() {
        return "PositionBase_effectiveFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="effectiveTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveTo;

    public Date getEffectiveTo() {
        return effectiveTo;
    }

    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }

    public String getEffectiveToDD() {
        return "PositionBase_effectiveTo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @Column(length = 1)
    private String status = "A";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "PositionBase_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusDate;

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusDateDD() {
        return "PositionBase_statusDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="occupation">
    @Column
    private Integer occupation;

    public Integer getOccupation() {
        return occupation;
    }

    public void setOccupation(Integer occupation) {
        this.occupation = occupation;
    }

    public String getOccupationDD() {
        return "PositionBase_occupation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="headcount">
    @Column(nullable = false)
    private Integer headcount;

    public Integer getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Integer headcount) {
        this.headcount = headcount;
    }

    public String getHeadcountDD() {
        return "PositionBase_headcount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="creatationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date creatationDate;

    public Date getCreatationDate() {
        return creatationDate;
    }

    public void setCreatationDate(Date creatationDate) {
        this.creatationDate = creatationDate;
    }

    public String getCreatationDateDD() {
        return "PositionBase_creatationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "PositionBase_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hrStaff">
    private boolean hrStaff;

    public boolean isHrStaff() {
        return hrStaff;
    }

    public void setHrStaff(boolean hrStaff) {
        this.hrStaff = hrStaff;
    }

    public String getHrStaffDD() {
        return "PositionBase_hrStaff";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hrManager">
    private boolean hrManager;

    public boolean isHrManager() {
        return hrManager;
    }

    public void setHrManager(boolean hrManager) {
        this.hrManager = hrManager;
    }

    public String getHrManagerDD() {
        return "PositionBase_hrManager";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hrUnit">
    private boolean hrUnit;

    public boolean isHrUnit() {
        return hrUnit;
    }

    public void setHrUnit(boolean hrUnit) {
        this.hrUnit = hrUnit;
    }

    public String getHrUnitDD() {
        return "PositionBase_hrUnit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="technicalFocus">
    private boolean technicalFocus;

    public boolean isTechnicalFocus() {
        return technicalFocus;
    }

    public void setTechnicalFocus(boolean technicalFocus) {
        this.technicalFocus = technicalFocus;
    }

    public String getTechnicalFocusDD() {
        return "PositionBase_technicalFocus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="managementFocus">
    private boolean managementFocus;

    public boolean isManagementFocus() {
        return managementFocus;
    }

    public void setManagementFocus(boolean managementFocus) {
        this.managementFocus = managementFocus;
    }

    public String getManagementFocusDD() {
        return "PositionBase_managementFocus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actualOccupation">
    @Column
    private Integer actualOccupation = 0;

    public Integer getActualOccupation() {
        return actualOccupation;
    }

    public void setActualOccupation(Integer actualOccupation) {
        this.actualOccupation = actualOccupation;
    }

    public String getActualOccupationDD() {
        return "PositionBase_actualOccupation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nameEnc">
    private String nameEnc;

    public String getNameEnc() {
        return nameEnc;
    }

    public void setNameEnc(String nameEnc) {
        this.nameEnc = nameEnc;
    }
    // </editor-fold>
}
