package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.Job;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobAppNomination extends BaseEntity {

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date nominationDate;
    public String getNominationDateDD(){
        return "JobAppNomination_nominationDate";
    }
    @ManyToOne
    private Job job;
    public String getJobDD(){
        return "JobAppNomination_job";
    }
    @Column(nullable = false)
    private String note;
    public String getNoteDD(){
        return "JobAppNomination_note";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
    private JobApplicant jobApplicant;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Employee nominatedBy;
    public String getNominatedByDD(){
        return "JobAppNomination_nominatedBy";
    }
    public JobApplicant getJobApplicant() {
        return jobApplicant;
    }

    public void setJobApplicant(JobApplicant jobApplicant) {
        this.jobApplicant = jobApplicant;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Date getNominationDate() {
        return nominationDate;
    }

    public void setNominationDate(Date nominationDate) {
        this.nominationDate = nominationDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String theNote) {
        note = theNote;
    }

    public Employee getNominatedBy() {
        return nominatedBy;
    }

    public void setNominatedBy(Employee theNominatedBy) {
        nominatedBy = theNominatedBy;
    }
}
