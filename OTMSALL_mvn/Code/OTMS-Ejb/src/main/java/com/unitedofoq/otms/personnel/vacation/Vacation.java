package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.IncentivesVacationSetup;
import com.unitedofoq.otms.payroll.formula.FormulaDistinctName;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.personnel.vacationgroup.VacationGroup;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = "company")
@DiscriminatorValue("VACATION")
@ChildEntity(fields = {"affectingDeductions", "affectingIncomes", "penalties",
    "members", "formulas", "incentives"})
public class Vacation extends VacationBase {
    // <editor-fold defaultstate="collapsed" desc="fromMonth formula">

    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName fromMonth;//fr_month_formula

    public FormulaDistinctName getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(FormulaDistinctName fromMonth) {
        this.fromMonth = fromMonth;
    }

    public String getFromMonthDD() {
        return "Vacation_fromMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="period Formula">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName period;//work_cal_formula

    public FormulaDistinctName getPeriod() {
        return period;
    }

    public void setPeriod(FormulaDistinctName period) {
        this.period = period;
    }

    public String getPeriodDD() {
        return "Vacation_period";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="advancedPay">
    @Column
    private String advancedPay;//advanced_pay

    public String getAdvancedPay() {
        return advancedPay;
    }

    public void setAdvancedPay(String advancedPay) {
        this.advancedPay = advancedPay;
    }

    public String getAdvancedPayDD() {
        return "Vacation_advancedPay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postBalance Formula">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName postBalance;//post_balance_frm

    public FormulaDistinctName getPostBalance() {
        return postBalance;
    }

    public void setPostBalance(FormulaDistinctName postBalance) {
        this.postBalance = postBalance;
    }

    public String getPostBalanceDD() {
        return "Vacation_postBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payrollCutoffUse">
    @Column
    private String payrollCutoffUse;//pay_use

    public String getPayrollCutoffUse() {
        return payrollCutoffUse;
    }

    public void setPayrollCutoffUse(String payrollCutoffUse) {
        this.payrollCutoffUse = payrollCutoffUse;
    }

    public String getPayrollCutoffUseDD() {
        return "Vacation_payrollCutoffUse";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payWorkingCalender">
    @Column(length = 1)
    //Y - N
    private String payWorkingCalender;//pay_wc

    public String getPayWorkingCalender() {
        return payWorkingCalender;
    }

    public void setPayWorkingCalender(String payWorkingCalender) {
        this.payWorkingCalender = payWorkingCalender;
    }

    public String getPayWorkingCalenderDD() {
        return "Vacation_payWorkingCalender";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="removePayrollHoliday">
    @Column
    private String removePayrollHoliday;//pay_holiday

    public String getRemovePayrollHoliday() {
        return removePayrollHoliday;
    }

    public void setRemovePayrollHoliday(String removePayrollHoliday) {
        this.removePayrollHoliday = removePayrollHoliday;
    }

    public String getRemovePayrollHolidayDD() {
        return "Vacation_removePayrollHoliday";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payrollPerid Formula">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName payrollPerid;//pay_perid_frm

    public FormulaDistinctName getPayrollPerid() {
        return payrollPerid;
    }

    public void setPayrollPerid(FormulaDistinctName payrollPerid) {
        this.payrollPerid = payrollPerid;
    }

    public String getPayrollPeridDD() {
        return "Vacation_payrollPerid";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="secondPosted">
    @Column(length = 1)
    //Y - N
    private String secondPosted;//secound_posted

    public String getSecondPosted() {
        return secondPosted;
    }

    public void setSecondPosted(String SecondPosted) {
        this.secondPosted = SecondPosted;
    }

    public String getSecondPostedDD() {
        return "Vacation_secondPosted";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="secondPostedMonth">
    @Column
    private Short secondPostedMonth;//secound_posted_month

    public Short getSecondPostedMonth() {
        return secondPostedMonth;
    }

    public void setSecondPostedMonth(Short secondPostedMonth) {
        this.secondPostedMonth = secondPostedMonth;
    }

    public String getSecondPostedMonthDD() {
        return "Vacation_secondPostedMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="conversionFactor">
    @Column(precision = 18, scale = 3)
    private BigDecimal conversionFactor;//conversion_factor

    public BigDecimal getConversionFactor() {
        return conversionFactor;
    }

    public void setConversionFactor(BigDecimal conversionFactor) {
        this.conversionFactor = conversionFactor;
    }

    public String getConversionFactorDD() {
        return "Vacation_conversionFactor";
    }
    @Transient
    private BigDecimal conversionFactorMask;

    public BigDecimal getConversionFactorMask() {
        conversionFactorMask = conversionFactor;
        return conversionFactorMask;
    }

    public void setConversionFactorMask(BigDecimal conversionFactorMask) {
        updateDecimalValue("conversionFactor", conversionFactorMask);
    }

    public String getConversionFactorMaskDD() {
        return "Vacation_conversionFactorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="need approval">
    @Column(length = 1)
    //Y - N
    private String needApproval;//need_decision

    public String getNeedApproval() {
        return needApproval;
    }

    public void setNeedApproval(String needApproval) {
        this.needApproval = needApproval;
    }

    public String getNeedApprovalDD() {
        return "Vacation_needApproval";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumToPost">
    @Column(precision = 18, scale = 3)
    private BigDecimal minimumToPost;//min_to_post

    public BigDecimal getMinimumToPost() {
        return minimumToPost;
    }

    public void setMinimumToPost(BigDecimal minimumToPost) {
        this.minimumToPost = minimumToPost;
    }

    public String getMinimumToPostDD() {
        return "Vacation_minimumToPost";
    }
    @Transient
    private BigDecimal minimumToPostMask;

    public BigDecimal getMinimumToPostMask() {
        minimumToPostMask = minimumToPost;
        return minimumToPostMask;
    }

    public void setMinimumToPostMask(BigDecimal minimumToPostMask) {
        updateDecimalValue("minimumToPost", minimumToPostMask);
    }

    public String getMinimumToPostMaskDD() {
        return "Vacation_minimumToPostMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startMonth">
    @Column
    private Short startMonth;//month_to_start

    public Short getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Short startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartMonthDD() {
        return "Vacation_startMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="continious">
    @Column(length = 1)
    // Y - N
    private String continious;//continious

    public String getContinious() {
        return continious;
    }

    public void setContinious(String continious) {
        this.continious = continious;
    }

    public String getContiniousDD() {
        return "Vacation_continious";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectActualWorkPeriod">
    @Column(length = 1)
    //Y - N
    private String affectActualWorkPeriod;//affect_actual_work_period

    public String getAffectActualWorkPeriod() {
        return affectActualWorkPeriod;
    }

    public void setAffectActualWorkPeriod(String affectActualWorkPeriod) {
        this.affectActualWorkPeriod = affectActualWorkPeriod;
    }

    public String getAffectActualWorkPeriodDD() {
        return "Vacation_affectActualWorkPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="members">
    @OneToMany(mappedBy = "vacation")
    private List<VacationMember> members;

    public List<VacationMember> getMembers() {
        return members;
    }

    public void setMembers(List<VacationMember> members) {
        this.members = members;
    }

    public String getMembersDD() {
        return "Vacation_members";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationAffectingDeductions">
    @OneToMany(mappedBy = "vacation")
    private List<VacationAffectingDeduction> affectingDeductions;

    public List<VacationAffectingDeduction> getAffectingDeductions() {
        return affectingDeductions;
    }

    public void setAffectingDeductions(List<VacationAffectingDeduction> affectingDeductions) {
        this.affectingDeductions = affectingDeductions;
    }

    public String getAffectingDeductionsDD() {
        return "Vacation_affectingDeductions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationAffectingIncomes">
    @OneToMany(mappedBy = "vacation")
    private List<VacationAffectingIncome> affectingIncomes;

    public List<VacationAffectingIncome> getAffectingIncomes() {
        return affectingIncomes;
    }

    public void setAffectingIncomes(List<VacationAffectingIncome> affectingIncomes) {
        this.affectingIncomes = affectingIncomes;
    }

    public String getAffectingIncomesDD() {
        return "Vacation_affectingIncomes";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationPenalty">
    @OneToMany(mappedBy = "vacation")
    private List<VacationPenalty> penalties;

    public List<VacationPenalty> getPenalties() {
        return penalties;
    }

    public void setPenalties(List<VacationPenalty> penalties) {
        this.penalties = penalties;
    }

    public String getPenaltiesDD() {
        return "Vacation_penalties";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationIncentives">
    @OneToMany(mappedBy = "vacation")
    private List<IncentivesVacationSetup> incentives;

    public List<IncentivesVacationSetup> getIncentives() {
        return incentives;
    }

    public void setIncentives(List<IncentivesVacationSetup> incentives) {
        this.incentives = incentives;
    }

    public String getIncentivesDD() {
        return "Vacation_incentives";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="perClosingConfidration">
    @Column(length = 1)
    private String perClosingConfidration;//StartEndDateFlag

    public String getPerClosingConfidration() {
        return perClosingConfidration;
    }

    public void setPerClosingConfidration(String perClosingConfidration) {
        this.perClosingConfidration = perClosingConfidration;
    }

    public String getPerClosingConfidrationDD() {
        return "Vacation_perClosingConfidration";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxDeductionDays">
    @Column(length = 1)
    //Y - N
    private String taxDeductionDays;

    public String getTaxDeductionDays() {
        return taxDeductionDays;
    }

    public void setTaxDeductionDays(String taxDeductionDays) {
        this.taxDeductionDays = taxDeductionDays;
    }

    public String getTaxDeductionDaysDD() {
        return "Vacation_taxDeductionDays";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="relatedToGroup">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private VacationGroup relatedToGroup;//group_vacation_code

    public VacationGroup getRelatedToGroup() {
        return relatedToGroup;
    }

    public void setRelatedToGroup(VacationGroup relatedToGroup) {
        this.relatedToGroup = relatedToGroup;
    }

    public String getRelatedToGroupDD() {
        return "Vacation_relatedToGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationSettlements">
    @OneToMany//(mappedBy = "originalVacation")
    private List<VacationSettlement> vacationSettlements;

    public List<VacationSettlement> getVacationSettlements() {
        return vacationSettlements;
    }

    public void setVacationSettlements(List<VacationSettlement> vacationSettlements) {
        this.vacationSettlements = vacationSettlements;
    }

    public String getVacationSettlementsDD() {
        return "Vacation_vacationSettlements";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PrePersist">

    @Override
    public void PrePersist() {
        super.PrePersist();
        if (this.getIsGroup() == null || this.getIsGroup().compareTo("Y") == 0 || this.getIsGroup().compareTo("") == 0) {
            this.setIsGroup("N");
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="shortDescription">
    @Column
    private String shortDescription;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescriptionDD() {
        return "Vacation_shortDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prorateHireTerminate">
//    @Column(length=1)
//    //Y-N
//    private String prorateHireTerminate;
//
//    public String getProrateHireTerminate() {
//        return prorateHireTerminate;
//    }
//
//    public void setProrateHireTerminate(String prorateHireTerminate) {
//        this.prorateHireTerminate = prorateHireTerminate;
//    }
//
//    public String getProrateHireTerminateDD() {
//        return "Vacation_prorateHireTerminate";
//    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="prorateRequest">
//    @Column(length=1)
//    //Y-N
//    private String prorateRequest;
//
//    public String getProrateRequest() {
//        return prorateRequest;
//    }
//
//    public void setProrateRequest(String prorateRequest) {
//        this.prorateRequest = prorateRequest;
//    }
//
//    public String getProrateRequestDD() {
//        return "Vacation_prorateRequest";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prorateDetail">
//    @Column(length=1)
//    //Y-N
//    private String prorateDetail;
//
//    public String getProrateDetail() {
//        return prorateDetail;
//    }
//
//    public void setProrateDetail(String prorateDetail) {
//        this.prorateDetail = prorateDetail;
//    }
//
//    public String getProrateDetailDD() {
//        return "Vacation_prorateDetail";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="balanceEffect">
    @Column(length = 1)
    private String balanceEffect;

    public String getBalanceEffect() {
        return balanceEffect;
    }

    public void setBalanceEffect(String balanceEffect) {
        this.balanceEffect = balanceEffect;
    }

    public String getBalanceEffectDD() {
        return "Vacation_balanceEffect";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectedVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation affectedVacation;

    public Vacation getAffectedVacation() {
        return affectedVacation;
    }

    public void setAffectedVacation(Vacation affectedVacation) {
        this.affectedVacation = affectedVacation;
    }

    public String getAffectedVacationDD() {
        return "Vacation_affectedVacation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="daysAffectingBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal daysAffectingBalance;

    public BigDecimal getDaysAffectingBalance() {
        return daysAffectingBalance;
    }

    public void setDaysAffectingBalance(BigDecimal daysAffectingBalance) {
        this.daysAffectingBalance = daysAffectingBalance;
    }

    public String getDaysAffectingBalanceDD() {
        return "Vacation_daysAffectingBalance";
    }
    @Transient
    private BigDecimal daysAffectingBalanceMask;

    public BigDecimal getDaysAffectingBalanceMask() {
        daysAffectingBalanceMask = daysAffectingBalance;
        return daysAffectingBalanceMask;
    }

    public void setDaysAffectingBalanceMask(BigDecimal daysAffectingBalanceMask) {
        updateDecimalValue("daysAffectingBalance", daysAffectingBalanceMask);
    }

    public String getDaysAffectingBalanceMaskDD() {
        return "Vacation_daysAffectingBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="formulas">
    @OneToMany(mappedBy = "vacation")
    private List<VacationFormula> formulas;

    public List<VacationFormula> getFormulas() {
        return formulas;
    }

    public void setFormulas(List<VacationFormula> formulas) {
        this.formulas = formulas;
    }

    public String getFormulasDD() {
        return "Vacation_formulas";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapse" desc="affectedVacationBalance">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation affectedVacationBalance;

    public Vacation getAffectedVacationBalance() {
        return affectedVacationBalance;
    }

    public String getAffectedVacationBalanceDD() {
        return "Vacation_affectedVacationBalance";
    }

    public void setAffectedVacationBalance(Vacation affectedVacationBalance) {
        this.affectedVacationBalance = affectedVacationBalance;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="settlementIncome">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income settlementIncome;

    public Income getSettlementIncome() {
        return settlementIncome;
    }

    public void setSettlementIncome(Income settlementIncome) {
        this.settlementIncome = settlementIncome;
    }

    public String getSettlementIncomeDD() {
        return "Vacation_settlementIncome";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="settlementDeduction">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction settlementDeduction;

    public Deduction getSettlementDeduction() {
        return settlementDeduction;
    }

    public void setSettlementDeduction(Deduction settlementDeduction) {
        this.settlementDeduction = settlementDeduction;
    }

    public String getSettlementDeductionDD() {
        return "Vacation_settlementDeduction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="maxSettlement">
    @Column(precision = 18, scale = 3)
    private BigDecimal maxSettlement;

    public BigDecimal getMaxSettlement() {
        return maxSettlement;
    }

    public void setMaxSettlement(BigDecimal maxSettlement) {
        this.maxSettlement = maxSettlement;
    }

    public String getMaxSettlementDD() {
        return "Vacation_maxSettlement";
    }
    @Transient
    private BigDecimal maxSettlementMask;

    public BigDecimal getMaxSettlementMask() {
        return maxSettlementMask;
    }

    public void setMaxSettlementMask(BigDecimal maxSettlementMask) {
        this.maxSettlementMask = maxSettlementMask;
    }

    public String getMaxSettlementMaskDD() {
        return "Vacation_maxSettlementMask";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mission">

    private boolean mission = false;

    public boolean isMission() {
        return mission;
    }

    public void setMission(boolean mission) {
        this.mission = mission;
    }

    public String getMissionDD() {
        return "Vacation_mission";
    }
    // </editor-fold>
    private boolean weekendMissionCompensation;
    private boolean holidayMissionCompensation;
    private boolean weekendEligibleOnly;
    private boolean holidatEligibleOnly;

    public boolean isWeekendMissionCompensation() {
        return weekendMissionCompensation;
    }

    public void setWeekendMissionCompensation(boolean weekendMissionCompensation) {
        this.weekendMissionCompensation = weekendMissionCompensation;
    }

    public String getWeekendMissionCompensationDD() {
        return "Vacation_weekendMissionCompensation";
    }

    public boolean isHolidayMissionCompensation() {
        return holidayMissionCompensation;
    }

    public void setHolidayMissionCompensation(boolean holidayMissionCompensation) {
        this.holidayMissionCompensation = holidayMissionCompensation;
    }

    public String getHolidayMissionCompensationDD() {
        return "Vacation_holidayMissionCompensation";
    }

    public boolean isWeekendEligibleOnly() {
        return weekendEligibleOnly;
    }

    public void setWeekendEligibleOnly(boolean weekendEligibleOnly) {
        this.weekendEligibleOnly = weekendEligibleOnly;
    }

    public String getWeekendEligibleOnlyDD() {
        return "Vacation_weekendEligibleOnly";
    }

    public boolean isHolidatEligibleOnly() {
        return holidatEligibleOnly;
    }

    public void setHolidatEligibleOnly(boolean holidatEligibleOnly) {
        this.holidatEligibleOnly = holidatEligibleOnly;
    }

    public String getHolidatEligibleOnlyDD() {
        return "Vacation_holidatEligibleOnly";
    }

    @ManyToOne
    private Vacation weekendVacCompensation;
    @ManyToOne
    private Vacation holidayVacCompensation;

    public Vacation getWeekendVacCompensation() {
        return weekendVacCompensation;
    }

    public void setWeekendVacCompensation(Vacation weekendVacCompensation) {
        this.weekendVacCompensation = weekendVacCompensation;
    }

    public String getWeekendVacCompensationDD() {
        return "Vacation_weekendVacCompensation";
    }

    public Vacation getHolidayVacCompensation() {
        return holidayVacCompensation;
    }

    public void setHolidayVacCompensation(Vacation holidayVacCompensation) {
        this.holidayVacCompensation = holidayVacCompensation;
    }

    public String getHolidayVacCompensationDD() {
        return "Vacation_holidayVacCompensation";
    }

    private BigDecimal weekendCompensationValue;

    public BigDecimal getWeekendCompensationValue() {
        return weekendCompensationValue;
    }

    public void setWeekendCompensationValue(BigDecimal weekendCompensationValue) {
        this.weekendCompensationValue = weekendCompensationValue;
    }

    public String getWeekendCompensationValueDD() {
        return "Vacation_weekendCompensationValue";
    }

    private BigDecimal holidayCompensationValue;

    public BigDecimal getHolidayCompensationValue() {
        return holidayCompensationValue;
    }

    public void setHolidayCompensationValue(BigDecimal holidayCompensationValue) {
        this.holidayCompensationValue = holidayCompensationValue;
    }

    public String getHolidayCompensationValueDD() {
        return "Vacation_holidayCompensationValue";
    }

    // <editor-fold defaultstate="collapsed" desc="intervalValidate">
    private boolean intervalValidate;

    public boolean isIntervalValidate() {
        return intervalValidate;
    }

    public void setIntervalValidate(boolean intervalValidate) {
        this.intervalValidate = intervalValidate;
    }

    public String getIntervalValidateDD() {
        return "Vacation_intervalValidate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="intervalValidateDuplicate">
    private boolean intervalValidateDuplicate;

    public boolean isIntervalValidateDuplicate() {
        return intervalValidateDuplicate;
    }

    public void setIntervalValidateDuplicate(boolean intervalValidateDuplicate) {
        this.intervalValidateDuplicate = intervalValidateDuplicate;
    }

    public String getIntervalValidateDuplicateDD() {
        return "Vacation_intervalValidateDuplicate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="haveNoLeaveBalance">
    private boolean haveNoLeaveBalance;

    public boolean isHaveNoLeaveBalance() {
        return haveNoLeaveBalance;
    }

    public void setHaveNoLeaveBalance(boolean haveNoLeaveBalance) {
        this.haveNoLeaveBalance = haveNoLeaveBalance;
    }

    public String getHaveNoLeaveBalanceDD() {
        return "Vacation_haveNoLeaveBalance";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="intervalRange">
    private BigDecimal intervalRange;

    public BigDecimal getIntervalRange() {
        return intervalRange;
    }

    public void setIntervalRange(BigDecimal intervalRange) {
        this.intervalRange = intervalRange;
    }

    public String getIntervalRangeDD() {
        return "Vacation_intervalRange";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="noLeaveBalanceDesc">
    @JoinColumn(name = "HaveNoLeaveBalance_dbid")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation noLeaveBalanceDesc;

    public Vacation getNoLeaveBalanceDesc() {
        return noLeaveBalanceDesc;
    }

    public void setNoLeaveBalanceDesc(Vacation noLeaveBalanceDesc) {
        this.noLeaveBalanceDesc = noLeaveBalanceDesc;
    }

    public String getNoLeaveBalanceDescDD() {
        return "Vacation_noLeaveBalanceDesc";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="vacFraction">
    private BigDecimal vacFraction;

    public BigDecimal getVacFraction() {
        return vacFraction;
    }

    public void setVacFraction(BigDecimal vacFraction) {
        this.vacFraction = vacFraction;
    }

    public String getVacFractionDD() {
        return "Vacation_vacFraction";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unpaidWarning">
    private boolean unpaidWarning;

    public boolean isUnpaidWarning() {
        return unpaidWarning;
    }

    public void setUnpaidWarning(boolean unpaidWarning) {
        this.unpaidWarning = unpaidWarning;
    }

    public String getUnpaidWarningDD() {
        return "Vacation_unpaidWarning";
    }
    // </editor-fold>
}
