package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepUnitPosition extends RepCompanyLevelBase {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private long dsDBID;

    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }

    public long getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepUnitPosition_dsDBID";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hierarchycode">
    @Column
    private String hierarchycode;

    public void setHierarchycode(String hierarchycode) {
        this.hierarchycode = hierarchycode;
    }

    public String getHierarchycode() {
        return hierarchycode;
    }

    public String getHierarchycodeDD() {
        return "RepUnitPosition_hierarchycode";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="headCount">
    @Column
    private int headCount;

    public void setHeadCount(int headCount) {
        this.headCount = headCount;
    }

    public int getHeadCount() {
        return headCount;
    }

    public String getHeadCountDD() {
        return "RepUnitPosition_headCount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentPositionName">
    @Column
    @Translatable(translationField = "parentPositionNameTranslated")
    private String parentPositionName;

    public void setParentPositionName(String parentPositionName) {
        this.parentPositionName = parentPositionName;
    }

    public String getParentPositionName() {
        return parentPositionName;
    }

    public String getParentPositionNameDD() {
        return "RepUnitPosition_parentPositionName";
    }
    @Transient
    @Translation(originalField = "parentPositionName")
    private String parentPositionNameTranslated;

    public String getParentPositionNameTranslated() {
        return parentPositionNameTranslated;
    }

    public void setParentPositionNameTranslated(String parentPositionNameTranslated) {
        this.parentPositionNameTranslated = parentPositionNameTranslated;
    }

    public String getParentPositionNameTranslatedDD() {
        return "RepUnitPosition_parentPositionName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    @Column
    @Translatable(translationField = "costCenterGroupDescriptionTranslated")
    private String costCenterGroupDescription;

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }

    public String getCostCenterGroupDescriptionDD() {
        return "RepUnitPosition_costCenterGroupDescription";
    }
    @Transient
    @Translation(originalField = "costCenterGroupDescription")
    private String costCenterGroupDescriptionTranslated;

    public String getCostCenterGroupDescriptionTranslated() {
        return costCenterGroupDescriptionTranslated;
    }

    public void setCostCenterGroupDescriptionTranslated(String costCenterGroupDescriptionTranslated) {
        this.costCenterGroupDescriptionTranslated = costCenterGroupDescriptionTranslated;
    }

    public String getCostCenterGroupDescriptionTranslatedDD() {
        return "RepUnitPosition_costCenterGroupDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeStepDescription">
    @Column
    @Translatable(translationField = "payGradeStepDescriptionTranslated")
    private String payGradeStepDescription;

    public void setPayGradeStepDescription(String payGradeStepDescription) {
        this.payGradeStepDescription = payGradeStepDescription;
    }

    public String getPayGradeStepDescription() {
        return payGradeStepDescription;
    }

    public String getPayGradeStepDescriptionDD() {
        return "RepUnitPosition_payGradeStepDescription";
    }
    @Transient
    @Translation(originalField = "payGradeStepDescription")
    private String payGradeStepDescriptionTranslated;

    public String getPayGradeStepDescriptionTranslated() {
        return payGradeStepDescriptionTranslated;
    }

    public void setPayGradeStepDescriptionTranslated(String payGradeStepDescriptionTranslated) {
        this.payGradeStepDescriptionTranslated = payGradeStepDescriptionTranslated;
    }

    public String getPayGradeStepDescriptionTranslatedDD() {
        return "RepUnitPosition_payGradeStepDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacancy">
    @Column
    private int vacancy;

    public void setVacancy(int vacancy) {
        this.vacancy = vacancy;
    }

    public int getVacancy() {
        return vacancy;
    }

    public String getVacancyDD() {
        return "RepUnitPosition_vacancy";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currency">
    private String currency;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "RepUnitPosition_currency";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parentPositionEmployee">
    @Translatable(translationField = "parentPositionEmployeeTranslated")
    private String parentPositionEmployee;

    public String getParentPositionEmployee() {
        return parentPositionEmployee;
    }

    public void setParentPositionEmployee(String parentPositionEmployee) {
        this.parentPositionEmployee = parentPositionEmployee;
    }

    public String getParentPositionEmployeeDD() {
        return "RepUnitPosition_parentPositionEmployee";
    }
    @Transient
    @Translation(originalField = "parentPositionEmployee")
    private String parentPositionEmployeeTranslated;

    public String getParentPositionEmployeeTranslated() {
        return parentPositionEmployeeTranslated;
    }

    public void setParentPositionEmployeeTranslated(String parentPositionEmployeeTranslated) {
        this.parentPositionEmployeeTranslated = parentPositionEmployeeTranslated;
    }

    public String getParentPositionEmployeeTranslatedDD() {
        return "RepUnitPosition_parentPositionEmployee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee">
    @Translatable(translationField = "employeeTranslated")
    private String employee;

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "RepUnitPosition_employee";
    }
    @Transient
    @Translation(originalField = "employee")
    private String employeeTranslated;

    public String getEmployeeTranslated() {
        return employeeTranslated;
    }

    public void setEmployeeTranslated(String employeeTranslated) {
        this.employeeTranslated = employeeTranslated;
    }

    public String getEmployeeTranslatedDD() {
        return "RepUnitPosition_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    @Translatable(translationField = "costCenterDescriptionTranslated")
    private String costCenterDescription;

    public String getCostCenterDescription() {
        return costCenterDescription;
    }

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescriptionDD() {
        return "RepUnitPosition_costCenterDescription";
    }
    @Transient
    @Translation(originalField = "costCenterDescription")
    private String costCenterDescriptionTranslated;

    public String getCostCenterDescriptionTranslated() {
        return costCenterDescriptionTranslated;
    }

    public void setCostCenterDescriptionTranslated(String costCenterDescriptionTranslated) {
        this.costCenterDescriptionTranslated = costCenterDescriptionTranslated;
    }

    public String getCostCenterDescriptionTranslatedDD() {
        return "RepUnitPosition_costCenterDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    @Translatable(translationField = "payGradeDescriptionTranslated")
    private String payGradeDescription;

    public String getPayGradeDescription() {
        return payGradeDescription;
    }

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescriptionDD() {
        return "RepUnitPosition_payGradeDescription";
    }
    @Transient
    @Translation(originalField = "payGradeDescription")
    private String payGradeDescriptionTranslated;

    public String getPayGradeDescriptionTranslated() {
        return payGradeDescriptionTranslated;
    }

    public void setPayGradeDescriptionTranslated(String payGradeDescriptionTranslated) {
        this.payGradeDescriptionTranslated = payGradeDescriptionTranslated;
    }

    public String getPayGradeDescriptionTranslatedDD() {
        return "RepUnitPosition_payGradeDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    @Translatable(translationField = "unitNameTranslated")
    private String unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitNameDD() {
        return "RepUnitPosition_unitName";
    }
    @Transient
    @Translation(originalField = "unitName")
    private String unitNameTranslated;

    public String getUnitNameTranslated() {
        return unitNameTranslated;
    }

    public void setUnitNameTranslated(String unitNameTranslated) {
        this.unitNameTranslated = unitNameTranslated;
    }

    public String getUnitNameTranslatedDD() {
        return "RepUnitPosition_unitName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    @Translatable(translationField = "jobNameTranslated")
    private String jobName;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobNameDD() {
        return "RepUnitPosition_jobName";
    }
    @Transient
    @Translation(originalField = "jobName")
    private String jobNameTranslated;

    public String getJobNameTranslated() {
        return jobNameTranslated;
    }

    public void setJobNameTranslated(String jobNameTranslated) {
        this.jobNameTranslated = jobNameTranslated;
    }

    public String getJobNameTranslatedDD() {
        return "RepUnitPosition_jobName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    @Translatable(translationField = "positionNameTranslated")
    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionNameDD() {
        return "RepUnitPosition_positionName";
    }
    @Transient
    @Translation(originalField = "positionName")
    private String positionNameTranslated;

    public String getPositionNameTranslated() {
        return positionNameTranslated;
    }

    public void setPositionNameTranslated(String positionNameTranslated) {
        this.positionNameTranslated = positionNameTranslated;
    }

    public String getPositionNameTranslatedDD() {
        return "RepUnitPosition_positionName";
    }
    //</editor-fold>
}
