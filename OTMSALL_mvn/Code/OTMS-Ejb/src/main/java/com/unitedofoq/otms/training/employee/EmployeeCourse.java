
package com.unitedofoq.otms.training.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.eds.foundation.employee.idp.EDSIDPActionForBusinessOutcome;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"employee"})
@ChildEntity(fields={"attendances"})
public class EmployeeCourse extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public String getRequestDateDD() {
        return "EmployeeCourse_requestDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestSource">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC requestSource;

    public void setRequestSource(UDC requestSource) {
        this.requestSource = requestSource;
    }

    public UDC getRequestSource() {
        return requestSource;
    }

    public String getRequestSourceDD() {
        return "EmployeeCourse_requestSource";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approved">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC approved;

    public void setApproved(UDC approved) {
        this.approved = approved;
    }

    public UDC getApproved() {
        return approved;
    }

    public String getApprovedDD() {
        return "EmployeeCourse_approved";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvalDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date approvalDate;

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public String getApprovalDateDD() {
        return "EmployeeCourse_approvalDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvedByWhom">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee approvedByWhom;

    public void setApprovedByWhom(Employee approvedByWhom) {
        this.approvedByWhom = approvedByWhom;
    }

    public Employee getApprovedByWhom() {
        return approvedByWhom;
    }

    public String getApprovedByWhomDD() {
        return "EmployeeCourse_approvedByWhom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseSelected">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC courseSelected;

    public void setCourseSelected(UDC courseSelected) {
        this.courseSelected = courseSelected;
    }

    public UDC getCourseSelected() {
        return courseSelected;
    }

    public String getCourseSelectedDD() {
        return "EmployeeCourse_courseSelected";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="selectedByWhom">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee selectedByWhom;

    public void setSelectedByWhom(Employee selectedByWhom) {
        this.selectedByWhom = selectedByWhom;
    }

    public Employee getSelectedByWhom() {
        return selectedByWhom;
    }

    public String getSelectedByWhomDD() {
        return "EmployeeCourse_selectedByWhom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="selectionDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date selectionDate;

    public void setSelectionDate(Date selectionDate) {
        this.selectionDate = selectionDate;
    }

    public Date getSelectionDate() {
        return selectionDate;
    }

    public String getSelectionDateDD() {
        return "EmployeeCourse_selectionDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="acceptanceDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date acceptanceDate;

    public void setAcceptanceDate(Date acceptanceDate) {
        this.acceptanceDate = acceptanceDate;
    }

    public Date getAcceptanceDate() {
        return acceptanceDate;
    }

    public String getAcceptanceDateDD() {
        return "EmployeeCourse_acceptanceDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="acceptedByWhom">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee acceptedByWhom;

    public void setAcceptedByWhom(Employee acceptedByWhom) {
        this.acceptedByWhom = acceptedByWhom;
    }

    public Employee getAcceptedByWhom() {
        return acceptedByWhom;
    }

    public String getAcceptedByWhomDD() {
        return "EmployeeCourse_acceptedByWhom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="accepted">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC accepted;

    public void setAccepted(UDC accepted) {
        this.accepted = accepted;
    }

    public UDC getAccepted() {
        return accepted;
    }
    
    public String getAcceptedDD() {
        return "EmployeeCourse_accepted";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="enrolled">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC enrolled;

    public void setEnrolled(UDC enrolled) {
        this.enrolled = enrolled;
    }

    public UDC getEnrolled() {
        return enrolled;
    }

    public String getEnrolledDD() {
        return "EmployeeCourse_enrolled";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="enrollementDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date enrollementDate;

    public void setEnrollementDate(Date enrollementDate) {
        this.enrollementDate = enrollementDate;
    }

    public Date getEnrollementDate() {
        return enrollementDate;
    }

    public String getEnrollementDateDD() {
        return "EmployeeCourse_enrollementDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="totalAbsenceDays">
    @Column
    private BigDecimal totalAbsenceDays;

    public void setTotalAbsenceDays(BigDecimal totalAbsenceDays) {
        this.totalAbsenceDays = totalAbsenceDays;
    }

    public BigDecimal getTotalAbsenceDays() {
        return totalAbsenceDays;
    }

    public String getTotalAbsenceDaysDD() {
        return "EmployeeCourse_totalAbsenceDays";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="finalScore">
    @Column
    private BigDecimal finalScore;

    public void setFinalScore(BigDecimal finalScore) {
        this.finalScore = finalScore;
    }

    public BigDecimal getFinalScore() {
        return finalScore;
    }

    public String getFinalScoreDD() {
        return "EmployeeCourse_finalScore";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="finalGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC finalGrade;

    public void setFinalGrade(UDC finalGrade) {
        this.finalGrade = finalGrade;
    }

    public UDC getFinalGrade() {
        return finalGrade;
    }

    public String getFinalGradeDD() {
        return "EmployeeCourse_finalGrade";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employeeComments">
    @Column
    @Translatable(translationField = "employeeCommentsTranslated")
    private String employeeComments;

    public void setEmployeeComments(String employeeComments) {
        this.employeeComments = employeeComments;
    }

    public String getEmployeeComments() {
        return employeeComments;
    }

    public String getEmployeeCommentsDD() {
        return "EmployeeCourse_employeeComments";
    }
    @Transient
    @Translation(originalField = "employeeComments")
    private String employeeCommentsTranslated;

    public String getEmployeeCommentsTranslated() {
        return employeeCommentsTranslated;
    }

    public void setEmployeeCommentsTranslated(String employeeCommentsTranslated) {
        this.employeeCommentsTranslated = employeeCommentsTranslated;
    }
    
    public String getEmployeeCommentsTranslatedDD() {
        return "EmployeeCourse_employeeComments";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="commentsOnEmployee">
    @Column
    @Translatable(translationField = "commentsOnEmployeeTranslated")
    private String commentsOnEmployee;

    public void setCommentsOnEmployee(String commentsOnEmployee) {
        this.commentsOnEmployee = commentsOnEmployee;
    }

    public String getCommentsOnEmployee() {
        return commentsOnEmployee;
    }

    public String getCommentsOnEmployeeDD() {
        return "EmployeeCourse_commentsOnEmployee";
    }
    @Transient
    @Translation(originalField = "commentsOnEmployee")
    private String commentsOnEmployeeTranslated;

    public String getCommentsOnEmployeeTranslated() {
        return commentsOnEmployeeTranslated;
    }

    public void setCommentsOnEmployeeTranslated(String commentsOnEmployeeTranslated) {
        this.commentsOnEmployeeTranslated = commentsOnEmployeeTranslated;
    }
    
    public String getCommentsOnEmployeeTranslatedDD() {
        return "EmployeeCourse_commentsOnEmployee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "EmployeeCourse_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC requestStatus;

    public void setRequestStatus(UDC requestStatus) {
        this.requestStatus = requestStatus;
    }

    public UDC getRequestStatus() {
        return requestStatus;
    }

    public String getRequestStatusDD() {
        return "EmployeeCourse_requestStatus";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeCourse_employee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseInformation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation courseInformation;

    public void setCourseInformation(ProviderCourseInformation courseInformation) {
        this.courseInformation = courseInformation;
    }

    public ProviderCourseInformation getCourseInformation() {
        return courseInformation;
    }

    public String getCourseInformationDD() {
        return "EmployeeCourse_courseInformation";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="attendances">
    @OneToMany(mappedBy="employeeCourse")
    private List<EmployeeCourseAttendance> attendances;
    
    public List<EmployeeCourseAttendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(List<EmployeeCourseAttendance> attendances) {
        this.attendances = attendances;
    }
    
    public String getAttendancesDD() {
        return "EmployeeCourse_attendances";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="enrolledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee enrolledBy;

    public Employee getEnrolledBy() {
        return enrolledBy;
    }

    public void setEnrolledBy(Employee enrolledBy) {
        this.enrolledBy = enrolledBy;
    }
    
    public String getEnrolledByDD() {
        return "EmployeeCourse_enrolledBy";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date statusDate;

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }
    
    public String getStatusDateDD() {
        return "EmployeeCourse_statusDate";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="attended">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC attended;

    public UDC getAttended() {
        return attended;
    }

    public void setAttended(UDC attended) {
        this.attended = attended;
    }
    
    public String getAttendedDD() {
        return "EmployeeCourse_attended";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="attendedDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date attendenceDate;

    public Date getAttendenceDate() {
        return attendenceDate;
    }

    public void setAttendenceDate(Date attendenceDate) {
        this.attendenceDate = attendenceDate;
    }
    
    public String getAttendenceDateDD() {
        return "EmployeeCourse_attendenceDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="providerName">
    @Column
    
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
    
    public String getProviderNameDD() {
        return "EmployeeCourse_providerName";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="courseName">
    @Column
    
    private String courseName;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
    public String getCourseNameDD() {
        return "EmployeeCourse_courseName";
    }
    
    
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getStartDateDD() {
        return "EmployeeCourse_startDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public String getEndDateDD() {
        return "EmployeeCourse_endDate";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getNotesDD() {
        return "EmployeeCourse_notes";
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Courseaddress">
    @Column
    private String courseAddress;

    public String getCourseAddress() {
        return courseAddress;
    }

    public void setCourseAddress(String courseAddress) {
        this.courseAddress = courseAddress;
    }
    
        public String getCourseAddressDD() {
        return "EmployeeCourse_courseAddress";
    }


    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="learningmethod">
    @Column
    private String learningMethod;

    public String getLearningMethod() {
        return learningMethod;
    }

    public void setLearningMethod(String learningMethod) {
        this.learningMethod = learningMethod;
    }

    public String getLearningMethodDD() {
        return "EmployeeCourse_learningMethod";
    }    
    
    // </editor-fold>
    
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeCourse_m2mCheck";
    }

    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSIDPActionForBusinessOutcome action;

    public EDSIDPActionForBusinessOutcome getAction() {
        return action;
    }
    
    public String getActionDD() {
        return "EmployeeCourse_action";
    }

    public void setAction(EDSIDPActionForBusinessOutcome action) {
        this.action = action;
    }
    
    
    
    @PostLoad
    public void postLoad() {
        setM2mCheck(true);
    }
}
