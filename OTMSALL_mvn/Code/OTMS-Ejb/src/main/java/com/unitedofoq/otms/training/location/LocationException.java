/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.location;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.TrainingExceptionBase;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"location"})
@DiscriminatorValue("LOCATION")
public class LocationException extends TrainingExceptionBase {
    // <editor-fold defaultstate="collapsed" desc="location">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Location location;
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
    
    public String getLocationDD() {
        return "LocationExceptions_location";
    }
    // </editor-fold>
}
