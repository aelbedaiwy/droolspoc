/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepEmployeeElementRating extends RepEmployeeBase  {
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeElementRating_dsDbid";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="duration">
    private String duration;
    
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
    
    public String getDurationDD() {
        return "RepEmployeeElementRating_duration";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="template">
    private String template;
   
    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
    
    public String getTemplateDD() {
        return "RepEmployeeElementRating_template";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;
    
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getStartDateDD() {
        return "RepEmployeeElementRating_startDate";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="finishDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finishDate;
    
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }
    
    public String getFinishDateDD() {
        return "RepEmployeeElementRating_finishDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="year">
    private String year;
    
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
    public String getYearDD() {
        return "RepEmployeeElementRating_year";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="purpose">
    private String purpose;

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
    
    public String getPurposeDD() {
        return "RepEmployeeElementRating_purpose";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="element">
    @Translatable(translationField = "elementTranslated")
    private String element;

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }
    
    public String getElementDD() {
        return "RepEmployeeElementRating_element";
    }
    
    @Transient
    @Translation(originalField = "element")
    private String elementTranslated;

    public String getElementTranslated() {
        return elementTranslated;
    }

    public void setElementTranslated(String elementTranslated) {
        this.elementTranslated = elementTranslated;
    }
    
    public String getElementTranslatedDD() {
        return "RepEmployeeElementRating_element";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="totalscore">
    private BigDecimal totalscore;

    public BigDecimal getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(BigDecimal totalscore) {
        this.totalscore = totalscore;
    }
    
    public String getTotalscoreDD() {
        return "RepEmployeeElementRating_totalscore";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="percentage">
    private BigDecimal percentage;
    
    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
    
    public String getPercentageDD() {
        return "RepEmployeeElementRating_percentage";
    }
    //</editor-fold>
}
