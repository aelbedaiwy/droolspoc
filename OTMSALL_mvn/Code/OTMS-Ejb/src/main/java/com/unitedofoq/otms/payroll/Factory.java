/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author unitedofoq
 */
@Entity
public class Factory extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="name">
    @Column
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "Factory_name";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="nameTranslated">
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "Factory_nameTranslated";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="owner">
    @Column
    @Translatable(translationField = "ownerTranslated")
    private String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerDD() {
        return "Factory_owner";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ownerTransalted">
    @Transient
    @Translation(originalField = "owner")
    private String ownerTranslated;

    public String getOwnerTranslated() {
        return ownerTranslated;
    }

    public void setOwnerTranslated(String ownerTranslated) {
        this.ownerTranslated = ownerTranslated;
    }

    public String getOwnerTranslatedDD() {
        return "Factory_ownerTransalted";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="buildingNumber">
    @Column
    private int buildingNumber;

    public int getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(int buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getBuildingNumberDD() {
        return "Factory_buildingNumber";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "Factory_legalEntity";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="comapnyLegalView">
    @Column
    private String companyLegalView;
    
    public String getCompanyLegalView() {
        return companyLegalView;
    }

    public void setCompanyLegalView(String comapnyLegalView) {
        this.companyLegalView = comapnyLegalView;
    }

    public String getCompanyLegalViewDD() {
        return "Factory_companyLegalView";
    }
    // </editor-fold>

}
