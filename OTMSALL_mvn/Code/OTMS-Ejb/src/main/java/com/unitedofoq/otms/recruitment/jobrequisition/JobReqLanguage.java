package com.unitedofoq.otms.recruitment.jobrequisition;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccupationElementRatingBase;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobRequisition"})
public class JobReqLanguage extends OccupationElementRatingBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="language_DBID", nullable=false)
    private UDC language;
    public String getLanguageDD(){
        return "JobReqLanguage_language";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobRequisition_DBID", nullable=false)
    private JobRequisition jobRequisition;

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public UDC getLanguage() {
        return language;
    }

    public void setLanguage(UDC language) {
        this.language = language;
    }
    
}