package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"employee"})
@ChildEntity(fields = {"loanBalanceSheets", "settlements"})
public class EmployeeLoan extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="loan">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Loan loan;

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public String getLoanDD() {
        return "EmployeeLoan_loan";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeLoan_employee";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="approvedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee approvedBy;

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "EmployeeLoan_approvedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startYear">
    private Integer startYear;

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public String getStartYearDD() {
        return "EmployeeLoan_startYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startMonth">
    private Integer startMonth;

    public Integer getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartMonthDD() {
        return "EmployeeLoan_startMonth";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="installmentNumber">
    private Integer installmentNumber;

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public String getInstallmentNumberDD() {
        return "EmployeeLoan_installmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal monthValue;

    public BigDecimal getMonthValue() {
        return monthValue;
    }

    public void setMonthValue(BigDecimal monthValue) {
        this.monthValue = monthValue;
    }

    public String getMonthValueDD() {
        return "EmployeeLoan_monthValue";
    }
    @Transient
    private BigDecimal monthValueMask;

    public BigDecimal getMonthValueMask() {
        monthValueMask = monthValue;
        return monthValueMask;
    }

    public void setMonthValueMask(BigDecimal monthValueMask) {
        updateDecimalValue("monthValue", monthValueMask);
    }

    public String getMonthValueMaskDD() {
        return "EmployeeLoan_monthValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="suspendedToMonth">
    private Integer suspendedToMonth;

    public Integer getSuspendedToMonth() {
        return suspendedToMonth;
    }

    public void setSuspendedToMonth(Integer suspendedToMonth) {
        this.suspendedToMonth = suspendedToMonth;
    }

    public String getSuspendedToMonthDD() {
        return "EmployeeLoan_suspendedToMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="suspendedToYear">
    private Integer suspendedToYear;

    public Integer getSuspendedToYear() {
        return suspendedToYear;
    }

    public void setSuspendedToYear(Integer suspendedToYear) {
        this.suspendedToYear = suspendedToYear;
    }

    public String getSuspendedToYearDD() {
        return "EmployeeLoan_suspendedToYear";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="comment">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "EmployeeLoan_comments";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal loanValue;

    public BigDecimal getLoanValue() {
        return loanValue;
    }

    public void setLoanValue(BigDecimal loanValue) {
        this.loanValue = loanValue;
    }

    public String getLoanValueDD() {
        return "EmployeeLoan_loanValue";
    }
    @Transient
    private BigDecimal loanValueMask;

    public BigDecimal getLoanValueMask() {
        loanValueMask = loanValue;
        return loanValueMask;
    }

    public void setLoanValueMask(BigDecimal loanValueMask) {
        updateDecimalValue("loanValue", loanValueMask);
    }

    public String getLoanValueMaskDD() {
        return "EmployeeLoan_loanValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="status">
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "EmployeeLoan_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal paidAmount;

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaidAmountDD() {
        return "EmployeeLoan_paidAmount";
    }
    @Transient
    private BigDecimal paidAmountMask;

    public BigDecimal getPaidAmountMask() {
        paidAmountMask = paidAmount;
        return paidAmountMask;
    }

    public void setPaidAmountMask(BigDecimal paidAmountMask) {
        updateDecimalValue("paidAmount", paidAmountMask);
    }

    public String getPaidAmountMaskDD() {
        return "EmployeeLoan_paidAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidInstallment">
    @Column
    private Integer paidInstallment;

    public Integer getPaidInstallment() {
        return paidInstallment;
    }

    public void setPaidInstallment(Integer paidInstallment) {
        this.paidInstallment = paidInstallment;
    }

    public String getPaidInstallmentDD() {
        return "EmployeeLoan_paidInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="lastMonth">
    private Integer lastMonth;

    public Integer getLastMonth() {
        return lastMonth;
    }

    public void setLastMonth(Integer lastMonth) {
        this.lastMonth = lastMonth;
    }

    public String getLastMonthDD() {
        return "EmployeeLoan_lastMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastYear">
    private Integer lastYear;

    public Integer getLastYear() {
        return lastYear;
    }

    public void setLastYear(Integer lastYear) {
        this.lastYear = lastYear;
    }

    public String getLastYearDD() {
        return "EmployeeLoan_lastYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastPaidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal lastPaidAmount;

    public BigDecimal getLastPaidAmount() {
        return lastPaidAmount;
    }

    public void setLastPaidAmount(BigDecimal lastPaidAmount) {
        this.lastPaidAmount = lastPaidAmount;
    }

    public String getLastPaidAmountDD() {
        return "EmployeeLoan_lastPaidAmount";
    }

    private String lastPaidAmountEnc;

    public String getLastPaidAmountEnc() {
        return lastPaidAmountEnc;
    }

    public void setLastPaidAmountEnc(String lastPaidAmountEnc) {
        this.lastPaidAmountEnc = lastPaidAmountEnc;
    }

    @Transient
    private BigDecimal lastPaidAmountMask;

    public BigDecimal getLastPaidAmountMask() {
        lastPaidAmountMask = lastPaidAmount;
        return lastPaidAmountMask;
    }

    public void setLastPaidAmountMask(BigDecimal lastPaidAmountMask) {
        updateDecimalValue("lastPaidAmount", lastPaidAmountMask);
    }

    public String getLastPaidAmountMaskDD() {
        return "EmployeeLoan_lastPaidAmountMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="loanOrInstallment">
    @Column(length = 1)
    private String loanOrInstallment = "L";

    public String getLoanOrInstallment() {
        return loanOrInstallment;
    }

    public void setLoanOrInstallment(String loanOrInstallment) {
        this.loanOrInstallment = loanOrInstallment;
    }

    public String getLoanOrInstallmentDD() {
        return "EmployeeLoan_loanOrInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="deducedMonth">
    private Integer deducedMonth;

    public Integer getDeducedMonth() {
        return deducedMonth;
    }

    public void setDeducedMonth(Integer deducedMonth) {
        this.deducedMonth = deducedMonth;
    }

    public String getDeducedMonthDD() {
        return "EmployeeLoan_deducedMonth";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="deducedYear">
    private Integer deducedYear;

    public Integer getDeducedYear() {
        return deducedYear;
    }

    public void setDeducedYear(Integer deducedYear) {
        this.deducedYear = deducedYear;
    }

    public String getDeducedYearDD() {
        return "EmployeeLoan_deducedYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deducedPercentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal deducedPercentage;

    public BigDecimal getDeducedPercentage() {
        return deducedPercentage;
    }

    public void setDeducedPercentage(BigDecimal deducedPercentage) {
        this.deducedPercentage = deducedPercentage;
    }

    public String getDeducedPercentageDD() {
        return "EmployeeLoan_deducedPercentage";
    }
    @Transient
    private BigDecimal deducedPercentageMask;

    public BigDecimal getDeducedPercentageMask() {
        deducedPercentageMask = deducedPercentage;
        return deducedPercentageMask;
    }

    public void setDeducedPercentageMask(BigDecimal deducedPercentageMask) {
        updateDecimalValue("deducedPercentage", deducedPercentageMask);
    }

    public String getDeducedPercentageMaskDD() {
        return "EmployeeLoan_deducedPercentageMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fixedInstallmentsNo">
    @Column(precision = 25, scale = 13)
    private BigDecimal fixedInstallmentsNo;

    public BigDecimal getFixedInstallmentsNo() {
        return fixedInstallmentsNo;
    }

    public void setFixedInstallmentsNo(BigDecimal fixedInstallmentsNo) {
        this.fixedInstallmentsNo = fixedInstallmentsNo;
    }

    public String getFixedInstallmentsNoDD() {
        return "EmployeeLoan_fixedInstallmentsNo";
    }
    @Transient
    private BigDecimal fixedInstallmentsNoMask;

    public BigDecimal getFixedInstallmentsNoMask() {
        fixedInstallmentsNoMask = fixedInstallmentsNo;
        return fixedInstallmentsNoMask;
    }

    public void setFixedInstallmentsNoMask(BigDecimal fixedInstallmentsNoMask) {
        updateDecimalValue("fixedInstallmentsNo", fixedInstallmentsNoMask);
    }

    public String getFixedInstallmentsNoMaskDD() {
        return "EmployeeLoan_fixedInstallmentsNoMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeeLoan_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "EmployeeLoan_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue", maximumValueMask);
    }

    public String getMaximumValueMaskDD() {
        return "EmployeeLoan_maximumValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="minimumValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal minimumValue;

    public BigDecimal getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(BigDecimal minimumValue) {
        this.minimumValue = minimumValue;
    }

    public String getMinimumValueDD() {
        return "EmployeeLoan_minimumValue";
    }
    @Transient
    private BigDecimal minimumValueMask;

    public BigDecimal getMinimumValueMask() {
        minimumValueMask = minimumValue;
        return minimumValueMask;
    }

    public void setMinimumValueMask(BigDecimal minimumValueMask) {
        updateDecimalValue("minimumValue", minimumValueMask);
    }

    public String getMinimumValueMaskDD() {
        return "EmployeeLoan_minimumValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="settInstallmentNumber">
    private Integer settInstallmentNumber;

    public Integer getSettInstallmentNumber() {
        return settInstallmentNumber;
    }

    public void setSettInstallmentNumber(Integer settInstallmentNumber) {
        this.settInstallmentNumber = settInstallmentNumber;
    }

    private String settInstallmentNumberEnc;

    public String getSettInstallmentNumberEnc() {
        return settInstallmentNumberEnc;
    }

    public void setSettInstallmentNumberEnc(String settInstallmentNumberEnc) {
        this.settInstallmentNumberEnc = settInstallmentNumberEnc;
    }

    public String getSettInstallmentNumberDD() {
        return "EmployeeLoan_settInstallmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="settLoanValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal settLoanValue;

    public BigDecimal getSettLoanValue() {
        return settLoanValue;
    }

    public void setSettLoanValue(BigDecimal settLoanValue) {
        this.settLoanValue = settLoanValue;
    }

    public String getSettLoanValueDD() {
        return "EmployeeLoan_settLoanValue";
    }
    @Transient
    private BigDecimal settLoanValueMask;

    public BigDecimal getSettLoanValueMask() {
        settLoanValueMask = settLoanValue;
        return settLoanValueMask;
    }

    public void setSettLoanValueMask(BigDecimal settLoanValueMask) {
        updateDecimalValue("settLoanValue", settLoanValueMask);
    }

    public String getSettLoanValueMaskDD() {
        return "EmployeeLoan_settLoanValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="settDeduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction settDeduction;

    public Deduction getSettDeduction() {
        return settDeduction;
    }

    public void setSettDeduction(Deduction settDeduction) {
        this.settDeduction = settDeduction;
    }

    public String getSettDeductionDD() {
        return "EmployeeLoan_settDeduction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="equallyDistributed">
    @Column(length = 1)
    private String equallyDistributed = "Y";

    public String getEquallyDistributed() {
        return equallyDistributed;
    }

    public void setEquallyDistributed(String equallyDistributed) {
        this.equallyDistributed = equallyDistributed;
    }

    public String getEquallyDistributedDD() {
        return "EmployeeLoan_equallyDistributed";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="remainingLoanValue">
    @Transient
    private BigDecimal remainingLoanValue;

    public BigDecimal getRemainingLoanValue() {
        BigDecimal orgLoanValue;
        BigDecimal orgPaidAmount;
        orgLoanValue = getLoanValue();
        if (orgLoanValue == null) {
            orgLoanValue = new BigDecimal(BigInteger.ZERO);
        }
        orgPaidAmount = getPaidAmount();
        if (orgPaidAmount == null) {
            orgPaidAmount = new BigDecimal(BigInteger.ZERO);
        }
        remainingLoanValue = orgLoanValue.subtract(orgPaidAmount);
        return remainingLoanValue;
    }

    public void setRemainingLoanValue(BigDecimal remainingLoanValue) {
        this.remainingLoanValue = remainingLoanValue;
    }

    public String getRemainingLoanValueDD() {
        return "EmployeeLoan_remainingLoanValue";
    }
    @Transient
    private BigDecimal remainingLoanValueMask;

    public BigDecimal getRemainingLoanValueMask() {
        remainingLoanValueMask = remainingLoanValue;
        return remainingLoanValueMask;
    }

    public void setRemainingLoanValueMask(BigDecimal remainingLoanValueMask) {
        updateDecimalValue("remainingLoanValue", remainingLoanValueMask);
    }

    public String getRemainingLoanValueMaskDD() {
        return "EmployeeLoan_remainingLoanValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="orgMonthValue">
    @Transient
    private BigDecimal orgMonthValue;

    public BigDecimal getOrgMonthValue() {
        orgMonthValue = monthValue;
        return orgMonthValue;
    }

    public void setOrgMonthValue(BigDecimal orgMonthValue) {
        this.orgMonthValue = orgMonthValue;
    }

    public String getOrgMonthValueDD() {
        return "EmployeeLoan_orgMonthValue";
    }
    @Transient
    private BigDecimal orgMonthValueMask;

    public BigDecimal getOrgMonthValueMask() {
        orgMonthValueMask = orgMonthValue;
        return orgMonthValueMask;
    }

    public void setOrgMonthValueMask(BigDecimal orgMonthValueMask) {
        updateDecimalValue("orgMonthValue", orgMonthValueMask);
    }

    public String getOrgMonthValueMaskDD() {
        return "EmployeeLoan_orgMonthValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="remainingIns">
    @Transient
    private Integer remainingIns;

    public Integer getRemainingIns() {
        Integer orgIns;
        Integer paidIns;
        orgIns = installmentNumber;
        if (orgIns == null) {
            orgIns = 0;
        }
        if (paidInstallment == null) {
            paidIns = 0;
        } else {
            paidIns = paidInstallment.intValue();
        }

        remainingIns = orgIns - paidIns;
        return remainingIns;
    }

    public void setRemainingIns(Integer remainingIns) {
        this.remainingIns = remainingIns;
    }

    public String getRemainingInsDD() {
        return "EmployeeLoan_remainingIns";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="orgInstallment">
    @Transient
    private Integer orgInstallment;

    public Integer getOrgInstallment() {
        orgInstallment = installmentNumber;
        return orgInstallment;
    }

    public void setOrgInstallment(Integer orgInstallment) {
        this.orgInstallment = orgInstallment;
    }

    public String getOrgInstallmentDD() {
        return "EmployeeLoan_orgInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee cancelledBy;

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancelledByDD() {
        return "EmployeeLoan_cancelledBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledComment">
    private String cancelledComment;

    public String getCancelledComment() {
        return cancelledComment;
    }

    public void setCancelledComment(String cancelledComment) {
        this.cancelledComment = cancelledComment;
    }

    public String getCancelledCommentDD() {
        return "EmployeeLoan_cancelledComment";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelledDate;

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getCancelledDateDD() {
        return "EmployeeLoan_cancelledDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="suspendOrCancel">
    private String suspendOrCancel;

    public String getSuspendOrCancel() {
        return suspendOrCancel;
    }

    public void setSuspendOrCancel(String suspendOrCancel) {
        this.suspendOrCancel = suspendOrCancel;
    }

    public String getSuspendOrCancelDD() {
        return "EmployeeLoan_suspendOrCancel";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="resInstallmentNumber">
    private Integer resInstallmentNumber;

    public Integer getResInstallmentNumber() {
        return resInstallmentNumber;
    }

    public void setResInstallmentNumber(Integer resInstallmentNumber) {
        this.resInstallmentNumber = resInstallmentNumber;
    }

    public String getResInstallmentNumberDD() {
        return "EmployeeLoan_resInstallmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="resMonthValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal resMonthValue;

    public BigDecimal getResMonthValue() {
        return resMonthValue;
    }

    public void setResMonthValue(BigDecimal resMonthValue) {
        this.resMonthValue = resMonthValue;
    }

    public String getResMonthValueDD() {
        return "EmployeeLoan_resMonthValue";
    }
    @Transient
    private BigDecimal resMonthValueMask;

    public BigDecimal getResMonthValueMask() {
        resMonthValueMask = resMonthValue;
        return resMonthValueMask;
    }

    public void setResMonthValueMask(BigDecimal resMonthValueMask) {
        updateDecimalValue("resMonthValue", resMonthValueMask);
    }

    public String getResMonthValueMaskDD() {
        return "EmployeeLoan_resMonthValueMask";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="loanBalanceSheets">
    @OneToMany(mappedBy = "employeeLoan")
    private List<LoanBalanceSheet> loanBalanceSheets;

    public List<LoanBalanceSheet> getLoanBalanceSheets() {
        return loanBalanceSheets;
    }

    public void setLoanBalanceSheets(List<LoanBalanceSheet> loanBalanceSheets) {
        this.loanBalanceSheets = loanBalanceSheets;
    }

    public String getLoanBalanceSheetsDD() {
        return "EmployeeLoan_loanBalanceSheets";

    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="loanCode">
    private String loanCode;

    public String getLoanCode() {
        return loanCode;
    }

    public void setLoanCode(String loanCode) {
        this.loanCode = loanCode;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="monthValueEnc">
    private String monthValueEnc;

    public String getMonthValueEnc() {
        return monthValueEnc;
    }

    public void setMonthValueEnc(String monthValueEnc) {
        this.monthValueEnc = monthValueEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="loanValueEnc">
    private String loanValueEnc;

    public String getLoanValueEnc() {
        return loanValueEnc;
    }

    public void setLoanValueEnc(String loanValueEnc) {
        this.loanValueEnc = loanValueEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="paidAmountEnc">
    private String paidAmountEnc;

    public String getPaidAmountEnc() {
        return paidAmountEnc;
    }

    public void setPaidAmountEnc(String paidAmountEnc) {
        this.paidAmountEnc = paidAmountEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="installmentNumberEnc">
    private String installmentNumberEnc;

    public String getInstallmentNumberEnc() {
        return installmentNumberEnc;
    }

    public void setInstallmentNumberEnc(String installmentNumberEnc) {
        this.installmentNumberEnc = installmentNumberEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="settloanvalueenc">
    private String settloanvalueenc;

    public String getSettloanvalueenc() {
        return settloanvalueenc;
    }

    public void setSettloanvalueenc(String settloanvalueenc) {
        this.settloanvalueenc = settloanvalueenc;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="settlements">
    @OneToMany(mappedBy = "employeeLoan")

    private List<EmployeeLoanSettlement> settlements;

    public List<EmployeeLoanSettlement> getSettlements() {
        return settlements;
    }

    public String getSettlementsDD() {
        return "EmployeeLoan_settlements";
    }

    public void setSettlements(List<EmployeeLoanSettlement> settlements) {

        this.settlements = settlements;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="lastSettlement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)

    private EmployeeLoanSettlement lastSettlement;

    public EmployeeLoanSettlement getLastSettlement() {
        return lastSettlement;
    }

    public String getLastSettlementDD() {
        return "EmployeeLoan_lastSettlement";
    }

    public void setLastSettlement(EmployeeLoanSettlement lastSettlement) {

        this.lastSettlement = lastSettlement;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="settCancelledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)

    private Employee settCancelledBy;

    public Employee getSettCancelledBy() {
        return settCancelledBy;
    }

    public String getSettCancelledByDD() {
        return "EmployeeLoan_settCancelledBy";
    }

    public void setSettCancelledBy(Employee settCancelledBy) {

        this.settCancelledBy = settCancelledBy;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="settCancelledByComments">
    private String settCancelledByComments;

    public String getSettCancelledByComments() {
        return settCancelledByComments;
    }

    public String getSettCancelledByCommentsDD() {
        return "EmployeeLoan_settCancelledByComments";
    }

    public void setSettCancelledByComments(String settCancelledByComments) {

        this.settCancelledByComments = settCancelledByComments;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="guaranteePerson">
    @ManyToOne
    private Employee guaranteePerson;

    public Employee getGuaranteePerson() {
        return guaranteePerson;
    }

    public void setGuaranteePerson(Employee guaranteePerson) {
        this.guaranteePerson = guaranteePerson;
    }

    public String getGuaranteePersonDD() {
        return "EmployeeLoan_guaranteePerson";
    }
    // </editor-fold>
}
