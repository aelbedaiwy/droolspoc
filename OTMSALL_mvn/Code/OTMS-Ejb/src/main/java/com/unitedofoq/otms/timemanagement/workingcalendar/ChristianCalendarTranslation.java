/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author lap2
 */
@Entity
public class ChristianCalendarTranslation extends BaseEntityTranslation{
    //<editor-fold defaultstate="collapsed" desc="holidayName">
    private String holidayName;

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }
    //</editor-fold>
}