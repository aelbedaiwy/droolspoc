package com.unitedofoq.otms.foundation.system;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

@Entity
public class SystemLookUpDetailTranslation extends BaseEntityTranslation {
    //<editor-fold defaultstate="collapsed" desc="description">

    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    //</editor-fold>
}
