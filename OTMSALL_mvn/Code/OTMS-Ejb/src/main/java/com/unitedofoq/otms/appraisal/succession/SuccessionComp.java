
package com.unitedofoq.otms.appraisal.succession;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import com.unitedofoq.otms.foundation.competency.CompetencyLevel;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"plan"})
public class SuccessionComp extends BaseEntity  {
    
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "SuccessionComp_description";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="competencyDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date competencyDate;

    public Date getCompetencyDate() {
        return competencyDate;
    }

    public void setCompetencyDate(Date competencyDate) {
        this.competencyDate = competencyDate;
    }

    public String getCompetencyDateDD() {
        return "SuccessionComp_competencyDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="competencyLevel">
    @ManyToOne(fetch= FetchType.LAZY)
    private CompetencyLevel competencyLevel;

    public CompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }

    public void setCompetencyLevel(CompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }

    public String getCompetencyLevelDD() {
        return "SuccessionComp_competencyLevel";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch= FetchType.LAZY)
    private Competency competency;

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }

    public Competency getCompetency() {
        return competency;
    }

    public String getCompetencyDD() {
        return "SuccessionComp_competency";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="plan">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch= FetchType.LAZY)
    private SuccessionPlan plan;

    public void setPlan(SuccessionPlan plan) {
        this.plan = plan;
    }

    public SuccessionPlan getPlan() {
        return plan;
    }

    public String getPlanDD() {
        return "SuccessionComp_plan";
    }
    // </editor-fold>
}