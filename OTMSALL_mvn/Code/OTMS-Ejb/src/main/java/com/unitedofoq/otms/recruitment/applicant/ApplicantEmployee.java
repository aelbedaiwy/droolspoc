/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author lap2
 */
@Entity
@DiscriminatorValue("APPEMP")  
public class ApplicantEmployee extends Applicant {
    //<editor-fold defaultstate="collapsed" desc="employee">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;
    
    public Employee getEmployee(){
        return employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD(){
        return "ApplicantEmployee_employee";
    }
    //</editor-fold>
}
