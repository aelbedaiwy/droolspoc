package com.unitedofoq.otms.appraisal.scaletype;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"scaleType"})
public class ScaleTypeRule extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="scaleType">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ScaleType scaleType;

    public ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    public String getScaleTypeDD() {
        return "ScaleTypeRule_scaleType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "ScaleTypeRule_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="value">
    @Column(nullable = false)
    private BigDecimal scaleValue;

    public BigDecimal getScaleValue() {
        return scaleValue;
    }

    public void setScaleValue(BigDecimal scaleValue) {
        this.scaleValue = scaleValue;
    }

    public String getScaleValueDD() {
        return "ScaleTypeRule_scaleValue";
    }
    @Transient
    private BigDecimal valueMask;

    public BigDecimal getValueMask() {
        valueMask = scaleValue;
        return valueMask;
    }

    public void setValueMask(BigDecimal valueMask) {
        updateDecimalValue("scaleValue", valueMask);
    }

    public String getValueMaskDD() {
        return "ScaleTypeRule_valueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueFrom">
    @Column
    private BigDecimal valueFrom;

    public BigDecimal getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(BigDecimal valueFrom) {
        this.valueFrom = valueFrom;
    }

    public String getValueFromDD() {
        return "ScaleTypeRule_valueFrom";
    }
    @Transient
    private BigDecimal valueFromMask;

    public BigDecimal getValueFromMask() {
        valueFromMask = valueFrom;
        return valueFromMask;
    }

    public void setValueFromMask(BigDecimal valueFromMask) {
        updateDecimalValue("valueFrom", valueFromMask);
    }

    public String getValueFromMaskDD() {
        return "ScaleTypeRule_valueFromMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueTo">
    @Column
    private BigDecimal valueTo;

    public BigDecimal getValueTo() {
        return valueTo;
    }

    public void setValueTo(BigDecimal valueTo) {
        this.valueTo = valueTo;
    }

    public String getValueToDD() {
        return "ScaleTypeRule_valueTo";
    }
    @Transient
    private BigDecimal valueToMask;

    public BigDecimal getValueToMask() {
        valueToMask = valueTo;
        return valueToMask;
    }

    public void setValueToMask(BigDecimal valueToMask) {
        updateDecimalValue("valueTo", valueToMask);
    }

    public String getValueToMaskDD() {
        return "ScaleTypeRule_valueToMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="meritIncrease">
    @Column
    private BigDecimal meritIncrease;

    public BigDecimal getMeritIncrease() {
        return meritIncrease;
    }

    public void setMeritIncrease(BigDecimal meritIncrease) {
        this.meritIncrease = meritIncrease;
    }

    public String getMeritIncreaseDD() {
        return "ScaleTypeRule_meritIncrease";
    }
    @Transient
    private BigDecimal meritIncreaseMask;

    public BigDecimal getMeritIncreaseMask() {
        meritIncreaseMask = meritIncrease;
        return meritIncreaseMask;
    }

    public void setMeritIncreaseMask(BigDecimal meritIncreaseMask) {
        updateDecimalValue("meritIncrease", meritIncreaseMask);
    }

    public String getMeritIncreaseMaskDD() {
        return "ScaleTypeRule_meritIncreaseMask";
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="bonusIncrease">
    @Column
    private BigDecimal bonusIncrease;

    public BigDecimal getBonusIncrease() {
        return bonusIncrease;
    }

    public void setBonusIncrease(BigDecimal bonusIncrease) {
        this.bonusIncrease = bonusIncrease;
    }

    public String getBonusIncreaseDD() {
        return "ScaleTypeRule_bonusIncrease";
    }
    @Transient
    private BigDecimal bonusIncreaseMask;

    public BigDecimal getBonusIncreaseMask() {
        bonusIncreaseMask = bonusIncrease;
        return bonusIncreaseMask;
    }

    public void setBonusIncreaseMask(BigDecimal bonusIncreaseMask) {
        updateDecimalValue("bonusIncrease", bonusIncreaseMask);
    }

    public String getBonusIncreaseMaskDD() {
        return "ScaleTypeRule_bonusIncreaseMask";
    }

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "ScaleTypeRule_code";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sequence">
    @Column
    private String sequence;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD() {
        return "ScaleTypeRule_sequence";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="meritIncreasePercent">
    @Column(precision = 25, scale = 13)
    private BigDecimal meritIncreasePercent;

    public BigDecimal getMeritIncreasePercent() {
        return meritIncreasePercent;
    }

    public void setMeritIncreasePercent(BigDecimal meritIncreasePercent) {
        this.meritIncreasePercent = meritIncreasePercent;
    }

    public String getMeritIncreasePercentDD() {
        return "ScaleTypeRule_meritIncreasePercent";
    }

    @Transient
    private BigDecimal meritIncreasePercentMask;

    public BigDecimal getMeritIncreasePercentMask() {
        meritIncreasePercentMask = meritIncreasePercent;
        return meritIncreasePercentMask;
    }

    public void setMeritIncreasePercentMask(BigDecimal meritIncreasePercentMask) {
        updateDecimalValue("meritIncreasePercent", meritIncreasePercentMask);
    }

    public String getMeritIncreasePercentMaskDD() {
        return "ScaleTypeRule_meritIncreasePercentMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="meritIncreaseVal">
    @Column(precision = 25, scale = 13)
    private BigDecimal meritIncreaseVal;

    public BigDecimal getMeritIncreaseVal() {
        return meritIncreaseVal;
    }

    public void setMeritIncreaseVal(BigDecimal meritIncreaseVal) {
        this.meritIncreaseVal = meritIncreaseVal;
    }

    public String getMeritIncreaseValDD() {
        return "ScaleTypeRule_meritIncreaseVal";
    }

    @Transient
    private BigDecimal meritIncreaseValMask;

    public BigDecimal getMeritIncreaseValMask() {
        meritIncreaseValMask = meritIncreaseVal;
        return meritIncreaseValMask;
    }

    public void setMeritIncreaseValMask(BigDecimal meritIncreaseValMask) {
        updateDecimalValue("meritIncreaseVal", meritIncreaseValMask);
    }

    public String getMeritIncreaseValMaskDD() {
        return "ScaleTypeRule_meritIncreaseValMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="distribution">
    private Double distribution;

    public Double getDistribution() {
        return distribution;
    }

    public void setDistribution(Double distribution) {
        this.distribution = distribution;
    }

    public String getDistributionDD() {
        return "ScaleTypeRule_distribution";
    }
    //</editor-fold>
}
