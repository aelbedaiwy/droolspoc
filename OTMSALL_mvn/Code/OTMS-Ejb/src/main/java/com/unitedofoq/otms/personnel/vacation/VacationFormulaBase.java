package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class VacationFormulaBase extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="balance">
    @Column(precision = 25, scale = 13)
    private BigDecimal balance;

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getBalanceDD() {
        return "VacationFormulaBase_balance";
    }
    @Transient
    private BigDecimal balanceMask = java.math.BigDecimal.ZERO;

    public BigDecimal getBalanceMask() {
        balanceMask = balance;
        return balanceMask;
    }

    public void setBalanceMask(BigDecimal balanceMask) {
        updateDecimalValue("balance", balanceMask);
    }

    public String getBalanceMaskDD() {
        return "VacationFormulaBase_balanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job job;

    public void setJob(Job job) {
        this.job = job;
    }

    public Job getJob() {
        return job;
    }

    public String getJobDD() {
        return "VacationFormulaBase_job";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public void setPosition(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "VacationFormulaBase_position";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "VacationFormulaBase_location";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="maritalStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC maritalStatus;

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatusDD() {
        return "VacationFormulaBase_maritalStatus";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade payGrade;

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public PayGrade getPayGrade() {
        return payGrade;
    }

    public String getPayGradeDD() {
        return "VacationFormulaBase_payGrade";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterDD() {
        return "VacationFormulaBase_costCenter";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nationality">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nationality;

    public UDC getNationality() {
        return nationality;
    }

    public void setNationality(UDC nationality) {
        this.nationality = nationality;
    }

    public String getNationalityDD() {
        return "VacationFormulaBase_nationality";
    }
    //</editor-fold >
}
