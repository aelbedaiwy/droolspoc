/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepPositionKPA extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeCode">
    @Column
    private String employeeCode;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "RepPositionKPA_employeeCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    @Translatable(translationField = "fullNameTranslated")
    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullNameDD() {
        return "RepPositionKPA_fullName";
    }
    
    @Transient
    @Translation(originalField = "fullName")
    private String fullNameTranslated;

    public String getFullNameTranslated() {
        return fullNameTranslated;
    }

    public void setFullNameTranslated(String fullNameTranslated) {
        this.fullNameTranslated = fullNameTranslated;
    }
    
    public String getFullNameTranslatedDD() {
        return "RepPositionKPA_fullName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="position">
    @Column
    @Translatable(translationField = "positionDescriptionTranslated")
    private String positionDescription;

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }
    
    public String getPositionDescriptionDD() {
        return "RepPositionKPA_positionDescription";
    }
    
    @Transient
    @Translation(originalField = "positionDescription")
    private String positionDescriptionTranslated;

    public String getPositionDescriptionTranslated() {
        return positionDescriptionTranslated;
    }

    public void setPositionDescriptionTranslated(String positionDescriptionTranslated) {
        this.positionDescriptionTranslated = positionDescriptionTranslated;
    }
    
    public String getPositionDescriptionTranslatedDD() {
        return "RepPositionKPA_positionDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionDD() {
        return "RepPositionKPA_description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="type">
    @Translatable(translationField = "typeTranslated")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getTypeDD() {
        return "RepPositionKPA_type";
    }
    
    @Transient
    @Translation(originalField = "type")
    private String typeTranslated;

    public String getTypeTranslated() {
        return typeTranslated;
    }

    public void setTypeTranslated(String typeTranslated) {
        this.typeTranslated = typeTranslated;
    }
    
    public String getTypeTranslatedDD() {
        return "RepPositionKPA_type";
    }    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="weight">
    @Column(scale=25,precision=13)
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
    
    public String getWeightDD() {
        return "RepPositionKPA_weight";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="unit">
    @Column
    @Translatable(translationField = "unitTranslated")
    private String unit;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public String getUnitDD() {
        return "RepPositionKPA_unit";
    }
    
    @Transient
    @Translation(originalField = "unit")
    private String unitTranslated;

    public String getUnitTranslated() {
        return unitTranslated;
    }

    public void setUnitTranslated(String unitTranslated) {
        this.unitTranslated = unitTranslated;
    }
    
    public String getUnitTranslatedDD() {
        return "RepPositionKPA_unit";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="companyID">
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }
    
    public String getCompanyIDDD() {
        return "RepPositionKPA_companyID";
    }
    //</editor-fold>
}