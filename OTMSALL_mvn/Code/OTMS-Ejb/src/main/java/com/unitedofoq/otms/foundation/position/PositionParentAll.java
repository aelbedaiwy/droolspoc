/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ashienawy
 */
@Entity
@Table(name="positionparentbase")
public class PositionParentAll extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="fromDate">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date fromDate;

        public Date getFromDate() {
            return fromDate;
        }

        public void setFromDate(Date fromDate) {
            this.fromDate = fromDate;
        }

         public String getFromDateDD() {
            return "PositionParentAll_fromDate";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date toDate;

        public Date getToDate() {
            return toDate;
        }

        public void setToDate(Date toDate) {
            this.toDate = toDate;
        }

         public String getToDateDD() {
            return "PositionParentAll_toDate";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionParentPriority">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC positionParentPriority;
    public String getPositionParentPriorityDD() {
            return "PositionParentAll_positionParentPriority";
    }
    public UDC getPositionParentPriority() {
         return positionParentPriority;
    }
    public void setPositionParentPriority(UDC positionParentPriority) {
            this.positionParentPriority = positionParentPriority;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentPosition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position parentPosition ;
    public Position getParentPosition() {
        return parentPosition;
    }
    public void setParentPosition(Position parentPosition) {
        this.parentPosition = parentPosition;
    }
    public String getParentPositionDD() {
        return "PositionParentAll_parentPosition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position ;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    public String getPositionDD() {
        return "PositionParentAll_position";
    }


    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentPositionType">
 @JoinColumn(name="EPTYPE",nullable=false)
@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
 private UDC parentPositionType;
 public UDC getParentPositionType() {
        return parentPositionType;
    }
public String getParentPositionTypeDD() {
        return "PositionParentAll_parentPositionType";
    }
 public void setParentPositionType(UDC parentPositionType) {
        this.parentPositionType = parentPositionType;
 }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionParentType">
     @Column(name="PPTYPE")
    private String  positionParentType;

    public String getPositionParentType() {
        return positionParentType;
    }

    public void setPositionParentType(String positionParentType) {
        this.positionParentType = positionParentType;
    }

    public String getPositionParentTypeDD() {
        return "PositionParentAll_positionParentType";
    }
    // </editor-fold>

  }
