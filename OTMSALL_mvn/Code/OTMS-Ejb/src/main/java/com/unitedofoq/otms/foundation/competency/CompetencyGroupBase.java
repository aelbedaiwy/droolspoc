/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.competency;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */

@MappedSuperclass
public class CompetencyGroupBase extends BusinessObjectBaseEntity {

   // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "CompetencyGroupBase_company";
    }
    // </editor-fold>// <editor-fold defaultstate="collapsed" desc="responsabilitys">
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false)
    @Translatable(translationField="nameTranslated")
    private String name;
    public String getName() {
        return name;
    }
    public String getNameTranslatedDD() {
        return "CompetencyGroupBase_name";
    }
    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
  
    // <editor-fold defaultstate="collapsed" desc="competencies">
    @JoinColumn
    @OneToMany(mappedBy="competencyGroup")
    private List<Competency> competencies;
    public List<Competency> getCompetencies() {
        return competencies;
    }
    public String getCompetenciesDD() {
        return "CompetencyGroupBase_competencies";
    }
    public void setCompetencies(List<Competency> competencies) {
        this.competencies = competencies;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gradingSchema">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private ScaleType gradingSchema;

    public ScaleType getGradingSchema() {
        return gradingSchema;
    }

    public void setGradingSchema(ScaleType gradingSchema) {
        this.gradingSchema = gradingSchema;
    }

   
    public String getGradingSchemaDD() {
        return "CompetencyGroupBase_gradingSchema";
    } 
    
 // </editor-fold>

   // <editor-fold defaultstate="collapsed" desc="description">    
    @Translatable(translationField="descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "CompetencyGroupBase_description";
    }

    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
}
