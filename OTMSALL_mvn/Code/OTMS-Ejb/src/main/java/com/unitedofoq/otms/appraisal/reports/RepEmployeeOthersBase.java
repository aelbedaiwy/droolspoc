package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@MappedSuperclass
public class RepEmployeeOthersBase extends RepEmployeeBase {
    // <editor-fold defaultstate="collapsed" desc="dsDbid">

    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeOthersBase_dsDbid";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="year">
    @Column
    private String year;

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public String getYearDD() {
        return "RepEmployeeOthersBase_year";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "RepEmployeeOthersBase_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="finishDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finishDate;

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public String getFinishDateDD() {
        return "RepEmployeeOthersBase_finishDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="duration">
    @Column
    private String duration;

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public String getDurationDD() {
        return "RepEmployeeOthersBase_duration";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="purpose">
    @Column
    private String purpose;

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getPurposeDD() {
        return "RepEmployeeOthersBase_purpose";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    @Translatable(translationField = "templateTranslated")
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public String getTemplateDD() {
        return "RepEmployeeOthersBase_template";
    }
    @Transient
    @Translation(originalField = "template")
    private String templateTranslated;

    public String getTemplateTranslated() {
        return templateTranslated;
    }

    public void setTemplateTranslated(String templateTranslated) {
        this.templateTranslated = templateTranslated;
    }

    public String getTemplateTranslatedDD() {
        return "RepEmployeeOthersBase_template";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "RepEmployeeOthersBase_description";
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "RepEmployeeOthersBase_description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="description2">
    private String description2;

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getDescription2DD() {
        return "RepEmployeeOthersBase_description2";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="description3">
    private String description3;

    public String getDescription3() {
        return description3;
    }

    public void setDescription3(String description3) {
        this.description3 = description3;
    }

    public String getDescription3DD() {
        return "RepEmployeeOthersBase_description3";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="target">
    @Column
    private BigDecimal target;

    public BigDecimal getTarget() {
        return target;
    }

    public void setTarget(BigDecimal target) {
        this.target = target;
    }

    public String getTargetDD() {
        return "RepEmployeeOthersBase_target";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="weight">
    @Column
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getWeightDD() {
        return "RepEmployeeOthersBase_weight";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="type">
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "RepEmployeeOthersBase_type";
    }
    //</editor-fold>
}
