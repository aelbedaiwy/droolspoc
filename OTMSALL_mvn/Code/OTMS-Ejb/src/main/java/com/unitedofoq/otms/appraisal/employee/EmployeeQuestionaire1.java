package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.otms.appraisal.questionaire.Questionaire1;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import com.unitedofoq.otms.training.activity.Provider;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeQuestionaire1 extends EmployeeKPA {

    //<editor-fold defaultstate="collapsed" desc="description">

    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "EmployeeQuestionaire1_description";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EmployeeQuestionaire1_code";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeQuestionaire1_employee";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="questionaire1">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Questionaire1 questionaire1;

    public Questionaire1 getQuestionaire1() {
        return questionaire1;
    }

    public void setQuestionaire1(Questionaire1 questionaire1) {
        this.questionaire1 = questionaire1;
    }

    public String getQuestionaire1DD() {
        return "EmployeeQuestionaire1_questionaire1";
    }

    //</editor-fold>
    // for recruitment:
    // =================
    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "EmployeeQuestionaire1_applicant";
    }
    //</editor-fold>

    // for training:
    // =================
    //<editor-fold defaultstate="collapsed" desc="course">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation course;

    public ProviderCourseInformation getCourse() {
        return course;
    }

    public void setCourse(ProviderCourseInformation course) {
        this.course = course;
    }

    public String getCourseDD() {
        return "EmployeeQuestionaire1_course";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "EmployeeQuestionaire1_provider";
    }
    //</editor-fold>
}
