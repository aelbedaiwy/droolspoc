/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.eds.competency.EDSCompetencyLearningResource;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
public class EDSLearningResource extends BusinessObjectBaseEntity{
    // <editor-fold defaultstate="collapsed" desc="url">
    @Column(nullable=false)
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlDD() {
        return "EDSLearningResource_url";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD(){   return "EDSLearningResource_description";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="channel">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSChannel channel;
    public EDSChannel getChannel() {
        return channel;
    }
    public void setChannel(EDSChannel channel) {
        this.channel = channel;
    }
    public String getChannelDD(){   return "EDSLearningResource_channel";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="learningResource">
    @OneToMany(mappedBy="learningResource")
    private List<EDSCompetencyLearningResource> competencyLearningResource;
    public List<EDSCompetencyLearningResource> getCompetencyLearningResource() {
        return competencyLearningResource;
    }
    public void setCompetencyLearningResource(List<EDSCompetencyLearningResource> competencyLearningResource) {
        this.competencyLearningResource = competencyLearningResource;
    }
    public String getCompetencyLearningResourceDD(){   return "EDSLearningResource_competencyLearningResource";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="keyWords">
    private String keyWords;

    public String getKeyWordsDD() {
        return "EDSLearningResource_keyWords";
    }
    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }
//</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approve">
    private boolean approve;

    public String getApproveDD() {
        return "EDSLearningResource_approve";
    }

    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    //</editor-fold>
    
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
}
