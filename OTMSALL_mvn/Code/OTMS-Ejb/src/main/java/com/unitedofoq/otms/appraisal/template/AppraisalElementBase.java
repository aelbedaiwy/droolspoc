package com.unitedofoq.otms.appraisal.template;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class AppraisalElementBase extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "AppraisalElementBase_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    //Objective,Competencies,UserDefine
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "AppraisalElementBase_type";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comments">
    @Lob
    @Translatable(translationField = "commentsTranslated")
    @Column
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Transient
    @Translation(originalField = "comments")
    private String commentsTranslated;

    public String getCommentsTranslated() {
        return commentsTranslated;
    }

    public void setCommentsTranslated(String commentsTranslated) {
        this.commentsTranslated = commentsTranslated;
    }

    public String getCommentsTranslatedDD() {
        return "AppraisalElementBase_comments";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="procedures">
    @Lob
    @Translatable(translationField = "proceduresTranslated")
    @Column
    private String procedures;

    public String getProcedures() {
        return procedures;
    }

    public void setProcedures(String procedures) {
        this.procedures = procedures;
    }

    @Transient
    @Translation(originalField = "procedures")
    private String proceduresTranslated;

    public String getProceduresTranslated() {
        return proceduresTranslated;
    }

    public void setProceduresTranslated(String proceduresTranslated) {
        this.proceduresTranslated = proceduresTranslated;
    }

    public String getProceduresTranslatedDD() {
        return "AppraisalElementBase_procedures";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculationType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC calculationType;

    public UDC getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(UDC calculationType) {
        this.calculationType = calculationType;
    }

    public String getCalculationTypeDD() {
        return "AppraisalElementBase_calculationType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scaleType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleType scaleType;

    public ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    public String getScaleTypeDD() {
        return "AppraisalElementBase_scaleType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="templateScale">
    //Quality Or Quantity Or Comment
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC templateScale;

    public UDC getTemplateScale() {
        return templateScale;
    }

    public void setTemplateScale(UDC templateScale) {
        this.templateScale = templateScale;
    }

    public String getTemplateScaleDD() {
        return "AppraisalElementBase_templateScale";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "AppraisalElementBase_code";
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="purpose">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC purpose;

    public UDC getPurpose() {
        return purpose;
    }

    public void setPurpose(UDC purpose) {
        this.purpose = purpose;
    }

    public String getPurposeDD() {
        return "AppraisalElementBase_purpose";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="detailDescription">
    @Column
    private String detailDescription;

    public String getDetailDescription() {
        return detailDescription;
    }

    public void setDetailDescription(String detailDescription) {
        this.detailDescription = detailDescription;
    }

    public String getDetailDescriptionDD() {
        return "AppraisalElementBase_detailDescription";
    }
    // </editor-fold >
    //    // <editor-fold defaultstate="collapsed" desc="appraisalElement">
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private AppraisalElement appraisalElement;
//
//    public AppraisalElement getAppraisalElement() {
//        return appraisalElement;
//    }
//
//    public void setAppraisalElement(AppraisalElement appraisalElement) {
//        this.appraisalElement = appraisalElement;
//    }
//
//    public String getAppraisalElementDD() {
//        return "AppraisalElementBase_appraisalElement";
//    }
//    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="appraisalTemplate">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplate appraisalTemplate;

    public AppraisalTemplate getAppraisalTemplate() {
        return appraisalTemplate;
    }

    public void setAppraisalTemplate(AppraisalTemplate appraisalTemplate) {
        this.appraisalTemplate = appraisalTemplate;
    }

    public String getAppraisalTemplateDD() {
        return "AppraisalElementBase_appraisalTemplate";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="weight">
    @Column
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getWeightDD() {
        return "AppraisalElementBase_weight";
    }
    @Transient
    private BigDecimal weightMask;

    public BigDecimal getWeightMask() {
        weightMask = weight;
        return weightMask;
    }

    public void setWeightMask(BigDecimal weightMask) {
        updateDecimalValue("weight", weightMask);
    }

    public String getWeightMaskDD() {
        return "AppraisalElementBase_weightMask";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="outOfPoints">
    private BigDecimal outOfPoints;

    public BigDecimal getOutOfPoints() {
        return outOfPoints;
    }

    public void setOutOfPoints(BigDecimal outOfPoints) {
        this.outOfPoints = outOfPoints;
    }

    public String getOutOfPointsDD() {
        return "AppraisalElementBase_outOfPoints";
    }

    @Transient
    private BigDecimal outOfPointsMask;

    public BigDecimal getOutOfPointsMask() {
        outOfPointsMask = outOfPoints;
        return outOfPointsMask;
    }

    public void setOutOfPointsMask(BigDecimal outOfPointsMask) {
        updateDecimalValue("outOfPoints", outOfPointsMask);
    }

    public String getOutOfPointsMaskDD() {
        return "AppraisalElementBase_outOfPointsMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculationMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC calculationMethod;

    public UDC getCalculationMethod() {
        return calculationMethod;
    }

    public void setCalculationMethod(UDC calculationMethod) {
        this.calculationMethod = calculationMethod;
    }

    public String getCalculationMethodDD() {
        return "AppraisalElementBase_calculationMethod";
    }
    //</editor-fold>
}
