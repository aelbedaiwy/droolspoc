/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.jobGroup.JobGroup;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author Abayomy
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeIqama extends BusinessObjectBaseEntity  {
     // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeIqama_employee";
    }
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="number">
    @Column(nullable=false, name="iqamaNum")
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public String getNumberDD() {
        return "EmployeeIqama_number";
    }
   // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="issueDate">

        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date issueDate;

        public Date getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(Date issueDate) {
            this.issueDate = issueDate;
        }
        public String getIssueDateDD() {
            return "EmployeeIqama_issueDate";
        }
      // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="renewalDate">

        @Column(nullable=false)
        @Temporal(TemporalType.TIMESTAMP)
        private Date renewalDate;

        public Date getRenewalDate() {
            return renewalDate;
        }

        public void setRenewalDate(Date renewalDate) {
            this.renewalDate = renewalDate;
        }
         public String getRenewalDateDD() {
            return "EmployeeIqama_renewalDate";
        }

      // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="issueLocation">

        @JoinColumn
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC issueLocation;

        public UDC getIssueLocation() {
            return issueLocation;
        }

        public void setIssueLocation(UDC issueLocation) {
            this.issueLocation = issueLocation;
        }

         public String getIssueLocationDD() {
            return "EmployeeIqama_issueLocation";
        }
          // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="entranceDate">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date entranceDate;

        public Date getEntranceDate() {
            return entranceDate;
        }

        public void setEntranceDate(Date entranceDate) {
            this.entranceDate = entranceDate;
        }

        public String getEntranceDateDD() {
            return "EmployeeIqama_entranceDate";
        }

      // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="entrancePort">

        @Column
        private String entrancePort;

        public String getEntrancePort() {
            return entrancePort;
        }

        public void setEntrancePort(String entrancePort) {
            this.entrancePort = entrancePort;
        }

        public String getEntrancePortDD() {
            return "EmployeeIqama_entrancePort";
        }

      // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="changeData">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date changeData;

        public Date getChangeData() {
            return changeData;
        }

        public void setChangeData(Date changeData) {
            this.changeData = changeData;
        }
        public String getChangeDataDD() {
            return "EmployeeIqama_changeData";
        }
      // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="followUp">

        @JoinColumn
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private Employee followUp;

        public Employee getFollowUp() {
            return followUp;
        }

        public void setFollowUp(Employee followUp) {
            this.followUp = followUp;
        }
        public String getFollowUpDD() {
            return "EmployeeIqama_followUp";
        }
          // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="jobGroup">
        @JoinColumn(nullable=false)
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private JobGroup jobGroup;

        public JobGroup getJobGroup() {
            return jobGroup;
        }

        public void setJobGroup(JobGroup jobGroup) {
            this.jobGroup = jobGroup;
        }
           public String getJobGroupDD() {
            return "EmployeeIqama_jobGroup";
        }

          // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="remarks">
        @Column
        private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
         public String getRemarksDD() {
        return "EmployeeIqama_remarks";
    }

          // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="status">
        @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }
        public String getStatusDD() {
        return "EmployeeIqama_status";
    }

          // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="labourOfficeNumber">
        @Column
        private String labourOfficeNumber;

    public String getLabourOfficeNumber() {
        return labourOfficeNumber;
    }

    public void setLabourOfficeNumber(String labourOfficeNumber) {
        this.labourOfficeNumber = labourOfficeNumber;
    }
        public String getLabourOfficeNumberDD() {
        return "EmployeeIqama_labourOfficeNumber";
    }
          // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="visible">
        @Column
        private boolean visible;

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
        public String isVisibleDD() {
        return "EmployeeIqama_visible";
    }

          // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="job">

        @JoinColumn(nullable=false)
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private Job job;

        public Job getJob() {
            return job;
        }

        public void setJob(Job job) {
            this.job = job;
        }
         public String getJobDD() {
            return "EmployeeIqama_job";
        }
          // </editor-fold>
      // <editor-fold defaultstate="collapsed" desc="expiryDate">
        @Temporal(javax.persistence.TemporalType.DATE)
         private Date expiryDate;

        public Date getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(Date expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getExpiryDateDD() {
            return "EmployeeIqama_expiryDate";
        }
     // </editor-fold>
      // <editor-fold defaultstate="collapsed" desc="issueDateHijri">
        @Temporal(javax.persistence.TemporalType.DATE)
        private Date issueDateHijri;

        public Date getIssueDateHijri() {
            return issueDateHijri;
        }

        public void setIssueDateHijri(Date issueDateHijri) {
            this.issueDateHijri = issueDateHijri;
        }
        public String getIssueDateHijriDD() {
            return "EmployeeIqama_issueDateHijri";
        }

     // </editor-fold>
      // <editor-fold defaultstate="collapsed" desc="expiryDateHijri">
        @Temporal(javax.persistence.TemporalType.DATE)
        private Date expiryDateHijri;

        public Date getExpiryDateHijri() {
            return expiryDateHijri;
        }

        public void setExpiryDateHijri(Date expiryDateHijri) {
            this.expiryDateHijri = expiryDateHijri;
        }
        public String getExpiryDateHijriDD() {
            return "EmployeeIqama_expiryDateHijri";
        }

     // </editor-fold>
      // <editor-fold defaultstate="collapsed" desc="renewalDateHijri">
        @Temporal(javax.persistence.TemporalType.DATE)
        private Date renewalDateHijri;

        public Date getRenewalDateHijri() {
            return renewalDateHijri;
        }

        public void setRenewalDateHijri(Date renewalDateHijri) {
            this.renewalDateHijri = renewalDateHijri;
        }

     public String getRenewalDateHijriDD() {
            return "EmployeeIqama_renewalDateHijri";
        }
     // </editor-fold>

 @Column(nullable=false)
  private String passport;

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }
  public String getPassportDD() {
        return "EmployeeIqama_passport";
    }

  String passportAddress;

    public String getPassportAddress() {
        return passportAddress;
    }

    public void setPassportAddress(String passportAddress) {
        this.passportAddress = passportAddress;
    }

public String getPassportAddressDD() {
        return "EmployeeIqama_passportAddress";
    }


}
