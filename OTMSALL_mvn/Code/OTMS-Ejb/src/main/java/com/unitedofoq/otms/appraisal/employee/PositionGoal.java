package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.position.Position;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"position"})
public class PositionGoal extends GoalBase {

    //<editor-fold defaultstate="collapsed" desc="position">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "PositionGoal_position";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="departmentGoal">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private DepartmentGoal departmentGoal;

    public DepartmentGoal getDepartmentGoal() {
        return departmentGoal;
    }

    public void setDepartmentGoal(DepartmentGoal departmentGoal) {
        this.departmentGoal = departmentGoal;
    }

    public String getDepartmentGoalDD() {
        return "PositionGoal_departmentGoal";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Goal">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public String getGoalDD() {
        return "PositionGoal_goal";
    }
    //</editor-fold>
}
