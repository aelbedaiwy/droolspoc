package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.timemanagement.TMOpenedCalcPeriod;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ETYPE")
@DiscriminatorValue(value = "MASTER")
public class EmployeeDailyAttendanceImport extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EmployeeDailyAttendanceImport_code";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="date">
    @Column(name = "currentDate")
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateDD() {
        return "EmployeeDailyAttendanceImport_date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="inTime">
    private String inTime;

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getInTimeDD() {
        return "EmployeeDailyAttendanceImport_inTime";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="outTime">
    private String outTime;

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getOutTimeDD() {
        return "EmployeeDailyAttendanceImport_outTime";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="machineCode">
    private String machineCode;

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }

    public String getMachineCodeDD() {
        return "EmployeeDailyAttendanceImport_machineCode";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "EmployeeDailyAttendanceImport_done";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FromTimeMachine">
    @Column(length = 1)
    //Y  - N
    private String fromTimeMachine;

    public String getFromTimeMachine() {
        return fromTimeMachine;
    }

    public void setFromTimeMachine(String fromTimeMachine) {
        this.fromTimeMachine = fromTimeMachine;
    }

    public String getFromTimeMachineDD() {
        return "EmployeeDailyAttendanceImport_fromTimeMachine";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dailyDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dailyDate;

    public Date getDailyDate() {
        return dailyDate;
    }

    public void setDailyDate(Date dailyDate) {
        this.dailyDate = dailyDate;
    }

    public String getDailyDateDD() {
        return "EmployeeDailyAttendanceImport_dailyDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="outDate">
    @Temporal(TemporalType.DATE)
    private Date outDate;

    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public String getOutDateDD() {
        return "EmployeeDailyAttendanceImport_outDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeDailyAttendanceImport_employee";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calcPeriod">
    @ManyToOne(fetch = FetchType.LAZY)
    private TMOpenedCalcPeriod calcPeriod;

    public TMOpenedCalcPeriod getCalcPeriod() {
        return calcPeriod;
    }

    public void setCalcPeriod(TMOpenedCalcPeriod calcPeriod) {
        this.calcPeriod = calcPeriod;
    }

    public String getCalcPeriodDD() {
        return "EmployeeDailyAttendanceImport_calcPeriod";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="externalOrNot">
    @Column(length = 1)
    private String externalOrNot;

    public String getExternalOrNot() {
        return externalOrNot;
    }

    public String getExternalOrNotDD() {
        return "EmployeeDailyAttendanceImport_externalOrNot";
    }

    public void setExternalOrNot(String externalOrNot) {
        this.externalOrNot = externalOrNot;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapse" desc="manualEntry">
    private String manualEntry;

    public String getManualEntry() {
        return manualEntry;
    }

    public void setManualEntry(String manualEntry) {
        this.manualEntry = manualEntry;
    }

    public String getManualEntryDD() {
        return "EmployeeDailyAttendanceImport_manualEntry";
    }
    //</editor-fold>
}
