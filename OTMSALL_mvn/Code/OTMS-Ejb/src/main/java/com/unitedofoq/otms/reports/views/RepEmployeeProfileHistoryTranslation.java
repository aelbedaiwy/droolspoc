package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name= "repempprohistranslation")
public class RepEmployeeProfileHistoryTranslation extends RepCompanyLevelBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="oldUnit">
    @Column
    private String oldUnit;

    public void setOldUnit(String oldUnit) {
        this.oldUnit = oldUnit;
    }

    public String getOldUnit() {
        return oldUnit;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oldPosition">
    @Column
    private String oldPosition;

    public void setOldPosition(String oldPosition) {
        this.oldPosition = oldPosition;
    }

    public String getOldPosition() {
        return oldPosition;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oldPayGrade">
    @Column
    private String oldPayGrade;

    public void setOldPayGrade(String oldPayGrade) {
        this.oldPayGrade = oldPayGrade;
    }

    public String getOldPayGrade() {
        return oldPayGrade;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oldLocation">
    @Column
    private String oldLocation;

    public void setOldLocation(String oldLocation) {
        this.oldLocation = oldLocation;
    }

    public String getOldLocation() {
        return oldLocation;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oldCostCenter">
    @Column
    private String oldCostCenter;

    public void setOldCostCenter(String oldCostCenter) {
        this.oldCostCenter = oldCostCenter;
    }

    public String getOldCostCenter() {
        return oldCostCenter;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    private String firstName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    private String middleName;

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    private String lastName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    private String fullName;

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    private String locationDescription;

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescription() {
        return locationDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    private String hiringType;

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringType() {
        return hiringType;
    }
    // </editor-fold>

}