package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepUnitLevel extends RepCompanyLevelBase {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private String dsDBID;

    public void setDsDBID(String dsDBID) {
        this.dsDBID = dsDBID;
    }

    public String getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepUnitLevel_dsDBID";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @Column
    @Translatable(translationField = "unitTranslated")
    private String unit;

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public String getUnitDD() {
        return "RepUnitLevel_unit";
    }
    @Transient
    @Translation(originalField = "unit")
    private String unitTranslated;

    public String getUnitTranslated() {
        return unitTranslated;
    }

    public void setUnitTranslated(String unitTranslated) {
        this.unitTranslated = unitTranslated;
    }

    public String getUnitTranslatedDD() {
        return "RepUnitLevel_unit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentUnit">
    @Column
    @Translatable(translationField = "parentUnitTranslated")
    private String parentUnit;

    public void setParentUnit(String parentUnit) {
        this.parentUnit = parentUnit;
    }

    public String getParentUnit() {
        return parentUnit;
    }

    public String getParentUnitDD() {
        return "RepUnitLevel_parentUnit";
    }
    @Transient
    @Translation(originalField = "parentUnit")
    private String parentUnitTranslated;

    public String getParentUnitTranslated() {
        return parentUnitTranslated;
    }

    public void setParentUnitTranslated(String parentUnitTranslated) {
        this.parentUnitTranslated = parentUnitTranslated;
    }

    public String getParentUnitTranslatedDD() {
        return "RepUnitLevel_parentUnit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitLevelIndex">
    @Column
    @Translatable(translationField = "unitLevelIndexTranslated")
    private String unitLevelIndex;

    public void setUnitLevelIndex(String unitLevelIndex) {
        this.unitLevelIndex = unitLevelIndex;
    }

    public String getUnitLevelIndex() {
        return unitLevelIndex;
    }

    public String getUnitLevelIndexDD() {
        return "RepUnitLevel_unitLevelIndex";
    }
    @Transient
    @Translation(originalField = "unitLevelIndex")
    private String unitLevelIndexTranslated;

    public String getUnitLevelIndexTranslated() {
        return unitLevelIndexTranslated;
    }

    public void setUnitLevelIndexTranslated(String unitLevelIndexTranslated) {
        this.unitLevelIndexTranslated = unitLevelIndexTranslated;
    }

    public String getUnitLevelIndexTranslatedDD() {
        return "RepUnitLevel_unitLevelIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @Column
    @Translatable(translationField = "costCenterTranslated")
    private String costCenter;

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public String getCostCenterDD() {
        return "RepUnitLevel_costCenter";
    }
    @Transient
    @Translation(originalField = "costCenter")
    private String costCenterTranslated;

    public String getCostCenterTranslated() {
        return costCenterTranslated;
    }

    public void setCostCenterTranslated(String costCenterTranslated) {
        this.costCenterTranslated = costCenterTranslated;
    }

    public String getCostCenterTranslatedDD() {
        return "RepUnitLevel_costCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="position">
    @Column
    @Translatable(translationField = "positionTranslated")
    private String position;

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "RepUnitLevel_position";
    }
    @Transient
    @Translation(originalField = "position")
    private String positionTranslated;

    public String getPositionTranslated() {
        return positionTranslated;
    }

    public void setPositionTranslated(String positionTranslated) {
        this.positionTranslated = positionTranslated;
    }

    public String getPositionTranslatedDD() {
        return "RepUnitLevel_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @Column
    private String currency;

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencyDD() {
        return "RepUnitLevel_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="managerName">
    @Column
    @Translatable(translationField = "managerNameTranslated")
    private String managerName;

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerName() {
        return managerName;
    }

    public String getManagerNameDD() {
        return "RepUnitLevel_managerName";
    }
    @Transient
    @Translation(originalField = "managerName")
    private String managerNameTranslated;

    public String getManagerNameTranslated() {
        return managerNameTranslated;
    }

    public void setManagerNameTranslated(String managerNameTranslated) {
        this.managerNameTranslated = managerNameTranslated;
    }

    public String getManagerNameTranslatedDD() {
        return "RepUnitLevel_managerName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="shrotName">
    @Column
    private String shortName;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getShortNameDD() {
        return "RepUnitLevel_shortName";
    }
    // </editor-fold>
}
