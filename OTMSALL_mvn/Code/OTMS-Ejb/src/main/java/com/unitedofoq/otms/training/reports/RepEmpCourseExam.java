
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
public class RepEmpCourseExam extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmpCourseExam_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }

    public String getCourseClassDD() {
        return "RepEmpCourseExam_courseClass";
    }
    
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;

    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }

    public String getCourseClassTranslatedDD() {
        return "RepEmpCourseExam_courseClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="course">
    @Column
    @Translatable(translationField = "courseTranslated")
    private String course;

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourse() {
        return course;
    }

    public String getCourseDD() {
        return "RepEmpCourseExam_course";
    }
    
    @Transient
    @Translation(originalField = "course")
    private String courseTranslated;

    public String getCourseTranslated() {
        return courseTranslated;
    }

    public void setCourseTranslated(String courseTranslated) {
        this.courseTranslated = courseTranslated;
    }

    public String getCourseTranslatedDD() {
        return "RepEmpCourseExam_courses";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="grade">
    @Column
    @Translatable(translationField = "gradeTranslated")
    private String grade;

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public String getGradeDD() {
        return "RepEmpCourseExam_grade";
    }
    
    @Transient
    @Translation(originalField = "grade")
    private String gradeTranslated;

    public String getGradeTranslated() {
        return gradeTranslated;
    }

    public void setGradeTranslated(String gradeTranslated) {
        this.gradeTranslated = gradeTranslated;
    }

    public String getGradeTranslatedDD() {
        return "RepEmpCourseExam_grade";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="finalScore">
    @Column
    private BigDecimal finalScore;

    public void setFinalScore(BigDecimal finalScore) {
        this.finalScore = finalScore;
    }

    public BigDecimal getFinalScore() {
        return finalScore;
    }

    public String getFinalScoreDD() {
        return "RepEmpCourseExam_finalScore";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="empComments">
    @Column
    @Translatable(translationField = "empCommentsTranslated")
    private String empComments;

    public void setEmpComments(String empComments) {
        this.empComments = empComments;
    }

    public String getEmpComments() {
        return empComments;
    }

    public String getEmpCommentsDD() {
        return "RepEmpCourseExam_empComments";
    }
    @Transient
    @Translation(originalField = "empComments")
    private String empCommentsTranslated;

    public String getEmpCommentsTranslated() {
        return empCommentsTranslated;
    }

    public void setEmpCommentsTranslated(String empCommentsTranslated) {
        this.empCommentsTranslated = empCommentsTranslated;
    }

    public String getEmpCommentsTranslatedDD() {
        return "RepEmpCourseExam_empComments";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="commentsOnEmp">
    @Column
    @Translatable(translationField = "commentsOnEmpTranslated")
    private String commentsOnEmp;

    public void setCommentsOnEmp(String commentsOnEmp) {
        this.commentsOnEmp = commentsOnEmp;
    }

    public String getCommentsOnEmp() {
        return commentsOnEmp;
    }

    public String getCommentsOnEmpDD() {
        return "RepEmpCourseExam_commentsOnEmp";
    }
    @Transient
    @Translation(originalField = "commentsOnEmp")
    private String commentsOnEmpTranslated;

    public String getCommentsOnEmpTranslated() {
        return commentsOnEmpTranslated;
    }

    public void setCommentsOnEmpTranslated(String commentsOnEmpTranslated) {
        this.commentsOnEmpTranslated = commentsOnEmpTranslated;
    }

    public String getCommentsOnEmpTranslatedDD() {
        return "RepEmpCourseExam_commentsOnEmp";
    }
    // </editor-fold>
}