package com.unitedofoq.otms.recruitment.jobrequisition;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.MeasurableField;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.recruitment.jobrequisitionapplicant.JobRequisitionApplicant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ChildEntity(fields={"skill", "task", "ability", "announcment", "interest",
                "language", "workContext", "workActivity", "workStyle", "workValue", "knowledge"})
public class JobRequisition extends BaseEntity {

    private Integer approvedRequiredHeadCount;

    public Integer getApprovedRequiredHeadCount() {
        return approvedRequiredHeadCount;
    }

    public void setApprovedRequiredHeadCount(Integer approvedRequiredHeadCount) {
        this.approvedRequiredHeadCount = approvedRequiredHeadCount;
    }

    public Integer getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(Integer daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public Integer getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(Integer expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public Integer getExpectedSalaryFrom() {
        return expectedSalaryFrom;
    }

    public void setExpectedSalaryFrom(Integer expectedSalaryFrom) {
        this.expectedSalaryFrom = expectedSalaryFrom;
    }

    public Integer getHiredHeadCount() {
        return hiredHeadCount;
    }

    public void setHiredHeadCount(Integer hiredHeadCount) {
        this.hiredHeadCount = hiredHeadCount;
    }

    public Integer getHoursPerDay() {
        return hoursPerDay;
    }

    public void setHoursPerDay(Integer hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }

    public Integer getMscto() {
        return mscto;
    }

    public void setMscto(Integer mscto) {
        this.mscto = mscto;
    }

    public Integer getOriginalReqHdCnt() {
        return originalReqHdCnt;
    }

    public void setOriginalReqHdCnt(Integer originalReqHdCnt) {
        this.originalReqHdCnt = originalReqHdCnt;
    }

    public Integer getRequiredHeadCount() {
        return requiredHeadCount;
    }

    public void setRequiredHeadCount(Integer requiredHeadCount) {
        this.requiredHeadCount = requiredHeadCount;
    }

    public Integer getVacantPost() {
        return vacantPost;
    }

    public void setVacantPost(Integer vacantPost) {
        this.vacantPost = vacantPost;
    }
    public String getApprovedRequiredHeadCountDD() {
        return "JobRequisition_approvedRequiredHeadCount";
    }
    // @Column(nullable = false)
    //duplicated field declaration
    //int approvedReqHeadCnt;
    //@Column(nullable = false)
    private String description;

    public String getDescriptionDD() {
        return "JobRequisition_description";
    }
    // @Column(nullable = false)
    private Integer originalReqHdCnt;

    public String getOriginalReqHdCntDD() {
        return "JobRequisition_originalRequiredHeadCount";
    }
    // @Column(nullable = false)
    private Integer requiredHeadCount;

    public String getRequiredHeadCountDD() {
        return "JobRequisition_requiredHeadCount";
    }
    // @Column(nullable = false)
    private Integer hiredHeadCount = new Integer(0);

    public String getHiredHeadCountDD() {
        return "JobRequisition_hiredHeadCount";
    }
    @Column(nullable = false)
    private boolean closed;// = new Boolean(false);

    public String getClosedDD() {
        return "JobRequisition_closed";
    }
    @Column(nullable = false)
    private boolean cancelled;// = new Boolean(false);

    public String getCancelledDD() {
        return "JobRequisition_cancelled";
    }
    @MeasurableField(name = "currency.value")
    private Integer expectedSalary;



    public String getExpectedSalaryDD() {
        return "JobRequisition_expectedSalary";
    }
    private Integer expectedSalaryTo;

    public Integer getExpectedSalaryTo() {
        return expectedSalaryTo;
    }

    public void setExpectedSalaryTo(Integer expectedSalaryTo) {
        this.expectedSalaryTo = expectedSalaryTo;
    }

    public String getExpectedSalaryToDD() {
        return "JobRequisition_expectedSalaryTo";
    }
    private Integer expectedSalaryFrom;

    public String getExpectedSalaryFromDD() {
        return "JobRequisition_expectedSalaryFrom";
    }
    // @Column(nullable = false)
    private String location;

    public String getLocationDD() {
        return "JobRequisition_location";
    }
    private String positionName;

    public String getPositionNameDD() {
        return "JobRequisition_positionName";
    }
    //@Column(nullable = false)
    private Integer vacantPost;

    public String getVacantPostDD() {
        return "JobRequisition_vacantPost";
    }
    //@Column(nullable = false)
    private String requisitionAtStep;

    public String getRequisitionAtStepDD() {
        return "JobRequisition_requisitionAtStep";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Employee recruiter;

   
    //@Column(nullable = false)
    private Integer hoursPerDay;

    public String getHoursPerDayDD() {
        return "JobRequisition_hoursPerDay";
    }
    //@Column(nullable = false)
    private Integer daysPerWeek;

    public String getDaysPerWeekDD() {
        return "JobRequisition_daysPerWeek";
    }
    private Double contractedPdInYrs;

    public String getContractedPdInYrsDD() {
        return "JobRequisition_contractedPeriodInYears";
    }
    @Column(nullable = false)
    private boolean announced;// = new Boolean(false);

    public String getAnnouncedDD() {
        return "JobRequisition_announced";
    }
    private Integer mscto;
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqLanguage> language = new ArrayList<JobReqLanguage>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqSkill> skill = new ArrayList<JobReqSkill>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqTask> task = new ArrayList<JobReqTask>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqAnnouncment> announcment = new ArrayList<JobReqAnnouncment>();
    @OneToMany
    private List<ReqHeadCountHistory> reqHeadCountHistory = new ArrayList<ReqHeadCountHistory>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqInterest> interest = new ArrayList<JobReqInterest>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqAbility> ability = new ArrayList<JobReqAbility>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqKnowledge> knowledge = new ArrayList<JobReqKnowledge>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqWorkActivity> workActivity = new ArrayList<JobReqWorkActivity>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqWorkContext> workContext = new ArrayList<JobReqWorkContext>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqWorkStyle> workStyle = new ArrayList<JobReqWorkStyle>();
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobReqWorkValue> workValue = new ArrayList<JobReqWorkValue>();
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private JobReqCusOccuElmnts customOccuElements;

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private JobReqRequestInfo requestInfo;

    @Temporal(TemporalType.DATE)
    protected Date approvalDate;
    @Temporal(TemporalType.DATE)
    //@Column(nullable=false)
    protected Date requestDate;
    public String getRequestDateDD() {
        return "JobRequisition_requestDate";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected Employee approvedBy;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected Employee toBeApprovedBy;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected Employee requestedBy;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC currency;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC employmentType;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC hiringPriority;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee reportTo;
    private double contractedPeriod;
    private String posting;
    public String getPostingDD() {
        return "JobRequisition_posting";
    }
    @OneToMany(mappedBy = "jobRequisition")
    private List<JobRequisitionApplicant> jobRequisitionApplicants;

    public List<JobRequisitionApplicant> getJobRequisitionApplicants() {
        return jobRequisitionApplicants;
    }

    public void setJobRequisitionApplicants(List<JobRequisitionApplicant> jobRequisitionApplicants) {
        this.jobRequisitionApplicants =jobRequisitionApplicants ;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Employee getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(Employee requestedBy) {
        this.requestedBy = requestedBy;
    }

    public Employee getToBeApprovedBy() {
        return toBeApprovedBy;
    }

    public void setToBeApprovedBy(Employee toBeApprovedBy) {
        this.toBeApprovedBy = toBeApprovedBy;
    }

    public String getMsctoDD() {
        return "JobRequisition_mscto";
    }

    public String getApprovedReqHeadCntDD() {
        return "JobRequisition_approvedRequiredHeadCount";
    }


    public double getContractedPeriod() {
        return contractedPeriod;
    }

    public void setContractedPeriod(double contractedPeriod) {
        this.contractedPeriod = contractedPeriod;
    }

    public String getPosting() {
        return posting;
    }

    public void setPosting(String posting) {
        this.posting = posting;
    }

    public String getRequisitionAtStep() {
        return requisitionAtStep;
    }

    public void setRequisitionAtStep(String RequisitionAtStep) {
        this.requisitionAtStep = RequisitionAtStep;
    }

   

    public UDC getCurrency() {
        return currency;
    }
    public String getCurrencyDD() {
        return "JobRequisition_Currency";
    }

    public void setCurrency(UDC currency) {
        this.currency = currency;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UDC getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(UDC employmentType) {
        this.employmentType = employmentType;
    }


    public UDC getHiringPriority() {
        return hiringPriority;
    }

    public void setHiringPriority(UDC hiringPriority) {
        this.hiringPriority = hiringPriority;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Employee getReportTo() {
        return reportTo;
    }

    public void setReportTo(Employee reportTo) {
        this.reportTo = reportTo;
    }

//    public JobReqRequestInfo getRequestInfo() {
//        return requestInfo;
//    }
//
//    public void setRequestInfo(JobReqRequestInfo requestInfo) {
//        this.requestInfo = requestInfo;
//    }

    public List<ReqHeadCountHistory> getReqHeadCountHistory() {
        return reqHeadCountHistory;
    }

    public void setReqHeadCountHistory(List<ReqHeadCountHistory> reqHeadCountHistory) {
        this.reqHeadCountHistory = reqHeadCountHistory;
    }


    public List<JobReqAbility> getAbility() {
        return ability;
    }

    public void setAbility(List<JobReqAbility> ability) {
        this.ability = ability;
    }

    public List<JobReqAnnouncment> getAnnouncment() {
        return announcment;
    }

    public void setAnnouncment(List<JobReqAnnouncment> announcment) {
        this.announcment = announcment;
    }

    public JobReqCusOccuElmnts getCustomOccuElements() {
        return customOccuElements;
    }

    public void setCustomOccuElements(JobReqCusOccuElmnts customOccuElements) {
        this.customOccuElements = customOccuElements;
    }

    public List<JobReqInterest> getInterest() {
        return interest;
    }

    public void setInterest(List<JobReqInterest> interest) {
        this.interest = interest;
    }

    public List<JobReqKnowledge> getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(List<JobReqKnowledge> knowledge) {
        this.knowledge = knowledge;
    }

    public List<JobReqLanguage> getLanguage() {
        return language;
    }

    public void setLanguage(List<JobReqLanguage> language) {
        this.language = language;
    }

    public Employee getRecruiter() {
        return recruiter;
    }

    public void setRecruiter(Employee recruiter) {
        this.recruiter = recruiter;
    }

    public List<JobReqSkill> getSkill() {
        return skill;
    }

    public void setSkill(List<JobReqSkill> skill) {
        this.skill = skill;
    }

    public List<JobReqTask> getTask() {
        return task;
    }

    public void setTask(List<JobReqTask> task) {
        this.task = task;
    }

    public List<JobReqWorkActivity> getWorkActivity() {
        return workActivity;
    }

    public void setWorkActivity(List<JobReqWorkActivity> workActivity) {
        this.workActivity = workActivity;
    }

    public List<JobReqWorkContext> getWorkContext() {
        return workContext;
    }

    public void setWorkContext(List<JobReqWorkContext> workContext) {
        this.workContext = workContext;
    }

    public List<JobReqWorkStyle> getWorkStyle() {
        return workStyle;
    }

    public void setWorkStyle(List<JobReqWorkStyle> workStyle) {
        this.workStyle = workStyle;
    }

    public Double getContractedPdInYrs() {
        return contractedPdInYrs;
    }

    public void setContractedPdInYrs(Double contractedPdInYrs) {
        this.contractedPdInYrs = contractedPdInYrs;
    }

//    public int getApprovedReqHeadCnt() {
//        return approvedReqHeadCnt;
//    }

//    public void setApprovedReqHeadCnt(int approvedReqHeadCnt) {
//        this.approvedReqHeadCnt = approvedReqHeadCnt;
//    }



    public List<JobReqWorkValue> getWorkValue() {
        return workValue;
    }

    public void setWorkValue(List<JobReqWorkValue> workValue) {
        this.workValue = workValue;
    }

    /**
     * @return the requestInfo
     */
    public JobReqRequestInfo getRequestInfo() {
        return requestInfo;
    }

    /**
     * @param requestInfo the requestInfo to set
     */
    public void setRequestInfo(JobReqRequestInfo requestInfo) {
        this.requestInfo = requestInfo;
    }

    public boolean isAnnounced() {
        return announced;
    }

    public void setAnnounced(boolean announced) {
        this.announced = announced;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

   
    
}
