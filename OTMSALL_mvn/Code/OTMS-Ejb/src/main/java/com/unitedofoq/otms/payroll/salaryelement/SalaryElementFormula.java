
package com.unitedofoq.otms.payroll.salaryelement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"salaryElement"})
public class SalaryElementFormula extends BaseEntity  {

    //<editor-fold defaultstate="collapsed" desc="salaryElement">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    
    public String getSalaryElementDD() {
        return "SalaryElementFormula_salaryElement";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public void setType(UDC type) {
        this.type = type;
    }

    public UDC getType() {
        return type;
    }

    public String getTypeDD() {
        return "SalaryElementFormula_type";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="value">
    @Column(precision=25, scale=13)
    private BigDecimal value;

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getValueDD() {
        return "SalaryElementFormula_value";
    }
    @Transient
    private BigDecimal valueMask = java.math.BigDecimal.ZERO;
    public BigDecimal getValueMask() {
        valueMask = value ;
        return valueMask;
    }
    public void setValueMask(BigDecimal valueMask) {
        updateDecimalValue("value",valueMask);
    }
    public String getValueMaskDD() {
        return "SalaryElementFormula_valueMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job job;

    public void setJob(Job job) {
        this.job = job;
    }

    public Job getJob() {
        return job;
    }

    public String getJobDD() {
        return "SalaryElementFormula_job";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public void setPosition(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "SalaryElementFormula_position";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="effectiveDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date effectiveDate;

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public String getEffectiveDateDD() {
        return "SalaryElementFormula_effectiveDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="operator">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC operator;

    public void setOperator(UDC operator) {
        this.operator = operator;
    }

    public UDC getOperator() {
        return operator;
    }

    public String getOperatorDD() {
        return "SalaryElementFormula_operator";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="attribute">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC attribute;

    public void setAttribute(UDC attribute) {
        this.attribute = attribute;
    }

    public UDC getAttribute() {
        return attribute;
    }

    public String getAttributeDD() {
        return "SalaryElementFormula_attribute";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }
    
    public String getLocationDD() {
        return "SalaryElementFormula_location";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="maritalStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC maritalStatus;

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
    
    public String getMaritalStatusDD() {
        return "SalaryElementFormula_maritalStatus";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade payGrade;

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public PayGrade getPayGrade() {
        return payGrade;
    }
    
    public String getPayGradeDD() {
        return "SalaryElementFormula_payGrade";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }
    
    public String getCostCenterDD() {
        return "SalaryElementFormula_costCenter";
    }
    //</editor-fold>
    
}