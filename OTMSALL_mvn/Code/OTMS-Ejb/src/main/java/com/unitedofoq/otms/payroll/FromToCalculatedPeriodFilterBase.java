package com.unitedofoq.otms.payroll;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class FromToCalculatedPeriodFilterBase extends FilterBase {

    // <editor-fold defaultstate="collapsed" desc="toCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod toCalculatedPeriod;

    public CalculatedPeriod getToCalculatedPeriod() {
        return toCalculatedPeriod;
    }

    public String getToCalculatedPeriodDD() {
        return "FromToCalculatedPeriodFilterBase_toCalculatedPeriod";
    }

    public void setToCalculatedPeriod(CalculatedPeriod toCalculatedPeriod) {
        this.toCalculatedPeriod = toCalculatedPeriod;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="fromCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod fromCalculatedPeriod;

    public CalculatedPeriod getFromCalculatedPeriod() {
        return fromCalculatedPeriod;
    }

    public void setFromCalculatedPeriod(CalculatedPeriod fromCalculatedPeriod) {
        this.fromCalculatedPeriod = fromCalculatedPeriod;
    }

    public String getFromCalculatedPeriodDD() {
        return "FromToCalculatedPeriodFilterBase_fromCalculatedPeriod";
    }
    // </editor-fold >

}
