/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author bassem
 */
@MappedSuperclass
public class EmployeeMedicalBase extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="MedicalCategories">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private MedicalCategories medicalcategories;

    public MedicalCategories getMedicalcategories() {
        return medicalcategories;
    }

    public String getMedicalcategoriesDD() {
        return "EmployeeMedicalBase_medicalcategories";
    }

    public void setMedicalcategories(MedicalCategories medicalcategories) {
        this.medicalcategories = medicalcategories;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Medicalinsured">
    @Column(length = 1)
    private String medicalInsured;

    public String getMedicalInsured() {
        return medicalInsured;
    }

    public void setMedicalInsured(String medicalInsured) {
        this.medicalInsured = medicalInsured;
    }

    public String getMedicalInsuredDD() {
        return "EmployeeMedicalBase_medicalInsured";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Medicalamount">
    @Column(precision = 18, scale = 3)
    private BigDecimal medicalAmount;

    public BigDecimal getMedicalAmount() {
        return medicalAmount;
    }

    public String getMedicalAmountDD() {
        return "EmployeeMedicalBase_medicalAmount";
    }

    public void setMedicalAmount(BigDecimal medicalAmount) {
        this.medicalAmount = medicalAmount;
    }
    @Transient
    private BigDecimal medicalAmountMask;

    public BigDecimal getMedicalAmountMask() {
        medicalAmountMask = medicalAmount;
        return medicalAmountMask;
    }

    public String getMedicalAmountMaskDD() {
        return "EmployeeMedicalBase_medicalAmountMask";
    }

    public void setMedicalAmountMask(BigDecimal medicalAmountMask) {
        updateDecimalValue("medicalAmount", medicalAmountMask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalGlassesStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC medicalGlassesStatus;

    public UDC getMedicalGlassesStatus() {
        return medicalGlassesStatus;
    }

    public String getMedicalGlassesStatusDD() {
        return "EmployeeMedicalBase_medicalGlassesStatus";
    }

    public void setMedicalGlassesStatus(UDC medicalGlassesStatus) {
        this.medicalGlassesStatus = medicalGlassesStatus;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalGlassesDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date medicalGlassesDate;

    public Date getMedicalGlassesDate() {
        return medicalGlassesDate;
    }

    public String getMedicalGlassesDateDD() {
        return "EmployeeMedicalBase_medicalGlassesDate";
    }

    public void setMedicalGlassesDate(Date medicalGlassesDate) {
        this.medicalGlassesDate = medicalGlassesDate;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="consumedMedicalAmount">
    @Column(precision = 18, scale = 3)
    private BigDecimal consumedMedicalAmount;

    public BigDecimal getConsumedMedicalAmount() {
        return consumedMedicalAmount;
    }

    public String getConsumedMedicalAmountDD() {
        return "EmployeeMedicalBase_consumedMedicalAmount";
    }

    public void setConsumedMedicalAmount(BigDecimal consumedMedicalAmount) {
        this.consumedMedicalAmount = consumedMedicalAmount;
    }
    @Transient
    private BigDecimal consumedMedicalAmountMask;

    public BigDecimal getConsumedMedicalAmountMask() {
        consumedMedicalAmountMask = consumedMedicalAmount;
        return consumedMedicalAmountMask;
    }

    public String getConsumedMedicalAmountMaskDD() {
        return "EmployeeMedicalBase_consumedMedicalAmountMask";
    }

    public void setConsumedMedicalAmountMask(BigDecimal consumedMedicalAmountMask) {
        updateDecimalValue("consumedMedicalAmount", consumedMedicalAmountMask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="exceedLimit">
    private boolean exceedLimit;

    public boolean isExceedLimit() {
        return exceedLimit;
    }
    
    public String getExceedLimitDD() {
        return "EmployeeMedicalBase_exceedLimit";
    }

    public void setExceedLimit(boolean exceedLimit) {
        this.exceedLimit = exceedLimit;
    }
    //</editor-fold >
}
