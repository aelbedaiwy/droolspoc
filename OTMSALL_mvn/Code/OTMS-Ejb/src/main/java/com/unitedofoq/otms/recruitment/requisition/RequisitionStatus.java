package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class RequisitionStatus extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="requisition">
    @OneToOne
    private Requisition requisition;

    public Requisition getRequisition() {
        return requisition;
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }

    public String getRequisitionDD() {
        return "RequisitionStatus_requisition";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="internalStatus">
    private String internalStatus;

    public String getInternalStatus() {
        return internalStatus;
    }

    public void setInternalStatus(String internalStatus) {
        this.internalStatus = internalStatus;
    }

    public String getInternalStatusDD() {
        return "RequisitionStatus_internalStatus";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="external">
    private String externalStatus;

    public String getExternalStatus() {
        return externalStatus;
    }

    public void setExternalStatus(String externalStatus) {
        this.externalStatus = externalStatus;
    }

    public String getExternalStatusDD() {
        return "RequisitionStatus_external";
    }
//</editor-fold>
}
