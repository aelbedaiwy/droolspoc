package com.unitedofoq.otms.appraisal.template;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = "appraisalTemplate")
public class AppraisalElement extends AppraisalElementBase {

    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    @Override
    public void PostLoad() {
        super.PostLoad();
        setM2mCheck(true);
    }
}
