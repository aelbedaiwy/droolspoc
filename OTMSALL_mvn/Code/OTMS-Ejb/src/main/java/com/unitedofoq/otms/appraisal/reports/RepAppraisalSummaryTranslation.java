package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepAppraisalSummaryTranslation extends RepAppraisalSheetTranslationBase {
    // <editor-fold defaultstate="collapsed" desc="element">

    @Column
    private String element;

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }
    // </editor-fold>
}
