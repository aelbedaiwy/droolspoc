/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;


/**
 *
 * @author unitedofoq
 */
@Entity
public class FactoryTranslation extends BaseEntityTranslation {
    
    //<editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="owner">
    @Column
    private String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    // </editor-fold>
}
