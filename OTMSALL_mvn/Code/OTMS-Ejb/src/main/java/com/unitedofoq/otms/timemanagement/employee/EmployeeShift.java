package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class EmployeeShift extends TMEmpAnnualBase {
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeShift_employee";
    }
    //</editor-fold>
    @Transient 
    private EmployeeAnnualPlanner annualPlanner;

    public EmployeeAnnualPlanner getAnnualPlanner() {
        return annualPlanner;
    }

    public void setAnnualPlanner(EmployeeAnnualPlanner annualPlanner) {
        this.annualPlanner = annualPlanner;
    }

}
