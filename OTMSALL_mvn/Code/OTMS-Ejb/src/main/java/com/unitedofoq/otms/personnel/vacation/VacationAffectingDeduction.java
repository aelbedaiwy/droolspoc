/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author abayomy
 */
@Entity
@DiscriminatorValue("VACATIONDED")
@ParentEntity(fields={"vacation"})
//@FABSEntitySpecs(hideInheritedFields={"salaryElement"})
public class VacationAffectingDeduction extends VacationAffectingSalaryElement {
  
    // <editor-fold defaultstate="collapsed" desc="deduction">
    @JoinColumn//(name="SALARYELEMENT_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
public String getDeductionDD() {
        return "VacationAffectingDeduction_deduction";
    }
// </editor-fold>
  
}
