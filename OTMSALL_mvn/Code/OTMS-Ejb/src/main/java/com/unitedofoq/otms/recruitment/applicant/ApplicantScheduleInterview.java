package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.interview.Interview;
import com.unitedofoq.otms.recruitment.interview.PositionInterview;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantScheduleInterview extends ApplicantScheduleBase {

    // <editor-fold defaultstate="collapsed" desc="interview">
    @ManyToOne(fetch = FetchType.LAZY)
    private Interview interview;

    public Interview getInterview() {
        return interview;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }

    public String getInterviewDD() {
        return "ApplicantScheduleInterview_interview";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="interviewer">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee interviewer;

    public Employee getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(Employee interviewer) {
        this.interviewer = interviewer;
    }

    public String getInterviewerDD() {
        return "ApplicantScheduleInterview_interviewer";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionInterview">
    @ManyToOne(fetch = FetchType.LAZY)

    private PositionInterview positionInterview;

    public PositionInterview getPositionInterview() {
        return positionInterview;
    }

    public void setPositionInterview(PositionInterview positionInterview) {
        this.positionInterview = positionInterview;
    }

    public String getPositionInterviewDD() {
        return "ApplicantScheduleInterview_positionInterview";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scheduleTime">
    private String scheduleTime;

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getScheduleTimeDD() {
        return "ApplicantScheduleInterview_time";
    }
     // </editor-fold>
}
