/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElementType;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
@ParentEntity(fields={"calculatedPeriod"})
public class CostCenterCalculation extends BaseEntity{
    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD() {
        return "CostCenterCalculation_employee";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    
    public String getSalaryElementDD() {
        return "CostCenterCalculation_salaryElement";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;
    
    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }
    
    public String getCostCenterDD() {
        return "CostCenterCalculation_costCenter";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }
    
    public String getCalculatedPeriodDD() {
        return "CostCenterCalculation_calculatedPeriod";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="salaryElementValue">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal salaryElementValue;

    public BigDecimal getSalaryElementValue() {
        return salaryElementValue;
    }

    public void setSalaryElementValue(BigDecimal salaryElementValue) {
        this.salaryElementValue = salaryElementValue;
    }
    
    public String getSalaryElementValueDD() {
        return "CostCenterCalculation_salaryElementValue";
    }
    @Transient
    private BigDecimal salaryElementValueMask = java.math.BigDecimal.ONE;
    public BigDecimal getSalaryElementValueMask() {
        salaryElementValueMask = salaryElementValue ;
        return salaryElementValueMask;
    }
    public void setSalaryElementValueMask(BigDecimal salaryElementValueMask) {
        updateDecimalValue("salaryElementValue",salaryElementValueMask);
    }
    public String getSalaryElementValueMaskDD() {
        return "CostCenterCalculation_salaryElementValueMask";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="distributionPercentage">
    @Column(precision=25, scale=13,nullable=false)
    private  BigDecimal distributionPercentage;

    public BigDecimal getDistributionPercentage() {
        return distributionPercentage;
    }

    public void setDistributionPercentage(BigDecimal distributionPercentage) {
        this.distributionPercentage = distributionPercentage;
    }
    
    public String getDistributionPercentageDD() {
        return "CostCenterCalculation_distributionPercentage";
    }
    @Transient
    private BigDecimal distributionPercentageMask = java.math.BigDecimal.ONE;
    public BigDecimal getDistributionPercentageMask() {
        distributionPercentageMask = distributionPercentage ;
        return distributionPercentageMask;
    }
    public void setDistributionPercentageMask(BigDecimal distributionPercentageMask) {
        updateDecimalValue("distributionPercentage",distributionPercentageMask);
    }
    public String getDistributionPercentageMaskDD() {
        return "CostCenterCalculation_distributionPercentageMask";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="distributionDays">
    @Column(precision=25, scale=13,nullable=false)
    private  BigDecimal distributionDays;

    public BigDecimal getDistributionDays() {
        return distributionDays;
    }

    public void setDistributionDays(BigDecimal distributionDays) {
        this.distributionDays = distributionDays;
    }
    
    public String getDistributionDaysDD() {
        return "CostCenterCalculation_distributionDays";
    }
    @Transient
    private BigDecimal distributionDaysMask = java.math.BigDecimal.ONE;
    public BigDecimal getDistributionDaysMask() {
        distributionDaysMask = distributionDays ;
        return distributionDaysMask;
    }
    public void setDistributionDaysMask(BigDecimal distributionDaysMask) {
        updateDecimalValue("distributionDays",distributionDaysMask);
    }
    public String getDistributionDaysMaskDD() {
        return "CostCenterCalculation_distributionDaysMask";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="distributionHours">
    @Column(precision=25, scale=13,nullable=false)
    private  BigDecimal distributionHours;

    public BigDecimal getDistributionHours() {
        return distributionHours;
    }

    public void setDistributionHours(BigDecimal distributionHours) {
        this.distributionHours = distributionHours;
    }
    
    public String getDistributionHoursDD() {
        return "CostCenterCalculation_distributionHours";
    }
    @Transient
    private BigDecimal distributionHoursMask = java.math.BigDecimal.ONE;
    public BigDecimal getDistributionHoursMask() {
        distributionHoursMask = distributionHours ;
        return distributionHoursMask;
    }
    public void setDistributionHoursMask(BigDecimal distributionHoursMask) {
        updateDecimalValue("distributionHours",distributionHoursMask);
    }
    public String getDistributionHoursMaskDD() {
        return "CostCenterCalculation_distributionHoursMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="distributionValue">
    @Column(precision=25, scale=13,nullable=false)
    private  BigDecimal distributionValue;

    public BigDecimal getDistributionValue() {
        return distributionValue;
    }

    public void setDistributionValue(BigDecimal distributionValue) {
        this.distributionValue = distributionValue;
    }
    
    public String getDistributionValueDD() {
        return "CostCenterCalculation_distributionValue";
    }
    @Transient
    private BigDecimal distributionValueMask = java.math.BigDecimal.ONE;
    public BigDecimal getDistributionValueMask() {
        distributionValueMask = distributionValue ;
        return distributionValueMask;
    }
    public void setDistributionValueMask(BigDecimal distributionValueMask) {
        updateDecimalValue("distributionValue",distributionValueMask);
    }
    public String getDistributionValueMaskDD() {
        return "CostCenterCalculation_distributionValueMask";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="salaryElementType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElementType salaryElementType;

    public SalaryElementType getSalaryElementType() {
        return salaryElementType;
    }

    public void setSalaryElementType(SalaryElementType salaryElementType) {
        this.salaryElementType = salaryElementType;
    }
    
    public String getSalaryElementTypeDD() {
        return "CostCenterCalculation_salaryElementType";
    }
    //</editor-fold>
}