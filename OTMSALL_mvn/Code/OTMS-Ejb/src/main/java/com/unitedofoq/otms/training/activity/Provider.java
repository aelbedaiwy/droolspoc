/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
//@ChildEntity(fields={"cancellationRegulations","providerActivities"})
@ChildEntity(fields={/*"trainingPlans",*/"providerCourseClasses"})
public class Provider extends BusinessObjectBaseEntity {
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "Provider_company";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
//    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    @Translatable(translationField = "descriptionTranslated")
	private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionDD() {
        return "Provider_description";
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "Provider_description";
    }
//    @Transient
//    @Translation(originalField="description")
//    private String descriptionTranslated;
//
//    public String getDescriptionTranslated() {
//        return descriptionTranslated;
//    }
//
//    public void setDescriptionTranslated(String descriptionTranslated) {
//        this.descriptionTranslated = descriptionTranslated;
//    }
//    public String getDescriptionTranslatedDD() {
//        return "Provider_description";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="address">
    @Column
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDD() {
        return "Provider_address";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="telephone">
    @Column
    private String telephone;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephoneDD() {
        return "Provider_telephone";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fax">
    @Column
    private String fax;

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFaxDD() {
        return "Provider_fax";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="country">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC country;

    public UDC getCountry() {
        return country;
    }

    public void setCountry(UDC country) {
        this.country = country;
    }

    public String getCountryDD() {
        return "Provider_country";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="webAddress">
    @Column
    private String webAddress;

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getWebAddressDD() {
        return "Provider_webAddress";
    }
    // </editor-fold>
    //    // <editor-fold defaultstate="collapsed" desc="cancellationRegulations">
    //    @OneToMany(mappedBy="provider")
    //    private List<ProviderCancellationRegulation> cancellationRegulations;
    //
    //    public List<ProviderCancellationRegulation> getCancellationRegulations() {
    //        return cancellationRegulations;
    //    }
    //
    //    public void setCancellationRegulations(List<ProviderCancellationRegulation> cancellationRegulations) {
    //        this.cancellationRegulations = cancellationRegulations;
    //    }
    //
    //    public String getCancellationRegulationsDD() {
    //        return "Provider_cancellationRegulations";
    //    }
    //    // </editor-fold>
    //    // <editor-fold defaultstate="collapsed" desc="providerActivities">
    //    @OneToMany(mappedBy = "provider")
    //    private List<ProviderActivity> providerActivities;
    //
    //    public List<ProviderActivity> getProviderActivities() {
    //        return providerActivities;
    //    }
    //
    //    public void setProviderActivities(List<ProviderActivity> providerActivities) {
    //        this.providerActivities = providerActivities;
    //    }
    //
    //    public String getProviderActivitiesDD() {
    //        return "Provider_providerActivities";
    //    }
    //    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="trainingPlans">
//    @OneToMany(mappedBy="provider")
//    private List<ProviderTrainingPlan> trainingPlans;
//
//    public List<ProviderTrainingPlan> getTrainingPlans() {
//        return trainingPlans;
//    }
//
//    public void setTrainingPlans(List<ProviderTrainingPlan> trainingPlans) {
//        this.trainingPlans = trainingPlans;
//    }
//    
//    public String getTrainingPlansDD() {
//        return "Provider_trainingPlans";
//    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="providerCourseClasses">
    @OneToMany(mappedBy="provider")
    private List<ProviderCourseClass> providerCourseClasses;

    public List<ProviderCourseClass> getProviderCourseClasses() {
        return providerCourseClasses;
    }

    public void setProviderCourseClasses(List<ProviderCourseClass> providerCourseClasses) {
        this.providerCourseClasses = providerCourseClasses;
    }
    
    public String getProviderCourseClassesDD() {
        return "Provider_providerCourseClasses";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }
    
    public String getTypeDD() {
        return "Provider_type";
    }
    //</editor-fold>
}