/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ashienawy
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PPTYPE")
public class PositionParentBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="fromDate">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date fromDate;

        public Date getFromDate() {
            return fromDate;
        }

        public void setFromDate(Date fromDate) {
            this.fromDate = fromDate;
        }

         public String getFromDateDD() {
            return "PositionParentBase_fromDate";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date toDate;

        public Date getToDate() {
            return toDate;
        }

        public void setToDate(Date toDate) {
            this.toDate = toDate;
        }

         public String getToDateDD() {
            return "PositionParentBase_toDate";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionParentPriority">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC positionParentPriority;
    public String getPositionParentPriorityDD() {
            return "PositionParentBase_positionParentPriority";
    }
    public UDC getPositionParentPriority() {
         return positionParentPriority;
    }
    public void setPositionParentPriority(UDC positionParentPriority) {
            this.positionParentPriority = positionParentPriority;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentPosition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position parentPosition ;
    public Position getParentPosition() {
        return parentPosition;
    }
    public void setParentPosition(Position parentPosition) {
        this.parentPosition = parentPosition;
    }
    public String getParentPositionDD() {
        return "PositionParentBase_parentPosition";
    }
    // </editor-fold>

  }
