
package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"company"})
public class GLAccountSegment extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public void setCompany(Company company) {
        this.company = company;
    }

    public Company getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "GLAccountSegment_company";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="segmentNumber">
    @Column
    private Integer segmentNumber;

    public void setSegmentNumber(Integer segmentNumber) {
        this.segmentNumber = segmentNumber;
    }

    public Integer getSegmentNumber() {
        return segmentNumber;
    }

    public String getSegmentNumberDD() {
        return "GLAccountSegment_segmentNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="referenceValue">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC referenceValue;

    public void setReferenceValue(UDC referenceValue) {
        this.referenceValue = referenceValue;
    }

    public UDC getReferenceValue() {
        return referenceValue;
    }

    public String getReferenceValueDD() {
        return "GLAccountSegment_referenceValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="constantvalue">
    @Column
    private String constantvalue;

    public void setConstantvalue(String constantvalue) {
        this.constantvalue = constantvalue;
    }

    public String getConstantvalue() {
        return constantvalue;
    }

    public String getConstantvalueDD() {
        return "GLAccountSegment_constantvalue";
    }
    // </editor-fold>

}
