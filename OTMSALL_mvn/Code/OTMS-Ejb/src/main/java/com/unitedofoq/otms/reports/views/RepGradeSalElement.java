
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepGradeSalElement extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="grade">
    @Column
    @Translatable(translationField = "gradeTranslated")
    private String grade;

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public String getGradeDD() {
        return "RepGradeSalElement_grade";
    }
    
    @Transient
    @Translation(originalField = "grade")
    private String gradeTranslated;

    public String getGradeTranslated() {
        return gradeTranslated;
    }

    public void setGradeTranslated(String gradeTranslated) {
        this.gradeTranslated = gradeTranslated;
    }
    
    public String getGradeTranslatedDD() {
        return "RepGradeSalElement_grade";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getCompanyIDDD() {
        return "RepGradeSalElement_companyID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepGradeSalElement_dsDbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "RepGradeSalElement_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="paymentMethod">
    @Column
    @Translatable(translationField = "paymentMethodTranslated")
    private String paymentMethod;

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getPaymentMethodDD() {
        return "RepGradeSalElement_paymentMethod";
    }
    
    @Transient
    @Translation(originalField = "paymentMethod")
    private String paymentMethodTranslated;

    public String getPaymentMethodTranslated() {
        return paymentMethodTranslated;
    }

    public void setPaymentMethodTranslated(String paymentMethodTranslated) {
        this.paymentMethodTranslated = paymentMethodTranslated;
    }
    
    public String getPaymentMethodTranslatedDD() {
        return "RepGradeSalElement_paymentMethod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @Column    
    @Translatable(translationField = "salaryElementTranslated")
    private String salaryElement;

    public void setSalaryElement(String salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElement() {
        return salaryElement;
    }

    public String getSalaryElementDD() {
        return "RepGradeSalElement_salaryElement";
    }
    @Transient
    @Translation(originalField = "salaryElement")
    private String salaryElementTranslated;

    public String getSalaryElementTranslated() {
        return salaryElementTranslated;
    }

    public void setSalaryElementTranslated(String salaryElementTranslated) {
        this.salaryElementTranslated = salaryElementTranslated;
    }
    
    public String getSalaryElementTranslatedDD() {
        return "RepGradeSalElement_salaryElement";
    }    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="minSalValue">
    @Column
    private BigDecimal minSalValue;

    public void setMinSalValue(BigDecimal minSalValue) {
        this.minSalValue = minSalValue;
    }

    public BigDecimal getMinSalValue() {
        return minSalValue;
    }

    public String getMinSalValueDD() {
        return "RepGradeSalElement_minSalValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="maxSalValue">
    @Column
    private BigDecimal maxSalValue;

    public void setMaxSalValue(BigDecimal maxSalValue) {
        this.maxSalValue = maxSalValue;
    }

    public BigDecimal getMaxSalValue() {
        return maxSalValue;
    }

    public String getMaxSalValueDD() {
        return "RepGradeSalElement_maxSalValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="midSalValue">
    @Column
    private BigDecimal midSalValue;

    public void setMidSalValue(BigDecimal midSalValue) {
        this.midSalValue = midSalValue;
    }

    public BigDecimal getMidSalValue() {
        return midSalValue;
    }

    public String getMidSalValueDD() {
        return "RepGradeSalElement_midSalValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextGrade">
    @Column
    @Translatable(translationField = "nextGradeTranslated")
    private String nextGrade;

    public void setNextGrade(String nextGrade) {
        this.nextGrade = nextGrade;
    }

    public String getNextGrade() {
        return nextGrade;
    }

    public String getNextGradeDD() {
        return "RepGradeSalElement_nextGrade";
    }
    
    @Transient
    @Translation(originalField = "nextGrade")
    private String nextGradeTranslated;

    public String getNextGradeTranslated() {
        return nextGradeTranslated;
    }

    public void setNextGradeTranslated(String nextGradeTranslated) {
        this.nextGradeTranslated = nextGradeTranslated;
    }
    
    public String getNextGradeTranslatedDD() {
        return "RepGradeSalElement_nextGrade";
    }
    // </editor-fold>

}