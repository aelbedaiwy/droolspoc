/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author lap2
 */
@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeOtherInfo extends BaseEntity {
    
        // <editor-fold defaultstate="collapsed" desc="employee">
       @OneToOne(fetch = javax.persistence.FetchType.LAZY)
       @JoinColumn(nullable=false)
       private Employee employee;
       public Employee getEmployee() {
           return employee;
       }
       public void setEmployee(Employee employee) {
           this.employee = employee;
       }
       public String getEmployeeDD() {
           return "EmployeeOtherInfo_employee";
       }
       // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="educationDegree">
       @Transient
       @Translation(originalField = "educationDegree")
     
       private String educationDegreeTranslated;

    public String getEducationDegreeTranslated() {
        return educationDegreeTranslated;
    }

    public void setEducationDegreeTranslated(String educationDegreeTranslated) {
        this.educationDegreeTranslated = educationDegreeTranslated;
    }
       public String getEducationDegreeTranslatedDD() {
        return "EmployeeOtherInfo_educationDegreeTranslated";
    }
        @Translatable(translationField = "educationDegreeTranslated")
        @Column
       private String educationDegree;
       public String getEducationDegree() {
           return educationDegree;
       }

       public void setEducationDegree(String educationDegree) {
           this.educationDegree = educationDegree;
       }
       public String getEducationDegreeDD() {
           return "EmployeeOtherInfo_educationDegree";
       }

           // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="grade">
       @Transient
       @Translation(originalField = "grade")
       private String gradeTranslated;

    public String getGradeTranslated() {
        return gradeTranslated;
    }

    public void setGradeTranslated(String gradeTranslated) {
        this.gradeTranslated = gradeTranslated;
    }
       public String getGradeTranslatedDD() {
        return "EmployeeOtherInfo_gradeTranslated";
    }
       @Translatable(translationField = "gradeTranslated")
        @Column
       private String grade;

       public String getGrade() {
           return grade;
       }

       public void setGrade(String grade) {
           this.grade = grade;
       }
       public String getGradeDD() {
           return "EmployeeOtherInfo_grade";
       }
           // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="graduationPlace">
        @Transient
        @Translation(originalField = "graduationPlace")
        private String graduationPlaceTranslated;

    public String getGraduationPlaceTranslated() {
        return graduationPlaceTranslated;
    }

    public void setGraduationPlaceTranslated(String graduationPlaceTranslated) {
        this.graduationPlaceTranslated = graduationPlaceTranslated;
    }
        public String getGraduationPlaceTranslatedDD() {
        return "EmployeeOtherInfo_graduationPlaceTranslated";
    }
    @Translatable(translationField = "graduationPlaceTranslated")
    @Column
       private  String graduationPlace;
       public String getGraduationPlace() {
           return graduationPlace;
       }

       public void setGraduationPlace(String graduationPlace) {
           this.graduationPlace = graduationPlace;
       }
        public String getGraduationPlaceDD() {
           return "EmployeeOtherInfo_graduationPlace";
       }
           // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="graduationYear">
         private int graduationYear;

        public int getGraduationYear() {
            return graduationYear;
        }

        public void setGraduationYear(int graduationYear) {
            this.graduationYear = graduationYear;
        }
         public String getGraduationYearDD() {
            return "EmployeeOtherInfo_graduationYear";
        }


    // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="militaryStatus">  
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        UDC militaryStatus;

        public UDC getMilitaryStatus() {
            return militaryStatus;
        }

        public void setMilitaryStatus(UDC militaryStatus) {
            this.militaryStatus = militaryStatus;
        }

         public String getMilitaryStatusDD() {
            return "EmployeeOtherInfo_militaryStatus";
        }
          // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="postponeDate">  
          @Temporal(TemporalType.TIMESTAMP)
        private Date postponeDate;
          public Date getPostponeDate() {
            return postponeDate;
        }

        public void setPostponeDate(Date postponeDate) {
            this.postponeDate = postponeDate;
        }

          public String getPostponeDateDD() {
            return "EmployeeOtherInfo_postponeDate";
        }
          // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="healthCertificate">  
          
           @Transient
        @Translation(originalField = "healthCertificate")
        private String healthCertificateTranslated;

    public String getHealthCertificateTranslated() {
        return healthCertificateTranslated;
    }

    public void setHealthCertificateTranslated(String healthCertificateTranslated) {
        this.healthCertificateTranslated = healthCertificateTranslated;
    }
          public String getHealthCertificateTranslatedDD() {
        return "EmployeeOtherInfo_healthCertificateTranslated";
    } 
    @Translatable(translationField = "healthCertificateTranslated")
    @Column
          private String healthCertificate;

        public String getHealthCertificate() {
            return healthCertificate;
        }

        public void setHealthCertificate(String healthCertificate) {
            this.healthCertificate = healthCertificate;
        }
         public String getHealthCertificateDD() {
            return "EmployeeOtherInfo_healthCertificate";
        }
                  // </editor-fold>  
        // <editor-fold defaultstate="collapsed" desc="healthCertificateEndDate"> 
             @Temporal(TemporalType.TIMESTAMP)
        private Date healthCertificateEndDate;

        public Date getHealthCertificateEndDate() {
            return healthCertificateEndDate;
        }

        public void setHealthCertificateEndDate(Date healthCertificateEndDate) {
            this.healthCertificateEndDate = healthCertificateEndDate;
        }
             public String getHealthCertificateEndDateDD() {
            return "EmployeeOtherInfo_healthCertificateEndDate";
        } 

                           // </editor-fold> 
}
