/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;


/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields = {"employee"})
public class TimeSheetConsolidation extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "TimeSheetConsolidation_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }
    
    public String getCalculatedPeriodDD() {
        return "TimeSheetConsolidation_calculatedPeriod";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute1">
    private BigDecimal attribute1;

    public BigDecimal getAttribute1() {
        return attribute1;
    }
    public String getAttribute1DD() {
        return "TimeSheetConsolidation_attribute1";
    }

    public void setAttribute1(BigDecimal attribute1) {
        this.attribute1 = attribute1;
    }
    @Transient
    private BigDecimal attribute1Mask;

    public BigDecimal getAttribute1Mask() {
        attribute1Mask = attribute1;
        return attribute1Mask;
    }
    public String getAttribute1MaskDD() {
        return "TimeSheetConsolidation_attribute1Mask";
    }

    public void setAttribute1Mask(BigDecimal attribute1Mask) {
        updateDecimalValue("attribute1", attribute1Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute2">
    private BigDecimal attribute2;

    public BigDecimal getAttribute2() {
        return attribute2;
    }
    public String getAttribute2DD() {
        return "TimeSheetConsolidation_attribute2";
    }

    public void setAttribute2(BigDecimal attribute2) {
        this.attribute2 = attribute2;
    }
    @Transient
    private BigDecimal attribute2Mask;

    public BigDecimal getAttribute2Mask() {
        attribute2Mask = attribute2;
        return attribute2Mask;
    }
    public String getAttribute2MaskDD() {
        return "TimeSheetConsolidation_attribute2Mask";
    }

    public void setAttribute2Mask(BigDecimal attribute2Mask) {
        updateDecimalValue("attribute2", attribute2Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute3">
    private BigDecimal attribute3;

    public BigDecimal getAttribute3() {
        return attribute3;
    }
    public String getAttribute3DD() {
        return "TimeSheetConsolidation_attribute3";
    }

    public void setAttribute3(BigDecimal attribute3) {
        this.attribute3 = attribute3;
    }
    @Transient
    private BigDecimal attribute3Mask;

    public BigDecimal getAttribute3Mask() {
        attribute3Mask = attribute3;
        return attribute3Mask;
    }
    public String getAttribute3MaskDD() {
        return "TimeSheetConsolidation_attribute3Mask";
    }

    public void setAttribute3Mask(BigDecimal attribute3Mask) {
        updateDecimalValue("attribute3", attribute3Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute4">
    private BigDecimal attribute4;

    public BigDecimal getAttribute4() {
        return attribute4;
    }
    public String getAttribute4DD() {
        return "TimeSheetConsolidation_attribute4";
    }

    public void setAttribute4(BigDecimal attribute4) {
        this.attribute4 = attribute4;
    }
    @Transient
    private BigDecimal attribute4Mask;

    public BigDecimal getAttribute4Mask() {
        attribute4Mask = attribute4;
        return attribute4Mask;
    }
    public String getAttribute4MaskDD() {
        return "TimeSheetConsolidation_attribute4Mask";
    }

    public void setAttribute4Mask(BigDecimal attribute4Mask) {
        updateDecimalValue("attribute4", attribute4Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute5">
    private BigDecimal attribute5;

    public BigDecimal getAttribute5() {
        return attribute5;
    }
    public String getAttribute5DD() {
        return "TimeSheetConsolidation_attribute5";
    }

    public void setAttribute5(BigDecimal attribute5) {
        this.attribute5 = attribute5;
    }
    @Transient
    private BigDecimal attribute5Mask;

    public BigDecimal getAttribute5Mask() {
        attribute5Mask = attribute5;
        return attribute5Mask;
    }
    public String getAttribute5MaskDD() {
        return "TimeSheetConsolidation_attribute5Mask";
    }

    public void setAttribute5Mask(BigDecimal attribute5Mask) {
        updateDecimalValue("attribute5", attribute5Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute6">
    private BigDecimal attribute6;

    public BigDecimal getAttribute6() {
        return attribute6;
    }
    public String getAttribute6DD() {
        return "TimeSheetConsolidation_attribute6";
    }

    public void setAttribute6(BigDecimal attribute6) {
        this.attribute6 = attribute6;
    }
    @Transient
    private BigDecimal attribute6Mask;

    public BigDecimal getAttribute6Mask() {
        attribute6Mask = attribute6;
        return attribute6Mask;
    }
    public String getAttribute6MaskDD() {
        return "TimeSheetConsolidation_attribute6Mask";
    }

    public void setAttribute6Mask(BigDecimal attribute6Mask) {
        updateDecimalValue("attribute6", attribute6Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute7">
    private BigDecimal attribute7;

    public BigDecimal getAttribute7() {
        return attribute7;
    }
    public String getAttribute7DD() {
        return "TimeSheetConsolidation_attribute7";
    }

    public void setAttribute7(BigDecimal attribute7) {
        this.attribute7 = attribute7;
    }
    @Transient
    private BigDecimal attribute7Mask;

    public BigDecimal getAttribute7Mask() {
        attribute7Mask = attribute7;
        return attribute7Mask;
    }
    public String getAttribute7MaskDD() {
        return "TimeSheetConsolidation_attribute7Mask";
    }

    public void setAttribute7Mask(BigDecimal attribute7Mask) {
        updateDecimalValue("attribute7", attribute7Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute8">
    private BigDecimal attribute8;

    public BigDecimal getAttribute8() {
        return attribute8;
    }
    public String getAttribute8DD() {
        return "TimeSheetConsolidation_attribute8";
    }

    public void setAttribute8(BigDecimal attribute8) {
        this.attribute8 = attribute8;
    }
    @Transient
    private BigDecimal attribute8Mask;

    public BigDecimal getAttribute8Mask() {
        attribute8Mask = attribute8;
        return attribute8Mask;
    }
    public String getAttribute8MaskDD() {
        return "TimeSheetConsolidation_attribute8Mask";
    }

    public void setAttribute8Mask(BigDecimal attribute8Mask) {
        updateDecimalValue("attribute8", attribute8Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute9">
    private BigDecimal attribute9;

    public BigDecimal getAttribute9() {
        return attribute9;
    }
    public String getAttribute9DD() {
        return "TimeSheetConsolidation_attribute9";
    }

    public void setAttribute9(BigDecimal attribute9) {
        this.attribute9 = attribute9;
    }
    @Transient
    private BigDecimal attribute9Mask;

    public BigDecimal getAttribute9Mask() {
        attribute9Mask = attribute9;
        return attribute9Mask;
    }
    public String getAttribute9MaskDD() {
        return "TimeSheetConsolidation_attribute9Mask";
    }

    public void setAttribute9Mask(BigDecimal attribute9Mask) {
        updateDecimalValue("attribute9", attribute9Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute10">
    private BigDecimal attribute10;

    public BigDecimal getAttribute10() {
        return attribute10;
    }
    public String getAttribute10DD() {
        return "TimeSheetConsolidation_attribute10";
    }

    public void setAttribute10(BigDecimal attribute10) {
        this.attribute10 = attribute10;
    }
    @Transient
    private BigDecimal attribute10Mask;

    public BigDecimal getAttribute10Mask() {
        attribute10Mask = attribute10;
        return attribute10Mask;
    }
    public String getAttribute10MaskDD() {
        return "TimeSheetConsolidation_attribute10Mask";
    }

    public void setAttribute10Mask(BigDecimal attribute10Mask) {
        updateDecimalValue("attribute10", attribute10Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute11">
    private BigDecimal attribute11;

    public BigDecimal getAttribute11() {
        return attribute11;
    }
    public String getAttribute11DD() {
        return "TimeSheetConsolidation_attribute11";
    }

    public void setAttribute11(BigDecimal attribute11) {
        this.attribute11 = attribute11;
    }
    @Transient
    private BigDecimal attribute11Mask;

    public BigDecimal getAttribute11Mask() {
        attribute11Mask = attribute11;
        return attribute11Mask;
    }
    public String getAttribute11MaskDD() {
        return "TimeSheetConsolidation_attribute11Mask";
    }

    public void setAttribute11Mask(BigDecimal attribute11Mask) {
        updateDecimalValue("attribute11", attribute11Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute12">
    private BigDecimal attribute12;

    public BigDecimal getAttribute12() {
        return attribute12;
    }
    public String getAttribute12DD() {
        return "TimeSheetConsolidation_attribute12";
    }

    public void setAttribute12(BigDecimal attribute12) {
        this.attribute12 = attribute12;
    }
    @Transient
    private BigDecimal attribute12Mask;

    public BigDecimal getAttribute12Mask() {
        attribute12Mask = attribute12;
        return attribute12Mask;
    }
    public String getAttribute12MaskDD() {
        return "TimeSheetConsolidation_attribute12Mask";
    }

    public void setAttribute12Mask(BigDecimal attribute12Mask) {
        updateDecimalValue("attribute12", attribute12Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute13">
    private BigDecimal attribute13;

    public BigDecimal getAttribute13() {
        return attribute13;
    }
    public String getAttribute13DD() {
        return "TimeSheetConsolidation_attribute13";
    }

    public void setAttribute13(BigDecimal attribute13) {
        this.attribute13 = attribute13;
    }
    @Transient
    private BigDecimal attribute13Mask;

    public BigDecimal getAttribute13Mask() {
        attribute13Mask = attribute13;
        return attribute13Mask;
    }
    public String getAttribute13MaskDD() {
        return "TimeSheetConsolidation_attribute13Mask";
    }

    public void setAttribute13Mask(BigDecimal attribute13Mask) {
        updateDecimalValue("attribute13", attribute13Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute14">
    private BigDecimal attribute14;

    public BigDecimal getAttribute14() {
        return attribute14;
    }
    public String getAttribute14DD() {
        return "TimeSheetConsolidation_attribute14";
    }

    public void setAttribute14(BigDecimal attribute14) {
        this.attribute14 = attribute14;
    }
    @Transient
    private BigDecimal attribute14Mask;

    public BigDecimal getAttribute14Mask() {
        attribute14Mask = attribute14;
        return attribute14Mask;
    }
    public String getAttribute14MaskDD() {
        return "TimeSheetConsolidation_attribute14Mask";
    }

    public void setAttribute14Mask(BigDecimal attribute14Mask) {
        updateDecimalValue("attribute14", attribute14Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute15">
    private BigDecimal attribute15;

    public BigDecimal getAttribute15() {
        return attribute15;
    }
    public String getAttribute15DD() {
        return "TimeSheetConsolidation_attribute15";
    }

    public void setAttribute15(BigDecimal attribute15) {
        this.attribute15 = attribute15;
    }
    @Transient
    private BigDecimal attribute15Mask;

    public BigDecimal getAttribute15Mask() {
        attribute15Mask = attribute15;
        return attribute15Mask;
    }
    public String getAttribute15MaskDD() {
        return "TimeSheetConsolidation_attribute15Mask";
    }

    public void setAttribute15Mask(BigDecimal attribute15Mask) {
        updateDecimalValue("attribute15", attribute15Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute16">
    private BigDecimal attribute16;

    public BigDecimal getAttribute16() {
        return attribute16;
    }
    public String getAttribute16DD() {
        return "TimeSheetConsolidation_attribute16";
    }

    public void setAttribute16(BigDecimal attribute16) {
        this.attribute16 = attribute16;
    }
    @Transient
    private BigDecimal attribute16Mask;

    public BigDecimal getAttribute16Mask() {
        attribute16Mask = attribute16;
        return attribute16Mask;
    }
    public String getAttribute16MaskDD() {
        return "TimeSheetConsolidation_attribute16Mask";
    }

    public void setAttribute16Mask(BigDecimal attribute16Mask) {
        updateDecimalValue("attribute16", attribute16Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute17">
    private BigDecimal attribute17;

    public BigDecimal getAttribute17() {
        return attribute17;
    }
    public String getAttribute17DD() {
        return "TimeSheetConsolidation_attribute17";
    }

    public void setAttribute17(BigDecimal attribute17) {
        this.attribute17 = attribute17;
    }
    @Transient
    private BigDecimal attribute17Mask;

    public BigDecimal getAttribute17Mask() {
        attribute17Mask = attribute17;
        return attribute17Mask;
    }
    public String getAttribute17MaskDD() {
        return "TimeSheetConsolidation_attribute17Mask";
    }

    public void setAttribute17Mask(BigDecimal attribute17Mask) {
        updateDecimalValue("attribute17", attribute17Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute18">
    private BigDecimal attribute18;

    public BigDecimal getAttribute18() {
        return attribute18;
    }
    public String getAttribute18DD() {
        return "TimeSheetConsolidation_attribute18";
    }

    public void setAttribute18(BigDecimal attribute18) {
        this.attribute18 = attribute18;
    }
    @Transient
    private BigDecimal attribute18Mask;

    public BigDecimal getAttribute18Mask() {
        attribute18Mask = attribute18;
        return attribute18Mask;
    }
    public String getAttribute18MaskDD() {
        return "TimeSheetConsolidation_attribute18Mask";
    }

    public void setAttribute18Mask(BigDecimal attribute18Mask) {
        updateDecimalValue("attribute18", attribute18Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute19">
    private BigDecimal attribute19;

    public BigDecimal getAttribute19() {
        return attribute19;
    }
    public String getAttribute19DD() {
        return "TimeSheetConsolidation_attribute19";
    }

    public void setAttribute19(BigDecimal attribute19) {
        this.attribute19 = attribute19;
    }
    @Transient
    private BigDecimal attribute19Mask;

    public BigDecimal getAttribute19Mask() {
        attribute19Mask = attribute19;
        return attribute19Mask;
    }
    public String getAttribute19MaskDD() {
        return "TimeSheetConsolidation_attribute19Mask";
    }

    public void setAttribute19Mask(BigDecimal attribute19Mask) {
        updateDecimalValue("attribute19", attribute19Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute20">
    private BigDecimal attribute20;

    public BigDecimal getAttribute20() {
        return attribute20;
    }
    public String getAttribute20DD() {
        return "TimeSheetConsolidation_attribute20";
    }

    public void setAttribute20(BigDecimal attribute20) {
        this.attribute20 = attribute20;
    }
    @Transient
    private BigDecimal attribute20Mask;

    public BigDecimal getAttribute20Mask() {
        attribute20Mask = attribute20;
        return attribute20Mask;
    }
    public String getAttribute20MaskDD() {
        return "TimeSheetConsolidation_attribute20Mask";
    }

    public void setAttribute20Mask(BigDecimal attribute20Mask) {
        updateDecimalValue("attribute20", attribute20Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute21">
    private BigDecimal attribute21;

    public BigDecimal getAttribute21() {
        return attribute21;
    }
    public String getAttribute21DD() {
        return "TimeSheetConsolidation_attribute21";
    }

    public void setAttribute21(BigDecimal attribute21) {
        this.attribute21 = attribute21;
    }
    @Transient
    private BigDecimal attribute21Mask;

    public BigDecimal getAttribute21Mask() {
        attribute21Mask = attribute21;
        return attribute21Mask;
    }
    public String getAttribute21MaskDD() {
        return "TimeSheetConsolidation_attribute21Mask";
    }

    public void setAttribute21Mask(BigDecimal attribute21Mask) {
        updateDecimalValue("attribute21", attribute21Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute22">
    private BigDecimal attribute22;

    public BigDecimal getAttribute22() {
        return attribute22;
    }
    public String getAttribute22DD() {
        return "TimeSheetConsolidation_attribute22";
    }

    public void setAttribute22(BigDecimal attribute22) {
        this.attribute22 = attribute22;
    }
    @Transient
    private BigDecimal attribute22Mask;

    public BigDecimal getAttribute22Mask() {
        attribute22Mask = attribute22;
        return attribute22Mask;
    }
    public String getAttribute22MaskDD() {
        return "TimeSheetConsolidation_attribute22Mask";
    }

    public void setAttribute22Mask(BigDecimal attribute22Mask) {
        updateDecimalValue("attribute22", attribute22Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute23">
    private BigDecimal attribute23;

    public BigDecimal getAttribute23() {
        return attribute23;
    }
    public String getAttribute23DD() {
        return "TimeSheetConsolidation_attribute23";
    }

    public void setAttribute23(BigDecimal attribute23) {
        this.attribute23 = attribute23;
    }
    @Transient
    private BigDecimal attribute23Mask;

    public BigDecimal getAttribute23Mask() {
        attribute23Mask = attribute23;
        return attribute23Mask;
    }
    public String getAttribute23MaskDD() {
        return "TimeSheetConsolidation_attribute23Mask";
    }

    public void setAttribute23Mask(BigDecimal attribute23Mask) {
        updateDecimalValue("attribute23", attribute23Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute24">
    private BigDecimal attribute24;

    public BigDecimal getAttribute24() {
        return attribute24;
    }
    public String getAttribute24DD() {
        return "TimeSheetConsolidation_attribute24";
    }

    public void setAttribute24(BigDecimal attribute24) {
        this.attribute24 = attribute24;
    }
    @Transient
    private BigDecimal attribute24Mask;

    public BigDecimal getAttribute24Mask() {
        attribute24Mask = attribute24;
        return attribute24Mask;
    }
    public String getAttribute24MaskDD() {
        return "TimeSheetConsolidation_attribute24Mask";
    }

    public void setAttribute24Mask(BigDecimal attribute24Mask) {
        updateDecimalValue("attribute24", attribute24Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute25">
    private BigDecimal attribute25;

    public BigDecimal getAttribute25() {
        return attribute25;
    }
    public String getAttribute25DD() {
        return "TimeSheetConsolidation_attribute25";
    }

    public void setAttribute25(BigDecimal attribute25) {
        this.attribute25 = attribute25;
    }
    @Transient
    private BigDecimal attribute25Mask;

    public BigDecimal getAttribute25Mask() {
        attribute25Mask = attribute25;
        return attribute25Mask;
    }
    public String getAttribute25MaskDD() {
        return "TimeSheetConsolidation_attribute25Mask";
    }

    public void setAttribute25Mask(BigDecimal attribute25Mask) {
        updateDecimalValue("attribute25", attribute25Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute26">
    private BigDecimal attribute26;

    public BigDecimal getAttribute26() {
        return attribute26;
    }
    public String getAttribute26DD() {
        return "TimeSheetConsolidation_attribute26";
    }

    public void setAttribute26(BigDecimal attribute26) {
        this.attribute26 = attribute26;
    }
    @Transient
    private BigDecimal attribute26Mask;

    public BigDecimal getAttribute26Mask() {
        attribute26Mask = attribute26;
        return attribute26Mask;
    }
    public String getAttribute26MaskDD() {
        return "TimeSheetConsolidation_attribute26Mask";
    }

    public void setAttribute26Mask(BigDecimal attribute26Mask) {
        updateDecimalValue("attribute26", attribute26Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute27">
    private BigDecimal attribute27;

    public BigDecimal getAttribute27() {
        return attribute27;
    }
    public String getAttribute27DD() {
        return "TimeSheetConsolidation_attribute27";
    }

    public void setAttribute27(BigDecimal attribute27) {
        this.attribute27 = attribute27;
    }
    @Transient
    private BigDecimal attribute27Mask;

    public BigDecimal getAttribute27Mask() {
        attribute27Mask = attribute27;
        return attribute27Mask;
    }
    public String getAttribute27MaskDD() {
        return "TimeSheetConsolidation_attribute27Mask";
    }

    public void setAttribute27Mask(BigDecimal attribute27Mask) {
        updateDecimalValue("attribute27", attribute27Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute28">
    private BigDecimal attribute28;

    public BigDecimal getAttribute28() {
        return attribute28;
    }
    public String getAttribute28DD() {
        return "TimeSheetConsolidation_attribute28";
    }

    public void setAttribute28(BigDecimal attribute28) {
        this.attribute28 = attribute28;
    }
    @Transient
    private BigDecimal attribute28Mask;

    public BigDecimal getAttribute28Mask() {
        attribute28Mask = attribute28;
        return attribute28Mask;
    }
    public String getAttribute28MaskDD() {
        return "TimeSheetConsolidation_attribute28Mask";
    }

    public void setAttribute28Mask(BigDecimal attribute28Mask) {
        updateDecimalValue("attribute28", attribute28Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute29">
    private BigDecimal attribute29;

    public BigDecimal getAttribute29() {
        return attribute29;
    }
    public String getAttribute29DD() {
        return "TimeSheetConsolidation_attribute29";
    }

    public void setAttribute29(BigDecimal attribute29) {
        this.attribute29 = attribute29;
    }
    @Transient
    private BigDecimal attribute29Mask;

    public BigDecimal getAttribute29Mask() {
        attribute29Mask = attribute29;
        return attribute29Mask;
    }
    public String getAttribute29MaskDD() {
        return "TimeSheetConsolidation_attribute29Mask";
    }

    public void setAttribute29Mask(BigDecimal attribute29Mask) {
        updateDecimalValue("attribute29", attribute29Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute30">
    private BigDecimal attribute30;

    public BigDecimal getAttribute30() {
        return attribute30;
    }
    public String getAttribute30DD() {
        return "TimeSheetConsolidation_attribute30";
    }

    public void setAttribute30(BigDecimal attribute30) {
        this.attribute30 = attribute30;
    }
    @Transient
    private BigDecimal attribute30Mask;

    public BigDecimal getAttribute30Mask() {
        attribute30Mask = attribute30;
        return attribute30Mask;
    }
    public String getAttribute30MaskDD() {
        return "TimeSheetConsolidation_attribute30Mask";
    }

    public void setAttribute30Mask(BigDecimal attribute30Mask) {
        updateDecimalValue("attribute30", attribute30Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute31">
    private BigDecimal attribute31;

    public BigDecimal getAttribute31() {
        return attribute31;
    }
    public String getAttribute31DD() {
        return "TimeSheetConsolidation_attribute31";
    }

    public void setAttribute31(BigDecimal attribute31) {
        this.attribute31 = attribute31;
    }
    @Transient
    private BigDecimal attribute31Mask;

    public BigDecimal getAttribute31Mask() {
        attribute31Mask = attribute31;
        return attribute31Mask;
    }
    public String getAttribute31MaskDD() {
        return "TimeSheetConsolidation_attribute31Mask";
    }

    public void setAttribute31Mask(BigDecimal attribute31Mask) {
        updateDecimalValue("attribute31", attribute31Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute32">
    private BigDecimal attribute32;

    public BigDecimal getAttribute32() {
        return attribute32;
    }
    public String getAttribute32DD() {
        return "TimeSheetConsolidation_attribute32";
    }

    public void setAttribute32(BigDecimal attribute32) {
        this.attribute32 = attribute32;
    }
    @Transient
    private BigDecimal attribute32Mask;

    public BigDecimal getAttribute32Mask() {
        attribute32Mask = attribute32;
        return attribute32Mask;
    }
    public String getAttribute32MaskDD() {
        return "TimeSheetConsolidation_attribute32Mask";
    }

    public void setAttribute32Mask(BigDecimal attribute32Mask) {
        updateDecimalValue("attribute32", attribute32Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute33">
    private BigDecimal attribute33;

    public BigDecimal getAttribute33() {
        return attribute33;
    }
    public String getAttribute33DD() {
        return "TimeSheetConsolidation_attribute33";
    }

    public void setAttribute33(BigDecimal attribute33) {
        this.attribute33 = attribute33;
    }
    @Transient
    private BigDecimal attribute33Mask;

    public BigDecimal getAttribute33Mask() {
        attribute33Mask = attribute33;
        return attribute33Mask;
    }
    public String getAttribute33MaskDD() {
        return "TimeSheetConsolidation_attribute33Mask";
    }

    public void setAttribute33Mask(BigDecimal attribute33Mask) {
        updateDecimalValue("attribute33", attribute33Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute34">
    private BigDecimal attribute34;

    public BigDecimal getAttribute34() {
        return attribute34;
    }
    public String getAttribute34DD() {
        return "TimeSheetConsolidation_attribute34";
    }

    public void setAttribute34(BigDecimal attribute34) {
        this.attribute34 = attribute34;
    }
    @Transient
    private BigDecimal attribute34Mask;

    public BigDecimal getAttribute34Mask() {
        attribute34Mask = attribute34;
        return attribute34Mask;
    }
    public String getAttribute34MaskDD() {
        return "TimeSheetConsolidation_attribute34Mask";
    }

    public void setAttribute34Mask(BigDecimal attribute34Mask) {
        updateDecimalValue("attribute34", attribute34Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute35">
    private BigDecimal attribute35;

    public BigDecimal getAttribute35() {
        return attribute35;
    }
    public String getAttribute35DD() {
        return "TimeSheetConsolidation_attribute35";
    }

    public void setAttribute35(BigDecimal attribute35) {
        this.attribute35 = attribute35;
    }
    @Transient
    private BigDecimal attribute35Mask;

    public BigDecimal getAttribute35Mask() {
        attribute35Mask = attribute35;
        return attribute35Mask;
    }
    public String getAttribute35MaskDD() {
        return "TimeSheetConsolidation_attribute35Mask";
    }

    public void setAttribute35Mask(BigDecimal attribute35Mask) {
        updateDecimalValue("attribute35", attribute35Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute36">
    private BigDecimal attribute36;

    public BigDecimal getAttribute36() {
        return attribute36;
    }
    public String getAttribute36DD() {
        return "TimeSheetConsolidation_attribute36";
    }

    public void setAttribute36(BigDecimal attribute36) {
        this.attribute36 = attribute36;
    }
    @Transient
    private BigDecimal attribute36Mask;

    public BigDecimal getAttribute36Mask() {
        attribute36Mask = attribute36;
        return attribute36Mask;
    }
    public String getAttribute36MaskDD() {
        return "TimeSheetConsolidation_attribute36Mask";
    }

    public void setAttribute36Mask(BigDecimal attribute36Mask) {
        updateDecimalValue("attribute36", attribute36Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute37">
    private BigDecimal attribute37;

    public BigDecimal getAttribute37() {
        return attribute37;
    }
    public String getAttribute37DD() {
        return "TimeSheetConsolidation_attribute37";
    }

    public void setAttribute37(BigDecimal attribute37) {
        this.attribute37 = attribute37;
    }
    @Transient
    private BigDecimal attribute37Mask;

    public BigDecimal getAttribute37Mask() {
        attribute37Mask = attribute37;
        return attribute37Mask;
    }
    public String getAttribute37MaskDD() {
        return "TimeSheetConsolidation_attribute37Mask";
    }

    public void setAttribute37Mask(BigDecimal attribute37Mask) {
        updateDecimalValue("attribute37", attribute37Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute38">
    private BigDecimal attribute38;

    public BigDecimal getAttribute38() {
        return attribute38;
    }
    public String getAttribute38DD() {
        return "TimeSheetConsolidation_attribute38";
    }

    public void setAttribute38(BigDecimal attribute38) {
        this.attribute38 = attribute38;
    }
    @Transient
    private BigDecimal attribute38Mask;

    public BigDecimal getAttribute38Mask() {
        attribute38Mask = attribute38;
        return attribute38Mask;
    }
    public String getAttribute38MaskDD() {
        return "TimeSheetConsolidation_attribute38Mask";
    }

    public void setAttribute38Mask(BigDecimal attribute38Mask) {
        updateDecimalValue("attribute38", attribute38Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute39">
    private BigDecimal attribute39;

    public BigDecimal getAttribute39() {
        return attribute39;
    }
    public String getAttribute39DD() {
        return "TimeSheetConsolidation_attribute39";
    }

    public void setAttribute39(BigDecimal attribute39) {
        this.attribute39 = attribute39;
    }
    @Transient
    private BigDecimal attribute39Mask;

    public BigDecimal getAttribute39Mask() {
        attribute39Mask = attribute39;
        return attribute39Mask;
    }
    public String getAttribute39MaskDD() {
        return "TimeSheetConsolidation_attribute39Mask";
    }

    public void setAttribute39Mask(BigDecimal attribute39Mask) {
        updateDecimalValue("attribute39", attribute39Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute40">
    private BigDecimal attribute40;

    public BigDecimal getAttribute40() {
        return attribute40;
    }
    public String getAttribute40DD() {
        return "TimeSheetConsolidation_attribute40";
    }

    public void setAttribute40(BigDecimal attribute40) {
        this.attribute40 = attribute40;
    }
    @Transient
    private BigDecimal attribute40Mask;

    public BigDecimal getAttribute40Mask() {
        attribute40Mask = attribute40;
        return attribute40Mask;
    }
    public String getAttribute40MaskDD() {
        return "TimeSheetConsolidation_attribute40Mask";
    }

    public void setAttribute40Mask(BigDecimal attribute40Mask) {
        updateDecimalValue("attribute40", attribute40Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute41">
    private BigDecimal attribute41;

    public BigDecimal getAttribute41() {
        return attribute41;
    }
    public String getAttribute41DD() {
        return "TimeSheetConsolidation_attribute41";
    }

    public void setAttribute41(BigDecimal attribute41) {
        this.attribute41 = attribute41;
    }
    @Transient
    private BigDecimal attribute41Mask;

    public BigDecimal getAttribute41Mask() {
        attribute41Mask = attribute41;
        return attribute41Mask;
    }
    public String getAttribute41MaskDD() {
        return "TimeSheetConsolidation_attribute41Mask";
    }

    public void setAttribute41Mask(BigDecimal attribute41Mask) {
        updateDecimalValue("attribute41", attribute41Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute42">
    private BigDecimal attribute42;

    public BigDecimal getAttribute42() {
        return attribute42;
    }
    public String getAttribute42DD() {
        return "TimeSheetConsolidation_attribute42";
    }

    public void setAttribute42(BigDecimal attribute42) {
        this.attribute42 = attribute42;
    }
    @Transient
    private BigDecimal attribute42Mask;

    public BigDecimal getAttribute42Mask() {
        attribute42Mask = attribute42;
        return attribute42Mask;
    }
    public String getAttribute42MaskDD() {
        return "TimeSheetConsolidation_attribute42Mask";
    }

    public void setAttribute42Mask(BigDecimal attribute42Mask) {
        updateDecimalValue("attribute42", attribute42Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute43">
    private BigDecimal attribute43;

    public BigDecimal getAttribute43() {
        return attribute43;
    }
    public String getAttribute43DD() {
        return "TimeSheetConsolidation_attribute43";
    }

    public void setAttribute43(BigDecimal attribute43) {
        this.attribute43 = attribute43;
    }
    @Transient
    private BigDecimal attribute43Mask;

    public BigDecimal getAttribute43Mask() {
        attribute43Mask = attribute43;
        return attribute43Mask;
    }
    public String getAttribute43MaskDD() {
        return "TimeSheetConsolidation_attribute43Mask";
    }

    public void setAttribute43Mask(BigDecimal attribute43Mask) {
        updateDecimalValue("attribute43", attribute43Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute44">
    private BigDecimal attribute44;

    public BigDecimal getAttribute44() {
        return attribute44;
    }
    public String getAttribute44DD() {
        return "TimeSheetConsolidation_attribute44";
    }

    public void setAttribute44(BigDecimal attribute44) {
        this.attribute44 = attribute44;
    }
    @Transient
    private BigDecimal attribute44Mask;

    public BigDecimal getAttribute44Mask() {
        attribute44Mask = attribute44;
        return attribute44Mask;
    }
    public String getAttribute44MaskDD() {
        return "TimeSheetConsolidation_attribute44Mask";
    }

    public void setAttribute44Mask(BigDecimal attribute44Mask) {
        updateDecimalValue("attribute44", attribute44Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute45">
    private BigDecimal attribute45;

    public BigDecimal getAttribute45() {
        return attribute45;
    }
    public String getAttribute45DD() {
        return "TimeSheetConsolidation_attribute45";
    }

    public void setAttribute45(BigDecimal attribute45) {
        this.attribute45 = attribute45;
    }
    @Transient
    private BigDecimal attribute45Mask;

    public BigDecimal getAttribute45Mask() {
        attribute45Mask = attribute45;
        return attribute45Mask;
    }
    public String getAttribute45MaskDD() {
        return "TimeSheetConsolidation_attribute45Mask";
    }

    public void setAttribute45Mask(BigDecimal attribute45Mask) {
        updateDecimalValue("attribute45", attribute45Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute46">
    private BigDecimal attribute46;

    public BigDecimal getAttribute46() {
        return attribute46;
    }
    public String getAttribute46DD() {
        return "TimeSheetConsolidation_attribute46";
    }

    public void setAttribute46(BigDecimal attribute46) {
        this.attribute46 = attribute46;
    }
    @Transient
    private BigDecimal attribute46Mask;

    public BigDecimal getAttribute46Mask() {
        attribute46Mask = attribute46;
        return attribute46Mask;
    }
    public String getAttribute46MaskDD() {
        return "TimeSheetConsolidation_attribute46Mask";
    }

    public void setAttribute46Mask(BigDecimal attribute46Mask) {
        updateDecimalValue("attribute46", attribute46Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute47">
    private BigDecimal attribute47;

    public BigDecimal getAttribute47() {
        return attribute47;
    }
    public String getAttribute47DD() {
        return "TimeSheetConsolidation_attribute47";
    }

    public void setAttribute47(BigDecimal attribute47) {
        this.attribute47 = attribute47;
    }
    @Transient
    private BigDecimal attribute47Mask;

    public BigDecimal getAttribute47Mask() {
        attribute47Mask = attribute47;
        return attribute47Mask;
    }
    public String getAttribute47MaskDD() {
        return "TimeSheetConsolidation_attribute47Mask";
    }

    public void setAttribute47Mask(BigDecimal attribute47Mask) {
        updateDecimalValue("attribute47", attribute47Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute48">
    private BigDecimal attribute48;

    public BigDecimal getAttribute48() {
        return attribute48;
    }
    public String getAttribute48DD() {
        return "TimeSheetConsolidation_attribute48";
    }

    public void setAttribute48(BigDecimal attribute48) {
        this.attribute48 = attribute48;
    }
    @Transient
    private BigDecimal attribute48Mask;

    public BigDecimal getAttribute48Mask() {
        attribute48Mask = attribute48;
        return attribute48Mask;
    }
    public String getAttribute48MaskDD() {
        return "TimeSheetConsolidation_attribute48Mask";
    }

    public void setAttribute48Mask(BigDecimal attribute48Mask) {
        updateDecimalValue("attribute48", attribute48Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute49">
    private BigDecimal attribute49;

    public BigDecimal getAttribute49() {
        return attribute49;
    }
    public String getAttribute49DD() {
        return "TimeSheetConsolidation_attribute49";
    }

    public void setAttribute49(BigDecimal attribute49) {
        this.attribute49 = attribute49;
    }
    @Transient
    private BigDecimal attribute49Mask;

    public BigDecimal getAttribute49Mask() {
        attribute49Mask = attribute49;
        return attribute49Mask;
    }
    public String getAttribute49MaskDD() {
        return "TimeSheetConsolidation_attribute49Mask";
    }

    public void setAttribute49Mask(BigDecimal attribute49Mask) {
        updateDecimalValue("attribute49", attribute49Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute50">
    private BigDecimal attribute50;

    public BigDecimal getAttribute50() {
        return attribute50;
    }
    public String getAttribute50DD() {
        return "TimeSheetConsolidation_attribute50";
    }

    public void setAttribute50(BigDecimal attribute50) {
        this.attribute50 = attribute50;
    }
    @Transient
    private BigDecimal attribute50Mask;

    public BigDecimal getAttribute50Mask() {
        attribute50Mask = attribute50;
        return attribute50Mask;
    }
    public String getAttribute50MaskDD() {
        return "TimeSheetConsolidation_attribute50Mask";
    }

    public void setAttribute50Mask(BigDecimal attribute50Mask) {
        updateDecimalValue("attribute50", attribute50Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute51">
    private BigDecimal attribute51;

    public BigDecimal getAttribute51() {
        return attribute51;
    }
    public String getAttribute51DD() {
        return "TimeSheetConsolidation_attribute51";
    }

    public void setAttribute51(BigDecimal attribute51) {
        this.attribute51 = attribute51;
    }
    @Transient
    private BigDecimal attribute51Mask;

    public BigDecimal getAttribute51Mask() {
        attribute51Mask = attribute51;
        return attribute51Mask;
    }
    public String getAttribute51MaskDD() {
        return "TimeSheetConsolidation_attribute51Mask";
    }

    public void setAttribute51Mask(BigDecimal attribute51Mask) {
        updateDecimalValue("attribute51", attribute51Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute52">
    private BigDecimal attribute52;

    public BigDecimal getAttribute52() {
        return attribute52;
    }
    public String getAttribute52DD() {
        return "TimeSheetConsolidation_attribute52";
    }

    public void setAttribute52(BigDecimal attribute52) {
        this.attribute52 = attribute52;
    }
    @Transient
    private BigDecimal attribute52Mask;

    public BigDecimal getAttribute52Mask() {
        attribute52Mask = attribute52;
        return attribute52Mask;
    }
    public String getAttribute52MaskDD() {
        return "TimeSheetConsolidation_attribute52Mask";
    }

    public void setAttribute52Mask(BigDecimal attribute52Mask) {
        updateDecimalValue("attribute52", attribute52Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute53">
    private BigDecimal attribute53;

    public BigDecimal getAttribute53() {
        return attribute53;
    }
    public String getAttribute53DD() {
        return "TimeSheetConsolidation_attribute53";
    }

    public void setAttribute53(BigDecimal attribute53) {
        this.attribute53 = attribute53;
    }
    @Transient
    private BigDecimal attribute53Mask;

    public BigDecimal getAttribute53Mask() {
        attribute53Mask = attribute53;
        return attribute53Mask;
    }
    public String getAttribute53MaskDD() {
        return "TimeSheetConsolidation_attribute53Mask";
    }

    public void setAttribute53Mask(BigDecimal attribute53Mask) {
        updateDecimalValue("attribute53", attribute53Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute54">
    private BigDecimal attribute54;

    public BigDecimal getAttribute54() {
        return attribute54;
    }
    public String getAttribute54DD() {
        return "TimeSheetConsolidation_attribute54";
    }

    public void setAttribute54(BigDecimal attribute54) {
        this.attribute54 = attribute54;
    }
    @Transient
    private BigDecimal attribute54Mask;

    public BigDecimal getAttribute54Mask() {
        attribute54Mask = attribute54;
        return attribute54Mask;
    }
    public String getAttribute54MaskDD() {
        return "TimeSheetConsolidation_attribute54Mask";
    }

    public void setAttribute54Mask(BigDecimal attribute54Mask) {
        updateDecimalValue("attribute54", attribute54Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute55">
    private BigDecimal attribute55;

    public BigDecimal getAttribute55() {
        return attribute55;
    }
    public String getAttribute55DD() {
        return "TimeSheetConsolidation_attribute55";
    }

    public void setAttribute55(BigDecimal attribute55) {
        this.attribute55 = attribute55;
    }
    @Transient
    private BigDecimal attribute55Mask;

    public BigDecimal getAttribute55Mask() {
        attribute55Mask = attribute55;
        return attribute55Mask;
    }
    public String getAttribute55MaskDD() {
        return "TimeSheetConsolidation_attribute55Mask";
    }

    public void setAttribute55Mask(BigDecimal attribute55Mask) {
        updateDecimalValue("attribute55", attribute55Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute56">
    private BigDecimal attribute56;

    public BigDecimal getAttribute56() {
        return attribute56;
    }
    public String getAttribute56DD() {
        return "TimeSheetConsolidation_attribute56";
    }

    public void setAttribute56(BigDecimal attribute56) {
        this.attribute56 = attribute56;
    }
    @Transient
    private BigDecimal attribute56Mask;

    public BigDecimal getAttribute56Mask() {
        attribute56Mask = attribute56;
        return attribute56Mask;
    }
    public String getAttribute56MaskDD() {
        return "TimeSheetConsolidation_attribute56Mask";
    }

    public void setAttribute56Mask(BigDecimal attribute56Mask) {
        updateDecimalValue("attribute56", attribute56Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute57">
    private BigDecimal attribute57;

    public BigDecimal getAttribute57() {
        return attribute57;
    }
    public String getAttribute57DD() {
        return "TimeSheetConsolidation_attribute57";
    }

    public void setAttribute57(BigDecimal attribute57) {
        this.attribute57 = attribute57;
    }
    @Transient
    private BigDecimal attribute57Mask;

    public BigDecimal getAttribute57Mask() {
        attribute57Mask = attribute57;
        return attribute57Mask;
    }
    public String getAttribute57MaskDD() {
        return "TimeSheetConsolidation_attribute57Mask";
    }

    public void setAttribute57Mask(BigDecimal attribute57Mask) {
        updateDecimalValue("attribute57", attribute57Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute58">
    private BigDecimal attribute58;

    public BigDecimal getAttribute58() {
        return attribute58;
    }
    public String getAttribute58DD() {
        return "TimeSheetConsolidation_attribute58";
    }

    public void setAttribute58(BigDecimal attribute58) {
        this.attribute58 = attribute58;
    }
    @Transient
    private BigDecimal attribute58Mask;

    public BigDecimal getAttribute58Mask() {
        attribute58Mask = attribute58;
        return attribute58Mask;
    }
    public String getAttribute58MaskDD() {
        return "TimeSheetConsolidation_attribute58Mask";
    }

    public void setAttribute58Mask(BigDecimal attribute58Mask) {
        updateDecimalValue("attribute58", attribute58Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute59">
    private BigDecimal attribute59;

    public BigDecimal getAttribute59() {
        return attribute59;
    }
    public String getAttribute59DD() {
        return "TimeSheetConsolidation_attribute59";
    }

    public void setAttribute59(BigDecimal attribute59) {
        this.attribute59 = attribute59;
    }
    @Transient
    private BigDecimal attribute59Mask;

    public BigDecimal getAttribute59Mask() {
        attribute59Mask = attribute59;
        return attribute59Mask;
    }
    public String getAttribute59MaskDD() {
        return "TimeSheetConsolidation_attribute59Mask";
    }

    public void setAttribute59Mask(BigDecimal attribute59Mask) {
        updateDecimalValue("attribute59", attribute59Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute60">
    private BigDecimal attribute60;

    public BigDecimal getAttribute60() {
        return attribute60;
    }
    public String getAttribute60DD() {
        return "TimeSheetConsolidation_attribute60";
    }

    public void setAttribute60(BigDecimal attribute60) {
        this.attribute60 = attribute60;
    }
    @Transient
    private BigDecimal attribute60Mask;

    public BigDecimal getAttribute60Mask() {
        attribute60Mask = attribute60;
        return attribute60Mask;
    }
    public String getAttribute60MaskDD() {
        return "TimeSheetConsolidation_attribute60Mask";
    }

    public void setAttribute60Mask(BigDecimal attribute60Mask) {
        updateDecimalValue("attribute60", attribute60Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute61">
    private BigDecimal attribute61;

    public BigDecimal getAttribute61() {
        return attribute61;
    }
    public String getAttribute61DD() {
        return "TimeSheetConsolidation_attribute61";
    }

    public void setAttribute61(BigDecimal attribute61) {
        this.attribute61 = attribute61;
    }
    @Transient
    private BigDecimal attribute61Mask;

    public BigDecimal getAttribute61Mask() {
        attribute61Mask = attribute61;
        return attribute61Mask;
    }
    public String getAttribute61MaskDD() {
        return "TimeSheetConsolidation_attribute61Mask";
    }

    public void setAttribute61Mask(BigDecimal attribute61Mask) {
        updateDecimalValue("attribute61", attribute61Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute62">
    private BigDecimal attribute62;

    public BigDecimal getAttribute62() {
        return attribute62;
    }
    public String getAttribute62DD() {
        return "TimeSheetConsolidation_attribute62";
    }

    public void setAttribute62(BigDecimal attribute62) {
        this.attribute62 = attribute62;
    }
    @Transient
    private BigDecimal attribute62Mask;

    public BigDecimal getAttribute62Mask() {
        attribute62Mask = attribute62;
        return attribute62Mask;
    }
    public String getAttribute62MaskDD() {
        return "TimeSheetConsolidation_attribute62Mask";
    }

    public void setAttribute62Mask(BigDecimal attribute62Mask) {
        updateDecimalValue("attribute62", attribute62Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute63">
    private BigDecimal attribute63;

    public BigDecimal getAttribute63() {
        return attribute63;
    }
    public String getAttribute63DD() {
        return "TimeSheetConsolidation_attribute63";
    }

    public void setAttribute63(BigDecimal attribute63) {
        this.attribute63 = attribute63;
    }
    @Transient
    private BigDecimal attribute63Mask;

    public BigDecimal getAttribute63Mask() {
        attribute63Mask = attribute63;
        return attribute63Mask;
    }
    public String getAttribute63MaskDD() {
        return "TimeSheetConsolidation_attribute63Mask";
    }

    public void setAttribute63Mask(BigDecimal attribute63Mask) {
        updateDecimalValue("attribute63", attribute63Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute64">
    private BigDecimal attribute64;

    public BigDecimal getAttribute64() {
        return attribute64;
    }
    public String getAttribute64DD() {
        return "TimeSheetConsolidation_attribute64";
    }

    public void setAttribute64(BigDecimal attribute64) {
        this.attribute64 = attribute64;
    }
    @Transient
    private BigDecimal attribute64Mask;

    public BigDecimal getAttribute64Mask() {
        attribute64Mask = attribute64;
        return attribute64Mask;
    }
    public String getAttribute64MaskDD() {
        return "TimeSheetConsolidation_attribute64Mask";
    }

    public void setAttribute64Mask(BigDecimal attribute64Mask) {
        updateDecimalValue("attribute64", attribute64Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute65">
    private BigDecimal attribute65;

    public BigDecimal getAttribute65() {
        return attribute65;
    }
    public String getAttribute65DD() {
        return "TimeSheetConsolidation_attribute65";
    }

    public void setAttribute65(BigDecimal attribute65) {
        this.attribute65 = attribute65;
    }
    @Transient
    private BigDecimal attribute65Mask;

    public BigDecimal getAttribute65Mask() {
        attribute65Mask = attribute65;
        return attribute65Mask;
    }
    public String getAttribute65MaskDD() {
        return "TimeSheetConsolidation_attribute65Mask";
    }

    public void setAttribute65Mask(BigDecimal attribute65Mask) {
        updateDecimalValue("attribute65", attribute65Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute66">
    private BigDecimal attribute66;

    public BigDecimal getAttribute66() {
        return attribute66;
    }
    public String getAttribute66DD() {
        return "TimeSheetConsolidation_attribute66";
    }

    public void setAttribute66(BigDecimal attribute66) {
        this.attribute66 = attribute66;
    }
    @Transient
    private BigDecimal attribute66Mask;

    public BigDecimal getAttribute66Mask() {
        attribute66Mask = attribute66;
        return attribute66Mask;
    }
    public String getAttribute66MaskDD() {
        return "TimeSheetConsolidation_attribute66Mask";
    }

    public void setAttribute66Mask(BigDecimal attribute66Mask) {
        updateDecimalValue("attribute66", attribute66Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute67">
    private BigDecimal attribute67;

    public BigDecimal getAttribute67() {
        return attribute67;
    }
    public String getAttribute67DD() {
        return "TimeSheetConsolidation_attribute67";
    }

    public void setAttribute67(BigDecimal attribute67) {
        this.attribute67 = attribute67;
    }
    @Transient
    private BigDecimal attribute67Mask;

    public BigDecimal getAttribute67Mask() {
        attribute67Mask = attribute67;
        return attribute67Mask;
    }
    public String getAttribute67MaskDD() {
        return "TimeSheetConsolidation_attribute67Mask";
    }

    public void setAttribute67Mask(BigDecimal attribute67Mask) {
        updateDecimalValue("attribute67", attribute67Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute68">
    private BigDecimal attribute68;

    public BigDecimal getAttribute68() {
        return attribute68;
    }
    public String getAttribute68DD() {
        return "TimeSheetConsolidation_attribute68";
    }

    public void setAttribute68(BigDecimal attribute68) {
        this.attribute68 = attribute68;
    }
    @Transient
    private BigDecimal attribute68Mask;

    public BigDecimal getAttribute68Mask() {
        attribute68Mask = attribute68;
        return attribute68Mask;
    }
    public String getAttribute68MaskDD() {
        return "TimeSheetConsolidation_attribute68Mask";
    }

    public void setAttribute68Mask(BigDecimal attribute68Mask) {
        updateDecimalValue("attribute68", attribute68Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute69">
    private BigDecimal attribute69;

    public BigDecimal getAttribute69() {
        return attribute69;
    }
    public String getAttribute69DD() {
        return "TimeSheetConsolidation_attribute69";
    }

    public void setAttribute69(BigDecimal attribute69) {
        this.attribute69 = attribute69;
    }
    @Transient
    private BigDecimal attribute69Mask;

    public BigDecimal getAttribute69Mask() {
        attribute69Mask = attribute69;
        return attribute69Mask;
    }
    public String getAttribute69MaskDD() {
        return "TimeSheetConsolidation_attribute69Mask";
    }

    public void setAttribute69Mask(BigDecimal attribute69Mask) {
        updateDecimalValue("attribute69", attribute69Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute70">
    private BigDecimal attribute70;

    public BigDecimal getAttribute70() {
        return attribute70;
    }
    public String getAttribute70DD() {
        return "TimeSheetConsolidation_attribute70";
    }

    public void setAttribute70(BigDecimal attribute70) {
        this.attribute70 = attribute70;
    }
    @Transient
    private BigDecimal attribute70Mask;

    public BigDecimal getAttribute70Mask() {
        attribute70Mask = attribute70;
        return attribute70Mask;
    }
    public String getAttribute70MaskDD() {
        return "TimeSheetConsolidation_attribute70Mask";
    }

    public void setAttribute70Mask(BigDecimal attribute70Mask) {
        updateDecimalValue("attribute70", attribute70Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute71">
    private BigDecimal attribute71;

    public BigDecimal getAttribute71() {
        return attribute71;
    }
    public String getAttribute71DD() {
        return "TimeSheetConsolidation_attribute71";
    }

    public void setAttribute71(BigDecimal attribute71) {
        this.attribute71 = attribute71;
    }
    @Transient
    private BigDecimal attribute71Mask;

    public BigDecimal getAttribute71Mask() {
        attribute71Mask = attribute71;
        return attribute71Mask;
    }
    public String getAttribute71MaskDD() {
        return "TimeSheetConsolidation_attribute71Mask";
    }

    public void setAttribute71Mask(BigDecimal attribute71Mask) {
        updateDecimalValue("attribute71", attribute71Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute72">
    private BigDecimal attribute72;

    public BigDecimal getAttribute72() {
        return attribute72;
    }
    public String getAttribute72DD() {
        return "TimeSheetConsolidation_attribute72";
    }

    public void setAttribute72(BigDecimal attribute72) {
        this.attribute72 = attribute72;
    }
    @Transient
    private BigDecimal attribute72Mask;

    public BigDecimal getAttribute72Mask() {
        attribute72Mask = attribute72;
        return attribute72Mask;
    }
    public String getAttribute72MaskDD() {
        return "TimeSheetConsolidation_attribute72Mask";
    }

    public void setAttribute72Mask(BigDecimal attribute72Mask) {
        updateDecimalValue("attribute72", attribute72Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute73">
    private BigDecimal attribute73;

    public BigDecimal getAttribute73() {
        return attribute73;
    }
    public String getAttribute73DD() {
        return "TimeSheetConsolidation_attribute73";
    }

    public void setAttribute73(BigDecimal attribute73) {
        this.attribute73 = attribute73;
    }
    @Transient
    private BigDecimal attribute73Mask;

    public BigDecimal getAttribute73Mask() {
        attribute73Mask = attribute73;
        return attribute73Mask;
    }
    public String getAttribute73MaskDD() {
        return "TimeSheetConsolidation_attribute73Mask";
    }

    public void setAttribute73Mask(BigDecimal attribute73Mask) {
        updateDecimalValue("attribute73", attribute73Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute74">
    private BigDecimal attribute74;

    public BigDecimal getAttribute74() {
        return attribute74;
    }
    public String getAttribute74DD() {
        return "TimeSheetConsolidation_attribute74";
    }

    public void setAttribute74(BigDecimal attribute74) {
        this.attribute74 = attribute74;
    }
    @Transient
    private BigDecimal attribute74Mask;

    public BigDecimal getAttribute74Mask() {
        attribute74Mask = attribute74;
        return attribute74Mask;
    }
    public String getAttribute74MaskDD() {
        return "TimeSheetConsolidation_attribute74Mask";
    }

    public void setAttribute74Mask(BigDecimal attribute74Mask) {
        updateDecimalValue("attribute74", attribute74Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute75">
    private BigDecimal attribute75;

    public BigDecimal getAttribute75() {
        return attribute75;
    }
    public String getAttribute75DD() {
        return "TimeSheetConsolidation_attribute75";
    }

    public void setAttribute75(BigDecimal attribute75) {
        this.attribute75 = attribute75;
    }
    @Transient
    private BigDecimal attribute75Mask;

    public BigDecimal getAttribute75Mask() {
        attribute75Mask = attribute75;
        return attribute75Mask;
    }
    public String getAttribute75MaskDD() {
        return "TimeSheetConsolidation_attribute75Mask";
    }

    public void setAttribute75Mask(BigDecimal attribute75Mask) {
        updateDecimalValue("attribute75", attribute75Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute76">
    private BigDecimal attribute76;

    public BigDecimal getAttribute76() {
        return attribute76;
    }
    public String getAttribute76DD() {
        return "TimeSheetConsolidation_attribute76";
    }

    public void setAttribute76(BigDecimal attribute76) {
        this.attribute76 = attribute76;
    }
    @Transient
    private BigDecimal attribute76Mask;

    public BigDecimal getAttribute76Mask() {
        attribute76Mask = attribute76;
        return attribute76Mask;
    }
    public String getAttribute76MaskDD() {
        return "TimeSheetConsolidation_attribute76Mask";
    }

    public void setAttribute76Mask(BigDecimal attribute76Mask) {
        updateDecimalValue("attribute76", attribute76Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute77">
    private BigDecimal attribute77;

    public BigDecimal getAttribute77() {
        return attribute77;
    }
    public String getAttribute77DD() {
        return "TimeSheetConsolidation_attribute77";
    }

    public void setAttribute77(BigDecimal attribute77) {
        this.attribute77 = attribute77;
    }
    @Transient
    private BigDecimal attribute77Mask;

    public BigDecimal getAttribute77Mask() {
        attribute77Mask = attribute77;
        return attribute77Mask;
    }
    public String getAttribute77MaskDD() {
        return "TimeSheetConsolidation_attribute77Mask";
    }

    public void setAttribute77Mask(BigDecimal attribute77Mask) {
        updateDecimalValue("attribute77", attribute77Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute78">
    private BigDecimal attribute78;

    public BigDecimal getAttribute78() {
        return attribute78;
    }
    public String getAttribute78DD() {
        return "TimeSheetConsolidation_attribute78";
    }

    public void setAttribute78(BigDecimal attribute78) {
        this.attribute78 = attribute78;
    }
    @Transient
    private BigDecimal attribute78Mask;

    public BigDecimal getAttribute78Mask() {
        attribute78Mask = attribute78;
        return attribute78Mask;
    }
    public String getAttribute78MaskDD() {
        return "TimeSheetConsolidation_attribute78Mask";
    }

    public void setAttribute78Mask(BigDecimal attribute78Mask) {
        updateDecimalValue("attribute78", attribute78Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute79">
    private BigDecimal attribute79;

    public BigDecimal getAttribute79() {
        return attribute79;
    }
    public String getAttribute79DD() {
        return "TimeSheetConsolidation_attribute79";
    }

    public void setAttribute79(BigDecimal attribute79) {
        this.attribute79 = attribute79;
    }
    @Transient
    private BigDecimal attribute79Mask;

    public BigDecimal getAttribute79Mask() {
        attribute79Mask = attribute79;
        return attribute79Mask;
    }
    public String getAttribute79MaskDD() {
        return "TimeSheetConsolidation_attribute79Mask";
    }

    public void setAttribute79Mask(BigDecimal attribute79Mask) {
        updateDecimalValue("attribute79", attribute79Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute80">
    private BigDecimal attribute80;

    public BigDecimal getAttribute80() {
        return attribute80;
    }
    public String getAttribute80DD() {
        return "TimeSheetConsolidation_attribute80";
    }

    public void setAttribute80(BigDecimal attribute80) {
        this.attribute80 = attribute80;
    }
    @Transient
    private BigDecimal attribute80Mask;

    public BigDecimal getAttribute80Mask() {
        attribute80Mask = attribute80;
        return attribute80Mask;
    }
    public String getAttribute80MaskDD() {
        return "TimeSheetConsolidation_attribute80Mask";
    }

    public void setAttribute80Mask(BigDecimal attribute80Mask) {
        updateDecimalValue("attribute80", attribute80Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute81">
    private BigDecimal attribute81;

    public BigDecimal getAttribute81() {
        return attribute81;
    }
    public String getAttribute81DD() {
        return "TimeSheetConsolidation_attribute81";
    }

    public void setAttribute81(BigDecimal attribute81) {
        this.attribute81 = attribute81;
    }
    @Transient
    private BigDecimal attribute81Mask;

    public BigDecimal getAttribute81Mask() {
        attribute81Mask = attribute81;
        return attribute81Mask;
    }
    public String getAttribute81MaskDD() {
        return "TimeSheetConsolidation_attribute81Mask";
    }

    public void setAttribute81Mask(BigDecimal attribute81Mask) {
        updateDecimalValue("attribute81", attribute81Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute82">
    private BigDecimal attribute82;

    public BigDecimal getAttribute82() {
        return attribute82;
    }
    public String getAttribute82DD() {
        return "TimeSheetConsolidation_attribute82";
    }

    public void setAttribute82(BigDecimal attribute82) {
        this.attribute82 = attribute82;
    }
    @Transient
    private BigDecimal attribute82Mask;

    public BigDecimal getAttribute82Mask() {
        attribute82Mask = attribute82;
        return attribute82Mask;
    }
    public String getAttribute82MaskDD() {
        return "TimeSheetConsolidation_attribute82Mask";
    }

    public void setAttribute82Mask(BigDecimal attribute82Mask) {
        updateDecimalValue("attribute82", attribute82Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute83">
    private BigDecimal attribute83;

    public BigDecimal getAttribute83() {
        return attribute83;
    }
    public String getAttribute83DD() {
        return "TimeSheetConsolidation_attribute83";
    }

    public void setAttribute83(BigDecimal attribute83) {
        this.attribute83 = attribute83;
    }
    @Transient
    private BigDecimal attribute83Mask;

    public BigDecimal getAttribute83Mask() {
        attribute83Mask = attribute83;
        return attribute83Mask;
    }
    public String getAttribute83MaskDD() {
        return "TimeSheetConsolidation_attribute83Mask";
    }

    public void setAttribute83Mask(BigDecimal attribute83Mask) {
        updateDecimalValue("attribute83", attribute83Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute84">
    private BigDecimal attribute84;

    public BigDecimal getAttribute84() {
        return attribute84;
    }
    public String getAttribute84DD() {
        return "TimeSheetConsolidation_attribute84";
    }

    public void setAttribute84(BigDecimal attribute84) {
        this.attribute84 = attribute84;
    }
    @Transient
    private BigDecimal attribute84Mask;

    public BigDecimal getAttribute84Mask() {
        attribute84Mask = attribute84;
        return attribute84Mask;
    }
    public String getAttribute84MaskDD() {
        return "TimeSheetConsolidation_attribute84Mask";
    }

    public void setAttribute84Mask(BigDecimal attribute84Mask) {
        updateDecimalValue("attribute84", attribute84Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute85">
    private BigDecimal attribute85;

    public BigDecimal getAttribute85() {
        return attribute85;
    }
    public String getAttribute85DD() {
        return "TimeSheetConsolidation_attribute85";
    }

    public void setAttribute85(BigDecimal attribute85) {
        this.attribute85 = attribute85;
    }
    @Transient
    private BigDecimal attribute85Mask;

    public BigDecimal getAttribute85Mask() {
        attribute85Mask = attribute85;
        return attribute85Mask;
    }
    public String getAttribute85MaskDD() {
        return "TimeSheetConsolidation_attribute85Mask";
    }

    public void setAttribute85Mask(BigDecimal attribute85Mask) {
        updateDecimalValue("attribute85", attribute85Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute86">
    private BigDecimal attribute86;

    public BigDecimal getAttribute86() {
        return attribute86;
    }
    public String getAttribute86DD() {
        return "TimeSheetConsolidation_attribute86";
    }

    public void setAttribute86(BigDecimal attribute86) {
        this.attribute86 = attribute86;
    }
    @Transient
    private BigDecimal attribute86Mask;

    public BigDecimal getAttribute86Mask() {
        attribute86Mask = attribute86;
        return attribute86Mask;
    }
    public String getAttribute86MaskDD() {
        return "TimeSheetConsolidation_attribute86Mask";
    }

    public void setAttribute86Mask(BigDecimal attribute86Mask) {
        updateDecimalValue("attribute86", attribute86Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute87">
    private BigDecimal attribute87;

    public BigDecimal getAttribute87() {
        return attribute87;
    }
    public String getAttribute87DD() {
        return "TimeSheetConsolidation_attribute87";
    }

    public void setAttribute87(BigDecimal attribute87) {
        this.attribute87 = attribute87;
    }
    @Transient
    private BigDecimal attribute87Mask;

    public BigDecimal getAttribute87Mask() {
        attribute87Mask = attribute87;
        return attribute87Mask;
    }
    public String getAttribute87MaskDD() {
        return "TimeSheetConsolidation_attribute87Mask";
    }

    public void setAttribute87Mask(BigDecimal attribute87Mask) {
        updateDecimalValue("attribute87", attribute87Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute88">
    private BigDecimal attribute88;

    public BigDecimal getAttribute88() {
        return attribute88;
    }
    public String getAttribute88DD() {
        return "TimeSheetConsolidation_attribute88";
    }

    public void setAttribute88(BigDecimal attribute88) {
        this.attribute88 = attribute88;
    }
    @Transient
    private BigDecimal attribute88Mask;

    public BigDecimal getAttribute88Mask() {
        attribute88Mask = attribute88;
        return attribute88Mask;
    }
    public String getAttribute88MaskDD() {
        return "TimeSheetConsolidation_attribute88Mask";
    }

    public void setAttribute88Mask(BigDecimal attribute88Mask) {
        updateDecimalValue("attribute88", attribute88Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute89">
    private BigDecimal attribute89;

    public BigDecimal getAttribute89() {
        return attribute89;
    }
    public String getAttribute89DD() {
        return "TimeSheetConsolidation_attribute89";
    }

    public void setAttribute89(BigDecimal attribute89) {
        this.attribute89 = attribute89;
    }
    @Transient
    private BigDecimal attribute89Mask;

    public BigDecimal getAttribute89Mask() {
        attribute89Mask = attribute89;
        return attribute89Mask;
    }
    public String getAttribute89MaskDD() {
        return "TimeSheetConsolidation_attribute89Mask";
    }

    public void setAttribute89Mask(BigDecimal attribute89Mask) {
        updateDecimalValue("attribute89", attribute89Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute90">
    private BigDecimal attribute90;

    public BigDecimal getAttribute90() {
        return attribute90;
    }
    public String getAttribute90DD() {
        return "TimeSheetConsolidation_attribute90";
    }

    public void setAttribute90(BigDecimal attribute90) {
        this.attribute90 = attribute90;
    }
    @Transient
    private BigDecimal attribute90Mask;

    public BigDecimal getAttribute90Mask() {
        attribute90Mask = attribute90;
        return attribute90Mask;
    }
    public String getAttribute90MaskDD() {
        return "TimeSheetConsolidation_attribute90Mask";
    }

    public void setAttribute90Mask(BigDecimal attribute90Mask) {
        updateDecimalValue("attribute90", attribute90Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute91">
    private BigDecimal attribute91;

    public BigDecimal getAttribute91() {
        return attribute91;
    }
    public String getAttribute91DD() {
        return "TimeSheetConsolidation_attribute91";
    }

    public void setAttribute91(BigDecimal attribute91) {
        this.attribute91 = attribute91;
    }
    @Transient
    private BigDecimal attribute91Mask;

    public BigDecimal getAttribute91Mask() {
        attribute91Mask = attribute91;
        return attribute91Mask;
    }
    public String getAttribute91MaskDD() {
        return "TimeSheetConsolidation_attribute91Mask";
    }

    public void setAttribute91Mask(BigDecimal attribute91Mask) {
        updateDecimalValue("attribute91", attribute91Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute92">
    private BigDecimal attribute92;

    public BigDecimal getAttribute92() {
        return attribute92;
    }
    public String getAttribute92DD() {
        return "TimeSheetConsolidation_attribute92";
    }

    public void setAttribute92(BigDecimal attribute92) {
        this.attribute92 = attribute92;
    }
    @Transient
    private BigDecimal attribute92Mask;

    public BigDecimal getAttribute92Mask() {
        attribute92Mask = attribute92;
        return attribute92Mask;
    }
    public String getAttribute92MaskDD() {
        return "TimeSheetConsolidation_attribute92Mask";
    }

    public void setAttribute92Mask(BigDecimal attribute92Mask) {
        updateDecimalValue("attribute92", attribute92Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute93">
    private BigDecimal attribute93;

    public BigDecimal getAttribute93() {
        return attribute93;
    }
    public String getAttribute93DD() {
        return "TimeSheetConsolidation_attribute93";
    }

    public void setAttribute93(BigDecimal attribute93) {
        this.attribute93 = attribute93;
    }
    @Transient
    private BigDecimal attribute93Mask;

    public BigDecimal getAttribute93Mask() {
        attribute93Mask = attribute93;
        return attribute93Mask;
    }
    public String getAttribute93MaskDD() {
        return "TimeSheetConsolidation_attribute93Mask";
    }

    public void setAttribute93Mask(BigDecimal attribute93Mask) {
        updateDecimalValue("attribute93", attribute93Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute94">
    private BigDecimal attribute94;

    public BigDecimal getAttribute94() {
        return attribute94;
    }
    public String getAttribute94DD() {
        return "TimeSheetConsolidation_attribute94";
    }

    public void setAttribute94(BigDecimal attribute94) {
        this.attribute94 = attribute94;
    }
    @Transient
    private BigDecimal attribute94Mask;

    public BigDecimal getAttribute94Mask() {
        attribute94Mask = attribute94;
        return attribute94Mask;
    }
    public String getAttribute94MaskDD() {
        return "TimeSheetConsolidation_attribute94Mask";
    }

    public void setAttribute94Mask(BigDecimal attribute94Mask) {
        updateDecimalValue("attribute94", attribute94Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute95">
    private BigDecimal attribute95;

    public BigDecimal getAttribute95() {
        return attribute95;
    }
    public String getAttribute95DD() {
        return "TimeSheetConsolidation_attribute95";
    }

    public void setAttribute95(BigDecimal attribute95) {
        this.attribute95 = attribute95;
    }
    @Transient
    private BigDecimal attribute95Mask;

    public BigDecimal getAttribute95Mask() {
        attribute95Mask = attribute95;
        return attribute95Mask;
    }
    public String getAttribute95MaskDD() {
        return "TimeSheetConsolidation_attribute95Mask";
    }

    public void setAttribute95Mask(BigDecimal attribute95Mask) {
        updateDecimalValue("attribute95", attribute95Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute96">
    private BigDecimal attribute96;

    public BigDecimal getAttribute96() {
        return attribute96;
    }
    public String getAttribute96DD() {
        return "TimeSheetConsolidation_attribute96";
    }

    public void setAttribute96(BigDecimal attribute96) {
        this.attribute96 = attribute96;
    }
    @Transient
    private BigDecimal attribute96Mask;

    public BigDecimal getAttribute96Mask() {
        attribute96Mask = attribute96;
        return attribute96Mask;
    }
    public String getAttribute96MaskDD() {
        return "TimeSheetConsolidation_attribute96Mask";
    }

    public void setAttribute96Mask(BigDecimal attribute96Mask) {
        updateDecimalValue("attribute96", attribute96Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute97">
    private BigDecimal attribute97;

    public BigDecimal getAttribute97() {
        return attribute97;
    }
    public String getAttribute97DD() {
        return "TimeSheetConsolidation_attribute97";
    }

    public void setAttribute97(BigDecimal attribute97) {
        this.attribute97 = attribute97;
    }
    @Transient
    private BigDecimal attribute97Mask;

    public BigDecimal getAttribute97Mask() {
        attribute97Mask = attribute97;
        return attribute97Mask;
    }
    public String getAttribute97MaskDD() {
        return "TimeSheetConsolidation_attribute97Mask";
    }

    public void setAttribute97Mask(BigDecimal attribute97Mask) {
        updateDecimalValue("attribute97", attribute97Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute98">
    private BigDecimal attribute98;

    public BigDecimal getAttribute98() {
        return attribute98;
    }
    public String getAttribute98DD() {
        return "TimeSheetConsolidation_attribute98";
    }

    public void setAttribute98(BigDecimal attribute98) {
        this.attribute98 = attribute98;
    }
    @Transient
    private BigDecimal attribute98Mask;

    public BigDecimal getAttribute98Mask() {
        attribute98Mask = attribute98;
        return attribute98Mask;
    }
    public String getAttribute98MaskDD() {
        return "TimeSheetConsolidation_attribute98Mask";
    }

    public void setAttribute98Mask(BigDecimal attribute98Mask) {
        updateDecimalValue("attribute98", attribute98Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute99">
    private BigDecimal attribute99;

    public BigDecimal getAttribute99() {
        return attribute99;
    }
    public String getAttribute99DD() {
        return "TimeSheetConsolidation_attribute99";
    }

    public void setAttribute99(BigDecimal attribute99) {
        this.attribute99 = attribute99;
    }
    @Transient
    private BigDecimal attribute99Mask;

    public BigDecimal getAttribute99Mask() {
        attribute99Mask = attribute99;
        return attribute99Mask;
    }
    public String getAttribute99MaskDD() {
        return "TimeSheetConsolidation_attribute99Mask";
    }

    public void setAttribute99Mask(BigDecimal attribute99Mask) {
        updateDecimalValue("attribute99", attribute99Mask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="attribute100">
    private BigDecimal attribute100;

    public BigDecimal getAttribute100() {
        return attribute100;
    }
    public String getAttribute100DD() {
        return "TimeSheetConsolidation_attribute100";
    }

    public void setAttribute100(BigDecimal attribute100) {
        this.attribute100 = attribute100;
    }
    @Transient
    private BigDecimal attribute100Mask;

    public BigDecimal getAttribute100Mask() {
        attribute100Mask = attribute100;
        return attribute100Mask;
    }
    public String getAttribute100MaskDD() {
        return "TimeSheetConsolidation_attribute100Mask";
    }

    public void setAttribute100Mask(BigDecimal attribute100Mask) {
        updateDecimalValue("attribute100", attribute100Mask);
    }
    //</editor-fold>
}