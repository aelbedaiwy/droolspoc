/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="repemployeeiqama")
public class RepEmployeeIqama extends RepEmployeeBase {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeIqama_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeIqama_genderDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepEmployeeIqama_employeeActive";
    }
    
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeeIqama_employeeActive";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="renewalDate">
     @Column
     @Temporal(javax.persistence.TemporalType.DATE)
     private Date renewalDate;

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewalDateDD() {
        return "RepEmployeeIqama_renewalDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="entrancePort">
    @Column
    private String entrancePort;

    public String getEntrancePort() {
        return entrancePort;
    }

    public void setEntrancePort(String entrancePort) {
        this.entrancePort = entrancePort;
    }

    public String getEntrancePortDD() {
        return "RepEmployeeIqama_entrancePort";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="expiryDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expiryDate;

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getExpiryDateDD() {
        return "RepEmployeeIqama_expiryDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="iqamaNumber">
    @Column
    private String iqamaNumber;

    public String getIqamaNumber() {
        return iqamaNumber;
    }

    public void setIqamaNumber(String iqamaNumber) {
        this.iqamaNumber = iqamaNumber;
    }

    public String getIqamaNumberDD() {
        return "RepEmployeeIqama_iqamaNumber";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="entranceDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date entranceDate;

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public String getEntranceDateDD() {
        return "RepEmployeeIqama_entranceDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="requestNotes">
    @Column
    private String requestNotes;

    public String getRequestNotes() {
        return requestNotes;
    }

    public void setRequestNotes(String requestNotes) {
        this.requestNotes = requestNotes;
    }

    public String getRequestNotesDD() {
        return "RepEmployeeIqama_requestNotes";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="issueDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date issueDate;

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueDateDD() {
        return "RepEmployeeIqama_issueDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="remarks">
    @Column
    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarksDD() {
        return "RepEmployeeIqama_remarks";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="expiryDateHijri">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expiryDateHijri;

    public Date getExpiryDateHijri() {
        return expiryDateHijri;
    }

    public void setExpiryDateHijri(Date expiryDateHijri) {
        this.expiryDateHijri = expiryDateHijri;
    }

    public String getExpiryDateHijriDD() {
        return "RepEmployeeIqama_expiryDateHijri";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="passportAddress">
    @Column
    private String passportAddress;

    public String getPassportAddress() {
        return passportAddress;
    }

    public void setPassportAddress(String passportAddress) {
        this.passportAddress = passportAddress;
    }

    public String getPassportAddressDD() {
        return "RepEmployeeIqama_passportAddress";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="changeDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date changeDate;

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public String getChangeDateDD() {
        return "RepEmployeeIqama_changeDate";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="passport">
    @Column
    private String passport;

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }
    
    public String getPassportDD() {
        return "RepEmployeeIqama_passport";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="labourOfficeNumber">
    @Column
    private String labourOfficeNumber;

    public String getLabourOfficeNumber() {
        return labourOfficeNumber;
    }

    public void setLabourOfficeNumber(String labourOfficeNumber) {
        this.labourOfficeNumber = labourOfficeNumber;
    }

    public String getLabourOfficeNumberDD() {
        return "RepEmployeeIqama_labourOfficeNumber";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="visible">
    @Column
    private String visible;

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getVisibleDD() {
        return "RepEmployeeIqama_visible";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="issueDateHijri">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date issueDateHijri;

    public Date getIssueDateHijri() {
        return issueDateHijri;
    }

    public void setIssueDateHijri(Date issueDateHijri) {
        this.issueDateHijri = issueDateHijri;
    }

    public String getIssueDateHijriDD() {
        return "RepEmployeeIqama_issueDateHijri";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="renewalDateHijri">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date renewalDateHijri;

    public Date getRenewalDateHijri() {
        return renewalDateHijri;
    }

    public void setRenewalDateHijri(Date renewalDateHijri) {
        this.renewalDateHijri = renewalDateHijri;
    }

    public String getRenewalDateHijriDD() {
        return "RepEmployeeIqama_renewalDateHijri";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="location">
    @Column
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "RepEmployeeIqama_location";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="status">
    @Column
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "RepEmployeeIqama_status";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="iqamaJobFamily">
    @Column
    @Translatable(translationField = "iqamaJobFamilyTranslated")
    private String iqamaJobFamily;

    public String getIqamaJobFamily() {
        return iqamaJobFamily;
    }

    public void setIqamaJobFamily(String iqamaJobFamily) {
        this.iqamaJobFamily = iqamaJobFamily;
    }

    public String getIqamaJobFamilyDD() {
        return "RepEmployeeIqama_iqamaJobFamily";
    }
    
    @Transient
    @Translation(originalField = "iqamaJobFamily")
    private String iqamaJobFamilyTranslated;

    public String getIqamaJobFamilyTranslated() {
        return iqamaJobFamilyTranslated;
    }

    public void setIqamaJobFamilyTranslated(String iqamaJobFamilyTranslated) {
        this.iqamaJobFamilyTranslated = iqamaJobFamilyTranslated;
    }
    
    public String getIqamaJobFamilyTranslatedDD() {
        return "RepEmployeeIqama_iqamaJobFamily";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="iqamaJob">
    @Translatable(translationField = "iqamaJobTranslated")
    private String iqamaJob;

    public String getIqamaJob() {
        return iqamaJob;
    }

    public void setIqamaJob(String iqamaJob) {
        this.iqamaJob = iqamaJob;
    }

    public String getIqamaJobDD() {
        return "RepEmployeeIqama_iqamaJob";
    }
    
    @Transient
    @Translation(originalField = "iqamaJob")
    private String iqamaJobTranslated;

    public String getIqamaJobTranslated() {
        return iqamaJobTranslated;
    }

    public void setIqamaJobTranslated(String iqamaJobTranslated) {
        this.iqamaJobTranslated = iqamaJobTranslated;
    }
    
    public String getIqamaJobTranslatedDD() {
        return "RepEmployeeIqama_iqamaJob";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="followupBy">
    @Column
    @Translatable(translationField = "followupByTranslated")
    private String followupBy;

    public String getFollowupBy() {
        return followupBy;
    }

    public void setFollowupBy(String followupBy) {
        this.followupBy = followupBy;
    }

    public String getFollowupByDD() {
        return "RepEmployeeIqama_followupBy";
    }

    @Transient
    @Translation(originalField = "followupBy")
    private String followupByTranslated;

    public String getFollowupByTranslated() {
        return followupByTranslated;
    }

    public void setFollowupByTranslated(String followupByTranslated) {
        this.followupByTranslated = followupByTranslated;
    }
    
    public String getFollowupByTranslatedDD() {
        return "RepEmployeeIqama_followupBy";
    }
    //</editor-fold>
}
