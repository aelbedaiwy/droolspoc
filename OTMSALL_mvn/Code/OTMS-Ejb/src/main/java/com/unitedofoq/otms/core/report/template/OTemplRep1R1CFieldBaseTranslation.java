
package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="otemplrep1r1cfieldbasei18n")
public class OTemplRep1R1CFieldBaseTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="ddTitleOverride">
    @Column
    private String ddTitleOverride;

    public void setDdTitleOverride(String ddTitleOverride) {
        this.ddTitleOverride = ddTitleOverride;
    }

    public String getDdTitleOverride() {
        return ddTitleOverride;
    }
    // </editor-fold>

}
