package com.unitedofoq.otms.core;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.uiframework.screen.OScreen;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author mohamed
 */
@Entity
public class CustomDataSecurity extends BaseEntity {

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private OScreen oscreen;

    public OScreen getOscreen() {
        return oscreen;
    }

    public void setOscreen(OScreen oscreen) {
        this.oscreen = oscreen;
    }

    public String getOscreenDD() {
        return "CustomDataSecurity_oscreen";
    }

}
