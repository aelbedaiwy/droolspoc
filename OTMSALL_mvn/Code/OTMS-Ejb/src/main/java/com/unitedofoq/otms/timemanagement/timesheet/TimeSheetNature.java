/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author mostafa
 */
@Entity
public class TimeSheetNature extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionDD() {
        return "TimeSheetNature_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="symbol">
    @Column
    private String symbol;

    public String getSymbol() {
        return symbol;
    }
    public String getSymbolDD() {
        return "TimeSheetNature_symbol";
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    // </editor-fold >
}
