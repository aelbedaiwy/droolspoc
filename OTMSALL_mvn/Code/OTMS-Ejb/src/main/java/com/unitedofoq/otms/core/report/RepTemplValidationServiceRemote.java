package com.unitedofoq.otms.core.report;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
interface RepTemplValidationServiceRemote {

    public OFunctionResult validateGrpBy(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateDSFilterFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validatePaySlipNumberOfElements(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateFilterByDateFieldType(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
