package com.unitedofoq.otms.timemanagement.reports;

import com.unitedofoq.otms.reports.views.RepEmployeeBaseTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name= "repempwoutpuntranslation")
public class RepEmployeesWithoutPunchInTranslation extends RepEmployeeBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="dayNature">
    @Column
    private String dayNature;

    public void setDayNature(String dayNature) {
        this.dayNature = dayNature;
    }

    public String getDayNature() {
        return dayNature;
    }
    // </editor-fold>

}