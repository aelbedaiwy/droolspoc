/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.instructor;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.Skill;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"instructor"})
public class InstructorSkill extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="instructor">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Instructor instructor;

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public String getInstructorDD() {
        return "InstructorSkills_instructor";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="skill">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Skill skill;

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
    
    public String getSkillDD() {
        return "InstructorSkill_skill";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="points">
    @Column(name = "points")
    private Long points;

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public String getPointsDD() {
        return "InstructorSkill_points";
    }
    // </editor-fold>
}
