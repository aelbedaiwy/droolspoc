
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepUserRole extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private long dsDBID;

    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }

    public long getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepUserRole_dsDBID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "RepUserRole_name";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="email">
    @Column
    private String email;

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailDD() {
        return "RepUserRole_email";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getTypeDD() {
        return "RepUserRole_type";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="role">
    @Column
    private String role;

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public String getRoleDD() {
        return "RepUserRole_role";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="userFullName">
    @Column
    @Translatable(translationField = "userFullNameTranslated")
    private String userFullName;

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public String getUserFullNameDD() {
        return "RepUserRole_userFullName";
    }
    @Transient
    @Translation(originalField = "userFullName")
    private String userFullNameTranslated;

    public String getUserFullNameTranslated() {
        return userFullNameTranslated;
    }

    public void setUserFullNameTranslated(String userFullNameTranslated) {
        this.userFullNameTranslated = userFullNameTranslated;
    }
    
    public String getUserFullNameTranslatedDD() {
        return "RepUserRole_userFullName";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="companyID">
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }
    
    public String getCompanyIDDD() {
        return "RepUserRole_companyID";
    }
    //</editor-fold>
}
