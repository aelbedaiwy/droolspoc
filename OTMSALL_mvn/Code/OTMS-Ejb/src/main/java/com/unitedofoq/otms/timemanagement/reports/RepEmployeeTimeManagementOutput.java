
package com.unitedofoq.otms.timemanagement.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import javax.persistence.*;
import java.math.BigDecimal;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name= "repemptmoutput")
public class RepEmployeeTimeManagementOutput extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeTimeManagementOutput_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="totalHours">
    @Column
    private BigDecimal totalHours;

    public void setTotalHours(BigDecimal totalHours) {
        this.totalHours = totalHours;
    }

    public BigDecimal getTotalHours() {
        return totalHours;
    }

    public String getTotalHoursDD() {
        return "RepEmployeeTimeManagementOutput_totalHours";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="parameterizedTotalHours">
    @Column
    private BigDecimal parameterizedTotalHours;

    public void setParameterizedTotalHours(BigDecimal parameterizedTotalHours) {
        this.parameterizedTotalHours = parameterizedTotalHours;
    }

    public BigDecimal getParameterizedTotalHours() {
        return parameterizedTotalHours;
    }

    public String getParameterizedTotalHoursDD() {
        return "RepEmployeeTimeManagementOutput_parameterizedTotalHours";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="transferredHours">
    @Column
    private BigDecimal transferredHours;

    public void setTransferredHours(BigDecimal transferredHours) {
        this.transferredHours = transferredHours;
    }

    public BigDecimal getTransferredHours() {
        return transferredHours;
    }

    public String getTransferredHoursDD() {
        return "RepEmployeeTimeManagementOutput_transferredHours";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="factor">
    @Column
    private BigDecimal factor;

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public BigDecimal getFactor() {
        return factor;
    }

    public String getFactorDD() {
        return "RepEmployeeTimeManagementOutput_factor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payrollTransferredElement">
    @Column
    @Translatable(translationField = "payrollTransferredElementTranslated")
    private String payrollTransferredElement;

    public void setPayrollTransferredElement(String payrollTransferredElement) {
        this.payrollTransferredElement = payrollTransferredElement;
    }

    public String getPayrollTransferredElement() {
        return payrollTransferredElement;
    }

    public String getPayrollTransferredElementDD() {
        return "RepEmployeeTimeManagementOutput_payrollTransferredElement";
    }
    
    @Transient
    @Translation(originalField = "payrollTransferredElement")
    private String payrollTransferredElementTranslated;

    public String getPayrollTransferredElementTranslated() {
        return payrollTransferredElementTranslated;
    }

    public void setPayrollTransferredElementTranslated(String payrollTransferredElementTranslated) {
        this.payrollTransferredElementTranslated = payrollTransferredElementTranslated;
    }
    
    public String getPayrollTransferredElementTranslatedDD() {
        return "RepEmployeeTimeManagementOutput_payrollTransferredElement";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="absenceTransferredElement">
    @Column
    @Translatable(translationField = "absenceTransferredElementTranslated")
    private String absenceTransferredElement;

    public void setAbsenceTransferredElement(String absenceTransferredElement) {
        this.absenceTransferredElement = absenceTransferredElement;
    }

    public String getAbsenceTransferredElement() {
        return absenceTransferredElement;
    }

    public String getAbsenceTransferredElementDD() {
        return "RepEmployeeTimeManagementOutput_absenceTransferredElement";
    }
    
    @Transient
    @Translation(originalField = "absenceTransferredElement")
    private String absenceTransferredElementTranslated;

    public String getAbsenceTransferredElementTranslated() {
        return absenceTransferredElementTranslated;
    }

    public void setAbsenceTransferredElementTranslated(String absenceTransferredElementTranslated) {
        this.absenceTransferredElementTranslated = absenceTransferredElementTranslated;
    }
    
    public String getAbsenceTransferredElementTranslatedDD() {
        return "RepEmployeeTimeManagementOutput_absenceTransferredElement";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="vacationTransferredElement">
    @Column
    @Translatable(translationField = "vacationTransferredElementTranslated")
    private String vacationTransferredElement;

    public void setVacationTransferredElement(String vacationTransferredElement) {
        this.vacationTransferredElement = vacationTransferredElement;
    }

    public String getVacationTransferredElement() {
        return vacationTransferredElement;
    }

    public String getVacationTransferredElementDD() {
        return "RepEmployeeTimeManagementOutput_vacationTransferredElement";
    }
    
    @Transient
    @Translation(originalField = "vacationTransferredElement")
    private String vacationTransferredElementTranslated;

    public String getVacationTransferredElementTranslated() {
        return vacationTransferredElementTranslated;
    }

    public void setVacationTransferredElementTranslated(String vacationTransferredElementTranslated) {
        this.vacationTransferredElementTranslated = vacationTransferredElementTranslated;
    }
    
    public String getVacationTransferredElementTranslatedDD() {
        return "RepEmployeeTimeManagementOutput_vacationTransferredElement";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="penaltyTransferredElement">
    @Column
    @Translatable(translationField = "penaltyTransferredElementTranslated")
    private String penaltyTransferredElement;

    public void setPenaltyTransferredElement(String penaltyTransferredElement) {
        this.penaltyTransferredElement = penaltyTransferredElement;
    }

    public String getPenaltyTransferredElement() {
        return penaltyTransferredElement;
    }

    public String getPenaltyTransferredElementDD() {
        return "RepEmployeeTimeManagementOutput_penaltyTransferredElement";
    }
    
    @Transient
    @Translation(originalField = "penaltyTransferredElement")
    private String penaltyTransferredElementTranslated;

    public String getPenaltyTransferredElementTranslated() {
        return penaltyTransferredElementTranslated;
    }

    public void setPenaltyTransferredElementTranslated(String penaltyTransferredElementTranslated) {
        this.penaltyTransferredElementTranslated = penaltyTransferredElementTranslated;
    }
    
    public String getPenaltyTransferredElementTranslatedDD() {
        return "RepEmployeeTimeManagementOutput_penaltyTransferredElement";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    @Translatable(translationField = "typeTranslated")
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getTypeDD() {
        return "RepEmployeeTimeManagementOutput_type";
    }
    
    @Transient
    @Translation(originalField = "type")
    private String typeTranslated;

    public String getTypeTranslated() {
        return typeTranslated;
    }

    public void setTypeTranslated(String typeTranslated) {
        this.typeTranslated = typeTranslated;
    }
    
    public String getTypeTranslatedDD() {
        return "RepEmployeeTimeManagementOutput_type";
    }
    // </editor-fold>
}