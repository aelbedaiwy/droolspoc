package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

@Entity
public class EmpServiceYears extends BaseEntity {

// <editor-fold defaultstate="collapsed" desc="EmpName">
    private String empName;

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }
    public String getEmpNameDD() {
        return "EmpServiceYears_empName";
    }

  //</editor-fold>
   
// <editor-fold defaultstate="collapsed" desc="Service Years">
    private Integer service;

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }
    public String getServiceDD() {
        return "EmpServiceYears_service";
    }
//</editor-fold>
}
