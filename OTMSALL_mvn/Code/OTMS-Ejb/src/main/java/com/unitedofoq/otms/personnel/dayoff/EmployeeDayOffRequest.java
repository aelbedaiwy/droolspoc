package com.unitedofoq.otms.personnel.dayoff;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationRequest;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DAYOFFTYPE")
@ParentEntity(fields = {"employee"})
public class EmployeeDayOffRequest extends BaseEntity {
    //dbid will be user as vacation_transaction_number
    // <editor-fold defaultstate="collapsed" desc="employee">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeDayOffRequest_employee";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvedBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee approvedBy;//name = "approved_by"

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "EmployeeDayOffRequest_approvedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee cancelledBy;//(name = "cancelled_by")

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancelledByDD() {
        return "EmployeeDayOffRequest_cancelledBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC cancelReason;//(name = "reason")

    public UDC getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(UDC cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelReasonDD() {
        return "EmployeeDayOffRequest_cancelReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;//(name = "vacation_start_date")

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "EmployeeDayOffRequest_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;//(name = "vacation_end_date")

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "EmployeeDayOffRequest_endDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="period">
    @Column(precision = 18, scale = 3)
    private BigDecimal period;//(name = "vacation_period")

    public void setPeriod(BigDecimal period) {
        this.period = period;
    }

    public BigDecimal getPeriod() {
        return period;
    }

    public String getPeriodDD() {
        return "EmployeeDayOffRequest_period";
    }
    @Transient
    private BigDecimal periodMask;

    public void setPeriodMask(BigDecimal periodMask) {
        updateDecimalValue("period", periodMask);
    }

    public BigDecimal getPeriodMask() {
        periodMask = period;
        return periodMask;
    }

    public String getPeriodMaskDD() {
        return "EmployeeDayOffRequest_periodMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestedDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestedDate;//(name = "requested_date")

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getRequestedDateDD() {
        return "EmployeeDayOffRequest_requestedDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;//(name = "notes")

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeeDayOffRequest_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    @Column(length = 1)
    //Y-N
    private String cancelled;//(name = "cancelled_flag")

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getCancelledDD() {
        return "EmployeeDayOffRequest_cancelled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelDate;//(name = "cancel_date")

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelDateDD() {
        return "EmployeeDayOffRequest_cancelDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="posted">
    @Column(length = 1)
    //Y-N
    private String posted;//(name = "posted")

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getPostedDD() {
        return "EmployeeDayOffRequest_posted";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="override">
    @Column(length = 1)
    //Y-N
    private String override;//(name = "override")

    public String getOverride() {
        return override;
    }

    public void setOverride(String override) {
        this.override = override;
    }

    public String getOverrideDD() {
        return "EmployeeDayOffRequest_override";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="replaceWorkingDay">
    @Column
    private String replaceWorkingDay;//(name = "replace_working_day")

    public String getReplaceWorkingDay() {
        return replaceWorkingDay;
    }

    public void setReplaceWorkingDay(String replaceWorkingDay) {
        this.replaceWorkingDay = replaceWorkingDay;
    }

    public String getReplaceWorkingDayDD() {
        return "EmployeeDayOffRequest_replaceWorkingDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penalty">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty penalty;//(name = "penalty")

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }

    public String getPenaltyDD() {
        return "EmployeeDayOffRequest_penalty";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyIndex">
    @Column(precision = 18, scale = 3)
    private BigDecimal penaltyIndex;//(name = "penalty_transaction_number")

    public BigDecimal getPenaltyIndex() {
        return penaltyIndex;
    }

    public void setPenaltyIndex(BigDecimal penaltyIndex) {
        this.penaltyIndex = penaltyIndex;
    }

    public String getPenaltyIndexDD() {
        return "EmployeeDayOffRequest_penaltyIndex";
    }
    @Transient
    private BigDecimal penaltyIndexMask;

    public BigDecimal getPenaltyIndexMask() {
        penaltyIndexMask = penaltyIndex;
        return penaltyIndexMask;
    }

    public void setPenaltyIndexMask(BigDecimal penaltyIndexMask) {
        updateDecimalValue("penaltyIndex", penaltyIndexMask);
    }

    public String getPenaltyIndexMaskDD() {
        return "EmployeeDayOffRequest_penaltyIndexMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="halfDay">
    @Column(length = 1)
    private String halfDay;//(name = "half_day")

    public String getHalfDay() {
        return halfDay;
    }

    public void setHalfDay(String halfDay) {
        this.halfDay = halfDay;
    }

    public String getHalfDayDD() {
        return "EmployeeDayOffRequest_halfDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @Column(length = 1)
    private String type;//(name = "vacation_type")

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "EmployeeDayOffRequest_type";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decisionNumber">
    @Column
    private Long decisionNumber;//(name = "decision_no")

    public Long getDecisionNumber() {
        return decisionNumber;
    }

    public void setDecisionNumber(Long decisionNumber) {
        this.decisionNumber = decisionNumber;
    }

    public String getDecisionNumberDD() {
        return "EmployeeDayOffRequest_decisionNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decisionDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date decisionDate;//(name = "decision_date")

    public Date getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }

    public String getDecisionDateDD() {
        return "EmployeeDayOffRequest_decisionDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decisionApprovedBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee decisionApprovedBy;//(name = "decision_approved_by")

    public Employee getDecisionApprovedBy() {
        return decisionApprovedBy;
    }

    public void setDecisionApprovedBy(Employee decisionApprovedBy) {
        this.decisionApprovedBy = decisionApprovedBy;
    }

    public String getDecisionApprovedByDD() {
        return "EmployeeDayOffRequest_decisionApprovedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entryDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;//(name = "entry_date")

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryDateDD() {
        return "EmployeeDayOffRequest_entryDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="needPayment">
    @Column(length = 1)
    private String needPayment;//(name = "NEED_PAYMENT")

    public String getNeedPayment() {
        return needPayment;
    }

    public void setNeedPayment(String needPayment) {
        this.needPayment = needPayment;
    }

    public String getNeedPaymentDD() {
        return "EmployeeDayOffRequest_needPayment";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="balance">
    @Column(precision = 18, scale = 3)
    private BigDecimal balance;//(name = "vac_balance")

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getBalanceDD() {
        return "EmployeeDayOffRequest_balance";
    }
    @Transient
    private BigDecimal balanceMask;

    public BigDecimal getBalanceMask() {
        balanceMask = balance;
        return balanceMask;
    }

    public void setBalanceMask(BigDecimal balanceMask) {
        updateDecimalValue("balance", balanceMask);
    }

    public String getBalanceMaskDD() {
        return "EmployeeDayOffRequest_balanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="annualBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal annualBalance;//(name = "annual_balance")

    public BigDecimal getAnnualBalance() {
        return annualBalance;
    }

    public void setAnnualBalance(BigDecimal annualBalance) {
        this.annualBalance = annualBalance;
    }

    public String getAnnualBalanceDD() {
        return "EmployeeDayOffRequest_annualBalance";
    }
    @Transient
    private BigDecimal annualBalanceMask;

    public BigDecimal getAnnualBalanceMask() {
        annualBalanceMask = annualBalance;
        return annualBalanceMask;
    }

    public void setAnnualBalanceMask(BigDecimal annualBalanceMask) {
        updateDecimalValue("annualBalance", annualBalanceMask);
    }

    public String getAnnualBalanceMaskDD() {
        return "EmployeeDayOffRequest_annualBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postedDays">
    @Column(precision = 18, scale = 3)
    private BigDecimal postedDays;//(name = "posted_vac")

    public BigDecimal getPostedDays() {
        return postedDays;
    }

    public void setPostedDays(BigDecimal postedDays) {
        this.postedDays = postedDays;
    }

    public String getPostedDaysDD() {
        return "EmployeeDayOffRequest_postedDays";
    }
    @Transient
    private BigDecimal postedDaysMask;

    public BigDecimal getPostedDaysMask() {
        postedDaysMask = postedDays;
        return postedDaysMask;
    }

    public void setPostedDaysMask(BigDecimal postedDaysMask) {
        updateDecimalValue("postedDays", postedDaysMask);
    }

    public String getPostedDaysMaskDD() {
        return "EmployeeDayOffRequest_postedDaysMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dType">
    @Transient
    private String dtype;

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getDtypeDD() {
        return "EmployeeDayOffRequest_dtype";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOff">
    @Transient
    private DayOff dayOff;

    public DayOff getDayOff() {
        EmployeeAbsenceRequest employeeAbsenceRequest;
        EmployeeVacationRequest employeeVacationRequest;
        if (this.getEntityDiscrminatorValue().equalsIgnoreCase("ABSENCE")) {
            employeeAbsenceRequest = (EmployeeAbsenceRequest) this;
            return employeeAbsenceRequest.getAbsence();
        } else if (this.getEntityDiscrminatorValue().equalsIgnoreCase("VACATION")) {
            employeeVacationRequest = (EmployeeVacationRequest) this;
            return employeeVacationRequest.getVacation();
        }
        return dayOff;
    }

    public void setDayOff(DayOff dayOff) {
        this.dayOff = dayOff;
    }

    public String getDayOffDD() {
        return "EmployeeDayOffRequest_dayOff";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal initialBalance;

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public String getInitialBalanceDD() {
        return "EmployeeDayOffRequest_initialBalance";
    }
    @Transient
    private BigDecimal initialBalanceMask;

    public BigDecimal getInitialBalanceMask() {
        initialBalanceMask = initialBalance;
        return initialBalanceMask;
    }

    public void setInitialBalanceMask(BigDecimal initialBalanceMask) {
        updateDecimalValue("initialBalance", initialBalanceMask);
    }

    public String getInitialBalanceMaskDD() {
        return "EmployeeDayOffRequest_initialBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal currentBalance;

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getCurrentBalanceDD() {
        return "EmployeeDayOffRequest_currentBalance";
    }
    @Transient
    private BigDecimal currentBalanceMask;

    public BigDecimal getCurrentBalanceMask() {
        currentBalanceMask = currentBalance;
        return currentBalanceMask;
    }

    public void setCurrentBalanceMask(BigDecimal currentBalanceMask) {
        updateDecimalValue("currentBalance", currentBalanceMask);
    }

    public String getCurrentBalanceMaskDD() {
        return "EmployeeDayOffRequest_currentBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    @Column(length = 1)
    //A-C-W
    private String status = "W";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "EmployeeDayOffRequest_status";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="actualPeriod">
    @Column(precision = 18, scale = 3)
    private BigDecimal actualPeriod;//(name = "vacation_period")

    public BigDecimal getActualPeriod() {
        return actualPeriod;
    }

    public void setActualPeriod(BigDecimal actualPeriod) {
        this.actualPeriod = actualPeriod;
    }

    public String getActualPeriodDD() {
        return "EmployeeDayOffRequest_actualPeriod";
    }
    @Transient
    private BigDecimal actualPeriodMask;//(name = "vacation_period")

    public BigDecimal getActualPeriodMask() {
        actualPeriodMask = actualPeriod;
        return actualPeriodMask;
    }

    public void setActualPeriodMask(BigDecimal actualPeriodMask) {
        updateDecimalValue("actualPeriod", actualPeriodMask);
    }

    public String getActualPeriodMaskDD() {
        return "EmployeeDayOffRequest_actualPeriodMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actualPeriod">
    @Column(precision = 18, scale = 3)
    private BigDecimal actualBalance;//(name = "vacation_period")

    public BigDecimal getActualBalance() {
        return actualBalance;
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }

    public String getActualBalanceDD() {
        return "EmployeeDayOffRequest_actualBalance";
    }
    @Transient
    private BigDecimal actualBalanceMask;//(name = "vacation_period")

    public BigDecimal getActualBalanceMask() {
        actualBalanceMask = actualBalance;
        return actualBalanceMask;
    }

    public void setActualBalanceMask(BigDecimal actualBalanceMask) {
        updateDecimalValue("actualBalance", actualBalanceMask);
    }

    public String getActualBalanceMaskDD() {
        return "EmployeeDayOffRequest_actualBalanceMask";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes1">
    private String notes1;

    public String getNotes1() {
        return notes1;
    }

    public void setNotes1(String notes1) {
        this.notes1 = notes1;
    }

    public String getNotes1DD() {
        return "EmployeeDayOffRequest_notes1";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes2">
    private String notes2;

    public String getNotes2() {
        return notes2;
    }

    public void setNotes2(String notes2) {
        this.notes2 = notes2;
    }

    public String getNotes2DD() {
        return "EmployeeDayOffRequest_notes2";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes3">
    private String notes3;

    public String getNotes3() {
        return notes3;
    }

    public void setNotes3(String notes3) {
        this.notes3 = notes3;
    }

    public String getNotes3DD() {
        return "EmployeeDayOffRequest_notes3";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes4">
    private String notes4;

    public String getNotes4() {
        return notes4;
    }

    public void setNotes4(String notes4) {
        this.notes4 = notes4;
    }

    public String getNotes4DD() {
        return "EmployeeDayOffRequest_notes4";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes5">
    private String notes5;

    public String getNotes5() {
        return notes5;
    }

    public void setNotes5(String notes5) {
        this.notes5 = notes5;
    }

    public String getNotes5DD() {
        return "EmployeeDayOffRequest_notes5";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes6">
    private String notes6;

    public String getNotes6() {
        return notes6;
    }

    public void setNotes6(String notes6) {
        this.notes6 = notes6;
    }

    public String getNotes6DD() {
        return "EmployeeDayOffRequest_notes6";
    }
    //</editor-fold>;cp
    //<editor-fold defaultstate="collapsed" desc="notes7">
    private String notes7;

    public String getNotes7() {
        return notes7;
    }

    public void setNotes7(String notes7) {
        this.notes7 = notes7;
    }

    public String getNotes7DD() {
        return "EmployeeDayOffRequest_notes7";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes8">
    private String notes8;

    public String getNotes8() {
        return notes8;
    }

    public void setNotes8(String notes8) {
        this.notes8 = notes8;
    }

    public String getNotes8DD() {
        return "EmployeeDayOffRequest_notes8";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes9">
    private String notes9;

    public String getNotes9() {
        return notes9;
    }

    public void setNotes9(String notes9) {
        this.notes9 = notes9;
    }

    public String getNotes9DD() {
        return "EmployeeDayOffRequest_notes9";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes10">
    private String notes10;

    public String getNotes10() {
        return notes10;
    }

    public void setNotes10(String notes10) {
        this.notes10 = notes10;
    }

    public String getNotes10DD() {
        return "EmployeeDayOffRequest_notes10";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes11">
    private String notes11;

    public String getNotes11() {
        return notes11;
    }

    public void setNotes11(String notes11) {
        this.notes11 = notes11;
    }

    public String getNotes11DD() {
        return "EmployeeDayOffRequest_notes11";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes12">
    private String notes12;

    public String getNotes12() {
        return notes12;
    }

    public void setNotes12(String notes12) {
        this.notes12 = notes12;
    }

    public String getNotes12DD() {
        return "EmployeeDayOffRequest_notes12";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes13">
    private String notes13;

    public String getNotes13() {
        return notes13;
    }

    public void setNotes13(String notes13) {
        this.notes13 = notes13;
    }

    public String getNotes13DD() {
        return "EmployeeDayOffRequest_notes13";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes14">
    private String notes14;

    public String getNotes14() {
        return notes14;
    }

    public void setNotes14(String notes14) {
        this.notes14 = notes14;
    }

    public String getNotes14DD() {
        return "EmployeeDayOffRequest_notes14";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes15">
    private String notes15;

    public String getNotes15() {
        return notes15;
    }

    public void setNotes15(String notes15) {
        this.notes15 = notes15;
    }

    public String getNotes15DD() {
        return "EmployeeDayOffRequest_notes15";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes16">
    private String notes16;

    public String getNotes16() {
        return notes16;
    }

    public void setNotes16(String notes16) {
        this.notes16 = notes16;
    }

    public String getNotes16DD() {
        return "EmployeeDayOffRequest_notes16";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes17">
    private String notes17;

    public String getNotes17() {
        return notes17;
    }

    public void setNotes17(String notes17) {
        this.notes17 = notes17;
    }

    public String getNotes17DD() {
        return "EmployeeDayOffRequest_notes17";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes18">
    private String notes18;

    public String getNotes18() {
        return notes18;
    }

    public void setNotes18(String notes18) {
        this.notes18 = notes18;
    }

    public String getNotes18DD() {
        return "EmployeeDayOffRequest_notes18";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes19">
    private String notes19;

    public String getNotes19() {
        return notes19;
    }

    public void setNotes19(String notes19) {
        this.notes19 = notes19;
    }

    public String getNotes19DD() {
        return "EmployeeDayOffRequest_notes19";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes20">
    private String notes20;

    public String getNotes20() {
        return notes20;
    }

    public void setNotes20(String notes20) {
        this.notes20 = notes20;
    }

    public String getNotes20DD() {
        return "EmployeeDayOffRequest_notes20";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes1Auto">
    private String notes1Auto;

    public String getNotes1Auto() {
        return notes1Auto;
    }

    public void setNotes1Auto(String notes1Auto) {
        this.notes1Auto = notes1Auto;
    }

    public String getNotes1AutoDD() {
        return "EmployeeDayOffRequest_notes1Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes2Auto">
    private String notes2Auto;

    public String getNotes2Auto() {
        return notes2Auto;
    }

    public void setNotes2Auto(String notes2Auto) {
        this.notes2Auto = notes2Auto;
    }

    public String getNotes2AutoDD() {
        return "EmployeeDayOffRequest_notes2Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes3Auto">
    private String notes3Auto;

    public String getNotes3Auto() {
        return notes3Auto;
    }

    public void setNotes3Auto(String notes3Auto) {
        this.notes3Auto = notes3Auto;
    }

    public String getNotes3AutoDD() {
        return "EmployeeDayOffRequest_notes3Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes4Auto">
    private String notes4Auto;

    public String getNotes4Auto() {
        return notes4Auto;
    }

    public void setNotes4Auto(String notes4Auto) {
        this.notes4Auto = notes4Auto;
    }

    public String getNotes4AutoDD() {
        return "EmployeeDayOffRequest_notes4Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes5Auto">
    private String notes5Auto;

    public String getNotes5Auto() {
        return notes5Auto;
    }

    public void setNotes5Auto(String notes5Auto) {
        this.notes5Auto = notes5Auto;
    }

    public String getNotes5AutoDD() {
        return "EmployeeDayOffRequest_notes5Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes6Auto">
    private String notes6Auto;

    public String getNotes6Auto() {
        return notes6Auto;
    }

    public void setNotes6Auto(String notes6Auto) {
        this.notes6Auto = notes6Auto;
    }

    public String getNotes6AutoDD() {
        return "EmployeeDayOffRequest_notes6Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes7Auto">
    private String notes7Auto;

    public String getNotes7Auto() {
        return notes7Auto;
    }

    public void setNotes7Auto(String notes7Auto) {
        this.notes7Auto = notes7Auto;
    }

    public String getNotes7AutoDD() {
        return "EmployeeDayOffRequest_notes7Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes8Auto">
    private String notes8Auto;

    public String getNotes8Auto() {
        return notes8Auto;
    }

    public void setNotes8Auto(String notes8Auto) {
        this.notes8Auto = notes8Auto;
    }

    public String getNotes8AutoDD() {
        return "EmployeeDayOffRequest_notes8Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes9Auto">
    private String notes9Auto;

    public String getNotes9Auto() {
        return notes9Auto;
    }

    public void setNotes9Auto(String notes9Auto) {
        this.notes9Auto = notes9Auto;
    }

    public String getNotes9AutoDD() {
        return "EmployeeDayOffRequest_notes9Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes10Auto">
    private String notes10Auto;

    public String getNotes10Auto() {
        return notes10Auto;
    }

    public void setNotes10Auto(String notes10Auto) {
        this.notes10Auto = notes10Auto;
    }

    public String getNotes10AutoDD() {
        return "EmployeeDayOffRequest_notes10Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes11Auto">
    private String notes11Auto;

    public String getNotes11Auto() {
        return notes11Auto;
    }

    public void setNotes11Auto(String notes11Auto) {
        this.notes11Auto = notes11Auto;
    }

    public String getNotes11AutoDD() {
        return "EmployeeDayOffRequest_notes11Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes12Auto">
    private String notes12Auto;

    public String getNotes12Auto() {
        return notes12Auto;
    }

    public void setNotes12Auto(String notes12Auto) {
        this.notes12Auto = notes12Auto;
    }

    public String getNotes12AutoDD() {
        return "EmployeeDayOffRequest_notes12Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes13Auto">
    private String notes13Auto;

    public String getNotes13Auto() {
        return notes13Auto;
    }

    public void setNotes13Auto(String notes13Auto) {
        this.notes13Auto = notes13Auto;
    }

    public String getNotes13AutoDD() {
        return "EmployeeDayOffRequest_notes13Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes14Auto">
    private String notes14Auto;

    public String getNotes14Auto() {
        return notes14Auto;
    }

    public void setNotes14Auto(String notes14Auto) {
        this.notes14Auto = notes14Auto;
    }

    public String getNotes14AutoDD() {
        return "EmployeeDayOffRequest_notes14Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes15Auto">
    private String notes15Auto;

    public String getNotes15Auto() {
        return notes15Auto;
    }

    public void setNotes15Auto(String notes15Auto) {
        this.notes15Auto = notes15Auto;
    }

    public String getNotes15AutoDD() {
        return "EmployeeDayOffRequest_notes15Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes16Auto">
    private String notes16Auto;

    public String getNotes16Auto() {
        return notes16Auto;
    }

    public void setNotes16Auto(String notes16Auto) {
        this.notes16Auto = notes16Auto;
    }

    public String getNotes16AutoDD() {
        return "EmployeeDayOffRequest_notes16Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes17Auto">
    private String notes17Auto;

    public String getNotes17Auto() {
        return notes17Auto;
    }

    public void setNotes17Auto(String notes17Auto) {
        this.notes17Auto = notes17Auto;
    }

    public String getNotes17AutoDD() {
        return "EmployeeDayOffRequest_notes17Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes18Auto">
    private String notes18Auto;

    public String getNotes18Auto() {
        return notes18Auto;
    }

    public void setNotes18Auto(String notes18Auto) {
        this.notes18Auto = notes18Auto;
    }

    public String getNotes18AutoDD() {
        return "EmployeeDayOffRequest_notes18Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes19Auto">
    private String notes19Auto;

    public String getNotes19Auto() {
        return notes19Auto;
    }

    public void setNotes19Auto(String notes19Auto) {
        this.notes19Auto = notes19Auto;
    }

    public String getNotes19AutoDD() {
        return "EmployeeDayOffRequest_notes19Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes20Auto">
    private String notes20Auto;

    public String getNotes20Auto() {
        return notes20Auto;
    }

    public void setNotes20Auto(String notes20Auto) {
        this.notes20Auto = notes20Auto;
    }

    public String getNotes20AutoDD() {
        return "EmployeeDayOffRequest_notes20Auto";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes1Trans">
    @Transient
    private String notes1Trans;

    public String getNotes1Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes1();
        commentsAuto = getNotes1Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);

        notes1Trans = commentsAuto + " " + comments;
        return notes1Trans;
    }

    public void setNotes1Trans(String notes1Trans) {
        this.notes1Trans = notes1Trans;
    }

    public String getNotes1TransDD() {
        return "EmployeeDayOffRequest_notes1Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes2Trans">
    @Transient
    private String notes2Trans;

    public String getNotes2Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes2();
        commentsAuto = getNotes2Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes2Trans = commentsAuto + " " + comments;
        return notes2Trans;
    }

    public void setNotes2Trans(String notes2Trans) {
        this.notes2Trans = notes2Trans;
    }

    public String getNotes2TransDD() {
        return "EmployeeDayOffRequest_notes2Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes3Trans">
    @Transient
    private String notes3Trans;

    public String getNotes3Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes3();
        commentsAuto = getNotes3Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes3Trans = commentsAuto + " " + comments;
        return notes3Trans;
    }

    public void setNotes3Trans(String notes3Trans) {
        this.notes3Trans = notes3Trans;
    }

    public String getNotes3TransDD() {
        return "EmployeeDayOffRequest_notes3Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes4Trans">
    @Transient
    private String notes4Trans;

    public String getNotes4Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes4();
        commentsAuto = getNotes4Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes4Trans = commentsAuto + " " + comments;
        return notes4Trans;
    }

    public void setNotes4Trans(String notes4Trans) {
        this.notes4Trans = notes4Trans;
    }

    public String getNotes4TransDD() {
        return "EmployeeDayOffRequest_notes4Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes5Trans">
    @Transient
    private String notes5Trans;

    public String getNotes5Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes5();
        commentsAuto = getNotes5Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes5Trans = commentsAuto + " " + comments;
        return notes5Trans;
    }

    public void setNotes5Trans(String notes5Trans) {
        this.notes5Trans = notes5Trans;
    }

    public String getNotes5TransDD() {
        return "EmployeeDayOffRequest_notes5Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes6Trans">
    @Transient
    private String notes6Trans;

    public String getNotes6Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes6();
        commentsAuto = getNotes6Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes6Trans = commentsAuto + " " + comments;
        return notes6Trans;
    }

    public void setNotes6Trans(String notes6Trans) {
        this.notes6Trans = notes6Trans;
    }

    public String getNotes6TransDD() {
        return "EmployeeDayOffRequest_notes6Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes7Trans">
    @Transient
    private String notes7Trans;

    public String getNotes7Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes7();
        commentsAuto = getNotes7Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes7Trans = commentsAuto + " " + comments;
        return notes7Trans;
    }

    public void setNotes7Trans(String notes7Trans) {
        this.notes7Trans = notes7Trans;
    }

    public String getNotes7TransDD() {
        return "EmployeeDayOffRequest_notes7Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes8Trans">
    @Transient
    private String notes8Trans;

    public String getNotes8Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes8();
        commentsAuto = getNotes8Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes8Trans = commentsAuto + " " + comments;
        return notes8Trans;
    }

    public void setNotes8Trans(String notes8Trans) {
        this.notes8Trans = notes8Trans;
    }

    public String getNotes8TransDD() {
        return "EmployeeDayOffRequest_notes8Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes9Trans">
    @Transient
    private String notes9Trans;

    public String getNotes9Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes9();
        commentsAuto = getNotes9Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        notes9Trans = commentsAuto + " " + comments;
        return notes9Trans;
    }

    public void setNotes9Trans(String notes9Trans) {
        this.notes9Trans = notes9Trans;
    }

    public String getNotes9TransDD() {
        return "EmployeeDayOffRequest_notes9Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes10Trans">
    @Transient
    private String notes10Trans;

    public String getNotes10Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes10();
        commentsAuto = getNotes10Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes10Trans = commentsAuto + " " + comments;
        return notes10Trans;
    }

    public void setNotes10Trans(String notes10Trans) {
        this.notes10Trans = notes10Trans;
    }

    public String getNotes10TransDD() {
        return "EmployeeDayOffRequest_notes10Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes11Trans">
    @Transient
    private String notes11Trans;

    public String getNotes11Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes11();
        commentsAuto = getNotes11Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);

        notes11Trans = commentsAuto + " " + comments;
        return notes11Trans;
    }

    public void setNotes11Trans(String notes11Trans) {
        this.notes11Trans = notes11Trans;
    }

    public String getNotes11TransDD() {
        return "EmployeeDayOffRequest_notes11Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes12Trans">
    @Transient
    private String notes12Trans;

    public String getNotes12Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes12();
        commentsAuto = getNotes12Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes12Trans = commentsAuto + " " + comments;
        return notes12Trans;
    }

    public void setNotes12Trans(String notes12Trans) {
        this.notes12Trans = notes12Trans;
    }

    public String getNotes12TransDD() {
        return "EmployeeDayOffRequest_notes12Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes13Trans">
    @Transient
    private String notes13Trans;

    public String getNotes13Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes13();
        commentsAuto = getNotes13Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes13Trans = commentsAuto + " " + comments;
        return notes13Trans;
    }

    public void setNotes13Trans(String notes13Trans) {
        this.notes13Trans = notes13Trans;
    }

    public String getNotes13TransDD() {
        return "EmployeeDayOffRequest_notes13Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes14Trans">
    @Transient
    private String notes14Trans;

    public String getNotes14Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes14();
        commentsAuto = getNotes14Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes14Trans = commentsAuto + " " + comments;
        return notes14Trans;
    }

    public void setNotes14Trans(String notes14Trans) {
        this.notes14Trans = notes14Trans;
    }

    public String getNotes14TransDD() {
        return "EmployeeDayOffRequest_notes14Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes15Trans">
    @Transient
    private String notes15Trans;

    public String getNotes15Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes15();
        commentsAuto = getNotes15Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes15Trans = commentsAuto + " " + comments;
        return notes15Trans;
    }

    public void setNotes15Trans(String notes15Trans) {
        this.notes15Trans = notes15Trans;
    }

    public String getNotes15TransDD() {
        return "EmployeeDayOffRequest_notes15Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes16Trans">
    @Transient
    private String notes16Trans;

    public String getNotes16Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes16();
        commentsAuto = getNotes16Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes16Trans = commentsAuto + " " + comments;
        return notes16Trans;
    }

    public void setNotes16Trans(String notes16Trans) {
        this.notes16Trans = notes16Trans;
    }

    public String getNotes16TransDD() {
        return "EmployeeDayOffRequest_notes16Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes17Trans">
    @Transient
    private String notes17Trans;

    public String getNotes17Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes17();
        commentsAuto = getNotes17Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes17Trans = commentsAuto + " " + comments;
        return notes17Trans;
    }

    public void setNotes17Trans(String notes17Trans) {
        this.notes17Trans = notes17Trans;
    }

    public String getNotes17TransDD() {
        return "EmployeeDayOffRequest_notes17Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes18Trans">
    @Transient
    private String notes18Trans;

    public String getNotes18Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes18();
        commentsAuto = getNotes18Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes18Trans = commentsAuto + " " + comments;
        return notes18Trans;
    }

    public void setNotes18Trans(String notes18Trans) {
        this.notes18Trans = notes18Trans;
    }

    public String getNotes18TransDD() {
        return "EmployeeDayOffRequest_notes18Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes19Trans">
    @Transient
    private String notes19Trans;

    public String getNotes19Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes19();
        commentsAuto = getNotes19Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        notes19Trans = commentsAuto + " " + comments;
        return notes19Trans;
    }

    public void setNotes19Trans(String notes19Trans) {
        this.notes19Trans = notes19Trans;
    }

    public String getNotes19TransDD() {
        return "EmployeeDayOffRequest_notes19Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes20Trans">
    @Transient
    private String notes20Trans;

    public String getNotes20Trans() {
        String comments;
        String commentsAuto;
        comments = getNotes20();
        commentsAuto = getNotes20Auto();
        if (comments == null) {
            comments = "";
        }
        if (commentsAuto == null) {
            commentsAuto = "";
        }
        String regex = "([a-z])([A-Z])";
        String replacement = "$1 $2";
        commentsAuto = commentsAuto.replaceAll(regex, replacement);
        notes20Trans = commentsAuto + " " + comments;
        return notes20Trans;
    }

    public void setNotes20Trans(String notes20Trans) {
        this.notes20Trans = notes20Trans;
    }

    public String getNotes20TransDD() {
        return "EmployeeDayOffRequest_notes20Trans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes1Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes1Date;

    public Date getNotes1Date() {
        return notes1Date;
    }

    public void setNotes1Date(Date notes1Date) {
        this.notes1Date = notes1Date;
    }

    public String getNotes1DateDD() {
        return "EmployeeDayOffRequest_notes1Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes2Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes2Date;

    public Date getNotes2Date() {
        return notes2Date;
    }

    public void setNotes2Date(Date notes2Date) {
        this.notes2Date = notes2Date;
    }

    public String getNotes2DateDD() {
        return "EmployeeDayOffRequest_notes2Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes3Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes3Date;

    public Date getNotes3Date() {
        return notes3Date;
    }

    public void setNotes3Date(Date notes3Date) {
        this.notes3Date = notes3Date;
    }

    public String getNotes3DateDD() {
        return "EmployeeDayOffRequest_notes3Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes4Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes4Date;

    public Date getNotes4Date() {
        return notes4Date;
    }

    public void setNotes4Date(Date notes4Date) {
        this.notes4Date = notes4Date;
    }

    public String getNotes4DateDD() {
        return "EmployeeDayOffRequest_notes4Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes5Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes5Date;

    public Date getNotes5Date() {
        return notes5Date;
    }

    public void setNotes5Date(Date notes5Date) {
        this.notes5Date = notes5Date;
    }

    public String getNotes5DateDD() {
        return "EmployeeDayOffRequest_notes5Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes6Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes6Date;

    public Date getNotes6Date() {
        return notes6Date;
    }

    public void setNotes6Date(Date notes6Date) {
        this.notes6Date = notes6Date;
    }

    public String getNotes6DateDD() {
        return "EmployeeDayOffRequest_notes6Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes7Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes7Date;

    public Date getNotes7Date() {
        return notes7Date;
    }

    public void setNotes7Date(Date notes7Date) {
        this.notes7Date = notes7Date;
    }

    public String getNotes7DateDD() {
        return "EmployeeDayOffRequest_notes7Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes8Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes8Date;

    public Date getNotes8Date() {
        return notes8Date;
    }

    public void setNotes8Date(Date notes8Date) {
        this.notes8Date = notes8Date;
    }

    public String getNotes8DateDD() {
        return "EmployeeDayOffRequest_notes8Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes9Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes9Date;

    public Date getNotes9Date() {
        return notes9Date;
    }

    public void setNotes9Date(Date notes9Date) {
        this.notes9Date = notes9Date;
    }

    public String getNotes9DateDD() {
        return "EmployeeDayOffRequest_notes9Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes10Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes10Date;

    public Date getNotes10Date() {
        return notes10Date;
    }

    public void setNotes10Date(Date notes10Date) {
        this.notes10Date = notes10Date;
    }

    public String getNotes10DateDD() {
        return "EmployeeDayOffRequest_notes10Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes11Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes11Date;

    public Date getNotes11Date() {
        return notes11Date;
    }

    public void setNotes11Date(Date notes11Date) {
        this.notes11Date = notes11Date;
    }

    public String getNotes11DateDD() {
        return "EmployeeDayOffRequest_notes11Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes12Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes12Date;

    public Date getNotes12Date() {
        return notes12Date;
    }

    public void setNotes12Date(Date notes12Date) {
        this.notes12Date = notes12Date;
    }

    public String getNotes12DateDD() {
        return "EmployeeDayOffRequest_notes12Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes13Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes13Date;

    public Date getNotes13Date() {
        return notes13Date;
    }

    public void setNotes13Date(Date notes13Date) {
        this.notes13Date = notes13Date;
    }

    public String getNotes13DateDD() {
        return "EmployeeDayOffRequest_notes13Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes14Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes14Date;

    public Date getNotes14Date() {
        return notes14Date;
    }

    public void setNotes14Date(Date notes14Date) {
        this.notes14Date = notes14Date;
    }

    public String getNotes14DateDD() {
        return "EmployeeDayOffRequest_notes14Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes15Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes15Date;

    public Date getNotes15Date() {
        return notes15Date;
    }

    public void setNotes15Date(Date notes15Date) {
        this.notes15Date = notes15Date;
    }

    public String getNotes15DateDD() {
        return "EmployeeDayOffRequest_notes15Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes16Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes16Date;

    public Date getNotes16Date() {
        return notes16Date;
    }

    public void setNotes16Date(Date notes16Date) {
        this.notes16Date = notes16Date;
    }

    public String getNotes16DateDD() {
        return "EmployeeDayOffRequest_notes16Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes17Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes17Date;

    public Date getNotes17Date() {
        return notes17Date;
    }

    public void setNotes17Date(Date notes17Date) {
        this.notes17Date = notes17Date;
    }

    public String getNotes17DateDD() {
        return "EmployeeDayOffRequest_notes17Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes18Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes18Date;

    public Date getNotes18Date() {
        return notes18Date;
    }

    public void setNotes18Date(Date notes18Date) {
        this.notes18Date = notes18Date;
    }

    public String getNotes18DateDD() {
        return "EmployeeDayOffRequest_notes18Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes19Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes19Date;

    public Date getNotes19Date() {
        return notes19Date;
    }

    public void setNotes19Date(Date notes19Date) {
        this.notes19Date = notes19Date;
    }

    public String getNotes19DateDD() {
        return "EmployeeDayOffRequest_notes19Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes20Date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date notes20Date;

    public Date getNotes20Date() {
        return notes20Date;
    }

    public void setNotes20Date(Date notes20Date) {
        this.notes20Date = notes20Date;
    }

    public String getNotes20DateDD() {
        return "EmployeeDayOffRequest_notes20Date";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDateDD() {
        return "EmployeeDayOffRequest_requestDate";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="processStatus">
    private String processStatus;

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getProcessStatusDD() {
        return "EmployeeDayOffRequest_processStatus";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="logoImage">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OImage logoImage;

    public OImage getLogoImage() {
        return logoImage;
    }

    public String getLogoImageDD() {
        return "EmployeeDayOffRequest_logoImage";
    }

    public void setLogoImage(OImage logoImage) {
        this.logoImage = logoImage;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="statusImage">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OImage statusImage;

    public OImage getStatusImage() {
        return statusImage;
    }

    public String getStatusImageDD() {
        return "EmployeeDayOffRequest_statusImage";
    }

    public void setStatusImage(OImage statusImage) {
        this.statusImage = statusImage;
    }
    //</editor-fold >
    // <editor-fold defaultstate="collapsed" desc="destination">
    @Column
    private String destination;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationDD() {
        return "EmployeeDayOffRequest_destination";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="usedTransportation">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC usedTransportation;//(name = "reason")

    public UDC getUsedTransportation() {
        return usedTransportation;
    }

    public void setUsedTransportation(UDC usedTransportation) {
        this.usedTransportation = usedTransportation;
    }

    public String getUsedTransportationDD() {
        return "EmployeeDayOffRequest_usedTransportation";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="passingNight">
    @Column
    private boolean passingNight;

    public boolean isPassingNight() {
        return passingNight;
    }

    public void setPassingNight(boolean passingNight) {
        this.passingNight = passingNight;
    }

    public String getPassingNightDD() {
        return "EmployeeDayOffRequest_passingNight";
    }
    //</editor-fold>
}
