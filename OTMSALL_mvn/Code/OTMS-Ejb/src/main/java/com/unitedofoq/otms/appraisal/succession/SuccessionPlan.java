
package com.unitedofoq.otms.appraisal.succession;

import java.util.Date;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.MeasurableField;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.List;

@Entity
@ParentEntity(fields={"employee"})
@ChildEntity(fields={"courses","competencies","others","certificates"})
public class SuccessionPlan extends BaseEntity  {

    //<editor-fold defaultstate="collapsed" desc="company">
//    @JoinColumn(nullable=false)
//    @ManyToOne(fetch= FetchType.LAZY)
//    private Company company;
//
//    public Company getCompany() {
//        return company;
//    }
//
//    public void setCompany(Company company) {
//        this.company = company;
//    }
//    
//    public String getCompanyDD() {
//        return "SuccessionPlan_company";
//    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "SuccessionPlan_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "SuccessionPlan_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="targetPos">
    @ManyToOne(fetch= FetchType.LAZY)
    private Position targetPos;

    public void setTargetPos(Position targetPos) {
        this.targetPos = targetPos;
    }

    public Position getTargetPos() {
        return targetPos;
    }

    public String getTargetPosDD() {
        return "SuccessionPlan_targetPos";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch= FetchType.LAZY)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "SuccessionPlan_employee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="creationDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date creationDate;

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getCreationDateDD() {
        return "SuccessionPlan_creationDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="planDuration">
    @Column
    @MeasurableField(name = "durationUnit.value")
    private Integer planDuration;

    public void setPlanDuration(Integer planDuration) {
        this.planDuration = planDuration;
    }

    public Integer getPlanDuration() {
        return planDuration;
    }

    public String getPlanDurationDD() {
        return "SuccessionPlan_planDuration";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="durationUnit">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC durationUnit;

    public UDC getDurationUnit() {
        return durationUnit;
    }
    public String getDurationUnitDD() {
        return "SuccessionPlan_durationUnit";
    }

    public void setDurationUnit(UDC durationUnit) {
        this.durationUnit = durationUnit;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "SuccessionPlan_startDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getEndDateDD() {
        return "SuccessionPlan_endDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="critical">
    @Column
    private boolean critical;

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean isCritical() {
        return critical;
    }

    public String getCriticalDD() {
        return "SuccessionPlan_critical";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public String getNotesDD() {
        return "SuccessionPlan_notes";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="competencies">
    @OneToMany(mappedBy="plan")
    private List<SuccessionComp> competencies;

    public List<SuccessionComp> getCompetencies() {
        return competencies;
    }

    public void setCompetencies(List<SuccessionComp> competencies) {
        this.competencies = competencies;
    }
    
    public String getCompetenciesDD() {
        return "SuccessionPlan_competencies";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="courses">
    @OneToMany(mappedBy="plan")
    private List<SuccessionCourse> courses;
    
    public List<SuccessionCourse> getCourses() {
        return courses;
    }

    public void setCourses(List<SuccessionCourse> courses) {
        this.courses = courses;
    }
    
    public String getCoursesDD() {
        return "SuccessionPlan_courses";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="createdBy">
    @ManyToOne(fetch= FetchType.LAZY)
    private Employee createdBy;

    public Employee getCreatedBy() {
        return createdBy;
    }
    public String getCreatedByDD() {
        return "SuccessionPlan_createdBy";
    }

    public void setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
    }
    // </editor-fold >
    
    // <editor-fold defaultstate="collapsed" desc="nextReviewerDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date nextReviewerDate;

    public Date getNextReviewerDate() {
        return nextReviewerDate;
    }
    public String getNextReviewerDateDD() {
        return "SuccessionPlan_nextReviewerDate";
    }

    public void setNextReviewerDate(Date nextReviewerDate) {
        this.nextReviewerDate = nextReviewerDate;
    }
    // </editor-fold >
    
    // <editor-fold defaultstate="collapsed" desc="lastReviewerDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date lastReviewerDate;

    public Date getLastReviewerDate() {
        return lastReviewerDate;
    }
    public String getLastReviewerDateDD() {
        return "SuccessionPlan_lastReviewerDate";
    }

    public void setLastReviewerDate(Date lastReviewerDate) {
        this.lastReviewerDate = lastReviewerDate;
    }
    // </editor-fold >
    
    //<editor-fold defaultstate="collapsed" desc="others">
    @OneToMany(mappedBy="plan")
    private List<SuccessionOther> others;

    public List<SuccessionOther> getOthers() {
        return others;
    }
    public String getOthersDD() {
        return "SuccessionPlan_others";
    }

    public void setOthers(List<SuccessionOther> others) {
        this.others = others;
    }
    //</editor-fold >
    
    //<editor-fold defaultstate="collapsed" desc="certificates">
    @OneToMany(mappedBy="plan")
    private List<SuccessionCertificate> certificates;

    public List<SuccessionCertificate> getCertificates() {
        return certificates;
    }
    public String getCertificatesDD() {
        return "SuccessionPlan_certificates";
    }

    public void setCertificates(List<SuccessionCertificate> certificates) {
        this.certificates = certificates;
    }
    //</editor-fold >
}