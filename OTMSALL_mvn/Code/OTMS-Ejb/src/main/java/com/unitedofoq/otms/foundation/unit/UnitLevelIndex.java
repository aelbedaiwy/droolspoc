/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"company"})
@DiscriminatorValue("MASTER")
public class UnitLevelIndex extends UnitLevelIndexBase {
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    Company company;


    public Company getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "UnitLevelIndex_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
}
