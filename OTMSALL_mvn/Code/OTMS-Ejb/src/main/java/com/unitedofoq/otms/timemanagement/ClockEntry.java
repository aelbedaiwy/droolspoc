package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Aly El-Bedaiwy
 */
@Entity
@Table(name = "reptm_clock_entry")
public class ClockEntry extends BaseEntity implements Comparable<ClockEntry> {

    private String employeeCode;
    private String organization_id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;
    private String entryTime;
    private String type;
    private String activeEntry;
    private String aux1;
    private String printOrder;

    public String getPrintOrder() {
        return printOrder;
    }

    public void setPrintOrder(String printOrder) {
        this.printOrder = printOrder;
    }

    public String getPrintOrderDD() {
        return "ClockEntry_printOrder";
    }

    public String getAux1() {
        return aux1;
    }

    public void setAux1(String aux1) {
        this.aux1 = aux1;
    }

    public String getAux1DD() {
        return "ClockEntry_aux1";
    }

    public String getActiveEntry() {
        return activeEntry;
    }

    public void setActiveEntry(String activeEntry) {
        this.activeEntry = activeEntry;
    }

    public String getActiveEntryDD() {
        return "ClockEntry_activeEntry";
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "ClockEntry_employeeCode";
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getOrganization_idDD() {
        return "ClockEntry_organization_id";
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryDateDD() {
        return "ClockEntry_entryDate";
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    public String getEntryTimeDD() {
        return "ClockEntry_entryTime";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "ClockEntry_type";
    }

    @Override
    public int compareTo(ClockEntry t) {
        return this.printOrder.compareTo(t.getPrintOrder());
    }

}
