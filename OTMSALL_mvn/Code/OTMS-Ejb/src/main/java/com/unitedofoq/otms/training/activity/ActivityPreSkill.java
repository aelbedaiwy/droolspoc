package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"activity"})
@DiscriminatorValue("ACTPRESKILL")
public class ActivityPreSkill extends TrainingCompetencyBase {
    // <editor-fold defaultstate="collapsed" desc="activity">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getActivityDD() {
        return "PreActivitySkills_activity";
    }
    // </editor-fold>
}
