package com.unitedofoq.otms.recruitment.vacancy;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
@ParentEntity(fields = {"position"})
public class VacancyRequest extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    // @JoinColumn (nullable=false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "VacancyRequest_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestedNumber">
    private int requestedNumber;

    public int getRequestedNumber() {
        return requestedNumber;
    }

    public void setRequestedNumber(int requestedNumber) {
        this.requestedNumber = requestedNumber;
    }

    public String getRequestedNumberDD() {
        return "VacancyRequest_requestedNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestedBy">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee requestedBy;

    public Employee getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(Employee requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRequestedByDD() {
        return "VacancyRequest_requestedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDateDD() {
        return "VacancyRequest_requestDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approved">
    private boolean approved;

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getApprovedDD() {
        return "VacancyRequest_approved";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvedBy">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee approvedBy;

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "VacancyRequest_approvedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvalDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date approvalDate;

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalDateDD() {
        return "VacancyRequest_approvalDate";
    }
    // </editor-fold>
}
