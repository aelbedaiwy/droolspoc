/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.jobrequisitionapplicant;


import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.recruitment.jobapplicant.JobApplicant;
import com.unitedofoq.otms.recruitment.jobrequisition.JobRequisition;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arezk
 */

@Entity
@ChildEntity(fields={"events", "jobReqAppOffer"})
@ParentEntity(fields={"jobRequisition"})
public class JobRequisitionApplicant extends BaseEntity{    
    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
    private Date creationDate;
    public String getCreationDateDD(){
        return "JobRequisitionApplicant_creationDate";
    }
    @Column(nullable=false)
    private boolean hired;// = new Boolean(false);
    public String getHiredDD(){
        return "JobRequisitionApplicant_hired";
    }
    @Column(nullable=false)
    private boolean accepted;// = new Boolean(false);
    public String getAcceptedDD(){
        return "JobRequisitionApplicant_accepted";
    }
    @Column(nullable=false)
    private boolean shortListed;// = new Boolean(false);
    public String getShortListedDD(){
        return "JobRequisitionApplicant_shortListed";
    }

    //Attributes from direct assosiation relations
    @OneToMany(mappedBy = "requisitionApplicant")
    private List<JobReqAppEvent> events;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="jobApplicant")
    private JobApplicant jobApplicant;
    public String getJobApplicantDD(){
        return "JobRequisitionApplicant_jobApplicant";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name = "JobRequisition")
    private JobRequisition jobRequisition;
    public String getJobRequisitionDD(){
        return "JobRequisitionApplicant_jobRequisition";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC status;
    public String getStatusDD(){
        return "JobRequisitionApplicant_status";
    }

    @OneToMany(mappedBy = "jobRequisitionApplicant")
    private List<JobReqAppOffer> jobReqAppOffer = new ArrayList<JobReqAppOffer>();
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser creator;
    public String getCreatorDD(){
        return "JobRequisitionApplicant_creator";
    }
   
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public OUser getCreator() {
        return creator;
    }

    public void setCreator(OUser creator) {
        this.creator = creator;
    }

    public List<JobReqAppEvent> getEvents() {
        return events;
    }

    public void setEvents(List<JobReqAppEvent> events) {
        this.events = events;
    }

   
    public JobApplicant getJobApplicant() {
        return jobApplicant;
    }

    public void setJobApplicant(JobApplicant jobApplicant) {
        this.jobApplicant = jobApplicant;
    }

    public List<JobReqAppOffer> getJobReqAppOffer() {
        return jobReqAppOffer;
    }

    public void setJobReqAppOffer(List<JobReqAppOffer> jobReqAppOffer) {
        this.jobReqAppOffer = jobReqAppOffer;
    }

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isHired() {
        return hired;
    }

    public void setHired(boolean hired) {
        this.hired = hired;
    }

    public boolean isShortListed() {
        return shortListed;
    }

    public void setShortListed(boolean shortListed) {
        this.shortListed = shortListed;
    }

   
    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

}
