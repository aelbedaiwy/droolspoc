/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.job;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author mmohamed
 */
@Entity
@Table(name="Job")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("MASTER")
@DiscriminatorColumn(name="JTYPE", discriminatorType=DiscriminatorType.STRING)
public class JobBase extends BusinessObjectBaseEntity   {
    // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
     public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNameTranslatedDD() {
        return " JobBase_name";
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "JobBase_code";
    }
// </editor-fold>
}
