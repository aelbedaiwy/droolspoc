/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll.formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;

/**
 *
 * @author lap2
 */
@Entity
public class FormulaDistinctName extends  BaseEntity{
    //<editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "FormulaDistinctName_description";
    }
    //</editor-fold>
}
