package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"otemplRep"})
public class OTemplRepFilterFieldValue extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="orepTemplDSField">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORepTemplDSField orepTemplDSField;

    /**
     * @return the oRepTemplDSField
     */
    public ORepTemplDSField getOrepTemplDSField() {
        return orepTemplDSField;
    }

    /**
     * @param oRepTemplDSField the oRepTemplDSField to set
     */
    public void setOrepTemplDSField(ORepTemplDSField oRepTemplDSField) {
        this.orepTemplDSField = oRepTemplDSField;
    }

    /**
     * @return the oRepTemplDSFieldDD
     */
    public String getOrepTemplDSFieldDD() {
        return "OTemplRepFilterFieldValue_oRepTemplDSField";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="otemplRep">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OTemplRep otemplRep;

    /**
     * @return the otemplRep
     */
    public OTemplRep getOtemplRep() {
        return otemplRep;
    }

    /**
     * @param otemplRep the otemplRep to set
     */
    public void setOtemplRep(OTemplRep otemplRep) {
        this.otemplRep = otemplRep;
    }

    /**
     * @return the otemplRep DD
     */
    public String getOtemplRepDD() {
        return "OTemplRepFilterFieldValue_otemplRep";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filterFieldDbid">
    @Column(nullable = false)
    private long filterFieldDbid;

    /**
     * @return the filterFieldDbid
     */
    public long getFilterFieldDbid() {
        return filterFieldDbid;
    }

    /**
     * @param filterFieldDbid the filterFieldDbid to set
     */
    public void setFilterFieldDbid(long filterFieldDbid) {
        this.filterFieldDbid = filterFieldDbid;
    }

    /**
     * @return the filterFieldDbidDD
     */
    public String getFilterFieldDbidDD() {
        return "OTemplRepFilterFieldValue_filterFieldDbid";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filterFieldEntity">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity filterFieldEntity;

    /**
     * @return the filterFieldEntity
     */
    public OEntity getFilterFieldEntity() {
        return filterFieldEntity;
    }

    /**
     * @param filterFieldEntity the filterFieldEntity to set
     */
    public void setFilterFieldEntity(OEntity filterFieldEntity) {
        this.filterFieldEntity = filterFieldEntity;
    }

    /**
     * @return the filterFieldEntityDD
     */
    public String getFilterFieldEntityDD() {
        return "OTemplRepFilterFieldValue_filterFieldEntity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitLevelIndex">
    @Column(nullable = true)
    private long unitLevelIndex;

    /**
     * @return the unitLevelIndex
     */
    public long getUnitLevelIndex() {
        return unitLevelIndex;
    }

    /**
     * @param unitLevelIndex the unitLevelIndex to set
     */
    public void setUnitLevelIndex(long unitLevelIndex) {
        this.unitLevelIndex = unitLevelIndex;
    }

    /**
     * @return the unitLevelIndexDD
     */
    public String getUnitLevelIndexDD() {
        return "OTemplRepFilterFieldValue_unitLevelIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;//date_to_start

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "Vacation_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;//date_to_start

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "Vacation_endDate";
    }
    // </editor-fold>
}
