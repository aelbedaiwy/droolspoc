/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author lap2
 */
@Entity
public class TempEmployeePostionUnits extends BusinessObjectBaseEntity  {
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeePostionUnits_employee";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="position">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "EmployeePostionUnits_position";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="unit">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitDD() {
        return "EmployeePostionUnits_unit";
    }
    //</editor-fold>
}
