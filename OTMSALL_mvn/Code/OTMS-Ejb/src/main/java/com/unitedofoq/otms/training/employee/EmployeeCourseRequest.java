
package com.unitedofoq.otms.training.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.training.activity.ProviderCourse;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"employee"})
public class EmployeeCourseRequest extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="reason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC reason;

    public void setReason(UDC reason) {
        this.reason = reason;
    }

    public UDC getReason() {
        return reason;
    }

    public String getReasonDD() {
        return "EmployeeCourseRequest_reason";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "EmployeeCourseRequest_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvedByWhom">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee approvedByWhom;

    public void setApprovedByWhom(Employee approvedByWhom) {
        this.approvedByWhom = approvedByWhom;
    }

    public Employee getApprovedByWhom() {
        return approvedByWhom;
    }

    public String getApprovedByWhomDD() {
        return "EmployeeCourseRequest_approvedByWhom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvalDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date approvalDate;

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public String getApprovalDateDD() {
        return "EmployeeCourseRequest_approvalDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approved">
    @Column
    private boolean approved;

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getApprovedDD() {
        return "EmployeeCourseRequest_approved";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public String getRequestDateDD() {
        return "EmployeeCourseRequest_requestDate";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="providerCourse">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourse providerCourse;

    public ProviderCourse getProviderCourse() {
        return providerCourse;
    }

    public void setProviderCourse(ProviderCourse providerCourse) {
        this.providerCourse = providerCourse;
    }
    
    public String getProviderCourseDD() {
        return "EmployeeCourseRequest_providerCourse";
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD() {
        return "EmployeeCourseRequest_employee";
    }

    //</editor-fold>
    
    
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }
    
    public String getM2mCheckDD() {
        return "EmployeeCourseRequest_m2mCheck";
    }
    
    
    
    @Override
    public void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }

}
