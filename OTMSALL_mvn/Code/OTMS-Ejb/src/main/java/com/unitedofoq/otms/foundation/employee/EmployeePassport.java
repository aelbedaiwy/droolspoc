/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.jobGroup.JobGroup;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
/**
 *
 * @author lap
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeePassport extends BusinessObjectBaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="employee">
@JoinColumn(nullable=false)
@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeePassport_employee";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="number">
    @Column(nullable=false, name="passportNum")
    private String number;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getNumberDD() {
            return "EmployeePassport_number";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date issueDate;

        public Date getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(Date issueDate) {
            this.issueDate = issueDate;
        }
        public String getIssueDateDD() {
            return "EmployeePassport_issueDate";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDate">
    @Column(nullable=false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date renewalDate;

        public Date getRenewalDate() {
            return renewalDate;
        }

        public void setRenewalDate(Date renewalDate) {
            this.renewalDate = renewalDate;
        }

        public String getRenewalDateDD() {
            return "EmployeePassport_renewalDate";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issuelocation">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
       private UDC issuelocation;

        public UDC getIssuelocation() {
            return issuelocation;
        }

        public void setIssuelocation(UDC issuelocation) {
            this.issuelocation = issuelocation;
        }

        public String getIssuelocationDD() {
            return "EmployeePassport_issuelocation";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDate">

        @Temporal(javax.persistence.TemporalType.DATE)
        private Date entranceDate;

        public Date getEntranceDate() {
            return entranceDate;
        }

        public void setEntranceDate(Date entranceDate) {
            this.entranceDate = entranceDate;
        }

        public String getEntranceDateDD() {
            return "EmployeePassport_entranceDate";
        }
       // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entrancePort">
        private String entrancePort;

        public String getEntrancePort() {
            return entrancePort;
        }

        public void setEntrancePort(String entrancePort) {
            this.entrancePort = entrancePort;
        }

        public String getEntrancePortDD() {
            return "EmployeePassport_entrancePort";
        }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="changeData">
        @Temporal(javax.persistence.TemporalType.DATE)
        private Date changeData;

        public Date getChangeData() {
            return changeData;
        }

        public void setChangeData(Date changeData) {
            this.changeData = changeData;
        }

         public String getChangeDataDD() {
            return "EmployeePassport_changeData";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="followUp">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private Employee followUp;

        public Employee getFollowUp() {
            return followUp;
        }

        public void setFollowUp(Employee followUp) {
            this.followUp = followUp;
        }

        public String getFollowUpDD() {
            return "EmployeePassport_followUp";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="remarks">
    private String remarks;

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

         public String getRemarksDD() {
            return "EmployeePassport_remarks";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @JoinColumn(nullable=false)
    private UDC status;

        public UDC getStatus() {
            return status;
        }

        public void setStatus(UDC status) {
            this.status = status;
        }
         public String getStatusDD() {
            return "EmployeePassport_status";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

        public UDC getType() {
            return type;
        }

        public void setType(UDC type) {
            this.type = type;
        }
        public String getTypeDD() {
            return "EmployeePassport_type";
        }
    // </editor-fold>

    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private JobGroup jobGroup;

    public JobGroup getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }
     public String getJobGroupDD() {
        return "EmployeePassport_jobGroup";
    }

}
