package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields="employeeValue")
public class EDSEmployeeValueHistory extends BaseEntity {

    private String source;
    private String evaluator;
    private String reference;
    @Temporal(TemporalType.DATE)
    private Date evaluationDate;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private EDSEmployeeValue employeeValue;

    public String getEvaluatorDD()      {    return "EDSEmployeeValueHistory_evaluator";  }
    public String getEvaluationDateDD() {    return "EDSEmployeeValueHistory_evaluationDate";  }
    public String getReferenceDD()      {    return "EDSEmployeeValueHistory_reference";  }
    public String getSourceDD()         {    return "EDSEmployeeValueHistory_source";  }

    public EDSEmployeeValue getEmployeeValue() {
        return employeeValue;
    }

    public void setEmployeeValue(EDSEmployeeValue employeeValue) {
        this.employeeValue = employeeValue;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(String evaluator) {
        this.evaluator = evaluator;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
