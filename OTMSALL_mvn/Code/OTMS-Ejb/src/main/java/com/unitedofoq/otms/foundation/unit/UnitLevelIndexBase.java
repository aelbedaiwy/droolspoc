/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author mmohamed
 */
@Entity
@Table(name="UnitLevelIndex")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DTYPE", discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue(value = "MASTER")

public class UnitLevelIndexBase extends BaseEntity {
    int levelIndex;
    
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    
    @Translation(originalField="description")
    @Transient   
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "UnitLevelIndexBase_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public int getLevelIndex() {
        return levelIndex;
    }

    public String getLevelIndexDD() {
        return "UnitLevelIndexBase_levelIndex";
    }

    public void setLevelIndex(int levelIndex) {
        this.levelIndex = levelIndex;
    }


    
}
