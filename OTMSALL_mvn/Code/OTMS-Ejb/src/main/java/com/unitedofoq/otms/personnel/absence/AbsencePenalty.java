/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.absence;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.dayoff.DayOffPenalty;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"absence"})
@DiscriminatorValue("ABSENCE")
public class AbsencePenalty extends DayOffPenalty {
    // <editor-fold defaultstate="collapsed" desc="abcence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn (nullable=false)
    private Absence absence;

    public Absence getAbsence() {
        return absence;
    }

    public void setAbsence(Absence absence) {
        this.absence = absence;
    }

    
    public String getAbsenceDD() {
        return "AbsencePenalty_absence";
    }
   
// </editor-fold>
 }
