package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeMonthlyConsolidation extends BusinessObjectBaseEntity {
    //<editor-fold defaultstate="collapsed" desc="absenceDays">

    private BigDecimal absenceDays;

    public BigDecimal getAbsenceDays() {
        return absenceDays;
    }

    public void setAbsenceDays(BigDecimal absenceDays) {
        this.absenceDays = absenceDays;
    }

    public String getAbsenceDaysDD() {
        return "EmployeeMonthlyConsolidation_absenceDays";
    }
    @Transient
    private BigDecimal absenceDaysMask;

    public BigDecimal getAbsenceDaysMask() {
        absenceDaysMask = absenceDays;
        return absenceDaysMask;
    }

    public void setAbsenceDaysMask(BigDecimal absenceDaysMask) {
        updateDecimalValue("absenceDays", absenceDaysMask);
    }

    public String getAbsenceDaysMaskDD() {
        return "EmployeeMonthlyConsolidation_absenceDaysMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedAbsenceDays">
    private BigDecimal parametrizedAbsenceDays;

    public BigDecimal getParametrizedAbsenceDays() {
        return parametrizedAbsenceDays;
    }

    public String getParametrizedAbsenceDaysDD() {
        return "EmployeeMonthlyConsolidation_parametrizedAbsenceDays";
    }

    public void setParametrizedAbsenceDays(BigDecimal parametrizedAbsenceDays) {
        this.parametrizedAbsenceDays = parametrizedAbsenceDays;
    }
    @Transient
    private BigDecimal parametrizedAbsenceDaysMask;

    public BigDecimal getParametrizedAbsenceDaysMask() {
        parametrizedAbsenceDaysMask = parametrizedAbsenceDays;
        return parametrizedAbsenceDaysMask;
    }

    public void setParametrizedAbsenceDaysMask(BigDecimal parametrizedAbsenceDaysMask) {
        updateDecimalValue("parametrizedAbsenceDays", parametrizedAbsenceDaysMask);
    }

    public String getParametrizedAbsenceDaysMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedAbsenceDaysMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="postedAbsenceDays">
    private BigDecimal postedAbsenceDays;

    public BigDecimal getPostedAbsenceDays() {
        return postedAbsenceDays;
    }

    public void setPostedAbsenceDays(BigDecimal postedAbsenceDays) {
        this.postedAbsenceDays = postedAbsenceDays;
    }

    public String getPostedAbsenceDaysDD() {
        return "EmployeeMonthlyConsolidation_postedAbsenceDays";
    }
    @Transient
    private BigDecimal postedAbsenceDaysMask;

    public BigDecimal getPostedAbsenceDaysMask() {
        postedAbsenceDaysMask = postedAbsenceDays;
        return postedAbsenceDaysMask;
    }

    public void setPostedAbsenceDaysMask(BigDecimal postedAbsenceDaysMask) {
        updateDecimalValue("postedAbsenceDays", postedAbsenceDaysMask);
    }

    public String getPostedAbsenceDaysMaskDD() {
        return "EmployeeMonthlyConsolidation_postedAbsenceDaysMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="delayHours">
    private BigDecimal delayHours;

    public BigDecimal getDelayHours() {
        return delayHours;
    }

    public void setDelayHours(BigDecimal delayHours) {
        this.delayHours = delayHours;
    }

    public String getDelayHoursDD() {
        return "EmployeeMonthlyConsolidation_delayHours";
    }
    @Transient
    private BigDecimal delayHoursMask;

    public BigDecimal getDelayHoursMask() {
        delayHoursMask = delayHours;
        return delayHoursMask;
    }

    public void setDelayHoursMask(BigDecimal delayHoursMask) {
        updateDecimalValue("delayHours", delayHoursMask);
    }

    public String getDelayHoursMaskDD() {
        return "EmployeeMonthlyConsolidation_delayHoursMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedDelayHours">
    private BigDecimal parametrizedDelayHours;

    public BigDecimal getParametrizedDelayHours() {
        return parametrizedDelayHours;
    }

    public void setParametrizedDelayHours(BigDecimal parametrizedDelayHours) {
        this.parametrizedDelayHours = parametrizedDelayHours;
    }

    public String getParametrizedDelayHoursDD() {
        return "EmployeeMonthlyConsolidation_parametrizedDelayHours";
    }
    @Transient
    private BigDecimal parametrizedDelayHoursMask;

    public BigDecimal getParametrizedDelayHoursMask() {
        parametrizedDelayHoursMask = parametrizedDelayHours;
        return parametrizedDelayHoursMask;
    }

    public void setParametrizedDelayHoursMask(BigDecimal parametrizedDelayHoursMask) {
        updateDecimalValue("parametrizedDelayHours", parametrizedDelayHoursMask);
    }

    public String getParametrizedDelayHoursMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedDelayHoursMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="postedDelayHours">
    private BigDecimal postedDelayHours;

    public BigDecimal getPostedDelayHours() {
        return postedDelayHours;
    }

    public void setPostedDelayHours(BigDecimal postedDelayHours) {
        this.postedDelayHours = postedDelayHours;
    }

    public String getPostedDelayHoursDD() {
        return "EmployeeMonthlyConsolidation_postedDelayHours";
    }
    @Transient
    private BigDecimal postedDelayHoursMask;

    public BigDecimal getPostedDelayHoursMask() {
        postedDelayHoursMask = postedDelayHours;
        return postedDelayHoursMask;
    }

    public void setPostedDelayHoursMask(BigDecimal postedDelayHoursMask) {
        updateDecimalValue("postedDelayHours", postedDelayHoursMask);
    }

    public String getPostedDelayHoursMaskDD() {
        return "EmployeeMonthlyConsolidation_postedDelayHoursMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="earlyLeave">
    private BigDecimal earlyLeave;

    public BigDecimal getEarlyLeave() {
        return earlyLeave;
    }

    public void setEarlyLeave(BigDecimal earlyLeave) {
        this.earlyLeave = earlyLeave;
    }

    public String getEarlyLeaveDD() {
        return "EmployeeMonthlyConsolidation_earlyLeave";
    }
    @Transient
    private BigDecimal earlyLeaveMask;

    public BigDecimal getEarlyLeaveMask() {
        earlyLeaveMask = earlyLeave;
        return earlyLeaveMask;
    }

    public void setEarlyLeaveMask(BigDecimal earlyLeaveMask) {
        updateDecimalValue("earlyLeave", earlyLeaveMask);
    }

    public String getEarlyLeaveMaskDD() {
        return "EmployeeMonthlyConsolidation_earlyLeaveMask";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedEarlyLeave">
    private BigDecimal parametrizedEarlyLeave;

    public BigDecimal getParametrizedEarlyLeave() {
        return parametrizedEarlyLeave;
    }

    public void setParametrizedEarlyLeave(BigDecimal parametrizedEarlyLeave) {
        this.parametrizedEarlyLeave = parametrizedEarlyLeave;
    }

    public String getParametrizedEarlyLeaveDD() {
        return "EmployeeMonthlyConsolidation_parametrizedEarlyLeave";
    }
    @Transient
    private BigDecimal parametrizedEarlyLeaveMask;

    public BigDecimal getParametrizedEarlyLeaveMask() {
        parametrizedEarlyLeaveMask = parametrizedEarlyLeave;
        return parametrizedEarlyLeaveMask;
    }

    public void setParametrizedEarlyLeaveMask(BigDecimal parametrizedEarlyLeaveMask) {
        updateDecimalValue("parametrizedEarlyLeave", parametrizedEarlyLeaveMask);
    }

    public String getParametrizedEarlyLeaveMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedEarlyLeaveMask";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="postedEarlyLeave">
    private BigDecimal postedEarlyLeave;

    public BigDecimal getPostedEarlyLeave() {
        return postedEarlyLeave;
    }

    public void setPostedEarlyLeave(BigDecimal postedEarlyLeave) {
        this.postedEarlyLeave = postedEarlyLeave;
    }

    public String getPostedEarlyLeaveDD() {
        return "EmployeeMonthlyConsolidation_postedEarlyLeave";
    }
    @Transient
    private BigDecimal postedEarlyLeaveMask;

    public BigDecimal getPostedEarlyLeaveMask() {
        postedEarlyLeaveMask = postedEarlyLeave;
        return postedEarlyLeaveMask;
    }

    public void setPostedEarlyLeaveMask(BigDecimal postedEarlyLeaveMask) {
        updateDecimalValue("postedEarlyLeave", postedEarlyLeaveMask);
    }

    public String getPostedEarlyLeaveMaskDD() {
        return "EmployeeMonthlyConsolidation_postedEarlyLeaveMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dayovertime">
    private BigDecimal dayOvertime;

    public BigDecimal getDayOvertime() {
        return dayOvertime;
    }

    public void setDayOvertime(BigDecimal dayOvertime) {
        this.dayOvertime = dayOvertime;
    }

    public String getDayOvertimeDD() {
        return "EmployeeMonthlyConsolidation_dayOvertime";
    }
    @Transient
    private BigDecimal dayOvertimeMask;

    public BigDecimal getDayOvertimeMask() {
        dayOvertimeMask = dayOvertime;
        return dayOvertimeMask;
    }

    public void setDayOvertimeMask(BigDecimal dayOvertimeMask) {
        updateDecimalValue("dayOvertime", dayOvertimeMask);
    }

    public String getDayOvertimeMaskDD() {
        return "EmployeeMonthlyConsolidation_dayOvertimeMask";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizeddayovertime">
    private BigDecimal parametrizedDayOvertime;

    public BigDecimal getParametrizedDayOvertime() {
        return parametrizedDayOvertime;
    }

    public void setParametrizedDayOvertime(BigDecimal parametrizedDayOvertime) {
        this.parametrizedDayOvertime = parametrizedDayOvertime;
    }

    public String getParametrizedDayOvertimeDD() {
        return "EmployeeMonthlyConsolidation_parametrizedDayOvertime";
    }
    @Transient
    private BigDecimal parametrizedDayOvertimeMask;

    public BigDecimal getParametrizedDayOvertimeMask() {
        parametrizedDayOvertimeMask = parametrizedDayOvertime;
        return parametrizedDayOvertimeMask;
    }

    public void setParametrizedDayOvertimeMask(BigDecimal parametrizedDayOvertimeMask) {
        updateDecimalValue("parametrizedDayOvertime", parametrizedDayOvertimeMask);
    }

    public String getParametrizedDayOvertimeMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedDayOvertimeMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="posteddayovertime">
    private BigDecimal postedDayOvertime;

    public BigDecimal getPostedDayOvertime() {
        return postedDayOvertime;
    }

    public void setPostedDayOvertime(BigDecimal postedDayOvertime) {
        this.postedDayOvertime = postedDayOvertime;
    }

    public String getPostedDayOvertimeDD() {
        return "EmployeeMonthlyConsolidation_postedDayOvertime";
    }
    @Transient
    private BigDecimal postedDayOvertimeMask;

    public BigDecimal getPostedDayOvertimeMask() {
        postedDayOvertimeMask = postedDayOvertime;
        return postedDayOvertimeMask;
    }

    public void setPostedDayOvertimeMask(BigDecimal postedDayOvertimeMask) {
        updateDecimalValue("postedDayOvertime", postedDayOvertimeMask);
    }

    public String getPostedDayOvertimeMaskDD() {
        return "EmployeeMonthlyConsolidation_postedDayOvertimeMask";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nightOvertime">
    private BigDecimal nightOvertime;

    public BigDecimal getNightOvertime() {
        return nightOvertime;
    }

    public void setNightOvertime(BigDecimal nightOvertime) {
        this.nightOvertime = nightOvertime;
    }

    public String getNightOvertimeDD() {
        return "EmployeeMonthlyConsolidation_nightOvertime";
    }
    @Transient
    private BigDecimal nightOvertimeMask;

    public BigDecimal getNightOvertimeMask() {
        nightOvertimeMask = nightOvertime;
        return nightOvertimeMask;
    }

    public void setNightOvertimeMask(BigDecimal nightOvertimeMask) {
        updateDecimalValue("nightOvertime", nightOvertimeMask);
    }

    public String getNightOvertimeMaskDD() {
        return "EmployeeMonthlyConsolidation_nightOvertimeMask";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedNightOvertime">
    private BigDecimal parametrizedNightOvertime;

    public BigDecimal getParametrizedNightOvertime() {
        return parametrizedNightOvertime;
    }

    public void setParametrizedNightOvertime(BigDecimal parametrizedNightOvertime) {
        this.parametrizedNightOvertime = parametrizedNightOvertime;
    }

    public String getParametrizedNightOvertimeDD() {
        return "EmployeeMonthlyConsolidation_parametrizedNightOvertime";
    }
    @Transient
    private BigDecimal parametrizedNightOvertimeMask;

    public BigDecimal getParametrizedNightOvertimeMask() {
        parametrizedNightOvertimeMask = parametrizedNightOvertime;
        return parametrizedNightOvertimeMask;
    }

    public void setParametrizedNightOvertimeMask(BigDecimal parametrizedNightOvertimeMask) {
        updateDecimalValue("parametrizedNightOvertime", parametrizedNightOvertimeMask);
    }

    public String getParametrizedNightOvertimeMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedNightOvertimeMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="postedNightOvertime">
    private BigDecimal postedNightOvertime;

    public BigDecimal getPostedNightOvertime() {
        return postedNightOvertime;
    }

    public void setPostedNightOvertime(BigDecimal postedNightOvertime) {
        this.postedNightOvertime = postedNightOvertime;
    }

    public String getPostedNightOvertimeDD() {
        return "EmployeeMonthlyConsolidation_postedNightOvertime";
    }
    @Transient
    private BigDecimal postedNightOvertimeMask;

    public BigDecimal getPostedNightOvertimeMask() {
        postedNightOvertimeMask = postedNightOvertime;
        return postedNightOvertimeMask;
    }

    public void setPostedNightOvertimeMask(BigDecimal postedNightOvertimeMask) {
        updateDecimalValue("postedNightOvertime", postedNightOvertimeMask);
    }

    public String getPostedNightOvertimeMaskDD() {
        return "EmployeeMonthlyConsolidation_postedNightOvertimeMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="holiday">
    private BigDecimal holiday;

    public BigDecimal getHoliday() {
        return holiday;
    }

    public void setHoliday(BigDecimal holiday) {
        this.holiday = holiday;
    }

    public String getHolidayDD() {
        return "EmployeeMonthlyConsolidation_holiday";
    }
    @Transient
    private BigDecimal holidayMask;

    public BigDecimal getHolidayMask() {
        holidayMask = holiday;
        return holidayMask;
    }

    public void setHolidayMask(BigDecimal holidayMask) {
        updateDecimalValue("holiday", holidayMask);
    }

    public String getHolidayMaskDD() {
        return "EmployeeMonthlyConsolidation_holidayMask";
    }
    //</editor-fold> // holiday OT
    //<editor-fold defaultstate="collapsed" desc="parametrizedHoliday">
    private BigDecimal parametrizedHoliday;

    public BigDecimal getParametrizedHoliday() {
        return parametrizedHoliday;
    }

    public void setParametrizedHoliday(BigDecimal parametrizedHoliday) {
        this.parametrizedHoliday = parametrizedHoliday;
    }

    public String getParametrizedHolidayDD() {
        return "EmployeeMonthlyConsolidation_parametrizedHoliday";
    }
    @Transient
    private BigDecimal parametrizedHolidayMask;

    public BigDecimal getParametrizedHolidayMask() {
        parametrizedHolidayMask = parametrizedHoliday;
        return parametrizedHolidayMask;
    }

    public void setParametrizedHolidayMask(BigDecimal parametrizedHolidayMask) {
        updateDecimalValue("parametrizedHoliday", parametrizedHolidayMask);
    }

    public String getParametrizedHolidayMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedHolidayMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="postedHoliday">
    private BigDecimal postedHoliday;

    public BigDecimal getPostedHoliday() {
        return postedHoliday;
    }

    public void setPostedHoliday(BigDecimal postedHoliday) {
        this.postedHoliday = postedHoliday;
    }

    public String getPostedHolidayDD() {
        return "EmployeeMonthlyConsolidation_postedHoliday";
    }
    @Transient
    private BigDecimal postedHolidayMask;

    public BigDecimal getPostedHolidayMask() {
        postedHolidayMask = postedHoliday;
        return postedHolidayMask;
    }

    public void setPostedHolidayMask(BigDecimal postedHolidayMask) {
        updateDecimalValue("postedHoliday", postedHolidayMask);
    }

    public String getPostedHolidayMaskDD() {
        return "EmployeeMonthlyConsolidation_postedHolidayMask";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="weekend">
    private BigDecimal weekend;

    public BigDecimal getWeekend() {
        return weekend;
    }

    public void setWeekend(BigDecimal weekend) {
        this.weekend = weekend;
    }

    public String getWeekendDD() {
        return "EmployeeMonthlyConsolidation_weekend";
    }
    @Transient
    private BigDecimal weekendMask;

    public BigDecimal getWeekendMask() {
        weekendMask = weekend;
        return weekendMask;
    }

    public void setWeekendMask(BigDecimal weekendMask) {
        updateDecimalValue("weekend", weekendMask);
    }

    public String getWeekendMaskDD() {
        return "EmployeeMonthlyConsolidation_weekendMask";
    }
    //</editor-fold> // weekend OT
    //<editor-fold defaultstate="collapsed" desc="parametrizedWeekend">
    private BigDecimal parametrizedWeekend;

    public BigDecimal getParametrizedWeekend() {
        return parametrizedWeekend;
    }

    public void setParametrizedWeekend(BigDecimal parametrizedWeekend) {
        this.parametrizedWeekend = parametrizedWeekend;
    }

    public String getParametrizedWeekendDD() {
        return "EmployeeMonthlyConsolidation_parametrizedWeekend";
    }
    @Transient
    private BigDecimal parametrizedWeekendMask;

    public BigDecimal getParametrizedWeekendMask() {
        parametrizedWeekendMask = parametrizedWeekend;
        return parametrizedWeekendMask;
    }

    public void setParametrizedWeekendMask(BigDecimal parametrizedWeekendMask) {
        updateDecimalValue("parametrizedWeekend", parametrizedWeekendMask);
    }

    public String getParametrizedWeekendMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedWeekendMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="postedWeekend">
    private BigDecimal postedWeekend;

    public BigDecimal getPostedWeekend() {
        return postedWeekend;
    }

    public void setPostedWeekend(BigDecimal postedWeekend) {
        this.postedWeekend = postedWeekend;
    }

    public String getPostedWeekendDD() {
        return "EmployeeMonthlyConsolidation_postedWeekend";
    }
    @Transient
    private BigDecimal postedWeekendMask;

    public BigDecimal getPostedWeekendMask() {
        postedWeekendMask = postedWeekend;
        return postedWeekendMask;
    }

    public void setPostedWeekendMask(BigDecimal postedWeekendMask) {
        updateDecimalValue("postedWeekend", postedWeekendMask);
    }

    public String getPostedWeekendMaskDD() {
        return "EmployeeMonthlyConsolidation_postedWeekendMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="workingDays">
    private BigDecimal workingDays;

    public BigDecimal getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(BigDecimal workingDays) {
        this.workingDays = workingDays;
    }

    public String getWorkingDaysDD() {
        return "EmployeeMonthlyConsolidation_workingDays";
    }
    @Transient
    private BigDecimal workingDaysMask;

    public BigDecimal getWorkingDaysMask() {
        workingDaysMask = workingDays;
        return workingDaysMask;
    }

    public void setWorkingDaysMask(BigDecimal workingDaysMask) {
        updateDecimalValue("workingDays", workingDaysMask);
    }

    public String getWorkingDaysMaskDD() {
        return "EmployeeMonthlyConsolidation_workingDaysMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="missions">
    private int missions;

    public int getMissions() {
        return missions;
    }

    public void setMissions(int missions) {
        this.missions = missions;
    }

    public String getMissionsDD() {
        return "EmployeeMonthlyConsolidation_missions";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="vacations">
    private BigDecimal vacations;

    public BigDecimal getVacations() {
        return vacations;
    }

    public void setVacations(BigDecimal vacations) {
        this.vacations = vacations;
    }

    public String getVacationsDD() {
        return "EmployeeMonthlyConsolidation_vacations";
    }
    @Transient
    private BigDecimal vacationsMask;

    public BigDecimal getVacationsMask() {
        vacationsMask = vacations;
        return vacationsMask;
    }

    public void setVacationsMask(BigDecimal vacationsMask) {
        updateDecimalValue("vacations", vacationsMask);
    }

    public String getVacationsMaskDD() {
        return "EmployeeMonthlyConsolidation_vacationsMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeMonthlyConsolidation_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }

    public String getCalculatedPeriodDD() {
        return "EmployeeMonthlyConsolidation_calculatedPeriod";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="posted">
    @Column(length = 1)
    private String posted;

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getPostedDD() {
        return "EmployeeMonthlyConsolidation_posted";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingHours">
    private BigDecimal workingHours;

    public BigDecimal getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(BigDecimal workingHours) {
        this.workingHours = workingHours;
    }

    public String getWorkingHoursDD() {
        return "EmployeeMonthlyConsolidation_workingHours";
    }
    @Transient
    private BigDecimal workingHoursMask;

    public BigDecimal getWorkingHoursMask() {
        workingHoursMask = workingHours;
        return workingHoursMask;
    }

    public void setWorkingHoursMask(BigDecimal workingHoursMask) {
        updateDecimalValue("workingHours", workingHoursMask);
    }

    public String getWorkingHoursMaskDD() {
        return "EmployeeMonthlyConsolidation_workingHoursMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingOnWeekEndDays">
    private int workingOnWeekEndDays;

    public int getWorkingOnWeekEndDays() {
        return workingOnWeekEndDays;
    }

    public void setWorkingOnWeekEndDays(int workingOnWeekEndDays) {
        this.workingOnWeekEndDays = workingOnWeekEndDays;
    }

    public String getWorkingOnWeekEndDaysDD() {
        return "EmployeeMonthlyConsolidation_workingOnWeekEndDays";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingOnHolidayDays">
    private int workingOnHolidayDays;

    public int getWorkingOnHolidayDays() {
        return workingOnHolidayDays;
    }

    public void setWorkingOnHolidayDays(int workingOnHolidayDays) {
        this.workingOnHolidayDays = workingOnHolidayDays;
    }

    public String getWorkingOnHolidayDaysDD() {
        return "EmployeeMonthlyConsolidation_workingOnHolidayDays";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actualWorkingDays">
    private int actualWorkingDays;

    public int getActualWorkingDays() {
        return actualWorkingDays;
    }

    public void setActualWorkingDays(int actualWorkingDays) {
        this.actualWorkingDays = actualWorkingDays;
    }

    public String getActualWorkingDaysDD() {
        return "EmployeeMonthlyConsolidation_actualWorkingDays";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="holidays">
    private int holidays;

    public int getHolidays() {
        return holidays;
    }

    public String getHolidaysDD() {
        return "EmployeeMonthlyConsolidation_holidays";
    }

    public void setHolidays(int holidays) {
        this.holidays = holidays;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="weekendDays">
    private int weekEndDays;

    public int getWeekEndDays() {
        return weekEndDays;
    }

    public void setWeekEndDays(int weekEndDays) {
        this.weekEndDays = weekEndDays;
    }

    public String getWeekEndDaysDD() {
        return "EmployeeMonthlyConsolidation_weekEndDays";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapse" desc="missionHours">
    private BigDecimal missionHours;

    public BigDecimal getMissionHours() {
        return missionHours;
    }

    public void setMissionHours(BigDecimal missionHours) {
        this.missionHours = missionHours;
    }

    public String getMissionHoursDD() {
        return "EmployeeMonthlyConsolidation_missionHours";
    }
    @Transient
    private BigDecimal missionHoursMask;

    public BigDecimal getMissionHoursMask() {
        missionHoursMask = missionHours;
        return missionHoursMask;
    }

    public void setMissionHoursMask(BigDecimal missionHoursMask) {
        updateDecimalValue("missionHours", missionHoursMask);
    }

    public String getMissionHoursMaskDD() {
        return "EmployeeMonthlyConsolidation_missionHoursMask";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="normalOT">
    private BigDecimal normalOT;

    public BigDecimal getNormalOT() {
        return normalOT;
    }

    public String getNormalOTDD() {
        return "EmployeeMonthlyConsolidation_normalOT";
    }

    public void setNormalOT(BigDecimal normalOT) {
        this.normalOT = normalOT;
    }
    @Transient
    private BigDecimal normalOTMask;

    public BigDecimal getNormalOTMask() {
        normalOTMask = normalOT;
        return normalOTMask;
    }

    public String getNormalOTMaskDD() {
        return "EmployeeMonthlyConsolidation_normalOTMask";
    }

    public void setNormalOTMask(BigDecimal normalOTMask) {
        updateDecimalValue("normalOT", normalOTMask);
    }
    // </editor-fold >

    //<editor-fold defaultstate="collapsed" desc="lessWork">
    private BigDecimal lessWork;

    public BigDecimal getLessWork() {
        return lessWork;
    }

    public void setLessWork(BigDecimal lessWork) {
        this.lessWork = lessWork;
    }

    public String getLessWorkDD() {
        return "EmployeeMonthlyConsolidation_lessWork";
    }
    @Transient
    private BigDecimal lessWorkMask;

    public BigDecimal getLessWorkMask() {
        lessWorkMask = lessWork;
        return lessWorkMask;
    }

    public void setLessWorkMask(BigDecimal lessWorkMask) {
        updateDecimalValue("lessWork", lessWorkMask);
    }

    public String getLessWorkMaskDD() {
        return "EmployeeMonthlyConsolidation_lessWorkMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedLessWork">
    private BigDecimal parametrizedLessWork;

    public BigDecimal getParametrizedLessWork() {
        return parametrizedLessWork;
    }

    public void setParametrizedLessWork(BigDecimal parametrizedLessWork) {
        this.parametrizedLessWork = parametrizedLessWork;
    }

    public String getParametrizedLessWorkDD() {
        return "EmployeeMonthlyConsolidation_parametrizedLessWork";
    }
    @Transient
    private BigDecimal parametrizedLessWorkMask;

    public BigDecimal getParametrizedLessWorkMask() {
        parametrizedLessWorkMask = parametrizedLessWork;
        return parametrizedLessWorkMask;
    }

    public void setParametrizedLessWorkMask(BigDecimal parametrizedLessWorkMask) {
        updateDecimalValue("parametrizedLessWork", parametrizedLessWorkMask);
    }

    public String getParametrizedLessWorkMaskDD() {
        return "EmployeeMonthlyConsolidation_parametrizedLessWorkMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="postedLessWork">
    private BigDecimal postedLessWork;

    public BigDecimal getPostedLessWork() {
        return postedLessWork;
    }

    public void setPostedLessWork(BigDecimal postedLessWork) {
        this.postedLessWork = postedLessWork;
    }

    public String getPostedLessWorkDD() {
        return "EmployeeMonthlyConsolidation_postedLessWork";
    }
    @Transient
    private BigDecimal postedLessWorkMask;

    public BigDecimal getPostedLessWorkMask() {
        postedLessWorkMask = postedLessWork;
        return postedLessWorkMask;
    }

    public void setPostedLessWorkMask(BigDecimal postedLessWorkMask) {
        updateDecimalValue("postedLessWork", postedLessWorkMask);
    }

    public String getPostedLessWorkMaskDD() {
        return "EmployeeMonthlyConsolidation_postedLessWorkMask";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deductedPenaltyDays">
    private BigDecimal deductedPenaltyDays;

    public BigDecimal getDeductedPenaltyDays() {
        return deductedPenaltyDays;
    }

    public void setDeductedPenaltyDays(BigDecimal deductedPenaltyDays) {
        this.deductedPenaltyDays = deductedPenaltyDays;
    }
    @Transient
    private BigDecimal deductedPenaltyDaysMask;

    public BigDecimal getDeductedPenaltyDaysMask() {
        deductedPenaltyDaysMask = deductedPenaltyDays;
        return deductedPenaltyDaysMask;
    }

    public void setDeductedPenaltyDaysMask(BigDecimal deductedPenaltyDaysMask) {
        updateDecimalValue("deductedPenaltyDays", deductedPenaltyDaysMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deductedAnnualDays">
    private BigDecimal deductedAnnualDays;

    public BigDecimal getDeductedAnnualDays() {
        return deductedAnnualDays;
    }

    public void setDeductedAnnualDays(BigDecimal deductedAnnualDays) {
        this.deductedAnnualDays = deductedAnnualDays;
    }
    @Transient
    private BigDecimal deductedAnnualDaysMask;

    public BigDecimal getDeductedAnnualDaysMask() {
        deductedAnnualDaysMask = deductedAnnualDays;
        return deductedAnnualDaysMask;
    }

    public void setDeductedAnnualDaysMask(BigDecimal deductedAnnualDaysMask) {
        updateDecimalValue("deductedAnnualDays", deductedAnnualDaysMask);
    }
    // </editor-fold>

    private BigDecimal dayLessWork;

    public BigDecimal getDayLessWork() {
        return dayLessWork;
    }

    public void setDayLessWork(BigDecimal dayLessWork) {
        this.dayLessWork = dayLessWork;
    }

    public String getDayLessWorkDD() {
        return "EmployeeMonthlyConsolidation_dayLessWork";
    }

    private BigDecimal dayWEOT;
    @Transient
    private BigDecimal dayWEOTMask;
    private BigDecimal nightWEOT;
    @Transient
    private BigDecimal nightWEOTMask;

    public BigDecimal getDayWEOT() {
        return dayWEOT;
    }

    public void setDayWEOT(BigDecimal dayWEOT) {
        this.dayWEOT = dayWEOT;
    }

    public String getDayWEOTDD() {
        return "EmployeeMonthlyConsolidation_dayWEOT";
    }

    public BigDecimal getDayWEOTMask() {
        dayWEOTMask = dayWEOT;
        return dayWEOTMask;
    }

    public void setDayWEOTMask(BigDecimal dayWEOTMask) {
        updateDecimalValue("dayWEOT", dayWEOTMask);
    }

    public String getDayWEOTMaskDD() {
        return "EmployeeMonthlyConsolidation_dayWEOTMask";
    }

    public BigDecimal getNightWEOT() {
        return nightWEOT;
    }

    public void setNightWEOT(BigDecimal nightWEOT) {
        this.nightWEOT = nightWEOT;
    }

    public String getNightWEOTDD() {
        return "EmployeeMonthlyConsolidation_nightWEOT";
    }

    public BigDecimal getNightWEOTMask() {
        nightWEOTMask = nightWEOT;
        return nightWEOTMask;
    }

    public void setNightWEOTMask(BigDecimal nightWEOTMask) {
        this.nightWEOTMask = nightWEOTMask;
    }

    public String getNightWEOTMaskDD() {
        return "EmployeeMonthlyConsolidation_nightWEOTMask";
    }

    private BigDecimal weOT;
    @Transient
    private BigDecimal weOTMask;

    public BigDecimal getWeOT() {
        return weOT;
    }

    public void setWeOT(BigDecimal weOT) {
        this.weOT = weOT;
    }

    public String getWeOTDD() {
        return "EmployeeMonthlyConsolidation_weOT";
    }

    public BigDecimal getWeOTMask() {
        weOTMask = weOT;
        return weOTMask;
    }

    public void setWeOTMask(BigDecimal weOTMask) {
        updateDecimalValue("weOT", weOTMask);
    }

    public String getWeOTMaskDD() {
        return "EmployeeMonthlyConsolidation_weOTMask";
    }
    private BigDecimal holidayPayrollOT;
    @Transient
    private BigDecimal holidayPayrollOTMask;

    public BigDecimal getHolidayPayrollOT() {
        return holidayPayrollOT;
    }

    public void setHolidayPayrollOT(BigDecimal holidayPayrollOT) {
        this.holidayPayrollOT = holidayPayrollOT;
    }

    public String getHolidayPayrollOTDD() {
        return "EmployeeMonthlyConsolidation_holidayPayrollOT";
    }

    public BigDecimal getHolidayPayrollOTMask() {
        holidayPayrollOTMask = holidayPayrollOT;
        return holidayPayrollOTMask;
    }

    public void setHolidayPayrollOTMask(BigDecimal holidayPayrollOT) {
        updateDecimalValue("holidayPayrollOT", holidayPayrollOTMask);
    }

    public String getHolidayPayrollOTMaskDD() {
        return "EmployeeMonthlyConsolidation_holidayPayrollOTMask";
    }
}
