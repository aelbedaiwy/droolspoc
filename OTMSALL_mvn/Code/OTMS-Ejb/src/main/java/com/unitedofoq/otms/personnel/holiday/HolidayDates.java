/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.holiday;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"holiday"})
//@Table(name = "holiday_dates")
public class HolidayDates extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="holiday">
     @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Holiday holiday;

    public Holiday getHoliday() {
        return holiday;
    }

    public void setHoliday(Holiday holiday) {
        this.holiday = holiday;
    }

    public String getHolidayDD() {
        return "HolidayDates_holiday";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startFrom">
    @Column(nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startFrom;

    public Date getStartFrom() {
        return startFrom;
    }

    public void setStartFrom(Date startFrom) {
        this.startFrom = startFrom;
    }
    public String getStartFromDD() {
        return "HolidayDates_startFrom";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endTo">

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTo;

    public Date getEndTo() {
        return endTo;
    }

    public void setEndTo(Date endTo) {
        this.endTo = endTo;
    }
    public String getEndToDD() {
        return "HolidayDates_endTo";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="note">
    @Column
    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    public String getNoteDD() {
        return "HolidayDates_note";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paid">

    @Column
    private String paid;

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

     public String getPaidDD() {
        return "HolidayDates_paid";
    }
 // </editor-fold>


}
