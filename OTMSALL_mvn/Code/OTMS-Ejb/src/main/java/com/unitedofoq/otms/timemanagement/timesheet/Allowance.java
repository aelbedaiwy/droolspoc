package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = "company")
public class Allowance extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Allowance_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="id">
    @Column
    private String id;

    public String getId() {
        return id;
    }

    public String getIdDD() {
        return "Allowance_id";
    }

    public void setId(String id) {
        this.id = id;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "Allowance_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="income">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;

    public Income getIncome() {
        return income;
    }

    public String getIncomeDD() {
        return "Allowance_income";
    }

    public void setIncome(Income income) {
        this.income = income;
    }
    // </editor-fold >
}
