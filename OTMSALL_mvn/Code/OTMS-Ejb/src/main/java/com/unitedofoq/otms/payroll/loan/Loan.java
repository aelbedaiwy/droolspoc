package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import com.unitedofoq.otms.payroll.formula.FormulaDistinctName;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "OLoan")
@ParentEntity(fields = "company")
@ChildEntity(fields = "loanMaximumValues")
public class Loan extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Loan_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescription() {
        return description;
    }

    public String getDescriptionTranslatedDD() {
        return "Loan_descriptionTranslated";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="needApproval">
    @Column
    private boolean needApproval = true;

    public boolean isNeedApproval() {
        return needApproval;
    }

    public void setNeedApproval(boolean needApproval) {
        this.needApproval = needApproval;
    }

    public String getNeedApprovalDD() {
        return "Loan_needApproval";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="multiOpenLoan">
    @Column(length = 1)//R & A & N
    private String multiOpenLoan;

    public String getMultiOpenLoan() {
        return multiOpenLoan;
    }

    public void setMultiOpenLoan(String multiOpenLoan) {
        this.multiOpenLoan = multiOpenLoan;
    }

    public String getMultiOpenLoanDD() {
        return "Loan_multiOpenLoan";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumValue">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal minimumValue;

    public BigDecimal getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(BigDecimal minimumValue) {
        this.minimumValue = minimumValue;
    }

    public String getMinimumValueDD() {
        return "Loan_minimumValue";
    }
    @Transient
    private BigDecimal minimumValueMask;

    public BigDecimal getMinimumValueMask() {
        minimumValueMask = minimumValue;
        return minimumValueMask;
    }

    public void setMinimumValueMask(BigDecimal minimumValueMask) {
        updateDecimalValue("minimumValue", minimumValueMask);
    }

    public String getMinimumValueMaskDD() {
        return "Loan_minimumValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "Loan_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue", maximumValueMask);
    }

    public String getMaximumValueMaskDD() {
        return "Loan_maximumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee approvedBy;

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "Loan_approvedBy";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="refreshUnit">
    @Column(length = 1)//M Or Y
    private String refreshUnit;

    public String getRefreshUnit() {
        return refreshUnit;
    }

    public void setRefreshUnit(String refreshUnit) {
        this.refreshUnit = refreshUnit;
    }

    public String getRefreshUnitDD() {
        return "Loan_refreshUnit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshPeriod">
    @Column(precision = 25, scale = 13)
    private BigDecimal refreshPeriod;

    public BigDecimal getRefreshPeriod() {
        return refreshPeriod;
    }

    public void setRefreshPeriod(BigDecimal refreshPeriod) {
        this.refreshPeriod = refreshPeriod;
    }

    public String getRefreshPeriodDD() {
        return "Loan_refreshPeriod";
    }
    @Transient
    private BigDecimal refreshPeriodMask;

    public BigDecimal getRefreshPeriodMask() {
        refreshPeriodMask = refreshPeriod;
        return refreshPeriodMask;
    }

    public void setRefreshPeriodMask(BigDecimal refreshPeriodMask) {
        updateDecimalValue("refreshPeriod", refreshPeriodMask);
    }

    public BigDecimal getRefreshPeriodMaskDD() {
        return refreshPeriodMask;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="incomePercent">
    @Column(precision = 25, scale = 13)
    private BigDecimal incomePercent;

    public BigDecimal getIncomePercent() {
        return incomePercent;
    }

    public void setIncomePercent(BigDecimal incomePercent) {
        this.incomePercent = incomePercent;
    }

    public String getIncomePercentDD() {
        return "Loan_incomePercent";
    }
    @Transient
    private BigDecimal incomePercentMask;

    public BigDecimal getIncomePercentMask() {
        incomePercentMask = incomePercent;
        return incomePercentMask;
    }

    public void setIncomePercentMask(BigDecimal incomePercentMask) {
        updateDecimalValue("incomePercent", incomePercentMask);
    }

    public String getIncomePercentMaskDD() {
        return "Loan_incomePercentMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="income">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }

    public String getIncomeDD() {
        return "Loan_income";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="formulaReplace">
    @Column(length = 1)//I & P & F
    private String formulaReplace;

    public String getFormulaReplace() {
        return formulaReplace;
    }

    public void setFormulaReplace(String formulaReplace) {
        this.formulaReplace = formulaReplace;
    }

    public String getFormulaReplaceDD() {
        return "Loan_formulaReplace";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="formula">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName formula;

    public FormulaDistinctName getFormula() {
        return formula;
    }

    public void setFormula(FormulaDistinctName formula) {
        this.formula = formula;
    }

    public String getFormulaDD() {
        return "Loan_formula";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }

    public String getDeductionDD() {
        return "Loan_deduction";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="minInstallmentNo">
    @Column(precision = 25, scale = 13)
    private BigDecimal minInstallmentNo;

    public BigDecimal getMinInstallmentNo() {
        return minInstallmentNo;
    }

    public void setMinInstallmentNo(BigDecimal minInstallmentNo) {
        this.minInstallmentNo = minInstallmentNo;
    }

    public String getMinInstallmentNoDD() {
        return "Loan_minInstallmentNo";
    }
    @Transient
    private BigDecimal minInstallmentNoMask;

    public BigDecimal getMinInstallmentNoMask() {
        minInstallmentNoMask = minInstallmentNo;
        return minInstallmentNoMask;
    }

    public void setMinInstallmentNoMask(BigDecimal minInstallmentNoMask) {
        updateDecimalValue("minInstallmentNo", minInstallmentNoMask);
    }

    public String getMinInstallmentNoMaskDD() {
        return "Loan_minInstallmentNoMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxInstallmentNo">
    @Column(precision = 25, scale = 13)
    private BigDecimal maxInstallmentNo;

    public BigDecimal getMaxInstallmentNo() {
        return maxInstallmentNo;
    }

    public void setMaxInstallmentNo(BigDecimal maxInstallmentNo) {
        this.maxInstallmentNo = maxInstallmentNo;
    }

    public String getMaxInstallmentNoDD() {
        return "Loan_maxInstallmentNo";
    }
    @Transient
    private BigDecimal maxInstallmentNoMask;

    public BigDecimal getMaxInstallmentNoMask() {
        maxInstallmentNoMask = maxInstallmentNo;
        return maxInstallmentNoMask;
    }

    public void setMaxInstallmentNoMask(BigDecimal maxInstallmentNoMask) {
        updateDecimalValue("maxInstallmentNo", maxInstallmentNoMask);
    }

    public String getMaxInstallmentNoMaskDD() {
        return "Loan_maxInstallmentNoMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="installNoFormula">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName installNoFormula;

    public FormulaDistinctName getInstallNoFormula() {
        return installNoFormula;
    }

    public void setInstallNoFormula(FormulaDistinctName installNoFormula) {
        this.installNoFormula = installNoFormula;
    }

    public String getInstallNoFormulaDD() {
        return "Loan_installNoFormula";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="minMonthValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal minMonthValue;

    public BigDecimal getMinMonthValue() {
        return minMonthValue;
    }

    public void setMinMonthValue(BigDecimal minMonthValue) {
        this.minMonthValue = minMonthValue;
    }

    public String getMinMonthValueDD() {
        return "Loan_minMonthValue";
    }
    @Transient
    private BigDecimal minMonthValueMask;

    public BigDecimal getMinMonthValueMask() {
        minMonthValueMask = minMonthValue;
        return minMonthValueMask;
    }

    public void setMinMonthValueMask(BigDecimal minMonthValueMask) {
        updateDecimalValue("minMonthValue", minMonthValueMask);
    }

    public String getMinMonthValueMaskDD() {
        return "Loan_minMonthValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxMonthValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal maxMonthValue;

    public BigDecimal getMaxMonthValue() {
        return maxMonthValue;
    }

    public void setMaxMonthValue(BigDecimal maxMonthValue) {
        this.maxMonthValue = maxMonthValue;
    }

    public String getMaxMonthValueDD() {
        return "Loan_maxMonthValue";
    }
    @Transient
    private BigDecimal maxMonthValueMask;

    public BigDecimal getMaxMonthValueMask() {
        maxMonthValueMask = maxMonthValue;
        return maxMonthValueMask;
    }

    public void setMaxMonthValueMask(BigDecimal maxMonthValueMask) {
        updateDecimalValue("maxMonthValue", maxMonthValueMask);
    }

    public String getMaxMonthValueMaskDD() {
        return "Loan_maxMonthValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValueFormula">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName monthValueFormula;

    public FormulaDistinctName getMonthValueFormula() {
        return monthValueFormula;
    }

    public void setMonthValueFormula(FormulaDistinctName monthValueFormula) {
        this.monthValueFormula = monthValueFormula;
    }

    public String getMonthValueFormulaDD() {
        return "Loan_monthValueFormula";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValueMinPercent">
    @Column(precision = 25, scale = 13)
    private BigDecimal monthValueMinPercent;

    public BigDecimal getMonthValueMinPercent() {
        return monthValueMinPercent;
    }

    public void setMonthValueMinPercent(BigDecimal monthValueMinPercent) {
        this.monthValueMinPercent = monthValueMinPercent;
    }

    public String getMonthValueMinPercentDD() {
        return "Loan_monthValueMinPercent";
    }
    @Transient
    private BigDecimal monthValueMinPercentMask;

    public BigDecimal getMonthValueMinPercentMask() {
        monthValueMinPercentMask = monthValueMinPercent;
        return monthValueMinPercentMask;
    }

    public void setMonthValueMinPercentMask(BigDecimal monthValueMinPercentMask) {
        updateDecimalValue("monthValueMinPercent", monthValueMinPercentMask);
    }

    public String getMonthValueMinPercentMaskDD() {
        return "Loan_monthValueMinPercentMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="monthValueMaxPercent">
    @Column(precision = 25, scale = 13)
    private BigDecimal monthValueMaxPercent;

    public BigDecimal getMonthValueMaxPercent() {
        return monthValueMaxPercent;
    }

    public void setMonthValueMaxPercent(BigDecimal monthValueMaxPercent) {
        this.monthValueMaxPercent = monthValueMaxPercent;
    }

    public String getMonthValueMaxPercentDD() {
        return "Loan_monthValueMaxPercent";
    }
    @Transient
    private BigDecimal monthValueMaxPercentMask;

    public BigDecimal getMonthValueMaxPercentMask() {
        monthValueMaxPercentMask = monthValueMaxPercent;
        return monthValueMaxPercentMask;
    }

    public void setMonthValueMaxPercentMask(BigDecimal monthValueMaxPercentMask) {
        updateDecimalValue("monthValueMaxPercent", monthValueMaxPercentMask);
    }

    public String getMonthValueMaxPercentMaskDD() {
        return "Loan_monthValueMaxPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValueIncome">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income monthValueIncome;

    public Income getMonthValueIncome() {
        return monthValueIncome;
    }

    public void setMonthValueIncome(Income monthValueIncome) {
        this.monthValueIncome = monthValueIncome;
    }

    public String getMonthValueIncomeDD() {
        return "Loan_monthValueIncome";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deductionPercent">
    @Column(precision = 25, scale = 13)
    private BigDecimal deductionPercent;//Not Used

    public BigDecimal getDeductionPercent() {
        return deductionPercent;
    }

    public void setDeductionPercent(BigDecimal deductionPercent) {
        this.deductionPercent = deductionPercent;
    }

    public String getDeductionPercentDD() {
        return "Loan_deductionPercent";
    }
    @Transient
    private BigDecimal deductionPercentMask;

    public BigDecimal getDeductionPercentMask() {
        return deductionPercentMask;
    }

    public void setDeductionPercentMask(BigDecimal deductionPercentMask) {
        updateDecimalValue("deductionPercent", deductionPercentMask);
    }

    public String getDeductionPercentMaskDD() {
        return "Loan_deductionPercentMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="loanOrInstallment">
    @Column(length = 1)
    private String loanOrInstallment = "L";

    public String getLoanOrInstallment() {
        return loanOrInstallment;
    }

    public void setLoanOrInstallment(String loanOrInstallment) {
        this.loanOrInstallment = loanOrInstallment;
    }

    public String getLoanOrInstallmentDD() {
        return "Loan_loanOrInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="automaticSetDeduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction automaticSetDeduction;

    public Deduction getAutomaticSetDeduction() {
        return automaticSetDeduction;
    }

    public void setAutomaticSetDeduction(Deduction automaticSetDeduction) {
        this.automaticSetDeduction = automaticSetDeduction;
    }

    public String getAutomaticSetDeductionDD() {
        return "Loan_automaticSetDeduction";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="automaticSetIncome">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income automaticSetIncome;

    public Income getAutomaticSetIncome() {
        return automaticSetIncome;
    }

    public void setAutomaticSetIncome(Income automaticSetIncome) {
        this.automaticSetIncome = automaticSetIncome;
    }

    public String getAutomaticSetIncomeDD() {
        return "Loan_automaticSetIncome";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="automaticSetPercentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal automaticSetPercentage;

    public BigDecimal getAutomaticSetPercentage() {
        return automaticSetPercentage;
    }

    public void setAutomaticSetPercentage(BigDecimal automaticSetPercentage) {
        this.automaticSetPercentage = automaticSetPercentage;
    }

    public String getAutomaticSetPercentageDD() {
        return "Loan_automaticSetPercentage";
    }
    @Transient
    private BigDecimal automaticSetPercentageMask;

    public BigDecimal getAutomaticSetPercentageMask() {
        automaticSetPercentageMask = automaticSetPercentage;
        return automaticSetPercentageMask;
    }

    public void setAutomaticSetPercentageMask(BigDecimal automaticSetPercentageMask) {
        updateDecimalValue("automaticSetPercentage", automaticSetPercentageMask);
    }

    public String getAutomaticSetPercentageMaskDD() {
        return "Loan_automaticSetPercentageMask";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="internalCode">
    @Column(unique = true)
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getInternalCodeDD() {
        return "Loan_internalCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="loanMaximumValues">
    @OneToMany(mappedBy = "loan")
    private List<LoanMaximumValue> loanMaximumValues;

    public List<LoanMaximumValue> getLoanMaximumValues() {
        return loanMaximumValues;
    }

    public void setLoanMaximumValues(List<LoanMaximumValue> loanMaximumValues) {
        this.loanMaximumValues = loanMaximumValues;
    }

    public String getLoanMaximumValuesDD() {
        return "Loan_loanMaximumValues";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="accumulative">
    @Column
    private boolean accumulative = true;

    public boolean isAccumulative() {
        return accumulative;
    }

    public void setAccumulative(boolean accumulative) {
        this.accumulative = accumulative;
    }

    public String getAccumulativeDD() {
        return "Loan_accumulative";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Loan_code";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "Loan_legalEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "Loan_legalEntityGroup";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC loanType;

    public UDC getLoanType() {
        return loanType;
    }

    public void setLoanType(UDC loanType) {
        this.loanType = loanType;
    }

    public String getLoanTypeDD() {
        return "Loan_loanType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanNature">
    @Column(length = 1)
    private String loanNature;

    public String getLoanNature() {
        return loanNature;
    }

    public void setLoanNature(String loanNature) {
        this.loanNature = loanNature;
    }

    public String getLoanNatureDD() {
        return "Loan_loanNature";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hiringDateEligibility">
    private BigDecimal hiringDateEligibility;

    public BigDecimal getHiringDateEligibility() {
        return hiringDateEligibility;
    }

    public void setHiringDateEligibility(BigDecimal hiringDateEligibility) {
        this.hiringDateEligibility = hiringDateEligibility;
    }

    public String getHiringDateEligibilityDD() {
        return "Loan_hiringDateEligibility";
    }
    @Transient
    private BigDecimal hiringDateEligibilityMask;

    public BigDecimal getHiringDateEligibilityMask() {
        hiringDateEligibilityMask = hiringDateEligibility;
        return hiringDateEligibilityMask;
    }

    public void setHiringDateEligibilityMask(BigDecimal hiringDateEligibilityMask) {
        updateDecimalValue("hiringDateEligibility", hiringDateEligibilityMask);
    }

    public String getHiringDateEligibilityMaskDD() {
        return "Loan_hiringDateEligibilityMask";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="secondRequestEligibility">
    private BigDecimal secondRequestEligibility;

    public BigDecimal getSecondRequestEligibility() {
        return secondRequestEligibility;
    }

    public void setSecondRequestEligibility(BigDecimal secondRequestEligibility) {
        this.secondRequestEligibility = secondRequestEligibility;
    }

    public String getSecondRequestEligibilityDD() {
        return "Loan_secondRequestEligibility";
    }
    @Transient
    private BigDecimal secondRequestEligibilityMask;

    public BigDecimal getSecondRequestEligibilityMask() {
        secondRequestEligibilityMask = secondRequestEligibility;
        return secondRequestEligibilityMask;
    }

    public void setSecondRequestEligibilityMask(BigDecimal secondRequestEligibilityMask) {
        updateDecimalValue("secondRequestEligibility", secondRequestEligibilityMask);
    }

    public String getSecondRequestEligibilityMaskDD() {
        return "Loan_secondRequestEligibilityMask";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="companyPercentage">
    private BigDecimal companyPercentage;

    public BigDecimal getCompanyPercentage() {
        return companyPercentage;
    }

    public void setCompanyPercentage(BigDecimal companyPercentage) {
        this.companyPercentage = companyPercentage;
    }

    public String getCompanyPercentageDD() {
        return "Loan_companyPercentage";
    }
    @Transient
    private BigDecimal companyPercentageMask;

    public BigDecimal getCompanyPercentageMask() {
        companyPercentageMask = companyPercentage;
        return companyPercentageMask;
    }

    public void setCompanyPercentageMask(BigDecimal companyPercentageMask) {
        updateDecimalValue("companyPercentage", companyPercentageMask);
    }

    public String getCompanyPercentageMaskDD() {
        return "Loan_companyPercentageMask";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="companyElement">
    @ManyToOne
    private SalaryElement companyElement;

    public SalaryElement getCompanyElement() {
        return companyElement;
    }

    public void setCompanyElement(SalaryElement companyElement) {
        this.companyElement = companyElement;
    }

    public String getCompanyElementDD() {
        return "Loan_companyElement";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="deductOpenLoans">
    @Column(length = 1)
    private String deductOpenLoans;

    public String getDeductOpenLoans() {
        return deductOpenLoans;
    }

    public void setDeductOpenLoans(String deductOpenLoans) {
        this.deductOpenLoans = deductOpenLoans;
    }

    public String getDeductOpenLoansDD() {
        return "Loan_deductOpenLoans";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="otherLoanValidation">
    @Column(length = 1)
    private String otherLoanValidation;

    public String getOtherLoanValidation() {
        return otherLoanValidation;
    }

    public void setOtherLoanValidation(String otherLoanValidation) {
        this.otherLoanValidation = otherLoanValidation;
    }

    public String getOtherLoanValidationDD() {
        return "Loan_otherLoanValidation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="rejectFromDay">
    private BigDecimal rejectFromDay;

    public BigDecimal getRejectFromDay() {
        return rejectFromDay;
    }

    public void setRejectFromDay(BigDecimal rejectFromDay) {
        this.rejectFromDay = rejectFromDay;
    }

    public String getRejectFromDayDD() {
        return "Loan_rejectFromDay";
    }
    @Transient
    private BigDecimal rejectFromDayMask;

    public BigDecimal getRejectFromDayMask() {
        rejectFromDayMask = rejectFromDay;
        return rejectFromDayMask;
    }

    public void setRejectFromDayMask(BigDecimal rejectFromDayMask) {
        updateDecimalValue("rejectFromDay", rejectFromDayMask);
    }

    public String getRejectFromDayMaskDD() {
        return "Loan_rejectFromDayMask";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="rejectToDay">
    private BigDecimal rejectToDay;

    public BigDecimal getRejectToDay() {
        return rejectToDay;
    }

    public void setRejectToDay(BigDecimal rejectToDay) {
        this.rejectToDay = rejectToDay;
    }

    public String getRejectToDayDD() {
        return "Loan_rejectToDay";
    }
    @Transient
    private BigDecimal rejectToDayMask;

    public BigDecimal getRejectToDayMask() {
        rejectToDayMask = rejectToDay;
        return rejectToDayMask;
    }

    public void setRejectToDayMask(BigDecimal rejectToDayMask) {
        updateDecimalValue("rejectToDay", rejectToDayMask);
    }

    public String getRejectToDayMaskDD() {
        return "Loan_rejectToDayMask";
    }
    // </editor-fold >

}
