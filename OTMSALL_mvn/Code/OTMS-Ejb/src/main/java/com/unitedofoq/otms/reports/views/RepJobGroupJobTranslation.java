
package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepJobGroupJobTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobGroupDescription">
    @Column
    private String jobGroupDescription;

    public void setJobGroupDescription(String jobGroupDescription) {
        this.jobGroupDescription = jobGroupDescription;
    }

    public String getJobGroupDescription() {
        return jobGroupDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="workDurationMeasureUnit">
    @Column
    private String workDurationMeasureUnit;

    public void setWorkDurationMeasureUnit(String workDurationMeasureUnit) {
        this.workDurationMeasureUnit = workDurationMeasureUnit;
    }

    public String getWorkDurationMeasureUnit() {
        return workDurationMeasureUnit;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="experienceMeasureUnit">
    @Column
    private String experienceMeasureUnit;

    public void setExperienceMeasureUnit(String experienceMeasureUnit) {
        this.experienceMeasureUnit = experienceMeasureUnit;
    }

    public String getExperienceMeasureUnit() {
        return experienceMeasureUnit;
    }
    // </editor-fold>

}
