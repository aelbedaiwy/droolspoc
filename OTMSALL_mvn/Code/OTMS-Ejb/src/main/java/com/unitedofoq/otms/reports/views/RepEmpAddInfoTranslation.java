/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.reports.views;

import javax.persistence.Entity;

/**
 *
 * @author 3dly
 */
@Entity
public class RepEmpAddInfoTranslation extends RepEmployeeBaseTranslation {
   
    // <editor-fold defaultstate="collapsed" desc="attrUdc1">

    private String attrUdc1;

    public String getAttrUdc1() {
        return attrUdc1;
    }

    public void setAttrUdc1(String attrUdc1) {
        this.attrUdc1 = attrUdc1;
    }
    // </editor-fold >
    
    // <editor-fold defaultstate="collapsed" desc="attrUdc2">
    private String attrUdc2;

    public String getAttrUdc2() {
        return attrUdc2;
    }

    public void setAttrUdc2(String attrUdc2) {
        this.attrUdc2 = attrUdc2;
    }
    // </editor-fold >
    
    // <editor-fold defaultstate="collapsed" desc="attrUdc3">
    private String attrUdc3;

    public String getAttrUdc3() {
        return attrUdc3;
    }

    public void setAttrUdc3(String attrUdc3) {
        this.attrUdc3 = attrUdc3;
    }
    // </editor-fold >
    
    // <editor-fold defaultstate="collapsed" desc="attrUdc4">
    private String attrUdc4;

    public String getAttrUdc4() {
        return attrUdc4;
    }

    public void setAttrUdc4(String attrUdc4) {
        this.attrUdc4 = attrUdc4;
    }
    // </editor-fold >
    
    // <editor-fold defaultstate="collapsed" desc="attrUdc5">
    private String attrUdc5;

    public String getAttrUdc5() {
        return attrUdc5;
    }

    public void setAttrUdc5(String attrUdc5) {
        this.attrUdc5 = attrUdc5;
    }
    // </editor-fold >
}
