
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;
import java.math.BigDecimal;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeBenifit extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeBenifit_dsDbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeBenifit_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeBenifit_genderDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobileLimit">
    @Column
    private BigDecimal mobileLimit;

    public void setMobileLimit(BigDecimal mobileLimit) {
        this.mobileLimit = mobileLimit;
    }

    public BigDecimal getMobileLimit() {
        return mobileLimit;
    }

    public String getMobileLimitDD() {
        return "RepEmployeeBenifit_mobileLimit";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobile1Limit">
    @Column
    private BigDecimal mobile1Limit;

    public void setMobile1Limit(BigDecimal mobile1Limit) {
        this.mobile1Limit = mobile1Limit;
    }

    public BigDecimal getMobile1Limit() {
        return mobile1Limit;
    }

    public String getMobile1LimitDD() {
        return "RepEmployeeBenifit_mobile1Limit";
    }
    // </editor-fold>
}