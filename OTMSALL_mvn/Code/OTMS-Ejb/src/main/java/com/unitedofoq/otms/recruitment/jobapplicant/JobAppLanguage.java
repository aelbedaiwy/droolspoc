
package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobAppLanguage extends BaseEntity {

    @Column(nullable=false)
	private String langLevel;
    public String getLangLevelDD(){
        return "JobAppLanguage_langLevel";
    }
    @ManyToOne
	private UDC language;
    public String getLanguageDD(){
        return "JobAppLanguage_language";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
	private JobApplicant jobApplicant;

    public String getLangLevel() {
        return langLevel;
    }

    public void setLangLevel(String langLevel) {
        this.langLevel = langLevel;
    }

	public UDC getLanguage() {
		return language;
	}

	public void setLanguage(UDC theLanguage) {
		language = theLanguage;
	}

	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant theJobApplicant) {
		jobApplicant = theJobApplicant;
	}
}
