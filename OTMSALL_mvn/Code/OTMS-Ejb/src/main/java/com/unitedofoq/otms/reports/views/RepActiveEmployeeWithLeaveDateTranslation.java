package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name = "repactempwitlvdattranslation")
public class RepActiveEmployeeWithLeaveDateTranslation extends RepEmployeeBaseTranslation {
    // <editor-fold defaultstate="collapsed" desc="genderDescription">

    @Column
    private String genderDescription;

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescription() {
        return genderDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column
    private String employeeActive;

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String getEmployeeActive() {
        return employeeActive;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="leaveReason">
    @Column
    private String leaveReason;

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }

    public String getLeaveReason() {
        return leaveReason;
    }
    // </editor-fold>
}
