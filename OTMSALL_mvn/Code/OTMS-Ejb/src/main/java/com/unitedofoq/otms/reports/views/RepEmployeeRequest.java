
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeRequest extends RepEmployeeBase  {
    // <editor-fold defaultstate="collapsed" desc="requestDescription">
    @Column
    @Translatable(translationField = "requestDescriptionTranslated")
    private String requestDescription;

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public String getRequestDescriptionDD() {
        return "RepEmployeeRequest_requestDescription";
    }
    
    @Transient
    @Translation(originalField = "requestDescription")
    private String requestDescriptionTranslated;

    public String getRequestDescriptionTranslated() {
        return requestDescriptionTranslated;
    }

    public void setRequestDescriptionTranslated(String requestDescriptionTranslated) {
        this.requestDescriptionTranslated = requestDescriptionTranslated;
    }
    
    public String getRequestDescriptionTranslatedDD() {
        return "RepEmployeeRequest_requestDescription";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public String getRequestDateDD() {
        return "RepEmployeeRequest_requestDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestTitle">
    @Column
    private String requestTitle;

    public void setRequestTitle(String requestTitle) {
        this.requestTitle = requestTitle;
    }

    public String getRequestTitle() {
        return requestTitle;
    }

    public String getRequestTitleDD() {
        return "RepEmployeeRequest_requestTitle";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestStatus">
    @Column
    @Translatable(translationField = "requestStatusTranslated")
    private String requestStatus;

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public String getRequestStatusDD() {
        return "RepEmployeeRequest_requestStatus";
    }
    
    @Transient
    @Translation(originalField = "requestStatus")
    private String requestStatusTranslated;

    public String getRequestStatusTranslated() {
        return requestStatusTranslated;
    }

    public void setRequestStatusTranslated(String requestStatusTranslated) {
        this.requestStatusTranslated = requestStatusTranslated;
    }
    
    public String getRequestStatusTranslatedDD() {
        return "RepEmployeeRequest_requestStatus";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestType">
    @Column
    @Translatable(translationField = "requestTypeTranslated")
    private String requestType;

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestType() {
        return requestType;
    }

    public String getRequestTypeDD() {
        return "RepEmployeeRequest_requestType";
    }
    
    @Transient
    @Translation(originalField = "requestType")
    private String requestTypeTranslated;

    public String getRequestTypeTranslated() {
        return requestTypeTranslated;
    }

    public void setRequestTypeTranslated(String requestTypeTranslated) {
        this.requestTypeTranslated = requestTypeTranslated;
    }
    
    public String getRequestTypeTranslatedDD() {
        return "RepEmployeeRequest_requestType";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="requestStatusDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestStatusDate;

    public Date getRequestStatusDate() {
        return requestStatusDate;
    }

    public void setRequestStatusDate(Date requestStatusDate) {
        this.requestStatusDate = requestStatusDate;
    }
    
    public String getRequestStatusDateDD() {
        return "RepEmployeeRequest_requestStatusDate";
    }
    //</editor-fold>
}
