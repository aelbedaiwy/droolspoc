
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;

@Entity
public class RepCourseCatalog extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepCourseCatalog_dsDbid";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="companyID">
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }
    
    public String getCompanyIDDD() {
        return "RepCourseCatalog_companyID";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }

    public String getCourseClassDD() {
        return "RepCourseCatalog_courseClass";
    }
    
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;
    
    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }
    
    public String getCourseClassTranslatedDD() {
        return "RepCourseCatalog_courseClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseType">
    @Column
    @Translatable(translationField = "courseTypeTranslated")
    private String courseType;

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getCourseType() {
        return courseType;
    }

    public String getCourseTypeDD() {
        return "RepCourseCatalog_courseType";
    }
    
    @Transient
    @Translation(originalField = "courseType")
    private String courseTypeTranslated;

    public String getCourseTypeTranslated() {
        return courseTypeTranslated;
    }

    public void setCourseTypeTranslated(String courseTypeTranslated) {
        this.courseTypeTranslated = courseTypeTranslated;
    }
    
    public String getCourseTypeTranslatedDD() {
        return "RepCourseCatalog_courseType";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    @Translatable(translationField = "nameTranslated")
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "RepCourseCatalog_name";
    }
    
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    
    public String getNameTranslatedDD() {
        return "RepCourseCatalog_name";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD() {
        return "RepCourseCatalog_code";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="learningMethod">
    @Column
    @Translatable(translationField = "learningMethodTranslated")
    private String learningMethod;

    public void setLearningMethod(String learningMethod) {
        this.learningMethod = learningMethod;
    }

    public String getLearningMethod() {
        return learningMethod;
    }

    public String getLearningMethodDD() {
        return "RepCourseCatalog_learningMethod";
    }
    
    @Transient
    @Translation(originalField = "learningMethod")
    private String learningMethodTranslated;

    public String getLearningMethodTranslated() {
        return learningMethodTranslated;
    }

    public void setLearningMethodTranslated(String learningMethodTranslated) {
        this.learningMethodTranslated = learningMethodTranslated;
    }
    
    public String getLearningMethodTranslatedDD() {
        return "RepCourseCatalog_learningMethod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="forWhom">
    @Column
    private String forWhom;

    public void setForWhom(String forWhom) {
        this.forWhom = forWhom;
    }

    public String getForWhom() {
        return forWhom;
    }

    public String getForWhomDD() {
        return "RepCourseCatalog_forWhom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="content">
    @Column
    @Translatable(translationField = "contentTranslated")
    private String content;

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getContentDD() {
        return "RepCourseCatalog_content";
    }
    
    @Transient
    @Translation(originalField = "content")
    private String contentTranslated;

    public String getContentTranslated() {
        return contentTranslated;
    }

    public void setContentTranslated(String contentTranslated) {
        this.contentTranslated = contentTranslated;
    }
    
    public String getContentTranslatedDD() {
        return "RepCourseCatalog_content";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="prerequisite">
    @Column
    @Translatable(translationField = "prerequisiteTranslated")
    private String prerequisite;

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public String getPrerequisiteDD() {
        return "RepCourseCatalog_prerequisite";
    }
    
    @Transient
    @Translation(originalField = "prerequisite")
    private String prerequisiteTranslated;

    public String getPrerequisiteTranslated() {
        return prerequisiteTranslated;
    }

    public void setPrerequisiteTranslated(String prerequisiteTranslated) {
        this.prerequisiteTranslated = prerequisiteTranslated;
    }
    
    public String getPrerequisiteTranslatedDD() {
        return "RepCourseCatalog_prerequisite";
    }
    // </editor-fold>

}
