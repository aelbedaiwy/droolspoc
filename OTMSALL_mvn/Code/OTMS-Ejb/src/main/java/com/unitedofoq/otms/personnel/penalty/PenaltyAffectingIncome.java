/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author abayomy
 */
//@MappedSuperclass
@Entity
@DiscriminatorValue("INCOME")
@ParentEntity(fields={"penalty"})
//@FABSEntitySpecs(hideInheritedFields={"salaryElement"})
public class PenaltyAffectingIncome extends PenaltyAffectingSalaryElement {
    @JoinColumn//(name="SALARYELEMENT_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }
     public String getIncomeDD() {
        return "PenaltyAffectingIncome_income";
    }


   
}
