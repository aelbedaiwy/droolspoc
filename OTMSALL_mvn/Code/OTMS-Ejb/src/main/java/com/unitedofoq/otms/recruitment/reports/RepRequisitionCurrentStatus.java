
package com.unitedofoq.otms.recruitment.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepRequisitionCurrentStatus extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="applicantFullName">
    @Column
    private String applicantFullName;

    public void setApplicantFullName(String applicantFullName) {
        this.applicantFullName = applicantFullName;
    }

    public String getApplicantFullName() {
        return applicantFullName;
    }

    public String getApplicantFullNameDD() {
        return "RepRequisitionCurrentStatus_applicantFullName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requisition">
    @Column
    private String requisition;

    public void setRequisition(String requisition) {
        this.requisition = requisition;
    }

    public String getRequisition() {
        return requisition;
    }

    public String getRequisitionDD() {
        return "RepRequisitionCurrentStatus_requisition";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="currentStatus">
    @Column
    private String currentStatus;

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public String getCurrentStatusDD() {
        return "RepRequisitionCurrentStatus_currentStatus";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="currentStatusResult">
    @Column
    private String currentStatusResult;

    public void setCurrentStatusResult(String currentStatusResult) {
        this.currentStatusResult = currentStatusResult;
    }

    public String getCurrentStatusResult() {
        return currentStatusResult;
    }

    public String getCurrentStatusResultDD() {
        return "RepRequisitionCurrentStatus_currentStatusResult";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="currentStatusComment">
    @Column
    private String currentStatusComment;

    public void setCurrentStatusComment(String currentStatusComment) {
        this.currentStatusComment = currentStatusComment;
    }

    public String getCurrentStatusComment() {
        return currentStatusComment;
    }

    public String getCurrentStatusCommentDD() {
        return "RepRequisitionCurrentStatus_currentStatusComment";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="currentStatusDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date currentStatusDate;

    public void setCurrentStatusDate(Date currentStatusDate) {
        this.currentStatusDate = currentStatusDate;
    }

    public Date getCurrentStatusDate() {
        return currentStatusDate;
    }

    public String getCurrentStatusDateDD() {
        return "RepRequisitionCurrentStatus_currentStatusDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="currentStatusWhat">
    @Column
    private String currentStatusWhat;

    public void setCurrentStatusWhat(String currentStatusWhat) {
        this.currentStatusWhat = currentStatusWhat;
    }

    public String getCurrentStatusWhat() {
        return currentStatusWhat;
    }

    public String getCurrentStatusWhatDD() {
        return "RepRequisitionCurrentStatus_currentStatusWhat";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="currentStatusByWhoName">
    @Column
    private String currentStatusByWhoName;

    public void setCurrentStatusByWhoName(String currentStatusByWhoName) {
        this.currentStatusByWhoName = currentStatusByWhoName;
    }

    public String getCurrentStatusByWhoName() {
        return currentStatusByWhoName;
    }

    public String getCurrentStatusByWhoNameDD() {
        return "RepRequisitionCurrentStatus_currentStatusByWhoName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextAction">
    @Column
    private String nextAction;

    public void setNextAction(String nextAction) {
        this.nextAction = nextAction;
    }

    public String getNextAction() {
        return nextAction;
    }

    public String getNextActionDD() {
        return "RepRequisitionCurrentStatus_nextAction";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextActionComment">
    @Column
    private String nextActionComment;

    public void setNextActionComment(String nextActionComment) {
        this.nextActionComment = nextActionComment;
    }

    public String getNextActionComment() {
        return nextActionComment;
    }

    public String getNextActionCommentDD() {
        return "RepRequisitionCurrentStatus_nextActionComment";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextActionWhat">
    @Column
    private String nextActionWhat;

    public void setNextActionWhat(String nextActionWhat) {
        this.nextActionWhat = nextActionWhat;
    }

    public String getNextActionWhat() {
        return nextActionWhat;
    }

    public String getNextActionWhatDD() {
        return "RepRequisitionCurrentStatus_nextActionWhat";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextActionByWhoName">
    @Column
    private String nextActionByWhoName;

    public void setNextActionByWhoName(String nextActionByWhoName) {
        this.nextActionByWhoName = nextActionByWhoName;
    }

    public String getNextActionByWhoName() {
        return nextActionByWhoName;
    }

    public String getNextActionByWhoNameDD() {
        return "RepRequisitionCurrentStatus_nextActionByWhoName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextActionDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date nextActionDate;

    public void setNextActionDate(Date nextActionDate) {
        this.nextActionDate = nextActionDate;
    }

    public Date getNextActionDate() {
        return nextActionDate;
    }

    public String getNextActionDateDD() {
        return "RepRequisitionCurrentStatus_nextActionDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="position">
    @Column
    private String position;

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "RepRequisitionCurrentStatus_position";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getCompanyIDDD() {
        return "RepRequisitionCurrentStatus_companyID";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepRequisitionCurrentStatus_dsDbid";
    }
    // </editor-fold>    
}