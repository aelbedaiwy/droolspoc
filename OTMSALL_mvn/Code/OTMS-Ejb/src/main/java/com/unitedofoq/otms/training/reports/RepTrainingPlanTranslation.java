/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author mostafa
 */
@Entity
public class RepTrainingPlanTranslation extends BaseEntityTranslation{
    
    // <editor-fold defaultstate="collapsed" desc="planDesription">
    @Column
    private String planDescription;

    public void setPlanDesription(String planDescription) {
        this.planDescription = planDescription;
    }

    public String getPlanDescription() {
        return planDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseName">
    @Column
    private String courseName;

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="planStatus">
    @Column
    private String planStatus;

    public String getPlanStatus() {
        return planStatus;
    }

    public void setPlanStatus(String planStatus) {
        this.planStatus = planStatus;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="provider">
    private String provider;
    
    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
    //</editor-fold>

    
}
