package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"employeeLoan"})
public class EmployeeLoanSettlement extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="employeeLoan">
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private EmployeeLoan employeeLoan;

    public EmployeeLoan getEmployeeLoan() {
        return employeeLoan;
    }

    public void setEmployeeLoan(EmployeeLoan employeeLoan) {
        this.employeeLoan = employeeLoan;
    }

    public String getEmployeeLoanDD() {
        return "EmployeeLoanSettlement_employeeLoan";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="settInstallmentNumber">
    private Integer settInstallmentNumber;

    public Integer getSettInstallmentNumber() {
        return settInstallmentNumber;
    }

    public void setSettInstallmentNumber(Integer settInstallmentNumber) {
        this.settInstallmentNumber = settInstallmentNumber;
    }

    public String getSettInstallmentNumberDD() {
        return "EmployeeLoanSettlement_settInstallmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="settLoanValue">
    private BigDecimal settLoanValue;

    public BigDecimal getSettLoanValue() {
        return settLoanValue;
    }

    public void setSettLoanValue(BigDecimal settLoanValue) {
        this.settLoanValue = settLoanValue;
    }

    public String getSettLoanValueDD() {
        return "EmployeeLoanSettlement_settLoanValue";
    }
    @Transient
    private BigDecimal settLoanValueMask;

    public BigDecimal getSettLoanValueMask() {
        settLoanValueMask = settLoanValue;
        return settLoanValueMask;
    }

    public void setSettLoanValueMask(BigDecimal settLoanValueMask) {
        updateDecimalValue("settLoanValue", settLoanValueMask);
    }

    public String getSettLoanValueMaskDD() {
        return "EmployeeLoanSettlement_settLoanValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="settDeduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction settDeduction;

    public Deduction getSettDeduction() {
        return settDeduction;
    }

    public void setSettDeduction(Deduction settDeduction) {
        this.settDeduction = settDeduction;
    }

    public String getSettDeductionDD() {
        return "EmployeeLoan_settDeduction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal paidAmount;

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaidAmountDD() {
        return "EmployeeLoanSettlement_paidAmount";
    }
    @Transient
    private BigDecimal paidAmountMask;

    public BigDecimal getPaidAmountMask() {
        paidAmountMask = paidAmount;
        return paidAmountMask;
    }

    public void setPaidAmountMask(BigDecimal paidAmountMask) {
        updateDecimalValue("paidAmount", paidAmountMask);
    }

    public String getPaidAmountMaskDD() {
        return "EmployeeLoanSettlement_paidAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidAmountB4Settlement">
    @Column(precision = 25, scale = 13)
    private BigDecimal paidAmountB4Settlement;

    public BigDecimal getPaidAmountB4Settlement() {
        return paidAmountB4Settlement;
    }

    public String getPaidAmountB4SettlementDD() {
        return "EmployeeLoanSettlement_paidAmountB4Settlement";
    }

    public void setPaidAmountB4Settlement(BigDecimal paidAmountB4Settlement) {
        this.paidAmountB4Settlement = paidAmountB4Settlement;
    }

    @Transient
    private BigDecimal paidAmountB4SettlementMask;

    public BigDecimal getPaidAmountB4SettlementMask() {
        paidAmountB4SettlementMask = paidAmountB4Settlement;
        return paidAmountB4SettlementMask;
    }

    public String getPaidAmountB4SettlementMaskDD() {
        return "EmployeeLoanSettlement_paidAmountB4SettlementMask";
    }

    public void setPaidAmountB4SettlementMask(BigDecimal paidAmountB4SettlementMask) {
        updateDecimalValue("paidAmountB4Settlement", paidAmountB4SettlementMask);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidInstallment">
    @Column
    private Integer paidInstallment;

    public Integer getPaidInstallment() {
        return paidInstallment;
    }

    public void setPaidInstallment(Integer paidInstallment) {
        this.paidInstallment = paidInstallment;
    }

    public String getPaidInstallmentDD() {
        return "EmployeeLoanSettlement_paidInstallment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="paidInstallmentB4Settlement">
    @Column
    private Integer paidInstallmentB4Settlement;

    public Integer getPaidInstallmentB4Settlement() {
        return paidInstallmentB4Settlement;
    }

    public String getPaidInstallmentB4SettlementDD() {
        return "EmployeeLoanSettlement_paidInstallmentB4Settlement";
    }

    public void setPaidInstallmentB4Settlement(Integer paidInstallmentB4Settlement) {
        this.paidInstallmentB4Settlement = paidInstallmentB4Settlement;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="status">
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "EmployeeLoanSettlement_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statusB4Settlement">
    private String statusB4Settlement;

    public String getStatusB4Settlement() {
        return statusB4Settlement;
    }

    public String getStatusB4SettlementDD() {
        return "EmployeeLoanSettlement_statusB4Settlement";
    }

    public void setStatusB4Settlement(String statusB4Settlement) {
        this.statusB4Settlement = statusB4Settlement;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    private String cancelled = "N";

    public String getCancelled() {
        return cancelled;
    }

    public String getCancelledDD() {
        return "EmployeeLoanSettlement_cancelled";
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee cancelledBy;

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public String getCancelledByDD() {
        return "EmployeeLoanSettlement_cancelledBy";
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="cancelledByComments">
    private String cancelledByComments;

    public String getCancelledByComments() {
        return cancelledByComments;
    }

    public String getCancelledByCommentsDD() {
        return "EmployeeLoanSettlement_cancelledByComments";
    }

    public void setCancelledByComments(String cancelledByComments) {
        this.cancelledByComments = cancelledByComments;
    }
    // </editor-fold >
}
