/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.training.EducationBase;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author aibrahim
 */
@MappedSuperclass
public class PersonnelEducationBase extends EducationBase {
    // <editor-fold defaultstate="collapsed" desc="graduationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date graduationDate;
    public Date getGraduationDate() {
        return graduationDate;
    }
    public void setGraduationDate(Date graduationDate) {
        this.graduationDate = graduationDate;
    }
    public String getGraduationDateDD() {
        return "PersonnelEducationBase_graduationDate";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="country">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC country;
    public UDC getCountry() {
        return country;
    }
    public void setCountry(UDC country) {
        this.country = country;
    }
    public String getCountryDD(){
        return "PersonnelEducationBase_country";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="city">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC city;
    public UDC getCity() {
        return city;
    }
    public void setCity(UDC city) {
        this.city = city;
    }
        public String getCityDD(){
        return "PersonnelEducationBase_city";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="studyFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date studyFrom;
 public Date getStudyFrom() {
        return studyFrom;
    }
    public void setStudyFrom(Date studyFrom) {
        this.studyFrom = studyFrom;
    }
 public String getStudyFromDD() {
        return "PersonnelEducationBase_studyFrom";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="studyTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date studyTo;
    public Date getStudyTo() {
        return studyTo;
    }
    public void setStudyTo(Date studyTo) {
        this.studyTo = studyTo;
    }
    public String getStudyToDD() {
        return "PersonnelEducationBase_studyTo";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
   public String getNoteseDD() {
        return "PersonnelEducationBase_notes";
    }
       // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gpa">
    @Column
    private BigDecimal gpa;
    public BigDecimal getGpa() {
        return gpa;
    }
    public void setGpa(BigDecimal gpa) {
        this.gpa = gpa;
    }
    public String getGpaDD() {
        return "PersonnelEducationBase_gpa";
    }
    @Transient
    private BigDecimal gpaMask;
    public BigDecimal getGpaMask() {
        gpaMask = gpa ;
        return gpaMask;
    }
    public void setGpaMask(BigDecimal gpaMask) {
        updateDecimalValue("gpa", gpaMask);
    }
    public String getGpaMaskDD() {
        return "PersonnelEducationBase_gpaMask";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="years">
    private Integer years;

    public Integer getYears() {
        return years;
    }

    public void setYears(Integer years) {
        this.years = years;
    }
    
    public String getYearsDD() {
        return "PersonnelEducationBase_years";
    }
    // </editor-fold >
}
