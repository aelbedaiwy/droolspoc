package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.appraisal.template.AppraisalElement;
import com.unitedofoq.otms.appraisal.template.AppraisalElementTemplate;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplateSequence;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import com.unitedofoq.otms.training.activity.Provider;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = "appraisee")
public class EmployeeAppraisalElement extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "AppraisalElementHeader_description";
    }
     //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "AppraisalElementHeader_comments";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="totalScore">
    @Column(precision = 25, scale = 13)
    private BigDecimal totalScore;

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public String getTotalScoreDD() {
        return "AppraisalElementHeader_totalScore";
    }

    @Transient
    private BigDecimal totalScoreMask;

    public BigDecimal getTotalScoreMask() {
        totalScoreMask = totalScore;
        return totalScoreMask;
    }

    public void setTotalScoreMask(BigDecimal totalScoreMask) {
        updateDecimalValue("totalScore", totalScoreMask);
    }

    public String getTotalScoreMaskDD() {
        return "AppraisalElementHeader_totalScoreMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="averageScore">
    //the final score after formula calculation
    @Column(precision = 25, scale = 13)
    private BigDecimal averageScore;

    public BigDecimal getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(BigDecimal averageScore) {
        this.averageScore = averageScore;
    }

    public String getAverageScoreDD() {
        return "AppraisalElementHeader_averageScore";
    }

    @Transient
    private BigDecimal averageScoreMask;

    public BigDecimal getAverageScoreMask() {
        averageScoreMask = averageScore;
        return averageScoreMask;
    }

    public void setAverageScoreMask(BigDecimal averageScoreMask) {
        updateDecimalValue("averageScore", averageScoreMask);
    }

    public String getAverageScoreMaskDD() {
        return "AppraisalElementHeader_averageScoreMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="appraisee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee appraisee;

    public Employee getAppraisee() {
        return appraisee;
    }

    public void setAppraisee(Employee appraisee) {
        this.appraisee = appraisee;
    }

    public String getAppraiseeDD() {
        return "AppraisalElementHeader_appraisee";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="appraiser">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee appraiser;

    public Employee getAppraiser() {
        return appraiser;
    }

    public String getAppraiserDD() {
        return "AppraisalElementHeader_appraiser";
    }

    public void setAppraiser(Employee appraiser) {
        this.appraiser = appraiser;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="appraisalElement">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalElement appraisalElement;

    public AppraisalElement getAppraisalElement() {
        return appraisalElement;
    }

    public void setAppraisalElement(AppraisalElement appraisalElement) {
        this.appraisalElement = appraisalElement;
    }

    public String getAppraisalElementDD() {
        return "AppraisalElementHeader_appraisalElement";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="sequence">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplateSequence sequence;

    public AppraisalTemplateSequence getSequence() {
        return sequence;
    }

    public void setSequence(AppraisalTemplateSequence sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD() {
        return "AppraisalElementHeader_sequence";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "AppraisalElementHeader_code";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="percentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal percentage;

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public String getPercentageDD() {
        return "AppraisalElementHeader_percentage";
    }

    @Transient
    private BigDecimal percentageMask;

    public BigDecimal getPercentageMask() {
        percentageMask = percentage;
        return percentageMask;
    }

    public void setPercentageMask(BigDecimal percentageMask) {
        updateDecimalValue("percentage", percentageMask);
    }

    public String getPercentageMaskDD() {
        return "AppraisalElementHeader_percentageMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="outOfMark">
    private BigDecimal outOfMark;

    public BigDecimal getOutOfMark() {
        return outOfMark;
    }

    public void setOutOfMark(BigDecimal outOfMark) {
        this.outOfMark = outOfMark;
    }

    public String getOutOfMarkDD() {
        return "AppraisalElementHeader_outOfMark";
    }

    @Transient
    private BigDecimal outOfMarkMask;

    public BigDecimal getOutOfMarkMask() {
        outOfMarkMask = outOfMark;
        return outOfMarkMask;
    }

    public void setOutOfMarkMask(BigDecimal outOfMarkMask) {
        updateDecimalValue("outOfMark", outOfMarkMask);
    }

    public String getOutOfMarkMaskDD() {
        return "AppraisalElementHeader_outOfMarkMask";
    }
    //</editor-fold>

    // for recruitment:
    // =================
    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "AppraisalElementHeader_applicant";
    }
    //</editor-fold>

    // for training:
    // =================
    //<editor-fold defaultstate="collapsed" desc="course">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation course;

    public ProviderCourseInformation getCourse() {
        return course;
    }

    public void setCourse(ProviderCourseInformation course) {
        this.course = course;
    }

    public String getCourseDD() {
        return "AppraisalElementHeader_course";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "AppraisalElementHeader_provider";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="template">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalElementTemplate template;

    public AppraisalElementTemplate getTemplate() {
        return template;
    }

    public String getTemplateDD() {
        return "EmployeeAppraisalElement_template";
    }

    public void setTemplate(AppraisalElementTemplate template) {
        this.template = template;
    }
    //</editor-fold >

}
