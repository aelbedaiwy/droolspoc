package com.unitedofoq.otms.security;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

@Local
public interface DataSecurityServiceLocal {

    public OFunctionResult applySecurity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult applySecurityOnUnitTreeScreen(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult applySecurityByEmployee(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult assignUserExitToEntity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult updateUserExitToEntity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult deleteUserExitToEntity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser);

    public List<String> getEmployeeSecurityDbids(OUser loggedUser);

    public List<String> getPositionSecurityDbids(OUser loggedUser);

    public List<String> getUnitSecurityDbids(OUser loggedUser);

    public Map<String, List<String>> getDataSecurityCondition(OUser loggedUser);

    public List<String> getVacationSecurityDbids(OUser loggedUser);
}
