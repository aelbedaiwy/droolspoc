package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.payroll.formula.FormulaDistinctName;
import com.unitedofoq.otms.personnel.dayoff.DayOff;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class VacationBase extends DayOff {
    // <editor-fold defaultstate="collapsed" desc="managedBySystem">

    @Column(length = 1)
    private String managedBySystem;//managed_by_system

    public String getManagedBySystem() {
        return managedBySystem;
    }

    public void setManagedBySystem(String managedBySystem) {
        this.managedBySystem = managedBySystem;
    }

    public String getManagedBySystemDD() {
        return "VacationBase_managedBySystem";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;//date_to_start

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "VacationBase_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paid">
    @Column(length = 1)
    //Y - N
    private String paid;//paid_flag

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getPaidDD() {
        return "VacationBase_paid";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="balanceMethod">
    @Column
    // 0 Year Balance
    // 1 Hire Balance
    // 2 Joining Balance
    // 3 None
    // 4 Year Taken Balance
    private Long balanceMethod;//vac_bal_method

    public Long getBalanceMethod() {
        return balanceMethod;
    }

    public void setBalanceMethod(Long balanceMethod) {
        this.balanceMethod = balanceMethod;
    }

    public String getBalanceMethodDD() {
        return "VacationBase_balanceMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="balanceEnd">
    private Long balanceEnd;

    public Long getBalanceEnd() {
        return balanceEnd;
    }

    public void setBalanceEnd(Long balanceEnd) {
        this.balanceEnd = balanceEnd;
    }

    public String getBalanceEndDD() {
        return "VacationBase_balanceEnd";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postingMonth">
    @Column
    private Long postingMonth;//vac_bal_end

    public Long getPostingMonth() {
        return postingMonth;
    }

    public void setPostingMonth(Long postingMonth) {
        this.postingMonth = postingMonth;
    }

    public String getPostingMonthDD() {
        return "VacationBase_postingMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumToPostFormula">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName maximumToPostFormula;//max_to_post_formula

    public FormulaDistinctName getMaximumToPostFormula() {
        return maximumToPostFormula;
    }

    public void setMaximumToPostFormula(FormulaDistinctName maximumToPostFormula) {
        this.maximumToPostFormula = maximumToPostFormula;
    }

    public String getMaximumToPostFormulaDD() {
        return "VacationBase_maximumToPostFormula";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumToRequest">
    @Column(precision = 18, scale = 3)
    private BigDecimal maximumToRequest;//max_to_request

    public BigDecimal getMaximumToRequest() {
        return maximumToRequest;
    }

    public void setMaximumToRequest(BigDecimal maximumToRequest) {
        this.maximumToRequest = maximumToRequest;
    }

    public String getMaximumToRequestDD() {
        return "VacationBase_maximumToRequest";
    }
    @Transient
    private BigDecimal maximumToRequestMask;

    public BigDecimal getMaximumToRequestMask() {
        maximumToRequestMask = maximumToRequest;
        return maximumToRequestMask;
    }

    public void setMaximumToRequestMask(BigDecimal maximumToRequestMask) {
        updateDecimalValue("maximumToRequest", maximumToRequestMask);
    }

    public String getMaximumToRequestMaskDD() {
        return "VacationBase_maximumToRequestMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumToRequest">
    @Column(precision = 18, scale = 3)
    private BigDecimal minimumToRequest;//min_to_request

    public BigDecimal getMinimumToRequest() {
        return minimumToRequest;
    }

    public void setMinimumToRequest(BigDecimal minimumToRequest) {
        this.minimumToRequest = minimumToRequest;
    }

    public String getMinimumToRequestDD() {
        return "VacationBase_minimumToRequest";
    }
    @Transient
    private BigDecimal minimumToRequestMask;

    public BigDecimal getMinimumToRequestMask() {
        minimumToRequestMask = minimumToRequest;
        return minimumToRequestMask;
    }

    public void setMinimumToRequestMask(BigDecimal minimumToRequestMask) {
        updateDecimalValue("minimumToRequest", minimumToRequestMask);
    }

    public String getMinimumToRequestMaskDD() {
        return "VacationBase_minimumToRequestMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="category">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC category;//category_id

    public UDC getCategory() {
        return category;
    }

    public void setCategory(UDC category) {
        this.category = category;
    }

    public String getCategoryDD() {
        return "VacationBase_category";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="formula">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName formula;//formula_name

    public FormulaDistinctName getFormula() {
        return formula;
    }

    public void setFormula(FormulaDistinctName formula) {
        this.formula = formula;
    }

    public String getFormulaDD() {
        return "VacationBase_formula";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOfCalculation">
    @Column(precision = 18, scale = 3)
    private BigDecimal dayOfCalculation;//day_of_calc

    public BigDecimal getDayOfCalculation() {
        return dayOfCalculation;
    }

    public void setDayOfCalculation(BigDecimal dayOfCalculation) {
        this.dayOfCalculation = dayOfCalculation;
    }

    public String getDayOfCalculationDD() {
        return "VacationBase_dayOfCalculation";
    }
    @Transient
    private BigDecimal dayOfCalculationMask;

    public BigDecimal getDayOfCalculationMask() {
        dayOfCalculationMask = dayOfCalculation;
        return dayOfCalculationMask;
    }

    public void setDayOfCalculationMask(BigDecimal dayOfCalculationMask) {
        updateDecimalValue("dayOfCalculation", dayOfCalculationMask);
    }

    public String getDayOfCalculationMaskDD() {
        return "VacationBase_dayOfCalculationMask";
    }
    // </editor-fold>
}
