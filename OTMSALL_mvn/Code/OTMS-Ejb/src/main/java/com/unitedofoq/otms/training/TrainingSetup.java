/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;


import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
public class TrainingSetup extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "TrainingRequest_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelDeduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction cancelDeduction;

    public Deduction getCancelDeduction() {
        return cancelDeduction;
    }

    public void setCancelDeduction(Deduction cancelDeduction) {
        this.cancelDeduction = cancelDeduction;
    }

    public String getCancelDeductionDD() {
        return "TrainingSetup_cancelDeduction";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attendanceDeduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction attendanceDeduction;

    public Deduction getAttendanceDeduction() {
        return attendanceDeduction;
    }

    public void setAttendanceDeduction(Deduction attendanceDeduction) {
        this.attendanceDeduction = attendanceDeduction;
    }

    public String getAttendanceDeductionDD() {
        return "TrainingSetup_attendanceDeduction";
    }
    // </editor-fold >

}