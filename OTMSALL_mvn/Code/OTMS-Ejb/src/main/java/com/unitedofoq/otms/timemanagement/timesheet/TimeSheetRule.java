/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.personnel.absence.Absence;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.personnel.vacation.Vacation;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields="projectNature")
public class TimeSheetRule extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="projectNature">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProjectNature projectNature;

    public ProjectNature getProjectNature() {
        return projectNature;
    }
    public String getProjectNatureDD() {
        return "TimeSheetRule_projectNature";
    }

    public void setProjectNature(ProjectNature projectNature) {
        this.projectNature = projectNature;
    }    
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="calendar">
    @ManyToOne
    private NormalWorkingCalendar calendar;

    public NormalWorkingCalendar getCalendar() {
        return calendar;
    }
    public String getCalendarDD() {
        return "TimeSheetRule_calendar";
    }

    public void setCalendar(NormalWorkingCalendar calendar) {
        this.calendar = calendar;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filter">
    @Column
    private String filter;

    public String getFilter() {
        return filter;
    }
    public String getFilterDD() {
        return "TimeSheetRule_filter";
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="income">
    @ManyToOne
    private Income income;

    public Income getIncome() {
        return income;
    }
    public String getIncomeDD() {
        return "TimeSheetRule_income";
    }

    public void setIncome(Income income) {
        this.income = income;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="deduction">
    @ManyToOne
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }
    public String getDeductionDD() {
        return "TimeSheetRule_deduction";
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="vacation">
    @ManyToOne
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }
    public String getVacationDD() {
        return "TimeSheetRule_vacation";
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="absence">
    @ManyToOne
    private Absence absence;

    public Absence getAbsence() {
        return absence;
    }
    public String getAbsenceDD() {
        return "TimeSheetRule_absence";
    }

    public void setAbsence(Absence absence) {
        this.absence = absence;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="penalty">
    @ManyToOne
    private Penalty penalty;

    public Penalty getPenalty() {
        return penalty;
    }
    public String getPenaltyDD() {
        return "TimeSheetRule_penalty";
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="factor">
    private BigDecimal factor;

    public BigDecimal getFactor() {
        return factor;
    }
    public String getFactorDD() {
        return "TimeSheetRule_factor";
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }
    @Transient
    private BigDecimal factorMask;

    public BigDecimal getFactorMask() {
        factor = factorMask;
        return factorMask;
    }

    public void setFactorMask(BigDecimal factorMask) {
        updateDecimalValue("factor", factorMask);
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comment">
    @Column(name="comments")
    private String comment;

    public String getComment() {
        return comment;
    }
    public String getCommentDD() {
        return "TimeSheetRule_comment";
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attributeNo">
    @Column
    private String attributeNo;

    public String getAttributeNo() {
        return attributeNo;
    }
    public String getAttributeNoDD() {
        return "TimeSheetRule_attributeNo";
    }

    public void setAttributeNo(String attributeNo) {
        this.attributeNo = attributeNo;
    }
    // </editor-fold>
}
