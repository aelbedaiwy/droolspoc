package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeObjective extends EmployeeKPA {

    //<editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "EmployeeObjectives_description";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EmployeeObjectives_code";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="year">
    @Column
    private String year;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYearDD() {
        return "EmployeeObjectives_year";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeObjectives_employee";
    }
    //</editor-fold>
}
