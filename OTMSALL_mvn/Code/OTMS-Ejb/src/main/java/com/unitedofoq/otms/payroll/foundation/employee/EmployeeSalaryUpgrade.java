package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeProfileHistory;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.PayrollMatrix;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.personnel.investigation.EmpInvestigation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 */
//FIXME: to be request
@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeSalaryUpgrade extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="applyDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date applyDate;

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public String getApplyDateDD() {
        return "EmployeeSalaryUpgrade_applyDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applied">
    @Column(length = 1)
    private String applied;

    public String getApplied() {
        return applied;
    }

    public void setApplied(String applyed) {
        this.applied = applyed;
    }

    public String getAppliedDD() {
        return "EmployeeSalaryUpgrade_applied";
    }

    public String getCancelled() {
        return cancelled;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelDate;

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelDateDD() {
        return "EmployeeSalaryUpgrade_cancelDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC cancelReason;

    public UDC getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(UDC cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelReasonDD() {
        return "EmployeeSalaryUpgrade_cancelReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee cancelledBy;

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancelledByDD() {
        return "EmployeeSalaryUpgrade_cancelledBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    @Column(length = 1)
    private String cancelled = "N";

    public String isCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getCancelledDD() {
        return "EmployeeSalaryUpgrade_cancelled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod fromCalculatedPeriod;

    public CalculatedPeriod getFromCalculatedPeriod() {
        return fromCalculatedPeriod;
    }

    public void setFromCalculatedPeriod(CalculatedPeriod fromCalculatedPeriod) {
        this.fromCalculatedPeriod = fromCalculatedPeriod;
    }

    public String getFromCalculatedPeriodDD() {
        return "EmployeeSalaryUpgrade_fromCalculatedPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="givenBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee givenBy;

    public Employee getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(Employee givenBy) {
        this.givenBy = givenBy;
    }

    public String getGivenByDD() {
        return "EmployeeSalaryUpgrade_givenBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="givenDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date givenDate;

    public Date getGivenDate() {
        return givenDate;
    }

    public void setGivenDate(Date givenDate) {
        this.givenDate = givenDate;
    }

    public String getGivenDateDD() {
        return "EmployeeSalaryUpgrade_givenDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="income">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Income income;

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }

    public String getIncomeDD() {
        return "EmployeeSalaryUpgrade_income";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeeSalaryUpgrade_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod toCalculatedPeriod;

    public CalculatedPeriod getToCalculatedPeriod() {
        return toCalculatedPeriod;
    }

    public void setToCalculatedPeriod(CalculatedPeriod toCalculatedPeriod) {
        this.toCalculatedPeriod = toCalculatedPeriod;
    }

    public String getToCalculatedPeriodDD() {
        return "EmployeeSalaryUpgrade_toCalculatedPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="transactionNumber">
    @Column(nullable = false, precision = 25, scale = 13)
    private BigDecimal transactionNumber; //AutoNumber per employee

    public BigDecimal getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(BigDecimal transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getTransactionNumberDD() {
        return "EmployeeSalaryUpgrade_transactionNumber";
    }
    @Transient
    private BigDecimal transactionNumberMask;

    public BigDecimal getTransactionNumberMask() {
        transactionNumberMask = transactionNumber;
        return transactionNumberMask;
    }

    public void setTransactionNumberMask(BigDecimal transactionNumberMask) {
        updateDecimalValue("transactionNumber", transactionNumberMask);
    }

    public String getTransactionNumberMaskDD() {
        return "EmployeeSalaryUpgrade_transactionNumberMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="upgradeFactor">
    @Column(precision = 25, scale = 13)
    private BigDecimal upgradeFactor;

    public BigDecimal getUpgradeFactor() {
        return upgradeFactor;
    }

    public void setUpgradeFactor(BigDecimal upgradeFactor) {
        this.upgradeFactor = upgradeFactor;
    }

    public String getUpgradeFactorDD() {
        return "EmployeeSalaryUpgrade_upgradeFactor";
    }
    @Transient
    private BigDecimal upgradeFactorMask;

    public BigDecimal getUpgradeFactorMask() {
        upgradeFactorMask = upgradeFactor;
        return upgradeFactorMask;
    }

    public void setUpgradeFactorMask(BigDecimal upgradeFactorMask) {
        updateDecimalValue("upgradeFactor", upgradeFactorMask);
    }

    public String getUpgradeFactorMaskDD() {
        return "EmployeeSalaryUpgrade_upgradeFactorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="upgradeReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC upgradeReason;

    public UDC getUpgradeReason() {
        return upgradeReason;
    }

    public void setUpgradeReason(UDC upgradeReason) {
        this.upgradeReason = upgradeReason;
    }

    public String getUpgradeReasonDD() {
        return "EmployeeSalaryUpgrade_upgradeReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueAfter">
    @Column(precision = 25, scale = 13)
    private BigDecimal valueAfter;

    public BigDecimal getValueAfter() {
        return valueAfter;
    }

    public void setValueAfter(BigDecimal valueAfter) {
        this.valueAfter = valueAfter;
    }

    public String getValueAfterDD() {
        return "EmployeeSalaryUpgrade_valueAfter";
    }
    @Transient
    private BigDecimal valueAfterMask;

    public BigDecimal getValueAfterMask() {
        valueAfterMask = valueAfter;
        return valueAfterMask;
    }

    public void setValueAfterMask(BigDecimal valueAfterMask) {
        updateDecimalValue("valueAfter", valueAfterMask);
    }

    public String getValueAfterMaskDD() {
        return "EmployeeSalaryUpgrade_valueAfterMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueBefore">
    @Column(precision = 25, scale = 13)
    private BigDecimal valueBefore;

    public BigDecimal getValueBefore() {
        return valueBefore;
    }

    public void setValueBefore(BigDecimal valueBefore) {
        this.valueBefore = valueBefore;
    }

    public String getValueBeforeDD() {
        return "EmployeeSalaryUpgrade_valueBefore";
    }
    @Transient
    private BigDecimal valueBeforeMask;

    public BigDecimal getValueBeforeMask() {
        valueBeforeMask = valueBefore;
        return valueBeforeMask;
    }

    public void setValueBeforeMask(BigDecimal valueBeforeMask) {
        updateDecimalValue("valueBefore", valueBeforeMask);
    }

    public String getValueBeforeMaskDD() {
        return "EmployeeSalaryUpgrade_valueBeforeMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueBeforeEnc">

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="difference">
    @Column(precision = 25, scale = 13)
    private BigDecimal difference;

    public BigDecimal getDifference() {
        return difference;
    }

    public void setDifference(BigDecimal difference) {
        this.difference = difference;
    }

    public String getDifferenceDD() {
        return "ImportEmployeeSalaryUpgrade_difference";
    }
    @Transient
    private BigDecimal differenceMask;

    public BigDecimal getDifferenceMask() {
        differenceMask = difference;
        return differenceMask;
    }

    public void setDifferenceMask(BigDecimal differenceMask) {
        updateDecimalValue("difference", differenceMask);
    }

    public String getDifferenceMaskDD() {
        return "EmployeeSalaryUpgrade_differenceMask";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeSalaryUpgrade_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "EmployeeSalaryUpgrade_currency";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="salaryElement">
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private EmployeeSalaryElement salaryElement;
//
//    public EmployeeSalaryElement getSalaryElement() {
//        return salaryElement;
//    }
//
//    public void setSalaryElement(EmployeeSalaryElement salaryElement) {
//        this.salaryElement = salaryElement;
//    }
//
//    public String getSalaryElementDD() {
//        return "EmployeeSalaryUpgrade_salaryElement";
//    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="m2mCheck">
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeKPA_m2mCheck";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="investigation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmpInvestigation investigation;

    public EmpInvestigation getInvestigation() {
        return investigation;
    }

    public String getInvestigationDD() {
        return "EmployeeSalaryUpgrade_investigation";
    }

    public void setInvestigation(EmpInvestigation investigation) {
        this.investigation = investigation;
    }
    // </editor-fold >

    @Override
    public void PostLoad() {
        super.PostLoad();
        setM2mCheck(true);
    }
    // <editor-fold defaultstate="collapsed" desc="valueBeforeEnc">
    private String valueBeforeEnc;

    public String getValueBeforeEnc() {
        return valueBeforeEnc;
    }

    public void setValueBeforeEnc(String valueBeforeEnc) {
        this.valueBeforeEnc = valueBeforeEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="valueAfterEnc">
    private String valueAfterEnc;

    public String getValueAfterEnc() {
        return valueAfterEnc;
    }

    public void setValueAfterEnc(String valueAfterEnc) {
        this.valueAfterEnc = valueAfterEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="differenceEnc">
    private String differenceEnc;

    public String getDifferenceEnc() {
        return differenceEnc;
    }

    public void setDifferenceEnc(String differenceEnc) {
        this.differenceEnc = differenceEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="upgradeFactorEnc">
    private String upgradeFactorEnc;

    public String getUpgradeFactorEnc() {
        return upgradeFactorEnc;
    }

    public void setUpgradeFactorEnc(String upgradeFactorEnc) {
        this.upgradeFactorEnc = upgradeFactorEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notesEnc">
    private String notesEnc;

    public String getNotesEnc() {
        return notesEnc;
    }

    public void setNotesEnc(String notesEnc) {
        this.notesEnc = notesEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payrollMatrix">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayrollMatrix payrollMatrix;

    public PayrollMatrix getPayrollMatrix() {
        return payrollMatrix;
    }

    public void setPayrollMatrix(PayrollMatrix payrollMatrix) {
        this.payrollMatrix = payrollMatrix;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="profileHistory">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeProfileHistory profileHistory;

    public EmployeeProfileHistory getProfileHistory() {
        return profileHistory;
    }

    public void setProfileHistory(EmployeeProfileHistory profileHistory) {
        this.profileHistory = profileHistory;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="fromPayrollMatrix">
    @Transient
    private String fromPayrollMatrix;

    public String getFromPayrollMatrix() {
        if (payrollMatrix == null && profileHistory == null) {
            fromPayrollMatrix = "N";
        } else {
            fromPayrollMatrix = "Y";
        }

        return fromPayrollMatrix;
    }

    public String getFromPayrollMatrixDD() {
        return "EmployeeSalaryUpgrade_fromPayrollMatrix";
    }

    public void setFromPayrollMatrix(String fromPayrollMatrix) {
        this.fromPayrollMatrix = fromPayrollMatrix;
    }
    // </editor-fold >
}
