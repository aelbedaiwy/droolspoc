package com.unitedofoq.otms.appraisal.measure;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class KPAGroup extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="description">

    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "KPAGroup_description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="weight">
    @Column(precision = 25, scale = 13)
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getWeightDD() {
        return "KPAGroup_weight";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="kPAGroup">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private KPAGroup group;

    public KPAGroup getGroup() {
        return group;
    }

    public void setGroup(KPAGroup group) {
        this.group = group;
    }

    public String getGroupDD() {
        return "KPAGroup_group";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="kpa">
   /* @OneToMany
     private List<KPA> kpas;

     public List<KPA> getKpas() {
     return kpas;
     }

     public void setKpas(List<KPA> kpas) {
     this.kpas = kpas;
     }
     public String getKpasDD() {
     return "KPAGroup_kpas";

     }*/
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="kpaView">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private KPAView kpaView;

    public KPAView getKpaView() {
        return kpaView;
    }

    public void setKpaView(KPAView kpaView) {
        this.kpaView = kpaView;
    }

    public String getKpaViewDD() {
        return "KPAGroup_kpaView";
    }
    //</editor-fold>

}
