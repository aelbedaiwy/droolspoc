/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.utilities.audit;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendance;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @Aly El-Bedaiwy
 */
@Entity
public class EmployeeDailyAttendanceAudit extends BaseEntity {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    EmployeeDailyAttendance employeeDailyAttendance;

    public EmployeeDailyAttendance getEmployeeDailyAttendance() {
        return employeeDailyAttendance;
    }

    public String getEmployeeDailyAttendanceDD() {
        return "EmployeeDailyAttendanceAudit_employeeDailyAttendance";
    }

    public void setEmployeeDailyAttendance(EmployeeDailyAttendance employeeDailyAttendance) {
        this.employeeDailyAttendance = employeeDailyAttendance;
    }

//<editor-fold defaultstate="collapsed" desc="start">
    private String startTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTimeDD() {
        return "EmployeeDailyAttendanceAudit_startTime";
    }
    //</editor-fold>
//<editor-fold defaultstate="collapsed" desc="endTime">
    private String endTime;

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndTimeDD() {
        return "EmployeeDailyAttendanceAudit_endTime";
    }
    //</editor-fold>
//<editor-fold defaultstate="collapsed" desc="period">
    private String period;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriodDD() {
        return "EmployeeDailyAttendance_Period";
    }
    //</editor-fold>
//<editor-fold defaultstate="collapsed" desc="type">
    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "EmployeeDailyAttendance_Period";
    }
    //</editor-fold>
//<editor-fold defaultstate="collapsed" desc="sequence">
    @Column(name = "printsortindex")
    Integer sequence;

    //</editor-fold>
    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD() {
        return "EmployeeDailyAttendanceAudit_sequence";
    }
}
