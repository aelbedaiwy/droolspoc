package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name = "repempquesttranslation")
public class RepEmployeeQuestionaireTranslation extends RepAppraisalSheetTranslationBase {

    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    // </editor-fold>
}
