package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

@Entity
public class EmpVacAdjImport extends BaseEntity {

    private String empCode;
    private String vacCode;
    private String applyDate;
    private String adjustValue;
    private String comments;
    private boolean done;

    public String getEmpCode() {
        return empCode;
    }

    public String getEmpCodeDD() {
        return "EmployeeVacationAdjustmentImport_empCode";
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getVacCode() {
        return vacCode;
    }

    public String getVacCodeDD() {
        return "EmployeeVacationAdjustmentImport_vacCode";
    }

    public void setVacCode(String vacCode) {
        this.vacCode = vacCode;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public String getApplyDateDD() {
        return "EmployeeVacationAdjustmentImport_applyDate";
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getAdjustValue() {
        return adjustValue;
    }

    public String getAdjustValueDD() {
        return "EmployeeVacationAdjustmentImport_adjustValue";
    }

    public void setAdjustValue(String adjustValue) {
        this.adjustValue = adjustValue;
    }

    public String getComments() {
        return comments;
    }

    public String getCommentsDD() {
        return "EmployeeVacationAdjustmentImport_comments";
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public boolean isDone() {
        return done;
    }

    public String getDoneDD() {
        return "EmployeeVacationAdjustmentImport_done";
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
