/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields = {"company"})
public class MedicalCategories extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="Description">

    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "MedicalCategories_description";
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslatedDD() {
        return "SalaryElement_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Gender">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC gender;//(name = "reason")

    public UDC getGender() {
        return gender;
    }

    public void setGender(UDC gender) {
        this.gender = gender;
    }

    public String getGenderDD() {
        return "MedicalCategories_gender";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maritalStatus">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC maritalStatus;

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatusDD() {
        return "MedicalCategories_maritalStatus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parents">
    @Column
    private String parents;

    public String getParents() {
        return parents;
    }

    public void setParents(String parents) {
        this.parents = parents;
    }

    public String getParentsDD() {
        return "MedicalCategories_parents";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Spouse">
    @Column
    private String spouse;

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getSpouseDD() {
        return "MedicalCategories_spouse";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Children">
    @Column
    private String children;

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getChildrenDD() {
        return "MedicalCategories_children";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="YearlyAmount">
    @Column(precision = 18, scale = 3)
    private BigDecimal yearlyAmount;

    public BigDecimal getYearlyAmount() {
        return yearlyAmount;
    }

    public String getYearlyAmountDD() {
        return "MedicalCategories_yearlyAmount";
    }

    public void setYearlyAmount(BigDecimal yearlyAmount) {
        this.yearlyAmount = yearlyAmount;
    }
    @Transient
    private BigDecimal yearlyAmountMask;

    public BigDecimal getYearlyAmountMask() {
        yearlyAmountMask = yearlyAmount;
        return yearlyAmountMask;
    }

    public String getYearlyAmountMaskDD() {
        return "MedicalCategories_yearlyAmountMask";
    }

    public void setYearlyAmountMask(BigDecimal yearlyAmountMask) {
        updateDecimalValue("yearlyAmount", yearlyAmountMask);
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Conditions">
    @Column
    private String conditions;

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getConditionsDD() {
        return "MedicalCategories_conditions";
    }
// </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "MedicalCategories_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    // </editor-fold>
}