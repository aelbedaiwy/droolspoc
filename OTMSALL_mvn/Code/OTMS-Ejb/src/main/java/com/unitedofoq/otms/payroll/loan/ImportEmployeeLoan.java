/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

/**
 *
 * @author 3dly
 */
@Entity
public class ImportEmployeeLoan extends BaseEntity {

    //<editor-fold defaultstate="collpased" desc="employeeCode">
    private String employeeCode;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "ImportEmployeeLoan_employeeCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="loanCode">
    private String loanCode;

    public String getLoanCode() {
        return loanCode;
    }

    public void setLoanCode(String loanCode) {
        this.loanCode = loanCode;
    }

    public String getLoanCodeDD() {
        return "ImportEmployeeLoan_loanCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="startMonth">
    private String startMonth;

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartMonthDD() {
        return "ImportEmployeeLoan_startMonth";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="startYear">
    private String startYear;

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getStartYearDD() {
        return "ImportEmployeeLoan_startYear";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="loanValue">
    private String loanValue;

    public String getLoanValue() {
        return loanValue;
    }

    public void setLoanValue(String loanValue) {
        this.loanValue = loanValue;
    }

    public String getLoanValueDD() {
        return "ImportEmployeeLoan_loanValue";
    }
    
    private String loanValueEnc;

    public String getLoanValueEnc() {
        return loanValueEnc;
    }

    public void setLoanValueEnc(String loanValueEnc) {
        this.loanValueEnc = loanValueEnc;
    }
    
    //</editor-fold>
    //<editor-fold defaultstate="monthValue" desc="monthValue">
    private String monthValue;

    public String getMonthValue() {
        return monthValue;
    }

    public void setMonthValue(String monthValue) {
        this.monthValue = monthValue;
    }

    public String getMonthValueDD() {
        return "ImportEmployeeLoan_monthValue";
    }
    
    private String monthValueEnc;

    public String getMonthValueEnc() {
        return monthValueEnc;
    }

    public void setMonthValueEnc(String monthValueEnc) {
        this.monthValueEnc = monthValueEnc;
    }
        
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="installmentValue">
    private String installmentNumber;

    public String getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(String installmentNumber) {
        this.installmentNumber = installmentNumber;
    }
    
    private String installmentNumberEnc;

    public String getInstallmentNumberEnc() {
        return installmentNumberEnc;
    }

    public void setInstallmentNumberEnc(String installmentNumberEnc) {
        this.installmentNumberEnc = installmentNumberEnc;
    }
    
    
    public String getInstallmentNumberDD() {
        return "ImportEmployeeLoan_installmentNumber";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="done">
    private boolean done=false;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "ImportEmployeeLoan_done";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notes">
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "ImportEmployeeLoan_notes";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="approved">
    private String approvedBy;

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "ImportEmployeeLoan_approvedBy";
    }
    //</editor-fold>
}
