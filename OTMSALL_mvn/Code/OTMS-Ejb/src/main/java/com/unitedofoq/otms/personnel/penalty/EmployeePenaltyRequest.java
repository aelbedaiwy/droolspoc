/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.personnel.investigation.EmpInvestigation;
import com.unitedofoq.otms.timemanagement.employee.EmployeeMonthlyConsolidation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields = {"employee"})
public class EmployeePenaltyRequest extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeePenaltyRequest_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penalty">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty penalty;

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }

    public String getPenaltyDD() {
        return "EmployeePenaltyRequest_penalty";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC penaltyReason;//(name = "penalty_reason")

    public UDC getPenaltyReason() {
        return penaltyReason;
    }

    public void setPenaltyReason(UDC penaltyReason) {
        this.penaltyReason = penaltyReason;
    }

    public String getPenaltyReasonDD() {
        return "EmployeePenaltyRequest_penaltyReason";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyCancelReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC penaltyCancelReason;//(name = "penalty_cancel_reason")

    public UDC getPenaltyCancelReason() {
        return penaltyCancelReason;
    }

    public void setPenaltyCancelReason(UDC penaltyCancelReason) {
        this.penaltyCancelReason = penaltyCancelReason;
    }

    public String getPenaltyCancelReasonDD() {
        return "EmployeePenaltyRequest_penaltyCancelReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="givenBy">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee givenBy;//(name = "given_by")

    public Employee getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(Employee givenBy) {
        this.givenBy = givenBy;
    }

    public String getGivenByDD() {
        return "EmployeePenaltyRequest_givenBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date penaltyDate;//(name = "date_time")

    public Date getPenaltyDate() {
        return penaltyDate;
    }

    public void setPenaltyDate(Date penaltyDate) {
        this.penaltyDate = penaltyDate;
    }

    public String getPenaltyDateDD() {
        return "EmployeePenaltyRequest_penaltyDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyValue">
    @Column(precision = 18, scale = 3)
    private BigDecimal penaltyValue;//(name = "penalty_value")

    public BigDecimal getPenaltyValue() {
        return penaltyValue;
    }

    public void setPenaltyValue(BigDecimal penaltyValue) {
        this.penaltyValue = penaltyValue;
    }

    public String getPenaltyValueDD() {
        return "EmployeePenaltyRequest_penaltyValue";
    }
    @Transient
    private BigDecimal penaltyValueMask;

    public BigDecimal getPenaltyValueMask() {
        penaltyValueMask = penaltyValue;
        return penaltyValueMask;
    }

    public void setPenaltyValueMask(BigDecimal penaltyValueMask) {
        updateDecimalValue("penaltyValueMask", penaltyValueMask);
    }

    public String getPenaltyValueMaskDD() {
        return "EmployeePenaltyRequest_penaltyValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;//(name = "notes")

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeePenaltyRequest_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    //Y - N
    @Column(length = 1)
    private String cancelled;//(name = "cancelled")

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getCancelledDD() {
        return "EmployeePenaltyRequest_cancelled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelDate;//(name = "cancel_date")

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelDateDD() {
        return "EmployeePenaltyRequest_cancelDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelComment">
    @Column
    private String cancelComment;//(name = "cancel_comment")

    public String getCancelComment() {
        return cancelComment;
    }

    public void setCancelComment(String cancelComment) {
        this.cancelComment = cancelComment;
    }

    public String getCancelCommentDD() {
        return "EmployeePenaltyRequest_cancelComment";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="pointsCount">
    @Column(precision = 18, scale = 3)
    private BigDecimal pointsCount;//(name = "points_count")

    public BigDecimal getPointsCount() {
        return pointsCount;
    }

    public void setPointsCount(BigDecimal pointsCount) {
        this.pointsCount = pointsCount;
    }

    public String getPointsCountDD() {
        return "EmployeePenaltyRequest_pointsCount";
    }
    @Transient
    private BigDecimal pointsCountMask;

    public BigDecimal getPointsCountMask() {
        pointsCountMask = pointsCount;
        return pointsCountMask;
    }

    public void setPointsCountMask(BigDecimal pointsCountMask) {
        updateDecimalValue("pointsCount", pointsCountMask);
    }

    public String getPointsCountMaskDD() {
        return "EmployeePenaltyRequest_pointsCountMask";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="posted">
    @Column
    private String posted;//(name = "posted")

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getPostedDD() {
        return "EmployeePenaltyRequest_posted";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @JoinColumn
    private Employee cancelledBy;//(name = "cancelled_by")

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancelledByDD() {
        return "EmployeePenaltyRequest_cancelledBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factor">
    @Column(precision = 18, scale = 3)
    private BigDecimal factor;//(name = "vacation_factor")

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public String getFactorDD() {
        return "EmployeePenaltyRequest_factor";
    }
    @Transient
    private BigDecimal factorMask;

    public BigDecimal getFactorMask() {
        factorMask = factor;
        return factorMask;
    }

    public void setFactorMask(BigDecimal factorMask) {
        updateDecimalValue("factor", factorMask);
    }

    public String getFactorMaskDD() {
        return "EmployeePenaltyRequest_factorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="previousPenaltyDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date previousPenaltyDate;//(name = "prev_penalty_date")

    public Date getPreviousPenaltyDate() {
        return previousPenaltyDate;
    }

    public void setPreviousPenaltyDate(Date previousPenaltyDate) {
        this.previousPenaltyDate = previousPenaltyDate;
    }

    public String getPreviousPenaltyDateDD() {
        return "EmployeePenaltyRequest_previousPenaltyDate";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyNo">
    @Column
    private Long penaltyNo;//(name = "penalty_no")

    public Long getPenaltyNo() {
        return penaltyNo;
    }

    public void setPenaltyNo(Long penaltyNo) {
        this.penaltyNo = penaltyNo;
    }

    public String getPenaltyNoDD() {
        return "EmployeePenaltyRequest_penaltyNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyNoDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date penaltyNoDate;//(name = "penalty_no_date")

    public Date getPenaltyNoDate() {
        return penaltyNoDate;
    }

    public void setPenaltyNoDate(Date penaltyNoDate) {
        this.penaltyNoDate = penaltyNoDate;
    }

    public String getPenaltyNoDateDD() {
        return "EmployeePenaltyRequest_penaltyNoDate";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyNoApproved">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee penaltyNoApprovedby;//(name = "penalty_no_approved")

    public Employee getPenaltyNoApprovedby() {
        return penaltyNoApprovedby;
    }

    public void setPenaltyNoApprovedby(Employee penaltyNoApprovedby) {
        this.penaltyNoApprovedby = penaltyNoApprovedby;
    }

    public String getPenaltyNoApprovedbyDD() {
        return "EmployeePenaltyRequest_penaltyNoApprovedby";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="investigation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmpInvestigation investigation;

    public EmpInvestigation getInvestigation() {
        return investigation;
    }

    public String getInvestigationDD() {
        return "EmployeePenaltyRequest_investigation";
    }

    public void setInvestigation(EmpInvestigation investigation) {
        this.investigation = investigation;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="consolidation">
    @ManyToOne(fetch = FetchType.LAZY)
    private EmployeeMonthlyConsolidation consolidation;

    public EmployeeMonthlyConsolidation getConsolidation() {
        return consolidation;
    }

    public void setConsolidation(EmployeeMonthlyConsolidation consolidation) {
        this.consolidation = consolidation;
    }

    public String getConsolidationDD() {
        return "EmployeePenaltyRequest_consolidation";
    }
    // </editor-fold>
}
