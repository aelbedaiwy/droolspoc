package com.unitedofoq.otms.personnel.dayoff;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"company"})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DAYOFFTYPE")
public class DayOff extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "DayOff_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "DayOff_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitOfMeasure">
    @Column(length = 1)
    /*
     * H Hour - D Day - W Week - M Month - Y
     * */
    private String unitOfMeasure;//unit_of_measure

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getUnitOfMeasureDD() {
        return "DayOff_unitOfMeasure";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshUnitOfMeasure">
    /* M Month - Y Year
     * */
    @Column(length = 1)
    private String refreshUnitOfMeasure;//refresh_unit_of_measure

    public String getRefreshUnitOfMeasure() {
        return refreshUnitOfMeasure;
    }

    public void setRefreshUnitOfMeasure(String refreshUnitOfMeasure) {
        this.refreshUnitOfMeasure = refreshUnitOfMeasure;
    }

    public String getRefreshUnitOfMeasureDD() {
        return "DayOff_refreshUnitOfMeasure";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshEvery">
    @Column(precision = 18, scale = 3)
    private BigDecimal refreshEvery;//refresh_every

    public BigDecimal getRefreshEvery() {
        return refreshEvery;
    }

    public void setRefreshEvery(BigDecimal refreshEvery) {
        this.refreshEvery = refreshEvery;
    }

    public String getRefreshEveryDD() {
        return "DayOff_refreshEvery";
    }
    @Transient
    private BigDecimal refreshEveryMask;//refresh_every

    public BigDecimal getRefreshEveryMask() {
        refreshEveryMask = refreshEvery;
        return refreshEveryMask;
    }

    public void setRefreshEveryMask(BigDecimal refreshEveryMask) {
        updateDecimalValue("refreshEvery", refreshEveryMask);
    }

    public String getRefreshEveryMaskDD() {
        return "DayOff_refreshEveryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="posted">
    @Column(length = 1)
    // Y - N
    private String posted;//posted

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getPostedDD() {
        return "DayOff_posted";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision = 18, scale = 3)
    private BigDecimal maximumValue; // max_value

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "DayOff_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask; // max_value

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue", maximumValueMask);
    }

    public String getMaximumValueMaskDD() {
        return "DayOff_maximumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumToPost">
    @Column(precision = 18, scale = 3)
    private BigDecimal maximumToPost;//max_to_post

    public BigDecimal getMaximumToPost() {
        return maximumToPost;
    }

    public void setMaximumToPost(BigDecimal maximumToPost) {
        this.maximumToPost = maximumToPost;
    }

    public String getMaximumToPostDD() {
        return "DayOff_maximumToPost";
    }
    @Transient
    private BigDecimal maximumToPostMask;//max_to_post

    public BigDecimal getMaximumToPostMask() {
        maximumToPostMask = maximumToPost;
        return maximumToPostMask;
    }

    public void setMaximumToPostMask(BigDecimal maximumToPostMask) {
        updateDecimalValue("maximumToPost", maximumToPostMask);
    }

    public String getMaximumToPostMaskDD() {
        return "DayOff_maximumToPostMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="overrideLimit">
    @Column(length = 1)
    // Y - N
    private String overrideLimit;//override_limit

    public String getOverrideLimit() {
        return overrideLimit;
    }

    public void setOverrideLimit(String overrideLimit) {
        this.overrideLimit = overrideLimit;
    }

    public String getOverrideLimitDD() {
        return "DayOff_overrideLimit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="removeHoliday">
    @Column(length = 1)
    //Y  - N
    private String removeHoliday;//rem_holiday_from_balance

    public String getRemoveHoliday() {
        return removeHoliday;
    }

    public void setRemoveHoliday(String removeHoliday) {
        this.removeHoliday = removeHoliday;
    }

    public String getRemoveHolidayDD() {
        return "DayOff_removeHoliday";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="group">
    @Column(length = 1)
    //Y is group vacation- N normal vacatioon
    private String isGroup;//group_vacation

    public String getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(String isGroup) {
        this.isGroup = isGroup;
    }

    public String getIsGroupDD() {
        return "DayOff_isGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="groupType">
    @Column(length = 1)
    //L Limit On Group
    //B Balance On Group
    //N Not Applicable
    private String groupType;//group_type

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupTypeDD() {
        return "DayOff_groupType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingCalender">
    @Column(length = 1)
    //Y - N
    private String workingCalender;//working_calender_vacation

    public String getWorkingCalender() {
        return workingCalender;
    }

    public void setWorkingCalender(String workingCalender) {
        this.workingCalender = workingCalender;
    }

    public String getWorkingCalenderDD() {
        return "DayOff_workingCalender";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dType">
    @Transient
    private String dtype;

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getDtypeDD() {
        return "DayOff_dtype";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "DayOff_company";
    }
    // </editor-fold>
    /* // <editor-fold defaultstate="collapsed" desc="dayOffPenaltys">
     @OneToMany(mappedBy="dayOffPenaltys")
     private List<DayOffPenalty> dayOffPenaltys;

     public List<DayOffPenalty> getDayOffPenaltys() {
     return dayOffPenaltys;
     }

     public void setDayOffPenaltys(List<DayOffPenalty> dayOffPenaltys) {
     this.dayOffPenaltys = dayOffPenaltys;
     }
     public String getDayOffPenaltysDD() {
     return "DayOff_dayOffPenaltys";
     }


     // </editor-fold>?*/
    //<editor-fold defaultstate="collapsed" desc="internalCode">
    @Column(unique = true)
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getInternalCodeDD() {
        return "DayOff_internalCode";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prorateHireTerminate">
    @Column(length = 1)
    //Y-N
    private String prorateHireTerminate;

    public String getProrateHireTerminate() {
        return prorateHireTerminate;
    }

    public void setProrateHireTerminate(String prorateHireTerminate) {
        this.prorateHireTerminate = prorateHireTerminate;
    }

    public String getProrateHireTerminateDD() {
        return "DayOff_prorateHireTerminate";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="prorateRequest">
    @Column(length = 1)
    //Y-N
    private String prorateRequest;

    public String getProrateRequest() {
        return prorateRequest;
    }

    public void setProrateRequest(String prorateRequest) {
        this.prorateRequest = prorateRequest;
    }

    public String getProrateRequestDD() {
        return "DayOff_prorateRequest";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prorateDetail">
    @Column(length = 1)
    //Y-N
    private String prorateDetail;

    public String getProrateDetail() {
        return prorateDetail;
    }

    public void setProrateDetail(String prorateDetail) {
        this.prorateDetail = prorateDetail;
    }

    public String getProrateDetailDD() {
        return "DayOff_prorateDetail";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="eligibleDangerDays">
    private boolean eligibleDangerDays;

    public boolean isEligibleDangerDays() {
        return eligibleDangerDays;
    }

    public void setEligibleDangerDays(boolean eligibleDangerDays) {
        this.eligibleDangerDays = eligibleDangerDays;
    }

    public String getEligibleDangerDaysDD() {
        return "DayOff_eligibleDangerDays";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="prorateDangerDays">
    private boolean prorateDangerDays;

    public boolean isProrateDangerDays() {
        return prorateDangerDays;
    }

    public void setProrateDangerDays(boolean prorateDangerDays) {
        this.prorateDangerDays = prorateDangerDays;
    }

    public String getProrateDangerDaysDD() {
        return "DayOff_prorateDangerDays";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="dangerDaysNo">
    @Column(precision = 18, scale = 3)
    private BigDecimal dangerDaysNo;

    public BigDecimal getDangerDaysNo() {
        return dangerDaysNo;
    }

    public void setDangerDaysNo(BigDecimal dangerDaysNo) {
        this.dangerDaysNo = dangerDaysNo;
    }

    public String getDangerDaysNoDD() {
        return "DayOff_dangerDaysNo";
    }
    @Transient
    private BigDecimal dangerDaysNoMask;

    public BigDecimal getDangerDaysNoMask() {
        dangerDaysNoMask = dangerDaysNo;
        return dangerDaysNoMask;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "DayOff_legalEntity";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "DayOff_legalEntityGroup";
    }
    //</editor-fold>
    @Column(name = "maximumtorequestpermonth")
    private BigDecimal maximumRequestPerMonth;

    public BigDecimal getMaximumRequestPerMonth() {
        return maximumRequestPerMonth;
    }

    public String getMaximumRequestPerMonthDD() {
        return "DayOff_maximumRequestPerMonth";
    }

    public void setMaximumRequestPerMonth(BigDecimal maximumRequestPerMonth) {
        this.maximumRequestPerMonth = maximumRequestPerMonth;
    }
    // <editor-fold defaultstate="collapsed" desc="acceptsHours">
    private boolean acceptsHours;

    public boolean isAcceptsHours() {
        return acceptsHours;
    }

    public void setAcceptsHours(boolean acceptsHours) {
        this.acceptsHours = acceptsHours;
    }

    public String getAcceptsHoursDD() {
        return "DayOff_acceptsHours";
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="oneReqForPeriod">
    private boolean oneReqForPeriod;

    public boolean isOneReqForPeriod() {
        return oneReqForPeriod;
    }

    public void setOneReqForPeriod(boolean oneReqForPeriod) {
        this.oneReqForPeriod = oneReqForPeriod;
    }

    public String getOneReqForPeriodDD() {
        return "Vacation_oneReqForPeriod";
    }
    // </editor-fold>

    @Column
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageDD() {
        return "DayOff_message";
    }

    // <editor-fold defaultstate="collapsed" desc="postAdjustment">
    private boolean postAdjustment;

    public boolean isPostAdjustment() {
        return postAdjustment;
    }

    public void setPostAdjustment(boolean postAdjustment) {
        this.postAdjustment = postAdjustment;
    }

    public String getPostAdjustmentDD() {
        return "DayOff_postAdjustment";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="postAdjustmentmonth">
    @Column(precision = 18, scale = 3)
    private BigDecimal postAdjustmentmonth;

    public BigDecimal getPostAdjustmentmonth() {
        return postAdjustmentmonth;
    }

    public void setPostAdjustmentmonth(BigDecimal postAdjustmentmonth) {
        this.postAdjustmentmonth = postAdjustmentmonth;
    }

    public String getPostAdjustmentmonthDD() {
        return "DayOff_postAdjustmentmonth";
    }
    @Transient
    private BigDecimal postAdjustmentmonthMask;

    public BigDecimal getPostAdjustmentmonthMask() {
        postAdjustmentmonthMask = postAdjustmentmonth;
        return postAdjustmentmonthMask;
    }
    // </editor-fold>
}
