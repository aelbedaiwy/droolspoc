/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.foundation.unit.Unit;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"position"})
public class PositionHistory extends BaseEntity {
    
   private String action;
    
    private int sortIndex;
    
    private String hierarchyCode;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit parentUnit;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position parentPosition;

    public String getAction() {
        return action;
    }
    
    public String getActionDD() {
        return "PositionHistory_action";
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHierarchyCode() {
        return hierarchyCode;
    }
    
    public String getHierarchyCodeDD() {
        return "PositionHistory_hierarchyCode";
    }

    public void setHierarchyCode(String hierarchyCode) {
        this.hierarchyCode = hierarchyCode;
    }

    public Unit getParentUnit() {
        return parentUnit;
    }
    
    public String getParentUnitDD() {
        return "PositionHistory_parentUnit";
    }

    public void setParentUnit(Unit parentUnit) {
        this.parentUnit = parentUnit;
    }

    public int getSortIndex() {
        return sortIndex;
    }
    
    public String getSortIndexDD() {
        return "PositionHistory_sortIndex";
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser user;

    public OUser getUser() {
        return user;
    }
    
    public String getUserDD() {
        return "PositionHistory_user";
    }

    public void setUser(OUser user) {
        this.user = user;
    }
    
    private String name;

    public String getName() {
        return name;
    }
    
    public String getNameDD() {
        return "PositionHistory_name";
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getParentPosition() {
        return parentPosition;
    }
    
    public String getParentPositionDD() {
        return "PositionHistory_parentPosition";
    }

    public void setParentPosition(Position parentPosition) {
        this.parentPosition = parentPosition;
    }

    public Position getPosition() {
        return position;
    }
    
    public String getPositionDD() {
        return "PositionHistory_position";
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
    
    
}
