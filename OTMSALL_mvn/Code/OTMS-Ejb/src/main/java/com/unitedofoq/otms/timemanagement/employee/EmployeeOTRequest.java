/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author mmohamed
 */
@Entity
public class EmployeeOTRequest extends BaseEntity {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestedDate;

    private String timeFrom;

    private String timeTo;

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeOTRequest_employee";
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getRequestedDate() {
        return requestedDate;
    }

    public String getRequestedDateDD() {
        return "EmployeeOTRequest_requestedDate";
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public String getTimeFromDD() {
        return "EmployeeOTRequest_timeFrom";
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public String getTimeToDD() {
        return "EmployeeOTRequest_timeTo";
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    //<editor-fold defaultstate="collapsed" desc="approved">
    @Column(length = 1)
    private String approved;

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getApprovedDD() {
        return "EmployeeOTRequest_approved";
    }
    //</editor-fold>

}
