package com.unitedofoq.otms.recruitment.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepCompanyLevelBase;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class RepVacancy extends RepCompanyLevelBase {
    // <editor-fold defaultstate="collapsed" desc="dsDBID">

    @Column
    private long dsDBID;

    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }

    public long getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepVacancy_dsDBID";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetedNoReq">
    @Column
    private long budgetedNoReq;

    public long getBudgetedNoReq() {
        return budgetedNoReq;
    }

    public String getBudgetedNoReqDD() {
        return "RepVacancy_budgetedNoReq";
    }

    public void setBudgetedNoReq(long budgetedNoReq) {
        this.budgetedNoReq = budgetedNoReq;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="budgetedWithReq">
    @Column
    private long budgetedWithReq;

    public long getBudgetedWithReq() {
        return budgetedWithReq;
    }

    public String getBudgetedWithReqDD() {
        return "RepVacancy_budgetedWithReq";
    }

    public void setBudgetedWithReq(long budgetedWithReq) {
        this.budgetedWithReq = budgetedWithReq;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="unBudgetedNoReq">
    @Column
    private long unBudgetedNoReq;

    public long getUnBudgetedNoReq() {
        return unBudgetedNoReq;
    }

    public String getUnBudgetedNoReqDD() {
        return "RepVacancy_unBudgetedNoReq";
    }

    public void setUnBudgetedNoReq(long unBudgetedNoReq) {
        this.unBudgetedNoReq = unBudgetedNoReq;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="unBudgetedWithReq">
    @Column
    private long unBudgetedWithReq;

    public long getUnBudgetedWithReq() {
        return unBudgetedWithReq;
    }

    public String getUnBudgetedWithReqDD() {
        return "RepVacancy_unBudgetedWithReq";
    }

    public void setUnBudgetedWithReq(long unBudgetedWithReq) {
        this.unBudgetedWithReq = unBudgetedWithReq;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="replacmentNoReq">
    @Column
    private long replacmentNoReq;

    public long getReplacmentNoReq() {
        return replacmentNoReq;
    }

    public String getReplacmentNoReqDD() {
        return "RepVacancy_replacmentNoReq";
    }

    public void setReplacmentNoReq(long replacmentNoReq) {
        this.replacmentNoReq = replacmentNoReq;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="replacmentWithReq">
    @Column
    private long replacmentWithReq;

    public long getReplacmentWithReq() {
        return replacmentWithReq;
    }

    public String getReplacmentWithReqDD() {
        return "RepVacancy_replacmentWithReq";
    }

    public void setReplacmentWithReq(long replacmentWithReq) {
        this.replacmentWithReq = replacmentWithReq;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="positionOccupation">
    @Column
    private long positionOccupation;

    public long getPositionOccupation() {
        return positionOccupation;
    }

    public String getPositionOccupationDD() {
        return "RepVacancy_positionOccupation";
    }

    public void setPositionOccupation(long positionOccupation) {
        this.positionOccupation = positionOccupation;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public String getPositionNameDD() {
        return "RepVacancy_positionName";
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public String getJobName() {
        return jobName;
    }

    public String getJobNameDD() {
        return "RepVacancy_jobName";
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public String getUnitName() {
        return unitName;
    }

    public String getUnitNameDD() {
        return "RepVacancy_unitName";
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    @Translatable(translationField = "locationDescriptionTranslated")
    private String locationDescription;

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescriptionDD() {
        return "RepVacancy_locationDescription";
    }
    @Transient
    @Translation(originalField = "locationDescription")
    private String locationDescriptionTranslated;

    public String getLocationDescriptionTranslated() {
        return locationDescriptionTranslated;
    }

    public void setLocationDescriptionTranslated(String locationDescriptionTranslated) {
        this.locationDescriptionTranslated = locationDescriptionTranslated;
    }

    public String getLocationDescriptionTranslatedDD() {
        return "RepVacancy_locationDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    @Translatable(translationField = "costCenterDescriptionTranslated")
    private String costCenterDescription;

    public String getCostCenterDescription() {
        return costCenterDescription;
    }

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescriptionDD() {
        return "RepVacancy_costCenterDescription";
    }
    @Transient
    @Translation(originalField = "costCenterDescription")
    private String costCenterDescriptionTranslated;

    public String getCostCenterDescriptionTranslated() {
        return costCenterDescriptionTranslated;
    }

    public void setCostCenterDescriptionTranslated(String costCenterDescriptionTranslated) {
        this.costCenterDescriptionTranslated = costCenterDescriptionTranslated;
    }

    public String getCostCenterDescriptionTranslatedDD() {
        return "RepVacancy_costCenterDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    @Translatable(translationField = "payGradeDescriptionTranslated")
    private String payGradeDescription;

    public String getPayGradeDescription() {
        return payGradeDescription;
    }

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescriptionDD() {
        return "RepVacancy_payGradeDescription";
    }
    @Transient
    @Translation(originalField = "payGradeDescription")
    private String payGradeDescriptionTranslated;

    public String getPayGradeDescriptionTranslated() {
        return payGradeDescriptionTranslated;
    }

    public void setPayGradeDescriptionTranslated(String payGradeDescriptionTranslated) {
        this.payGradeDescriptionTranslated = payGradeDescriptionTranslated;
    }

    public String getPayGradeDescriptionTranslatedDD() {
        return "RepVacancy_payGradeDescription";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    @Translatable(translationField = "hiringTypeTranslated")
    private String hiringType;

    public String getHiringType() {
        return hiringType;
    }

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringTypeDD() {
        return "RepVacancy_hiringType";
    }
    @Transient
    @Translation(originalField = "hiringType")
    private String hiringTypeTranslated;

    public String getHiringTypeTranslated() {
        return hiringTypeTranslated;
    }

    public void setHiringTypeTranslated(String hiringTypeTranslated) {
        this.hiringTypeTranslated = hiringTypeTranslated;
    }

    public String getHiringTypeTranslatedDD() {
        return "RepVacancy_hiringType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="headcount">
    @Column(nullable = false)
    private Integer headcount;

    public Integer getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Integer headcount) {
        this.headcount = headcount;
    }

    public String getHeadcountDD() {
        return "RepVacancy_headcount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacancy">
    @Column
    private Integer vacancy;

    public Integer getVacancy() {
        return vacancy;
    }

    public void setVacancy(Integer vacancy) {
        this.vacancy = vacancy;
    }

    public String getVacancyDD() {
        return "RepVacancy_vacancy";
    }
    // </editor-fold>
}
