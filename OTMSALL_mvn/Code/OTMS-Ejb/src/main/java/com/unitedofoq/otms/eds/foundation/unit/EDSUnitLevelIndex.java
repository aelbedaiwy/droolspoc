/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import com.unitedofoq.otms.foundation.unit.UnitLevelIndexBase;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author mmohamed
 */
@Entity
@Table(name="UnitLevelIndex")
@ParentEntity(fields={"company"})
@DiscriminatorValue("MASTER")
public class EDSUnitLevelIndex extends UnitLevelIndexBase {
   
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    EDSCompany company;   

    public EDSCompany getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "EDSUnitLevelIndex_company";
    }

    public void setCompany(EDSCompany company) {
        this.company = company;
    }   

}
