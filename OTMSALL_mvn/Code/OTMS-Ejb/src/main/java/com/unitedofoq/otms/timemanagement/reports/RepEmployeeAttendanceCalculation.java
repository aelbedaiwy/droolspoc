package com.unitedofoq.otms.timemanagement.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name = "repempattcalc")
public class RepEmployeeAttendanceCalculation extends RepEmployeeBase {

    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private Long dsDbid;

    public Long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(Long dsDbid) {
        this.dsDbid = dsDbid;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dailyDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dailyDate;

    public void setDailyDate(Date dailyDate) {
        this.dailyDate = dailyDate;
    }

    public Date getDailyDate() {
        return dailyDate;
    }

    public String getDailyDateDD() {
        return "RepEmployeeAttendanceCalculation_dailyDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeIn">
    @Column
    private String timeIn;

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public String getTimeInDD() {
        return "RepEmployeeAttendanceCalculation_timeIn";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeOut">
    @Column
    private String timeOut;

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public String getTimeOutDD() {
        return "RepEmployeeAttendanceCalculation_timeOut";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="earlyLeave">
    @Column(precision = 25, scale = 13)
    private BigDecimal earlyLeave;

    public BigDecimal getEarlyLeave() {
        return earlyLeave;
    }

    public void setEarlyLeave(BigDecimal earlyLeave) {
        this.earlyLeave = earlyLeave;
    }

    public String getEarlyLeaveDD() {
        return "RepEmployeeAttendanceCalculation_earlyLeave";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="delay">
    @Column(precision = 25, scale = 13)
    private BigDecimal delay;

    public BigDecimal getDelay() {
        return delay;
    }

    public void setDelay(BigDecimal delay) {
        this.delay = delay;
    }

    public String getDelayDD() {
        return "RepEmployeeAttendanceCalculation_delay";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOverTime">
    @Column(precision = 25, scale = 13)
    private BigDecimal dayOverTime;

    public BigDecimal getDayOverTime() {
        return dayOverTime;
    }

    public void setDayOverTime(BigDecimal dayOverTime) {
        this.dayOverTime = dayOverTime;
    }

    public String getDayOverTimeDD() {
        return "RepEmployeeAttendanceCalculation_dayOverTime";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nightOverTime">
    @Column(precision = 25, scale = 13)
    private BigDecimal nightOverTime;

    public BigDecimal getNightOverTime() {
        return nightOverTime;
    }

    public void setNightOverTime(BigDecimal nightOverTime) {
        this.nightOverTime = nightOverTime;
    }

    public String getNightOverTimeDD() {
        return "RepEmployeeAttendanceCalculation_nightOverTime";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lessWork">
    @Column(precision = 25, scale = 13)
    private BigDecimal lessWork;

    public BigDecimal getLessWork() {
        return lessWork;
    }

    public void setLessWork(BigDecimal lessWork) {
        this.lessWork = lessWork;
    }

    public String getLessWorkDD() {
        return "RepEmployeeAttendanceCalculation_lessWork";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="breakIn">
    @Column
    private String breakIn;

    public void setBreakIn(String breakIn) {
        this.breakIn = breakIn;
    }

    public String getBreakIn() {
        return breakIn;
    }

    public String getBreakInDD() {
        return "RepEmployeeAttendanceCalculation_breakIn";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="breakOut">
    @Column
    private String breakOut;

    public void setBreakOut(String breakOut) {
        this.breakOut = breakOut;
    }

    public String getBreakOut() {
        return breakOut;
    }

    public String getBreakOutDD() {
        return "RepEmployeeAttendanceCalculation_breakOut";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayNature">
    @Column
    @Translatable(translationField = "dayNatureTranslated")
    private String dayNature;

    public void setDayNature(String dayNature) {
        this.dayNature = dayNature;
    }

    public String getDayNature() {
        return dayNature;
    }

    public String getDayNatureDD() {
        return "RepEmployeeAttendanceCalculation_dayNature";
    }

    @Transient
    @Translation(originalField = "dayNature")
    private String dayNatureTranslated;

    public String getDayNatureTranslated() {
        return dayNatureTranslated;
    }

    public void setDayNatureTranslated(String dayNatureTranslated) {
        this.dayNatureTranslated = dayNatureTranslated;
    }

    public String getDayNatureTranslatedDD() {
        return "RepEmployeeAttendanceCalculation_dayNature";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="machineDescription">
    private String machineDescription;

    public String getMachineDescription() {
        return machineDescription;
    }

    public void setMachineDescription(String machineDescription) {
        this.machineDescription = machineDescription;
    }

    public String getMachineDescriptionDD() {
        return "RepEmployeeAttendanceCalculation_machineDescription";
    }
    //</editor-fold>

    @Column
    private String missionHours;

    public String getMissionHours() {
        return missionHours;
    }

    public void setMissionHours(String missionHours) {
        this.missionHours = missionHours;
    }

    public String getMissionHoursDD() {
        return "RepEmployeeAttendanceCalculation_missionHours";
    }
}
