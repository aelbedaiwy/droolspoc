
package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class RepPositionKPATranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    private String fullName;

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionDescription">
    @Column
    private String positionDescription;

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public String getPositionDescription() {
        return positionDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unit">
    @Column
    private String unit;

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }
    // </editor-fold>

}
