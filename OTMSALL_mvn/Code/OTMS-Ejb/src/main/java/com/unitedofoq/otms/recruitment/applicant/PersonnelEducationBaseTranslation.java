
package com.unitedofoq.otms.recruitment.applicant;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
@Table(name="peredubasetranslation")
public class PersonnelEducationBaseTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="schDescription">
    @Column
    private String schDescription;

    public void setSchDescription(String schDescription) {
        this.schDescription = schDescription;
    }

    public String getSchDescription() {
        return schDescription;
    }
    // </editor-fold>

}