/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author arezk
 */
@Entity
@Table(name="oemployee")
@DiscriminatorValue(value="REPORTING")
public class SFReportingEmployee extends Employee{
    
}
