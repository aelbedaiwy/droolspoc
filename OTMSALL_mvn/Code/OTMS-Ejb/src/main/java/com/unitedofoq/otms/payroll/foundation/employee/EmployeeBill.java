
package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"employee"})
public class EmployeeBill extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeBill_employee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="month">
    @Column
    private int month;

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMonth() {
        return month;
    }

    public String getMonthDD() {
        return "EmployeeBill_month";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobileAmount">
    @Column
    private BigDecimal mobileAmount;

    public void setMobileAmount(BigDecimal mobileAmount) {
        this.mobileAmount = mobileAmount;
    }

    public BigDecimal getMobileAmount() {
        return mobileAmount;
    }

    public String getMobileAmountDD() {
        return "EmployeeBill_mobileAmount";
    }
    
    @Transient
    private BigDecimal mobileAmountMask;
    public BigDecimal getMobileAmountMask() {
        mobileAmountMask = mobileAmount ;
        return mobileAmountMask;
    }
    public void setMobileAmountMask(BigDecimal mobileAmountMask) {
        updateDecimalValue("mobileAmount",mobileAmountMask);
    }
    public String getMobileAmountMaskDD() {
        return "EmployeeBill_mobileAmountMask";
    }
    // </editor-fold>
    
    @Column
    private BigDecimal mobileBenifit;
    
    @Column
    private BigDecimal mobile1Benifit;

    public BigDecimal getMobile1Benifit() {
        return mobile1Benifit;
    }
    
    public String getMobile1BenifitDD() {
        return "EmployeeBill_mobile1Benifit";
    }

    public void setMobile1Benifit(BigDecimal mobile1Benifit) {
        this.mobile1Benifit = mobile1Benifit;
    }

    public BigDecimal getMobileBenifit() {
        return mobileBenifit;
    }
    
    public String getMobileBenifitDD() {
        return "EmployeeBill_mobileBenifit";
    }

    public void setMobileBenifit(BigDecimal mobileBenifit) {
        this.mobileBenifit = mobileBenifit;
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="mobileDifference">
    @Column
    private BigDecimal mobileDifference;

    public void setMobileDifference(BigDecimal mobileDifference) {
        this.mobileDifference = mobileDifference;
    }

    public BigDecimal getMobileDifference() {
        return mobileDifference;
    }

    public String getMobileDifferenceDD() {
        return "EmployeeBill_mobileDifference";
    }
    
    @Transient
    private BigDecimal mobileDifferenceMask;
    public BigDecimal getMobileDifferenceMask() {
        mobileDifferenceMask = mobileDifference ;
        return mobileDifferenceMask;
    }
    public void setMobileDifferenceMask(BigDecimal mobileDifferenceMask) {
        updateDecimalValue("mobileDifference",mobileDifferenceMask);
    }
    public String getMobileDifferenceMaskDD() {
        return "EmployeeBill_mobileDifferenceMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobile1Amount">
    @Column
    private BigDecimal mobile1Amount;

    public void setMobile1Amount(BigDecimal mobile1Amount) {
        this.mobile1Amount = mobile1Amount;
    }

    public BigDecimal getMobile1Amount() {
        return mobile1Amount;
    }

    public String getMobile1AmountDD() {
        return "EmployeeBill_mobile1Amount";
    }
    
    @Transient
    private BigDecimal mobile1AmountMask;
    public BigDecimal getMobile1AmountMask() {
        mobile1AmountMask = mobile1Amount ;
        return mobile1AmountMask;
    }
    public void setMobile1AmountMask(BigDecimal mobile1AmountMask) {
        updateDecimalValue("mobile1Amount",mobile1AmountMask);
    }
    public String getMobile1AmountMaskDD() {
        return "EmployeeBill_mobile1AmountMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mobile1Difference">
    @Column
    private BigDecimal mobile1Difference;

    public void setMobile1Difference(BigDecimal mobile1Difference) {
        this.mobile1Difference = mobile1Difference;
    }

    public BigDecimal getMobile1Difference() {
        return mobile1Difference;
    }

    public String getMobile1DifferenceDD() {
        return "EmployeeBill_mobile1Difference";
    }
    
    @Transient
    private BigDecimal mobile1DifferenceMask;
    public BigDecimal getMobile1DifferenceMask() {
        mobile1DifferenceMask = mobile1Difference ;
        return mobile1DifferenceMask;
    }
    public void setMobile1DifferenceMask(BigDecimal mobile1DifferenceMask) {
        updateDecimalValue("mobile1Difference",mobile1DifferenceMask);
    }
    public String getMobile1DifferenceMaskDD() {
        return "EmployeeBill_mobile1DifferenceMask";
    }
    // </editor-fold>
    
    
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }
    
    public String getM2mCheckDD() {
        return "EmployeeBill_m2mCheck";
    }

    @Override
    public void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
}