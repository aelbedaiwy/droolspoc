/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.jobrequisitionapplicant;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.validation.DateFromToValidation;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.occupation.OccupationElementScale;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields={"requisitionApplicant"})
@DateFromToValidation (from={"scheduledStartDate", "actualStartDate"},
                         to={"scheduledEndDate",   "actualEndDate"})
public class JobReqAppEvent extends BaseEntity {

    //Attributes from direct assosiation relations
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private JobReqAppEventNotification notification;
    public String getNotificationDD() {
        return "JobReqAppEvent_notification";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC location;
    public String getLocationDD() {
        return "JobReqAppEvent_location";
    }
     
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC eventType;
    public String getEventTypeDD() {
        return "JobReqAppEvent_eventType";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OUser eventOwner;
    public String getEventOwnerDD() {
        return "JobReqAppEvent_eventOwner";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="JobRequisitionApplicantID")
    private JobRequisitionApplicant requisitionApplicant;
    public String getRequisitionApplicantDD() {
        return "JobReqAppEvent_requisitionApplicant";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee evaluator;
    public String getEvaluatorDD() {
        return "JobReqAppEvent_evaluator";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OccupationElementScale gradeScale;
    public String getGradeScaleDD() {
        return "JobReqAppEvent_gradeScale";
    }

    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
    private Date scheduledStartDate;
    public String getScheduledStartDateDD() {
        return "JobReqAppEvent_scheduledStartDate";
    }
    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
    private Date scheduledEndDate;
    public String getScheduledEndDateDD() {
        return "JobReqAppEvent_scheduledEndDate";
    }
    private String prerequisite;
    public String getPrerequisiteDD() {
        return "JobReqAppEvent_prerequisite";
    }
    @Temporal(TemporalType.DATE)
    private Date actualStartDate;
    public String getActualStartDateDD() {
        return "JobReqAppEvent_actualStartDate";
    }
    @Temporal(TemporalType.DATE)
    private Date actualEndDate;
    public String getActualEndDateDD() {
        return "JobReqAppEvent_actualEndDate";
    }
    private String comments;
    public String getCommentsDD() {
        return "JobReqAppEvent_comments";
    }
    private String recommendations;
    public String getRecommendationsDD() {
        return "JobReqAppEvent_recommendations";
    }
    private Double grade;
    public String getGradeDD() {
        return "JobReqAppEvent_grade";
    }
    public Date getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(Date actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public Date getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(Date actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Employee getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(Employee evaluator) {
        this.evaluator = evaluator;
    }

    public OUser getEventOwner() {
        return eventOwner;
    }

    public void setEventOwner(OUser eventOwner) {
        this.eventOwner = eventOwner;
    }

    public UDC getEventType() {
        return eventType;
    }

    public void setEventType(UDC eventType) {
        this.eventType = eventType;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }

    public OccupationElementScale getGradeScale() {
        return gradeScale;
    }

    public void setGradeScale(OccupationElementScale gradeScale) {
        this.gradeScale = gradeScale;
    }

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public JobReqAppEventNotification getNotification() {
        return notification;
    }

    public void setNotification(JobReqAppEventNotification notification) {
        this.notification = notification;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public JobRequisitionApplicant getRequisitionApplicant() {
        return requisitionApplicant;
    }

    public void setRequisitionApplicant(JobRequisitionApplicant requisitionApplicant) {
        this.requisitionApplicant = requisitionApplicant;
    }

    public Date getScheduledEndDate() {
        return scheduledEndDate;
    }

    public void setScheduledEndDate(Date scheduledEndDate) {
        this.scheduledEndDate = scheduledEndDate;
    }

    public Date getScheduledStartDate() {
        return scheduledStartDate;
    }

    public void setScheduledStartDate(Date scheduledStartDate) {
        this.scheduledStartDate = scheduledStartDate;
    }
    
}
