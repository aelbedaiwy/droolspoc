package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.report.OReport;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
// @Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@ChildEntity(fields = {"filterFieldValues"})
//@DiscriminatorValue(value = "TemplRep") Don't allow instantiation
public class OTemplRep extends OReport {

    // <editor-fold defaultstate="collapsed" desc="orepTemplDS">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ORepTemplDS orepTemplDS;

    /**
     * @return the orepTemplDS
     */
    public ORepTemplDS getOrepTemplDS() {
        return orepTemplDS;
    }

    /**
     * @param orepTemplDS the orepTemplDS to set
     */
    public void setOrepTemplDS(ORepTemplDS orepTemplDS) {
        this.orepTemplDS = orepTemplDS;
    }

    /**
     * @return the orepTemplDS
     */
    public String getOrepTemplDSDD() {
        return "OTemplRep_orepTemplDS";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oRepTempl">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    //@JoinColumn(nullable = false)
    //private ORepTempl orepTempl;
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC orepTempl;

    public UDC getOrepTempl() {
        return orepTempl;
    }

    public void setOrepTempl(UDC oRepTempl) {
        this.orepTempl = oRepTempl;
    }

    public String getOrepTemplDD() {
        return "OTemplRep_orepTempl";
    }
    /**
     * @return the oRepTempl
     */
    //public ORepTempl getOrepTempl() {
    //    return orepTempl;
    ///}
    /**
     * @param oRepTempl the oRepTempl to set
     */
    //public void setOrepTempl(ORepTempl oRepTempl) {
    //    this.orepTempl = oRepTempl;
    //}
    /**
     * @return the oRepTemplDD
     */
    // public String getOrepTemplDD() {
    //    return "OTemplRep_orepTempl";
    //}
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="countTitle">
    @Translatable(translationField = "countTitleTranslated")
    @Column(nullable = true)
    private String countTitle;

    /**
     * @return the countTitle
     */
    public String getCountTitle() {
        return countTitle;
    }

    /**
     * @param countTitle the countTitle to set
     */
    public void setCountTitle(String countTitle) {
        this.countTitle = countTitle;
    }
    @Transient
    @Translation(originalField = "countTitle")
    private String countTitleTranslated;

    /**
     * @return the countTitleDD
     */
    public String getCountTitleDD() {
        return "OTemplRep_countTitle";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="countPosition">
    @Column(nullable = true)
    private Integer countPosition = 2;

    /**
     * @return the countPosition
     */
    public Integer getCountPosition() {
        return countPosition;
    }

    /**
     * @param countPosition the countPosition to set
     */
    public void setCountPosition(Integer countPosition) {
        this.countPosition = countPosition;
    }

    /**
     * @return the countPosition
     */
    public String getCountPositionDD() {
        return "OTemplRep_countPosition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showDetail">
    @Column
    private boolean showDetail = true;

    /**
     * @return the showDetail
     */
    public boolean isShowDetail() {
        return showDetail;
    }

    /**
     * @param showDetail the showDetail to set
     */
    public void setShowDetail(boolean showDetail) {
        this.showDetail = showDetail;
    }

    /**
     * @return the showDetailDD
     */
    public String getShowDetailDD() {
        return "OTemplRep_showDetail";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showSummary">
    @Column
    private boolean showSummary;

    /**
     * @return the showSummary
     */
    public boolean isShowSummary() {
        return showSummary;
    }

    /**
     * @param showSummary the showSummary to set
     */
    public void setShowSummary(boolean showSummary) {
        this.showSummary = showSummary;
    }

    /**
     * @return the showSummaryDD
     */
    public String getShowSummaryDD() {
        return "OTemplRep_showSummary";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="orderByDescending">
    @Column
    private boolean orderByDescending;

    /**
     * @return the orderByDescending
     */
    public boolean isOrderByDescending() {
        return orderByDescending;
    }

    /**
     * @param orderByDescending the orderByDescending to set
     */
    public void setOrderByDescending(boolean orderByDescending) {
        this.orderByDescending = orderByDescending;
    }

    /**
     * @return the orderByDescendingDD
     */
    public String getOrderByDescendingDD() {
        return "OTemplRep_orderByDescending";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="displayFirstGroupByInNewPage">
    @Column
    private boolean displayFirstGroupByInNewPage;

    /**
     * @return the orderByDescending
     */
    public boolean isDisplayFirstGroupByInNewPage() {
        return displayFirstGroupByInNewPage;
    }

    /**
     * @param orderByDescending the orderByDescending to set
     */
    public void setDisplayFirstGroupByInNewPage(boolean displayFirstGroupByInNewPage) {
        this.displayFirstGroupByInNewPage = displayFirstGroupByInNewPage;
    }

    /**
     * @return the orderByDescendingDD
     */
    public String getDisplayFirstGroupByInNewPageDD() {
        return "OTemplRep_displayFirstGroupByInNewPage";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="displaySecondGroupByInNewPage">
    @Column
    private boolean displaySecondGroupByInNewPage;

    /**
     * @return the orderByDescending
     */
    public boolean isDisplaySecondGroupByInNewPage() {
        return displaySecondGroupByInNewPage;
    }

    /**
     * @param orderByDescending the orderByDescending to set
     */
    public void setDisplaySecondGroupByInNewPage(boolean displaySecondGroupByInNewPage) {
        this.displaySecondGroupByInNewPage = displaySecondGroupByInNewPage;
    }

    /**
     * @return the orderByDescendingDD
     */
    public String getDisplaySecondGroupByInNewPageDD() {
        return "OTemplRep_displaySecondGroupByInNewPage";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filterFieldValues">
    @OneToMany(mappedBy = "otemplRep")
    private List<OTemplRepFilterFieldValue> filterFieldValues;

    /**
     * @return the filterFieldValues
     */
    public List<OTemplRepFilterFieldValue> getFilterFieldValues() {
        return filterFieldValues;
    }

    /**
     * @param filterFieldValues the filterFieldValues to set
     */
    public void setFilterFieldValues(List<OTemplRepFilterFieldValue> filterFieldValues) {
        this.filterFieldValues = filterFieldValues;
    }

    /**
     * @return the filterFieldValuesDD
     */
    public String getFilterFieldValuesDD() {
        return "OTemplRep_filterFieldValues";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showTotal">
    @Column
    private boolean showTotal;

    /**
     * @return the showTotal
     */
    public boolean isShowTotal() {
        return showTotal;
    }

    /**
     * @param showTotal the showTotal to set
     */
    public void setShowTotal(boolean showTotal) {
        this.showTotal = showTotal;
    }

    /**
     * @return the showTotalDD
     */
    public String getShowTotalDD() {
        return "OTemplRep_showTotal";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showNegative">
    @Column
    private boolean showNegative;

    /**
     * @return the showNegative
     */
    public boolean isShowNegative() {
        return showNegative;
    }

    /**
     * @param showNegative the showNegative to set
     */
    public void setShowNegative(boolean showNegative) {
        this.showNegative = showNegative;
    }

    /**
     * @return the showNegativeDD
     */
    public String getShowNegativeDD() {
        return "OTemplRep_showNegative";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showPositive">
    @Column
    private boolean showPositive;

    /**
     * @return the showPositive
     */
    public boolean isShowPositive() {
        return showPositive;
    }

    /**
     * @param showPositive the showPositive to set
     */
    public void setShowPositive(boolean showPositive) {
        this.showPositive = showPositive;
    }

    /**
     * @return the showPositiveDD
     */
    public String getShowPositiveDD() {
        return "OTemplRep_showPositive";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showNet">
    @Column
    private boolean showNet;

    /**
     * @return the showNet
     */
    public boolean isShowNet() {
        return showNet;
    }

    /**
     * @param showNet the showNet to set
     */
    public void setShowNet(boolean showNet) {
        this.showNet = showNet;
    }

    /**
     * @return the showNetDD
     */
    public String getShowNetDD() {
        return "OTemplRep_showNet";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dataFontSize">
    @Column
    private int dataFontSize = 7;

    /**
     * @return the dataFontSize
     */
    public int getDataFontSize() {
        return dataFontSize;
    }

    /**
     * @param dataFontSize the dataFontSize to set
     */
    public void setDataFontSize(int dataFontSize) {
        this.dataFontSize = dataFontSize;
    }

    public String getDataFontSizeDD() {
        return "OTemplRep_dataFontSize";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="columnFontSize">
    @Column
    private int columnFontSize = 7;

    /**
     * @return the columnFontSize
     */
    public int getColumnFontSize() {
        return columnFontSize;
    }

    /**
     * @param columnFontSize the columnFontSize to set
     */
    public void setColumnFontSize(int columnFontSize) {
        this.columnFontSize = columnFontSize;
    }

    public String getColumnFontSizeDD() {
        return "OTemplRep_columnFontSize";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="useFullWidth">
    @Column
    private boolean useFullWidth = true;

    /**
     * @return the useFullWidth
     */
    public boolean isUseFullWidth() {
        return useFullWidth;
    }

    /**
     * @param useFullWidth the useFullWidth to set
     */
    public void setUseFullWidth(boolean useFullWidth) {
        this.useFullWidth = useFullWidth;
    }

    /**
     * @return the useFullWidthDD
     */
    public String getUseFullWidthDD() {
        return "OTemplRep_useFullWidth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filterByUser">
    private boolean filterByUser;

    /**
     * @return the filterByUser
     */
    public boolean isFilterByUser() {
        return filterByUser;
    }

    /**
     * @param filterByUser the filterByUser to set
     */
    public void setFilterByUser(boolean filterByUser) {
        this.filterByUser = filterByUser;
    }

    /**
     * @return the filterByUserDD
     */
    public String getFilterByUserDD() {
        return "OTemplRep_filterByUser";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="verticalColumnTitle">
    @Column
    private boolean verticalColumnTitle = true;

    /**
     * @return the verticalColumnTitle
     */
    public boolean isVerticalColumnTitle() {
        return verticalColumnTitle;
    }

    /**
     * @param verticalColumnTitle the useFullWidth to set
     */
    public void setVerticalColumnTitle(boolean verticalColumnTitle) {
        this.verticalColumnTitle = verticalColumnTitle;
    }

    /**
     * @return the useFullWidthDD
     */
    public String getVerticalColumnTitleDD() {
        return "OTemplRep_verticalColumnTitle";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showCount">
    @Column
    private boolean showCount = true;

    /**
     * @return the showCount
     */
    public boolean isShowCount() {
        return showCount;
    }

    /**
     * @param showCount the showCount to set
     */
    public void setShowCount(boolean showCount) {
        this.showCount = showCount;
    }

    /**
     * @return the showCountDD
     */
    public String getShowCountDD() {
        return "OTemplRep_showCount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filterByManager">
    private boolean filterByManager;

    /**
     * @return the filterByManager
     */
    public boolean isFilterByManager() {
        return filterByManager;
    }

    /**
     * @param filterByManager the filterByManager to set
     */
    public void setFilterByManager(boolean filterByManager) {
        this.filterByManager = filterByManager;
    }

    /**
     * @return the filterByManagerDD
     */
    public String getFilterByManagerDD() {
        return "OTemplRep_filterByManager";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="filterDateExp">
    private String filterDateExp;

    /**
     * @return the filterDateExp
     */
    public String getFilterDateExp() {
        return filterDateExp;
    }

    /**
     * @param filterDateExp the filterDateExp to set
     */
    public void setFilterDateExp(String filterDateExp) {
        this.filterDateExp = filterDateExp;
    }

    /**
     * @return the filterDateExpDD
     */
    public String getFilterDateExpDD() {
        return "OTemplRep_filterDateExp";
    }
    // </editor-fold>

    /**
     * @return the countTitleTranslated
     */
    public String getCountTitleTranslated() {
        return countTitleTranslated;
    }

    /**
     * @param countTitleTranslated the countTitleTranslated to set
     */
    public void setCountTitleTranslated(String countTitleTranslated) {
        this.countTitleTranslated = countTitleTranslated;
    }

    /**
     * @return the countTitleTranslated
     */
    public String getCountTitleTranslatedDD() {
        return "OTemplRep_countTitle";
    }
}
