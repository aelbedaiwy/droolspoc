package com.unitedofoq.otms.nursery;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class Nursery extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="servicePeriodNum">
    private float servicePeriodNum; // in months

    public float getServicePeriodNum() {
        return servicePeriodNum;
    }

    public void setServicePeriodNum(float servicePeriodNum) {
        this.servicePeriodNum = servicePeriodNum;
    }

    public String getServicePeriodNumDD() {
        return "Nursery_servicePeriodNum";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hourType">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC hourType;

    public UDC getHourType() {
        return hourType;
    }

    public void setHourType(UDC hourType) {
        this.hourType = hourType;
    }

    public String getHourTypeDD() {
        return "Nursery_hourType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="eligibilityNumTimes">
    private int eligibilityNumTimes;

    public int getEligibilityNumTimes() {
        return eligibilityNumTimes;
    }

    public void setEligibilityNumTimes(int eligibilityNumTimes) {
        this.eligibilityNumTimes = eligibilityNumTimes;
    }

    public String getEligibilityNumTimesDD() {
        return "Nursery_eligibilityNumTimes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="depEligibilityPeriod">
    private float depEligibilityPeriod;

    public float getDepEligibilityPeriod() {
        return depEligibilityPeriod;
    }

    public void setDepEligibilityPeriod(float depEligibilityPeriod) {
        this.depEligibilityPeriod = depEligibilityPeriod;
    }

    public String getDepEligibilityPeriodDD() {
        return "Nursery_depEligibilityPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hoursNumPerDay">
    private float hoursNumPerDay;

    public float getHoursNumPerDay() {
        return hoursNumPerDay;
    }

    public void setHoursNumPerDay(float hoursNumPerDay) {
        this.hoursNumPerDay = hoursNumPerDay;
    }

    public String getHoursNumPerDayDD() {
        return "Nursery_hoursNumPerDay";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "Nursery_legalEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "Nursery_legalEntityGroup";
    }
    //</editor-fold>
}
