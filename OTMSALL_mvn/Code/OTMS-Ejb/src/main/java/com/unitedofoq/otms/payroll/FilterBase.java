package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class FilterBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }
    public String getEmployeeDD() {
        return "FilterBase_employee";
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }
    public String getPositionDD() {
        return "FilterBase_position";
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="unit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }
    public String getUnitDD() {
        return "FilterBase_unit";
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job job;

    public Job getJob() {
        return job;
    }
    public String getJobDD() {
        return "FilterBase_job";
    }

    public void setJob(Job job) {
        this.job = job;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }
    public String getCostCenterDD() {
        return "FilterBase_costCenter";
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade payGrade;

    public PayGrade getPayGrade() {
        return payGrade;
    }
    public String getPayGradeDD() {
        return "FilterBase_payGrade";
    }

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }
    public String getLocationDD() {
        return "FilterBase_location";
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC hiringType;

    public UDC getHiringType() {
        return hiringType;
    }
    public String getHiringTypeDD() {
        return "FilterBase_hiringType";
    }

    public void setHiringType(UDC hiringType) {
        this.hiringType = hiringType;
    }
    // </editor-fold >
}
