
package com.unitedofoq.otms.appraisal.template;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class AppraisalElementTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="procedures">
    @Column
    private String procedures;

    public void setProcedures(String procedures) {
        this.procedures = procedures;
    }

    public String getProcedures() {
        return procedures;
    }
    // </editor-fold>

}
