package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.math.BigDecimal;
import javax.persistence.ManyToOne;
import com.unitedofoq.otms.payroll.salaryelement.Income;


        // </editor-fold>
/**
 * The persistent class for the tax_income database table.
 * 
 */
@Entity
@ParentEntity(fields={"tax"})
public class TaxIncome extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="tax">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Tax tax;
    public Tax getTax() {
        return tax;
    }
    public void setTax(Tax tax) {
        this.tax = tax;
    }
    public String getTaxDD() {
        return "TaxIncome_tax";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="accumulationCode">
    @Column //FIXME: To be UDC
    private String accumulationCode;
    public String getAccumulationCode() {
        return accumulationCode;
    }
    public void setAccumulationCode(String accumulationCode) {
        this.accumulationCode = accumulationCode;
    }
    public String getAccumulationCodeDD() {
        return "TaxIncome_accumulationCode";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="incomePercentage">
    @Column(precision=10, scale=3,nullable=false)
    private BigDecimal incomePercentage;
    public BigDecimal getIncomePercentage() {
        return incomePercentage;
    }
    public void setIncomePercentage(BigDecimal incomePercentage) {
        this.incomePercentage = incomePercentage;
    }
    public String getIncomePercentageDD() {
        return "TaxIncome_incomePercentage";
    }
    @Transient
    private BigDecimal incomePercentageMask;
    public BigDecimal getIncomePercentageMask() {
        incomePercentageMask = incomePercentage;
        return incomePercentageMask;
    }
    public void setIncomePercentageMask(BigDecimal incomePercentageMask) {
        updateDecimalValue("incomePercentage",incomePercentageMask);
    }
    public String getIncomePercentageMaskDD() {
        return "TaxIncome_incomePercentageMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxPercentFromSalary">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal maxPercentFromSalary;
    public BigDecimal getMaxPercentFromSalary() {
        return maxPercentFromSalary;
    }
    public void setMaxPercentFromSalary(BigDecimal maxPercentFromSalary) {
        this.maxPercentFromSalary = maxPercentFromSalary;
    }
    public String getMaxPercentFromSalaryDD() {
        return "TaxIncome_maxPercentFromSalary";
    }
    @Transient
    private BigDecimal maxPercentFromSalaryMask;
    public BigDecimal getMaxPercentFromSalaryMask() {
        maxPercentFromSalaryMask = maxPercentFromSalary ;
        return maxPercentFromSalaryMask;
    }
    public void setMaxPercentFromSalaryMask(BigDecimal maxPercentFromSalaryMask) {
        updateDecimalValue("maxPercentFromSalary",maxPercentFromSalaryMask);
    }
    public String getMaxPercentFromSalaryMaskDD() {
        return "TaxIncome_maxPercentFromSalaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="secondAccumlation">
	@Column //FIXME: To be UDC?
	private String secondAccumlation;
    public String getSecondAccumlation() {
        return secondAccumlation;
    }
    public void setSecondAccumlation(String secondAccumlation) {
        this.secondAccumlation = secondAccumlation;
    }
    public String getSecondAccumlationDD() {
        return "TaxIncome_secondAccumlation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision=25, scale=13)
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "TaxIncome_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue ;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue",maximumValueMask);
    }

    public String getMaximumValueMaskDD() {
        return "TaxIncome_maximumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="income">
	@JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Income income;
    public Income getIncome() {
        return income;
    }
    public void setIncome(Income income) {
        this.income = income;
    }
    public String getIncomeDD() {
        return "TaxIncome_income";
    }
    // </editor-fold>
}