/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author lap2
 */
@Entity
@ParentEntity(fields={"calendar"})
@ChildEntity(fields={"dateExcs"})
public class DateCalendarException extends CalendarExceptionBase {
    //<editor-fold defaultstate="collapsed" desc="calendar">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private NormalWorkingCalendar calendar;

    public NormalWorkingCalendar getCalendar() {
        return calendar;
    }

    public void setCalendar(NormalWorkingCalendar calendar) {
        this.calendar = calendar;
    }
    
    public String getCalendarDD() {
        return "DateCalendarException_calendar";
    }

    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="dayOff1">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dayOff1;

    public void setDayOff1(UDC dayOff1) {
        this.dayOff1 = dayOff1;
    }

    public UDC getDayOff1() {
        return dayOff1;
    }

    public String getDayOff1DD() {
        return "DateCalendarException_dayOff1";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOff2">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dayOff2;

    public void setDayOff2(UDC dayOff2) {
        this.dayOff2 = dayOff2;
    }

    public UDC getDayOff2() {
        return dayOff2;
    }

    public String getDayOff2DD() {
        return "DateCalendarException_dayOff2";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dateExcs">
    @OneToMany(mappedBy = "dateException")
    private List<OTDateExc> dateExcs;

    public List<OTDateExc> getDateExcs() {
        return dateExcs;
    }

    public void setDateExcs(List<OTDateExc> dateExcs) {
        this.dateExcs = dateExcs;
    }

    public String getDateExcsDD() {
        return "DateCalendarException_dateExcs";
    }
    //</editor-fold>
}
