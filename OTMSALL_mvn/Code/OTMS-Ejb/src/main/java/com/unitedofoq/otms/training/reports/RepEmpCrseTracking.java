
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.util.Date;
import javax.persistence.*;

@Entity
public class RepEmpCrseTracking extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmpCrseTracking_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }

    public String getCourseClassDD() {
        return "RepEmpCrseTracking_courseClass";
    }
    
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;

    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }

    public String getCourseClassTranslatedDD() {
        return "RepEmpCrseTracking_courseClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="course">
    @Column
    @Translatable(translationField = "courseTranslated")
    private String course;

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourse() {
        return course;
    }

    public String getCourseDD() {
        return "RepEmpCrseTracking_course";
    }
    
    @Transient
    @Translation(originalField = "course")
    private String courseTranslated;

    public String getCourseTranslated() {
        return courseTranslated;
    }

    public void setCourseTranslated(String courseTranslated) {
        this.courseTranslated = courseTranslated;
    }

    public String getCourseTranslatedDD() {
        return "RepEmpCrseTracking_courses";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @Column
    @Translatable(translationField = "statusTranslated")
    private String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "RepEmpCrseTracking_status";
    }
    
    @Transient
    @Translation(originalField = "status")
    private String statusTranslated;

    public String getStatusTranslated() {
        return statusTranslated;
    }

    public void setStatusTranslated(String statusTranslated) {
        this.statusTranslated = statusTranslated;
    }

    public String getStatusTranslatedDD() {
        return "RepEmpCrseTracking_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="doneBy">
    @Column
    @Translatable(translationField = "doneByTranslated")
    private String doneBy;

    public void setDoneBy(String doneBy) {
        this.doneBy = doneBy;
    }

    public String getDoneBy() {
        return doneBy;
    }

    public String getDoneByDD() {
        return "RepEmpCrseTracking_doneBy";
    }
    
    @Transient
    @Translation(originalField = "doneBy")
    private String doneByTranslated;

    public String getDoneByTranslated() {
        return doneByTranslated;
    }

    public void setDoneByTranslated(String doneByTranslated) {
        this.doneByTranslated = doneByTranslated;
    }

    public String getDoneByTranslatedDD() {
        return "RepEmpCrseTracking_doneByTranslated";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date statusDate;

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public String getStatusDateDD() {
        return "RepEmpCrseTracking_statusDate";
    }
    // </editor-fold>

}
