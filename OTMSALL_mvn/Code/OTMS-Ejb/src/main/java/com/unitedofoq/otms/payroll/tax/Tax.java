package com.unitedofoq.otms.payroll.tax;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.*;

/**
 * 
 */
@Entity
@Table(name="otax")
@ParentEntity(fields="company")
@ChildEntity(fields={"breaks", "incomes", "deductions","taxGroupCalculations","exemptions"})
public class Tax extends BusinessObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "Tax_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="annualyExemptedValue">
    @Column(precision=25, scale=13, nullable=false)
    private BigDecimal annualyExemptedValue;
    public BigDecimal getAnnualyExemptedValue() {
        return annualyExemptedValue;
    }
    public void setAnnualyExemptedValue(BigDecimal annualyExemptedValue) {
        this.annualyExemptedValue = annualyExemptedValue;
    }
    public String getAnnualyExemptedValueDD() {
        return "Tax_annualyExemptedValue";
    }
    @Transient
    private BigDecimal annualyExemptedValueMask;
    public BigDecimal getAnnualyExemptedValueMask() {
        annualyExemptedValueMask = annualyExemptedValue ;
        return annualyExemptedValueMask;
    }
    public void setAnnualyExemptedValueMask(BigDecimal annualyExemptedValueMask) {
        updateDecimalValue("annualyExemptedValue",annualyExemptedValueMask);
    }
    public String getAnnualyExemptedValueMaskDD() {
        return "Tax_annualyExemptedValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="correctionDeduction">
	@JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Deduction correctionDeduction;
    public Deduction getCorrectionDeduction() {
        return correctionDeduction;
    }
    public void setCorrectionDeduction(Deduction correctionDeduction) {
        this.correctionDeduction = correctionDeduction;
    }
    public String getCorrectionDeductionDD() {
        return "Tax_correctionDeduction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deduction">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Deduction deduction;
    public Deduction getDeduction() {
        return deduction;
    }
    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
    public String getDeductionDD() {
        return "Tax_deduction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "Tax_description";
    }
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exemptedPercent">
    @Column(precision=25, scale=13)
    private BigDecimal exemptedPercent;
    public BigDecimal getExemptedPercent() {
        return exemptedPercent;
    }
    public void setExemptedPercent(BigDecimal exemptedPercent) {
        this.exemptedPercent = exemptedPercent;
    }
    public String getExemptedPercentDD() {
        return "Tax_exemptedPercent";
    }
    @Transient
    private BigDecimal exemptedPercentMask;
    public BigDecimal getExemptedPercentMask() {
        exemptedPercentMask = exemptedPercent ;
        return exemptedPercentMask;
    }
    public void setExemptedPercentMask(BigDecimal exemptedPercentMask) {
        updateDecimalValue("exemptedPercent",exemptedPercentMask);
    }
    public String getExemptedPercentMaskDD() {
        return "Tax_exemptedPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="income">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Income income;
    public Income getIncome() {
        return income;
    }
    public void setIncome(Income income) {
        this.income = income;
    }
    public String getIncomeDD() {
        return "Tax_income";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mandSttMonth">
    @Column
	private int mandSttMonth;
    public int getMandSttMonth() {
        return mandSttMonth;
    }
    public void setMandSttMonth(int mandSttMonth) {
        this.mandSttMonth = mandSttMonth;
    }
    public String getMandSttMonthDD() {
        return "Tax_mandSttMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maritalExempted">
	@Column(length=1)
	private String maritalExempted;
    public String getMaritalExempted() {
        return maritalExempted;
    }
    public void setMaritalExempted(String maritalExempted) {
        this.maritalExempted = maritalExempted;
    }

    public String getMaritalExemptedDD() {
        return "Tax_maritalExempted";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentPerMonth">
    @Column(precision=25, scale=13, nullable=false)
    private BigDecimal paymentPerMonth;
    public BigDecimal getPaymentPerMonth() {
        return paymentPerMonth;
    }
    public void setPaymentPerMonth(BigDecimal paymentPerMonth) {
        this.paymentPerMonth = paymentPerMonth;
    }
    public String getPaymentPerMonthDD() {
        return "Tax_paymentPerMonth";
    }
    @Transient
    private BigDecimal paymentPerMonthMask;
    public BigDecimal getPaymentPerMonthMask() {
        paymentPerMonthMask = paymentPerMonth;
        return paymentPerMonthMask;
    }
    public void setPaymentPerMonthMask(BigDecimal paymentPerMonthMask) {
        updateDecimalValue("paymentPerMonth",paymentPerMonthMask);
    }
    public String getPaymentPerMonthMaskDD() {
        return "Tax_paymentPerMonthMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="periodsCountPerYear">
	@Column(nullable=false)
	private int periodsCountPerYear;
    public int getPeriodsCountPerYear() {
        return periodsCountPerYear;
    }
    public void setPeriodsCountPerYear(int periodsCountPerYear) {
        this.periodsCountPerYear = periodsCountPerYear;
    }
    public String getPeriodsCountPerYearDD() {
        return "Tax_periodsCountPerYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="settleMonth">
	@Column(nullable=false)
	private int settleMonth;
    public int getSettleMonth() {
        return settleMonth;
    }
    public void setSettleMonth(int sttleMonth) {
        this.settleMonth = sttleMonth;
    }
    public String getSettleMonthDD() {
        return "Tax_settleMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="settleStartMnth">
	@Column(nullable=false)
	private int settleStartMnth;
    public int getSettleStartMnth() {
        return settleStartMnth;
    }
    public void setSettleStartMnth(int settleStartMnth) {
        this.settleStartMnth = settleStartMnth;
    }
    public String getSettleStartMnthDD() {
        return "Tax_settleStartMnth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
	@Column
	private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "Tax_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="incomes">
    @OneToMany(mappedBy="tax")
    private List<TaxIncome> incomes;
    public List<TaxIncome> getIncomes() {
        return incomes;
    }
    public void setIncomes(List<TaxIncome> incomes) {
        this.incomes = incomes;
    }
    public String getIncomesDD() {
        return "Tax_incomes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="breaks">
    @OneToMany(mappedBy="tax")
    private List<TaxBreak> breaks;
    public List<TaxBreak> getBreaks() {
        return breaks;
    }
    public void setBreaks(List<TaxBreak> breaks) {
        this.breaks = breaks;
    }
    public String getBreaksDD() {
        return "Tax_breaks";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deductions">
    @OneToMany(mappedBy="tax")
    private List<TaxDeduction> deductions;
    public List<TaxDeduction> getDeductions() {
        return deductions;
    }
    public void setDeductions(List<TaxDeduction> deductions) {
        this.deductions = deductions;
    }
    public String getDeductionsDD() {
        return "Tax_deductions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxGroupCalculations">
    @OneToMany(mappedBy = "tax")
    private List<TaxGroupCalculation> taxGroupCalculations;

    public List<TaxGroupCalculation> getTaxGroupCalculations() {
        return taxGroupCalculations;
    }

    public void setTaxGroupCalculations(List<TaxGroupCalculation> taxGroupCalculations) {
        this.taxGroupCalculations = taxGroupCalculations;
    }

    public String getTaxGroupCalculationsDD() {
        return "Tax_taxGroupCalculations";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exemptions">
    @OneToMany(mappedBy = "tax")
    private List<MaritalTaxExemption> exemptions;

    public List<MaritalTaxExemption> getExemptions() {
        return exemptions;
    }

    public void setExemptions(List<MaritalTaxExemption> exemptions) {
        this.exemptions = exemptions;
    }
    
    public String getExemptionsDD() {
        return "Tax_exemptions";
    }
    // </editor-fold>
}
