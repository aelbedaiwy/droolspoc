
package com.unitedofoq.otms.recruitment.jobrequisition;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccupationElementRatingBase;
import com.unitedofoq.otms.foundation.occupation.WorkActivity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobRequisition"})
public class JobReqWorkActivity extends OccupationElementRatingBase {
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="workActivity_DBID", nullable=false)
    private WorkActivity workActivity;
    public String getWorkActivityDD(){
        return "JobReqWorkActivity_workActivity";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobRequisition_DBID", nullable=false)
    private JobRequisition jobRequisition;

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public WorkActivity getWorkActivity() {
        return workActivity;
    }

    public void setWorkActivity(WorkActivity workActivity) {
        this.workActivity = workActivity;
    }
    
}