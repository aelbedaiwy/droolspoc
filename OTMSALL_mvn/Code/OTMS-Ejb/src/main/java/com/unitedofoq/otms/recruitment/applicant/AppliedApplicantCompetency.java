/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="appliedApplicant")
public class AppliedApplicantCompetency extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="appliedApplicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private AppliedApplicant appliedApplicant;

    public AppliedApplicant getAppliedApplicant() {
        return appliedApplicant;
    }

    public void setAppliedApplicant(AppliedApplicant appliedApplicant) {
        this.appliedApplicant = appliedApplicant;
    }
    
    public String getAppliedApplicantDD(){
        return "AppliedApplicantCompetency_appliedApplicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    
    public String getCompetenciesDD(){
        return "AppliedApplicantCompetency_competency";
    }
    // </editor-fold>
}
