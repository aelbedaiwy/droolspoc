/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.training.TaskBase;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"position"})
public class PositionTask extends TaskBase {
    // <editor-fold defaultstate="collapsed" desc="position">
	@JoinColumn(nullable=false)
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    public String getPositionDD() {
        return "PositionTask_position";
    }
    // </editor-fold>
}
