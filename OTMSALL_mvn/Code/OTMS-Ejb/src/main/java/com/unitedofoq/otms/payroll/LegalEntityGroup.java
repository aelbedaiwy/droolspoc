package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@ChildEntity(fields = {"members"})
public class LegalEntityGroup extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "LegalEntityGroup_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "LegalEntityGroup_description";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="members">
    @OneToMany(mappedBy = "legalEntityGroup")
    private List<LegalEntityGroupMember> members;

    public List<LegalEntityGroupMember> getMembers() {
        return members;
    }

    public void setMembers(List<LegalEntityGroupMember> members) {
        this.members = members;
    }

    public String getMembersDD() {
        return "LegalEntityGroup_members";
    }
    //</editor-fold>
}
