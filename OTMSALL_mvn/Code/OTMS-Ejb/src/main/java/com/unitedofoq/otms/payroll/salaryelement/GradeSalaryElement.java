/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.salaryelement;

import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields = "payGrade")
public class GradeSalaryElement extends ObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private PayGrade payGrade;

    public PayGrade getPayGrade() {
        return payGrade;
    }

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public String getPayGradeDD() {
        return "GradeSalaryElement_payGrade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "GradeSalaryElement_salaryElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minSalValue">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal minSalValue = BigDecimal.ZERO;

    public BigDecimal getMinSalValue() {
        return minSalValue;
    }

    public void setMinSalValue(BigDecimal minSalValue) {
        this.minSalValue = minSalValue;
    }

    public String getMinSalValueDD() {
        return "GradeSalaryElement_minSalValue";
    }

    @Transient
    private BigDecimal minSalValueMask = BigDecimal.ZERO;

    public BigDecimal getMinSalValueMask() {
        minSalValueMask = minSalValue;
        return minSalValueMask;
    }

    public void setMinSalValueMask(BigDecimal minSalValue) {
        updateDecimalValue("minSalValue", minSalValue);
    }

    public String getMinSalValueMaskDD() {
        return "GradeSalaryElement_minSalValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="midSalValue">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal midSalValue = BigDecimal.ZERO;

    public BigDecimal getMidSalValue() {
        return midSalValue;
    }

    public void setMidSalValue(BigDecimal midSalValue) {
        this.midSalValue = midSalValue;
    }

    public String getMidSalValueDD() {
        return "GradeSalaryElement_midSalValue";
    }

    @Transient
    private BigDecimal midSalValueMask = BigDecimal.ZERO;

    public BigDecimal getMidSalValueMask() {
        midSalValueMask = midSalValue;
        return midSalValueMask;
    }

    public void setMidSalValueMask(BigDecimal midSalValueMask) {
        updateDecimalValue("midSalValue", midSalValueMask);
    }

    public String getMidSalValueMaskDD() {
        return "GradeSalaryElement_midSalValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxSalValue">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal maxSalValue = BigDecimal.ZERO;

    public BigDecimal getMaxSalValue() {
        return maxSalValue;
    }

    public void setMaxSalValue(BigDecimal maxSalValue) {
        this.maxSalValue = maxSalValue;
    }

    public String getMaxSalValueDD() {
        return "GradeSalaryElement_maxSalValue";
    }

    @Transient
    private BigDecimal maxSalValueMask = BigDecimal.ZERO;

    public BigDecimal getMaxSalValueMask() {
        maxSalValueMask = maxSalValue;
        return maxSalValueMask;
    }

    public void setMaxSalValueMask(BigDecimal maxSalValueMask) {
        updateDecimalValue("maxSalValue", maxSalValueMask);
    }

    public String getMaxSalValueMaskDD() {
        return "GradeSalaryElement_maxSalValueMask";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="midSalValue1">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal midSalValue1 = BigDecimal.ZERO;

    public BigDecimal getMidSalValue1() {
        return midSalValue1;
    }

    public void setMidSalValue1(BigDecimal midSalValue1) {
        this.midSalValue1 = midSalValue1;
    }

    public String getMidSalValue1DD() {
        return "GradeSalaryElement_midSalValue1";
    }

    @Transient
    private BigDecimal midSalValue1Mask = BigDecimal.ZERO;

    public BigDecimal getMidSalValue1Mask() {
        midSalValue1Mask = midSalValue1;
        return midSalValue1Mask;
    }

    public void setMidSalValue1Mask(BigDecimal midSalValue1Mask) {
        updateDecimalValue("midSalValue1", midSalValue1Mask);
    }

    public String getMidSalValue1MaskDD() {
        return "GradeSalaryElement_midSalValue1Mask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="midSalValue2">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal midSalValue2 = BigDecimal.ZERO;

    public BigDecimal getMidSalValue2() {
        return midSalValue2;
    }

    public void setMidSalValue2(BigDecimal midSalValue2) {
        this.midSalValue2 = midSalValue2;
    }

    public String getMidSalValue2DD() {
        return "GradeSalaryElement_midSalValue2";
    }

    @Transient
    private BigDecimal midSalValue2Mask = BigDecimal.ZERO;

    public BigDecimal getMidSalValue2Mask() {
        midSalValue2Mask = midSalValue2;
        return midSalValue2Mask;
    }

    public void setMidSalValue2Mask(BigDecimal midSalValue2Mask) {
        updateDecimalValue("midSalValue2", midSalValue2Mask);
    }

    public String getMidSalValue2MaskDD() {
        return "GradeSalaryElement_midSalValue2Mask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="midSalValue3">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal midSalValue3 = BigDecimal.ZERO;

    public BigDecimal getMidSalValue3() {
        return midSalValue3;
    }

    public void setMidSalValue3(BigDecimal midSalValue3) {
        this.midSalValue3 = midSalValue3;
    }

    public String getMidSalValue3DD() {
        return "GradeSalaryElement_midSalValue3";
    }

    @Transient
    private BigDecimal midSalValue3Mask = BigDecimal.ZERO;

    public BigDecimal getMidSalValue3Mask() {
        midSalValue3Mask = midSalValue3;
        return midSalValue3Mask;
    }

    public void setMidSalValue3Mask(BigDecimal midSalValue3Mask) {
        updateDecimalValue("midSalValue3", midSalValue3Mask);
    }

    public String getMidSalValue3MaskDD() {
        return "GradeSalaryElement_midSalValue3Mask";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "GradeSalaryElement_currency";
    }
    // </editor-fold>
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "GradeSalaryElement_m2mCheck";
    }

    @PostLoad
    public void postLoad() {
        setM2mCheck(true);
    }
}
