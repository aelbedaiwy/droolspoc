package com.unitedofoq.otms.recruitment.interview;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class Interview extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="name">
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getNameDD() {
        return "Interview_name";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "Interview_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }
    
    public String getTypeDD() {
        return "Interview_type";
    }
    // </editor-fold>
}
