
package com.unitedofoq.otms.personnel;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"employee"})
public class EmployeeRequest extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getDateDD() {
        return "EmployeeRequest_date";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "EmployeeRequest_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="title">
    @Column
    private String title;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleDD() {
        return "EmployeeRequest_title";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC requestType;

    public void setRequestType(UDC requestType) {
        this.requestType = requestType;
    }

    public UDC getRequestType() {
        return requestType;
    }

    public String getRequestTypeDD() {
        return "EmployeeRequest_requestType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "EmployeeRequest_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date statusDate;

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public String getStatusDateDD() {
        return "EmployeeRequest_statusDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeRequest_employee";
    }
    // </editor-fold>

}
