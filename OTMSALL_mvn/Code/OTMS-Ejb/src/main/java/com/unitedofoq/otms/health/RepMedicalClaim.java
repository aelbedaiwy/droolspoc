/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;


//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dsDid">
/**
 *
 * @author mostafa
 */

@Entity
@ReadOnly
@Table(name="repmedicalclaim")

public class RepMedicalClaim extends RepEmployeeBase  {
    //<editor-fold defaultstate="collapsed" desc="dsDid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="transactionNumber">
@Column
private long transactionNumber;

    public long getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(long transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
    public String getTransactionNumberDD() {
        return "RepMedicalClaim_transactionNumber";
    }


//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependenceName">

@Transient
    @Translation(originalField = "dependenceName")
    private String dependenceNameTranslated;

    public String getDependenceNameTranslated() {
        return dependenceNameTranslated;
    }

    public void setDependenceNameTranslated(String dependenceNameTranslated) {
        this.dependenceNameTranslated = dependenceNameTranslated;
    }
    public String getDependenceNameTranslatedDD() {
        return "RepMedicalClaim_dependenceNameTranslated";
    }

    

@Translatable(translationField = "dependenceNameTranslated")
private String dependenceName;

    public String getDependenceName() {
        return dependenceName;
    }

    public void setDependenceName(String dependenceName) {
        this.dependenceName = dependenceName;
    }
    
    public String getDependenceNameDD() {
        return "RepMedicalClaim_dependenceName";
    }



//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantMedicalCategoryDescription">
    @Transient
    @Translation(originalField = "dependantMedicalCategoryDescription")
    private String dependantMedicalCategoryDescriptionTranslated;

    public String getDependantMedicalCategoryDescriptionTranslated() {
        return dependantMedicalCategoryDescriptionTranslated;
    }

    public void setDependantMedicalCategoryDescriptionTranslated(String dependantMedicalCategoryDescriptionTranslated) {
        this.dependantMedicalCategoryDescriptionTranslated = dependantMedicalCategoryDescriptionTranslated;
    }

    public String getDependantMedicalCategoryDescriptionTranslatedDD() {
        return "RepMedicalClaim_dependantMedicalCategoryDescriptionTranslated";
    }
    
@Translatable(translationField = "dependantMedicalCategoryDescriptionTranslated")
@Column(name= "depmedcatdesc")
private String dependantMedicalCategoryDescription;

    public String getDependantMedicalCategoryDescription() {
        return dependantMedicalCategoryDescription;
    }

    public void setDependantMedicalCategoryDescription(String dependantMedicalCategoryDescription) {
        this.dependantMedicalCategoryDescription = dependantMedicalCategoryDescription;
    }
    
    public String getDependantMedicalCategoryDescriptionDD() {
        return "RepMedicalClaim_dependantMedicalCategoryDescription";
    }





//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantMedicalInsured">
@Column

private String dependantMedicalInsured;

    public String getDependantMedicalInsured() {
        return dependantMedicalInsured;
    }

    public void setDependantMedicalInsured(String dependantMedicalInsured) {
        this.dependantMedicalInsured = dependantMedicalInsured;
    }
    public String getDependantMedicalInsuredDD() {
        return "RepMedicalClaim_dependantMedicalInsured";
    }


//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantMedicalAmount">
@Column

private BigDecimal dependantMedicalAmount;

    public BigDecimal getDependantMedicalAmount() {
        return dependantMedicalAmount;
    }

    public void setDependantMedicalAmount(BigDecimal dependantMedicalAmount) {
        this.dependantMedicalAmount = dependantMedicalAmount;
    }
    public String getDependantMedicalAmountDD() {
        return "RepMedicalClaim_dependantMedicalAmount";
    }


//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantMedicalGlassesDate">
@Column
@Temporal(TemporalType.TIMESTAMP)
private Date dependantMedicalGlassesDate;

    public Date getDependantMedicalGlassesDate() {
        return dependantMedicalGlassesDate;
    }

    public void setDependantMedicalGlassesDate(Date dependantMedicalGlassesDate) {
        this.dependantMedicalGlassesDate = dependantMedicalGlassesDate;
    }
    public String getDependantMedicalGlassesDateDD() {
        return "RepMedicalClaim_dependantMedicalGlassesDate";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantConsumedMedicalAmount">
@Column

private BigDecimal dependantConsumedMedicalAmount;

    public BigDecimal getDependantConsumedMedicalAmount() {
        return dependantConsumedMedicalAmount;
    }

    public void setDependantConsumedMedicalAmount(BigDecimal dependantConsumedMedicalAmount) {
        this.dependantConsumedMedicalAmount = dependantConsumedMedicalAmount;
    }
    public String getDependantConsumedMedicalAmountDD() {
        return "RepMedicalClaim_dependantConsumedMedicalAmount";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantMedicalGlassesStatus">
    @Transient
    @Translation(originalField = "dependantMedicalGlassesStatus")
    private String dependantMedicalGlassesStatusTranslated;

    public String getDependantMedicalGlassesStatusTranslated() {
        return dependantMedicalGlassesStatusTranslated;
    }

    public void setDependantMedicalGlassesStatusTranslated(String dependantMedicalGlassesStatusTranslated) {
        this.dependantMedicalGlassesStatusTranslated = dependantMedicalGlassesStatusTranslated;
    }
      public String getDependantMedicalGlassesStatusTranslatedDD() {
        return "RepMedicalClaim_dependantMedicalGlassesStatusTranslated";
    }  
    @Translatable(translationField = "dependantMedicalGlassesStatusTranslated")
private String dependantMedicalGlassesStatus;

    public String getDependantMedicalGlassesStatus() {
        return dependantMedicalGlassesStatus;
    }

    public void setDependantMedicalGlassesStatus(String dependantMedicalGlassesStatus) {
        this.dependantMedicalGlassesStatus = dependantMedicalGlassesStatus;
    }
    public String getDependantMedicalGlassesStatusDD() {
        return "RepMedicalClaim_dependantMedicalGlassesStatus";
    }



//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="beneficiary">
    @Transient
    @Translation(originalField = "beneficiary")
    private String beneficiaryTranslated;

    public String getBeneficiaryTranslated() {
        return beneficiaryTranslated;
    }

    public void setBeneficiaryTranslated(String beneficiaryTranslated) {
        this.beneficiaryTranslated = beneficiaryTranslated;
    }
        public String getBeneficiaryTranslatedDD() {
        return "RepMedicalClaim_beneficiaryTranslated";
    }
    @Translatable(translationField = "beneficiaryTranslated")

private String beneficiary;

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }
    public String getBeneficiaryDD() {
        return "RepMedicalClaim_beneficiary";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalGroup">
    @Transient
    @Translation(originalField = "medicalGroup")
    private String medicalGroupTranslated;

    public String getMedicalGroupTranslated() {
        return medicalGroupTranslated;
    }

    public void setMedicalGroupTranslated(String medicalGroupTranslated) {
        this.medicalGroupTranslated = medicalGroupTranslated;
    }
      public String getMedicalGroupTranslatedDD() {
        return "RepMedicalClaim_medicalGroupTranslated";
    }  
    @Translatable(translationField = "medicalGroupTranslated")

private String medicalGroup;

    public String getMedicalGroup() {
        return medicalGroup;
    }

    public void setMedicalGroup(String medicalGroup) {
        this.medicalGroup = medicalGroup;
    }
    public String getMedicalGroupDD() {
        return "RepMedicalClaim_medicalGroup";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cash">
@Column

private String cash;

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }
    public String getCashDD() {
        return "RepMedicalClaim_cash";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalStatusCash">
@Column

private String medicalStatusCash;

    public String getMedicalStatusCash() {
        return medicalStatusCash;
    }

    public void setMedicalStatusCash(String medicalStatusCash) {
        this.medicalStatusCash = medicalStatusCash;
    }
    public String getMedicalStatusCashDD() {
        return "RepMedicalClaim_medicalStatusCash";
    }

//</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="medicalClaimAmount">
@Column

private String medicalClaimAmount;

    public String getMedicalClaimAmount() {
        return medicalClaimAmount;
    }

    public void setMedicalClaimAmount(String medicalClaimAmount) {
        this.medicalClaimAmount = medicalClaimAmount;
    }
    public String getMedicalClaimAmountDD() {
        return "RepMedicalClaim_medicalClaimAmount";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalClaimTransactionDate">
@Column
@Temporal(TemporalType.TIMESTAMP)
private Date medicalClaimTransactionDate;

    public Date getMedicalClaimTransactionDate() {
        return medicalClaimTransactionDate;
    }

    public void setMedicalClaimTransactionDate(Date medicalClaimTransactionDate) {
        this.medicalClaimTransactionDate = medicalClaimTransactionDate;
    }
    public String getMedicalClaimTransactionDateDD() {
        return "RepMedicalClaim_medicalClaimTransactionDate";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalStatus">
    @Transient
    @Translation(originalField = "medicalStatus")
    private String medicalStatusTranslated;

    public String getMedicalStatusTranslated() {
        return medicalStatusTranslated;
    }

    public void setMedicalStatusTranslated(String medicalStatusTranslated) {
        this.medicalStatusTranslated = medicalStatusTranslated;
    }
     public String getMedicalStatusTranslatedDD() {
        return "RepMedicalClaim_medicalStatusTranslated";
    }   
    @Translatable(translationField = "medicalStatusTranslated")

private String medicalStatus;

    public String getMedicalStatus() {
        return medicalStatus;
    }

    public void setMedicalStatus(String medicalStatus) {
        this.medicalStatus = medicalStatus;
    }
    public String getMedicalStatusDD() {
        return "RepMedicalClaim_medicalStatus";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalTreatmentFinancialOperations">
    @Transient
    @Translation(originalField = "medicalTreatmentFinancialOperations")
    private String medicalTreatmentFinancialOperationsTranslated;

    public String getMedicalTreatmentFinancialOperationsTranslated() {
        return medicalTreatmentFinancialOperationsTranslated;
    }

    public void setMedicalTreatmentFinancialOperationsTranslated(String medicalTreatmentFinancialOperationsTranslated) {
        this.medicalTreatmentFinancialOperationsTranslated = medicalTreatmentFinancialOperationsTranslated;
    }
    public String getMedicalTreatmentFinancialOperationsTranslatedDD() {
        return "RepMedicalClaim_medicalTreatmentFinancialOperationsTranslated";
    }    
    @Translatable(translationField = "medicalTreatmentFinancialOperationsTranslated")
    @Column(name= "medtrefinope")
    
private String medicalTreatmentFinancialOperations;

    public String getMedicalTreatmentFinancialOperations() {
        return medicalTreatmentFinancialOperations;
    }

    public void setMedicalTreatmentFinancialOperations(String medicalTreatmentFinancialOperations) {
        this.medicalTreatmentFinancialOperations = medicalTreatmentFinancialOperations;
    }
    public String getMedicalTreatmentFinancialOperationsDD() {
        return "RepMedicalClaim_medicalTreatmentFinancialOperations";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalTreatmentConditions">
@Column

private String medicalTreatmentConditions;

    public String getMedicalTreatmentConditions() {
        return medicalTreatmentConditions;
    }

    public void setMedicalTreatmentConditions(String medicalTreatmentConditions) {
        this.medicalTreatmentConditions = medicalTreatmentConditions;
    }
    public String getMedicalTreatmentConditionsDD() {
        return "RepMedicalClaim_medicalTreatmentConditions";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeMedicalInsured">
@Column

private String employeeMedicalInsured;

    public String getEmployeeMedicalInsured() {
        return employeeMedicalInsured;
    }

    public void setEmployeeMedicalInsured(String employeeMedicalInsured) {
        this.employeeMedicalInsured = employeeMedicalInsured;
    }
    public String getEmployeeMedicalInsuredDD() {
        return "RepMedicalClaim_employeeMedicalInsured";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeMedicalAmount">
    @Column
    private BigDecimal employeeMedicalAmount;

        public BigDecimal getEmployeeMedicalAmount() {
            return employeeMedicalAmount;
        }

        public void setEmployeeMedicalAmount(BigDecimal employeeMedicalAmount) {
            this.employeeMedicalAmount = employeeMedicalAmount;
        }
        public String getEmployeeMedicalAmountDD() {
            return "RepMedicalClaim_employeeMedicalAmount";
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeMedicalGlassesDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date employeeMedicalGlassesDate;

        public Date getEmployeeMedicalGlassesDate() {
            return employeeMedicalGlassesDate;
        }

        public void setEmployeeMedicalGlassesDate(Date employeeMedicalGlassesDate) {
            this.employeeMedicalGlassesDate = employeeMedicalGlassesDate;
        }
        public String getEmployeeMedicalGlassesDateDD() {
            return "RepMedicalClaim_employeeMedicalGlassesDate";
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeConsumedMedicalAmount">
    @Column

    private BigDecimal employeeConsumedMedicalAmount;

        public BigDecimal getEmployeeConsumedMedicalAmount() {
            return employeeConsumedMedicalAmount;
        }

        public void setEmployeeConsumedMedicalAmount(BigDecimal employeeConsumedMedicalAmount) {
            this.employeeConsumedMedicalAmount = employeeConsumedMedicalAmount;
        }
        public String getEmployeeConsumedMedicalAmountDD() {
            return "RepMedicalClaim_employeeConsumedMedicalAmount";
        }



    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeMedicalGlassesStatus">
       @Transient
    @Translation(originalField = "employeeMedicalGlassesStatus")
    private String employeeMedicalGlassesStatusTranslated;

    public String getEmployeeMedicalGlassesStatusTranslated() {
        return employeeMedicalGlassesStatusTranslated;
    }

    public void setEmployeeMedicalGlassesStatusTranslated(String employeeMedicalGlassesStatusTranslated) {
        this.employeeMedicalGlassesStatusTranslated = employeeMedicalGlassesStatusTranslated;
    }
     public String getEmployeeMedicalGlassesStatusTranslatedDD() {
        return "RepMedicalClaim_employeeMedicalGlassesStatusTranslated";
    }   
    @Translatable(translationField = "employeeMedicalGlassesStatusTranslated")

    private String employeeMedicalGlassesStatus;

        public String getEmployeeMedicalGlassesStatus() {
            return employeeMedicalGlassesStatus;
        }

        public void setEmployeeMedicalGlassesStatus(String employeeMedicalGlassesStatus) {
            this.employeeMedicalGlassesStatus = employeeMedicalGlassesStatus;
        }
        public String getEmployeeMedicalGlassesStatusDD() {
            return "RepMedicalClaim_employeeMedicalGlassesStatus";
        }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalCategoryDescription">
    @Transient
    @Translation(originalField = "medicalCategoryDescription")
    private String medicalCategoryDescriptionTranslated;

    public String getMedicalCategoryDescriptionTranslated() {
        return medicalCategoryDescriptionTranslated;
    }

    public void setMedicalCategoryDescriptionTranslated(String medicalCategoryDescriptionTranslated) {
        this.medicalCategoryDescriptionTranslated = medicalCategoryDescriptionTranslated;
    }
     public String getMedicalCategoryDescriptionTranslatedDD() {
        return "RepMedicalClaim_medicalCategoryDescriptionTranslated";
    }   
    @Translatable(translationField = "medicalCategoryDescriptionTranslated")

    private String medicalCategoryDescription;

        public String getMedicalCategoryDescription() {
            return medicalCategoryDescription;
        }

        public void setMedicalCategoryDescription(String medicalCategoryDescription) {
            this.medicalCategoryDescription = medicalCategoryDescription;
        }
        public String getMedicalCategoryDescriptionDD() {
            return "RepMedicalClaim_medicalCategoryDescription";
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="categoriesGender">
        @Transient
    @Translation(originalField = "categoriesGender")
    private String categoriesGenderTranslated;

    public String getCategoriesGenderTranslated() {
        return categoriesGenderTranslated;
    }

    public void setCategoriesGenderTranslated(String categoriesGenderTranslated) {
        this.categoriesGenderTranslated = categoriesGenderTranslated;
    }
    public String getCategoriesGenderTranslatedDD() {
        return "RepMedicalClaim_categoriesGenderTranslated";
    }    
    @Translatable(translationField = "categoriesGenderTranslated")

    private String categoriesGender;

        public String getCategoriesGender() {
            return categoriesGender;
        }

        public void setCategoriesGender(String categoriesGender) {
            this.categoriesGender = categoriesGender;
        }
        public String getCategoriesGenderDD() {
            return "RepMedicalClaim_categoriesGender";
        }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="categoriesMaritalStatus">
    @Transient
    @Translation(originalField = "categoriesMaritalStatus")
    private String categoriesMaritalStatusTranslated;

    public String getCategoriesMaritalStatusTranslated() {
        return categoriesMaritalStatusTranslated;
    }

    public void setCategoriesMaritalStatusTranslated(String categoriesMaritalStatusTranslated) {
        this.categoriesMaritalStatusTranslated = categoriesMaritalStatusTranslated;
    }
    public String getCategoriesMaritalStatusTranslatedDD() {
        return "RepMedicalClaim_categoriesMaritalStatusTranslated";
    }    
    @Translatable(translationField = "categoriesMaritalStatusTranslated")

    private String categoriesMaritalStatus;

        public String getCategoriesMaritalStatus() {
            return categoriesMaritalStatus;
        }

    public void setCategoriesMaritalStatus(String categoriesMaritalStatus) {
            this.categoriesMaritalStatus = categoriesMaritalStatus;
        }
        public String getCategoriesMaritalStatusDD() {
            return "RepMedicalClaim_categoriesMaritalStatus";
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalCategoriesYearlyAmount">
    @Column

    private BigDecimal medicalCategoriesYearlyAmount;

        public BigDecimal getMedicalCategoriesYearlyAmount() {
            return medicalCategoriesYearlyAmount;
        }

        public void setMedicalCategoriesYearlyAmount(BigDecimal medicalCategoriesYearlyAmount) {
            this.medicalCategoriesYearlyAmount = medicalCategoriesYearlyAmount;
        }
        public String getMedicalCategoriesYearlyAmountDD() {
            return "RepMedicalClaim_medicalCategoriesYearlyAmount";
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalCategoryConditions">
    @Column

    private String medicalCategoryConditions;

        public String getMedicalCategoryConditions() {
            return medicalCategoryConditions;
        }

        public void setMedicalCategoryConditions(String medicalCategoryConditions) {
            this.medicalCategoryConditions = medicalCategoryConditions;
        }
        public String getMedicalCategoryConditionsDD() {
            return "RepMedicalClaim_medicalCategoryConditions";
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalCategoryForParents">
    @Column

    private String medicalCategoryForParents;

        public String getMedicalCategoryForParents() {
            return medicalCategoryForParents;
        }

        public void setMedicalCategoryForParents(String medicalCategoryForParents) {
            this.medicalCategoryForParents = medicalCategoryForParents;
        }
        public String getMedicalCategoryForParentsDD() {
            return "RepMedicalClaim_medicalCategoryForParents";
        }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalCategoryForSpouse">
    @Column

    private String medicalCategoryForSpouse;

        public String getMedicalCategoryForSpouse() {
            return medicalCategoryForSpouse;
        }

        public void setMedicalCategoryForSpouse(String medicalCategoryForSpouse) {
            this.medicalCategoryForSpouse = medicalCategoryForSpouse;
        }
        public String getMedicalCategoryForSpouseDD() {
            return "RepMedicalClaim_medicalCategoryForSpouse";
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalCategoryForChildren">
    @Column

    private String medicalCategoryForChildren;

        public String getMedicalCategoryForChildren() {
            return medicalCategoryForChildren;
        }

        public void setMedicalCategoryForChildren(String medicalCategoryForChildren) {
            this.medicalCategoryForChildren = medicalCategoryForChildren;
        }

        public String getMedicalCategoryForChildrenDD() {
            return "RepMedicalClaim_medicalCategoryForChildren";
        }

    //</editor-fold>

}