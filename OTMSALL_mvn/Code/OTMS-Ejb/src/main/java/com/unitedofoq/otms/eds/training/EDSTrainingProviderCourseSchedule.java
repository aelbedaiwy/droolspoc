/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.training;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="trainingProviderCourse")
@Table(name= "edstrainprocoursch")
public class EDSTrainingProviderCourseSchedule extends BaseEntity{
    @Column(nullable=false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
    private Date endDate;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSTrainingProviderCourse trainingProviderCourse;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public EDSTrainingProviderCourse getTrainingProviderCourse() {
        return trainingProviderCourse;
    }

    public void setTrainingProviderCourse(EDSTrainingProviderCourse trainingProviderCourse) {
        this.trainingProviderCourse = trainingProviderCourse;
    }
}