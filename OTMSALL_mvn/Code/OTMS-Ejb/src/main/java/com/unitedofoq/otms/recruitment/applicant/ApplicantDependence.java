/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantDependence extends PersonnelDependenceBase {
// <editor-fold defaultstate="collapsed" desc="Applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "ApplicantDependence_applicant";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="entranceport">
    @Column
    private String entranceport;
    public String getEntranceport() {
        return entranceport;
    }
    public void setEntranceport(String entranceport) {
        this.entranceport = entranceport;
    }
       public String getEntranceportDD() {
        return "ApplicantDependence_entranceport";
    }
 // </editor-fold>
}
