
package com.unitedofoq.otms.recruitment.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepApplicantData extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public String getCommentsDD() {
        return "RepApplicantData_comments";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="loginName">
    @Column
    private String loginName;

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getLoginNameDD() {
        return "RepApplicantData_loginName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="location">
    @Column
    private String location;

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "RepApplicantData_location";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="firstLanguage">
    @Column
    private String firstLanguage;

    public void setFirstLanguage(String firstLanguage) {
        this.firstLanguage = firstLanguage;
    }

    public String getFirstLanguage() {
        return firstLanguage;
    }

    public String getFirstLanguageDD() {
        return "RepApplicantData_firstLanguage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="secondLanguage">
    @Column
    private String secondLanguage;

    public void setSecondLanguage(String secondLanguage) {
        this.secondLanguage = secondLanguage;
    }

    public String getSecondLanguage() {
        return secondLanguage;
    }

    public String getSecondLanguageDD() {
        return "RepApplicantData_secondLanguage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="applicantFullName">
    @Column
    private String applicantFullName;

    public void setApplicantFullName(String applicantFullName) {
        this.applicantFullName = applicantFullName;
    }

    public String getApplicantFullName() {
        return applicantFullName;
    }

    public String getApplicantFullNameDD() {
        return "RepApplicantData_applicantFullName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="alternativeEmail">
    @Column
    private String alternativeEmail;

    public void setAlternativeEmail(String alternativeEmail) {
        this.alternativeEmail = alternativeEmail;
    }

    public String getAlternativeEmail() {
        return alternativeEmail;
    }

    public String getAlternativeEmailDD() {
        return "RepApplicantData_alternativeEmail";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="email">
    @Column
    private String email;

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailDD() {
        return "RepApplicantData_email";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="phone">
    @Column
    private String phone;

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhoneDD() {
        return "RepApplicantData_phone";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="address">
    @Column
    private String address;

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public String getAddressDD() {
        return "RepApplicantData_address";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="gender">
    @Column
    private String gender;

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getGenderDD() {
        return "RepApplicantData_gender";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="birthDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getBirthDateDD() {
        return "RepApplicantData_birthDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="country">
    @Column
    private String country;

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryDD() {
        return "RepApplicantData_country";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="certificate1">
    @Column
    private String certificate1;

    public void setCertificate1(String certificate1) {
        this.certificate1 = certificate1;
    }

    public String getCertificate1() {
        return certificate1;
    }

    public String getCertificate1DD() {
        return "RepApplicantData_certificate1";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="certificate2">
    @Column
    private String certificate2;

    public void setCertificate2(String certificate2) {
        this.certificate2 = certificate2;
    }

    public String getCertificate2() {
        return certificate2;
    }

    public String getCertificate2DD() {
        return "RepApplicantData_certificate2";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="certificate3">
    @Column
    private String certificate3;

    public void setCertificate3(String certificate3) {
        this.certificate3 = certificate3;
    }

    public String getCertificate3() {
        return certificate3;
    }

    public String getCertificate3DD() {
        return "RepApplicantData_certificate3";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepApplicantData_dsDbid";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="companyID">
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }
    
    public String getCompanyIDDD() {
        return "RepApplicantData_companyID";
    }
    //</editor-fold>

}
