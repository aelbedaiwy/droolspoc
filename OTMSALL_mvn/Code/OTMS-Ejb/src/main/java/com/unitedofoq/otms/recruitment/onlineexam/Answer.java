package com.unitedofoq.otms.recruitment.onlineexam;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"question"})
public class Answer extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="question">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private MCQuestion question;

    public MCQuestion getQuestion() {
        return question;
    }

    public void setQuestion(MCQuestion question) {
        this.question = question;
    }

    public String getQuestionDD() {
        return "Answer_question";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "Answer_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    @Column(nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "Answer_name";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="rankValue">
    private int rankValue;

    public int getRankValue() {
        return rankValue;
    }

    public void setRankValue(int rankValue) {
        this.rankValue = rankValue;
    }

    public String getRankValueDD() {
        return "Answer_rankValue";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable = false)
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "Answer_sortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="checked">
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getCheckedDD() {
        return "Answer_checked";
    }
    // </editor-fold>
}
