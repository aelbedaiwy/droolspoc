package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.timemanagement.TimeManagementMachine;
import com.unitedofoq.otms.timemanagement.timesheet.ProjectNature;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"annualPlanner"})
public class EmployeeDailyAttendance extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="annualPlanner">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private EmployeeAnnualPlanner annualPlanner;

    public EmployeeAnnualPlanner getAnnualPlanner() {
        return annualPlanner;
    }

    public void setAnnualPlanner(EmployeeAnnualPlanner annualPlanner) {
        this.annualPlanner = annualPlanner;
    }

    public String getAnnualPlannerDD() {
        return "EmployeeDailyAttendance_annualPlanner";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeDailyAttendance_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="String">
    @Temporal(TemporalType.DATE)
    private Date dailyDate;

    public Date getDailyDate() {
        return dailyDate;
    }

    public void setDailyDate(Date dailyDate) {
        this.dailyDate = dailyDate;
    }

    public String getDailyDateDD() {
        return "EmployeeDailyAttendance_dailyDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="plannedIn">
    private String plannedIn;

    public String getPlannedIn() {
        return plannedIn;
    }

    public void setPlannedIn(String plannedIn) {
        this.plannedIn = plannedIn;
    }

    public String getPlannedInDD() {
        return "EmployeeDailyAttendance_plannedIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="plannedOut">
    private String plannedOut;

    public String getPlannedOut() {
        return plannedOut;
    }

    public void setPlannedOut(String plannedOut) {
        this.plannedOut = plannedOut;
    }

    public String getPlannedOutDD() {
        return "EmployeeDailyAttendance_plannedOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="timeIn">
    //@Column
    ////@Temporal(TemporalType.TIMESTAMP)
    private String timeIn;

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeInDD() {
        return "EmployeeDailyAttendance_timeIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="timeOut">
//    @Column
//    //@Temporal(TemporalType.TIMESTAMP)
    private String timeOut;

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getTimeOutDD() {
        return "EmployeeDailyAttendance_timeOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dayNature">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dayNature;

    public UDC getDayNature() {
        return dayNature;
    }

    public void setDayNature(UDC dayNature) {
        this.dayNature = dayNature;
    }

    public String getDayNatureDD() {
        return "EmployeeDailyAttendance_dayNature";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="oldDayNature">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC oldDayNature;

    public UDC getOldDayNature() {
        return oldDayNature;
    }

    public void setOldDayNature(UDC oldDayNature) {
        this.oldDayNature = oldDayNature;
    }

    public String getOldDayNatureDD() {
        return "EmployeeDailyAttendance_oldDayNature";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="earlyLeavePeriod">
    private BigDecimal earlyLeavePeriod;

    public BigDecimal getEarlyLeavePeriod() {
        return earlyLeavePeriod;
    }

    public void setEarlyLeavePeriod(BigDecimal earlyLeavePeriod) {
        this.earlyLeavePeriod = earlyLeavePeriod;
    }

    public String getEarlyLeavePeriodDD() {
        return "EmployeeDailyAttendance_earlyLeavePeriod";
    }
    @Transient
    private BigDecimal earlyLeavePeriodMask;

    public BigDecimal getEarlyLeavePeriodMask() {
        earlyLeavePeriodMask = earlyLeavePeriod;
        return earlyLeavePeriodMask;
    }

    public void setEarlyLeavePeriodMask(BigDecimal earlyLeavePeriodMask) {
        updateDecimalValue("earlyLeavePeriod", earlyLeavePeriodMask);
    }

    public String getEarlyLeavePeriodMaskDD() {
        return "EmployeeDailyAttendance_earlyLeavePeriodMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="earlyLeaveDisplay">
    private String earlyLeaveDisplay;

    public String getEarlyLeaveDisplay() {
        return earlyLeaveDisplay;
    }

    public void setEarlyLeaveDisplay(String earlyLeaveDisplay) {
        this.earlyLeaveDisplay = earlyLeaveDisplay;
    }

    public String getEarlyLeaveDisplayDD() {
        return "EmployeeDailyAttendance_earlyLeaveDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="earlyLeaveIn">
    ////@Temporal(TemporalType.TIMESTAMP)
    private String earlyLeaveIn;

    public String getEarlyLeaveIn() {
        return earlyLeaveIn;
    }

    public void setEarlyLeaveIn(String earlyLeaveIn) {
        this.earlyLeaveIn = earlyLeaveIn;
    }

    public String getEarlyLeaveInDD() {
        return "EmployeeDailyAttendance_earlyLeaveIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="earlyLeaveOut">
    ////@Temporal(TemporalType.TIMESTAMP)
    private String earlyLeaveOut;

    public String getEarlyLeaveOut() {
        return earlyLeaveOut;
    }

    public void setEarlyLeaveOut(String earlyLeaveOut) {
        this.earlyLeaveOut = earlyLeaveOut;
    }

    public String getEarlyLeaveOutDD() {
        return "EmployeeDailyAttendance_earlyLeaveOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="delayHours">
    private BigDecimal delayHoursPeriod;

    public BigDecimal getDelayHoursPeriod() {
        return delayHoursPeriod;
    }

    public void setDelayHoursPeriod(BigDecimal delayHoursPeriod) {
        this.delayHoursPeriod = delayHoursPeriod;
    }

    public String getDelayHoursPeriodDD() {
        return "EmployeeDailyAttendance_delayHoursPeriod";
    }
    @Transient
    private BigDecimal delayHoursPeriodMask;

    public BigDecimal getDelayHoursPeriodMask() {
        delayHoursPeriodMask = delayHoursPeriod;
        return delayHoursPeriodMask;
    }

    public void setDelayHoursPeriodMask(BigDecimal delayHoursPeriodMask) {
        updateDecimalValue("delayHoursPeriod", delayHoursPeriodMask);
    }

    public String getDelayHoursPeriodMaskDD() {
        return "EmployeeDailyAttendance_delayHoursPeriodMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="delayHoursDisplay">
    private String delayHoursDisplay;

    public String getDelayHoursDisplay() {
        return delayHoursDisplay;
    }

    public void setDelayHoursDisplay(String delayHoursDisplay) {
        this.delayHoursDisplay = delayHoursDisplay;
    }

    public String getDelayHoursDisplayDD() {
        return "EmployeeDailyAttendance_delayHoursDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="delayIn">
    //@Temporal(TemporalType.TIMESTAMP)
    private String delayIn;

    public String getDelayIn() {
        return delayIn;
    }

    public void setDelayIn(String delayIn) {
        this.delayIn = delayIn;
    }

    public String getDelayInDD() {
        return "EmployeeDailyAttendance_delayIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="delayOut">
    //@Temporal(TemporalType.TIMESTAMP)
    private String delayOut;

    public String getDelayOut() {
        return delayOut;
    }

    public void setDelayOut(String delayOut) {
        this.delayOut = delayOut;
    }

    public String getDelayOutDD() {
        return "EmployeeDailyAttendance_delayOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dayOverTimePeriod">
    private BigDecimal dayOverTimePeriod;

    public BigDecimal getDayOverTimePeriod() {
        return dayOverTimePeriod;
    }

    public void setDayOverTimePeriod(BigDecimal dayOverTimePeriod) {
        this.dayOverTimePeriod = dayOverTimePeriod;
    }

    public String getDayOverTimePeriodDD() {
        return "EmployeeDailyAttendance_dayOverTimePeriod";
    }
    @Transient
    private BigDecimal dayOverTimePeriodMask;

    public BigDecimal getDayOverTimePeriodMask() {
        dayOverTimePeriodMask = dayOverTimePeriod;
        return dayOverTimePeriodMask;
    }

    public void setDayOverTimePeriodMask(BigDecimal dayOverTimePeriodMask) {
        updateDecimalValue("dayOverTimePeriod", dayOverTimePeriodMask);
    }

    public String getDayOverTimePeriodMaskDD() {
        return "EmployeeDailyAttendance_dayOverTimePeriodMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="dayOverTimeDisplay">
    private String dayOverTimeDisplay;

    public String getDayOverTimeDisplay() {
        return dayOverTimeDisplay;
    }

    public void setDayOverTimeDisplay(String dayOverTimeDisplay) {
        this.dayOverTimeDisplay = dayOverTimeDisplay;
    }

    public String getDayOverTimeDisplayDD() {
        return "EmployeeDailyAttendance_dayOverTimeDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dayOverTimeIn">
    //@Temporal(TemporalType.TIMESTAMP)
    private String dayOverTimeIn;

    public String getDayOverTimeIn() {
        return dayOverTimeIn;
    }

    public void setDayOverTimeIn(String dayOverTimeIn) {
        this.dayOverTimeIn = dayOverTimeIn;
    }

    public String getDayOverTimeInDD() {
        return "EmployeeDailyAttendance_dayOverTimeIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dayOverTimeOut">
    //@Temporal(TemporalType.TIMESTAMP)
    private String dayOverTimeOut;

    public String getDayOverTimeOut() {
        return dayOverTimeOut;
    }

    public void setDayOverTimeOut(String dayOverTimeOut) {
        this.dayOverTimeOut = dayOverTimeOut;
    }

    public String getDayOverTimeOutDD() {
        return "EmployeeDailyAttendance_dayOverTimeOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nightOverTimePeriod">
    private BigDecimal nightOverTimePeriod;

    public BigDecimal getNightOverTimePeriod() {
        return nightOverTimePeriod;
    }

    public void setNightOverTimePeriod(BigDecimal nightOverTimePeriod) {
        this.nightOverTimePeriod = nightOverTimePeriod;
    }

    public String getNightOverTimePeriodDD() {
        return "EmployeeDailyAttendance_nightOverTimePeriod";
    }
    @Transient
    private BigDecimal nightOverTimePeriodMask;

    public BigDecimal getNightOverTimePeriodMask() {
        nightOverTimePeriodMask = nightOverTimePeriod;
        return nightOverTimePeriodMask;
    }

    public void setNightOverTimePeriodMask(BigDecimal nightOverTimePeriodMask) {
        updateDecimalValue("nightOverTimePeriod", nightOverTimePeriodMask);
    }

    public String getNightOverTimePeriodMaskDD() {
        return "EmployeeDailyAttendance_nightOverTimePeriodMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="nightOverTimeDisplay">
    private String nightOverTimeDisplay;

    public String getNightOverTimeDisplay() {
        return nightOverTimeDisplay;
    }

    public void setNightOverTimeDisplay(String nightOverTimeDisplay) {
        this.nightOverTimeDisplay = nightOverTimeDisplay;
    }

    public String getNightOverTimeDisplayDD() {
        return "EmployeeDailyAttendance_nightOverTimeDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nightOverTimeIn">
    //@Temporal(TemporalType.TIMESTAMP)
    private String nightOverTimeIn;

    public String getNightOverTimeIn() {
        return nightOverTimeIn;
    }

    public void setNightOverTimeIn(String nightOverTimeIn) {
        this.nightOverTimeIn = nightOverTimeIn;
    }

    public String getNightOverTimeInDD() {
        return "EmployeeDailyAttendance_nightOverTimeIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nightOverTimeOut">
    //@Temporal(TemporalType.TIMESTAMP)
    private String nightOverTimeOut;

    public String getNightOverTimeOut() {
        return nightOverTimeOut;
    }

    public void setNightOverTimeOut(String nightOverTimeOut) {
        this.nightOverTimeOut = nightOverTimeOut;
    }

    public String getNightOverTimeOutDD() {
        return "EmployeeDailyAttendance_nightOverTimeOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lessWorkPeriod">
    private BigDecimal lessWorkPeriod;

    public BigDecimal getLessWorkPeriod() {
        return lessWorkPeriod;
    }

    public void setLessWorkPeriod(BigDecimal lessWorkPeriod) {
        this.lessWorkPeriod = lessWorkPeriod;
    }

    public String getLessWorkPeriodDD() {
        return "EmployeeDailyAttendance_lessWorkPeriod";
    }
    @Transient
    private BigDecimal lessWorkPeriodMask;

    public BigDecimal getLessWorkPeriodMask() {
        lessWorkPeriodMask = lessWorkPeriod;
        return lessWorkPeriodMask;
    }

    public void setLessWorkPeriodMask(BigDecimal lessWorkPeriodMask) {
        updateDecimalValue("lessWorkPeriod", lessWorkPeriodMask);
    }

    public String getLessWorkPeriodMaskDD() {
        return "EmployeeDailyAttendance_lessWorkPeriodMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="lessWorkDisplay">
    private String lessWorkDisplay;

    public String getLessWorkDisplay() {
        return lessWorkDisplay;
    }

    public void setLessWorkDisplay(String lessWorkDisplay) {
        this.lessWorkDisplay = lessWorkDisplay;
    }

    public String getLessWorkDisplayDD() {
        return "EmployeeDailyAttendance_lessWorkDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lessWorkIn">
    //@Temporal(TemporalType.TIMESTAMP)
    private String lessWorkIn;

    public String getLessWorkIn() {
        return lessWorkIn;
    }

    public void setLessWorkIn(String lessWorkIn) {
        this.lessWorkIn = lessWorkIn;
    }

    public String getLessWorkInDD() {
        return "EmployeeDailyAttendance_lessWorkIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lessWorkOut">
    //@Temporal(TemporalType.TIMESTAMP)
    private String lessWorkOut;

    public String getLessWorkOut() {
        return lessWorkOut;
    }

    public void setLessWorkOut(String lessWorkOut) {
        this.lessWorkOut = lessWorkOut;
    }

    public String getLessWorkOutDD() {
        return "EmployeeDailyAttendance_lessWorkOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedDelay">
    private BigDecimal parametrizedDelay;

    public BigDecimal getParametrizedDelay() {
        return parametrizedDelay;
    }

    public void setParametrizedDelay(BigDecimal parametrizedDelay) {
        this.parametrizedDelay = parametrizedDelay;
    }

    public String getParametrizedDelayDD() {
        return "EmployeeDailyAttendance_parametrizedDelay";
    }
    @Transient
    private BigDecimal parametrizedDelayMask;

    public BigDecimal getParametrizedDelayMask() {
        parametrizedDelayMask = parametrizedDelay;
        return parametrizedDelayMask;
    }

    public void setParametrizedDelayMask(BigDecimal parametrizedDelayMask) {
        updateDecimalValue("parametrizedDelay", parametrizedDelayMask);
    }

    public String getParametrizedDelayMaskDD() {
        return "EmployeeDailyAttendance_parametrizedDelayMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedDayOverTime">
    private BigDecimal parametrizedDayOverTime;

    public BigDecimal getParametrizedDayOverTime() {
        return parametrizedDayOverTime;
    }

    public void setParametrizedDayOverTime(BigDecimal parametrizedDayOverTime) {
        this.parametrizedDayOverTime = parametrizedDayOverTime;
    }

    public String getParametrizedDayOverTimeDD() {
        return "EmployeeDailyAttendance_parametrizedDayOverTime";
    }
    @Transient
    private BigDecimal parametrizedDayOverTimeMask;

    public BigDecimal getParametrizedDayOverTimeMask() {
        parametrizedDayOverTimeMask = parametrizedDayOverTime;
        return parametrizedDayOverTimeMask;
    }

    public void setParametrizedDayOverTimeMask(BigDecimal parametrizedDayOverTimeMask) {
        updateDecimalValue("parametrizedDayOverTime", parametrizedDayOverTimeMask);
    }

    public String getParametrizedDayOverTimeMaskDD() {
        return "EmployeeDailyAttendance_parametrizedDayOverTimeMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedNightOverTime">
    private BigDecimal parametrizedNightOverTime;

    public BigDecimal getParametrizedNightOverTime() {
        return parametrizedNightOverTime;
    }

    public void setParametrizedNightOverTime(BigDecimal parametrizedNightOverTime) {
        this.parametrizedNightOverTime = parametrizedNightOverTime;
    }

    public String getParametrizedNightOverTimeDD() {
        return "EmployeeDailyAttendance_parametrizedNightOverTime";
    }
    @Transient
    private BigDecimal parametrizedNightOverTimeMask;

    public BigDecimal getParametrizedNightOverTimeMask() {
        parametrizedNightOverTimeMask = parametrizedNightOverTime;
        return parametrizedNightOverTimeMask;
    }

    public void setParametrizedNightOverTimeMask(BigDecimal parametrizedNightOverTimeMask) {
        updateDecimalValue("parametrizedNightOverTime", parametrizedNightOverTimeMask);
    }

    public String getParametrizedNightOverTimeMaskDD() {
        return "EmployeeDailyAttendance_parametrizedNightOverTimeMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedHolidayOverTime">
    private BigDecimal parametrizedHolidayOverTime;

    public BigDecimal getParametrizedHolidayOverTime() {
        return parametrizedHolidayOverTime;
    }

    public void setParametrizedHolidayOverTime(BigDecimal parametrizedHolidayOverTime) {
        this.parametrizedHolidayOverTime = parametrizedHolidayOverTime;
    }

    public String getParametrizedHolidayOverTimeDD() {
        return "EmployeeDailyAttendance_parametrizedHolidayOverTime";
    }
    @Transient
    private BigDecimal parametrizedHolidayOverTimeMask;

    public BigDecimal getParametrizedHolidayOverTimeMask() {
        parametrizedHolidayOverTimeMask = parametrizedHolidayOverTime;
        return parametrizedHolidayOverTimeMask;
    }

    public void setParametrizedHolidayOverTimeMask(BigDecimal parametrizedHolidayOverTimeMask) {
        updateDecimalValue("parametrizedHolidayOverTime", parametrizedHolidayOverTimeMask);
    }

    public String getParametrizedHolidayOverTimeMaskDD() {
        return "EmployeeDailyAttendance_parametrizedHolidayOverTimeMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedWeekendOverTime">
    private BigDecimal parametrizedWeekendOverTime;

    public BigDecimal getParametrizedWeekendOverTime() {
        return parametrizedWeekendOverTime;
    }

    public void setParametrizedWeekendOverTime(BigDecimal parametrizedWeekendOverTime) {
        this.parametrizedWeekendOverTime = parametrizedWeekendOverTime;
    }

    public String getParametrizedWeekendOverTimeDD() {
        return "EmployeeDailyAttendance_parametrizedWeekendOverTime";
    }
    @Transient
    private BigDecimal parametrizedWeekendOverTimeMask;

    public BigDecimal getParametrizedWeekendOverTimeMask() {
        parametrizedWeekendOverTimeMask = parametrizedWeekendOverTime;
        return parametrizedWeekendOverTimeMask;
    }

    public void setParametrizedWeekendOverTimeMask(BigDecimal parametrizedWeekendOverTimeMask) {
        updateDecimalValue("parametrizedWeekendOverTime", parametrizedWeekendOverTimeMask);
    }

    public String getParametrizedWeekendOverTimeMaskDD() {
        return "EmployeeDailyAttendance_parametrizedWeekendOverTimeMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedAbsence">
    private BigDecimal parametrizedAbsence;

    public BigDecimal getParametrizedAbsence() {
        return parametrizedAbsence;
    }

    public void setParametrizedAbsence(BigDecimal parametrizedAbsence) {
        this.parametrizedAbsence = parametrizedAbsence;
    }

    public String getParametrizedAbsenceDD() {
        return "EmployeeDailyAttendance_parametrizedAbsence";
    }
    @Transient
    private BigDecimal parametrizedAbsenceMask;

    public BigDecimal getParametrizedAbsenceMask() {
        parametrizedAbsenceMask = parametrizedAbsence;
        return parametrizedAbsenceMask;
    }

    public void setParametrizedAbsenceMask(BigDecimal parametrizedAbsenceMask) {
        updateDecimalValue("parametrizedAbsence", parametrizedAbsenceMask);
    }

    public String getParametrizedAbsenceMaskDD() {
        return "EmployeeDailyAttendance_parametrizedAbsenceMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedEarlyLeave">
    private BigDecimal parametrizedEarlyLeave;

    public BigDecimal getParametrizedEarlyLeave() {
        return parametrizedEarlyLeave;
    }

    public void setParametrizedEarlyLeave(BigDecimal parametrizedEarlyLeave) {
        this.parametrizedEarlyLeave = parametrizedEarlyLeave;
    }

    public String getParametrizedEarlyLeaveDD() {
        return "EmployeeDailyAttendance_parametrizedEarlyLeave";
    }
    @Transient
    private BigDecimal parametrizedEarlyLeaveMask;

    public BigDecimal getParametrizedEarlyLeaveMask() {
        parametrizedEarlyLeaveMask = parametrizedEarlyLeave;
        return parametrizedEarlyLeaveMask;
    }

    public void setParametrizedEarlyLeaveMask(BigDecimal parametrizedEarlyLeaveMask) {
        updateDecimalValue("parametrizedEarlyLeave", parametrizedEarlyLeaveMask);
    }

    public String getParametrizedEarlyLeaveMaskDD() {
        return "EmployeeDailyAttendance_parametrizedEarlyLeaveMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parametrizedLessWork">
    private BigDecimal parametrizedLessWork;

    public BigDecimal getParametrizedLessWork() {
        return parametrizedLessWork;
    }

    public void setParametrizedLessWork(BigDecimal parametrizedLessWork) {
        this.parametrizedLessWork = parametrizedLessWork;
    }

    public String getParametrizedLessWorkDD() {
        return "EmployeeDailyAttendance_parametrizedLessWork";
    }
    @Transient
    private BigDecimal parametrizedLessWorkMask;

    public BigDecimal getParametrizedLessWorkMask() {
        parametrizedLessWorkMask = parametrizedLessWork;
        return parametrizedLessWorkMask;
    }

    public void setParametrizedLessWorkMask(BigDecimal parametrizedLessWorkMask) {
        updateDecimalValue("parametrizedLessWork", parametrizedLessWorkMask);
    }

    public String getParametrizedLessWorkMaskDD() {
        return "EmployeeDailyAttendance_parametrizedLessWorkMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="breakPeriod">
    private BigDecimal breakPeriod;

    public BigDecimal getBreakPeriod() {
        return breakPeriod;
    }

    public void setBreakPeriod(BigDecimal breakPeriod) {
        this.breakPeriod = breakPeriod;
    }

    public String getBreakPeriodDD() {
        return "EmployeeDailyAttendance_breakPeriod";
    }
    @Transient
    private BigDecimal breakPeriodMask;

    public BigDecimal getBreakPeriodMask() {
        breakPeriodMask = breakPeriod;
        return breakPeriodMask;
    }

    public void setBreakPeriodMask(BigDecimal breakPeriodMask) {
        updateDecimalValue("breakPeriod", breakPeriodMask);
    }

    public String getBreakPeriodMaskDD() {
        return "EmployeeDailyAttendance_breakPeriodMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="breakIn">
    //@Temporal(TemporalType.TIMESTAMP)
    private String breakIn;

    public String getBreakIn() {
        return breakIn;
    }

    public void setBreakIn(String breakIn) {
        this.breakIn = breakIn;
    }

    public String getBreakInDD() {
        return "EmployeeDailyAttendance_breakIn";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="breakOut">
    //@Temporal(TemporalType.TIMESTAMP)
    private String breakOut;

    public String getBreakOut() {
        return breakOut;
    }

    public void setBreakOut(String breakOut) {
        this.breakOut = breakOut;
    }

    public String getBreakOutDD() {
        return "EmployeeDailyAttendance_breakOut";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="break2From">
//    @Column
//    @Temporal(TemporalType.TIMESTAMP)
    private String break2From;

    public String getBreak2From() {
        return break2From;
    }

    public void setBreak2From(String breakFrom) {
        this.break2From = breakFrom;
    }

    public String getBreak2FromDD() {
        return "EmployeeDailyAttendance_break2From";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="break2To">
    //@Temporal(TemporalType.TIMESTAMP)
    private String break2to;

    public String getBreak2to() {
        return break2to;
    }

    public void setBreak2to(String break2to) {
        this.break2to = break2to;
    }

    public String getBreak2toDD() {
        return "EmployeeDailyAttendance_break2to";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notApproved">
    private String notApproved;

    public String getNotApproved() {
        return notApproved;
    }

    public void setNotApproved(String notApproved) {
        this.notApproved = notApproved;
    }

    public String getNotApprovedDD() {
        return "EmployeeDailyAttendance_notApproved";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notApprovedDay">
    private String notApprovedDay;

    public String getNotApprovedDay() {
        return notApprovedDay;
    }

    public void setNotApprovedDay(String notApprovedDay) {
        this.notApprovedDay = notApprovedDay;
    }

    public String getNotApprovedDayDD() {
        return "EmployeeDailyAttendance_notApprovedDay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notApprovedNight">
    private String notApprovedNight;

    public String getNotApprovedNight() {
        return notApprovedNight;
    }

    public void setNotApprovedNight(String notApprovedNight) {
        this.notApprovedNight = notApprovedNight;
    }

    public String getNotApprovedNightDD() {
        return "EmployeeDailyAttendance_notApprovedNight";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nonPlanned">
    private String nonPlanned;

    public String getNonPlanned() {
        return nonPlanned;
    }

    public void setNonPlanned(String nonPlanned) {
        this.nonPlanned = nonPlanned;
    }

    public String getNonPlannedDD() {
        return "EmployeeDailyAttendance_nonPlanned";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="exceptionFrom">
    private String exceptionFrom;

    public String getExceptionFrom() {
        return exceptionFrom;
    }

    public void setExceptionFrom(String exceptionFrom) {
        this.exceptionFrom = exceptionFrom;
    }

    public String getExceptionFromDD() {
        return "EmployeeDailyAttendance_exceptionFrom";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="exceptionTo">
    private String exceptionTo;

    public String getExceptionTo() {
        return exceptionTo;
    }

    public void setExceptionTo(String exceptionTo) {
        this.exceptionTo = exceptionTo;
    }

    public String getExceptionToDD() {
        return "EmployeeDailyAttendance_exceptionTo";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="exceptionHours">
    private BigDecimal exceptionHours;

    public BigDecimal getExceptionHours() {
        return exceptionHours;
    }

    public void setExceptionHours(BigDecimal exceptionHours) {
        this.exceptionHours = exceptionHours;
    }

    public String getExceptionHoursDD() {
        return "EmployeeDailyAttendance_exceptionHours";
    }
    @Transient
    private BigDecimal exceptionHoursMask;

    public BigDecimal getExceptionHoursMask() {
        exceptionHoursMask = exceptionHours;
        return exceptionHoursMask;
    }

    public void setExceptionHoursMask(BigDecimal exceptionHoursMask) {
        updateDecimalValue("exceptionHours", exceptionHoursMask);
    }

    public String getExceptionHoursMaskDD() {
        return "EmployeeDailyAttendance_exceptionHoursMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notApprovedWeekendOT">
    private BigDecimal notApprovedWeekendOT;

    public BigDecimal getNotApprovedWeekendOT() {
        return notApprovedWeekendOT;
    }

    public void setNotApprovedWeekendOT(BigDecimal notApprovedWeekendOT) {
        this.notApprovedWeekendOT = notApprovedWeekendOT;
    }

    public String getNotApprovedWeekendOTDD() {
        return "EmployeeDailyAttendance_notApprovedWeekendOT";
    }
    @Transient
    private BigDecimal notApprovedWeekendOTMask;

    public BigDecimal getNotApprovedWeekendOTMask() {
        notApprovedWeekendOTMask = notApprovedWeekendOT;
        return notApprovedWeekendOTMask;
    }

    public void setNotApprovedWeekendOTMask(BigDecimal notApprovedWeekendOTMask) {
        updateDecimalValue("notApprovedWeekendOT", notApprovedWeekendOTMask);
    }

    public String getNotApprovedWeekendOTMaskDD() {
        return "EmployeeDailyAttendance_notApprovedWeekendOTMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="notApprovedWeekendDisplay">
    private String notApprovedWeekendDisplay;

    public String getNotApprovedWeekendDisplay() {
        return notApprovedWeekendDisplay;
    }

    public void setNotApprovedWeekendDisplay(String notApprovedWeekendDisplay) {
        this.notApprovedWeekendDisplay = notApprovedWeekendDisplay;
    }

    public String getNotApprovedWeekendDisplayDD() {
        return "EmployeeDailyAttendance_notApprovedWeekendDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="notApprovedHolidayOT">
    private BigDecimal notApprovedHolidayOT;

    public BigDecimal getNotApprovedHolidayOT() {
        return notApprovedHolidayOT;
    }

    public void setNotApprovedHolidayOT(BigDecimal notApprovedHolidayOT) {
        this.notApprovedHolidayOT = notApprovedHolidayOT;
    }

    public String getNotApprovedHolidayOTDD() {
        return "EmployeeDailyAttendance_notApprovedHolidayOT";
    }
    @Transient
    private BigDecimal notApprovedHolidayOTMask;

    public BigDecimal getNotApprovedHolidayOTMask() {
        notApprovedHolidayOTMask = notApprovedHolidayOT;
        return notApprovedHolidayOTMask;
    }

    public void setNotApprovedHolidayOTMask(BigDecimal notApprovedHolidayOTMask) {
        updateDecimalValue("notApprovedHolidayOT", notApprovedHolidayOTMask);
    }

    public String getNotApprovedHolidayOTMaskDD() {
        return "EmployeeDailyAttendance_notApprovedHolidayOTMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="notApprovedHolidayDisplay">
    private String notApprovedHolidayDisplay;

    public String getNotApprovedHolidayDisplay() {
        return notApprovedHolidayDisplay;
    }

    public void setNotApprovedHolidayDisplay(String notApprovedHolidayDisplay) {
        this.notApprovedHolidayDisplay = notApprovedHolidayDisplay;
    }

    public String getNotApprovedHolidayDisplayDD() {
        return "EmployeeDailyAttendance_notApprovedHolidayDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="approvedWeekendOT">
    private BigDecimal approvedWeekendOT;

    public BigDecimal getApprovedWeekendOT() {
        return approvedWeekendOT;
    }

    public void setApprovedWeekendOT(BigDecimal approvedWeekendOT) {
        this.approvedWeekendOT = approvedWeekendOT;
    }

    public String getApprovedWeekendOTDD() {
        return "EmployeeDailyAttendance_approvedWeekendOT";
    }
    @Transient
    private BigDecimal approvedWeekendOTMask;

    public BigDecimal getApprovedWeekendOTMask() {
        approvedWeekendOTMask = approvedWeekendOT;
        return approvedWeekendOTMask;
    }

    public void setApprovedWeekendOTMask(BigDecimal approvedWeekendOTMask) {
        updateDecimalValue("approvedWeekendOT", approvedWeekendOTMask);
    }

    public String getApprovedWeekendOTMaskDD() {
        return "EmployeeDailyAttendance_approvedWeekendOTMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="approvedWeekendOTDisplay">
    private String approvedWeekendOTDisplay;

    public String getApprovedWeekendOTDisplay() {
        return approvedWeekendOTDisplay;
    }

    public void setApprovedWeekendOTDisplay(String approvedWeekendOTDisplay) {
        this.approvedWeekendOTDisplay = approvedWeekendOTDisplay;
    }

    public String getApprovedWeekendOTDisplayDD() {
        return "EmployeeDailyAttendance_approvedWeekendOTDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="approvedHolidayOT">
    private BigDecimal approvedHolidayOT;

    public BigDecimal getApprovedHolidayOT() {
        return approvedHolidayOT;
    }

    public void setApprovedHolidayOT(BigDecimal approvedHolidayOT) {
        this.approvedHolidayOT = approvedHolidayOT;
    }

    public String getApprovedHolidayOTDD() {
        return "EmployeeDailyAttendance_approvedHolidayOT";
    }
    @Transient
    private BigDecimal approvedHolidayOTMask;

    public BigDecimal getApprovedHolidayOTMask() {
        approvedHolidayOTMask = approvedHolidayOT;
        return approvedHolidayOTMask;
    }

    public void setApprovedHolidayOTMask(BigDecimal approvedHolidayOTMask) {
        updateDecimalValue("approvedHolidayOT", approvedHolidayOTMask);
    }

    public String getApprovedHolidayOTMaskDD() {
        return "EmployeeDailyAttendance_approvedHolidayOTMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="approvedHolidayOTDisplay">
    private String approvedHolidayOTDisplay;

    public String getApprovedHolidayOTDisplay() {
        return approvedHolidayOTDisplay;
    }

    public void setApprovedHolidayOTDisplay(String approvedHolidayOTDisplay) {
        this.approvedHolidayOTDisplay = approvedHolidayOTDisplay;
    }

    public String getApprovedHolidayOTDisplayDD() {
        return "EmployeeDailyAttendance_approvedHolidayOTDisplay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="accessMachine">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TimeManagementMachine accessMachine;

    public TimeManagementMachine getAccessMachine() {
        return accessMachine;
    }

    public String getAccessMachineDD() {
        return "EmployeeDailyAttendance_accessMachine";
    }

    public void setAccessMachine(TimeManagementMachine accessMachine) {
        this.accessMachine = accessMachine;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="shift">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private NormalWorkingCalendar shift;

    public NormalWorkingCalendar getShift() {
        return shift;
    }

    public void setShift(NormalWorkingCalendar shift) {
        this.shift = shift;
    }

    public String getShiftDD() {
        return "EmployeeDailyAttendance_shift";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="projectNature">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProjectNature projectNature;

    public ProjectNature getProjectNature() {
        return projectNature;
    }

    public String getProjectNatureDD() {
        return "EmployeeDailyAttendance_projectNature";
    }

    public void setProjectNature(ProjectNature projectNature) {
        this.projectNature = projectNature;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="diffValue">
    private BigDecimal diffValue;

    public BigDecimal getDiffValue() {
        return diffValue;
    }

    public String getDiffValueDD() {
        return "EmployeeDailyAttendance_diffValue";
    }

    public void setDiffValue(BigDecimal diffValue) {
        this.diffValue = diffValue;
    }
    @Transient
    private BigDecimal diffValueMask;

    public BigDecimal getDiffValueMask() {
        diffValueMask = diffValue;
        return diffValueMask;
    }

    public String getDiffValueMaskDD() {
        return "EmployeeDailyAttendance_diffValueMask";
    }

    public void setDiffValueMask(BigDecimal diffValueMask) {
        updateDecimalValue("diffValue", diffValueMask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="outDate">
    @Temporal(TemporalType.DATE)
    private Date outDate;

    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public String getOutDateDD() {
        return "EmployeeDailyAttendance_outDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="header">
    private boolean header;

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public String getHeaderDD() {
        return "EmployeeDailyAttendance_header";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empDAHeader">
    @ManyToOne(fetch = FetchType.LAZY)
    private EmployeeDailyAttendance empDAHeader;

    public EmployeeDailyAttendance getEmpDAHeader() {
        return empDAHeader;
    }

    public void setEmpDAHeader(EmployeeDailyAttendance empDAHeader) {
        this.empDAHeader = empDAHeader;
    }

    public String getEmpDAHeaderDD() {
        return "EmployeeDailyAttendance_empDAHeader";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="manualDA">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private ManualDailyAttendance manualDA;

    public ManualDailyAttendance getManualDA() {
        return manualDA;
    }

    public void setManualDA(ManualDailyAttendance manualDA) {
        this.manualDA = manualDA;
    }

    public String getManualDADD() {
        return "EmployeeDailyAttendance_manualDA";
    }
    //</editor-fold>
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeDailyAttendance_m2mCheck";
    }

    @PostLoad
    public void postLoad() {
        setM2mCheck(true);
    }
    // <editor-fold defaultstate="collapsed" desc="monthlyConsolidation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeMonthlyConsolidation monthlyConsolidation;

    public EmployeeMonthlyConsolidation getMonthlyConsolidation() {
        return monthlyConsolidation;
    }

    public void setMonthlyConsolidation(EmployeeMonthlyConsolidation monthlyConsolidation) {
        this.monthlyConsolidation = monthlyConsolidation;
    }

    public String getMonthlyConsolidationDD() {
        return "EmployeeDailyAttendance_monthlyConsolidation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingHours">
    private BigDecimal workingHours;

    public BigDecimal getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(BigDecimal workingHours) {
        this.workingHours = workingHours;
    }

    public String getWorkingHoursDD() {
        return "EmployeeDailyAttendance_workingHours";
    }
    @Transient
    private BigDecimal workingHoursMask;

    public BigDecimal getWorkingHoursMask() {
        workingHoursMask = workingHours;
        return workingHoursMask;
    }

    public void setWorkingHoursMaskMask(BigDecimal workingHoursMask) {
        updateDecimalValue("workingHours", workingHoursMask);
    }

    public String getWorkingHoursMaskMaskDD() {
        return "EmployeeDailyAttendance_workingHoursMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingCalendar">
    @ManyToOne(fetch = FetchType.LAZY)
    private NormalWorkingCalendar workingCalendar;

    public NormalWorkingCalendar getWorkingCalendar() {
        return workingCalendar;
    }

    public void setWorkingCalendar(NormalWorkingCalendar workingCalendar) {
        this.workingCalendar = workingCalendar;
    }

    public String getWorkingCalendarDD() {
        return "EmployeeDailyAttendance_workingCalendar";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="externalOrNot">
    @Column(length = 1)
    private String externalOrNot;

    public String getExternalOrNot() {
        return externalOrNot;
    }

    public String getExternalOrNotDD() {
        return "EmployeeDailyAttendance_externalOrNot";
    }

    public void setExternalOrNot(String externalOrNot) {
        this.externalOrNot = externalOrNot;
    }
    //</editor-fold >

    private boolean consolidation;

    public boolean isConsolidation() {
        return consolidation;
    }

    public void setConsolidation(boolean consolidation) {
        this.consolidation = consolidation;
    }

    public String getConsolidationDD() {
        return "EmployeeDailyAttendance_consolidation";
    }

    private String dayLessWork;

    public String getDayLessWork() {
        return dayLessWork;
    }

    public void setDayLessWork(String dayLessWork) {
        this.dayLessWork = dayLessWork;
    }

    public String getDayLessWorkDD() {
        return "EmployeeDailyAttendance_dayLessWork";
    }
    private BigDecimal dayLessWorkPeriod;

    public BigDecimal getDayLessWorkPeriod() {
        return dayLessWorkPeriod;
    }

    public void setDayLessWorkPeriod(BigDecimal dayLessWorkPeriod) {
        this.dayLessWorkPeriod = dayLessWorkPeriod;
    }

    public String getDayLessWorkPeriodDD() {
        return "EmployeeDailyAttendance_dayLessWorkPeriod";
    }
    private BigDecimal dayWEOT;
    @Transient
    private BigDecimal dayWEOTMask;
    private BigDecimal nightWEOT;
    @Transient
    private BigDecimal nightWEOTMask;
    private BigDecimal weOT;
    @Transient
    private BigDecimal weOTMask;

    public BigDecimal getDayWEOT() {
        return dayWEOT;
    }

    public void setDayWEOT(BigDecimal dayWEOT) {
        this.dayWEOT = dayWEOT;
    }

    public String getDayWEOTDD() {
        return "EmployeeDailyAttendance_dayWEOT";
    }

    public BigDecimal getDayWEOTMask() {
        dayWEOTMask = dayWEOT;
        return dayWEOTMask;
    }

    public void setDayWEOTMask(BigDecimal dayWEOTMask) {
        updateDecimalValue("dayWEOT", dayWEOTMask);
    }

    public String getDayWEOTMaskDD() {
        return "EmployeeDailyAttendance_dayWEOTMask";
    }

    public BigDecimal getNightWEOT() {
        return nightWEOT;
    }

    public void setNightWEOT(BigDecimal nightWEOT) {
        this.nightWEOT = nightWEOT;
    }

    public String getNightWEOTDD() {
        return "EmployeeDailyAttendance_nightWEOT";
    }

    public BigDecimal getNightWEOTMask() {
        nightWEOTMask = nightWEOT;
        return nightWEOTMask;
    }

    public void setNightWEOTMask(BigDecimal nightWEOTMask) {
        this.nightWEOTMask = nightWEOTMask;
    }

    public String getNightWEOTMaskDD() {
        return "EmployeeDailyAttendance_nightWEOTMask";
    }

    public BigDecimal getWeOT() {
        return weOT;
    }

    public void setWeOT(BigDecimal weOT) {
        this.weOT = weOT;
    }

    public String getWeOTDD() {
        return "EmployeeDailyAttendance_weOT";
    }

    public BigDecimal getWeOTMask() {
        weOTMask = weOT;
        return weOTMask;
    }

    public void setWeOTMask(BigDecimal weOTMask) {
        updateDecimalValue("weOT", weOTMask);
    }

    public String getWeOTMaskDD() {
        return "EmployeeDailyAttendance_weOTMask";
    }

    private BigDecimal holidayPayrollOT;
    @Transient
    private BigDecimal holidayPayrollOTMask;

    public BigDecimal getHolidayPayrollOT() {
        return holidayPayrollOT;
    }

    public void setHolidayPayrollOT(BigDecimal holidayPayrollOT) {
        this.holidayPayrollOT = holidayPayrollOT;
    }

    public String getHolidayPayrollOTDD() {
        return "EmployeeMonthlyConsolidation_holidayPayrollOT";
    }

    public BigDecimal getHolidayPayrollOTMask() {
        holidayPayrollOTMask = holidayPayrollOT;
        return holidayPayrollOTMask;
    }

    public void setHolidayPayrollOTMask(BigDecimal holidayPayrollOT) {
        updateDecimalValue("holidayPayrollOT", holidayPayrollOTMask);
    }

    public String getHolidayPayrollOTMaskDD() {
        return "EmployeeDailyAttendance_holidayPayrollOTMask";
    }
}
