/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.validation.DateFromToValidation;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@DateFromToValidation (from={"plannedStartDate"}, to={"plannedEndDate"})
public class EDSIDPAction extends BaseEntity{
    @Translatable(translationField="nameTranslated")
    private String name;
    @Translatable(translationField="achievementCommentTranslated")
    private String achievementComment;
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;    
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    @Transient
    @Translation(originalField="achievementComment")
    private String achievementCommentTranslated;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    private double completionProgress;
    
    private double weight;
    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
    private Date plannedStartDate;
    @Temporal(TemporalType.DATE)
    @Column(nullable=false)
    private Date plannedEndDate;
    @Temporal(TemporalType.DATE)
    private Date lastUpdatedDate;
    @Temporal(TemporalType.DATE)
    
    private Date completionDate;
    
    private String plannedComment;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date actualFinishedDate;

    public String getNameTranslatedDD()               {   return "EDSIDPAction_name";  }
    public String getWeightDD()             {   return "EDSIDPAction_weight";  }
    public String getPlannedStartDateDD()   {   return "EDSIDPAction_plannedStartDate";  }
    public String getPlannedEndDateDD()     {   return "EDSIDPAction_plannedEndDate";  }
    public String getLastUpdatedDateDD()    {   return "EDSIDPAction_lastUpdatedDate";  }
    public String getCompletionDateDD()     {   return "EDSIDPAction_completionDate";  }
    public String getAchievementCommentTranslatedDD() {   return "EDSIDPAction_achievementComment";  }
    public String getCompletionProgressDD() {   return "EDSIDPAction_completionProgress";  }
    public String getActualFinishedDateDD() {   return "EDSIDPAction_actualFinishedDate"; }
    public String getPlannedCommentDD()     {   return "EDSIDPAction_plannedComment";    }
    
    public Date getActualFinishedDate() {
        return actualFinishedDate;
    }

    public void setActualFinishedDate(Date actualFinishedDate) {
        this.actualFinishedDate = actualFinishedDate;
    }

    public String getPlannedComment() {
        return plannedComment;
    }

    public void setPlannedComment(String plannedComment) {
        this.plannedComment = plannedComment;
    }


    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAchievementComment() {
        return achievementComment;
    }

    public void setAchievementComment(String achievementComment) {
        this.achievementComment = achievementComment;
    }


    public double getCompletionProgress() {
        return completionProgress;
    }

    public void setCompletionProgress(double completionProgress) {
        this.completionProgress = completionProgress;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionTranslatedDD() {
        return "EDSIDPAction_description";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    private double actualCompletProg;

    public String getActualCompletProgDD() {
        return "EDSIDPAction_actualCompletProg";
    }

    public double getActualCompletProg() {
        return actualCompletProg;
    }

    public void setActualCompletProg(double actualCompletProg) {
        this.actualCompletProg = actualCompletProg;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date actualCompletionDate;

    public String getActualCompletionDateDD() {
        return "EDSIDPAction_actualCompletionDate";
    }

    public Date getActualCompletionDate() {
        return actualCompletionDate;
    }

    public void setActualCompletionDate(Date actualCompletionDate) {
        this.actualCompletionDate = actualCompletionDate;
    }

    private double plannedWeight;

    public String getPlannedWeightDD() {
        return "EDSIDPAction_plannedWeight";
    }

    public double getPlannedWeight() {
        return plannedWeight;
    }

    public void setPlannedWeight(double plannedWeight) {
        this.plannedWeight = plannedWeight;
    }

    private String actualAchievementComment;

    public String getActualAchievementCommentDD() {
        return "EDSIDPAction_actualAchievementComment";
    }

    public String getActualAchievementComment() {
        return actualAchievementComment;
    }

    public void setActualAchievementComment(String actualAchievementComment) {
        this.actualAchievementComment = actualAchievementComment;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date actualStartDate;

    public String getActualStartDateDD() {
        return "EDSIDPAction_actualStartDate";
    }

    public Date getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(Date actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public String getAchievementCommentTranslated() {
        return achievementCommentTranslated;
    }

    public void setAchievementCommentTranslated(String achievementCommentTranslated) {
        this.achievementCommentTranslated = achievementCommentTranslated;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation courseInformation;

    public String getCourseInformationDD() {
        return "EDSIDPAction_courseInformation";
    }
    
    public ProviderCourseInformation getCourseInformation() {
        return courseInformation;
    }

    public void setCourseInformation(ProviderCourseInformation courseInformation) {
        this.courseInformation = courseInformation;
    }
    
    
}
