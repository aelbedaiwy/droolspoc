/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.scheduleevent;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"scheduleEvent"})
public class ScheduleDay extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="scheduleEvent">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEvent scheduleEvent;

    public ScheduleEvent getScheduleEvent() {
        return scheduleEvent;
    }

    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
        this.scheduleEvent = scheduleEvent;
    }

    public String getScheduleEventDD() {
        return "ScheduleDay_scheduleEvent";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dayDate;

    public Date getDayDate() {
        return dayDate;
    }

    public void setDayDate(Date dayDate) {
        this.dayDate = dayDate;
    }
    
    public String getDayDateDD() {
        return "ScheduleDay_dayDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromTime;

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }
    
    public String getFromTimeDD() {
        return "ScheduleDay_fromTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toTime;

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public String getToTimeDD() {
        return "ScheduleDay_toTime";
    }
    // </editor-fold>
}
