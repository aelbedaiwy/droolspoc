/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author eismail
 */
@Entity
public class CancellationRegulationBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="daysBeforeStartTime">
    @Column
    private Integer daysBeforeStartTime;

    public Integer getDaysBeforeStartTime() {
        return daysBeforeStartTime;
    }

    public void setDaysBeforeStartTime(Integer daysBeforeStartTime) {
        this.daysBeforeStartTime = daysBeforeStartTime;
    }

    public String getDaysBeforeStartTimeDD() {
        return "ProviderActivityCancellationRegulation_daysBeforeStartTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="userType">
    //Manger Or Employee Or HR
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private UDC userType;

    public UDC getUserType() {
        return userType;
    }

    public void setUserType(UDC userType) {
        this.userType = userType;
    }


    public String getUserTypeDD() {
        return "ProviderActivityCancellationRegulation_userType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="percentageOfCourseCost">
    @Column(precision=25, scale=13)
    private BigDecimal percentageOfCourseCost;

    public BigDecimal getPercentageOfCourseCost() {
        return percentageOfCourseCost;
    }

    public void setPercentageOfCourseCost(BigDecimal percentageOfCourseCost) {
        this.percentageOfCourseCost = percentageOfCourseCost;
    }

    public String getPercentageOfCourseCostDD() {
        return "ProviderActivityCancellationRegulation_percentageOfCourseCost";
    }
    @Transient
    private BigDecimal percentageOfCourseCostMask;

    public BigDecimal getPercentageOfCourseCostMask() {
        percentageOfCourseCostMask = percentageOfCourseCost ;
        return percentageOfCourseCostMask;
    }

    public void setPercentageOfCourseCostMask(BigDecimal percentageOfCourseCostMask) {
        updateDecimalValue("percentageOfCourseCost", percentageOfCourseCostMask);
    }

    public String getPercentageOfCourseCostMaskDD() {
        return "ProviderActivityCancellationRegulation_percentageOfCourseCostMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deductionElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deductionElement;
    public Deduction getDeductionElement() {
        return deductionElement;
    }
    public void setDeductionElement(Deduction deductionElement) {
        this.deductionElement = deductionElement;
    }
    public String getDeductionElementDD() {
        return "CancellationRegulationBase_deductionElement";
    }
    // </editor-fold>
}