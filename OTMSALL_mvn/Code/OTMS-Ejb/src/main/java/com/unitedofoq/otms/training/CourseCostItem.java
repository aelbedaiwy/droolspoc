/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training;

import com.unitedofoq.otms.foundation.CostItemMember;
import com.unitedofoq.otms.training.activity.ProviderCourse;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
public class CourseCostItem extends CostItemMember{
    // <editor-fold defaultstate="collapsed" desc="course">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourse course;

    public ProviderCourse getCourse() {
        return course;
    }

    public void setCourse(ProviderCourse course) {
        this.course = course;
    }
    
    public String getCourseDD() {
        return "CourseCostItem_course";
    }
    // </editor-fold >
}
