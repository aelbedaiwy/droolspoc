/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mmohamed
 */
@Entity
public class CompetencyCourse extends BaseEntity {
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Competency competency;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourse course;

    public Competency getCompetency() {
        return competency;
    }
    
    public String getCompetencyDD() {
        return "CompetencyCourse_competency";
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }

    public ProviderCourse getCourse() {
        return course;
    }
    
    public String getCourseDD() {
        return "CompetencyCourse_course";
    }

    public void setCourse(ProviderCourse course) {
        this.course = course;
    }        
    
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }
    
    public String getM2mCheckDD() {
        return "CompetencyCourse_m2mCheck";
    }
    
    @Override
    public void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
    
}
