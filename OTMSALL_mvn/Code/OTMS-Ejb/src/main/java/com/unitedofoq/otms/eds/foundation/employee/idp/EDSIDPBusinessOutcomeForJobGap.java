/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author nkhalil
 */
@Entity
@ChildEntity(fields={"screenInputs", "screenOutputs"})
public class EDSIDPBusinessOutcomeForJobGap extends BaseEntity {
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Translation(originalField="description")
    private String descriptionTranslated;
    private double contributionPercentage;
    private double compeletionProgress;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date whenBOFJG;
    private double evaluation;
    private String evaluationDescription;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date evaluationDate;
    
    @Column(name="comments")
    private String comment;

    public String getCommentDD() {
        return "EDSIDPBusinessOutcomeForJobGap_comment";
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCompeletionProgressDD() {
        return "EDSIDPBusinessOutcomeForJobGap_compeletionProgress";
    }

    public double getCompeletionProgress() {
        return compeletionProgress;
    }

    public void setCompeletionProgress(double compeletionProgress) {
        this.compeletionProgress = compeletionProgress;
    }

    public String getContributionPercentageDD() {
        return "EDSIDPBusinessOutcomeForJobGap_contributionPercentage";
    }

    public double getContributionPercentage() {
        return contributionPercentage;
    }

    public void setContributionPercentage(double contributionPercentage) {
        this.contributionPercentage = contributionPercentage;
    }

    public String getEvaluationDD() {
        return "EDSIDPBusinessOutcomeForJobGap_evaluation";
    }
    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }
    public String getEvaluationDateDD() {
        return "EDSIDPBusinessOutcomeForJobGap_evaluationDate";
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluationDescriptionDD() {
        return "EDSIDPBusinessOutcomeForJobGap_evaluationDescription";
    }

    public String getEvaluationDescription() {
        return evaluationDescription;
    }

    public void setEvaluationDescription(String evaluationDescription) {
        this.evaluationDescription = evaluationDescription;
    }
    public String getWhenBOFJGDD() {
        return "EDSIDPBusinessOutcomeForJobGap_whenBOFJG";
    }

    public Date getWhenBOFJG() {
        return whenBOFJG;
    }

    public void setWhenBOFJG(Date whenBOFJG) {
        this.whenBOFJG = whenBOFJG;
    }

    public String getDescriptionTranslatedDD() {
        return "EDSIDPBusinessOutcomeForJobGap_description";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }   

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private EDSIDPBusinessOutcomeForJobGapAAC gapAAC;

    public String getGapAACDD() {
        return "EDSIDPBusinessOutcomeForJobGap_gapAAC";
    }

    public EDSIDPBusinessOutcomeForJobGapAAC getGapAAC() {
        return gapAAC;
    }

    public void setGapAAC(EDSIDPBusinessOutcomeForJobGapAAC gapAAC) {
        this.gapAAC = gapAAC;
    }

    @OneToMany(mappedBy="jobGap")
    private List<EDSIDPBusinessOutcomeForJobGapAAC> jobGapACCs;    

    public List<EDSIDPBusinessOutcomeForJobGapAAC> getJobGapACCs() {
        return jobGapACCs;
    }

    public void setJobGapACCs(List<EDSIDPBusinessOutcomeForJobGapAAC> jobGapACCs) {
        this.jobGapACCs = jobGapACCs;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
}
