
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"company"})
public class CompanyCalendarInstance extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "CompanyCalendarInstance_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public void setCompany(Company company) {
        this.company = company;
    }

    public Company getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "CompanyCalendarInstance_company";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="year">
    @Column
    private Integer year;

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public String getYearDD() {
        return "CompanyCalendarInstance_year";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "CompanyCalendarInstance_description";
    }
    // </editor-fold>
}