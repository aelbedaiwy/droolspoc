package com.unitedofoq.otms.recruitment.applicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.PersonnelTrainingBase;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantTraining extends PersonnelTrainingBase {
    // <editor-fold defaultstate="collapsed" desc="Applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "ApplicantTraining_applicant";
    }
    // </editor-fold>

}
