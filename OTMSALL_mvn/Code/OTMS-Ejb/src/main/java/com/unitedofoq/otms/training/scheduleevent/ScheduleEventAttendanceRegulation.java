/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.scheduleevent;



import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"scheduleEvent"})
@Table(name= "scheventattendancereg")
public class ScheduleEventAttendanceRegulation extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="scheduleEvent">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEvent scheduleEvent;

    public ScheduleEvent getScheduleEvent() {
        return scheduleEvent;
    }

    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
        this.scheduleEvent = scheduleEvent;
    }

    public String getScheduleEventDD() {
        return "ScheduleEventAttendanceRegulation_scheduleEvent";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendPercentFrom">
    @Column
    private BigDecimal attendPercentFrom;

    public BigDecimal getAttendPercentFrom() {
        return attendPercentFrom;
    }

    public void setAttendPercentFrom(BigDecimal attendPercentFrom) {
        this.attendPercentFrom = attendPercentFrom;
    }

    public String getAttendPercentFromDD() {
        return "ScheduleEventAttendanceRegulation_attendPercentFrom";
    }
    @Transient
    private BigDecimal attendPercentFromMask;

    public BigDecimal getAttendPercentFromMask() {
        attendPercentFromMask = attendPercentFrom;
        return attendPercentFromMask;
    }

    public void setAttendPercentFromMask(BigDecimal attendPercentFromMask) {
        updateDecimalValue("attendPercentFrom", attendPercentFromMask);
    }

    public String getAttendPercentFromMaskDD() {
        return "ScheduleEventAttendanceRegulation_attendPercentFromMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendPercentTo">
    @Column
    private BigDecimal attendPercentTo;

    public BigDecimal getAttendPercentTo() {
        return attendPercentTo;
    }

    public void setAttendPercentTo(BigDecimal attendPercentTo) {
        this.attendPercentTo = attendPercentTo;
    }

    public String getAttendPercentToDD() {
        return "ScheduleEventAttendanceRegulation_attendPercentTo";
    }
    @Transient
    private BigDecimal attendPercentToMask;

    public BigDecimal getAttendPercentToMask() {
        attendPercentToMask = attendPercentTo ;
        return attendPercentToMask;
    }

    public void setAttendPercentToMask(BigDecimal attendPercentToMask) {
        updateDecimalValue("attendPercentTo", attendPercentToMask);
    }

    public String getAttendPercentToMaskDD() {
        return "ScheduleEventAttendanceRegulation_attendPercentToMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="percentageOfCourseCost">
    @Column(precision=25, scale=13)
    private BigDecimal percentageOfCourseCost;

    public BigDecimal getPercentageOfCourseCost() {
        return percentageOfCourseCost;
    }

    public void setPercentageOfCourseCost(BigDecimal percentageOfCourseCost) {
        this.percentageOfCourseCost = percentageOfCourseCost;
    }

    public String getPercentageOfCourseCostDD() {
        return "ScheduleEventAttendanceRegulation_percentageOfCourseCost";
    }
    @Transient
    private BigDecimal percentageOfCourseCostMask;

    public BigDecimal getPercentageOfCourseCostMask() {
        percentageOfCourseCostMask = percentageOfCourseCost ;
        return percentageOfCourseCostMask;
    }

    public void setPercentageOfCourseCostMask(BigDecimal percentageOfCourseCostMask) {
        updateDecimalValue("percentageOfCourseCost", percentageOfCourseCostMask);
    }

    public String getPercentageOfCourseCostMaskDD() {
        return "ScheduleEventAttendanceRegulation_percentageOfCourseCostMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendanceDeduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction attendanceDeduction;
    public Deduction getAttendanceDeduction() {
        return attendanceDeduction;
    }
    public void setAttendanceDeduction(Deduction attendanceDeduction) {
        this.attendanceDeduction = attendanceDeduction;
    }
    public String getAttendanceDeductionDD() {
        return "ScheduleEventAttendanceRegulation_attendanceDeduction";
    }

    // </editor-fold>
}