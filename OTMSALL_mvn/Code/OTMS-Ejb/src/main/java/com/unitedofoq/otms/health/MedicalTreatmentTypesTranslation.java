/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;



/**
 *
 * @author mostafa
 */
@Entity
@Table(name= "medtretypetranslation")
public class MedicalTreatmentTypesTranslation extends BaseEntityTranslation{
    // <editor-fold defaultstate="collapsed" desc="medicalStatus">
    private String medicalStatus ;

    public String getMedicalStatus() {
        return medicalStatus;
    }

    public void setMedicalStatus(String medicalStatus) {
        this.medicalStatus = medicalStatus;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Conditions">
    @Column
    private String conditions;

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }
    // </editor-fold>
}
