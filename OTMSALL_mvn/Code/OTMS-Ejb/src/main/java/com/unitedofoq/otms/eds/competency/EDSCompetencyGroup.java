/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@Table(name="CompetencyGroup")
@ParentEntity(fields={"company"})
public class EDSCompetencyGroup extends BusinessObjectBaseEntity{
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private EDSCompany company;
    
    public EDSCompany getCompany() {
        return company;
    }
    
    public void setCompany(EDSCompany company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "EDSCompetencyGroup_company";
    }
    // </editor-fold>
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
    @Translatable(translationField="descriptionTranslated")
    private String description;

    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    @OneToMany(mappedBy="group")
    private List<EDSCompetency> competencies;

    public String getNameTranslatedDD()       {    return "EDSCompetencyGroup_name";  }
    public String getDescriptionTranslatedDD(){    return "EDSCompetencyGroup_description";  }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EDSCompetency> getCompetencies() {
        return competencies;
    }

    public void setCompetencies(List<EDSCompetency> competencies) {
        this.competencies = competencies;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
}
