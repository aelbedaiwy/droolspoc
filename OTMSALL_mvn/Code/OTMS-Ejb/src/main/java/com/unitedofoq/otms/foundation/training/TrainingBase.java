/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.training;


import com.unitedofoq.fabs.core.entitybase.BaseEntity;

import com.unitedofoq.otms.training.activity.ProviderCourse;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@MappedSuperclass
public class TrainingBase extends BaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="isCatalogOrUser">
    @Column( nullable= false , length=1 )
    private String isCatalogOrUser;

    public String getIsCatalogOrUser() {
        return isCatalogOrUser;
    }

    public void setIsCatalogOrUser(String isCatalogOrUser) {
        this.isCatalogOrUser = isCatalogOrUser;
    }
    
    public String getIsCatalogOrUserDD() {
        return "TrainingBase_isCatalogOrUser";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="catalogActivity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private ProviderCourse providerCourse;

    public ProviderCourse getProviderCourse() {
        return providerCourse;
    }

    public void setProviderCourse(ProviderCourse providerCourse) {
        this.providerCourse = providerCourse;
    }
    
    public String getProviderCourseDD() {
        return "TrainingBase_providerCourse";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="catalogName">
    @Column
    private String catalogName;

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }
     public String getCatalogNameDD() {
        return "TrainingBase_catalogName";
    }

      @Transient
    private String catalogNameTranslated;

    public String getCatalogNameTranslated() {
        return catalogNameTranslated;
    }

    public void setCatalogNameTranslated(String catalogNameTranslated) {
        this.catalogNameTranslated = catalogNameTranslated;
    }

     public String getCatalogNameTranslatedDD() {
        return "TrainingBase_catalogNameTranslated";
    }
      // </editor-fold>
}
