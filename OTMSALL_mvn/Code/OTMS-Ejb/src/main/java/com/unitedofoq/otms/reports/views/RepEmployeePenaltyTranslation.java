package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeePenaltyTranslation extends RepCompanyLevelBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    private String genderDescription;

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescription() {
        return genderDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column
    private String employeeActive;

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String getEmployeeActive() {
        return employeeActive;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="canceledBy">
    @Column
    private String canceledBy;

    public void setCanceledBy(String canceledBy) {
        this.canceledBy = canceledBy;
    }

    public String getCanceledBy() {
        return canceledBy;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvedBy">
    @Column
    private String approvedBy;

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedBy() {
        return approvedBy;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="givenBy">
    @Column
    private String givenBy;

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }

    public String getGivenBy() {
        return givenBy;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="penaltyReason">
    @Column
    private String penaltyReason;

    public void setPenaltyReason(String penaltyReason) {
        this.penaltyReason = penaltyReason;
    }

    public String getPenaltyReason() {
        return penaltyReason;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="cancelReason">
    @Column
    private String cancelReason;

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelReason() {
        return cancelReason;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    private String firstName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    private String middleName;

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    private String lastName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    private String fullName;

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    private String locationDescription;

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescription() {
        return locationDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    private String hiringType;

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringType() {
        return hiringType;
    }
    // </editor-fold>

}
