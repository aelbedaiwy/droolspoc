/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.dayoff.DayOffPenalty;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"vacationGroup"})
@DiscriminatorValue("VACATIONGROUP")
public class VacationGroupPenalty extends DayOffPenalty {
    // <editor-fold defaultstate="collapsed" desc="vacationGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn (nullable=false)
    private VacationGroup vacationGroup;

    public VacationGroup getVacationGroup() {
        return vacationGroup;
    }

    public void setVacationGroup(VacationGroup vacationGroup) {
        this.vacationGroup = vacationGroup;
    }

    public String getVacationGroupDD() {
        return "VacationGroupPenalty_vacationGroup";
    }
    // </editor-fold >
    
}
