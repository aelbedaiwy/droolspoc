/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.loan.Loan;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeEosLoan extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeEosLoan_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Loan loan;

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
    
    public String getLoanDD() {
        return "EmployeeEosLoan_loan";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="calcSetteledValue">
    @Column(precision=25,scale=13)
    private BigDecimal calcSetteledValue = new BigDecimal(0);

    public BigDecimal getCalcSetteledValue() {
        return calcSetteledValue;
    }

    public void setCalcSetteledValue(BigDecimal calcSetteledValue) {
        this.calcSetteledValue = calcSetteledValue;
    }
    
    public String getCalcSetteledValueDD() {
        return "EmployeeEosLoan_calcSetteledValue";
    }
    @Transient
    private BigDecimal calcSetteledValueMask;

    public BigDecimal getCalcSetteledValueMask() {
        calcSetteledValueMask = calcSetteledValue;
        return calcSetteledValueMask;
    }

    public void setCalcSetteledValueMask(BigDecimal calcSetteledValueMask) {
        updateDecimalValue("calcSetteledValue", calcSetteledValueMask);
    }
    
    public String getCalcSetteledValueMaskDD() {
        return "EmployeeEosLoan_calcSetteledValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="setteledValue">
    @Column(precision=25,scale=13)
    private BigDecimal setteledValue = new BigDecimal(0);

    public BigDecimal getSetteledValue() {
        return setteledValue;
    }

    public void setSetteledValue(BigDecimal setteledValue) {
        this.setteledValue = setteledValue;
    }
    
    public String getSetteledValueDD() {
        return "EmployeeEosLoan_setteledValue";
    }
    @Transient
    private BigDecimal setteledValueMask;

    public BigDecimal getSetteledValueMask() {
        setteledValueMask = setteledValue;
        return setteledValueMask;
    }

    public void setSetteledValueMask(BigDecimal setteledValueMask) {
        updateDecimalValue("setteledValue", setteledValueMask);
    }
    
    public String getSetteledValueMaskDD() {
        return "EmployeeEosLoan_setteledValueMask";
    }
    // </editor-fold >
    
    //<editor-fold defaultstate="collapsed" desc="settled">
    @Column(nullable=false, length=1) // Y-N
    private String settled = "N";

    public String getSettled() {
        return settled;
    }

    public void setSettled(String settled) {
        this.settled = settled;
    }
    
    public String getSettledDD() {
        return "EmployeeEosLoan_settled";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }
   
     public String getCalculatedPeriodDD() {
        return "EmployeeEosLoan_calculatedPeriod";
    }
// </editor-fold>
}
