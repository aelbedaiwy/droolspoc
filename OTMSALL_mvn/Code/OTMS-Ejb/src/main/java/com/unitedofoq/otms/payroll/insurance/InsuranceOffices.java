/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll.insurance;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
@ParentEntity(fields="company")
public class InsuranceOffices extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "InsuranceOffices_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="address">
    @Column
    private String address;
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDD() {
        return "InsuranceOffices_address";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="telephone">
    @Column
    private String telephone;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephoneDD() {
        return "InsuranceOffices_telephone";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "InsuranceOffices_company";
    }
    //</editor-fold>

   /*
    //<editor-fold defaultstate="collapsed" desc="employeeInsurances">
    @OneToMany(mappedBy="insuranceOffices")
    private List<EmployeeInsuranceBase> employeeInsurances;

    public List<EmployeeInsuranceBase> getEmployeeInsurances() {
        return employeeInsurances;
    }

    public void setEmployeeInsurances(List<EmployeeInsuranceBase> employeeInsurances) {
        this.employeeInsurances = employeeInsurances;
    }

    public String getEmployeeInsurancesDD() {
        return "InsuranceOffices_employeeInsurances";
    }
    //</editor-fold>
    * 
    */
     //<editor-fold defaultstate="collapsed" desc="companyInsuranceNumber">
    @Column
    private Integer companyInsuranceNumber;

    public Integer getCompanyInsuranceNumber() {
        return companyInsuranceNumber;
    }

    public void setCompanyInsuranceNumber(Integer companyInsuranceNumber) {
        this.companyInsuranceNumber = companyInsuranceNumber;
    }
    public String getCompanyInsuranceNumberDD() {
        return "InsuranceOffices_companyInsuranceNumber";
    }
//</editor-fold>
   //<editor-fold defaultstate="collapsed" desc="buildingNumber">
    @Column
    private String buildingNumber;

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }
     public String getBuildingNumberDD() {
        return "InsuranceOffices_buildingNumber";
    }
      //</editor-fold>
   //<editor-fold defaultstate="collapsed" desc="street">
    @Column
    private String street;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    public String getStreetDD() {
        return "InsuranceOffices_street";
    }
          //</editor-fold>
   //<editor-fold defaultstate="collapsed" desc="village">
    @Column
    private String village;  

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }
    public String getVillageDD() {
        return "InsuranceOffices_village";
    }
    //</editor-fold>
   //<editor-fold defaultstate="collapsed" desc="division">
    @Column
    private String division;  

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
     public String getDivisionDINSDD() {
        return "InsuranceOffices_division";
    }
      //</editor-fold>
   //<editor-fold defaultstate="collapsed" desc="city">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC city;

    public UDC getCity() {
        return city;
    }

    public void setCity(UDC city) {
        this.city = city;
    }
    public String getCityDD() {
        return "InsuranceOffices_city";
    }
          //</editor-fold>

}
