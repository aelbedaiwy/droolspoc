/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.math.BigDecimal;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@MappedSuperclass
public class CostItemMember extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="CIP1">
    private BigDecimal CIP1 = BigDecimal.ZERO;

    public BigDecimal getCIP1() {
        return CIP1;
    }

    public void setCIP1(BigDecimal CIP1) {
        this.CIP1 = CIP1;
    }
    
    public String getCIP1DD(){
        return "CostItemMember_CIP1";
    }
    @Transient
    private BigDecimal CIP1Mask;

    public BigDecimal getCIP1Mask() {
        CIP1Mask = CIP1;
        return CIP1Mask;
    }

    public void setCIP1Mask(BigDecimal CIP1Mask) {
        updateDecimalValue("CIP1", CIP1Mask);
    }
    
    public String getCIP1MaskDD(){
        return "CostItemMember_CIP1Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP2">
    private BigDecimal CIP2 = BigDecimal.ZERO;

    public BigDecimal getCIP2() {
        return CIP2;
    }

    public void setCIP2(BigDecimal CIP2) {
        this.CIP2 = CIP2;
    }
    
    public String getCIP2DD(){
        return "CostItemMember_CIP2";
    }
    @Transient
    private BigDecimal CIP2Mask;

    public BigDecimal getCIP2Mask() {
        CIP2Mask = CIP2;
        return CIP2Mask;
    }

    public void setCIP2Mask(BigDecimal CIP2Mask) {
        updateDecimalValue("CIP2", CIP2Mask);
    }
    
    public String getCIP2MaskDD(){
        return "CostItemMember_CIP2Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP3">
    private BigDecimal CIP3 = BigDecimal.ZERO;

    public BigDecimal getCIP3() {
        return CIP3;
    }

    public void setCIP3(BigDecimal CIP3) {
        this.CIP3 = CIP3;
    }
    
    public String getCIP3DD(){
        return "CostItemMember_CIP3";
    }
    @Transient
    private BigDecimal CIP3Mask;

    public BigDecimal getCIP3Mask() {
        CIP3Mask =CIP3;
        return CIP3Mask;
    }

    public void setCIP3Mask(BigDecimal CIP3Mask) {
        updateDecimalValue("CIP3", CIP3Mask);
    }
    
    public String getCIP3MaskDD(){
        return "CostItemMember_CIP3Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP4">
    private BigDecimal CIP4 = BigDecimal.ZERO;

    public BigDecimal getCIP4() {
        return CIP4;
    }

    public void setCIP4(BigDecimal CIP4) {
        this.CIP4 = CIP4;
    }
    
    public String getCIP4DD(){
        return "CostItemMember_CIP4";
    }
    @Transient
    private BigDecimal CIP4Mask;

    public BigDecimal getCIP4Mask() {
        CIP4Mask = CIP4;
        return CIP4Mask;
    }

    public void setCIP4Mask(BigDecimal CIP4Mask) {
        updateDecimalValue("CIP4", CIP4Mask);
    }
    
    public String getCIP4MaskDD(){
        return "CostItemMember_CIP4Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP5">
    private BigDecimal CIP5 = BigDecimal.ZERO;

    public BigDecimal getCIP5() {
        return CIP5;
    }

    public void setCIP5(BigDecimal CIP5) {
        this.CIP5 = CIP5;
    }
    
    public String getCIP5DD(){
        return "CostItemMember_CIP5";
    }
    @Transient
    private BigDecimal CIP5Mask;

    public BigDecimal getCIP5Mask() {
        CIP5Mask = CIP5;
        return CIP5Mask;
    }

    public void setCIP5Mask(BigDecimal CIP5Mask) {
        updateDecimalValue("CIP5", CIP5Mask);
    }
    
    public String getCIP5MaskDD(){
        return "CostItemMember_CIP5Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP6">
    private BigDecimal CIP6 = BigDecimal.ZERO;

    public BigDecimal getCIP6() {
        return CIP6;
    }

    public void setCIP6(BigDecimal CIP6) {
        this.CIP6 = CIP6;
    }
    
    public String getCIP6DD(){
        return "CostItemMember_CIP6";
    }
    @Transient
    private BigDecimal CIP6Mask;

    public BigDecimal getCIP6Mask() {
        CIP6Mask = CIP6;
        return CIP6Mask;
    }

    public void setCIP6Mask(BigDecimal CIP6Mask) {
        updateDecimalValue("CIP6", CIP6Mask);
    }
    
    public String getCIP6MaskDD(){
        return "CostItemMember_CIP6Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP7">
    private BigDecimal CIP7 = BigDecimal.ZERO;

    public BigDecimal getCIP7() {
        return CIP7;
    }

    public void setCIP7(BigDecimal CIP7) {
        this.CIP7 = CIP7;
    }
    
    public String getCIP7DD(){
        return "CostItemMember_CIP7";
    }
    @Transient
    private BigDecimal CIP7Mask;

    public BigDecimal getCIP7Mask() {
        CIP7Mask= CIP7;
        return CIP7Mask;
    }

    public void setCIP7Mask(BigDecimal CIP7Mask) {
        updateDecimalValue("CIP7", CIP7Mask);
    }
    
    public String getCIP7MaskDD(){
        return "CostItemMember_CIP7Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP8">
    private BigDecimal CIP8 = BigDecimal.ZERO;

    public BigDecimal getCIP8() {
        return CIP8;
    }

    public void setCIP8(BigDecimal CIP8) {
        this.CIP8 = CIP8;
    }
    
    public String getCIP8DD(){
        return "CostItemMember_CIP8";
    }
    @Transient
    private BigDecimal CIP8Mask;

    public BigDecimal getCIP8Mask() {
        CIP8Mask =CIP8;
        return CIP8Mask;
    }

    public void setCIP8Mask(BigDecimal CIP8Mask) {
        updateDecimalValue("CIP8", CIP8Mask);
    }
    
    public String getCIP8MaskDD(){
        return "CostItemMember_CIP8Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP9">
    private BigDecimal CIP9 = BigDecimal.ZERO;

    public BigDecimal getCIP9() {
        return CIP9;
    }

    public void setCIP9(BigDecimal CIP9) {
        this.CIP9 = CIP9;
    }
    
    public String getCIP9DD(){
        return "CostItemMember_CIP9";
    }
    @Transient
    private BigDecimal CIP9Mask;

    public BigDecimal getCIP9Mask() {
        CIP9Mask = CIP9;
        return CIP9Mask;
    }

    public void setCIP9Mask(BigDecimal CIP9Mask) {
        updateDecimalValue("CIP9", CIP9Mask);
    }
    
    public String getCIP9MaskDD(){
        return "CostItemMember_CIP9Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP10">
    private BigDecimal CIP10 = BigDecimal.ZERO;

    public BigDecimal getCIP10() {
        return CIP10;
    }

    public void setCIP10(BigDecimal CIP10) {
        this.CIP10 = CIP10;
    }
    
    public String getCIP10DD(){
        return "CostItemMember_CIP10";
    }
    @Transient
    private BigDecimal CIP10Mask;

    public BigDecimal getCIP10Mask() {
        CIP10Mask = CIP10;
        return CIP10Mask;
    }

    public void setCIP10Mask(BigDecimal CIP10Mask) {
        updateDecimalValue("CIP10", CIP10Mask);
    }
    
    public String getCIP10MaskDD(){
        return "CostItemMember_CIP10Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP11">
    private BigDecimal CIP11 = BigDecimal.ZERO;

    public BigDecimal getCIP11() {
        return CIP11;
    }

    public void setCIP11(BigDecimal CIP11) {
        this.CIP11 = CIP11;
    }
    
    public String getCIP11DD(){
        return "CostItemMember_CIP11";
    }
    @Transient
    private BigDecimal CIP11Mask;

    public BigDecimal getCIP11Mask() {
        CIP11Mask = CIP11;
        return CIP11Mask;
    }

    public void setCIP11Mask(BigDecimal CIP11Mask) {
        updateDecimalValue("CIP11", CIP11Mask);
    }
    
    public String getCIP11MaskDD(){
        return "CostItemMember_CIP11Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP12">
    private BigDecimal CIP12 = BigDecimal.ZERO;

    public BigDecimal getCIP12() {
        return CIP12;
    }

    public void setCIP12(BigDecimal CIP12) {
        this.CIP12 = CIP12;
    }
    
    public String getCIP12DD(){
        return "CostItemMember_CIP12";
    }
    @Transient
    private BigDecimal CIP12Mask;

    public BigDecimal getCIP12Mask() {
        CIP12Mask = CIP12;
        return CIP12Mask;
    }

    public void setCIP12Mask(BigDecimal CIP12Mask) {
        updateDecimalValue("CIP12", CIP12Mask);
    }
    
    public String getCIP12MaskDD(){
        return "CostItemMember_CIP12Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP13">
    private BigDecimal CIP13 = BigDecimal.ZERO;

    public BigDecimal getCIP13() {
        return CIP13;
    }

    public void setCIP13(BigDecimal CIP13) {
        this.CIP13 = CIP13;
    }
    
    public String getCIP13DD(){
        return "CostItemMember_CIP13";
    }
    @Transient
    private BigDecimal CIP13Mask;

    public BigDecimal getCIP13Mask() {
        CIP13Mask = CIP13;
        return CIP13Mask;
    }

    public void setCIP13Mask(BigDecimal CIP13Mask) {
        updateDecimalValue("CIP13", CIP13Mask);
    }
    
    public String getCIP13MaskDD(){
        return "CostItemMember_CIP13Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP14">
    private BigDecimal CIP14 = BigDecimal.ZERO;

    public BigDecimal getCIP14() {
        return CIP14;
    }

    public void setCIP14(BigDecimal CIP14) {
        this.CIP14 = CIP14;
    }
    
    public String getCIP14DD(){
        return "CostItemMember_CIP14";
    }
    @Transient
    private BigDecimal CIP14Mask;

    public BigDecimal getCIP14Mask() {
        CIP14Mask = CIP14;
        return CIP14Mask;
    }

    public void setCIP14Mask(BigDecimal CIP14Mask) {
        updateDecimalValue("CIP14", CIP14Mask);
    }
    
    public String getCIP14MaskDD(){
        return "CostItemMember_CIP14Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP15">
    private BigDecimal CIP15 = BigDecimal.ZERO;

    public BigDecimal getCIP15() {
        return CIP15;
    }

    public void setCIP15(BigDecimal CIP15) {
        this.CIP15 = CIP15;
    }
    
    public String getCIP15DD(){
        return "CostItemMember_CIP15";
    }
    @Transient
    private BigDecimal CIP15Mask;

    public BigDecimal getCIP15Mask() {
        CIP15Mask = CIP15;
        return CIP15Mask;
    }

    public void setCIP15Mask(BigDecimal CIP15Mask) {
        updateDecimalValue("CIP15", CIP15Mask);
    }
    
    public String getCIP15MaskDD(){
        return "CostItemMember_CIP15Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP16">
    private BigDecimal CIP16 = BigDecimal.ZERO;

    public BigDecimal getCIP16() {
        return CIP16;
    }

    public void setCIP16(BigDecimal CIP16) {
        this.CIP16 = CIP16;
    }
    
    public String getCIP16DD(){
        return "CostItemMember_CIP16";
    }
    @Transient
    private BigDecimal CIP16Mask;

    public BigDecimal getCIP16Mask() {
        CIP16Mask = CIP16;
        return CIP16Mask;
    }

    public void setCIP16Mask(BigDecimal CIP16Mask) {
        updateDecimalValue("CIP16", CIP16Mask);
    }
    
    public String getCIP16MaskDD(){
        return "CostItemMember_CIP16Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP17">
    private BigDecimal CIP17 = BigDecimal.ZERO;

    public BigDecimal getCIP17() {
        return CIP17;
    }

    public void setCIP17(BigDecimal CIP17) {
        this.CIP17 = CIP17;
    }
    
    public String getCIP17DD(){
        return "CostItemMember_CIP17";
    }
    @Transient
    private BigDecimal CIP17Mask;

    public BigDecimal getCIP17Mask() {
        CIP17Mask = CIP17;
        return CIP17Mask;
    }

    public void setCIP17Mask(BigDecimal CIP17Mask) {
        updateDecimalValue("CIP17", CIP17Mask);
    }
    
    public String getCIP17MaskDD(){
        return "CostItemMember_CIP17Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP18">
    private BigDecimal CIP18 = BigDecimal.ZERO;

    public BigDecimal getCIP18() {
        return CIP18;
    }

    public void setCIP18(BigDecimal CIP18) {
        this.CIP18 = CIP18;
    }
    
    public String getCIP18DD(){
        return "CostItemMember_CIP18";
    }
    @Transient
    private BigDecimal CIP18Mask;

    public BigDecimal getCIP18Mask() {
        CIP18Mask = CIP18;
        return CIP18Mask;
    }

    public void setCIP18Mask(BigDecimal CIP18Mask) {
        updateDecimalValue("CIP18", CIP18Mask);
    }
    
    public String getCIP18MaskDD(){
        return "CostItemMember_CIP18Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP19">
    private BigDecimal CIP19 = BigDecimal.ZERO;

    public BigDecimal getCIP19() {
        return CIP19;
    }

    public void setCIP19(BigDecimal CIP19) {
        this.CIP19 = CIP19;
    }
    
    public String getCIP19DD(){
        return "CostItemMember_CIP19";
    }
    @Transient
    private BigDecimal CIP19Mask;

    public BigDecimal getCIP19Mask() {
        CIP19Mask = CIP19;
        return CIP19Mask;
    }

    public void setCIP19Mask(BigDecimal CIP19Mask) {
        updateDecimalValue("CIP19", CIP19Mask);
    }
    
    public String getCIP19MaskDD(){
        return "CostItemMember_CIP19Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP20">
    private BigDecimal CIP20 = BigDecimal.ZERO;

    public BigDecimal getCIP20() {
        return CIP20;
    }

    public void setCIP20(BigDecimal CIP20) {
        this.CIP20 = CIP20;
    }
    
    public String getCIP20DD(){
        return "CostItemMember_CIP20";
    }
    @Transient
    private BigDecimal CIP20Mask;

    public BigDecimal getCIP20Mask() {
        CIP20Mask = CIP20;
        return CIP20Mask;
    }

    public void setCIP20Mask(BigDecimal CIP20Mask) {
        updateDecimalValue("CIP20", CIP20Mask);
    }
    
    public String getCIP20MaskDD(){
        return "CostItemMember_CIP20Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP21">
    private BigDecimal CIP21 = BigDecimal.ZERO;

    public BigDecimal getCIP21() {
        return CIP21;
    }

    public void setCIP21(BigDecimal CIP21) {
        this.CIP21 = CIP21;
    }
    
    public String getCIP21DD(){
        return "CostItemMember_CIP21";
    }
    @Transient
    private BigDecimal CIP21Mask;

    public BigDecimal getCIP21Mask() {
        CIP21Mask =CIP21;
        return CIP21Mask;
    }

    public void setCIP21Mask(BigDecimal CIP21Mask) {
        updateDecimalValue("CIP21", CIP21Mask);
    }
    
    public String getCIP21MaskDD(){
        return "CostItemMember_CIP21Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP22">
    private BigDecimal CIP22 = BigDecimal.ZERO;

    public BigDecimal getCIP22() {
        return CIP22;
    }

    public void setCIP22(BigDecimal CIP22) {
        this.CIP22 = CIP22;
    }
    
    public String getCIP22DD(){
        return "CostItemMember_CIP22";
    }
    @Transient
    private BigDecimal CIP22Mask;

    public BigDecimal getCIP22Mask() {
        CIP22Mask =CIP22;
        return CIP22Mask;
    }

    public void setCIP22Mask(BigDecimal CIP22Mask) {
        updateDecimalValue("CIP22", CIP22Mask);
    }
    
    public String getCIP22MaskDD(){
        return "CostItemMember_CIP22Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP23">
    private BigDecimal CIP23 = BigDecimal.ZERO;

    public BigDecimal getCIP23() {
        return CIP23;
    }

    public void setCIP23(BigDecimal CIP23) {
        this.CIP23 = CIP23;
    }
    
    public String getCIP23DD(){
        return "CostItemMember_CIP23";
    }
    @Transient
    private BigDecimal CIP23Mask;

    public BigDecimal getCIP23Mask() {
        CIP23Mask = CIP23;
        return CIP23Mask;
    }

    public void setCIP23Mask(BigDecimal CIP23Mask) {
        updateDecimalValue("CIP23", CIP23Mask);
    }
    
    public String getCIP23MaskDD(){
        return "CostItemMember_CIP23Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP24">
    private BigDecimal CIP24 = BigDecimal.ZERO;

    public BigDecimal getCIP24() {
        return CIP24;
    }

    public void setCIP24(BigDecimal CIP24) {
        this.CIP24 = CIP24;
    }
    
    public String getCIP24DD(){
        return "CostItemMember_CIP24";
    }
    @Transient
    private BigDecimal CIP24Mask;

    public BigDecimal getCIP24Mask() {
        CIP24Mask = CIP24;
        return CIP24Mask;
    }

    public void setCIP24Mask(BigDecimal CIP24Mask) {
        updateDecimalValue("CIP24", CIP24Mask);
    }
    
    public String getCIP24MaskDD(){
        return "CostItemMember_CIP24Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIP25">
    private BigDecimal CIP25 = BigDecimal.ZERO;

    public BigDecimal getCIP25() {
        return CIP25;
    }

    public void setCIP25(BigDecimal CIP25) {
        this.CIP25 = CIP25;
    }
    
    public String getCIP25DD(){
        return "CostItemMember_CIP25";
    }
    @Transient
    private BigDecimal CIP25Mask;

    public BigDecimal getCIP25Mask() {
        CIP25Mask =CIP25;
        return CIP25Mask;
    }

    public void setCIP25Mask(BigDecimal CIP25Mask) {
        updateDecimalValue("CIP25", CIP25Mask);
    }
    
    public String getCIP25MaskDD(){
        return "CostItemMember_CIP25Mask";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="CIA1">
    private BigDecimal CIA1 = BigDecimal.ZERO;

    public BigDecimal getCIA1() {
        return CIA1;
    }

    public void setCIA1(BigDecimal CIA1) {
        this.CIA1 = CIA1;
    }
    
    public String getCIA1DD(){
        return "CostItemMember_CIA1";
    }
    @Transient
    private BigDecimal CIA1Mask;

    public BigDecimal getCIA1Mask() {
        CIA1Mask = CIA1;
        return CIA1Mask;
    }

    public void setCIA1Mask(BigDecimal CIA1Mask) {
        updateDecimalValue("CIA1", CIA1Mask);
    }
    
    public String getCIA1MaskDD(){
        return "CostItemMember_CIA1Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA2">
    private BigDecimal CIA2 = BigDecimal.ZERO;

    public BigDecimal getCIA2() {
        return CIA2;
    }

    public void setCIA2(BigDecimal CIA2) {
        this.CIA2 = CIA2;
    }
    
    public String getCIA2DD(){
        return "CostItemMember_CIA2";
    }
    @Transient
    private BigDecimal CIA2Mask;

    public BigDecimal getCIA2Mask() {
        CIA2Mask = CIA2;
        return CIA2Mask;
    }

    public void setCIA2Mask(BigDecimal CIA2Mask) {
        updateDecimalValue("CIA2", CIA2Mask);
    }
    
    public String getCIA2MaskDD(){
        return "CostItemMember_CIA2Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA3">
    private BigDecimal CIA3 = BigDecimal.ZERO;

    public BigDecimal getCIA3() {
        return CIA3;
    }

    public void setCIA3(BigDecimal CIA3) {
        this.CIA3 = CIA3;
    }
    
    public String getCIA3DD(){
        return "CostItemMember_CIA3";
    }
    @Transient
    private BigDecimal CIA3Mask;

    public BigDecimal getCIA3Mask() {
        CIA3Mask =CIA3;
        return CIA3Mask;
    }

    public void setCIA3Mask(BigDecimal CIA3Mask) {
        updateDecimalValue("CIA3", CIA3Mask);
    }
    
    public String getCIA3MaskDD(){
        return "CostItemMember_CIA3Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA4">
    private BigDecimal CIA4 = BigDecimal.ZERO;

    public BigDecimal getCIA4() {
        return CIA4;
    }

    public void setCIA4(BigDecimal CIA4) {
        this.CIA4 = CIA4;
    }
    
    public String getCIA4DD(){
        return "CostItemMember_CIA4";
    }
    @Transient
    private BigDecimal CIA4Mask;

    public BigDecimal getCIA4Mask() {
        CIA4Mask = CIA4;
        return CIA4Mask;
    }

    public void setCIA4Mask(BigDecimal CIA4Mask) {
        updateDecimalValue("CIA4", CIA4Mask);
    }
    
    public String getCIA4MaskDD(){
        return "CostItemMember_CIA4Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA5">
    private BigDecimal CIA5 = BigDecimal.ZERO;

    public BigDecimal getCIA5() {
        return CIA5;
    }

    public void setCIA5(BigDecimal CIA5) {
        this.CIA5 = CIA5;
    }
    
    public String getCIA5DD(){
        return "CostItemMember_CIA5";
    }
    @Transient
    private BigDecimal CIA5Mask;

    public BigDecimal getCIA5Mask() {
        CIA5Mask = CIA5;
        return CIA5Mask;
    }

    public void setCIA5Mask(BigDecimal CIA5Mask) {
        updateDecimalValue("CIA5", CIA5Mask);
    }
    
    public String getCIA5MaskDD(){
        return "CostItemMember_CIA5Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA6">
    private BigDecimal CIA6 = BigDecimal.ZERO;

    public BigDecimal getCIA6() {
        return CIA6;
    }

    public void setCIA6(BigDecimal CIA6) {
        this.CIA6 = CIA6;
    }
    
    public String getCIA6DD(){
        return "CostItemMember_CIA6";
    }
    @Transient
    private BigDecimal CIA6Mask;

    public BigDecimal getCIA6Mask() {
        CIA6Mask = CIA6;
        return CIA6Mask;
    }

    public void setCIA6Mask(BigDecimal CIA6Mask) {
        updateDecimalValue("CIA6", CIA6Mask);
    }
    
    public String getCIA6MaskDD(){
        return "CostItemMember_CIA6Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA7">
    private BigDecimal CIA7 = BigDecimal.ZERO;

    public BigDecimal getCIA7() {
        return CIA7;
    }

    public void setCIA7(BigDecimal CIA7) {
        this.CIA7 = CIA7;
    }
    
    public String getCIA7DD(){
        return "CostItemMember_CIA7";
    }
    @Transient
    private BigDecimal CIA7Mask;

    public BigDecimal getCIA7Mask() {
        CIA7Mask= CIA7;
        return CIA7Mask;
    }

    public void setCIA7Mask(BigDecimal CIA7Mask) {
        updateDecimalValue("CIA7", CIA7Mask);
    }
    
    public String getCIA7MaskDD(){
        return "CostItemMember_CIA7Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA8">
    private BigDecimal CIA8 = BigDecimal.ZERO;

    public BigDecimal getCIA8() {
        return CIA8;
    }

    public void setCIA8(BigDecimal CIA8) {
        this.CIA8 = CIA8;
    }
    
    public String getCIA8DD(){
        return "CostItemMember_CIA8";
    }
    @Transient
    private BigDecimal CIA8Mask;

    public BigDecimal getCIA8Mask() {
        CIA8Mask =CIA8;
        return CIA8Mask;
    }

    public void setCIA8Mask(BigDecimal CIA8Mask) {
        updateDecimalValue("CIA8", CIA8Mask);
    }
    
    public String getCIA8MaskDD(){
        return "CostItemMember_CIA8Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA9">
    private BigDecimal CIA9 = BigDecimal.ZERO;

    public BigDecimal getCIA9() {
        return CIA9;
    }

    public void setCIA9(BigDecimal CIA9) {
        this.CIA9 = CIA9;
    }
    
    public String getCIA9DD(){
        return "CostItemMember_CIA9";
    }
    @Transient
    private BigDecimal CIA9Mask;

    public BigDecimal getCIA9Mask() {
        CIA9Mask = CIA9;
        return CIA9Mask;
    }

    public void setCIA9Mask(BigDecimal CIA9Mask) {
        updateDecimalValue("CIA9", CIA9Mask);
    }
    
    public String getCIA9MaskDD(){
        return "CostItemMember_CIA9Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA10">
    private BigDecimal CIA10 = BigDecimal.ZERO;

    public BigDecimal getCIA10() {
        return CIA10;
    }

    public void setCIA10(BigDecimal CIA10) {
        this.CIA10 = CIA10;
    }
    
    public String getCIA10DD(){
        return "CostItemMember_CIA10";
    }
    @Transient
    private BigDecimal CIA10Mask;

    public BigDecimal getCIA10Mask() {
        CIA10Mask = CIA10;
        return CIA10Mask;
    }

    public void setCIA10Mask(BigDecimal CIA10Mask) {
        updateDecimalValue("CIA10", CIA10Mask);
    }
    
    public String getCIA10MaskDD(){
        return "CostItemMember_CIA10Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA11">
    private BigDecimal CIA11 = BigDecimal.ZERO;

    public BigDecimal getCIA11() {
        return CIA11;
    }

    public void setCIA11(BigDecimal CIA11) {
        this.CIA11 = CIA11;
    }
    
    public String getCIA11DD(){
        return "CostItemMember_CIA11";
    }
    @Transient
    private BigDecimal CIA11Mask;

    public BigDecimal getCIA11Mask() {
        CIA11Mask = CIA11;
        return CIA11Mask;
    }

    public void setCIA11Mask(BigDecimal CIA11Mask) {
        updateDecimalValue("CIA11", CIA11Mask);
    }
    
    public String getCIA11MaskDD(){
        return "CostItemMember_CIA11Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA12">
    private BigDecimal CIA12 = BigDecimal.ZERO;

    public BigDecimal getCIA12() {
        return CIA12;
    }

    public void setCIA12(BigDecimal CIA12) {
        this.CIA12 = CIA12;
    }
    
    public String getCIA12DD(){
        return "CostItemMember_CIA12";
    }
    @Transient
    private BigDecimal CIA12Mask;

    public BigDecimal getCIA12Mask() {
        CIA12Mask = CIA12;
        return CIA12Mask;
    }

    public void setCIA12Mask(BigDecimal CIA12Mask) {
        updateDecimalValue("CIA12", CIA12Mask);
    }
    
    public String getCIA12MaskDD(){
        return "CostItemMember_CIA12Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA13">
    private BigDecimal CIA13 = BigDecimal.ZERO;

    public BigDecimal getCIA13() {
        return CIA13;
    }

    public void setCIA13(BigDecimal CIA13) {
        this.CIA13 = CIA13;
    }
    
    public String getCIA13DD(){
        return "CostItemMember_CIA13";
    }
    @Transient
    private BigDecimal CIA13Mask;

    public BigDecimal getCIA13Mask() {
        CIA13Mask = CIA13;
        return CIA13Mask;
    }

    public void setCIA13Mask(BigDecimal CIA13Mask) {
        updateDecimalValue("CIA13", CIA13Mask);
    }
    
    public String getCIA13MaskDD(){
        return "CostItemMember_CIA13Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA14">
    private BigDecimal CIA14 = BigDecimal.ZERO;

    public BigDecimal getCIA14() {
        return CIA14;
    }

    public void setCIA14(BigDecimal CIA14) {
        this.CIA14 = CIA14;
    }
    
    public String getCIA14DD(){
        return "CostItemMember_CIA14";
    }
    @Transient
    private BigDecimal CIA14Mask;

    public BigDecimal getCIA14Mask() {
        CIA14Mask = CIA14;
        return CIA14Mask;
    }

    public void setCIA14Mask(BigDecimal CIA14Mask) {
        updateDecimalValue("CIA14", CIA14Mask);
    }
    
    public String getCIA14MaskDD(){
        return "CostItemMember_CIA14Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA15">
    private BigDecimal CIA15 = BigDecimal.ZERO;

    public BigDecimal getCIA15() {
        return CIA15;
    }

    public void setCIA15(BigDecimal CIA15) {
        this.CIA15 = CIA15;
    }
    
    public String getCIA15DD(){
        return "CostItemMember_CIA15";
    }
    @Transient
    private BigDecimal CIA15Mask;

    public BigDecimal getCIA15Mask() {
        CIA15Mask = CIA15;
        return CIA15Mask;
    }

    public void setCIA15Mask(BigDecimal CIA15Mask) {
        updateDecimalValue("CIA15", CIA15Mask);
    }
    
    public String getCIA15MaskDD(){
        return "CostItemMember_CIA15Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA16">
    private BigDecimal CIA16 = BigDecimal.ZERO;

    public BigDecimal getCIA16() {
        return CIA16;
    }

    public void setCIA16(BigDecimal CIA16) {
        this.CIA16 = CIA16;
    }
    
    public String getCIA16DD(){
        return "CostItemMember_CIA16";
    }
    @Transient
    private BigDecimal CIA16Mask;

    public BigDecimal getCIA16Mask() {
        CIA16Mask = CIA16;
        return CIA16Mask;
    }

    public void setCIA16Mask(BigDecimal CIA16Mask) {
        updateDecimalValue("CIA16", CIA16Mask);
    }
    
    public String getCIA16MaskDD(){
        return "CostItemMember_CIA16Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA17">
    private BigDecimal CIA17 = BigDecimal.ZERO;

    public BigDecimal getCIA17() {
        return CIA17;
    }

    public void setCIA17(BigDecimal CIA17) {
        this.CIA17 = CIA17;
    }
    
    public String getCIA17DD(){
        return "CostItemMember_CIA17";
    }
    @Transient
    private BigDecimal CIA17Mask;

    public BigDecimal getCIA17Mask() {
        CIA17Mask = CIA17;
        return CIA17Mask;
    }

    public void setCIA17Mask(BigDecimal CIA17Mask) {
        updateDecimalValue("CIA17", CIA17Mask);
    }
    
    public String getCIA17MaskDD(){
        return "CostItemMember_CIA17Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA18">
    private BigDecimal CIA18 = BigDecimal.ZERO;

    public BigDecimal getCIA18() {
        return CIA18;
    }

    public void setCIA18(BigDecimal CIA18) {
        this.CIA18 = CIA18;
    }
    
    public String getCIA18DD(){
        return "CostItemMember_CIA18";
    }
    @Transient
    private BigDecimal CIA18Mask;

    public BigDecimal getCIA18Mask() {
        CIA18Mask = CIA18;
        return CIA18Mask;
    }

    public void setCIA18Mask(BigDecimal CIA18Mask) {
        updateDecimalValue("CIA18", CIA18Mask);
    }
    
    public String getCIA18MaskDD(){
        return "CostItemMember_CIA18Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA19">
    private BigDecimal CIA19 = BigDecimal.ZERO;

    public BigDecimal getCIA19() {
        return CIA19;
    }

    public void setCIA19(BigDecimal CIA19) {
        this.CIA19 = CIA19;
    }
    
    public String getCIA19DD(){
        return "CostItemMember_CIA19";
    }
    @Transient
    private BigDecimal CIA19Mask;

    public BigDecimal getCIA19Mask() {
        CIA19Mask = CIA19;
        return CIA19Mask;
    }

    public void setCIA19Mask(BigDecimal CIA19Mask) {
        updateDecimalValue("CIA19", CIA19Mask);
    }
    
    public String getCIA19MaskDD(){
        return "CostItemMember_CIA19Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA20">
    private BigDecimal CIA20 = BigDecimal.ZERO;

    public BigDecimal getCIA20() {
        return CIA20;
    }

    public void setCIA20(BigDecimal CIA20) {
        this.CIA20 = CIA20;
    }
    
    public String getCIA20DD(){
        return "CostItemMember_CIA20";
    }
    @Transient
    private BigDecimal CIA20Mask;

    public BigDecimal getCIA20Mask() {
        CIA20Mask = CIA20;
        return CIA20Mask;
    }

    public void setCIA20Mask(BigDecimal CIA20Mask) {
        updateDecimalValue("CIA20", CIA20Mask);
    }
    
    public String getCIA20MaskDD(){
        return "CostItemMember_CIA20Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA21">
    private BigDecimal CIA21 = BigDecimal.ZERO;

    public BigDecimal getCIA21() {
        return CIA21;
    }

    public void setCIA21(BigDecimal CIA21) {
        this.CIA21 = CIA21;
    }
    
    public String getCIA21DD(){
        return "CostItemMember_CIA21";
    }
    @Transient
    private BigDecimal CIA21Mask;

    public BigDecimal getCIA21Mask() {
        CIA21Mask =CIA21;
        return CIA21Mask;
    }

    public void setCIA21Mask(BigDecimal CIA21Mask) {
        updateDecimalValue("CIA21", CIA21Mask);
    }
    
    public String getCIA21MaskDD(){
        return "CostItemMember_CIA21Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA22">
    private BigDecimal CIA22 = BigDecimal.ZERO;

    public BigDecimal getCIA22() {
        return CIA22;
    }

    public void setCIA22(BigDecimal CIA22) {
        this.CIA22 = CIA22;
    }
    
    public String getCIA22DD(){
        return "CostItemMember_CIA22";
    }
    @Transient
    private BigDecimal CIA22Mask;

    public BigDecimal getCIA22Mask() {
        CIA22Mask =CIA22;
        return CIA22Mask;
    }

    public void setCIA22Mask(BigDecimal CIA22Mask) {
        updateDecimalValue("CIA22", CIA22Mask);
    }
    
    public String getCIA22MaskDD(){
        return "CostItemMember_CIA22Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA23">
    private BigDecimal CIA23 = BigDecimal.ZERO;

    public BigDecimal getCIA23() {
        return CIA23;
    }

    public void setCIA23(BigDecimal CIA23) {
        this.CIA23 = CIA23;
    }
    
    public String getCIA23DD(){
        return "CostItemMember_CIA23";
    }
    @Transient
    private BigDecimal CIA23Mask;

    public BigDecimal getCIA23Mask() {
        CIA23Mask = CIA23;
        return CIA23Mask;
    }

    public void setCIA23Mask(BigDecimal CIA23Mask) {
        updateDecimalValue("CIA23", CIA23Mask);
    }
    
    public String getCIA23MaskDD(){
        return "CostItemMember_CIA23Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA24">
    private BigDecimal CIA24 = BigDecimal.ZERO;

    public BigDecimal getCIA24() {
        return CIA24;
    }

    public void setCIA24(BigDecimal CIA24) {
        this.CIA24 = CIA24;
    }
    
    public String getCIA24DD(){
        return "CostItemMember_CIA24";
    }
    @Transient
    private BigDecimal CIA24Mask;

    public BigDecimal getCIA24Mask() {
        CIA24Mask = CIA24;
        return CIA24Mask;
    }

    public void setCIA24Mask(BigDecimal CIA24Mask) {
        updateDecimalValue("CIA24", CIA24Mask);
    }
    
    public String getCIA24MaskDD(){
        return "CostItemMember_CIA24Mask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="CIA25">
    private BigDecimal CIA25 = BigDecimal.ZERO;

    public BigDecimal getCIA25() {
        return CIA25;
    }

    public void setCIA25(BigDecimal CIA25) {
        this.CIA25 = CIA25;
    }
    
    public String getCIA25DD(){
        return "CostItemMember_CIA25";
    }
    @Transient
    private BigDecimal CIA25Mask;

    public BigDecimal getCIA25Mask() {
        CIA25Mask =CIA25;
        return CIA25Mask;
    }

    public void setCIA25Mask(BigDecimal CIA25Mask) {
        updateDecimalValue("CIA25", CIA25Mask);
    }
    
    public String getCIA25MaskDD(){
        return "CostItemMember_CIA25Mask";
    }
    // </editor-fold>
}
