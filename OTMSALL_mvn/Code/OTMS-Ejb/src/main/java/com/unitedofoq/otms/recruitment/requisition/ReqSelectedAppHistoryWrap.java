package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ReqSelectedAppHistory")
public class ReqSelectedAppHistoryWrap extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="commment">

    private String commment;

    public String getCommment() {
        return commment;
    }

    public void setCommment(String commment) {
        this.commment = commment;
    }

    public String getCommmentDD() {
        return "RequisitionSelectedApplicantHistory_commment";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="requisitionSelectedApplicant">
    @Column(name = "ReqSelApp_DBID")
    private long requisitionSelectedApplicant_dbid;

    public long getRequisitionSelectedApplicant_dbid() {
        return requisitionSelectedApplicant_dbid;
    }

    public void setRequisitionSelectedApplicant_dbid(long requisitionSelectedApplicant_dbid) {
        this.requisitionSelectedApplicant_dbid = requisitionSelectedApplicant_dbid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="status">
    private long status_dbid;

    public long getStatus_dbid() {
        return status_dbid;
    }

    public void setStatus_dbid(long status_dbid) {
        this.status_dbid = status_dbid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="who">
    private long who_dbid;

    public long getWho_dbid() {
        return who_dbid;
    }

    public void setWho_dbid(long who_dbid) {
        this.who_dbid = who_dbid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusDate;

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusDateDD() {
        return "RequisitionSelectedApplicantHistory_statusDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="what">
    private String what;

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhatDD() {
        return "RequisitionSelectedApplicantHistory_what";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="statusResult">
    private String statusResult;

    public String getStatusResult() {
        return statusResult;
    }

    public void setStatusResult(String statusResult) {
        this.statusResult = statusResult;
    }

    public String getStatusResultDD() {
        return "RequisitionSelectedApplicantHistory_statusResult";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="exam">
    private long exam_dbid;

    public long getExam_dbid() {
        return exam_dbid;
    }

    public void setExam_dbid(long exam_dbid) {
        this.exam_dbid = exam_dbid;
    }
    //</editor-fold>
}
