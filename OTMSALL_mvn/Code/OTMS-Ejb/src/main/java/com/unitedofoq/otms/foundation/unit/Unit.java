package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.appraisal.employee.DepartmentGoal;
import com.unitedofoq.otms.foundation.NodeColor;
import com.unitedofoq.otms.foundation.UnitColor;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.costcenter.SapCostCode;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = "company")
@DiscriminatorValue("MASTER")
@ChildEntity(fields = {"positions", "sapCostCodes", "departmentGoal"})
public class Unit extends UnitBase {

    private int levelColor;
    // <editor-fold defaultstate="collapsed" desc="departmentGoal">
    @OneToMany(mappedBy = "unit")

    private List<DepartmentGoal> departmentGoal;

    public List<DepartmentGoal> getDepartmentGoal() {
        return departmentGoal;
    }

    public void setDepartmentGoal(List<DepartmentGoal> departmentGoal) {
        this.departmentGoal = departmentGoal;
    }

    public String getDepartmentGoalDD() {
        return "Unit_departmentGoal";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="childUnits">
    @OneToMany(mappedBy = "parentUnit")
    private List<Unit> childUnits;

    public List<Unit> getChildUnits() {
        return childUnits;
    }

    public void setChildUnits(List<Unit> childUnits) {
        this.childUnits = childUnits;
    }

    public String getChildUnitsDD() {
        return "Unit_childUnits";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit parentUnit;

    public Unit getParentUnit() {
        return parentUnit;
    }

    public void setParentUnit(Unit parentUnit) {
        this.parentUnit = parentUnit;
    }

    public String getParentUnitDD() {
        return "Unit_parentUnit";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positions">
    @OneToMany(mappedBy = "unit")
    private List<Position> positions;

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public String getPositionsDD() {
        return "Unit_positions";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitLevel">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UnitLevel unitLevel;

    public UnitLevel getUnitLevel() {
        return unitLevel;
    }

    public void setUnitLevel(UnitLevel unitLevel) {
        this.unitLevel = unitLevel;
    }

    public String getUnitLevelDD() {
        return "Unit_unitLevel";
    }
    // </editor-fold>

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Unit_company";
    }
    // Changes Done By Ayman
    // <editor-fold defaultstate="collapsed" desc="shortName">
    @Column
    private String shortName;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getShortNameDD() {
        return "Unit_shortName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="systemCode">
    @Column
    private String systemCode;

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getSystemCodeDD() {
        return "Unit_systemCode";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCentr">
    //ibr 10/10/2011 12:10
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterDD() {
        return "Unit_costCenter";
    }
    //ibr 10/10/2011 12:15
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "Unit_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="manger">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee manger;

    public Employee getManger() {
        return manger;
    }

    public void setManger(Employee manger) {
        this.manger = manger;
    }

    public String getMangerDD() {
        return "Unit_manger";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="createdBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee createdBy;

    public Employee getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByDD() {
        return "Unit_createdBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="creationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDateDD() {
        return "Unit_creationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="effectiveFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveFrom;

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public String getEffectiveFromDD() {
        return "Unit_effectiveFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="effectiveTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveTo;

    public Date getEffectiveTo() {
        return effectiveTo;
    }

    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }

    public String getEffectiveToDD() {
        return "Unit_effectiveTo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="updatedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee updatedBy;

    public Employee getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Employee updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedByDD() {
        return "Unit_updatedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="updateDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateDateDD() {
        return "Unit_updateDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="isChartViewable">
    private boolean isChartViewable;

    public boolean isIsChartViewable() {
        return isChartViewable;
    }

    public void setIsChartViewable(boolean isChartViewable) {
        this.isChartViewable = isChartViewable;
    }

    public String getIsChartViewableDD() {
        return "Unit_isChartViewable";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionDescription">
    private String positionDescription;

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public String getPositionDescriptionDD() {
        return "Unit_positionDescription";
    }
    // </editor-fold >

    //<editor-fold defaultstate="collapse" desc="sapCostCodes">
    @OneToMany(mappedBy = "unit")
    private List<SapCostCode> sapCostCodes;

    public List<SapCostCode> getSapCostCodes() {
        return sapCostCodes;
    }

    public void setSapCostCodes(List<SapCostCode> sapCostCodes) {
        this.sapCostCodes = sapCostCodes;
    }

    public String getSapCostCodesDD() {
        return "Unit_sapCostCodes";
    }
    //</editor-fold>

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UnitLevelIndex unitLevelindex;

    public UnitLevelIndex getUnitLevelindex() {
        return unitLevelindex;
    }

    public String getUnitLevelindexDD() {
        return "Unit_unitLevelindex";
    }

    public void setUnitLevelindex(UnitLevelIndex unitLevelindex) {
        this.unitLevelindex = unitLevelindex;
    }

    //<editor-fold defaultstate="collapsed" desc="glAccount">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }

    public String getGlAccountDD() {
        return "Unit_glAccount";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="TempByLAhmed">
    //<editor-fold defaultstate="collapsed" desc="TempEmployeePostionUnits">
    /*@OneToMany(mappedBy="unit")
     private List<TempEmployeePostionUnits> tempEmployeePostionUnits;

     public List<TempEmployeePostionUnits> getTempEmployeePostionUnits() {
     return tempEmployeePostionUnits;
     }

     public void setTempEmployeePostionUnits(List<TempEmployeePostionUnits> tempEmployeePostionUnits) {
     this.tempEmployeePostionUnits = tempEmployeePostionUnits;
     }

     public String getTempEmployeePostionUnitsDD() {
     return "Unit_tempEmployeePostionUnits";
     }

     //</editor-fold>
     //</editor-fold>
     */
    // <editor-fold defaultstate="collapsed" desc="Levels Parents">
    @Column(name = "levelZeroParent_DBID")
    private Long levelZeroParent;

    @Column(name = "levelOneParent_DBID")
    private Long levelOneParent;

    @Column(name = "levelTwoParent_DBID")
    private Long levelTwoParent;

    @Column(name = "levelThreeParent_DBID")
    private Long levelThreeParent;

    @Column(name = "levelFourParent_DBID")
    private Long levelFourParent;

    @Column(name = "levelFiveParent_DBID")
    private Long levelFiveParent;

    @Column(name = "levelSixParent_DBID")
    private Long levelSixParent;

    @Column(name = "levelSevenParent_DBID")
    private Long levelSevenParent;

    @Column(name = "levelEightParent_DBID")
    private Long levelEightParent;

    @Column(name = "levelNineParent_DBID")
    private Long levelNineParent;

    public Long getLevelEightParent() {
        return levelEightParent;
    }

    public void setLevelEightParent(Long levelEightParent) {
        this.levelEightParent = levelEightParent;
    }

    public Long getLevelFiveParent() {
        return levelFiveParent;
    }

    public void setLevelFiveParent(Long levelFiveParent) {
        this.levelFiveParent = levelFiveParent;
    }

    public Long getLevelFourParent() {
        return levelFourParent;
    }

    public void setLevelFourParent(Long levelFourParent) {
        this.levelFourParent = levelFourParent;
    }

    public Long getLevelNineParent() {
        return levelNineParent;
    }

    public void setLevelNineParent(Long levelNineParent) {
        this.levelNineParent = levelNineParent;
    }

    public Long getLevelOneParent() {
        return levelOneParent;
    }

    public void setLevelOneParent(Long levelOneParent) {
        this.levelOneParent = levelOneParent;
    }

    public Long getLevelSevenParent() {
        return levelSevenParent;
    }

    public void setLevelSevenParent(Long levelSevenParent) {
        this.levelSevenParent = levelSevenParent;
    }

    public Long getLevelSixParent() {
        return levelSixParent;
    }

    public void setLevelSixParent(Long levelSixParent) {
        this.levelSixParent = levelSixParent;
    }

    public Long getLevelThreeParent() {
        return levelThreeParent;
    }

    public void setLevelThreeParent(Long levelThreeParent) {
        this.levelThreeParent = levelThreeParent;
    }

    public Long getLevelTwoParent() {
        return levelTwoParent;
    }

    public void setLevelTwoParent(Long levelTwoParent) {
        this.levelTwoParent = levelTwoParent;
    }

    public Long getLevelZeroParent() {
        return levelZeroParent;
    }

    public void setLevelZeroParent(Long levelZeroParent) {
        this.levelZeroParent = levelZeroParent;
    }

    // </editor-fold>
    @Override
    public void PrePersist() {
        super.PrePersist();
        if (getLevelZeroParent() == null || getLevelZeroParent() == 0) {
            setLevelZeroParent(getDbid());
        } else if (getLevelOneParent() == null || getLevelOneParent() == 0) {
            setLevelOneParent(getDbid());
        } else if (getLevelTwoParent() == null || getLevelTwoParent() == 0) {
            setLevelTwoParent(getDbid());
        } else if (getLevelThreeParent() == null || getLevelThreeParent() == 0) {
            setLevelThreeParent(getDbid());
        } else if (getLevelFourParent() == null || getLevelFourParent() == 0) {
            setLevelFourParent(getDbid());
        } else if (getLevelFiveParent() == null || getLevelFiveParent() == 0) {
            setLevelFiveParent(getDbid());
        } else if (getLevelSixParent() == null || getLevelSixParent() == 0) {
            setLevelSixParent(getDbid());
        } else if (getLevelSevenParent() == null || getLevelSevenParent() == 0) {
            setLevelSevenParent(getDbid());
        } else if (getLevelEightParent() == null || getLevelEightParent() == 0) {
            setLevelEightParent(getDbid());
        } else if (getLevelNineParent() == null || getLevelNineParent() == 0) {
            setLevelNineParent(getDbid());
        }
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "Unit_legalEntity";
    }

    public int getLevelColor() {
        return levelColor;
    }

    public void setLevelColor(int levelColor) {
        this.levelColor = levelColor;
    }

    public String getLevelColorDD() {
        return "Unit_levelColor";
    }
    
    // <editor-fold defaultstate="collapsed" desc="nodeLevel">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UnitColor nodeLevel;

    public UnitColor getNodeLevel() {
        return nodeLevel;
    }

    public void setNodeLevel(UnitColor nodeLevel) {
        this.nodeLevel = nodeLevel;
    }
    
    public String getNodeLevelDD() {
        return "Unit_nodeLevel";
    }
    // </editor-fold>
    
}
