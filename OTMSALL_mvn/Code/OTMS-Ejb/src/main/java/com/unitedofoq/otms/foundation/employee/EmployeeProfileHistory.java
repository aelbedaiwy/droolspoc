package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ComputedField;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeProfileHistory extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeProfileHistory_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actionDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date actionDate;

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public String getActionDateDD() {
        return "EmployeeProfileHistory_actionDate";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "EmployeeProfileHistory_position";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "costCenter_dbid")
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterDD() {
        return "EmployeeProfileHistory_costCenter";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "payGrade_dbid")
    private PayGrade payGrade;

    public PayGrade getPayGrade() {
        return payGrade;
    }

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public String getPayGradeDD() {
        return "EmployeeProfileHistory_payGrade";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "location_dbid")
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "EmployeeProfileHistory_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reason">
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonDD() {
        return "EmployeeProfileHistory_reason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="HiringType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC HiringType;

    public UDC getHiringType() {
        return HiringType;
    }

    public void setHiringType(UDC HiringType) {
        this.HiringType = HiringType;
    }

    public String getHiringTypeDD() {
        return "EmployeeProfileHistory_HiringType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @Transient
    @ComputedField(filterLHSExpression = {"position.unit"})
    Unit unit;

    public Unit getUnit() {
        try {
            if (getPosition() != null) {
                return getPosition().getUnit();
            }
            return null;
        } catch (Exception Ex) {
            return null;
        }

    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitDD() {
        return "EmployeeProfileHistory_unit";
    }
    @Transient
    @ComputedField(filterLHSExpression = {"position.unit"})
    Unit unitFilter;

    public Unit getUnitFilter() {
        return unitFilter;
    }

    public void setUnitFilter(Unit unitFilter) {
        this.unitFilter = unitFilter;
    }

    @Override
    protected void PostLoad() {
        super.PostLoad();
        unitFilter = getUnit();
    }

    public String getUnitFilterDD() {
        return "EmployeeProfileHistory_unit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "EmployeeProfileHistory_type";
    }

    // <editor-fold defaultstate="collapsed" desc="driverType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC driverType;

    public UDC getDriverType() {
        return driverType;
    }

    public void setDriverType(UDC driverType) {
        this.driverType = driverType;
    }

    public String getDriverTypeDD() {
        return "EmployeeProfileHistory_driverType";
    }

}
