/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields={"company"})
public class MedicalProvider extends BaseEntity {
//<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "MedicalProvider_code";
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="providerName">
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
    public String getProviderNameDD() {
        return "MedicalProvider_providerName";
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="city">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC city;

    public UDC getCity() {
        return city;
    }

    public void setCity(UDC city) {
        this.city = city;
    }

    public String getCityDD() {
        return "MedicalProvider_city";
    }
//</editor-fold>
   
// <editor-fold defaultstate="collapsed" desc="medicalGroup"> 
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC medicalGroup;

    public UDC getMedicalGroup() {
        return medicalGroup;
    }

    public void setMedicalGroup(UDC medicalGroup) {
        this.medicalGroup = medicalGroup;
    }

   
    public String getMedicalGroupDD() {
        return "MedicalProvider_medicalGroup";
    }


 // </editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="contractNumber">
    private String contractNumber;

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }
   
    public String getContractNumberDD() {
        return "MedicalProvider_contractNumber";
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="startDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getStartDateDD() {
        return "MedicalProvider_startDate";
    }
    //</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="endDate">
    @Temporal(TemporalType.TIMESTAMP)
   private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public String getEndDateDD() {
        return "MedicalProvider_endDate";
    } 
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="contactsCoordinator">
private String contactsCoordinator;

    public String getContactsCoordinator() {
        return contactsCoordinator;
    }

    public void setContactsCoordinator(String contactsCoordinator) {
        this.contactsCoordinator = contactsCoordinator;
    }

    public String getContactsCoordinatorDD() {
        return "MedicalProvider_contactsCoordinator";
    }
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc="phoneNumber">
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getPhoneNumberDD() {
        return "MedicalProvider_phoneNumber";
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="email">    
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getEmailDD() {
        return "MedicalProvider_email";
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
  private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }
  
     public String getStatusDD() {
        return "MedicalProvider_status";
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="company">
@ManyToOne (fetch = javax.persistence.FetchType.LAZY)
@JoinColumn(nullable=false)
     private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "MedicalProvider_company";
    }
//</editor-fold>
    
    

    
    


}

