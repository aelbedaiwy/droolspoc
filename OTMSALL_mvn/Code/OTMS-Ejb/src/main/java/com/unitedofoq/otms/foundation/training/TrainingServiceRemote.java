/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author arezk
 */
public interface TrainingServiceRemote {

    public com.unitedofoq.fabs.core.function.OFunctionResult finish(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, OUser loggedUser);
    public com.unitedofoq.fabs.core.function.OFunctionResult announce(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult postCreateProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult closeProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult announceProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult finishProviderTainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult cancelEmployeeCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult postEmployeeCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateProviderCourseInfo(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateProviderTrainingPlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public com.unitedofoq.fabs.core.function.OFunctionResult reserveCourse(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, OUser loggedUser);
    public com.unitedofoq.fabs.core.function.OFunctionResult setPlanDuration(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, OUser loggedUser);
    public com.unitedofoq.fabs.core.function.OFunctionResult validatePlannedPCI(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, OUser loggedUser);
    public com.unitedofoq.fabs.core.function.OFunctionResult validateNewProviderCourseInfo(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult setCoursesCreated(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateEmployeeCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public com.unitedofoq.fabs.core.function.OFunctionResult employeeProviderUE(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult scheduledJobReservedCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult scheduledJobFinishCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult scheduledJobClosePlan(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult scheduledJobExpireCourse(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public com.unitedofoq.fabs.core.function.OFunctionResult validateUpdateProviderCourseInfo(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);
}