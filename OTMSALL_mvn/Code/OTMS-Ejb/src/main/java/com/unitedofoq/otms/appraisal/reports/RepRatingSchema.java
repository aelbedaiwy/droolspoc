/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepRatingSchema extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepRatingSchema_dsDbid";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ratingDescription">
    @Translatable(translationField = "ratingDescriptionTranslated")
    private String ratingDescription;

    public String getRatingDescription() {
        return ratingDescription;
    }

    public void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription;
    }
    
    public String getRatingDescriptionDD() {
        return "RepRatingSchema_ratingDescription";
    }
    
    @Transient
    @Translation(originalField = "ratingDescription")
    private String ratingDescriptionTranslated;

    public String getRatingDescriptionTranslated() {
        return ratingDescriptionTranslated;
    }

    public void setRatingDescriptionTranslated(String ratingDescriptionTranslated) {
        this.ratingDescriptionTranslated = ratingDescriptionTranslated;
    }
    
    public String getRatingDescriptionTranslatedDD() {
        return "RepRatingSchema_ratingDescription";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="levelDescription">
    @Translatable(translationField = "levelDescriptionTranslated")
    private String levelDescription;

    public String getLevelDescription() {
        return levelDescription;
    }

    public void setLevelDescription(String levelDescription) {
        this.levelDescription = levelDescription;
    }
    
    public String getLevelDescriptionDD() {
        return "RepRatingSchema_levelDescription";
    }
    
    @Transient
    @Translation(originalField = "levelDescription")
    private String levelDescriptionTranslated;

    public String getLevelDescriptionTranslated() {
        return levelDescriptionTranslated;
    }

    public void setLevelDescriptionTranslated(String levelDescriptionTranslated) {
        this.levelDescriptionTranslated = levelDescriptionTranslated;
    }
    
    public String getLevelDescriptionTranslatedDD() {
        return "RepRatingSchema_levelDescription";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="score">
    private BigDecimal score;

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }
    
    public String getScoreDD() {
        return "RepRatingSchema_score";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="minimumScore">
    private BigDecimal minimumScore;

    public BigDecimal getMinimumScore() {
        return minimumScore;
    }

    public void setMinimumScore(BigDecimal minimumScore) {
        this.minimumScore = minimumScore;
    }
    
    public String getMinimumScoreDD() {
        return "RepRatingSchema_minimumScore";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="maximumScore">
    private BigDecimal maximumScore;

    public BigDecimal getMaximumScore() {
        return maximumScore;
    }

    public void setMaximumScore(BigDecimal maximumScore) {
        this.maximumScore = maximumScore;
    }
    
    public String getMaximumScoreDD() {
        return "RepRatingSchema_maximumScore";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyIDDD() {
        return "RepRatingSchema_companyID";
    }
    // </editor-fold>
}
