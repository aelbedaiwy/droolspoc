/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author mostafa
 */
@Entity
@ReadOnly
@Table(name="repmedicalclaimtranslation")
public class RepMedicalClaimTranslation extends BaseEntityTranslation {
    
    //<editor-fold defaultstate="collapsed" desc="dependenceName">
    @Column

    private String dependenceName;

    public String getDependenceName() {
        return dependenceName;
    }

    public void setDependenceName(String dependenceName) {
        this.dependenceName = dependenceName;
    }
    public String getDependenceNameDD() {
        return "RepMedicalClaimTranslation_dependenceName";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantMedicalCategoryDescription">
@Column(name= "depmedcatdesc")

private String dependantMedicalCategoryDescription;

    public String getDependantMedicalCategoryDescription() {
        return dependantMedicalCategoryDescription;
    }

    public void setDependantMedicalCategoryDescription(String dependantMedicalCategoryDescription) {
        this.dependantMedicalCategoryDescription = dependantMedicalCategoryDescription;
    }
    public String getDependantMedicalCategoryDescriptionDD() {
        return "RepMedicalClaimTranslation_dependantMedicalCategoryDescription";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependantMedicalGlassesStatus">
@Column

private String dependantMedicalGlassesStatus;

    public String getDependantMedicalGlassesStatus() {
        return dependantMedicalGlassesStatus;
    }

    public void setDependantMedicalGlassesStatus(String dependantMedicalGlassesStatus) {
        this.dependantMedicalGlassesStatus = dependantMedicalGlassesStatus;
    }

    public String getDependantMedicalGlassesStatusDD() {
        return "RepMedicalClaimTranslation_dependantMedicalGlassesStatus";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="beneficiary">
@Column

private String beneficiary;

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }
    public String getBeneficiaryDD() {
        return "RepMedicalClaimTranslation_beneficiary";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalGroup">
@Column

private String medicalGroup;

    public String getMedicalGroup() {
        return medicalGroup;
    }

    public void setMedicalGroup(String medicalGroup) {
        this.medicalGroup = medicalGroup;
    }
    public String getMedicalGroupDD() {
        return "RepMedicalClaimTranslation_medicalGroup";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalStatus">
@Column

private String medicalStatus;

    public String getMedicalStatus() {
        return medicalStatus;
    }

    public void setMedicalStatus(String medicalStatus) {
        this.medicalStatus = medicalStatus;
    }
    public String getMedicalStatusDD() {
        return "RepMedicalClaimTranslation_medicalStatus";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalTreatmentFinancialOperations">
@Column(name= "medtrefinope")

private String medicalTreatmentFinancialOperations;

    public String getMedicalTreatmentFinancialOperations() {
        return medicalTreatmentFinancialOperations;
    }

    public void setMedicalTreatmentFinancialOperations(String medicalTreatmentFinancialOperations) {
        this.medicalTreatmentFinancialOperations = medicalTreatmentFinancialOperations;
    }
    public String getMedicalTreatmentFinancialOperationsDD() {
        return "RepMedicalClaimTranslation_medicalTreatmentFinancialOperations";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeMedicalGlassesStatus">
@Column

private String employeeMedicalGlassesStatus;

    public String getEmployeeMedicalGlassesStatus() {
        return employeeMedicalGlassesStatus;
    }

    public void setEmployeeMedicalGlassesStatus(String employeeMedicalGlassesStatus) {
        this.employeeMedicalGlassesStatus = employeeMedicalGlassesStatus;
    }
    public String getEmployeeMedicalGlassesStatusDD() {
        return "RepMedicalClaimTranslation_employeeMedicalGlassesStatus";
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalCategoryDescription">
@Column

private String medicalCategoryDescription;

    public String getMedicalCategoryDescription() {
        return medicalCategoryDescription;
    }

    public void setMedicalCategoryDescription(String medicalCategoryDescription) {
        this.medicalCategoryDescription = medicalCategoryDescription;
    }
    public String getMedicalCategoryDescriptionDD() {
        return "RepMedicalClaimTranslation_medicalCategoryDescription";
    }        
    
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="categoriesGender">
@Column

private String categoriesGender;

    public String getCategoriesGender() {
        return categoriesGender;
    }

    public void setCategoriesGender(String categoriesGender) {
        this.categoriesGender = categoriesGender;
    }

     public String getCategoriesGenderDD() {
        return "RepMedicalClaimTranslation_categoriesGender";
    }   

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="categoriesMaritalStatus">
@Column

private String categoriesMaritalStatus;

    public String getCategoriesMaritalStatus() {
        return categoriesMaritalStatus;
    }

    public void setCategoriesMaritalStatus(String categoriesMaritalStatus) {
        this.categoriesMaritalStatus = categoriesMaritalStatus;
    }
    public String getCategoriesMaritalStatusDD() {
        return "RepMedicalClaimTranslation_categoriesMaritalStatus";
    }

//</editor-fold>



}