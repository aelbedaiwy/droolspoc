package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

@Entity
public class ImportEmployeeVacation extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="empCode">

    private String empCode;

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getEmpCodeDD() {
        return "ImportEmployeeVacation_empCode";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="vacCode">
    private String vacCode;

    public String getVacCode() {
        return vacCode;
    }

    public void setVacCode(String vacCode) {
        this.vacCode = vacCode;
    }

    public String getVacCodeDD() {
        return "ImportEmployeeVacation_vacCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="startDate">
    private String startDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "ImportEmployeeVacation_startDate";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="endDate">
    private String endDate;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "ImportEmployeeVacation_endDate";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="vacPeriod">
    private String vacPeriod;

    public String getVacPeriod() {
        return vacPeriod;
    }

    public void setVacPeriod(String vacPeriod) {
        this.vacPeriod = vacPeriod;
    }

    public String getVacPeriodDD() {
        return "ImportEmployeeVacation_vacPeriod";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="readyToPost">
    private String readyToPost;

    public String getReadyToPost() {
        return readyToPost;
    }

    public void setReadyToPost(String readyToPost) {
        this.readyToPost = readyToPost;
    }

    public String getReadyToPostDD() {
        return "ImportEmployeeVacation_readyToPost";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "ImportEmployeeVacation_comments";
    }
    //</editor-fold >
}
