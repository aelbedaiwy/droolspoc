/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.job;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import com.unitedofoq.otms.foundation.competency.CompetencyLevel;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@Entity
//@ChildEntity(fields={"levels"})
//@ParentEntity(fields="competencyGroup")
@ParentEntity(fields="job")
public class JobCompetency extends BusinessObjectBaseEntity    {
 // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Job job;

    public Job getJob() {
        return job;
    }
    public String getJobDD() {
        return "JobCompetency_job";
    }
    public void setJob(Job job) {
        this.job = job;
    }
 // </editor-fold>
 // <editor-fold defaultstate="collapsed" desc="competencyLevel">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private CompetencyLevel competencyLevel;

    public CompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }
    public String getCompetencyLevelDD() {
        return "JobCompetency_competencyLevel";
    }
    public void setCompetencyLevel(CompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }
    // </editor-fold>
 // <editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }
    public String getCompetencyDD() {
        return "JobCompetency_competency";
    }
    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    // </editor-fold>
    
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeKPA_m2mCheck";
    }

    @PostLoad
    public void postLoad(){
        setM2mCheck(true);
    }
}
