/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import com.unitedofoq.otms.foundation.competency.Competency;
import com.unitedofoq.otms.foundation.competency.CompetencyLevel;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@Entity
//@ChildEntity(fields={"levels"})
//@ParentEntity(fields="competencyGroup")
@ParentEntity(fields={"position"})
public class PositionCompetency extends BusinessObjectBaseEntity  {
   // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

  public String getPositionDD() {
        return "PositionCompetency_position";
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="competencyLevel">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private CompetencyLevel competencyLevel;

    public CompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }
    public String getCompetencyLevelDD() {
        return "PositionCompetency_competencyLevel";
    }
    public void setCompetencyLevel(CompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }
    public String getCompetencyDD() {
        return "PositionCompetency_competency";
    }
    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    // </editor-fold>
    
    
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }
    
    public String getM2mCheckDD() {
        return "PositionCompetency_m2mCheck";
    }
    
    
    @Column(precision = 25, scale = 13)
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }
    
    public String getWeightDD() {
        return "PositionCompetency_weight";
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule rating;

    public ScaleTypeRule getRating() {
        return rating;
    }
    
    public String getRatingDD() {
        return "PositionCompetency_rating";
    }

    public void setRating(ScaleTypeRule rating) {
        this.rating = rating;
    }

    
    
    @Override
    public void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
}
