package com.unitedofoq.otms.foundation.system;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields = {"systemLookUp"})
public class SystemLookUpDetail extends SystemLookUpBase {

    // <editor-fold defaultstate="collapsed" desc="systemLookUp">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private SystemLookUp systemLookUp;

    public void setSystemLookUp(SystemLookUp systemLookUp) {
        this.systemLookUp = systemLookUp;
    }

    public SystemLookUp getSystemLookUp() {
        return systemLookUp;
    }

    public String getSystemLookUpDD() {
        return "SystemLookUpDetail_systemLookUp";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="date">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDateDD() {
        return "SystemLookUpDetail_date";
    }
    // </editor-fold>
}
