
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import javax.persistence.*;

@Entity
public class RepProviderEvaluat extends RepEmployeeBase  {

    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private Long dsDbid;

    public Long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(Long dsDbid) {
        this.dsDbid = dsDbid;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="providerName">
    @Column
    @Translatable(translationField = "providerNameTranslated")
    private String providerName;

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderName() {
        return providerName;
    }

    public String getProviderNameDD() {
        return "RepProviderEvaluat_providerName";
    }
    
    @Transient
    @Translation(originalField = "providerName")
    private String providerNameTranslated;

    public String getProviderNameTranslated() {
        return providerNameTranslated;
    }

    public void setProviderNameTranslated(String providerNameTranslated) {
        this.providerNameTranslated = providerNameTranslated;
    }

    public String getProviderNameTranslatedDD() {
        return "RepProviderEvaluat_providerName";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }

    public String getCourseClassDD() {
        return "RepProviderEvaluat_courseClass";
    }
    
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;

    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }

    public String getCourseClassTranslatedDD() {
        return "RepProviderEvaluat_courseClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="course">
    @Column
    @Translatable(translationField = "courseTranslated")
    private String course;

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourse() {
        return course;
    }

    public String getCourseDD() {
        return "RepProviderEvaluat_course";
    }
    
    @Transient
    @Translation(originalField = "course")
    private String courseTranslated;

    public String getCourseTranslated() {
        return courseTranslated;
    }

    public void setCourseTranslated(String courseTranslated) {
        this.courseTranslated = courseTranslated;
    }

    public String getCourseTranslatedDD() {
        return "RepProviderEvaluat_courses";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="question">
    @Column
    private String question;

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public String getQuestionDD() {
        return "RepProviderEvaluat_question";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="answer">
    @Column
    private String answer;

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public String getAnswerDD() {
        return "RepProviderEvaluat_answer";
    }
    // </editor-fold>
}