package com.unitedofoq.otms.core.report;

import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.List;
import javax.ejb.Local;

@Local
public interface RepTemplFilterFieldsBeanLocal {

    public List<RepFilterField> getDataSetFilterFields(long dsDBID, OUser loggedUser);

    public List<Object> getReportFilterScreenDDs(OUser loggedUser);
}
