
package com.unitedofoq.otms.reports.views;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
public class RepEmployeeVacationHistory extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "RepEmployeeVacationHistory_startDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getEndDateDD() {
        return "RepEmployeeVacationHistory_endDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="period">
    @Column
    private BigDecimal period;

    public void setPeriod(BigDecimal period) {
        this.period = period;
    }

    public BigDecimal getPeriod() {
        return period;
    }

    public String getPeriodDD() {
        return "RepEmployeeVacationHistory_period";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="vacation">
    @Column
    private String vacation;

    public void setVacation(String vacation) {
        this.vacation = vacation;
    }

    public String getVacation() {
        return vacation;
    }

    public String getVacationDD() {
        return "RepEmployeeVacationHistory_vacation";
    }
    // </editor-fold>

}
