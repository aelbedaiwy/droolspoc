package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.training.EDSLearningResource;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields="competency")
public class EDSCompetencyLearningResource extends BaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetency competency;
    public String getCompetencyDD(){ return "EDSCompetencyLearningResource_competency";  }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSLearningResource learningResource;
    public String getLearningResourceDD(){ return "EDSCompetencyLearningResource_learningResource";  }
    public EDSCompetency getCompetency() {
        return competency;
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }

    public EDSLearningResource getLearningResource() {
        return learningResource;
    }

    public void setLearningResource(EDSLearningResource learningResource) {
        this.learningResource = learningResource;
    }
}
