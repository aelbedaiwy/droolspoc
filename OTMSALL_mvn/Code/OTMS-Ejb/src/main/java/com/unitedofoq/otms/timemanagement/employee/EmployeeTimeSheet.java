package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"employee"})
public class EmployeeTimeSheet extends TMEmpAnnualBase {
    
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeTimeSheet_employee";
    }
    //</editor-fold>
    // field recordNum to differentiate between the 1st record holding the day natures and the 2nd for the number of hours
    //<editor-fold defaultstate="collapsed" desc="recordNum">
    private int recordNum; // 1: First Record, 2: Second Record

    public int getRecordNum() {
        return recordNum;
    }

    public void setRecordNum(int recordNum) {
        this.recordNum = recordNum;
    }
    
    public String getRecordNumDD() {
        return "EmployeeTimeSheet_recordNum";
    }
    //</editor-fold>
    
}