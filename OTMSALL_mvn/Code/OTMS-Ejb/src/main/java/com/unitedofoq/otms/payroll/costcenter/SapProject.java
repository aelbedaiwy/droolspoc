/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author A.Alaa
 */
@Entity
@ParentEntity (fields="company")
public class SapProject extends BaseEntity {
    
    // <editor-fold defaultstate="collapse" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "SapProject_code";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapse" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionDD() {
        return "SapProject_description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="wbs">
    @OneToMany(mappedBy="sapProject")
    private List<Wbs> wbs;

    public List<Wbs> getWbs() {
        return wbs;
    }

    public void setWbs(List<Wbs> wbs) {
        this.wbs = wbs;
    }
    public String getWbsDD() {
        return "SapProject_wbs";
    }

    
    

    
    //</editor-fold>  
    // <editor-fold defaultstate="collapse" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "SapProject_company";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="employeeSapIntegerationProject">
    //@OneToMany(fetch = javax.persistence.FetchType.LAZY)
    /*private List<EmployeeSapIntegerationProject> employeeSapIntegerationProject;

    
   

    public List<EmployeeSapIntegerationProject> getEmployeeSapIntegerationProject() {
        return employeeSapIntegerationProject;
    }

    public void setEmployeeSapIntegerationProject(List<EmployeeSapIntegerationProject> employeeSapIntegerationProject) {
        this.employeeSapIntegerationProject = employeeSapIntegerationProject;
    }
    public String getEmployeeSapIntegerationProjectDD() {
        return "SapProject_employeeSapIntegerationProject";
    }*/
 //</editor-fold>
}
