package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.eds.competency.EDSCompetency;
import com.unitedofoq.otms.eds.competency.EDSCompetencyLevel;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class ApplicantQuestionnaire extends BaseEntity {
    
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }
    
    public String getApplicantDD() {
        return "ApplicantQuestionnaire_applicant";
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    private String comment;
    public String getComment() {
        return comment;
    }
    
    public String getCommentDD() {
        return "ApplicantQuestionnaire_comment";
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    EDSCompetency competency;
    
    public EDSCompetency getCompetency() {
        return competency;
    }
    
    public String getCompetencyDD() {
        return "ApplicantQuestionnaire_competency";
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }

    @Transient
    private boolean level;
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    @JoinColumn
//    private EDSIDPBusinessOutcomeForJobGap jobGap;

  

//    public EDSIDPBusinessOutcomeForJobGap getJobGap() {
//        return jobGap;
//    }
//
//    public void setJobGap(EDSIDPBusinessOutcomeForJobGap jobGap) {
//        this.jobGap = jobGap;
//    }

    @Transient
    private int levelOrder;
    

   
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetencyLevel competencyLevel;
    

    public EDSCompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }
    
    public String getCompetencyLevelDD() {
        return "ApplicantQuestionnaire_competencyLevel";
    }

    public void setCompetencyLevel(EDSCompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }

    public void setLevelOrder(int levelOrder) {
        this.levelOrder = levelOrder;
    }

    public int getLevelOrder() {
        levelOrder = competencyLevel.getSortIndex();
        return levelOrder;
    }
    
    public String getLevelOrderDD() {        
        return "ApplicantQuestionnaire_levelOrder";
    }

    public boolean isLevel() {
        return level;
    }
    
    public String getLevelDD() {
        return "ApplicantQuestionnaire_level";
    }

    public void setLevel(boolean level) {
        this.level = level;
    }
    @Override
    public  void PostLoad(){
        super.PostLoad();
        level = true;
    }
}
