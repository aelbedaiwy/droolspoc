
package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"position"})
public class PositionCertificateNew extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
    public String getPositionDD() {
        return "PositionCertificateNew_position";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="certificate">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC certificate;

    public void setCertificate(UDC certificate) {
        this.certificate = certificate;
    }

    public UDC getCertificate() {
        return certificate;
    }

    public String getCertificateDD() {
        return "PositionCertificateNew_certificate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="otherCertificate">
    @Column
    private String otherCertificate;

    public void setOtherCertificate(String otherCertificate) {
        this.otherCertificate = otherCertificate;
    }

    public String getOtherCertificate() {
        return otherCertificate;
    }

    public String getOtherCertificateDD() {
        return "PositionCertificateNew_otherCertificate";
    }
    // </editor-fold>

}
