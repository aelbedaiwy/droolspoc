package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.recruitment.onlineexam.Exam;
import com.unitedofoq.otms.recruitment.onlineexam.PositionExam;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantScheduleExam extends ApplicantScheduleBase {

    // <editor-fold defaultstate="collapsed" desc="exam">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Exam exam;

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public String getExamDD() {
        return "ApplicantScheduleTests_exam";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "ApplicantScheduleTests_endDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="examURL">
    private String examURL;

    public String getExamURL() {
        return examURL;
    }

    public void setExamURL(String examURL) {
        this.examURL = examURL;
    }

    public String getExamURLDD() {
        return "ApplicantScheduleExam_examURL";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionExam">
    @ManyToOne(fetch = FetchType.LAZY)
    private PositionExam positionExam;

    public PositionExam getPositionExam() {
        return positionExam;
    }

    public void setPositionExam(PositionExam positionExam) {
        this.positionExam = positionExam;
    }

    public String getPositionExamDD() {
        return "ApplicantScheduleExam_positionExam";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taken">
    private boolean taken;

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public String getTakenDD() {
        return "ApplicantScheduleExam_taken";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="opened">
    private boolean opened;

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public String getOpenedDD() {
        return "ApplicantScheduleExam_opened";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="duration">
    private Integer duration;

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDurationDD() {
        return "ApplicantScheduleExam_duration";
    }
    // </editor-fold>
}
