/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.payroll.loan.Loan;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arezk
 */

@Entity
public class UnitPostpone extends BaseEntity{
    
    // <editor-fold defaultstate="collapsed" desc="dateFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }
    
    public String getDateFromDD() {
        return "UnitPostpone_dateFrom";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="dateTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
    
    public String getDateToDD() {
        return "UnitPostpone_dateTo";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="unit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Unit unit;
    
    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
    
    public String getUnitDD(){
        return "UnitPostpone_unit";
    }
    // </editor-fold> 
    
    // <editor-fold defaultstate="collapsed" desc="loanDescription">
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Loan loan;

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
    public String getLoanDD(){
        return "UnitPostpone_loan";
    }
    // </editor-fold> 
}
