package com.unitedofoq.otms.personnel.absence;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.IncentivesAbsenceSetup;
import com.unitedofoq.otms.personnel.dayoff.DayOff;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("ABSENCE")
@ParentEntity(fields="company")
@ChildEntity(fields = {"affectingIncomes", "affectingDeductions","penalties" , "incentives" })
public class Absence extends DayOff {
 // <editor-fold defaultstate="collapsed" desc="affectingIncomes">
    @JoinColumn
    @OneToMany(mappedBy="absence")
   private List<AbsenceAffectingIncome> affectingIncomes;

    public List<AbsenceAffectingIncome> getAffectingIncomes() {
        return affectingIncomes;
    }

    public void setAffectingIncomes(List<AbsenceAffectingIncome> affectingIncomes) {
        this.affectingIncomes = affectingIncomes;
    }

    public String getAffectingIncomesDD() {
        return "Absence_affectingIncomes";
    }
   
    // </editor-fold>
 // <editor-fold defaultstate="collapsed" desc="absenceAffectingDeductions">
    @JoinColumn
    @OneToMany(mappedBy="absence")
   private List<AbsenceAffectingDeduction> affectingDeductions;

    public List<AbsenceAffectingDeduction> getAffectingDeductions() {
        return affectingDeductions;
    }

    public void setAffectingDeductions(List<AbsenceAffectingDeduction> affectingDeductions) {
        this.affectingDeductions = affectingDeductions;
    }
public String getAffectingDeductionsDD() {
        return "Absence_affectingDeductions";
    }
    
    // </editor-fold>
 // <editor-fold defaultstate="collapsed" desc="penalties">
    @OneToMany(mappedBy = "absence")
    private List<AbsencePenalty> penalties;

    public List<AbsencePenalty> getPenalties() {
        return penalties;
    }

    public void setPenalties(List<AbsencePenalty> penalties) {
        this.penalties = penalties;
    }

 public String getPenaltiesDD() {
        return "Absence_penalties";
    }
 // </editor-fold>
 // <editor-fold defaultstate="collapsed" desc="absenceIncentives">
    @OneToMany(mappedBy = "absence")
    private List<IncentivesAbsenceSetup> incentives;

    public List<IncentivesAbsenceSetup> getIncentives() {
        return incentives;
    }

    public void setIncentives(List<IncentivesAbsenceSetup> incentives) {
        this.incentives = incentives;
    }

 public String getIncentivesDD() {
        return "Absence_incentives";
    }
 // </editor-fold>
 // <editor-fold defaultstate="collapsed" desc="company">
 @JoinColumn(nullable=false )
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
 private  Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
     public String getCompanyDD() {
        return "Absence_company";
    }
      // </editor-fold>


}
