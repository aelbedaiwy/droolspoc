/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author arezk
 */
@Entity
@ChildEntity(fields="businessOutcomes")
@ParentEntity(fields="employee")
public class EDSIDPForBusinessOutcome extends EDSIDP{
    private boolean approved;
    private boolean finalUpdate;

    public String getApprovedDD() {
        return "EDSIDPForBusinessOutcome__approved";
    }
    public String getFinalUpdateDD() {
        return "EDSIDPForBusinessOutcome__finalUpdate";
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isFinalUpdate() {
        return finalUpdate;
    }

    public void setFinalUpdate(boolean finalUpdate) {
        this.finalUpdate = finalUpdate;
    }

    private boolean finished;

    public String getFinishedDD() {
        return "EDSIDPForBusinessOutcome__finished";
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    @OneToMany(mappedBy="idp")
    private List<EDSIDPBusinessOutcome> businessOutcomes;

    public String getBusinessOutcomesDD(){  return "EDSIDPForBusinessOutcome_businessOutcomes";  }

    public List<EDSIDPBusinessOutcome> getBusinessOutcomes() {
        return businessOutcomes;
    }

    public void setBusinessOutcomes(List<EDSIDPBusinessOutcome> businessOutcomes) {
        this.businessOutcomes = businessOutcomes;
    }
}
