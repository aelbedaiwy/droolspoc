package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ObjectBaseEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@ChildEntity(fields = {"fieldGroup"})
public class ORepTemplDS extends ObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    /**
     * @return the descriptionTranslated
     */
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    /**
     * @param descriptionTranslated the descriptionTranslated to set
     */
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    /**
     * @return the descriptionTranslated DD
     */
    public String getDescriptionTranslatedDD() {
        return "ORepTemplDS_descriptionTranslated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nameTranslated">
    @Column(unique = true, nullable = false)
    @Translatable(translationField = "nameTranslated")
    private String name;
    @Translation(originalField = "name")
    @Transient
    private String nameTranslated;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the name DD
     */
    public String getNameTranslatedDD() {
        return "ORepTemplDS_name";
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="otemplReps">
    @OneToMany(mappedBy = "orepTemplDS")
    private List<OTemplRep> otemplReps;

    /**
     * @return the otemplReps
     */
    public List<OTemplRep> getOtemplReps() {
        return otemplReps;
    }

    /**
     * @param otemplReps the otemplReps to set
     */
    public void setOtemplReps(List<OTemplRep> otemplReps) {
        this.otemplReps = otemplReps;
    }

    /**
     * @return the otemplReps DD
     */
    public String getOtemplRepsDD() {
        return "ORepTemplDS_otemplReps";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    @JoinColumn
    @OneToMany(mappedBy = "orepTemplDS")
    private List<ORepTemplDSField> fields;

    /**
     * @return the oRepTemplDSFields
     */
    public List<ORepTemplDSField> getFields() {
        return fields;
    }

    /**
     * @param fields the oRepTemplDSFields to set
     */
    public void setFields(List<ORepTemplDSField> fields) {
        this.fields = fields;
    }

    /**
     * @return the oRepTemplDSFields DD
     */
    public String getFieldsDD() {
        return "ORepTemplDS_fields";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldGroup">
    @JoinColumn(nullable = false)
    @OneToMany(mappedBy = "orepTemplDS")
    private List<ORepTemplDSFieldGroup> fieldGroup;

    /**
     * @return the fieldGroup
     */
    public List<ORepTemplDSFieldGroup> getFieldGroup() {
        return fieldGroup;
    }

    /**
     * @param fieldGroup the fieldGroup to set
     */
    public void setFieldGroup(List<ORepTemplDSFieldGroup> fieldGroup) {
        this.fieldGroup = fieldGroup;
    }

    /**
     * @return the fieldGroupDD
     */
    public String getFieldGroupDD() {
        return "ORepTemplDS_fieldGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dsEntity">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OEntity dsEntity;

    /**
     * @return the dsEntity
     */
    public OEntity getDsEntity() {
        return dsEntity;
    }

    /**
     * @param dsEntity the dsEntity to set
     */
    public void setDsEntity(OEntity dsEntity) {
        this.dsEntity = dsEntity;
    }

    /**
     * @return the dsEntityDD
     */
    public String getDsEntityDD() {
        return "ORepTemplDS_dsEntity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="showCompanyLevels">
    private boolean showCompanyLevels;

    /**
     * @return the showCompanyLevels
     */
    public boolean isShowCompanyLevels() {
        return showCompanyLevels;
    }

    /**
     * @param showCompanyLevels the showCompanyLevels to set
     */
    public void setShowCompanyLevels(boolean showCompanyLevels) {
        this.showCompanyLevels = showCompanyLevels;
    }

    /**
     * @return the showCompanyLevelsDD
     */
    public String getShowCompanyLevelsDD() {
        return "ORepTemplDS_showCompanyLevels";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyLevelsCount">
    private int companyLevelsCount;

    /**
     * @return the companyLevelsCount
     */
    public int getCompanyLevelsCount() {
        return companyLevelsCount;
    }

    /**
     * @param companyLevelsCount the companyLevelsCount to set
     */
    public void setCompanyLevelsCount(int companyLevelsCount) {
        this.companyLevelsCount = companyLevelsCount;
    }

    /**
     * @return the companyLevelsCountDD
     */
    public String getCompanyLevelsCountDD() {
        return "ORepTemplDS_companyLevelsCount";
    }
    // </editor-fold>
}
