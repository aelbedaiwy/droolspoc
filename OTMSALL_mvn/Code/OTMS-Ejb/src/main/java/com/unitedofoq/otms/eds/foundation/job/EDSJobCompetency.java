/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.job;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.competency.EDSCompetency;
import com.unitedofoq.otms.eds.competency.EDSCompetencyLevel;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="job")
@ChildEntity(fields={"competencyLevel"})
@Table(name="PositionCompetency")
public class EDSJobCompetency extends BaseEntity{
    @Transient
    protected boolean level;

    public boolean isLevel() {
        return level;
    }

    public void setLevel(boolean level) {
        this.level = level;
    }
    

    @Override
    public  void PostLoad(){
        super.PostLoad();
        level = true;
    }
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    EDSCompetency competency;
    @Transient
    private int levelOrder;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="position_dbid")
    private EDSJob job;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetencyLevel competencyLevel;
    public String getCompetencyDD()      {    return "EDSJobCompetency_competency";  }
    public String getLevelOrderDD()      {    return "EDSJobCompetency_levelOrder";  }
    public String getJobDD()      {    return "EDSJobCompetency_job";  }
    public String getCompetencyLevelDD()      {    return "EDSJobCompetency_competencyLevel";  }
    public EDSCompetency getCompetency() {
        return competency;
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }
    public EDSCompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }

    public void setCompetencyLevel(EDSCompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }

    public int getLevelOrder() {
        levelOrder = competencyLevel.getSortIndex();
        return levelOrder;
    }

    public void setLevelOrder(int levelOrder) {
        this.levelOrder = levelOrder;
    }

    public EDSJob getJob() {
        return job;
    }

    public void setJob(EDSJob job) {
        this.job = job;
    }
}
