/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arezk
 */
@Entity
public class EDSEmployeeBehaviorHistory extends BaseEntity{
    private String source;
    private String evaluator;
    private String reference;
    @Temporal(TemporalType.DATE)
    private Date evaluationDate;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSEmployeeBehavior employeeBehavior;

    public String getEvaluatorDD()      {    return "EDSEmployeeBehaviorHistory_evaluator";  }
    public String getEvaluationDateDD() {    return "EDSEmployeeBehaviorHistory_evaluationDate";  }
    public String getReferenceDD()      {    return "EDSEmployeeBehaviorHistory_reference";  }
    public String getSourceDD()         {    return "EDSEmployeeBehaviorHistory_source";  }

    public EDSEmployeeBehavior getEmployeeBehavior() {
        return employeeBehavior;
    }

    public void setEmployeeBehavior(EDSEmployeeBehavior employeeBehavior) {
        this.employeeBehavior = employeeBehavior;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(String evaluator) {
        this.evaluator = evaluator;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
