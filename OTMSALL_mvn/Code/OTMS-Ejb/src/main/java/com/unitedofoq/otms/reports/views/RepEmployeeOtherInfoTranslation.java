package com.unitedofoq.otms.reports.views;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name= "repempothinfotranslation")
public class RepEmployeeOtherInfoTranslation extends RepCompanyLevelBaseTranslation {
    // <editor-fold defaultstate="collapsed" desc="educationDegree">
    private String educationDegree;

    public String getEducationDegree() {
        return educationDegree;
    }

    public void setEducationDegree(String educationDegree) {
        this.educationDegree = educationDegree;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="grade">
    private String grade;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="graduationPlace">
    private String graduationPlace;

    public String getGraduationPlace() {
        return graduationPlace;
    }

    public void setGraduationPlace(String graduationPlace) {
        this.graduationPlace = graduationPlace;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="healthCertificate">  
    private String healthCertificate;

    public String getHealthCertificate() {
        return healthCertificate;
    }

    public void setHealthCertificate(String healthCertificate) {
        this.healthCertificate = healthCertificate;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    private String genderDescription;

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescription() {
        return genderDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column
    private String employeeActive;

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String getEmployeeActive() {
        return employeeActive;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    private String firstName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    private String middleName;

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    private String lastName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    private String locationDescription;

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescription() {
        return locationDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    private String hiringType;

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringType() {
        return hiringType;
    }
    // </editor-fold>
}