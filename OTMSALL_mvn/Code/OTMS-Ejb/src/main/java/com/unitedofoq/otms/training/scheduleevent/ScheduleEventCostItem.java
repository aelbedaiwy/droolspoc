/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.scheduleevent;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.CostItemBase;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"scheduleEvent"})
@DiscriminatorValue("SCHEDULEEVENT")
public class ScheduleEventCostItem extends CostItemBase {
    // <editor-fold defaultstate="collapsed" desc="scheduleEvent">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEvent scheduleEvent;

    public ScheduleEvent getScheduleEvent() {
        return scheduleEvent;
    }

    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
        this.scheduleEvent = scheduleEvent;
    }

    public String getScheduleEventDD() {
        return "ScheduleEventCostItem_scheduleEvent";
    }
    // </editor-fold>
}