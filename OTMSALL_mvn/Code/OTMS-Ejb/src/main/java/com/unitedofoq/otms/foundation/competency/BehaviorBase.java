/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.competency;




import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;

/** *
 * @author abayomy
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BTYPE")
@DiscriminatorValue("MASTER")
public class BehaviorBase extends BusinessObjectBaseEntity  {
    
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false)
    @Translatable(translationField="nameTranslated")
    private String name;
    public String getName() {
        return name;
    }
     public String getNameTranslatedDD() {
        return "BehaviorBase_nameTranslated";
    }
    public void setName(String name) {
        this.name = name;
    }
    @Translation(originalField="name")
    @Transient
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField="descriptionTranslated")
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "BehaviorBase_description";
    }
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
// </editor-fold>
}

