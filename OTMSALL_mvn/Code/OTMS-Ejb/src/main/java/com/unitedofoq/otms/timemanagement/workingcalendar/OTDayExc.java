/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lahmed
 */
@Entity
@Table(name="OTSegment")
@ParentEntity(fields={"dayException"})
//@ChildEntity(fields={"dayExcs"})
public class OTDayExc extends OTSegmentBase {
    
    //<editor-fold defaultstate="collapsed" desc="dayException">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private DaysCalendarException dayException;

    public DaysCalendarException getDayException() {
        return dayException;
    }

    public void setDayException(DaysCalendarException dayException) {
        this.dayException = dayException;
    }
    
    public String getDayExceptionDD() {
        return "OTDayExc_dayException";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="dayExcs">
    /*@OneToMany(mappedBy = "dayException")
    private List<OTDayExc> dayExcs;

    public List<OTDayExc> getDayExcs() {
        return dayExcs;
    }

    public void setDayExcs(List<OTDayExc> dayExcs) {
        this.dayExcs = dayExcs;
    }
    
    public String getDayExcsDD() {
        return "DaysCalendarException_dayExcs";
    }*/
    //</editor-fold>
}