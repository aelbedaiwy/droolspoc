/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.otms.eds.foundation.employee.EmployeeProfilerAssessorConsultant;
import com.unitedofoq.otms.foundation.employee.ConsultantPerson;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author nkhalil
 */
@Entity
@ChildEntity(fields={"consultantPerson", "consultantUser"})
public class EDSConsultant extends BaseEntity {
    @OneToMany(mappedBy = "assossr")
    private List<EmployeeProfilerAssessorConsultant> employeeProfilerAssessorConsultants;

    public List<EmployeeProfilerAssessorConsultant> getEmployeeProfilerAssessorConsultants() {
        return employeeProfilerAssessorConsultants;
    }

    public void setEmployeeProfilerAssessorConsultants(List<EmployeeProfilerAssessorConsultant> employeeProfilerAssessorConsultants) {
        this.employeeProfilerAssessorConsultants = employeeProfilerAssessorConsultants;
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSIDPAccountability accountability;

    public EDSIDPAccountability getAccountability() {
        return accountability;
    }

    public void setAccountability(EDSIDPAccountability accountability) {
        this.accountability = accountability;
    }
     public String getAccountabilityDD() {
        return "EDSConsultant_accountability";
    }

    // <editor-fold defaultstate="collapsed" desc="consultantUser">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy="consultant")
    @JoinColumn
    private EDSConsultantUser consultantUser;

    public EDSConsultantUser getConsultantUser() {
        return consultantUser;
    }

    public void setConsultantUser(EDSConsultantUser consultantUser) {
        this.consultantUser = consultantUser;
    }
    
    public String getEmployeeUserDD() {
        return "EDSConsultant_consultantUser" ;
    }
    // </editor-fold>
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade={CascadeType.ALL}, mappedBy="consultant")
    @JoinColumn(nullable=false)
    private ConsultantPerson consultantPerson;
    private String job;
    private String company;

    public String getJobDD() {
        return "EDSConsultant_job";
    }

    public String getCompanyDD() {
        return "EDSConsultant_company";
    }

    public String getConsultantPersonDD() {
        return "EDSConsultant_consultantPerson";
    }

    public ConsultantPerson getConsultantPerson() {
        return consultantPerson;
    }

    public void setConsultantPerson(ConsultantPerson consultantPerson) {
        this.consultantPerson = consultantPerson;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
    
    // <editor-fold defaultstate="collapsed" desc="consultantPerson prepersist">
    @Override
    public void PrePersist() {
        super.PrePersist();
        if (consultantPerson != null)
            if (consultantPerson.getConsultant() == null)
                consultantPerson.setConsultant(this);
    }
    // </editor-fold>
}
