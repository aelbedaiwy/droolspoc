package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationBase;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("VACATIONGROUP")
@ParentEntity(fields = {"employee"})
public class EmployeeVacationGroup extends EmployeeVacationBase {
    // <editor-fold defaultstate="collapsed" desc="vacationGroup">

    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private VacationGroup vacationGroup;

    public VacationGroup getVacationGroup() {
        return vacationGroup;
    }

    public void setVacationGroup(VacationGroup vacationGroup) {
        this.vacationGroup = vacationGroup;
    }

    public String getVacationGroupDD() {
        return "EmployeeVacationGroup_vacationGroup";
    }
    // </editor-fold>
}
