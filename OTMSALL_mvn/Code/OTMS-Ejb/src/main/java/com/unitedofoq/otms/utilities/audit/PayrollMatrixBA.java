package com.unitedofoq.otms.utilities.audit;

import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.PayrollMatrix;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Aboelnour
 */
@Entity
public class PayrollMatrixBA extends BusinessAudit {

    // <editor-fold defaultstate="collapsed" desc="payrollMatrix">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    PayrollMatrix payrollMatrix;

    public PayrollMatrix getPayrollMatrix() {
        return payrollMatrix;
    }

    public void setPayrollMatrix(PayrollMatrix payrollMatrix) {
        this.payrollMatrix = payrollMatrix;
    }

    public String getSequenceDD() {
        return "BusinessAudit_payrollMatrix";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "BusinessAudit_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElementAssigned">
    Boolean salaryElementAssigned;

    public Boolean getSalaryElementAssigned() {
        return salaryElementAssigned;
    }

    public void setSalaryElementAssigned(Boolean salaryElementAssigned) {
        this.salaryElementAssigned = salaryElementAssigned;
    }

    public String getSalaryElementAssignedDD() {
        return "BusinessAudit_salaryElementAssigned";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElementValue">
    BigDecimal salaryElementValue;

    public BigDecimal getSalaryElementValue() {
        return salaryElementValue;
    }

    public void setSalaryElementValue(BigDecimal salaryElementValue) {
        this.salaryElementValue = salaryElementValue;
    }

    public String getEmpSalEleValueDD() {
        return "BusinessAudit_salaryElementValue";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applied">
    Boolean applied;

    public Boolean getApplied() {
        return applied;
    }

    public void setApplied(Boolean applied) {
        this.applied = applied;
    }

    public String getAppliedDD() {
        return "BusinessAudit_applied";
    }
    // </editor-fold>
}
