/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author mmohamed
 */
@Local
public interface PlanServiceLocal {

    public OFunctionResult postCreatePlanInstance (ODataMessage odm, OFunctionParms params, OUser loggedUser);
    public OFunctionResult applyPlanInstance (ODataMessage odm, OFunctionParms params, OUser loggedUser);
    public OFunctionResult addCompanyPlanUser(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}