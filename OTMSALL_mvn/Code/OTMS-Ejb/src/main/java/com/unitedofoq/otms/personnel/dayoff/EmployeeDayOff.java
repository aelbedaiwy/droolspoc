package com.unitedofoq.otms.personnel.dayoff;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DAYOFFTYPE")
@ParentEntity(fields = {"employee"})
public class EmployeeDayOff extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeDayOff_employee";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="balance">
    @Column(precision = 18, scale = 3)
    private BigDecimal balance; //name = "vacation_balance"

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getBalanceDD() {
        return "EmployeeDayOff_balance";
    }
    @Transient
    private BigDecimal balanceMask; //name = "vacation_balance"

    public BigDecimal getBalanceMask() {
        balanceMask = balance;
        return balanceMask;
    }

    public void setBalanceMask(BigDecimal balanceMask) {
        updateDecimalValue("balance", balanceMask);
    }

    public String getBalanceMaskDD() {
        return "EmployeeDayOff_balanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastVacationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastVacationDate;//name = "last_vacation_date"

    public Date getLastVacationDate() {
        return lastVacationDate;
    }

    public void setLastVacationDate(Date lastVacationDate) {
        this.lastVacationDate = lastVacationDate;
    }

    public String getLastVacationDateDD() {
        return "EmployeeDayOff_lastVacationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastVacationPeriod">
    @Column(precision = 18, scale = 3)
    private BigDecimal lastVacationPeriod;//name = "last_vacation_period"

    public BigDecimal getLastVacationPeriod() {
        return lastVacationPeriod;
    }

    public void setLastVacationPeriod(BigDecimal lastVacationPeriod) {
        this.lastVacationPeriod = lastVacationPeriod;
    }

    public String getLastVacationPeriodDD() {
        return "EmployeeDayOff_lastVacationPeriod";
    }
    @Transient
    private BigDecimal lastVacationPeriodMask;

    public BigDecimal getLastVacationPeriodMask() {
        lastVacationPeriodMask = lastVacationPeriod;
        return lastVacationPeriodMask;
    }

    public void setLastVacationPeriodMask(BigDecimal lastVacationPeriodMask) {
        updateDecimalValue("lastVacationPeriod", lastVacationPeriodMask);
    }

    public String getLastVacationPeriodMaskDD() {
        return "EmployeeDayOff_lastVacationPeriodMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taken">
    @Column(precision = 18, scale = 3)
    private BigDecimal taken;//name = "vacation_taken"

    public BigDecimal getTaken() {
        return taken;
    }

    public void setTaken(BigDecimal taken) {
        this.taken = taken;
    }

    public String getTakenDD() {
        return "EmployeeDayOff_taken";
    }
    @Transient
    private BigDecimal takenMask;

    public BigDecimal getTakenMask() {
        takenMask = taken;
        return takenMask;
    }

    public void setTakenMask(BigDecimal takenMask) {
        updateDecimalValue("taken", takenMask);
    }

    public String getTakenMaskDD() {
        return "EmployeeDayOff_takenMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="overLimit">
    @Column(precision = 18, scale = 3)
    private BigDecimal overLimit;//name = "vacation_over_limit"

    public BigDecimal getOverLimit() {
        return overLimit;
    }

    public void setOverLimit(BigDecimal overLimit) {
        this.overLimit = overLimit;
    }

    public String getOverLimitDD() {
        return "EmployeeDayOff_overLimit";
    }
    @Transient
    private BigDecimal overLimitMask;

    public BigDecimal getOverLimitMask() {
        overLimitMask = overLimit;
        return overLimitMask;
    }

    public void setOverLimitMask(BigDecimal overLimitMask) {
        updateDecimalValue("overLimit", overLimitMask);
    }

    public String getOverLimitMaskDD() {
        return "EmployeeDayOff_overLimitMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="occuranceOverLimit">
    @Column
    private Short occuranceOverLimit;//name = "occurance_over_limit"

    public Short getOccuranceOverLimit() {
        return occuranceOverLimit;
    }

    public void setOccuranceOverLimit(Short occuranceOverLimit) {
        this.occuranceOverLimit = occuranceOverLimit;
    }

    public String getOccuranceOverLimitDD() {
        return "EmployeeDayOff_occuranceOverLimit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dtype">
    @Transient
    private String dtype;

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getDtypeDD() {
        return "EmployeeDayOff_dtype";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOff">
    @Transient
    private DayOff dayOff;

    public DayOff getDayOff() {
        return dayOff;
    }

    public void setDayOff(DayOff dayOff) {
        this.dayOff = dayOff;
    }

    public String getDayOffDD() {
        return "EmployeeDayOff_dayOff";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="adjustmentValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal adjustmentValue;

    public BigDecimal getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(BigDecimal adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    public String getAdjustmentValueDD() {
        return "EmployeeDayOff_adjustmentValue";
    }

    @Transient
    private BigDecimal adjustmentValueMask;

    public BigDecimal getAdjustmentValueMask() {
        adjustmentValueMask = adjustmentValue;
        return adjustmentValueMask;
    }

    public void setAdjustmentValueMask(BigDecimal adjustmentValueMask) {
        updateDecimalValue("adjustmentValue", adjustmentValueMask);
    }

    public String getAdjustmentValueMaskDD() {
        return "EmployeeDayOff_adjustmentValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="adjustmentDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date adjustmentDate;

    public Date getAdjustmentDate() {
        return adjustmentDate;
    }

    public void setAdjustmentDate(Date adjustmentDate) {
        this.adjustmentDate = adjustmentDate;
    }

    public String getAdjustmentDateDD() {
        return "EmployeeDayOff_adjustmentDate";
    }
    // </editor-fold>
}
