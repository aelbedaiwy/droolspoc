/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ChildEntity(fields={"planActivities","checkLists"})
public class TrainingPlan extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "TrainingPlan_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "TrainingPlan_description";
    }
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDate">
    @Column (nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }
    
    public String getFromDateDD() {
        return "TrainingPlan_fromDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
    @Column(nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
    
    public String getToDateDD() {
        return "TrainingPlan_toDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="collectFrom">
    @Column(nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date collectFrom;

    public Date getCollectFrom() {
        return collectFrom;
    }

    public void setCollectFrom(Date collectFrom) {
        this.collectFrom = collectFrom;
    }

    public String getCollectFromDD() {
        return "TrainingPlan_collectFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="collectTo">
    @Column(nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date collectTo;

    public Date getCollectTo() {
        return collectTo;
    }

    public void setCollectTo(Date collectTo) {
        this.collectTo = collectTo;
    }

    public String getCollectToDD() {
        return "TrainingPlan_collectTo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainingPlanStatus">
    //Contain Plan Status
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC trainingPlanStatus;

    public UDC getTrainingPlanStatus() {
        return trainingPlanStatus;
    }

    public void setTrainingPlanStatus(UDC trainingPlanStatus) {
        this.trainingPlanStatus = trainingPlanStatus;
    }

    public String getTrainingPlanStatusDD() {
        return "TrainingPlan_trainingPlanStatus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeesNumber">
    @Column
    private BigDecimal employeesNumber;

    public BigDecimal getEmployeesNumber() {
        return employeesNumber;
    }

    public void setEmployeesNumber(BigDecimal employeesNumber) {
        this.employeesNumber = employeesNumber;
    }

    public String getEmployeesNumberDD() {
        return "TrainingPlan_employeesNumber";
    }
    @Transient
    private BigDecimal employeesNumberMask;

    public BigDecimal getEmployeesNumberMask() {
        employeesNumberMask = employeesNumber ;
        return employeesNumberMask;
    }

    public void setEmployeesNumberMask(BigDecimal employeesNumberMask) {
        updateDecimalValue("employeesNumber", employeesNumberMask);
    }

    public String getEmployeesNumberMaskDD() {
        return "TrainingPlan_employeesNumberMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="planActivities">
    @OneToMany(mappedBy = "trainingPlan")
    private List<PlanActivity> planActivities;

    public List<PlanActivity> getPlanActivities() {
        return planActivities;
    }

    public void setPlanActivities(List<PlanActivity> planActivities) {
        this.planActivities = planActivities;
    }

    public String getPlanActivitiesDD() {
        return "TrainingPlan_planActivities";
    }
    // </editor-fold>
}