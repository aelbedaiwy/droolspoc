/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.competency;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@DiscriminatorValue("MASTER")
@ParentEntity(fields="competency")
public class CompetencyLevel extends CompetencyLevelBase{
   
    // <editor-fold defaultstate="collapsed" desc="Competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Competency competency;
    public Competency getCompetency() {
        return competency;
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
     public String getCompetencyDD() {
        return "CompetencyLevel_competency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GradingSchemaSetup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule gradingSchemaSetup;

    public ScaleTypeRule getGradingSchemaSetup() {
        return gradingSchemaSetup;
    }

    public void setGradingSchemaSetup(ScaleTypeRule gradingSchemaSetup) {
        this.gradingSchemaSetup = gradingSchemaSetup;
    }

    public String getGradingSchemaSetupDD() {
        return "CompetencyLevel_gradingSchemaSetup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="rankValue">
    private int rankValue;
    
    public int getRankValue() {
        return rankValue;
    }

    public void setRankValue(int rankValue) {
        this.rankValue = rankValue;
    }

    public String getRankValueDD() {
        return "CompetencyLevel_rankValue";
    }
    // </editor-fold>
}
