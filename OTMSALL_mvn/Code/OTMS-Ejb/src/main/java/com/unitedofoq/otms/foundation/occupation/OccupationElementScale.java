/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.occupation;


import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author abdullahm
 */
@Entity
public class OccupationElementScale extends BaseEntity {

    @Column(nullable = false)
    private String scaleID;
    public String getScaleIDDD(){
        return "OccupationElementScale_scaleID";
    }
    @Column(nullable = false)
    private String scaleName;
    public String getScaleNameDD(){
        return "OccupationElementScale_scaleName";
    }
    @Column(nullable = false)
    private double minimum;
    public String getMinimumDD(){
        return "OccupationElementScale_minimum";
    }
    @Column(nullable = false)
    private double maximum;
    public String getMaximumDD(){
        return "OccupationElementScale_maximum";
    }
    private boolean onet;

    public String getOnetDD(){
        return "OccupationElementScale_onet";
    }

    public boolean isOnet() {
        return onet;
    }

    public void setOnet(boolean onet) {
        this.onet = onet;
    }
   

    public String getScaleID() {
        return scaleID;
    }

    public void setScaleID(String ScaleID) {
        this.scaleID = ScaleID;
    }

    public double getMaximum() {
        return maximum;
    }

    public void setMaximum(double maximum) {
        this.maximum = maximum;
    }

    public double getMinimum() {
        return minimum;
    }

    public void setMinimum(double minimum) {
        this.minimum = minimum;
    }

    public String getScaleName() {
        return scaleName;
    }

    public void setScaleName(String scaleName) {
        this.scaleName = scaleName;
    }

    

}
