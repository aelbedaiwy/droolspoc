package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"vacation"})
public class VacationFormula extends VacationFormulaBase {
    // <editor-fold defaultstate="collapsed" desc="vacation">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "VacationFormula_vacation";
    }
    // </editor-fold>
}
