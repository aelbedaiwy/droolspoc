/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.occupation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author abdullahm
 */
@MappedSuperclass
public class OccuCustomElements extends BaseEntity {

    protected String skill;
    public String getSkillDD(){
        return "OccuCustomElements_skill";
    }
    protected String task;
    public String getTaskDD(){
        return "OccuCustomElements_task";
    }
    protected String interest;
    public String getInterestDD(){
        return "OccuCustomElements_interest";
    }
    protected String ability;
    public String getAbilityDD(){
        return "OccuCustomElements_ability";
    }
    protected String language;
    public String getLanguageDD(){
        return "OccuCustomElements_language";
    }
    protected String workStle;
    public String getWorkStleDD(){
        return "OccuCustomElements_workStle";
    }
    protected String workValue;
    public String getWorkValueDD(){
        return "OccuCustomElements_workValue";
    }
    protected String workContext;
    public String getWorkContextDD(){
        return "OccuCustomElements_workContext";
    }
    protected String workActivity;
    public String getWorkActivityDD(){
        return "OccuCustomElements_workActivity ";
    }
    protected String knowledge;
    public String getKnowledgeDD(){
        return "OccuCustomElements_knowledge";
    }
    protected String experience;
    public String getExperienceDD(){
        return "OccuCustomElements_experience";
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(String knowledge) {
        this.knowledge = knowledge;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getWorkActivity() {
        return workActivity;
    }

    public void setWorkActivity(String workActivity) {
        this.workActivity = workActivity;
    }

    public String getWorkContext() {
        return workContext;
    }

    public void setWorkContext(String workContext) {
        this.workContext = workContext;
    }

    public String getWorkStle() {
        return workStle;
    }

    public void setWorkStle(String workStle) {
        this.workStle = workStle;
    }

    public String getWorkValue() {
        return workValue;
    }

    public void setWorkValue(String workValue) {
        this.workValue = workValue;
    }

}
