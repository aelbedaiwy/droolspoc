package com.unitedofoq.otms.core.report.template;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"orepTemplDS"})
@ChildEntity(fields = {"fields"})
public class ORepTemplDSFieldGroup extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable = false)
    private String name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the name DD
     */
    public String getNameDD() {
        return "ORepTemplDSFieldGroup_name";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Translation(originalField = "description")
    @Transient
    private String descriptionTranslated;

    /**
     * @return the descriptionTranslated
     */
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    /**
     * @param descriptionTranslated the descriptionTranslated to set
     */
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    /**
     * @return the descriptionTranslated DD
     */
    public String getDescriptionTranslatedDD() {
        return "ORepTemplDSFieldGroup_descriptionTranslated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="orepTemplDS">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private ORepTemplDS orepTemplDS;

    /**
     * @return the orepTemplDS
     */
    public ORepTemplDS getOrepTemplDS() {
        return orepTemplDS;
    }

    /**
     * @param orepTemplDS the orepTemplDS to set
     */
    public void setOrepTemplDS(ORepTemplDS orepTemplDS) {
        this.orepTemplDS = orepTemplDS;
    }

    /**
     * @return the orepTemplDSDD
     */
    public String getOrepTemplDSDD() {
        return "ORepTemplDSFieldGroup_orepTemplDS";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    @JoinColumn
    @OneToMany(mappedBy = "fieldGroup")
    private List<ORepTemplDSField> fields;

    /**
     * @return the oRepTemplDSFields
     */
    public List<ORepTemplDSField> getFields() {
        return fields;
    }

    /**
     * @param fields the oRepTemplDSFields to set
     */
    public void setFields(List<ORepTemplDSField> fields) {
        this.fields = fields;
    }

    /**
     * @return the ORepTemplDSFieldGroup DD
     */
    public String getFieldsDD() {
        return "ORepTemplDSFieldGroup_fields";
    }
    // </editor-fold>
}
