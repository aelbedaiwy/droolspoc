package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"appliedApplicant"})
public class AppliedApplicantHistory extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="appliedApplicant">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppliedApplicant appliedApplicant;

    public AppliedApplicant getAppliedApplicant() {
        return appliedApplicant;
    }

    public String getAppliedApplicantDD() {
        return "AppliedApplicantHistory_appliedApplicant";
    }

    public void setAppliedApplicant(AppliedApplicant appliedApplicant) {
        this.appliedApplicant = appliedApplicant;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "AppliedApplicantHistory_status";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="who">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee who;

    public Employee getWho() {
        return who;
    }

    public void setWho(Employee who) {
        this.who = who;
    }

    public String getWhoDD() {
        return "AppliedApplicantHistory_who";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusDate;

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusDateDD() {
        return "AppliedApplicantHistory_statusDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="what">
    private String what;

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhatDD() {
        return "AppliedApplicantHistory_what";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="statusResult">
    private String statusResult;

    public String getStatusResult() {
        return statusResult;
    }

    public void setStatusResult(String statusResult) {
        this.statusResult = statusResult;
    }

    public String getStatusResultDD() {
        return "AppliedApplicantHistory_statusResult";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="commment">

    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "AppliedApplicantHistory_comments";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentAction">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC currentAction;

    public UDC getCurrentAction() {
        return currentAction;
    }

    public void setCurrentAction(UDC currentAction) {
        this.currentAction = currentAction;
    }

    public String getCurrentActionDD() {
        return "AppliedApplicantHistory_currentAction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scheduleInterview">
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicantScheduleInterview scheduleInterview;

    public ApplicantScheduleInterview getScheduleInterview() {
        return scheduleInterview;
    }

    public void setScheduleInterview(ApplicantScheduleInterview scheduleInterview) {
        this.scheduleInterview = scheduleInterview;
    }

    public String getScheduleInterviewDD() {
        return "AppliedApplicantHistory_scheduleInterview";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scheduleExam">
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicantScheduleExam scheduleExam;

    public ApplicantScheduleExam getScheduleExam() {
        return scheduleExam;
    }

    public void setScheduleExam(ApplicantScheduleExam scheduleExam) {
        this.scheduleExam = scheduleExam;
    }

    public String getScheduleExamDD() {
        return "AppliedApplicantHistory_scheduleExam";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="offer">
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicantOffer applicantOffer;

    public ApplicantOffer getApplicantOffer() {
        return applicantOffer;
    }

    public void setApplicantOffer(ApplicantOffer applicantOffer) {
        this.applicantOffer = applicantOffer;
    }

    public String getApplicantOfferDD() {
        return "AppliedApplicantHistory_applicantOffer";
    }
    // </editor-fold>
}
