
package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.*;

@Entity
@Table(name="Unit")
public class UnitPlan extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "UnitPlan_name";
    }
    // </editor-fold>

    //    // <editor-fold defaultstate="collapsed" desc="description">
//    @Column
//    private String description;
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public String getDescriptionDD() {
//        return "UnitPlan_description";
//    }
//    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="level_dbid">
    @Column
    private Long unitLevel_dbid;

    public Long getUnitLevel_dbid() {
        return unitLevel_dbid;
    }

    public void setUnitLevel_dbid(Long unitLevel_dbid) {
        this.unitLevel_dbid = unitLevel_dbid;
    }
    
    public String getUnitLevel_dbidDD() {
        return "UnitPlan_unitLevel_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitLevelIndex_dbid">
    @Column
    private Long unitLevelIndex_dbid;

    public void setUnitLevelIndex_dbid(Long levelIndex_dbid) {
        this.unitLevelIndex_dbid = levelIndex_dbid;
    }

    public Long getUnitLevelIndex_dbid() {
        return unitLevelIndex_dbid;
    }

    public String getUnitLevelIndex_dbidDD() {
        return "UnitPlan_unitLevelIndex_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="company_dbid">
    @Column
    private Long company_dbid;

    public void setCompany_dbid(Long company_dbid) {
        this.company_dbid = company_dbid;
    }

    public Long getCompany_dbid() {
        return company_dbid;
    }

    public String getCompany_dbidDD() {
        return "UnitPlan_company_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="originalUnit_dbid">
    @Column
    private Long originalUnit_dbid;

    public void setOriginalUnit_dbid(Long originalUnit_dbid) {
        this.originalUnit_dbid = originalUnit_dbid;
    }

    public Long getOriginalUnit_dbid() {
        return originalUnit_dbid;
    }

    public String getOriginalUnit_dbidDD() {
        return "UnitPlan_originalUnit_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hierarchycode">
    @Column
    private String hierarchycode;

    public String getHierarchycode() {
        return hierarchycode;
    }

    public String getHierarchycodeDD() {
        return "Unit_hierarchycode";
    }

    public void setHierarchycode(String hierarchycode) {
        this.hierarchycode = hierarchycode;
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="DTYPE">
    private String DTYPE = "MASTER";

    public String getDTYPE() {
        return DTYPE;
    }

    public void setDTYPE(String DTYPE) {
        this.DTYPE = DTYPE;
    }
    
    public String getDTYPEDD() {
        return "UnitPlan_DTYPE";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="parentUnit">
    private Long parentUnit_dbid;

    public Long getParentUnit_dbid() {
        return parentUnit_dbid;
    }

    public void setParentUnit_dbid(Long parentUnit_dbid) {
        this.parentUnit_dbid = parentUnit_dbid;
    }
    public String getParentUnit_dbidDD() {  
        return "UnitPlan_parentUnit_dbid";  
    }
// </editor-fold>
}