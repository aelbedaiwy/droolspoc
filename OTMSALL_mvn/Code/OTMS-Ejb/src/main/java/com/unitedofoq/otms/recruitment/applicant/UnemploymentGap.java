/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.applicant;
import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author aibrahim
 */
@Entity
@ParentEntity(fields={"applicant"})
public class UnemploymentGap  extends BusinessObjectBaseEntity {
// <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "UnemploymentGap_applicant";
    }
   // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="explanationForGap">
    @Column
    private String explanationForGap;
    public String getExplanationForGap() {
        return explanationForGap;
    }
    public void setExplanationForGap(String explanationForGap) {
        this.explanationForGap = explanationForGap;
    }
    public String getExplanationForGapDD() {
        return "UnemploymentGap_explanationForGap";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="dateFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    public Date getDateFrom() {
        return dateFrom;
    }
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }
    public String getDateFromDD() {
        return "UnemploymentGap_dateFrom";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="dateTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    public Date getDateTo() {
        return dateTo;
    }
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
    public String getDateToDD() {
        return "UnemploymentGap_dateTo";
    }
// </editor-fold>
 }
