package com.unitedofoq.otms.appraisal.template;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import com.unitedofoq.otms.recruitment.requisition.RequisitionSelectedApplicant;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"appraisalTemplate"})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class AppraisalTemplateSequence extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="appraisalTemplate">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplate appraisalTemplate;

    public AppraisalTemplate getAppraisalTemplate() {
        return appraisalTemplate;
    }

    public void setAppraisalTemplate(AppraisalTemplate appraisalTemplate) {
        this.appraisalTemplate = appraisalTemplate;
    }

    public String getAppraisalTemplateDD() {
        return "AppraisalTemplateSequence_appraisalTemplate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sequenceNumber">
    @Column
    private long sequenceNumber;

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getSequenceNumberDD() {
        return "AppraisalTemplateSequence_sequenceNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "AppraisalTemplateSequence_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="appraiseeType">
    //Employee OR Trainning Or Applicant Or Instructor Or userDefined
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC appraiseeType;

    public UDC getAppraiseeType() {
        return appraiseeType;
    }

    public void setAppraiseeType(UDC appraiseeType) {
        this.appraiseeType = appraiseeType;
    }

    public String getAppraiseeTypeDD() {
        return "AppraisalTemplateSequence_appraiseeType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="evaluationPeriodType">
    //Monthly Or Quarterly Or Semi Or ...
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC evaluationPeriodType;

    public UDC getEvaluationPeriodType() {
        return evaluationPeriodType;
    }

    public void setEvaluationPeriodType(UDC evaluationPeriodType) {
        this.evaluationPeriodType = evaluationPeriodType;
    }

    public String getEvaluationPeriodTypeDD() {
        return "AppraisalTemplateSequence_evaluationPeriodType";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "AppraisalTemplateSequence_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="finishDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getFinishDateDD() {
        return "AppraisalTemplateSequence_finishDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scope">
    //Appraisal Model Or Training Model Or Applicant Model
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC scope;

    public UDC getScope() {
        return scope;
    }

    public void setScope(UDC scope) {
        this.scope = scope;
    }

    public String getScopeDD() {
        return "AppraisalTemplateSequence_scope";
    }
    // </editor-fold>
    //    // <editor-fold defaultstate="collapsed" desc="providerActivity">
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private  ProviderActivity providerActivity;
//
//    public ProviderActivity getProviderActivity() {
//        return providerActivity;
//    }
//
//    public void setProviderActivity(ProviderActivity providerActivity) {
//        this.providerActivity = providerActivity;
//    }
//
//    public String getProviderActivityDD() {
//        return "AppraisalTemplateSequence_providerActivity";
//    }
//    // </editor-fold>
    //    // <editor-fold defaultstate="collapsed" desc="scheduleEvent">
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private ScheduleEvent scheduleEvent;
//
//    public ScheduleEvent getScheduleEvent() {
//        return scheduleEvent;
//    }
//
//    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
//        this.scheduleEvent = scheduleEvent;
//    }
//
//    public String getScheduleEventDD() {
//        return "AppraisalTemplateSequence_scheduleEvent";
//    }
//    // </editor-fold>
    //    // <editor-fold defaultstate="collapsed" desc="applicantScheduleInterview">
//
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    @JoinColumn
//    private ApplicantScheduleInterview applicantScheduleInterview;
//
//    public ApplicantScheduleInterview getApplicantScheduleInterview() {
//        return applicantScheduleInterview;
//    }
//
//    public void setApplicantScheduleInterview(ApplicantScheduleInterview applicantScheduleInterview) {
//        this.applicantScheduleInterview = applicantScheduleInterview;
//    }
//    public String getApplicantScheduleInterviewDD() {
//        return "AppraisalTemplateSequence_applicantScheduleInterview";
//    }
//
//    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "AppraisalTemplateSequence_description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="sequenceYear">
    @Column
    private String sequenceYear;

    public String getSequenceYear() {
        return sequenceYear;
    }

    public void setSequenceYear(String year) {
        this.sequenceYear = year;
    }

    public String getSequenceYearDD() {
        return "AppraisalTemplateSequence_sequenceYear";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="appraiseeCount">
    private long appraiseeCount;

    public long getAppraiseeCount() {
        return appraiseeCount;
    }

    public void setAppraiseeCount(long appraiseeCount) {
        this.appraiseeCount = appraiseeCount;
    }

    public String getAppraiseeCountDD() {
        return "AppraisalTemplateSequence_appraiseeCount";
    }
    //</editor-fold>

    // for recruitment:
    // =================
    //<editor-fold defaultstate="collapsed" desc="interviewer">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee interviewer;

    public Employee getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(Employee interviewer) {
        this.interviewer = interviewer;
    }

    public String getInterviewerDD() {
        return "AppraisalTemplateSequence_interviewer";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "AppraisalTemplateSequence_applicant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedApplicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private RequisitionSelectedApplicant selectedApplicant;

    public RequisitionSelectedApplicant getSelectedApplicant() {
        return selectedApplicant;
    }

    public void setSelectedApplicant(RequisitionSelectedApplicant selectedApplicant) {
        this.selectedApplicant = selectedApplicant;
    }

    public String getSelectedApplicantDD() {
        return "AppraisalTemplateSequence_selectedApplicant";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "AppraisalTemplateSequence_done";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="internalCode">
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getInternalCodeDD() {
        return "AppraisalTemplateSequence_internalCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne
    private Position position;

    public Position getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "AppraisalTemplateSequence_position";
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="scaleType">
    @ManyToOne(fetch = FetchType.LAZY)
    private ScaleType scaleType;

    public ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    public String getScaleTypeDD() {
        return "AppraisalTemplateSequence_scaleType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="distributionScaleType">
    @ManyToOne(fetch = FetchType.LAZY)
    private ScaleType distributionScaleType;

    public ScaleType getDistributionScaleType() {
        return distributionScaleType;
    }

    public void setDistributionScaleType(ScaleType distributionScaleType) {
        this.distributionScaleType = distributionScaleType;
    }

    public String getDistributionScaleTypeDD() {
        return "GoalAppraisalTemplateSequence_distributionScaleType";
    }
    // </editor-fold>

    // for distribution curve
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public String getSalaryElementDD() {
        return "GoalDistributionCurve_salaryElement";
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    // </editor-fold>
}
