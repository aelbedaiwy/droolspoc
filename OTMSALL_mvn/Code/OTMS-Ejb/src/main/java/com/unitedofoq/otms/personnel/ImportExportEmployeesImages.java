package com.unitedofoq.otms.personnel;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.payroll.PayrollParameter;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Mohamed Aboelnour
 */

@Entity
public class ImportExportEmployeesImages extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="PayrollParameter">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private PayrollParameter payrollParameter;

    public PayrollParameter getPayrollParameter() {
        return payrollParameter;
    }

    public void setPayrollParameter(PayrollParameter payrollParameter) {
        this.payrollParameter = payrollParameter;
    }

    public String getPayrollParameterDD() {
        return "ImportExportEmployeesImages_payrollParameter";
    }
    //</editor-fold>
}
