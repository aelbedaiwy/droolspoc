/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author lap2
 */
@Entity
@ParentEntity(fields={"company"})
@FABSEntitySpecs(customCascadeFields={"reflection"})
public class HijriCalendar extends HolidayCalendarBase {
    //<editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "HijriCalendar_company";
    }
    //</editor-fold>
    
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade= CascadeType.PERSIST, mappedBy="hijriCalendar")
    @JoinColumn(nullable=false)
    private HijriCalendarReflection reflection;

    public HijriCalendarReflection getReflection() {
        return reflection;
    }

    public void setReflection(HijriCalendarReflection reflection) {
        this.reflection = reflection;
    }
    
    public String getReflectionDD() {
        return "HijriCalendar_reflection";
    }
    
    @Override
    public void PrePersist() {
        super.PrePersist();
        if (reflection != null)
            if (reflection.getHijriCalendar() == null)
                reflection.setHijriCalendar(this);        
    }

}
