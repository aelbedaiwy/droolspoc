package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.otms.payroll.costcenter.CostCenterCalculation;
import com.unitedofoq.otms.payroll.gl.FinancialIntegrationSummary;
import com.unitedofoq.otms.personnel.PersonnelEffectonPayroll;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ChildEntity(fields = {"personnelEffectonPayrolls", "financialSummary", "costCenterCalculations"})
public class CalculatedPeriod extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="code">

    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "CalculatedPeriod_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="closed">
    @Column(length = 1)
    private String closed = "N";

    public String getClosed() {
        return closed;
    }

    public void setClosed(String closed) {
        this.closed = closed;
    }

    public String getClosedDD() {
        return "CalculatedPeriod_closed";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="month">
    @Column(nullable = false)
    private int month;

    public int getMonth() {
        return month;
    }

    public void setMonth(int currentMonth) {
        this.month = currentMonth;
    }

    public String getMonthDD() {
        return "CalculatedPeriod_month";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="year">
    @Column(nullable = false) //FIXME: to have current date, and auto-calc current year & month in transient fields
    private int year;

    public int getYear() {
        return year;
    }

    public void setYear(int currentYear) {
        this.year = currentYear;
    }

    public String getYearDD() {
        return "CalculatedPeriod_year";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "CalculatedPeriod_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxSettlementEnabled">
    @Column(length = 1)
    private String taxSettlementEnabled;

    public String getTaxSettlementEnabled() {
        return taxSettlementEnabled;
    }

    public void setTaxSettlementEnabled(String taxSettlementEnabled) {
        this.taxSettlementEnabled = taxSettlementEnabled;
    }

    public String getTaxSettlementEnabledDD() {
        return "CalculatedPeriod_taxSettlementEnabled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "CalculatedPeriod_endDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "CalculatedPeriod_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payPeriod">
    @JoinColumn(nullable = false)
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private PaymentPeriod payPeriod;

    public PaymentPeriod getPayPeriod() {
        return payPeriod;
    }

    public void setPayPeriod(PaymentPeriod payPeriod) {
        this.payPeriod = payPeriod;
    }

    public String getPayPeriodDD() {
        return "CalculatedPeriod_payPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentMethod">
    @JoinColumn(nullable = false)
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodDD() {
        return "CalculatedPeriod_paymentMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="personnelEffectonPayrolls">
    @OneToMany(mappedBy = "calculatedPeriod")
    private List<PersonnelEffectonPayroll> personnelEffectonPayrolls;

    public List<PersonnelEffectonPayroll> getPersonnelEffectonPayrolls() {
        return personnelEffectonPayrolls;
    }

    public void setPersonnelEffectonPayrolls(List<PersonnelEffectonPayroll> personnelEffectonPayrolls) {
        this.personnelEffectonPayrolls = personnelEffectonPayrolls;
    }

    public String getPersonnelEffectonPayrollsDD() {
        return "CalculatedPeriod_personnelEffectonPayrolls";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="financialSummary">
    @OneToMany(mappedBy = "calculatedPeriod")
    private List<FinancialIntegrationSummary> financialSummary;

    public List<FinancialIntegrationSummary> getFinancialSummary() {
        return financialSummary;
    }

    public void setFinancialSummary(List<FinancialIntegrationSummary> financialSummary) {
        this.financialSummary = financialSummary;
    }

    public String getFinancialSummaryDD() {
        return "CalculatedPeriod_financialSummary";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="costCenterCalculations">
    @OneToMany(mappedBy = "calculatedPeriod")
    private List<CostCenterCalculation> costCenterCalculations;

    public List<CostCenterCalculation> getCostCenterCalculations() {
        return costCenterCalculations;
    }

    public void setCostCenterCalculations(List<CostCenterCalculation> costCenterCalculations) {
        this.costCenterCalculations = costCenterCalculations;
    }

    public String getCostCenterCalculationsDD() {
        return "CalculatedPeriod_costCenterCalculations";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculateInactiveEmployees">
    private boolean calculateInactiveEmployees;

    public boolean isCalculateInactiveEmployees() {
        return calculateInactiveEmployees;
    }

    public boolean getCalculateInactiveEmployeesDD() {
        return calculateInactiveEmployees;
    }

    public void setCalculateInactiveEmployees(boolean calculateInactiveEmployees) {
        this.calculateInactiveEmployees = calculateInactiveEmployees;
    }
    //</editor-fold >
    // <editor-fold defaultstate="collapsed" desc="payCalcStartDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date payCalcStartDate;

    public Date getPayCalcStartDate() {
        return payCalcStartDate;
    }

    public String getPayCalcStartDateDD() {
        return "CalculatedPeriod_payCalcStartDate";
    }

    public void setPayCalcStartDate(Date payCalcStartDate) {
        this.payCalcStartDate = payCalcStartDate;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="payCalcEndDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date payCalcEndDate;

    public Date getPayCalcEndDate() {
        return payCalcEndDate;
    }

    public String getPayCalcEndDateDD() {
        return "CalculatedPeriod_payCalcEndDate";
    }

    public void setPayCalcEndDate(Date payCalcEndDate) {
        this.payCalcEndDate = payCalcEndDate;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="persClosingStartDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date persClosingStartDate;

    public Date getPersClosingStartDate() {
        return persClosingStartDate;
    }

    public String getPersClosingStartDateDD() {
        return "CalculatedPeriod_persClosingStartDate";
    }

    public void setPersClosingStartDate(Date persClosingStartDate) {
        this.persClosingStartDate = persClosingStartDate;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="persClosingEndDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date persClosingEndDate;

    public Date getPersClosingEndDate() {
        return persClosingEndDate;
    }

    public String getPersClosingEndDateDD() {
        return "CalculatedPeriod_persClosingEndDate";
    }

    public void setPersClosingEndDate(Date persClosingEndDate) {
        this.persClosingEndDate = persClosingEndDate;
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "CalculatedPeriod_done";
    }
    //</editor-fold>
}
