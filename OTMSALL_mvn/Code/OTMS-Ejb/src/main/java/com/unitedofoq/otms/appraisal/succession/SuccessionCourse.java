
package com.unitedofoq.otms.appraisal.succession;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.activity.ProviderCourse;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"plan"})
public class SuccessionCourse extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="plan">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch= FetchType.LAZY)
    private SuccessionPlan plan;

    public void setPlan(SuccessionPlan plan) {
        this.plan = plan;
    }

    public SuccessionPlan getPlan() {
        return plan;
    }

    public String getPlanDD() {
        return "SuccessionCourse_plan";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "SuccessionCourse_code";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "SuccessionCourse_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="score">
    @Column
    private BigDecimal score;

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public BigDecimal getScore() {
        return score;
    }

    public String getScoreDD() {
        return "SuccessionCourse_score";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date courseDate;

    public Date getCourseDate() {
        return courseDate;
    }

    public void setCourseDate(Date courseDate) {
        this.courseDate = courseDate;
    }
    
    public String getCourseDateDD() {
        return "SuccessionCourse_courseDate";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="course">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourse course;

    public ProviderCourse getCourse() {
        return course;
    }
    public String getCourseDD() {
        return "SuccessionCourse_course";
    }

    public void setCourse(ProviderCourse course) {
        this.course = course;
    }
    // </editor-fold >
    
    // <editor-fold defaultstate="collapsed" desc="plannedDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date plannedDate;

    public Date getPlannedDate() {
        return plannedDate;
    }
    public String getPlannedDateDD() {
        return "SuccessionCourse_plannedDate";
    }

    public void setPlannedDate(Date plannedDate) {
        this.plannedDate = plannedDate;
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="comment">
    @Column(name="comments")
    private String comment;
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public String getCommentDD() {
        return "SuccessionCourse_comment";
    }
    //</editor-fold>
}