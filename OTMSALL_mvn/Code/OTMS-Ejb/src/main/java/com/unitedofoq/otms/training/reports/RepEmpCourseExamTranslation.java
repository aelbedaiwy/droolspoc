package com.unitedofoq.otms.training.reports;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class RepEmpCourseExamTranslation extends RepEmpCourseTranslationBase {
    // <editor-fold defaultstate="collapsed" desc="courseClass">

    @Column
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="course">
    @Column
    private String course;

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourse() {
        return course;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="empComments">
    @Column
    private String empComments;

    public void setEmpComments(String empComments) {
        this.empComments = empComments;
    }

    public String getEmpComments() {
        return empComments;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="commentsOnEmp">
    @Column
    private String commentsOnEmp;

    public void setCommentsOnEmp(String commentsOnEmp) {
        this.commentsOnEmp = commentsOnEmp;
    }

    public String getCommentsOnEmp() {
        return commentsOnEmp;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="grade">
    private String grade;

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }
    //</editor-fold>
}
