
package com.unitedofoq.otms.training.company;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.competency.Competency;
import javax.persistence.*;

@Entity
public class CompanyCourse extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "CompanyCourse_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "CompanyCourse_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="learningMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC learningMethod;

    public void setLearningMethod(UDC learningMethod) {
        this.learningMethod = learningMethod;
    }

    public UDC getLearningMethod() {
        return learningMethod;
    }

    public String getLearningMethodDD() {
        return "CompanyCourse_learningMethod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="content">
    @Column
    private String content;

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getContentDD() {
        return "CompanyCourse_content";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="preRequisites">
    @Column
    private String preRequisites;

    public void setPreRequisites(String preRequisites) {
        this.preRequisites = preRequisites;
    }

    public String getPreRequisites() {
        return preRequisites;
    }

    public String getPreRequisitesDD() {
        return "CompanyCourse_preRequisites";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="forWhom">
    @Column
    private String forWhom;

    public void setForWhom(String forWhom) {
        this.forWhom = forWhom;
    }

    public String getForWhom() {
        return forWhom;
    }

    public String getForWhomDD() {
        return "CompanyCourse_forWhom";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="courseClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CompanyCourseClass courseClass;

    public CompanyCourseClass getCourseClass() {
        return courseClass;
    }

    public void setCourseClass(CompanyCourseClass courseClass) {
        this.courseClass = courseClass;
    }
    
    public String getCourseClassDD() {
        return "CompanyCourse_courseClass";
    }
    //</editor-fold>
    
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }
    
    public String getCompetencyDD() {
        return "CompanyCourse_competency";
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    
}
