package com.unitedofoq.otms.appraisal.questionaire;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplate;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Questionaire4 extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="description">

    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "Questionaire4_description";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="weight">
    @Column(precision = 25, scale = 13)
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getWeightDD() {
        return "Questionaire4_weight";
    }

    @Transient
    private BigDecimal weightMasked;

    public BigDecimal getWeightMasked() {
        weightMasked = weight;
        return weightMasked;
    }

    public void setWeightMasked(BigDecimal weightMasked) {
        updateDecimalValue("weight", weightMasked);
    }

    public String getWeightMaskedDD() {
        return "Questionaire4_weightMasked";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Questionaire4_code";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="gradingSchema">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private ScaleType gradingSchema;

    public ScaleType getGradingSchema() {
        return gradingSchema;
    }

    public void setGradingSchema(ScaleType gradingSchema) {
        this.gradingSchema = gradingSchema;
    }

    public String getGradingSchemaDD() {
        return "Questionaire4_gradingSchema";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="template">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplate template;

    public AppraisalTemplate getTemplate() {
        return template;
    }

    public void setTemplate(AppraisalTemplate template) {
        this.template = template;
    }

    public String getTemplateDD() {
        return "Questionaire4_template";
    }
    //</editor-fold>
}
