/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author aibrahim
 */
@Entity
@ParentEntity(fields={"requisition"})
public class ShortList extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "ShortList_applicant";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requisition">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
   
    private Requisition requisition;
    public Requisition getRequisition() {
        return requisition;
    }
    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }
     public String getRequisitionDD() {
        return "ShortList_requisition";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="assigingDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date assigingDate;
    public Date getAssigingDate() {
        return assigingDate;
    }
    public void setAssigingDate(Date assigingDate) {
        this.assigingDate = assigingDate;
    }
        public String getAssigingDateDD() {
        return "ShortList_assigingDate";
    }
 // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentStatus">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC currentStatus;
    public UDC getCurrentStatus() {
        return currentStatus;
    }
    public void setCurrentStatus(UDC currentStatus) {
        this.currentStatus = currentStatus;
    }
    public String getCurrentStatusDD() {
        return "ShortList_currentStatus";
    }
 // </editor-fold>




}
