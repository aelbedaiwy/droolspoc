
package com.unitedofoq.otms.recruitment.reports;


import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepReqCostItem extends RepReqCostItemBase  {


    
    //<editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionDD() {
        return "RepReqCostItem_description";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="vacancy">
    private String vacancy;

    public String getVacancy() {
        return vacancy;
    }

    public void setVacancy(String vacancy) {
        this.vacancy = vacancy;
    }
    
    public String getVacancyDD() {
        return "RepReqCostItem_vacancy";
    }

    //</editor-fold>
    

    
    //<editor-fold defaultstate="collapsed" desc="position">
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    
    public String getPositionDD() {
        return "RepReqCostItem_position";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="unit">
    private String unit;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public String getUnitDD() {
        return "RepReqCostItem_unit";
    }
    //</editor-fold>


}
