package com.unitedofoq.otms.security;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 *
 * @author mohamed
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.security.CustomReportDataSecurityServiceLocal", beanInterface = CustomReportDataSecurityServiceLocal.class)
public class CustomReportDataSecurityService implements CustomReportDataSecurityServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    DataTypeServiceRemote dataTypeService;
    @EJB
    DataSecurityServiceLocal dataSecurityServiceLocal;

    private String getDBConnectionString(OUser loggedUser) {
        String connectionString = "";
        try {
            DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
            Connection connection = datasource.getConnection();
            if (connection != null
                    && connection.getMetaData() != null) {
                connectionString = connection.getMetaData().getDatabaseProductName();
                connection.close();
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return connectionString;
    }

    @Override
    public OFunctionResult birtReportApplySecurity(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String selectSTMT = (String) odm.getData().get(0);
            String repName = (String) odm.getData().get(1);
            String dataSetName = (String) odm.getData().get(2);
            String newSelect = selectSTMT;
            newSelect = applyReportSecuirty(newSelect, loggedUser);
            ODataType screenDataLoadingUEDTRet = dataTypeService.loadODataType("ScreenDataLoadingUERet", loggedUser);
            ODataMessage returnedODM = new ODataMessage();
            returnedODM.setODataType(screenDataLoadingUEDTRet);
            ArrayList<Object> odmData = new ArrayList<>(1);

            odmData.add(newSelect);
            returnedODM.setData(odmData);
            ofr.setReturnedDataMessage(returnedODM);
            return ofr;

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    private String getViewName(String query) {
        String result = "";
        query = query.replaceAll(",", " ").replaceAll("\n", " ").replace("\r", " ").replaceAll("'", " ").replaceAll("\t", " ").toLowerCase();
        int count = 0;
        for (int i = 0; i < query.length(); i++) {
            if (query.charAt(i) == '(') {
                count++;
            } else if (query.charAt(i) == ')') {
                count--;
            } else if (count == 0 && query.substring(i, i + 6).equals(" from ")) {
                result = query.substring(i).split(" ")[2];
                break;
            }
        }
        int open;
        if ((open = result.indexOf("(")) != -1) {

            result = result.substring(0, open);
        }
        return result;
    }

    private int getMainWhereIndex(String query) {
        query = query.replaceAll(",", " ").replaceAll("\n", " ").replace("\r", " ").replaceAll("'", " ").replaceAll("\t", " ").toLowerCase();
        if (query.contains(" where ")) {
            int count = 0;
            for (int i = 0; i < query.length(); i++) {
                if (query.charAt(i) == '(') {
                    count++;
                } else if (query.charAt(i) == ')') {
                    count--;
                } else if (count == 0 && query.substring(i, i + 7).equals(" where ")) {
                    return i + 1;
                }
            }
        }
        return -1;
    }

    private String getQueryWithOperator(String query, String operator, OUser loggedUser) {

        boolean isOperator = true;
        int indexOfOperator;
        String queryBeforeOperator;
        String queryAfterOperator;
        String finalQuery = "";
        List<String> selectStatements = new ArrayList<>();
        while (isOperator) {
            indexOfOperator = query.toLowerCase().indexOf(operator);
            queryBeforeOperator = query.substring(0, indexOfOperator);
            queryBeforeOperator = getQuery(queryBeforeOperator, loggedUser);
            selectStatements.add(queryBeforeOperator);
            queryAfterOperator = query.substring(indexOfOperator + operator.length());
            query = queryAfterOperator;
            if (!query.toLowerCase().contains(operator)) {
                isOperator = false;
            }
        }
        for (String selectStatement : selectStatements) {
            finalQuery += selectStatement + " " + operator + " ";
        }
        finalQuery += query;
        return finalQuery;
    }

    private boolean isDataSecurity(String viewName, String columnName, OUser loggedUser) {
        String viewColumnsQuery = "";
        String databaseConnection = getDBConnectionString(loggedUser).toLowerCase();
        boolean isExist = false;
        try {
            if (databaseConnection.contains("oracle")) {
                viewColumnsQuery = "SELECT column_name "
                        + "FROM USER_TAB_COLUMNS "
                        + "WHERE lower(table_name) = '" + viewName.toLowerCase() + "' and lower(column_name)='" + columnName.toLowerCase() + "'";
            } else if (databaseConnection.contains("mssql")) {
                viewColumnsQuery = "SELECT name"
                        + " FROM sys.columns"
                        + " WHERE object_id = OBJECT_ID('" + viewName + "')"
                        + " AND name = '" + columnName + "'";
            } else {
                String dataBaseName = getDatabaseName(databaseConnection);
                viewColumnsQuery = "SELECT COLUMN_NAME"
                        + " FROM INFORMATION_SCHEMA.COLUMNS"
                        + " WHERE TABLE_NAME ='" + viewName + "'"
                        + " AND TABLE_SCHEMA='" + dataBaseName + "'"
                        + " AND COLUMN_NAME='" + columnName + "';";
            }
            Object result = oem.executeEntityNativeQuery(viewColumnsQuery, loggedUser);
            if (result != null) {
                isExist = true;
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);

        }

        return isExist;
    }

    private String addCondition(String query, String viewName, String aliasName, String condition) {
        String tmpQuery = query.toLowerCase();
        int mainWhereIndex = getMainWhereIndex(query), order_groupIndex = -1;
        String queryAfterWhere, queryBeforeWhere, order_groupQuery = "", finalQuery = "";
        if (tmpQuery.contains("group by")) {
            order_groupIndex = tmpQuery.indexOf("group by");
        } else if (tmpQuery.contains("order by")) {
            order_groupIndex = tmpQuery.indexOf("order by");
        }
        if (order_groupIndex != -1) {
            if (isGroupForMainSelect(tmpQuery, viewName, order_groupIndex)) {
                order_groupQuery = query.substring(order_groupIndex);
                query = query.substring(0, order_groupIndex);
            }
        }
        if (mainWhereIndex == -1) {
            finalQuery = query + " where " + ((aliasName != null) ? aliasName : viewName) + "." + condition;

        } else {
            queryAfterWhere = query.substring(mainWhereIndex + 6);
            queryBeforeWhere = query.substring(0, mainWhereIndex + 6);
            queryAfterWhere = ((aliasName != null) ? aliasName : viewName) + "." + condition + " and " + queryAfterWhere;
            finalQuery = queryBeforeWhere + queryAfterWhere;
        }
        return finalQuery + " " + order_groupQuery;
    }

    @Override
    public String getQuery(String query, OUser loggedUser) {
        String viewName = getViewName(query);
        String aliasName = getAliasName(viewName, query);
        for (Map.Entry<String, List<String>> dataSecurityConditions : dataSecurityServiceLocal.getDataSecurityCondition(loggedUser).entrySet()) {
            String entityName = dataSecurityConditions.getKey();
            List<String> entityCondition = dataSecurityConditions.getValue();
            for (int i = 0; i < entityCondition.size(); i++) {
                boolean isDataSecurity = isDataSecurity(viewName, entityName.toLowerCase() + "_dbid", loggedUser);
                if (isDataSecurity) {
                    query = addCondition(query, viewName, aliasName, entityName + "_dbid in " + entityCondition.get(i));
                }
            }
        }
        return query;
    }

    private String getAliasName(String viewName, String query) {
        int LENGTH_FROM = 6;

        query = query.replaceAll("\n", " ").replace("\r", " ").replaceAll("'", " ").replaceAll("\t", " ").toLowerCase();
        int indexOfTable = query.lastIndexOf(" from " + viewName) + LENGTH_FROM;

        query = query.substring(indexOfTable + viewName.length()).trim();
        String[] result = query.split(" ");
        int indexOfComma;
        if (!result[0].equals("")) {
            if (result[0].equals("where") || result[0].equals("group") || result[0].equals("order") || result[0].equals("join") || result[0].equals("left")
                    || result[0].equals("right") || result[0].equals("outter") || query.charAt(0) == ',') {
                return null;
            } else if (result[0].equals("as")) {
                if ((indexOfComma = result[1].indexOf(",")) != -1) {
                    return result[1].substring(0, indexOfComma);
                }
                return result[1];
            } else {
                if ((indexOfComma = result[0].indexOf(",")) != -1) {
                    return result[0].substring(0, indexOfComma);
                }
                return result[0];
            }
        } else {
            return null;
        }
    }

    private boolean isGroupForMainSelect(String query, String viewName, int order_groupIndex) {
        int indexOfMainTable = query.indexOf(" from " + viewName);
        if (indexOfMainTable < order_groupIndex) {
            return true;
        }
        return false;
    }

    private String getDatabaseName(String url) {
        String modifiedURL = url.substring(url.indexOf(":"));
        String dbName = modifiedURL.substring(modifiedURL.indexOf("/"));
        int dbNameEndIndex = modifiedURL.indexOf("?");
        if (dbNameEndIndex > 0) {
            dbName = dbName.substring(0, dbNameEndIndex);
        }
        return dbName;
    }

    @Override
    public String applyReportSecuirty(String newSelect, OUser loggedUser) {
        if (newSelect.toLowerCase().contains("union")) {
            newSelect = getQueryWithOperator(newSelect, "union", loggedUser);
        } else if (newSelect.toLowerCase().contains("intersect")) {
            newSelect = getQueryWithOperator(newSelect, "intersect", loggedUser);
        } else {
            newSelect = getQuery(newSelect, loggedUser);
        }
        return newSelect;
    }
}
