package com.unitedofoq.otms.recruitment.jobrequisition;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccupationElementRatingBase;
import com.unitedofoq.otms.foundation.occupation.Ability;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobRequisition"})
public class JobReqAbility extends OccupationElementRatingBase {
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="ability_DBID", nullable=false)
    private Ability ability;
    public String getAbilityDD(){
        return "JobReqAbility_ability";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobRequisition_DBID", nullable=false)
    private JobRequisition jobRequisition;

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }
}