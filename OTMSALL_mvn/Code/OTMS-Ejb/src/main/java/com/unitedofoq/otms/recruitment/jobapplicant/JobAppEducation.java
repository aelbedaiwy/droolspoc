
package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.validation.DateFromToValidation;
import com.unitedofoq.otms.foundation.occupation.Education;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DateFromToValidation (from={"fromDate"}, to={"toDate"})
@ParentEntity(fields={"jobApplicant"})
public class JobAppEducation extends Education {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
	private JobApplicant jobApplicant;

	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobapplicant(JobApplicant theJobapplicant) {
		jobApplicant = theJobapplicant;
	}
}
