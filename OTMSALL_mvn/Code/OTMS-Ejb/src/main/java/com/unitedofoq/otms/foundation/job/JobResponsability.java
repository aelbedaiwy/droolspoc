/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.job;

import com.unitedofoq.otms.foundation.training.Responsability;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"job"})
public class JobResponsability extends Responsability {
// <editor-fold defaultstate="collapsed" desc="job">
	@JoinColumn(nullable=false)
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Job job;
    public Job getJob() {
        return job;
    }
    public void setJob(Job job) {
        this.job = job;
    }
    public String getJobDD() {
        return "JobResponsability_job";
    }
    // </editor-fold>
}