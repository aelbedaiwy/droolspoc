package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.PaymentMethod;
import com.unitedofoq.otms.payroll.PaymentPeriod;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class EmployeeSalaryElementBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="checked">

    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getCheckedDD() {
        return "EmployeeSalaryElement_checked";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="checkedEmployee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee checkedEmployee;

    public Employee getCheckedEmployee() {
        return checkedEmployee;
    }

    public void setCheckedEmployee(Employee checkedEmployee) {
        this.checkedEmployee = checkedEmployee;
    }

    public String getCheckedEmployeeDD() {
        return "EmployeeSalaryElement_checkedEmployee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="checkerDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkerDate;

    public Date getCheckerDate() {
        return checkerDate;
    }

    public void setCheckerDate(Date checkerDate) {
        this.checkerDate = checkerDate;
    }

    public String getCheckerDateDD() {
        return "EmployeeSalaryElement_checkerDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entryMethod">
    @Column(length = 1) //FIXME: to be udc
    private String entryMethod;

    public String getEntryMethod() {
        return entryMethod;
    }

    public void setEntryMethod(String elementEntryMethod) {
        this.entryMethod = elementEntryMethod;
    }

    public String getEntryMethodDD() {
        return "EmployeeSalaryElement_entryMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="value">
    @Column(precision = 25, scale = 13)
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal elementValue) {
        this.value = elementValue;
    }

    public String getValueDD() {
        return "EmployeeSalaryElement_value";
    }
    @Transient
    private BigDecimal valueMask;

    public BigDecimal getValueMask() {
        valueMask = value;
        return valueMask;
    }

    public void setValueMask(BigDecimal valueMask) {
        updateDecimalValue("value", valueMask);
    }

    public String getValueMaskDD() {
        return "EmployeeSalaryElement_valueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="excessValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal excessValue;

    public BigDecimal getExcessValue() {
        return excessValue;
    }

    public void setExcessValue(BigDecimal excessValue) {
        this.excessValue = excessValue;
    }

    public String getExcessValueDD() {
        return "EmployeeSalaryElement_excessValue";
    }
    @Transient
    private BigDecimal excessValueMask;

    public BigDecimal getExcessValueMask() {
        excessValueMask = excessValue;
        return excessValueMask;
    }

    public void setExcessValueMask(BigDecimal excessValueMask) {
        updateDecimalValue("excessValue", excessValueMask);
    }

    public String getExcessValueMaskDD() {
        return "EmployeeSalaryElement_excessValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="holdFromDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date holdFromDate;

    public Date getHoldFromDate() {
        return holdFromDate;
    }

    public void setHoldFromDate(Date holdFromDate) {
        this.holdFromDate = holdFromDate;
    }

    public String getHoldFromDateDD() {
        return "EmployeeSalaryElement_holdFromDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="holdToDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date holdToDate;

    public Date getHoldToDate() {
        return holdToDate;
    }

    public void setHoldToDate(Date holdToDate) {
        this.holdToDate = holdToDate;
    }

    public String getHoldToDateDD() {
        return "EmployeeSalaryElement_holdToDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="makerDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date makerDate;

    public Date getMakerDate() {
        return makerDate;
    }

    public void setMakerDate(Date makerDate) {
        this.makerDate = makerDate;
    }

    public String getMakerDateDD() {
        return "EmployeeSalaryElement_makerDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maker">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee maker;

    public Employee getMaker() {
        return maker;
    }

    public void setMaker(Employee maker) {
        this.maker = maker;
    }

    public String getMakerDD() {
        return "EmployeeSalaryElement_maker";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="makerValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal makerValue;

    public BigDecimal getMakerValue() {
        return makerValue;
    }

    public void setMakerValue(BigDecimal makerValue) {
        this.makerValue = makerValue;
    }

    public String getMakerValueDD() {
        return "EmployeeSalaryElement_makerValue";
    }
    @Transient
    private BigDecimal makerValueMask;

    public BigDecimal getMakerValueMask() {
        makerValueMask = makerValue;
        return makerValueMask;
    }

    public void setMakerValueMask(BigDecimal makerValueMask) {
        updateDecimalValue("makerValue", makerValueMask);
    }

    public String getMakerValueMaskDD() {
        return "EmployeeSalaryElement_makerValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="modificationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModificationDateDD() {
        return "EmployeeSalaryElement_modificationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthly">
    @Column(nullable = false, length = 1)
    private String monthly;

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getMonthlyDD() {
        return "EmployeeSalaryElement_monthly";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthlyElementValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal monthlyElementValue;

    public BigDecimal getMonthlyElementValue() {
        return monthlyElementValue;
    }

    public void setMonthlyElementValue(BigDecimal monthlyElementValue) {
        this.monthlyElementValue = monthlyElementValue;
    }

    public String getMonthlyElementValueDD() {
        return "EmployeeSalaryElement_monthlyElementValue";
    }
    @Transient
    private BigDecimal monthlyElementValueMask;

    public BigDecimal getMonthlyElementValueMask() {
        monthlyElementValueMask = monthlyElementValue;
        return monthlyElementValueMask;
    }

    public void setMonthlyElementValueMask(BigDecimal monthlyElementValueMask) {
        updateDecimalValue("monthlyElementValue", monthlyElementValueMask);
    }

    public String getMonthlyElementValueMaskDD() {
        return "EmployeeSalaryElement_monthlyElementValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="operator">
    @Column(length = 1) //FIXME: make sure it's not UDC
    private String operator;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorDD() {
        return "EmployeeSalaryElement_operator";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private PaymentPeriod paymentPeriod;

    public PaymentPeriod getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(PaymentPeriod paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public String getPaymentPeriodDD() {
        return "EmployeeSalaryElement_paymentPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod PaymentMethod) {
        this.paymentMethod = PaymentMethod;
    }

    public String getPaymentMethodDD() {
        return "EmployeeSalaryElement_paymentMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentMethodBack">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PaymentMethod paymentMethodBack;

    public PaymentMethod getPaymentMethodBack() {
        return paymentMethodBack;
    }

    public void setPaymentMethodBack(PaymentMethod paymentMethodBack) {
        this.paymentMethodBack = paymentMethodBack;
    }

    public String getPaymentMethodBackDD() {
        return "EmployeeSalaryElement_paymentMethodBack";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payPeriodBack">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private PaymentPeriod paymentPeriodBack;

    public PaymentPeriod getPaymentPeriodBack() {
        return paymentPeriodBack;
    }

    public void setPaymentPeriodBack(PaymentPeriod paymentPeriodBack) {
        this.paymentPeriodBack = paymentPeriodBack;
    }

    public String getPaymentPeriodBackDD() {
        return "EmployeeSalaryElement_payPeriodBack";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="retroactiveValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal retroactiveValue;

    public BigDecimal getRetroactiveValue() {
        return retroactiveValue;
    }

    public void setRetroactiveValue(BigDecimal retroactiveValue) {
        this.retroactiveValue = retroactiveValue;
    }

    public String getRetroactiveValueDD() {
        return "EmployeeSalaryElement_retroactiveValue";
    }
    @Transient
    private BigDecimal retroactiveValueMask;

    public BigDecimal getRetroactiveValueMask() {
        retroactiveValueMask = retroactiveValue;
        return retroactiveValueMask;
    }

    public void setRetroactiveValueMask(BigDecimal retroactiveValueMask) {
        updateDecimalValue("retroactiveValue", retroactiveValueMask);
    }

    public String getRetroactiveValueMaskDD() {
        return "EmployeeSalaryElement_retroactiveValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable = false)
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "EmployeeSalaryElement_sortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="whatIfValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal whatIfValue;

    public BigDecimal getWhatIfValue() {
        return whatIfValue;
    }

    public void setWhatIfValue(BigDecimal whatIfValue) {
        this.whatIfValue = whatIfValue;
    }

    public String getWhatIfValueDD() {
        return "EmployeeSalaryElement_whatIfValue";
    }
    @Transient
    private BigDecimal whatIfValueMask;

    public BigDecimal getWhatIfValueMask() {
        whatIfValueMask = whatIfValue;
        return whatIfValueMask;
    }

    public void setWhatIfValueMask(BigDecimal whatIfValueMask) {
        updateDecimalValue("whatIfValue", whatIfValueMask);
    }

    public String getWhatIfValueMaskDD() {
        return "EmployeeSalaryElement_whatIfValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "EmployeeSalaryElement_salaryElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "EmployeeSalaryElement_currency";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="valueEnc">
    private String valueEnc;

    public String getValueEnc() {
        return valueEnc;
    }

    public void setValueEnc(String valueEnc) {
        this.valueEnc = valueEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="makerValueEnc">
    private String makerValueEnc;

    public String getMakerValueEnc() {
        return makerValueEnc;
    }

    public void setMakerValueEnc(String makerValueEnc) {
        this.makerValueEnc = makerValueEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthlyElementValueEnc">
    private String monthlyElementValueEnc;

    public String getMonthlyElementValueEnc() {
        return monthlyElementValueEnc;
    }

    public void setMonthlyElementValueEnc(String monthlyElementValueEnc) {
        this.monthlyElementValueEnc = monthlyElementValueEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="transactionType">
    private String transactionType;

    public String getTransactionType() {
        return transactionType;
    }

    public String getTransactionTypeDD() {
        return "EmployeeSalaryElement_TransactionType";
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="fromImport">
    @Transient
    private boolean fromImport = false;

    public boolean isFromImport() {
        return fromImport;
    }

    public void setFromImport(boolean fromImport) {
        this.fromImport = fromImport;
    }
    // </editor-fold >
}
