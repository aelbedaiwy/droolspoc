package com.unitedofoq.otms.payroll.insurance;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;


/**
 * 
 */
@Entity
@ParentEntity(fields={"insurance"})
public class InsuranceGroupMember extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
	 @Column(nullable=false)
	private int sortIndex;
    public int getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "InsuranceGroupMember_sortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insurance">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Insurance insurance;
    public Insurance getInsurance() {
        return insurance;
    }
    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }
    public String getInsuranceDD() {
        return "InsuranceGroupMember_insurance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private InsuranceGroup insuranceGroup;

    public InsuranceGroup getInsuranceGroup() {
        return insuranceGroup;
    }

    public void setInsuranceGroup(InsuranceGroup insuranceGroup) {
        this.insuranceGroup = insuranceGroup;
    }

    public String getInsuranceGroupDD() {
        return "InsuranceGroupMember_insuranceGroup";
    }
    // </editor-fold >
}
