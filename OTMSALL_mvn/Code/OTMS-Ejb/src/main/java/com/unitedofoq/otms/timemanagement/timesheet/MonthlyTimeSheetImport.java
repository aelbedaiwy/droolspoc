package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class MonthlyTimeSheetImport extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "MonthlyTimeSheetImport_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="month">
    private String month;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMonthDD() {
        return "MonthlyTimeSheetImport_month";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="year">
    private String year;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYearDD() {
        return "MonthlyTimeSheetImport_year";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d1">
    private String d1;

    public String getD1() {
        return d1;
    }

    public void setD1(String d) {
        d1 = d;
    }

    public String getD1DD() {
        return "MonthlyTimeSheetImport_d1";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d2">
    private String d2;

    public String getD2() {
        return d2;
    }

    public void setD2(String d) {
        d2 = d;
    }

    public String getD2DD() {
        return "MonthlyTimeSheetImport_d2";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d3">
    private String d3;

    public String getD3() {
        return d3;
    }

    public void setD3(String d) {
        d3 = d;
    }

    public String getD3DD() {
        return "MonthlyTimeSheetImport_d3";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d4">
    private String d4;

    public String getD4() {
        return d4;
    }

    public void setD4(String d) {
        d4 = d;
    }

    public String getD4DD() {
        return "MonthlyTimeSheetImport_d4";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d5">
    private String d5;

    public String getD5() {
        return d5;
    }

    public void setD5(String d) {
        d5 = d;
    }

    public String getD5DD() {
        return "MonthlyTimeSheetImport_d5";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d6">
    private String d6;

    public String getD6() {
        return d6;
    }

    public void setD6(String d) {
        d6 = d;
    }

    public String getD6DD() {
        return "MonthlyTimeSheetImport_d6";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d7">
    private String d7;

    public String getD7() {
        return d7;
    }

    public void setD7(String d) {
        d7 = d;
    }

    public String getD7DD() {
        return "MonthlyTimeSheetImport_d7";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d8">
    private String d8;

    public String getD8() {
        return d8;
    }

    public void setD8(String d) {
        d8 = d;
    }

    public String getD8DD() {
        return "MonthlyTimeSheetImport_d8";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d9">
    private String d9;

    public String getD9() {
        return d9;
    }

    public void setD9(String d) {
        d9 = d;
    }

    public String getD9DD() {
        return "MonthlyTimeSheetImport_d9";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d10">
    private String d10;

    public String getD10() {
        return d10;
    }

    public void setD10(String d) {
        d10 = d;
    }

    public String getD10DD() {
        return "MonthlyTimeSheetImport_d10";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d11">
    private String d11;

    public String getD11() {
        return d11;
    }

    public void setD11(String d) {
        d11 = d;
    }

    public String getD11DD() {
        return "MonthlyTimeSheetImport_d11";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d12">
    private String d12;

    public String getD12() {
        return d12;
    }

    public void setD12(String d) {
        d12 = d;
    }

    public String getD12DD() {
        return "MonthlyTimeSheetImport_d12";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d13">
    private String d13;

    public String getD13() {
        return d13;
    }

    public void setD13(String d) {
        d13 = d;
    }

    public String getD13DD() {
        return "MonthlyTimeSheetImport_d13";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d14">
    private String d14;

    public String getD14() {
        return d14;
    }

    public void setD14(String d) {
        d14 = d;
    }

    public String getD14DD() {
        return "MonthlyTimeSheetImport_d14";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d15">
    private String d15;

    public String getD15() {
        return d15;
    }

    public void setD15(String d) {
        d15 = d;
    }

    public String getD15DD() {
        return "MonthlyTimeSheetImport_d15";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d16">
    private String d16;

    public String getD16() {
        return d16;
    }

    public void setD16(String d) {
        d16 = d;
    }

    public String getD16DD() {
        return "MonthlyTimeSheetImport_d16";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d17">
    private String d17;

    public String getD17() {
        return d17;
    }

    public void setD17(String d) {
        d17 = d;
    }

    public String getD17DD() {
        return "MonthlyTimeSheetImport_d17";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d18">
    private String d18;

    public String getD18() {
        return d18;
    }

    public void setD18(String d) {
        d18 = d;
    }

    public String getD18DD() {
        return "MonthlyTimeSheetImport_d18";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d19">
    private String d19;

    public String getD19() {
        return d19;
    }

    public void setD19(String d) {
        d19 = d;
    }

    public String getD19DD() {
        return "MonthlyTimeSheetImport_d19";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d20">
    private String d20;

    public String getD20() {
        return d20;
    }

    public void setD20(String d) {
        d20 = d;
    }

    public String getD20DD() {
        return "MonthlyTimeSheetImport_d20";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d21">
    private String d21;

    public String getD21() {
        return d21;
    }

    public void setD21(String d) {
        d21 = d;
    }

    public String getD21DD() {
        return "MonthlyTimeSheetImport_d21";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d22">
    private String d22;

    public String getD22() {
        return d22;
    }

    public void setD22(String d) {
        d22 = d;
    }

    public String getD22DD() {
        return "MonthlyTimeSheetImport_d22";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d23">
    private String d23;

    public String getD23() {
        return d23;
    }

    public void setD23(String d) {
        d23 = d;
    }

    public String getD23DD() {
        return "MonthlyTimeSheetImport_d23";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d24">
    private String d24;

    public String getD24() {
        return d24;
    }

    public void setD24(String d) {
        d24 = d;
    }

    public String getD24DD() {
        return "MonthlyTimeSheetImport_d24";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d25">
    private String d25;

    public String getD25() {
        return d25;
    }

    public void setD25(String d) {
        d25 = d;
    }

    public String getD25DD() {
        return "MonthlyTimeSheetImport_d25";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d26">
    private String d26;

    public String getD26() {
        return d26;
    }

    public void setD26(String d) {
        d26 = d;
    }

    public String getD26DD() {
        return "MonthlyTimeSheetImport_d26";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d27">
    private String d27;

    public String getD27() {
        return d27;
    }

    public void setD27(String d) {
        d27 = d;
    }

    public String getD27DD() {
        return "MonthlyTimeSheetImport_d27";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d28">
    private String d28;

    public String getD28() {
        return d28;
    }

    public void setD28(String d) {
        d28 = d;
    }

    public String getD28DD() {
        return "MonthlyTimeSheetImport_d28";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d29">
    private String d29;

    public String getD29() {
        return d29;
    }

    public void setD29(String d) {
        d29 = d;
    }

    public String getD29DD() {
        return "MonthlyTimeSheetImport_d29";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d30">
    private String d30;

    public String getD30() {
        return d30;
    }

    public void setD30(String d) {
        d30 = d;
    }

    public String getD30DD() {
        return "MonthlyTimeSheetImport_d30";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="d31">
    private String d31;

    public String getD31() {
        return d31;
    }

    public void setD31(String d) {
        d31 = d;
    }

    public String getD31DD() {
        return "MonthlyTimeSheetImport_d31";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="recNum">
    private int recNum;

    public int getRecNum() {
        return recNum;
    }

    public void setRecNum(int recNum) {
        this.recNum = recNum;
    }

    public String getRecNumDD() {
        return "MonthlyTimeSheetImport_recNum";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="posted">
     private boolean posted;

     public boolean isPosted() {
     return posted;
     }

     public void setPosted(boolean posted) {
     this.posted = posted;
     }
    
     public String getPostedDD() {
     return "MonthlyTimeSheetImport_posted";
     }
     // </editor-fold>
}