package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class RepEmployeeBase extends RepCompanyLevelBase {
    //<editor-fold defaultstate="collapsed" desc="employeeCode">

    @Column
    private String employeeCode;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "RepEmployee_employeeCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeCodeEnc">
    @Column
    private String employeeCodeEnc;

    public String getEmployeeCodeEnc() {
        return employeeCodeEnc;
    }

    public void setEmployeeCodeEnc(String employeeCodeEnc) {
        this.employeeCodeEnc = employeeCodeEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    @Translatable(translationField = "firstNameTranslated")
    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstNameDD() {
        return "RepEmployee_firstName";
    }
    @Transient
    @Translation(originalField = "firstName")
    private String firstNameTranslated;

    public String getFirstNameTranslated() {
        return firstNameTranslated;
    }

    public void setFirstNameTranslated(String firstNameTranslated) {
        this.firstNameTranslated = firstNameTranslated;
    }

    public String getFirstNameTranslatedDD() {
        return "RepEmployee_firstName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    @Translatable(translationField = "middleNameTranslated")
    private String middleName;

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleNameDD() {
        return "RepEmployee_middleName";
    }
    @Transient
    @Translation(originalField = "middleName")
    private String middleNameTranslated;

    public String getMiddleNameTranslated() {
        return middleNameTranslated;
    }

    public void setMiddleNameTranslated(String middleNameTranslated) {
        this.middleNameTranslated = middleNameTranslated;
    }

    public String getMiddleNameTranslatedDD() {
        return "RepEmployee_middleName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    @Translatable(translationField = "lastNameTranslated")
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastNameDD() {
        return "RepEmployee_lastName";
    }
    @Transient
    @Translation(originalField = "lastName")
    private String lastNameTranslated;

    public String getLastNameTranslated() {
        return lastNameTranslated;
    }

    public void setLastNameTranslated(String lastNameTranslated) {
        this.lastNameTranslated = lastNameTranslated;
    }

    public String getLastNameTranslatedDD() {
        return "RepEmployee_lastName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    @Translatable(translationField = "fullNameTranslated")
    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullNameDD() {
        return "RepEmployee_fullName";
    }
    @Transient
    @Translation(originalField = "fullName")
    private String fullNameTranslated;

    public String getFullNameTranslated() {
        return fullNameTranslated;
    }

    public void setFullNameTranslated(String fullNameTranslated) {
        this.fullNameTranslated = fullNameTranslated;
    }

    public String getFullNameTranslatedDD() {
        return "RepEmployee_fullName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    @Translatable(translationField = "positionNameTranslated")
    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionNameDD() {
        return "RepEmployee_positionName";
    }
    @Transient
    @Translation(originalField = "positionName")
    private String positionNameTranslated;

    public String getPositionNameTranslated() {
        return positionNameTranslated;
    }

    public void setPositionNameTranslated(String positionNameTranslated) {
        this.positionNameTranslated = positionNameTranslated;
    }

    public String getPositionNameTranslatedDD() {
        return "RepEmployee_positionName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    @Translatable(translationField = "locationDescriptionTranslated")
    private String locationDescription;

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescriptionDD() {
        return "RepEmployee_locationDescription";
    }
    @Transient
    @Translation(originalField = "locationDescription")
    private String locationDescriptionTranslated;

    public String getLocationDescriptionTranslated() {
        return locationDescriptionTranslated;
    }

    public void setLocationDescriptionTranslated(String locationDescriptionTranslated) {
        this.locationDescriptionTranslated = locationDescriptionTranslated;
    }

    public String getLocationDescriptionTranslatedDD() {
        return "RepEmployee_locationDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    @Translatable(translationField = "costCenterDescriptionTranslated")
    private String costCenterDescription;

    public String getCostCenterDescription() {
        return costCenterDescription;
    }

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescriptionDD() {
        return "RepEmployee_costCenterDescription";
    }
    @Transient
    @Translation(originalField = "costCenterDescription")
    private String costCenterDescriptionTranslated;

    public String getCostCenterDescriptionTranslated() {
        return costCenterDescriptionTranslated;
    }

    public void setCostCenterDescriptionTranslated(String costCenterDescriptionTranslated) {
        this.costCenterDescriptionTranslated = costCenterDescriptionTranslated;
    }

    public String getCostCenterDescriptionTranslatedDD() {
        return "RepEmployee_costCenterDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    @Translatable(translationField = "payGradeDescriptionTranslated")
    private String payGradeDescription;

    public String getPayGradeDescription() {
        return payGradeDescription;
    }

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescriptionDD() {
        return "RepEmployee_payGradeDescription";
    }
    @Transient
    @Translation(originalField = "payGradeDescription")
    private String payGradeDescriptionTranslated;

    public String getPayGradeDescriptionTranslated() {
        return payGradeDescriptionTranslated;
    }

    public void setPayGradeDescriptionTranslated(String payGradeDescriptionTranslated) {
        this.payGradeDescriptionTranslated = payGradeDescriptionTranslated;
    }

    public String getPayGradeDescriptionTranslatedDD() {
        return "RepEmployee_payGradeDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    @Translatable(translationField = "unitNameTranslated")
    private String unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitNameDD() {
        return "RepEmployee_unitName";
    }
    @Transient
    @Translation(originalField = "unitName")
    private String unitNameTranslated;

    public String getUnitNameTranslated() {
        return unitNameTranslated;
    }

    public void setUnitNameTranslated(String unitNameTranslated) {
        this.unitNameTranslated = unitNameTranslated;
    }

    public String getUnitNameTranslatedDD() {
        return "RepEmployee_unitName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    @Translatable(translationField = "jobNameTranslated")
    private String jobName;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobNameDD() {
        return "RepEmployee_jobName";
    }
    @Transient
    @Translation(originalField = "jobName")
    private String jobNameTranslated;

    public String getJobNameTranslated() {
        return jobNameTranslated;
    }

    public void setJobNameTranslated(String jobNameTranslated) {
        this.jobNameTranslated = jobNameTranslated;
    }

    public String getJobNameTranslatedDD() {
        return "RepEmployee_jobName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empCodeName">
    @Column
    private String empCodeName;

    public String getEmpCodeName() {
        return empCodeName;
    }

    public void setEmpCodeName(String empCodeName) {
        this.empCodeName = empCodeName;
    }

    public String getEmpCodeNameDD() {
        return "RepEmployee_empCodeName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empNameCode">
    @Column
    private String empNameCode;

    public String getEmpNameCode() {
        return empNameCode;
    }

    public void setEmpNameCode(String empNameCode) {
        this.empNameCode = empNameCode;
    }

    public String getEmpNameCodeDD() {
        return "RepEmployee_empNameCode";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    @Translatable(translationField = "hiringTypeTranslated")
    private String hiringType;

    public String getHiringType() {
        return hiringType;
    }

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringTypeDD() {
        return "RepEmployee_hiringType";
    }
    @Transient
    @Translation(originalField = "hiringType")
    private String hiringTypeTranslated;

    public String getHiringTypeTranslated() {
        return hiringTypeTranslated;
    }

    public void setHiringTypeTranslated(String hiringTypeTranslated) {
        this.hiringTypeTranslated = hiringTypeTranslated;
    }

    public String getHiringTypeTranslatedDD() {
        return "RepEmployee_hiringType";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="unit_dbid">
    // unit_dbid for unit role in reports
    private long unit_dbid;

    public long getUnit_dbid() {
        return unit_dbid;
    }

    public void setUnit_dbid(long unit_dbid) {
        this.unit_dbid = unit_dbid;
    }
    // </editor-fold>
}
