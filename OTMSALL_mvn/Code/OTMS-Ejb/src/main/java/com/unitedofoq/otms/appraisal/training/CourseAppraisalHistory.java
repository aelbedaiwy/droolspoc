package com.unitedofoq.otms.appraisal.training;

import com.unitedofoq.otms.appraisal.employee.AppraisalHistoryBase;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class CourseAppraisalHistory extends AppraisalHistoryBase {


    //<editor-fold defaultstate="collapsed" desc="courseInformation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation courseInformation;

    public ProviderCourseInformation getCourseInformation() {
        return courseInformation;
    }

    public void setCourseInformation(ProviderCourseInformation courseInformation) {
        this.courseInformation = courseInformation;
    }

    public String getCourseInformationDD() {
        return "CourseAppraisalHistory_courseInformation";
    }
    //</editor-fold>

}
