package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.currency.Currency;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class PersonnelExperienceBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="experienceType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC experienceType;
    public UDC getExperienceType() {
        return experienceType;
    }
    public void setExperienceType(UDC experienceType) {
        this.experienceType = experienceType;
    }
        public String getExperienceTypeDD(){
        return "ApplicantExperience_experienceType";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyDescription">
    @Column
    private String companyDescription;
    public String getCompanyDescription() {
        return companyDescription;
    }
    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }
    public String getCompanyDescriptionDD() {
        return "ApplicantExperience_companyDescription";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobDescription">
    @Column
    private String jobDescription;
    public String getJobDescription() {
        return jobDescription;
    }
    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }
    public String getJobDescriptionDD() {
        return "ApplicantExperience_jobDescription";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endOfServiceReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC endOfServiceReason;
    public UDC getEndOfServiceReason() {
        return endOfServiceReason;
    }
    public void setEndOfServiceReason(UDC endOfServiceReason) {
        this.endOfServiceReason = endOfServiceReason;
    }
    public String getEndOfServiceReasonDD(){
        return "ApplicantExperience_endOfServiceReason";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    public Date getFromDate() {
        return fromDate;
    }
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }
    public String getFromDateDD() {
        return "ApplicantExperience_fromDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    public Date getToDate() {
        return toDate;
    }
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
    public String getToDateDD() {
        return "ApplicantExperience_toDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceCompany">
    @Column
    private String insuranceCompany;
    public String getInsuranceCompany() {
        return insuranceCompany;
    }
    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }
    public String getInsuranceCompanyDD() {
        return "ApplicantExperience_insuranceCompany";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceDescription">
    @Column
    private String insuranceDescription;
    public String getInsuranceDescription() {
        return insuranceDescription;
    }
    public void setInsuranceDescription(String insuranceDescription) {
        this.insuranceDescription = insuranceDescription;
    }
    public String getInsuranceDescriptionDD() {
        return "ApplicantExperience_insuranceDescription";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceNumber">
    @Column
    private String insuranceNumber;
    public String getInsuranceNumber() {
        return insuranceNumber;
    }
    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }
    public String getInsuranceNumberDD() {
        return "ApplicantExperience_insuranceNumber";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceStatus">
    @JoinColumn
    private UDC insuranceStatus;
    public UDC getInsuranceStatus() {
        return insuranceStatus;
    }
    public void setInsuranceStatus(UDC insuranceStatus) {
        this.insuranceStatus = insuranceStatus;
    }
    public String getInsuranceStatusDD() {
        return "ApplicantExperience_insuranceStatus";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
	@Column(nullable=true)
	private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getNotesDD() {
        return "ApplicantExperience_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="country">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC country;
    public UDC getCountry() {
        return country;
    }
    public void setCountry(UDC country) {
        this.country = country;
    }
    public String getCountryDD(){
        return "ApplicantExperience_country";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getCurrencyDD() {
        return "ApplicantExperience_currency";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingPeriod">
    @Transient
    private BigDecimal workingPeriod = BigDecimal.ZERO;

    public BigDecimal getWorkingPeriod() {
        if( (fromDate !=null ) && (toDate != null))
        {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            long milis1;
            long milis2;
            double diff;
            double diffDays;
            //divisor = new BigDecimal(24 * 60 * 60 * 1000);
           // diff = new BigDecimal(0);
            //diffDays = new BigDecimal(0);
            //Calculate Days
            cal1.setTime(toDate);
            cal1.add(Calendar.DAY_OF_MONTH, 1);
            cal2.setTime(fromDate);
            // Get the represented date in milliseconds
            milis1 = cal1.getTimeInMillis();
            milis2 = cal2.getTimeInMillis();
            // Calculate difference in milliseconds
            diff = milis2 - milis1;
            // Calculate difference in hours
            diffDays = diff/(24 * 60 * 60 * 1000);//.divide(divisor);
            workingPeriod = new BigDecimal(diffDays);
        }
        return workingPeriod;
    }

    public void setWorkingPeriod(BigDecimal workingPeriod) {
        this.workingPeriod = workingPeriod;
    }

    public String getWorkingPeriodDD() {
        return "PersonnelExperienceBase_workingPeriod";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="companySize">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC companySize;

    public UDC getCompanySize() {
        return companySize;
    }

    public void setCompanySize(UDC companySize) {
        this.companySize = companySize;
    }

    public String getCompanySizeDD() {
        return "PersonnelExperienceBase_companySize";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyWebSite">
    private String companyWebSite;

    public String getCompanyWebSite() {
        return companyWebSite;
    }

    public void setCompanyWebSite(String companyWebSite) {
        this.companyWebSite = companyWebSite;
    }

    public String getCompanyWebSiteDD() {
        return "PersonnelExperienceBase_companyWebSite";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyIndustry">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC companyIndustry;

    public UDC getCompanyIndustry() {
        return companyIndustry;
    }

    public void setCompanyIndustry(UDC companyIndustry) {
        this.companyIndustry = companyIndustry;
    }
    public String getCompanyIndustryDD() {
        return "PersonnelExperienceBase_companyIndustry";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobRole">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC jobRole;

    public UDC getJobRole() {
        return jobRole;
    }

    public void setJobRole(UDC jobRole) {
        this.jobRole = jobRole;
    }

    public String getJobRoleDD() {
        return "PersonnelExperienceBase_jobRole";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="careerLevel">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC careerLevel;

    public UDC getCareerLevel() {
        return careerLevel;
    }

    public void setCareerLevel(UDC careerLevel) {
        this.careerLevel = careerLevel;
    }

    public String getCareerLevelDD() {
        return "PersonnelExperienceBase_careerLevel";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="city">
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityDD() {
        return "PersonnelExperienceBase_city";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "PersonnelExperienceBase_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startingSalary">
    @Column(precision=25, scale=13)
    private BigDecimal startingSalary;

    public BigDecimal getStartingSalary() {
        return startingSalary;
    }

    public void setStartingSalary(BigDecimal startingSalary) {
        this.startingSalary = startingSalary;
    }

    public String getStartingSalaryDD() {
        return "PersonnelExperienceBase_startingSalary";
    }
    @Transient
    private BigDecimal startingSalaryMask = java.math.BigDecimal.ZERO;
    public BigDecimal getStartingSalaryMask() {
        startingSalaryMask = startingSalary ;
        return startingSalaryMask;
    }
    public void setStartingSalaryMask(BigDecimal startingSalaryMaskMask) {
        updateDecimalValue("startingSalary",startingSalaryMask);
    }
    public String getStartingSalaryMaskDD() {
        return "PersonnelExperienceBase_startingSalaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endingSalary">
    @Column(precision=25, scale=13)
    private BigDecimal endingSalary;

    public BigDecimal getEndingSalary() {
        return endingSalary;
    }

    public void setEndingSalary(BigDecimal endingSalary) {
        this.endingSalary = endingSalary;
    }

    public String getEndingSalaryDD() {
        return "PersonnelExperienceBase_endingSalary";
    }

    @Transient
    private BigDecimal endingSalaryMask = java.math.BigDecimal.ZERO;
    public BigDecimal getEndingSalaryMask() {
        endingSalaryMask = endingSalary ;
        return endingSalaryMask;
    }
    public void setEndingSalaryMask(BigDecimal endingSalaryMaskMask) {
        updateDecimalValue("endingSalary",endingSalaryMask);
    }
    public String getEndingSalaryMaskDD() {
        return "PersonnelExperienceBase_endingSalaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryOtherInfo">
    private String salaryOtherInfo;

    public String getSalaryOtherInfo() {
        return salaryOtherInfo;
    }

    public void setSalaryOtherInfo(String salaryOtherInfo) {
        this.salaryOtherInfo = salaryOtherInfo;
    }

    public String getSalaryOtherInfoDD() {
        return "PersonnelExperienceBase_salaryOtherInfo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryPerPeriod">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC salaryPerPeriod;

    public UDC getSalaryPerPeriod() {
        return salaryPerPeriod;
    }

    public void setSalaryPerPeriod(UDC salaryPerPeriod) {
        this.salaryPerPeriod = salaryPerPeriod;
    }

    public String getSalaryPerPeriodDD() {
        return "PersonnelExperienceBase_salaryPerPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingCurrently">
    private boolean workingCurrently;

    public boolean isWorkingCurrently() {
        return workingCurrently;
    }

    public void setWorkingCurrently(boolean workingCurrently) {
        this.workingCurrently = workingCurrently;
    }

    public String getWorkingCurrentlyDD() {
        return "PersonnelExperienceBase_workingCurrently";
    }
    // </editor-fold>
}
