package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeCertificate extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="Employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeCertificate_employee";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="certificate from (date)">    
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date certFrom;

    public Date getCertFrom() {
        return certFrom;
    }

    public void setCertFrom(Date certFrom) {
        this.certFrom = certFrom;
    }

    public String getCertFromDD() {
        return "EmployeeCertificate_certFrom";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="certificate to (date)">  
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date certTo;

    public Date getCertTo() {
        return certTo;
    }

    public void setCertTo(Date certTo) {
        this.certTo = certTo;
    }

    public String getCertToDD() {
        return "EmployeeCertificate_certTo";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="name">    
    @Column
    @Translatable(translationField = "nameTranslated")
    private String name;
    @Column
    @Translatable(translationField = "nameTranslated")
    private String nameTranslated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslatedDD() {
        return "ApplicantCertificateNew_name";
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="provider">
    @Column
    private String provider;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "ApplicantCertificateNew_provider";
    }
    // </editor-fold >
}
