/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
/**
 *
 * @author EhabIsmail
 */
@Entity
@ParentEntity(fields="penalty")
public class IncentivesPenaltySetup extends BusinessObjectBaseEntity {
// <editor-fold defaultstate="collapsed" desc="penalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn (nullable= false)
    private Penalty penalty;

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }
    public String getPenaltyDD() {
        return "IncentivesPenaltySetup_vacation";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="fromValue">
    @Column(precision=18, scale=3)
    private BigDecimal fromValue;

    public BigDecimal getFromValue() {
        return fromValue;
    }

    public void setFromValue(BigDecimal fromValue) {
        this.fromValue = fromValue;
    }

    public String getFromValueDD() {
        return "IncentivesPenaltySetup_fromValue";
    }
    @Transient
    private BigDecimal fromValueMask;

    public BigDecimal getFromValueMask() {
        fromValueMask = fromValue ;
        return fromValueMask;
    }

    public void setFromValueMask(BigDecimal fromValueMask) {
        updateDecimalValue("fromValue",fromValueMask);
    }

    public String getFromValueMaskDD() {
        return "IncentivesPenaltySetup_fromValueMask";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="toValue">
    @Column(precision=18, scale=3)
    private BigDecimal toValue;

    public BigDecimal getToValue() {
        return toValue;
    }

    public void setToValue(BigDecimal toValue) {
        this.toValue = toValue;
    }

    public String getToValueDD() {
        return "IncentivesPenaltySetup_toValue";
    }
    @Transient
    private BigDecimal toValueMask;

    public BigDecimal getToValueMask() {
        toValueMask = toValue ;
        return toValueMask;
    }

    public void setToValueMask(BigDecimal toValueMask) {
        updateDecimalValue("toValue",toValueMask);
    }

    public String getToValueMaskDD() {
        return "IncentivesPenaltySetup_toValueMask";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="percentageOrValue">
@ManyToOne
 private UDC percentageOrValue;
 public UDC getPercentageOrValue() {
       return percentageOrValue;
 }
 public void setPercentageOrValue(UDC percentageOrValue) {
       this.percentageOrValue = percentageOrValue;
 }
 public String getPercentageOrValueDD() {
       return "IncentivesPenaltySetup_percentageOrValue";
 }
    // </editor-fold>  
// <editor-fold defaultstate="collapsed" desc="incentiveValue">
    @Column(precision=18, scale=3)
    private BigDecimal incentiveValue;

    public BigDecimal getIncentiveValue() {
        return incentiveValue;
    }

    public void setIncentiveValue(BigDecimal incentiveValue) {
        this.incentiveValue = incentiveValue;
    }

    public String getIncentiveValueDD() {
        return "IncentivesPenaltySetup_incentiveValue";
    }
    @Transient
    private BigDecimal incentiveValueMask;

    public BigDecimal getIncentiveValueMask() {
        incentiveValueMask = incentiveValue ;
        return incentiveValueMask;
    }

    public void setIncentiveValueMask(BigDecimal incentiveValueMask) {
        updateDecimalValue("incentiveValue",incentiveValueMask);
    }

    public String getIncentiveValueMaskDD() {
        return "IncentivesPenaltySetup_incentiveValueMask";
    }
    // </editor-fold>  
    
}