/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
@Table(name="repentityPostActions")
public class RepEntityPostActions extends BaseEntity{
       // <editor-fold defaultstate="collapsed" desc="title">
    @Column
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitleDD() {
        return "RepEntityPostActions_title";
    }
    // </editor-fold>
       // <editor-fold defaultstate="collapsed" desc="classPath">
    @Column
    private String classPath;

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }
    public String getClassPathDD() {
        return "RepEntityPostActions_classPath";
    }
    // </editor-fold>
       // <editor-fold defaultstate="collapsed" desc="actionName">
    @Column
    private String actionName;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName; 
    }
    public String getActionNameDD() {
        return "RepEntityPostActions_actionName";
    }
     // </editor-fold>
       // <editor-fold defaultstate="collapsed" desc="postAction">
    @Column
    private String postAction;
        
   public String getPostAction() {
        return postAction;
    }

    public void setPostAction(String postAction) {
        this.postAction = postAction;
    }
     public String getPostActionDD() {
        return "RepEntityPostActions_postAction";
    }
    
         // </editor-fold>
       // <editor-fold defaultstate="collapsed" desc="postActionComments">
    @Column
    private String postActionComments;
   
    public String getPostActionComments() {
        return postActionComments;
    }

    public void setPostActionComments(String postActionComments) {
        this.postActionComments = postActionComments;
    }
    
    public String getPostActionCommentsDD() {
        return "RepEntityPostActions_postActionComments";
    }

         // </editor-fold>   
       // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column
    private String sortIndex;
  
    public String getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(String sortIndex) {
        this.sortIndex = sortIndex;
    }
      public String getSortIndexDD() {
        return "RepEntityPostActions_sortIndex";
    }
           // </editor-fold>   
       // <editor-fold defaultstate="collapsed" desc="actionType">
       @Column
        private String actionType;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
    public String getActionTypeDD() {
        return "RepEntityPostActions_actionType";
    }
     // </editor-fold>   
}
