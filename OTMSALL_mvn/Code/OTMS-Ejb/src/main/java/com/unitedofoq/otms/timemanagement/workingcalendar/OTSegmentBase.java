/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author lahmed
 */
@MappedSuperclass
public class OTSegmentBase extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="fromTime">
    private String fromTime;

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getFromTimeDD() {
        return "OTSegmentBase_fromTime";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toTime">
    private String toTime;

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getToTimeDD() {
        return "OTSegmentBase_toTime";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="segmentSeq">
    private int segmentSeq;

    public int getSegmentSeq() {
        return segmentSeq;
    }

    public void setSegmentSeq(int segmentSeq) {
        this.segmentSeq = segmentSeq;
    }

    public String getSegmentSeqDD() {
        return "OTSegmentBase_segmentSeq";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="otType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC otType;

    public UDC getOtType() {
        return otType;
    }

    public void setOtType(UDC otType) {
        this.otType = otType;
    }

    public String getOtTypeDD() {
        return "OTSegmentBase_otType";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mandatory">
    private boolean mandatory;

    public boolean isMandatory() {
        return mandatory;
    }

    public String getMandatoryDD() {
        return "OTSegmentBase_mandatory";
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }
    //</editor-fold >

    // <editor-fold defaultstate="collapsed" desc="approvalNeeded">
    private boolean approvalNeeded;

    public boolean isApprovalNeeded() {
        return approvalNeeded;
    }

    public void setApprovalNeeded(boolean approvalNeeded) {
        this.approvalNeeded = approvalNeeded;
    }

    public String getApprovalNeededDD() {
        return "OTSegmentBase_approvalNeeded";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="maxOT">
    @Column(name = "MAXOT")

    private String maxOt;

    public String getMaxOt() {

        return maxOt;

    }

    public void setMaxOt(String maxOt) {

        this.maxOt = maxOt;

    }

    public String getMaxOtDD() {

        return "OTSegment_maxOt";

    }
    // </editor-fold>
}
