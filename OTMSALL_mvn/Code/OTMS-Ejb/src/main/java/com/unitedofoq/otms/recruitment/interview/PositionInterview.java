package com.unitedofoq.otms.recruitment.interview;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.position.Position;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"position"})
public class PositionInterview extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
    public String getPositionDD() {
        return "PositionInterview_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="interview">
    @ManyToOne(fetch = FetchType.LAZY)
    private Interview interview;

    public Interview getInterview() {
        return interview;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }
    
    public String getInterviewDD() {
        return "PositionInterview_interview";
    }
    // </editor-fold>
}
