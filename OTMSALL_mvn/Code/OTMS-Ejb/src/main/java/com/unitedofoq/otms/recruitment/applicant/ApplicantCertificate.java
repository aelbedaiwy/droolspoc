package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantCertificate extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantCertificate_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date certFrom;

    public Date getCertFrom() {
        return certFrom;
    }

    public void setCertFrom(Date certFrom) {
        this.certFrom = certFrom;
    }

    public String getCertFromDD() {
        return "ApplicantCertificate_certFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date certTo;

    public Date getCertTo() {
        return certTo;
    }

    public void setCertTo(Date certTo) {
        this.certTo = certTo;
    }

    public String getCertToDD() {
        return "ApplicantCertificate_certTo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "ApplicantCertificate_name";
    }

    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="provider">
    @Column
    private String provider;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "ApplicantCertificate_provider";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "ApplicantCertificate_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="score">
    private String score;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getScoreDD() {
        return "ApplicantCertificate_score";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="outOfScore">
    private String outOfScore;

    public String getOutOfScore() {
        return outOfScore;
    }

    public void setOutOfScore(String outOfScore) {
        this.outOfScore = outOfScore;
    }

    public String getOutOfScoreDD() {
        return "ApplicantCertificate_outOfScore";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="resultURL">
    private String resultURL;

    public String getResultURL() {
        return resultURL;
    }

    public void setResultURL(String resultURL) {
        this.resultURL = resultURL;
    }

    public String getResultURLDD() {
        return "ApplicantCertificate_resultURL";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="resultIDNum">
    private String resultIDNum;

    public String getResultIDNum() {
        return resultIDNum;
    }

    public void setResultIDNum(String resultIDNum) {
        this.resultIDNum = resultIDNum;
    }

    public String getResultIDNumDD() {
        return "ApplicantCertificate_resultIDNum";
    }
    // </editor-fold>
}
