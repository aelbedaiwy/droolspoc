/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.scheduleevent;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.activity.CancellationRegulationBase;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"scheduleEvent"})
@Table(name= "scheventcancelreg")
public class ScheduleEventCancellationRegulation extends CancellationRegulationBase {
    // <editor-fold defaultstate="collapsed" desc="scheduleEvent">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEvent scheduleEvent;

    public ScheduleEvent getScheduleEvent() {
        return scheduleEvent;
    }

    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
        this.scheduleEvent = scheduleEvent;
    }

    public String getScheduleEventDD() {
        return "ScheduleEventCancellationRegulation_scheduleEvent";
    }
    // </editor-fold>
}