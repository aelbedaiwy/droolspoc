package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantReferal extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="referalDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date referalDate;

    public Date getReferalDate() {
        return referalDate;
    }

    public void setReferalDate(Date referalDate) {
        this.referalDate = referalDate;
    }

    public String getReferalDateDD() {
        return "ApplicantReferal_referalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="referedBy">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee referedBy;

    public Employee getReferedBy() {
        return referedBy;
    }

    public void setReferedBy(Employee referedBy) {
        this.referedBy = referedBy;
    }
    
    public String getReferedByDD() {
        return "ApplicantReferal_referedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    
    public String getApplicantDD() {
        return "ApplicantReferal_applicant";
    }
    // </editor-fold>
}