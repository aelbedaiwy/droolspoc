
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeProfileHistory extends RepEmployeeBase  {
    
    // <editor-fold defaultstate="collapsed" desc="oldUnit">
    @Column
    @Translatable(translationField = "oldUnitTranslated")
    private String oldUnit;

    public String getOldUnit() {
        return oldUnit;
    }

    public void setOldUnit(String oldUnit) {
        this.oldUnit = oldUnit;
    }
    
    public String getOldUnitDD() {
        return "RepEmployeeProfileHistory_oldUnit";
    }
    
    @Transient
    @Translation(originalField = "oldUnit")
    private String oldUnitTranslated;

    public String getOldUnitTranslated() {
        return oldUnitTranslated;
    }

    public void setOldUnitTranslated(String oldUnitTranslated) {
        this.oldUnitTranslated = oldUnitTranslated;
    }
    
    public String getOldUnitTranslatedDD() {
        return "RepEmployeeProfileHistory_oldUnit";
    }
    // </editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="oldPosition">
    @Translatable(translationField = "oldPositionTranslated")
    private String oldPosition;

    public String getOldPosition() {
        return oldPosition;
    }

    public void setOldPosition(String oldPosition) {
        this.oldPosition = oldPosition;
    }
    
    public String getOldPositionDD() {
        return "RepEmployeeProfileHistory_oldPosition";
    }
    
    @Transient
    @Translation(originalField = "oldPosition")
    private String oldPositionTranslated;

    public String getOldPositionTranslated() {
        return oldPositionTranslated;
    }

    public void setOldPositionTranslated(String oldPositionTranslated) {
        this.oldPositionTranslated = oldPositionTranslated;
    }
    
    public String getOldPositionTranslatedDD() {
        return "RepEmployeeProfileHistory_oldPosition";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="oldPayGrade">
    @Translatable(translationField = "oldPayGradeTranslated")
    private String oldPayGrade;

    public String getOldPayGrade() {
        return oldPayGrade;
    }

    public void setOldPayGrade(String oldPayGrade) {
        this.oldPayGrade = oldPayGrade;
    }
    
    public String getOldPayGradeDD() {
        return "RepEmployeeProfileHistory_oldPayGrade";
    }
    
    @Transient
    @Translation(originalField = "oldPayGrade")
    private String oldPayGradeTranslated;

    public String getOldPayGradeTranslated() {
        return oldPayGradeTranslated;
    }

    public void setOldPayGradeTranslated(String oldPayGradeTranslated) {
        this.oldPayGradeTranslated = oldPayGradeTranslated;
    }
    
    public String getOldPayGradeTranslatedDD() {
        return "RepEmployeeProfileHistory_oldPayGrade";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="oldLocation">
    @Translatable(translationField = "oldLocationTranslated")
    private String oldLocation;

    public String getOldLocation() {
        return oldLocation;
    }

    public void setOldLocation(String oldLocation) {
        this.oldLocation = oldLocation;
    }
    
    public String getOldLocationDD() {
        return "RepEmployeeProfileHistory_oldLocation";
    }
    
    @Transient
    @Translation(originalField = "oldLocation")
    private String oldLocationTranslated;

    public String getOldLocationTranslated() {
        return oldLocationTranslated;
    }

    public void setOldLocationTranslated(String oldLocationTranslated) {
        this.oldLocationTranslated = oldLocationTranslated;
    }
    
    public String getOldLocationTranslatedDD() {
        return "RepEmployeeProfileHistory_oldLocation";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="oldCostCenter">
    @Translatable(translationField = "oldCostCenterTranslated")
    private String oldCostCenter;

    public String getOldCostCenter() {
        return oldCostCenter;
    }

    public void setOldCostCenter(String oldCostCenter) {
        this.oldCostCenter = oldCostCenter;
    }
    
    public String getOldCostCenterDD() {
        return "RepEmployeeProfileHistory_oldCostCenter";
    }
    
    @Transient
    @Translation(originalField = "oldCostCenter")
    private String oldCostCenterTranslated;

    public String getOldCostCenterTranslated() {
        return oldCostCenterTranslated;
    }

    public void setOldCostCenterTranslated(String oldCostCenterTranslated) {
        this.oldCostCenterTranslated = oldCostCenterTranslated;
    }
    
    public String getOldCostCenterTranslatedDD() {
        return "RepEmployeeProfileHistory_oldCostCenter";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="actionDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date actionDate;

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }
    
    public String getActionDateDD() {
        return "RepEmployeeProfileHistory_actionDate";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="reason">
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    
    public String getReasonDD() {
        return "RepEmployeeProfileHistory_reason";
    }
    //</editor-fold>
}