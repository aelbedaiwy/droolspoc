
package com.unitedofoq.otms.payroll.custody;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"employee"})
public class EmployeeCustody extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeCustody_employee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="custody">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CustodyDetail custody;

    public void setCustody(CustodyDetail custody) {
        this.custody = custody;
    }

    public CustodyDetail getCustody() {
        return custody;
    }

    public String getCustodyDD() {
        return "EmployeeCustody_custody";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="recievalDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date returnDate;

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnDateDD() {
        return "EmployeeCustody_returnDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvalDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date approvalDate;

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalDateDD() {
        return "EmployeeCustody_approvalDate";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="cancellationDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date cancellationDate;

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }


    public String getCancellationDateDD() {
        return "EmployeeCustody_cancellationDate";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="settlementDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date settlementDate;
    
    public Date getSettlementDate() {
        return settlementDate;
    }
    
    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }
    
    public String getSettlementDateDD() {
        return "EmployeeCustody_settlementDate";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="affectingDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date affectingDate;

    public Date getAffectingDate() {
        return affectingDate;
    }

    public void setAffectingDate(Date affectingDate) {
        this.affectingDate = affectingDate;
    }
    
    public String getAffectingDateDD() {
        return "EmployeeCustody_affectingDate";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="posReturnedTo">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position posReturnedTo; // was "toBeRecievedBy" [23/12/2013 - Loubna - J251]

    public Position getPosReturnedTo() {
        return posReturnedTo;
    }

    public void setPosReturnedTo(Position posReturnedTo) {
        this.posReturnedTo = posReturnedTo;
    }

    public String getPosReturnedToDD() {
        return "EmployeeCustody_posReturnedTo";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="returnedTo">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee returnedTo; // was "recievedBy" [23/12/2013 - Loubna - J251]

    public Employee getReturnedTo() {
        return returnedTo;
    }

    public void setReturnedTo(Employee returnedTo) {
        this.returnedTo = returnedTo;
    }

    public String getReturnedToDD() {
        return "EmployeeCustody_returnedTo";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="approvedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee approvedBy; // was "givinBy" [23/12/2013 - Loubna - J251]

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }
    
    public String getApprovedByDD() {
        return "EmployeeCustody_approvedBy";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="settledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee settledBy;

    public Employee getSettledBy() {
        return settledBy;
    }

    public void setSettledBy(Employee settledBy) {
        this.settledBy = settledBy;
    }
    
    public String getSettledByDD() {
        return "EmployeeCustody_settledBy";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="cancelledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee cancelledBy;

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }
    
    public String getCancelledByDD() {
        return "EmployeeCustody_cancelledBy";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="valueReturned">
    @Column
    private BigDecimal valueReturned; // value to be changed by the user

    public void setValueReturned(BigDecimal valueReturned) {
        this.valueReturned = valueReturned;
    }

    public BigDecimal getValueReturned() {
        return valueReturned;
    }

    public String getValueReturnedDD() {
        return "EmployeeCustody_valueReturned";
    }
    
    @Transient
    private BigDecimal valueReturnedMask;

    public BigDecimal getValueReturnedMask() {
        valueReturnedMask = valueReturned ;
        return valueReturnedMask;
    }
    
    public String getValueReturnedMaskDD() {
        return "EmployeeCustody_valueReturnedMask";
    }

    public void setValueReturnedMask(BigDecimal valueReturnedMask) {
         updateDecimalValue("valueReturned",valueReturnedMask);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="settled">
    @Column(nullable=false, length=1) // Y-N
    private String settled = "N";

    public void setSettled(String settled) {
        this.settled = settled;
    }

    public String getSettled() {
        return settled;
    }

    public String getSettledDD() {
        return "EmployeeCustody_settled";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="cancelled">
    @Column(nullable=false, length=1) // Y-N
    private String cancelled = "N";

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getCancelledDD() {
        return "EmployeeCustody_cancelled";
    }    
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="returned">
    @Column(length=1) // Y-N
    private String returned; // delivered

    public void setReturned(String returned) {
        this.returned = returned;
    }

    public String getReturned() {
        return returned;
    }

    public String getReturnedDD() {
        return "EmployeeCustody_returned";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="approved">
    @Column(length=1) // Y-N
    private String approved;

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }
    
    public String getApprovedDD() {
        return "EmployeeCustody_approved";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="comment">
    @Column(name="comments")
    private String comment;

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public String getCommentDD() {
        return "EmployeeCustody_comment";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="numItems">
    private int numItems = 1;

    public int getNumItems() {
        return numItems;
    }

    public void setNumItems(int numItems) {
        this.numItems = numItems;
    }
    
    public String getNumItemsDD() {
        return "EmployeeCustody_numItems";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="custodyAmount">
    @Column(precision=25, scale=13)
    private BigDecimal custodyAmount;  // value calculated from the code (dimmed)

    public BigDecimal getCustodyAmount() {
        return custodyAmount;
    }

    public void setCustodyAmount(BigDecimal custodyAmount) {
        this.custodyAmount = custodyAmount;
    }
    
    public String getCustodyAmountDD() {
        return "EmployeeCustody_custodyAmount";
    }
    
    @Transient
    private BigDecimal custodyAmountMask;
    public BigDecimal getCustodyAmountMask() {
        custodyAmountMask = custodyAmount ;
        return custodyAmountMask;
    }
    public void setCustodyAmountMask(BigDecimal custodyAmountMask) {
        updateDecimalValue("custodyAmount",custodyAmountMask);
    }
    public String getCustodyAmountMaskDD() {
        return "EmployeeCustody_custodyAmountMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }
    
    public String getStatusDD() {
        return "EmployeeCustody_status";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="returnReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC returnReason;

    public UDC getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(UDC returnReason) {
        this.returnReason = returnReason;
    }
    
    public String getReturnReasonDD() {
        return "EmployeeCustody_returnReason";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="deduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
    
    public String getDeductionDD() {
        return "EmployeeCustody_deduction";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="custodyAmountEnc">
    private String custodyAmountEnc;

    public String getCustodyAmountEnc() {
        return custodyAmountEnc;
    }

    public void setCustodyAmountEnc(String custodyAmountEnc) {
        this.custodyAmountEnc = custodyAmountEnc;
    }
    
    public String getCustodyAmountEncDD() {
        return "EmployeeCustody_custodyAmountEnc";
    }
    //</editor-fold>
}