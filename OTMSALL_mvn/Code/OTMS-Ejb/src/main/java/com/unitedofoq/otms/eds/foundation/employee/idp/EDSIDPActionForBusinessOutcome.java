/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields="channelLrnIntr")
public class EDSIDPActionForBusinessOutcome extends EDSIDPAction{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSIDPBusinessOutcomeForOutLrnIntr channelLrnIntr;
    public EDSIDPBusinessOutcomeForOutLrnIntr getChannelLrnIntr() {
        return channelLrnIntr;
    }

    public void setChannelLrnIntr(EDSIDPBusinessOutcomeForOutLrnIntr channelLrnIntr) {
        this.channelLrnIntr = channelLrnIntr;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSIDPBusinessOutcomeAcc byWho;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSIDPBusinessOutcomeAcc responsible;

    public EDSIDPBusinessOutcomeAcc getByWho() {
        return byWho;
    }

    public void setByWho(EDSIDPBusinessOutcomeAcc byWho) {
        this.byWho = byWho;
    }

    public EDSIDPBusinessOutcomeAcc getResponsible() {
        return responsible;
    }

    public void setResponsible(EDSIDPBusinessOutcomeAcc responsible) {
        this.responsible = responsible;
    }



}
