/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author mostafa
 */
@Entity
public class RepCourseCatalogTranslation extends BaseEntityTranslation{
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="content">
    @Column
    private String content;

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="prerequisite">
    @Column
    private String prerequisite;

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public String getPrerequisite() {
        return prerequisite;
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseType">
    @Column
    private String courseType;

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getCourseType() {
        return courseType;
    }
    // </editor-fold>
}
