package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = "employee")
public class EmployeeProject extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="employee">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeProject_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="project">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getProjectDD() {
        return "EmployeeProject_project";
    }
    //</editor-fold>
}
