/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author lap2
 */
@Entity
public class EmployeeDependanceTranslation extends BaseEntityTranslation{
    private String dependenceName;

    public String getDependenceName() {
        return dependenceName;
    }

    public void setDependenceName(String dependenceName) {
        this.dependenceName = dependenceName;
    }
    
    private String dependenceFirstName;

    public String getDependencFirstName() {
        return dependenceFirstName;
    }

    public void setDependenceFirstName(String dependenceFirstName) {
        this.dependenceFirstName = dependenceFirstName;
    }
    
    private String dependenceMiddleName;

    public String getDependenceMiddleName() {
        return dependenceMiddleName;
    }

    public void setDependenceMiddleName(String dependenceMiddleName) {
        this.dependenceMiddleName = dependenceMiddleName;
    }
    
    private String dependenceLastName;

    public String getDependenceLastName() {
        return dependenceLastName;
    }

    public void setDependenceLastName(String dependenceLastName) {
        this.dependenceLastName = dependenceLastName;
    }
    
    private String dependanceAddress;

    public String getDependanceAddress() {
        return dependanceAddress;
    }

    public void setDependanceAddress(String dependanceAddress) {
        this.dependanceAddress = dependanceAddress;
    }
    
    private String dependancePermanentAddress;

    public String getDependancePermanentAddress() {
        return dependancePermanentAddress;
    }

    public void setDependancePermanentAddress(String dependancePermanentAddress) {
        this.dependancePermanentAddress = dependancePermanentAddress;
    }
    
    private String job;

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
