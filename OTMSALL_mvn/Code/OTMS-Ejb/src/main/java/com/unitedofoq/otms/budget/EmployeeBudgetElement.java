/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.budget;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields = "employee")
public class EmployeeBudgetElement extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    public Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeBudgetElement_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetInstance">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    public BudgetInstance budgetInstance;

    public BudgetInstance getBudgetInstance() {
        return budgetInstance;
    }

    public void setBudgetInstance(BudgetInstance budgetInstance) {
        this.budgetInstance = budgetInstance;
    }

    public String getBudgetInstanceDD() {
        return "EmployeeBudgetElement_budgetInstance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    public BudgetElement budgetElement;

    public BudgetElement getBudgetElement() {
        return budgetElement;
    }

    public void setBudgetElement(BudgetElement budgetElement) {
        this.budgetElement = budgetElement;
    }

    public String getBudgetElementDD() {
        return "EmployeeBudgetElement_budgetElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="plannedTotalValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedTotalValue = BigDecimal.ZERO;

    public BigDecimal getPlannedTotalValue() {
        return plannedTotalValue;
    }

    public void setPlannedTotalValue(BigDecimal plannedTotalValue) {
        this.plannedTotalValue = plannedTotalValue;
    }

    public String getPlannedTotalValueDD() {
        return "EmployeeBudgetElement_plannedTotalValue";
    }
    @Transient
    private BigDecimal plannedTotalValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedTotalValueMask() {
        plannedTotalValueMask = plannedTotalValue;
        return plannedTotalValueMask;
    }

    public void setPlannedTotalValueMask(BigDecimal plannedTotalValueMask) {
        updateDecimalValue("plannedTotalValue", plannedTotalValueMask);
    }

    public String getPlannedTotalValueMaskDD() {
        return "EmployeeBudgetElement_plannedTotalValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actualTotalValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal actualTotalValue= BigDecimal.ZERO;

    public BigDecimal getActualTotalValue() {
        return actualTotalValue;
    }

    public void setActualTotalValue(BigDecimal actualTotalValue) {
        this.actualTotalValue = actualTotalValue;
    }

    public String getActualTotalValueDD() {
        return "EmployeeBudgetElement_actualTotalValue";
    }
    @Transient
    private BigDecimal actualTotalValueMask = BigDecimal.ZERO;

    public BigDecimal getActualTotalValueMask() {
        actualTotalValueMask = actualTotalValue;
        return actualTotalValueMask;
    }

    public void setActualTotalValueMask(BigDecimal actualTotalValueMask) {
        updateDecimalValue("actualTotalValue", actualTotalValueMask);
    }

    public String getActualTotalValueMaskDD() {
        return "EmployeeBudgetElement_actualTotalValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="diffTotalValue">
    @Transient
    private BigDecimal diffTotalValue = BigDecimal.ZERO;

    public BigDecimal getDiffTotalValue() {
        if(this.plannedTotalValue.subtract(this.actualTotalValue).compareTo(BigDecimal.ZERO) ==0)
            this.diffTotalValue = BigDecimal.ZERO;
        else
            this.diffTotalValue = this.plannedTotalValue.subtract(this.actualTotalValue);
        return diffTotalValue;
    }

    public void setDiffTotalValue(BigDecimal diffTotalValue) {
        this.diffTotalValue = diffTotalValue;
    }

    public String getDiffTotalValueDD() {
        return "EmployeeBudgetElement_diffTotalValue";
    }
    @Transient
    private BigDecimal diffTotalValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffTotalValueMask() {
        if(this.plannedTotalValue.subtract(this.actualTotalValue).compareTo(BigDecimal.ZERO) ==0)
            this.diffTotalValueMask = BigDecimal.ZERO;
        else
            diffTotalValueMask = this.plannedTotalValue.subtract(this.actualTotalValue);
        return diffTotalValueMask;
    }

    public void setDiffTotalValueMask(BigDecimal diffTotalValueMask) {
        this.diffTotalValueMask = diffTotalValueMask;
        updateDecimalValue("diffTotalValue", diffTotalValueMask);
    }

    public String getDiffTotalValueMaskDD() {
        return "EmployeeBudgetElement_differenceTotalValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Planned Values Fields">
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedJanValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedFebValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedMarValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedAprValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedMayValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedJunValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedJulValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedAugValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedSepValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedOctValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedNovValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal plannedDecValue = BigDecimal.ZERO;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Actual Values Fields">
    @Column(precision = 25, scale = 13)
    private BigDecimal actualJanValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualFebValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualMarValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualAprValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualMayValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualJunValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualJulValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualAugValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualSepValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualOctValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualNovValue = BigDecimal.ZERO;
    @Column(precision = 25, scale = 13)
    private BigDecimal actualDecValue = BigDecimal.ZERO;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Difference Values Fields">
    @Transient
    private BigDecimal diffJanValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffFebValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffMarValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffAprValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffMayValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffJunValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffJulValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffAugValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffSepValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffOctValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffNovValue = BigDecimal.ZERO;
    @Transient
    private BigDecimal diffDecValue = BigDecimal.ZERO;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">

    public BigDecimal getPlannedJanValue() {
        return plannedJanValue;
    }

    public void setPlannedJanValue(BigDecimal plannedJanValue) {
        this.plannedJanValue = plannedJanValue;
    }

    public BigDecimal getPlannedFebValue() {
        return plannedFebValue;
    }

    public void setPlannedFebValue(BigDecimal plannedFebValue) {
        this.plannedFebValue = plannedFebValue;
        this.diffFebValue = this.plannedFebValue.subtract(this.actualFebValue);
    }

    public BigDecimal getPlannedMarValue() {
        return plannedMarValue;
    }

    public void setPlannedMarValue(BigDecimal plannedMarValue) {
        this.plannedMarValue = plannedMarValue;
        this.diffMarValue = this.plannedMarValue.subtract(this.actualMarValue);
    }

    public BigDecimal getPlannedAprValue() {
        return plannedAprValue;
    }

    public void setPlannedAprValue(BigDecimal plannedAprValue) {
        this.plannedAprValue = plannedAprValue;
        this.diffAprValue = this.plannedAprValue.subtract(this.actualAprValue);
    }

    public BigDecimal getPlannedMayValue() {
        return plannedMayValue;
    }

    public void setPlannedMayValue(BigDecimal plannedMayValue) {
        this.plannedMayValue = plannedMayValue;
        this.diffMayValue = this.plannedMayValue.subtract(this.actualMayValue);
    }

    public BigDecimal getPlannedJunValue() {
        return plannedJunValue;
    }

    public void setPlannedJunValue(BigDecimal plannedJunValue) {
        this.plannedJunValue = plannedJunValue;
//        this.diffJunValue = this.plannedJunValue.subtract(this.actualJunValue);
    }

    public BigDecimal getPlannedJulValue() {
        return plannedJulValue;
    }

    public void setPlannedJulValue(BigDecimal plannedJulValue) {
        this.plannedJulValue = plannedJulValue;
        this.diffJulValue = this.plannedJulValue.subtract(this.actualJulValue);
    }

    public BigDecimal getPlannedAugValue() {
        return plannedAugValue;
    }

    public void setPlannedAugValue(BigDecimal plannedAugValue) {
        this.plannedAugValue = plannedAugValue;
        this.diffAugValue = this.plannedAugValue.subtract(this.actualAugValue);
    }

    public BigDecimal getPlannedSepValue() {
        return plannedSepValue;
    }

    public void setPlannedSepValue(BigDecimal plannedSepValue) {
        this.plannedSepValue = plannedSepValue;
        this.diffSepValue = this.plannedSepValue.subtract(this.actualSepValue);
    }

    public BigDecimal getPlannedOctValue() {
        return plannedOctValue;
    }

    public void setPlannedOctValue(BigDecimal plannedOctValue) {
        this.plannedOctValue = plannedOctValue;
        this.diffOctValue = this.plannedOctValue.subtract(this.actualOctValue);
    }

    public BigDecimal getPlannedNovValue() {
        return plannedNovValue;
    }

    public void setPlannedNovValue(BigDecimal plannedNovValue) {
        this.plannedNovValue = plannedNovValue;
        this.diffNovValue = this.plannedNovValue.subtract(this.actualNovValue);
    }

    public BigDecimal getPlannedDecValue() {
        return plannedDecValue;
    }

    public void setPlannedDecValue(BigDecimal plannedDecValue) {
        this.plannedDecValue = plannedDecValue;
        this.diffDecValue = this.plannedDecValue.subtract(this.actualDecValue);
    }

    public BigDecimal getActualJanValue() {
        return actualJanValue;
    }

    public void setActualJanValue(BigDecimal actualJanValue) {
        this.actualJanValue = actualJanValue;
//        this.diffJanValue = this.plannedJanValue.subtract(this.actualJanValue);
    }

    public BigDecimal getActualFebValue() {
        return actualFebValue;
    }

    public void setActualFebValue(BigDecimal actualFebValue) {
        this.actualFebValue = actualFebValue;
        this.diffFebValue = this.plannedFebValue.subtract(this.actualFebValue);
    }

    public BigDecimal getActualMarValue() {
        return actualMarValue;
    }

    public void setActualMarValue(BigDecimal actualMarValue) {
        this.actualMarValue = actualMarValue;
        this.diffMarValue = this.plannedMarValue.subtract(this.actualMarValue);
    }

    public BigDecimal getActualAprValue() {
        return actualAprValue;
    }

    public void setActualAprValue(BigDecimal actualAprValue) {
        this.actualAprValue = actualAprValue;
        this.diffAprValue = this.plannedAprValue.subtract(this.actualAprValue);
    }

    public BigDecimal getActualMayValue() {
        return actualMayValue;
    }

    public void setActualMayValue(BigDecimal actualMayValue) {
        this.actualMayValue = actualMayValue;
        this.diffMayValue = this.plannedMayValue.subtract(this.actualMayValue);
    }

    public BigDecimal getActualJunValue() {
        return actualJunValue;
    }

    public void setActualJunValue(BigDecimal actualJunValue) {
        this.actualJunValue = actualJunValue;
//        this.diffJunValue = this.plannedJunValue.subtract(this.actualJunValue);
    }

    public BigDecimal getActualJulValue() {
        return actualJulValue;
    }

    public void setActualJulValue(BigDecimal actualJulValue) {
        this.actualJulValue = actualJulValue;
        this.diffJulValue = this.plannedJulValue.subtract(this.actualJulValue);
    }

    public BigDecimal getActualAugValue() {
        return actualAugValue;
    }

    public void setActualAugValue(BigDecimal actualAugValue) {
        this.actualAugValue = actualAugValue;
        this.diffAugValue = this.plannedAugValue.subtract(this.actualAugValue);
    }

    public BigDecimal getActualSepValue() {
        return actualSepValue;
    }

    public void setActualSepValue(BigDecimal actualSepValue) {
        this.actualSepValue = actualSepValue;
        this.diffSepValue = this.plannedSepValue.subtract(this.actualSepValue);
    }

    public BigDecimal getActualOctValue() {
        return actualOctValue;
    }

    public void setActualOctValue(BigDecimal actualOctValue) {
        this.actualOctValue = actualOctValue;
        this.diffOctValue = this.plannedOctValue.subtract(this.actualOctValue);
    }

    public BigDecimal getActualNovValue() {
        return actualNovValue;
    }

    public void setActualNovValue(BigDecimal actualNovValue) {
        this.actualNovValue = actualNovValue;
        this.diffNovValue = this.plannedNovValue.subtract(this.actualNovValue);
    }

    public BigDecimal getActualDecValue() {
        return actualDecValue;
    }

    public void setActualDecValue(BigDecimal actualDecValue) {
        this.actualDecValue = actualDecValue;
        this.diffDecValue = this.plannedDecValue.subtract(this.actualDecValue);
    }

    public BigDecimal getDiffJanValue() {
        return diffJanValue;
    }

    public void setDiffJanValue(BigDecimal diffJanValue) {
        this.diffJanValue = diffJanValue;
    }

    public BigDecimal getDiffFebValue() {
        if(this.plannedFebValue.subtract(this.actualFebValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffFebValue = BigDecimal.ZERO;
        else
            this.diffFebValue = this.plannedFebValue.subtract(this.actualFebValue);
        return diffFebValue;
    }

    public void setDiffFebValue(BigDecimal diffFebValue) {
        this.diffFebValue = diffFebValue;
    }

    public BigDecimal getDiffMarValue() {
        if(this.plannedMarValue.subtract(this.actualMarValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffMarValue = BigDecimal.ZERO;
        else
            this.diffMarValue = this.plannedMarValue.subtract(this.actualMarValue);
        return diffMarValue;
    }

    public void setDiffMarValue(BigDecimal diffMarValue) {
        this.diffMarValue = diffMarValue;
    }

    public BigDecimal getDiffAprValue() {
        if(this.plannedAprValue.subtract(this.actualAprValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffAprValue = BigDecimal.ZERO;
        else
            this.diffAprValue = this.plannedAprValue.subtract(this.actualAprValue);
        return diffAprValue;
    }

    public void setDiffAprValue(BigDecimal diffAprValue) {
        this.diffAprValue = diffAprValue;
    }

    public BigDecimal getDiffMayValue() {
        if(this.plannedMayValue.subtract(this.actualMayValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffMayValue = BigDecimal.ZERO;
        else
            this.diffMayValue = this.plannedMayValue.subtract(this.actualMayValue);
        return diffMayValue;
    }

    public void setDiffMayValue(BigDecimal diffMayValue) {
        this.diffMayValue = diffMayValue;
    }

    public BigDecimal getDiffJunValue() {
        if(this.plannedJunValue.subtract(this.actualJunValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffJunValue = BigDecimal.ZERO;
        else
            this.diffJunValue = this.plannedJunValue.subtract(this.actualJunValue);
        return diffJunValue;
    }

    public void setDiffJunValue(BigDecimal diffJunValue) {
        this.diffJunValue = diffJunValue;
    }

    public BigDecimal getDiffJulValue() {
        if(this.plannedJulValue.subtract(this.actualJulValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffJulValue = BigDecimal.ZERO;
        else
            this.diffJulValue = this.plannedJulValue.subtract(this.actualJulValue);
        return diffJulValue;
    }

    public void setDiffJulValue(BigDecimal diffJulValue) {
        this.diffJulValue = diffJulValue;
    }

    public BigDecimal getDiffAugValue() {
        if(this.plannedAugValue.subtract(this.actualAugValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffAugValue = BigDecimal.ZERO;
        else
            this.diffAugValue = this.plannedAugValue.subtract(this.actualAugValue);
        return diffAugValue;
    }

    public void setDiffAugValue(BigDecimal diffAugValue) {
        this.diffAugValue = diffAugValue;
    }

    public BigDecimal getDiffSepValue() {
        if(this.plannedSepValue.subtract(this.actualSepValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffSepValue = BigDecimal.ZERO;
        else
            this.diffSepValue = this.plannedSepValue.subtract(this.actualSepValue);
        return diffSepValue;
    }

    public void setDiffSepValue(BigDecimal diffSepValue) {
        this.diffSepValue = diffSepValue;
    }

    public BigDecimal getDiffOctValue() {
        if(this.plannedOctValue.subtract(this.actualOctValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffOctValue = BigDecimal.ZERO;
        else
            this.diffOctValue = this.plannedOctValue.subtract(this.actualOctValue);
        return diffOctValue;
    }

    public void setDiffOctValue(BigDecimal diffOctValue) {
        this.diffOctValue = diffOctValue;
    }

    public BigDecimal getDiffNovValue() {
        if(this.plannedNovValue.subtract(this.actualNovValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffNovValue = BigDecimal.ZERO;
        else
            this.diffNovValue = this.plannedNovValue.subtract(this.actualNovValue);
        return diffNovValue;
    }

    public void setDiffNovValue(BigDecimal diffNovValue) {
        this.diffNovValue = diffNovValue;
    }

    public BigDecimal getDiffDecValue() {
        if(this.plannedDecValue.subtract(this.actualDecValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffDecValue = BigDecimal.ZERO;
        else
            this.diffDecValue = this.plannedDecValue.subtract(this.actualDecValue);
        return diffDecValue;
    }

    public void setDiffDecValue(BigDecimal diffDecValue) {
        this.diffDecValue = diffDecValue;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Masking">
    @Transient
    private BigDecimal plannedJanValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedJanValueMask() {
        plannedJanValueMask = plannedJanValue;
        return plannedJanValueMask;
    }

    public void setPlannedJanValueMask(BigDecimal plannedJanValueMask) {
        updateDecimalValue("plannedJanValue", plannedJanValueMask);
        setDiffJanValueMask(plannedJanValueMask.subtract(this.actualJanValueMask));
    }

    public String getPlannedJanValueMaskDD() {
        return "EmployeeBudgetElement_plannedJanValueMask";
    }
    @Transient
    private BigDecimal plannedFebValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedFebValueMask() {
        plannedFebValueMask = plannedFebValue;
        return plannedFebValueMask;
    }

    public void setPlannedFebValueMask(BigDecimal plannedFebValueMask) {
        updateDecimalValue("plannedFebValue", plannedFebValueMask);
        setDiffFebValueMask(plannedFebValueMask.subtract(this.actualFebValueMask));
    }

    public String getPlannedFebValueMaskDD() {
        return "EmployeeBudgetElement_plannedFebValueMask";
    }
    @Transient
    private BigDecimal plannedMarValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedMarValueMask() {
        plannedMarValueMask = plannedMarValue;
        return plannedMarValueMask;
    }

    public void setPlannedMarValueMask(BigDecimal plannedMarValueMask) {
        updateDecimalValue("plannedMarValue", plannedMarValueMask);
        setDiffMarValueMask(plannedMarValueMask.subtract(this.actualMarValueMask));
    }

    public String getPlannedMarValueMaskDD() {
        return "EmployeeBudgetElement_plannedMarValueMask";
    }
    @Transient
    private BigDecimal plannedAprValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedAprValueMask() {
        plannedAprValueMask = plannedAprValue;
        return plannedAprValueMask;
    }

    public void setPlannedAprValueMask(BigDecimal plannedAprValueMask) {
        updateDecimalValue("plannedAprValue", plannedAprValueMask);
        setDiffAprValueMask(plannedAprValueMask.subtract(this.actualAprValueMask));
    }

    public String getPlannedAprValueMaskDD() {
        return "EmployeeBudgetElement_plannedAprValueMask";
    }
    @Transient
    private BigDecimal plannedMayValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedMayValueMask() {
        plannedMayValueMask = plannedMayValue;
        return plannedMayValueMask;
    }

    public void setPlannedMayValueMask(BigDecimal plannedMayValueMask) {
        updateDecimalValue("plannedMayValue", plannedMayValueMask);
        setDiffMarValueMask(plannedMayValueMask.subtract(this.actualMayValueMask));
    }

    public String getPlannedMayValueMaskDD() {
        return "EmployeeBudgetElement_plannedMayValueMask";
    }
    @Transient
    private BigDecimal plannedJunValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedJunValueMask() {
        plannedJunValueMask = plannedJunValue;
        return plannedJunValueMask;
    }

    public void setPlannedJunValueMask(BigDecimal plannedJunValueMask) {
        updateDecimalValue("plannedJunValue", plannedJunValueMask);
        setDiffJunValueMask(plannedJunValueMask.subtract(this.actualJunValueMask));
    }

    public String getPlannedJunValueMaskDD() {
        return "EmployeeBudgetElement_plannedJunValueMask";
    }
    @Transient
    private BigDecimal plannedJulValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedJulValueMask() {
        plannedJulValueMask = plannedJulValue;
        return plannedJulValueMask;
    }

    public void setPlannedJulValueMask(BigDecimal plannedJulValueMask) {
        updateDecimalValue("plannedJulValue", plannedJulValueMask);
        setDiffJulValueMask(plannedJulValueMask.subtract(this.actualJulValueMask));
    }

    public String getPlannedJulValueMaskDD() {
        return "EmployeeBudgetElement_plannedJulValueMask";
    }
    @Transient
    private BigDecimal plannedAugValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedAugValueMask() {
        plannedAugValueMask = plannedAugValue;
        return plannedAugValueMask;
    }

    public void setPlannedAugValueMask(BigDecimal plannedAugValueMask) {
        updateDecimalValue("plannedAugValue", plannedAugValueMask);
        setDiffAugValueMask(plannedAugValueMask.subtract(this.actualAugValueMask));
    }

    public String getPlannedAugValueMaskDD() {
        return "EmployeeBudgetElement_plannedAugValueMask";
    }
    @Transient
    private BigDecimal plannedSepValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedSepValueMask() {
        plannedSepValueMask = plannedSepValue;
        return plannedSepValueMask;
    }

    public void setPlannedSepValueMask(BigDecimal plannedSepValueMask) {
        updateDecimalValue("plannedSepValue", plannedSepValueMask);
        setDiffSepValueMask(plannedSepValueMask.subtract(this.actualSepValueMask));
    }

    public String getPlannedSepValueMaskDD() {
        return "EmployeeBudgetElement_plannedSepValueMask";
    }
    @Transient
    private BigDecimal plannedOctValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedOctValueMask() {
        plannedOctValueMask = plannedOctValue;
        return plannedOctValueMask;
    }

    public void setPlannedOctValueMask(BigDecimal plannedOctValueMask) {
        updateDecimalValue("plannedOctValue", plannedOctValueMask);
        setDiffOctValueMask(plannedOctValueMask.subtract(this.actualOctValueMask));
    }

    public String getPlannedOctValueMaskDD() {
        return "EmployeeBudgetElement_plannedOctValueMask";
    }
    @Transient
    private BigDecimal plannedNovValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedNovValueMask() {
        plannedNovValueMask = plannedNovValue;
        return plannedNovValueMask;
    }

    public void setPlannedNovValueMask(BigDecimal plannedNovValueMask) {
        updateDecimalValue("plannedNovValue", plannedNovValueMask);
        setDiffNovValueMask(plannedNovValueMask.subtract(this.actualNovValueMask));
    }

    public String getPlannedNovValueMaskDD() {
        return "EmployeeBudgetElement_plannedNovValueMask";
    }
    @Transient
    private BigDecimal plannedDecValueMask = BigDecimal.ZERO;

    public BigDecimal getPlannedDecValueMask() {
        plannedDecValueMask = plannedDecValue;
        return plannedDecValueMask;
    }

    public void setPlannedDecValueMask(BigDecimal plannedDecValueMask) {
        updateDecimalValue("plannedDecValue", plannedDecValueMask);
        setDiffDecValueMask(plannedDecValueMask.subtract(this.actualDecValueMask));
    }

    public String getPlannedDecValueMaskDD() {
        return "EmployeeBudgetElement_plannedDecValueMask";
    }
    @Transient
    private BigDecimal actualJanValueMask = BigDecimal.ZERO;

    public BigDecimal getActualJanValueMask() {
        actualJanValueMask = actualJanValue;
        return actualJanValueMask;
    }

    public void setActualJanValueMask(BigDecimal actualJanValueMask) {
        updateDecimalValue("actualJanValue", actualJanValueMask);
        setDiffJanValueMask(this.plannedJanValueMask.subtract(actualJanValueMask));
    }

    public String getActualJanValueMaskDD() {
        return "EmployeeBudgetElement_actualJanValueMask";
    }
    @Transient
    private BigDecimal actualFebValueMask = BigDecimal.ZERO;

    public BigDecimal getActualFebValueMask() {
        actualFebValueMask = actualFebValue;
        return actualFebValueMask;
    }

    public void setActualFebValueMask(BigDecimal actualFebValueMask) {
        updateDecimalValue("actualFebValue", actualFebValueMask);
        setDiffFebValueMask(this.plannedFebValueMask.subtract(actualFebValueMask));
    }

    public String getActualFebValueMaskDD() {
        return "EmployeeBudgetElement_actualFebValueMask";
    }
    @Transient
    private BigDecimal actualMarValueMask = BigDecimal.ZERO;

    public BigDecimal getActualMarValueMask() {
        actualMarValueMask = actualMarValue;
        return actualMarValueMask;
    }

    public void setActualMarValueMask(BigDecimal actualMarValueMask) {
        updateDecimalValue("actualMarValue", actualMarValueMask);
        setDiffMarValueMask(this.plannedMarValueMask.subtract(actualMarValueMask));
    }

    public String getActualMarValueMaskDD() {
        return "EmployeeBudgetElement_actualMarValueMask";
    }
    @Transient
    private BigDecimal actualAprValueMask = BigDecimal.ZERO;

    public BigDecimal getActualAprValueMask() {
        actualAprValueMask = actualAprValue;
        return actualAprValueMask;
    }

    public void setActualAprValueMask(BigDecimal actualAprValueMask) {
        updateDecimalValue("actualAprValue", actualAprValueMask);
        setDiffAprValueMask(this.plannedAprValueMask.subtract(actualAprValueMask));
    }

    public String getActualAprValueMaskDD() {
        return "EmployeeBudgetElement_actualAprValueMask";
    }
    @Transient
    private BigDecimal actualMayValueMask = BigDecimal.ZERO;

    public BigDecimal getActualMayValueMask() {
        actualMayValueMask = actualMayValue;
        return actualMayValueMask;
    }

    public void setActualMayValueMask(BigDecimal actualMayValueMask) {
        updateDecimalValue("actualMayValue", actualMayValueMask);
        setDiffMayValueMask(this.plannedMayValueMask.subtract(actualMayValueMask));
    }

    public String getActualMayValueMaskDD() {
        return "EmployeeBudgetElement_actualMayValueMask";
    }
    @Transient
    private BigDecimal actualJunValueMask = BigDecimal.ZERO;

    public BigDecimal getActualJunValueMask() {
        actualJunValueMask = actualJunValue;
        return actualJunValueMask;
    }

    public void setActualJunValueMask(BigDecimal actualJunValueMask) {
        updateDecimalValue("actualJunValue", actualJunValueMask);
        setDiffJunValueMask(this.plannedJunValueMask.subtract(actualJunValueMask));
    }

    public String getActualJunValueMaskDD() {
        return "EmployeeBudgetElement_actualJunValueMask";
    }
    @Transient
    private BigDecimal actualJulValueMask = BigDecimal.ZERO;

    public BigDecimal getActualJulValueMask() {
        actualJulValueMask = actualJulValue;
        return actualJulValueMask;
    }

    public void setActualJulValueMask(BigDecimal actualJulValueMask) {
        updateDecimalValue("actualJulValue", actualJulValueMask);
        setDiffJulValueMask(this.plannedJulValueMask.subtract(actualJulValueMask));
    }

    public String getActualJulValueMaskDD() {
        return "EmployeeBudgetElement_actualJulValueMask";
    }
    @Transient
    private BigDecimal actualAugValueMask = BigDecimal.ZERO;

    public BigDecimal getActualAugValueMask() {
        actualAugValueMask = actualAugValue;
        return actualAugValueMask;
    }

    public void setActualAugValueMask(BigDecimal actualAugValueMask) {
        updateDecimalValue("actualAugValue", actualAugValueMask);
        setDiffAugValueMask(this.plannedAugValueMask.subtract(actualAugValueMask));
    }

    public String getActualAugValueMaskDD() {
        return "EmployeeBudgetElement_actualAugValueMask";
    }
    @Transient
    private BigDecimal actualSepValueMask = BigDecimal.ZERO;

    public BigDecimal getActualSepValueMask() {
        actualSepValueMask = actualSepValue;
        return actualSepValueMask;
    }

    public void setActualSepValueMask(BigDecimal actualSepValueMask) {
        updateDecimalValue("actualSepValue", actualSepValueMask);
        setDiffSepValueMask(this.plannedSepValueMask.subtract(actualSepValueMask));
    }

    public String getActualSepValueMaskDD() {
        return "EmployeeBudgetElement_actualSepValueMask";
    }
    @Transient
    private BigDecimal actualOctValueMask = BigDecimal.ZERO;

    public BigDecimal getActualOctValueMask() {
        actualOctValueMask = actualOctValue;
        return actualOctValueMask;
    }

    public void setActualOctValueMask(BigDecimal actualOctValueMask) {
        updateDecimalValue("actualOctValue", actualOctValueMask);
        setDiffOctValueMask(this.plannedOctValueMask.subtract(actualOctValueMask));
    }

    public String getActualOctValueMaskDD() {
        return "EmployeeBudgetElement_actualOctValueMask";
    }
    @Transient
    private BigDecimal actualNovValueMask = BigDecimal.ZERO;

    public BigDecimal getActualNovValueMask() {
        actualNovValueMask = actualNovValue;
        return actualNovValueMask;
    }

    public void setActualNovValueMask(BigDecimal actualNovValueMask) {
        updateDecimalValue("actualNovValue", actualNovValueMask);
        setDiffNovValueMask(this.plannedNovValueMask.subtract(actualNovValueMask));
    }

    public String getActualNovValueMaskDD() {
        return "EmployeeBudgetElement_actualNovValueMask";
    }
    @Transient
    private BigDecimal actualDecValueMask = BigDecimal.ZERO;

    public BigDecimal getActualDecValueMask() {
        actualDecValueMask = actualDecValue;
        return actualDecValueMask;
    }

    public void setActualDecValueMask(BigDecimal actualDecValueMask) {
        updateDecimalValue("actualDecValue", actualDecValueMask);
        setDiffDecValueMask(this.plannedDecValueMask.subtract(actualDecValueMask));
    }

    public String getActualDecValueMaskDD() {
        return "EmployeeBudgetElement_actualDecValueMask";
    }
    @Transient
    private BigDecimal diffJanValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffJanValueMask() {
        if(this.plannedJanValue.subtract(this.actualJanValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffJanValueMask = BigDecimal.ZERO;
        else
            this.diffJanValueMask = this.plannedJanValue.subtract(this.actualJanValue);
        return diffJanValueMask;
    }

    public void setDiffJanValueMask(BigDecimal diffJanValueMask) {
        this.diffJanValueMask = diffJanValueMask;//this.plannedJanValue.subtract(this.actualJanValue);
        updateDecimalValue("diffJanValue", diffJanValueMask);
    }

    public String getDiffJanValueMaskDD() {
        return "EmployeeBudgetElement_diffJanValueMask";
    }
    @Transient
    private BigDecimal diffFebValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffFebValueMask() {
        if(this.plannedFebValue.subtract(this.actualFebValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffFebValueMask = BigDecimal.ZERO;
        else
            this.diffFebValueMask = this.plannedFebValue.subtract(this.actualFebValue);
        return diffFebValueMask;
    }

    public void setDiffFebValueMask(BigDecimal diffFebValueMask) {
        this.diffFebValueMask = diffFebValueMask;//this.plannedFebValue.subtract(this.actualFebValue);
        updateDecimalValue("diffFebValue", diffFebValueMask);
    }

    public String getDiffFebValueMaskDD() {
        return "EmployeeBudgetElement_diffFebValueMask";
    }
    @Transient
    private BigDecimal diffMarValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffMarValueMask() {
        if(this.plannedMarValue.subtract(this.actualMarValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffMarValueMask = BigDecimal.ZERO;
        else
            this.diffMarValueMask =this.plannedMarValue.subtract(this.actualMarValue);
        return diffMarValueMask;
    }

    public void setDiffMarValueMask(BigDecimal diffMarValueMask) {
        this.diffMarValueMask = diffMarValueMask;//this.plannedMarValue.subtract(this.actualMarValue);
        updateDecimalValue("diffMarValue", diffMarValueMask);
    }

    public String getDiffMarValueMaskDD() {
        return "EmployeeBudgetElement_diffMarValueMask";
    }
    @Transient
    private BigDecimal diffAprValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffAprValueMask() {
        if(this.plannedAprValue.subtract(this.actualAprValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffAprValueMask = BigDecimal.ZERO;
        else
            this.diffAprValueMask =this.plannedAprValue.subtract(this.actualAprValue);
        return diffAprValueMask;
    }

    public void setDiffAprValueMask(BigDecimal diffAprValueMask) {
        this.diffAprValueMask = diffAprValueMask; //this.plannedAprValue.subtract(this.actualAprValue);
        updateDecimalValue("diffAprValue", diffAprValueMask);
    }

    public String getDiffAprValueMaskDD() {
        return "EmployeeBudgetElement_diffAprValueMask";
    }
    @Transient
    private BigDecimal diffMayValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffMayValueMask() {
        if(this.plannedMayValue.subtract(this.actualMayValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffMayValueMask = BigDecimal.ZERO;
        else
            this.diffMayValueMask =this.plannedMayValue.subtract(this.actualMayValue);
        return diffMayValueMask;
    }

    public void setDiffMayValueMask(BigDecimal diffMayValueMask) {
        this.diffMayValueMask = diffMayValueMask; //this.plannedMayValue.subtract(this.actualMayValue);
        updateDecimalValue("diffMayValue", diffMayValueMask);
    }

    public String getDiffMayValueMaskDD() {
        return "EmployeeBudgetElement_diffMayValueMask";
    }
    @Transient
    private BigDecimal diffJunValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffJunValueMask() {
        if(this.plannedJunValue.subtract(this.actualJunValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffJunValueMask = BigDecimal.ZERO;
        else
            this.diffJunValueMask =this.plannedJunValue.subtract(this.actualJunValue);
        return diffJunValueMask;
    }

    public void setDiffJunValueMask(BigDecimal diffJunValueMask) {
        this.diffJunValueMask = diffJunValueMask; //this.plannedJunValue.subtract(this.actualJunValue);
        updateDecimalValue("diffJunValue", diffJunValueMask);
    }

    public String getDiffJunValueMaskDD() {
        return "EmployeeBudgetElement_diffDecValueMask";
    }
    @Transient
    private BigDecimal diffJulValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffJulValueMask() {
        if(this.plannedJulValue.subtract(this.actualJulValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffJulValueMask = BigDecimal.ZERO;
        else
            this.diffJulValueMask =this.plannedJulValue.subtract(this.actualJulValue);
        return diffJulValueMask;
    }

    public void setDiffJulValueMask(BigDecimal diffJulValueMask) {
        this.diffJulValueMask = diffJulValueMask; //this.plannedJulValue.subtract(this.actualJulValue);
        updateDecimalValue("diffJulValue", diffJulValueMask);
    }

    public String getDiffJulValueMaskDD() {
        return "EmployeeBudgetElement_diffJulValueMask";
    }
    @Transient
    private BigDecimal diffAugValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffAugValueMask() {
        if(this.plannedAugValue.subtract(this.actualAugValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffAugValueMask = BigDecimal.ZERO;
        else
            this.diffAugValueMask =this.plannedAugValue.subtract(this.actualAugValue);
        return diffAugValueMask;
    }

    public void setDiffAugValueMask(BigDecimal diffAugValueMask) {
        this.diffAugValueMask = diffAugValueMask; //this.plannedAugValue.subtract(this.actualAugValue);
        updateDecimalValue("diffAugValue", diffAugValueMask);
    }

    public String getDiffAugValueMaskDD() {
        return "EmployeeBudgetElement_diffAugValueMask";
    }
    @Transient
    private BigDecimal diffSepValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffSepValueMask() {
        if(this.plannedSepValue.subtract(this.actualSepValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffSepValueMask = BigDecimal.ZERO;
        else
            this.diffSepValueMask =this.plannedSepValue.subtract(this.actualSepValue);
        return diffSepValueMask;
    }

    public void setDiffSepValueMask(BigDecimal diffSepValueMask) {
        this.diffSepValueMask = diffSepValueMask; //this.plannedSepValue.subtract(this.actualSepValue);
        updateDecimalValue("diffSepValue", diffSepValueMask);
    }

    public String getDiffSepValueMaskDD() {
        return "EmployeeBudgetElement_diffSepValueMask";
    }
    @Transient
    private BigDecimal diffOctValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffOctValueMask() {
        if(this.plannedOctValue.subtract(this.actualOctValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffOctValueMask = BigDecimal.ZERO;
        else
            this.diffOctValueMask =this.plannedOctValue.subtract(this.actualOctValue);
        return diffOctValueMask;
    }

    public void setDiffOctValueMask(BigDecimal diffOctValueMask) {
        this.diffOctValueMask = diffOctValueMask; //this.plannedOctValue.subtract(this.actualOctValue);
        updateDecimalValue("diffOctValue", diffOctValueMask);
    }

    public String getDiffOctValueMaskDD() {
        return "EmployeeBudgetElement_diffOctValueMask";
    }
    @Transient
    private BigDecimal diffNovValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffNovValueMask() {
        if(this.plannedNovValue.subtract(this.actualNovValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffNovValueMask = BigDecimal.ZERO;
        else
            this.diffNovValueMask =this.plannedNovValue.subtract(this.actualNovValue);
        return diffNovValueMask;
    }

    public void setDiffNovValueMask(BigDecimal diffNovValueMask) {
        this.diffNovValueMask = diffNovValueMask; //this.plannedNovValue.subtract(this.actualNovValue);
        updateDecimalValue("diffNovValue", diffNovValueMask);
    }

    public String getDiffNovValueMaskDD() {
        return "EmployeeBudgetElement_diffNovValueMask";
    }
    @Transient
    private BigDecimal diffDecValueMask = BigDecimal.ZERO;

    public BigDecimal getDiffDecValueMask() {
        if(this.plannedDecValue.subtract(this.actualDecValue).compareTo(BigDecimal.ZERO) == 0)
            this.diffDecValueMask = BigDecimal.ZERO;
        else
            this.diffDecValueMask =this.plannedDecValue.subtract(this.actualDecValue);
        return diffDecValueMask;
    }

    public void setDiffDecValueMask(BigDecimal diffDecValueMask) {
        this.diffDecValueMask = diffDecValueMask; //this.plannedDecValue.subtract(this.actualDecValue);
        updateDecimalValue("diffDecValue", diffDecValueMask);
    }

    public String getDiffDecValueMaskDD() {
        return "EmployeeBudgetElement_diffDecValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="m2mCheck">
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeBudgetElement_m2mCheck";
    }
    // </editor-fold>
}
