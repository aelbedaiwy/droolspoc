package com.unitedofoq.otms.foundation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author tbadran
 */
@Entity
@DiscriminatorValue("Position")
public class PositionColor extends NodeColor {

}
