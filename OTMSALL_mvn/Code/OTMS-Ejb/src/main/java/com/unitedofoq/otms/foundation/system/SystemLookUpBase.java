package com.unitedofoq.otms.foundation.system;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class SystemLookUpBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="code">

    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "SystemLookUpDetail_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "SystemLookUpDetail_description";
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "SystemLookUpDetail_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public String getCommentsDD() {
        return "SystemLookUpDetail_comments";
    }
    // </editor-fold>
}
