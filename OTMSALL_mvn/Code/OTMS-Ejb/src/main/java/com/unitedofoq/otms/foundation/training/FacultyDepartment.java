/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields="faculty")
public class FacultyDepartment extends BaseEntity {

     // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "FacultyDepartment_code";
    }
// </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
     public String getName() {
        return name;
    }
     public String getNameTranslatedDD() {
        return "FacultyDepartment_nameTranslated";
    }
    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="faculty">
    
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Faculty faculty;

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }
public String getFacultyDD() {
        return "FacultyDepartment_faculty";
    }
 // </editor-fold>



}
