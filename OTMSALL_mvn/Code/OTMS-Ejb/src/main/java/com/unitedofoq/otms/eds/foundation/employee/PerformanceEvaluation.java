/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields={"employee"})

public class PerformanceEvaluation extends BaseEntity {
    
 @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)   
    private UDC evaluation;

    public UDC getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(UDC evaluation) {
        this.evaluation = evaluation;
    }
    
    public String getEvaluationDD() {
        return "PerformanceEvaluation_evaluation";
    }
@JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
private UDC evaluationYear;

    public UDC getEvaluationYear() {
        return evaluationYear;
    }

    public void setEvaluationYear(UDC evaluationYear) {
        this.evaluationYear = evaluationYear;
    }

     public String getEvaluationYearDD() {
        return "PerformanceEvaluation_evaluationYear";
    }
   
@JoinColumn(nullable=false)
@ManyToOne(fetch = javax.persistence.FetchType.LAZY)

private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

     public String getEmployeeDD() {
        return "PerformanceEvaluation_employee";
    }

}
