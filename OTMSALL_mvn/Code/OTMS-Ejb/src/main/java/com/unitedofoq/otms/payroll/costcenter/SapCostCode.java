/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.unit.Unit;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author A.Alaa
 */
@Entity
@ParentEntity(fields ="unit")
public class SapCostCode extends BaseEntity {
   
    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "SapCostCode_code";
    }
   // </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionDD() {
        return "SapCostCode_description";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCodeFunction">
    @OneToMany(mappedBy="sapCostCode")
    private List<CostCodeFunction> costCodeFunction;

    public List<CostCodeFunction> getCostCodeFunction() {
        return costCodeFunction;
    }

    public void setCostCodeFunction(List<CostCodeFunction> costCodeFunction) {
        this.costCodeFunction = costCodeFunction;
    }
     public String getCostCodeFunctionDD() {
        return "SapCostCode_costCodeFunction";
    }
   
    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="unit">
    
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	
    private Unit unit;
    
    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
    public String getUnitDD() {
        return "SapCostCode_unit";
    }
    
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="employeeSapIntegerationCostCode">
   // @OneToMany(fetch = javax.persistence.FetchType.LAZY)
    /*private List<EmployeeSapIntegerationCostCode> employeeSapIntegerationCostCode ;

    
   

    public List<EmployeeSapIntegerationCostCode> getEmployeeSapIntegerationCostCode() {
        return employeeSapIntegerationCostCode;
    }

    public void setEmployeeSapIntegerationCostCode(List<EmployeeSapIntegerationCostCode> employeeSapIntegerationCostCode) {
        this.employeeSapIntegerationCostCode = employeeSapIntegerationCostCode;
    }
    
    public String getEmployeeSapIntegerationCostCodeDD() {
        return "SapCostCode_employeeSapIntegerationCostCode";
    }
    */
    
     //</editor-fold>
}
