package com.unitedofoq.otms.recruitment.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tbadran
 */
@Entity
@ChildEntity(fields = {"employeeHiringDocumentDetails"})
public class EmployeeHiringDocument extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeHiringDocument_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="document">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC documentType;

    public UDC getDocumentType() {
        return documentType;
    }

    public void setDocumentType(UDC documentType) {
        this.documentType = documentType;
    }

    public String getDocumentTypeDD() {
        return "EmployeeHiringDocument_documentType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="submit">
    @Temporal(TemporalType.TIMESTAMP)
    private Date submitDate;

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public String getSubmitDateDD() {
        return "EmployeeHiringDocument_submitDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeHiringDocumentDetails">
    @OneToMany(mappedBy = "employeeHiringDocument")
    private List<EmployeeHiringDocumentDetails> employeeHiringDocumentDetails;

    public List<EmployeeHiringDocumentDetails> getEmployeeHiringDocumentDetails() {
        return employeeHiringDocumentDetails;
    }

    public void setEmployeeHiringDocumentDetails(List<EmployeeHiringDocumentDetails> employeeHiringDocumentDetails) {
        this.employeeHiringDocumentDetails = employeeHiringDocumentDetails;
    }

    public String getEmployeeHiringDocumentDetailsDD() {
        return "EmployeeHiringDocument_employeeHiringDocumentDetails";
    }
// </editor-fold>
}
