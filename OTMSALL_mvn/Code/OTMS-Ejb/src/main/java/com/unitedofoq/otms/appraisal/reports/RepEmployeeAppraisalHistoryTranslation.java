package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeAppraisalHistoryTranslation extends RepAppraisalSheetTranslationBase {

    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }
    // </editor-fold>
}
