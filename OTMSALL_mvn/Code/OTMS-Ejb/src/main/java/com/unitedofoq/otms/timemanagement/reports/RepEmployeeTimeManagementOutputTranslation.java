package com.unitedofoq.otms.timemanagement.reports;

import com.unitedofoq.otms.reports.views.RepCompanyLevelBaseTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name= "repemptmoutputtranslation")
public class RepEmployeeTimeManagementOutputTranslation extends RepCompanyLevelBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="payrollTransferredElement">
    @Column
    private String payrollTransferredElement;

    public void setPayrollTransferredElement(String payrollTransferredElement) {
        this.payrollTransferredElement = payrollTransferredElement;
    }

    public String getPayrollTransferredElement() {
        return payrollTransferredElement;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="absenceTransferredElement">
    @Column
    private String absenceTransferredElement;

    public void setAbsenceTransferredElement(String absenceTransferredElement) {
        this.absenceTransferredElement = absenceTransferredElement;
    }

    public String getAbsenceTransferredElement() {
        return absenceTransferredElement;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="vacationTransferredElement">
    @Column
    private String vacationTransferredElement;

    public void setVacationTransferredElement(String vacationTransferredElement) {
        this.vacationTransferredElement = vacationTransferredElement;
    }

    public String getVacationTransferredElement() {
        return vacationTransferredElement;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="penaltyTransferredElement">
    @Column
    private String penaltyTransferredElement;

    public void setPenaltyTransferredElement(String penaltyTransferredElement) {
        this.penaltyTransferredElement = penaltyTransferredElement;
    }

    public String getPenaltyTransferredElement() {
        return penaltyTransferredElement;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    private String firstName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    private String middleName;

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    private String lastName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    private String fullName;

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    private String locationDescription;

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescription() {
        return locationDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    private String hiringType;

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringType() {
        return hiringType;
    }
    // </editor-fold>

}