/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author mostafa
 */
@Entity
public class CostCodeFunction extends BaseEntity {
    
    //<editor-fold defaultstate="collapse" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "CostCodeFunction_code";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapse" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionDD() {
        return "CostCodeFunction_description";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapse" desc="sapCostCode">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SapCostCode sapCostCode;

    public SapCostCode getSapCostCode() {
        return sapCostCode;
    }

    public void setSapCostCode(SapCostCode sapCostCode) {
        this.sapCostCode = sapCostCode;
    }
    public String getSapCostCodeDD() {
        return "CostCodeFunction_sapCostCode";
    }
    //</editor-fold>
}

