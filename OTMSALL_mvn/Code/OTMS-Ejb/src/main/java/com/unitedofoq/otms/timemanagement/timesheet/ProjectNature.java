/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields="project")
@ChildEntity(fields = {"allowances","rules"})
public class ProjectNature extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="project">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Project project;

    public Project getProject() {
        return project;
    }
    public String getProjectDD() {
        return "ProjectNature_project";
    }

    public void setProject(Project project) {
        this.project = project;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "ProjectNature_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="symbol">
    @Column
    private String symbol;

    public String getSymbol() {
        return symbol;
    }
    public String getSymbolDD() {
        return "ProjectNature_symbol";
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="item">
    @Column
    private String item;

    public String getItem() {
        return item;
    }
    public String getItemDD() {
        return "ProjectNature_item";
    }

    public void setItem(String item) {
        this.item = item;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="defaultValue">
    @Column(precision=25, scale=13)
    private BigDecimal defaultValue;

    public BigDecimal getDefaultValue() {
        return defaultValue;
    }
    public String getDefaultValueDD() {
        return "ProjectNature_defaultValue";
    }

    public void setDefaultValue(BigDecimal defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    @Transient
    private BigDecimal defaultValueMask;

    public BigDecimal getDefaultValueMask() {
        defaultValueMask = defaultValue;
        return defaultValueMask;
    }
    public String getDefaultValueMaskDD() {
        return "ProjectNature_defaultValueMask";
    }

    public void setDefaultValueMask(BigDecimal defaultValueMask) {
        updateDecimalValue("defaultValue", defaultValueMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="project">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC color;

    public UDC getColor() {
        return color;
    }
    public String getColorDD() {
        return "ProjectNature_color";
    }

    public void setColor(UDC color) {
        this.color = color;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="allowances">
    @OneToMany(mappedBy="projectNature")
    private List<ProjectAllowance> allowances;

    public List<ProjectAllowance> getAllowances() {
        return allowances;
    }
    public String getAllowancesDD() {
        return "Company_allowances";
    }

    public void setAllowances(List<ProjectAllowance> allowances) {
        this.allowances = allowances;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="rules">
    @OneToMany(mappedBy="projectNature")
    private List<TimeSheetRule> rules;

    public List<TimeSheetRule> getRules() {
        return rules;
    }
    public String getRulesDD() {
        return "ProjectNature_rules";
    }

    public void setRules(List<TimeSheetRule> rules) {
        this.rules = rules;
    }
    //</editor-fold >
}
