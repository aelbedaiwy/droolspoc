package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class EDSEmployeeCompetencyHistory extends BaseEntity{
    private String reference;
    private String evaluator;
    private String source;
    @Temporal(TemporalType.DATE)
    private Date evaluationDate;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSEmployeeCompetency employeeCompetency;

    public String getEvaluatorDD()      {    return "EDSEmployeeCompetencyHistory_evaluator";  }
    public String getEvaluationDateDD() {    return "EDSEmployeeCompetencyHistory_evaluationDate";  }
    public String getReferenceDD()      {    return "EDSEmployeeCompetencyHistory_reference";  }
    public String getSourceDD()         {    return "EDSEmployeeCompetencyHistory_source";  }
    public String getEmployeeCompetencyDD()         {    return "EDSEmployeeCompetencyHistory_employeeCompetency";  }

    public EDSEmployeeCompetency getEmployeeCompetency() {
        return employeeCompetency;
    }

    public void setEmployeeCompetency(EDSEmployeeCompetency employeeCompetency) {
        this.employeeCompetency = employeeCompetency;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(String evaluator) {
        this.evaluator = evaluator;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
