/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author mostafa
 */
@Entity
public class QualificationTranslation extends BaseEntityTranslation {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "QualificationTranslation_description";
    }
}