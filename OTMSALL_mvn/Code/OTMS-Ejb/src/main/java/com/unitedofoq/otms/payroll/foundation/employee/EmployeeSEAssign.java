package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.payroll.FilterBase;
import com.unitedofoq.otms.payroll.PaymentMethod;
import com.unitedofoq.otms.payroll.PaymentPeriod;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class EmployeeSEAssign extends FilterBase {

    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public String getSalaryElementDD() {
        return "EmployeeSEAssign_salaryElement";
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="paymentMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public String getPaymentMethodDD() {
        return "EmployeeSEAssign_paymentMethod";
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="paymentPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PaymentPeriod paymentPeriod;

    public PaymentPeriod getPaymentPeriod() {
        return paymentPeriod;
    }

    public String getPaymentPeriodDD() {
        return "EmployeeSEAssign_paymentPeriod";
    }

    public void setPaymentPeriod(PaymentPeriod paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="value">
    @Column(precision = 25, scale = 13)
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal elementValue) {
        this.value = elementValue;
    }

    public String getValueDD() {
        return "EmployeeSEAssign_value";
    }
    @Transient
    private BigDecimal valueMask;

    public BigDecimal getValueMask() {
        valueMask = value;
        return valueMask;
    }

    public void setValueMask(BigDecimal valueMask) {
        updateDecimalValue("value", valueMask);
    }

    public String getValueMaskDD() {
        return "EmployeeSEAssign_valueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "EmployeeSEAssign_currency";
    }
    // </editor-fold >
}
