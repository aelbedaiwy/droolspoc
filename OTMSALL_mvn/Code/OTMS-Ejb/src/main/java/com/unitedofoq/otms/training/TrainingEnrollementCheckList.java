/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
public class TrainingEnrollementCheckList extends BusinessObjectBaseEntity {
    
    // <editor-fold defaultstate="collapsed" desc="checkingScope">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC checkingScope;

    public UDC getCheckingScope() {
        return checkingScope;
    }

    public void setCheckingScope(UDC checkingScope) {
        this.checkingScope = checkingScope;
    }

    public String getCheckingScopeDD() {
        return "TrainingEnrollementCheckList_checkingScope";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="checkingCriteria">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC checkingCriteria;

    public UDC getCheckingCriteria() {
        return checkingCriteria;
    }

    public void setCheckingCriteria(UDC checkingCriteria) {
        this.checkingCriteria = checkingCriteria;
    }

    public String getCheckingCriteriaDD() {
        return "TrainingEnrollementCheckList_checkingCriteria";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="locationType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC locationType;

    public UDC getLocationType() {
        return locationType;
    }

    public void setLocationType(UDC locationType) {
        this.locationType = locationType;
    }
    // </editor-fold>
}