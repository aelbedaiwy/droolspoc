/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.occupation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author abdullahm
 */
@MappedSuperclass
public class OccupationElementRatingBase extends BaseEntity{
    
    protected double upperCIBound;
    public String getUpperCIBoundDD(){
        return "OccupationElementRatingBase_upperCIBound";
    }
    @Column(nullable = false)
    protected Integer dataValue;
    public String getDataValueDD(){
        return "OccupationElementRatingBase_dataValue";
    }
    protected double lowerCIBound;
    public String getLowerCIBoundDD(){
        return "OccupationElementRatingBase_lowerCIBound";
    }
    protected String recommendSuppress;
    public String getRecommendSuppressDD(){
        return "OccupationElementRatingBase_recommendSuppress";
    }
    protected double standardError;
    public String getStandardErrorDD(){
        return "OccupationElementRatingBase_standardError";
    }
    protected double weight=100;
    public String getWeightDD(){
        return "OccupationElementRatingBase_weight";
    }
    //assosiation with OccupationElementScale
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    OccupationElementScale scale;

    public Integer getDataValue() {
        return dataValue;
    }

    public void setDataValue(Integer dataValue) {
        this.dataValue = dataValue;
    }

    public double getLowerCIBound() {
        return lowerCIBound;
    }

    public void setLowerCIBound(double lowerCIBound) {
        this.lowerCIBound = lowerCIBound;
    }

    public String getRecommendSuppress() {
        return recommendSuppress;
    }

    public void setRecommendSuppress(String recommendSuppress) {
        this.recommendSuppress = recommendSuppress;
    }

    public double getStandardError() {
        return standardError;
    }

    public void setStandardError(double standardError) {
        this.standardError = standardError;
    }

    public double getUpperCIBound() {
        return upperCIBound;
    }

    public void setUpperCIBound(double upperCIBound) {
        this.upperCIBound = upperCIBound;
    }

    public OccupationElementScale getScale() {
        return scale;
    }

    public void setScale(OccupationElementScale scale) {
        this.scale = scale;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    

}
