package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class Employee3MonthNetSalary extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "employee_dbid")
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "Employee3MonthNetSalary_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="net1">

    @Column(name = "net1")
    private BigDecimal net1;

    public BigDecimal getNet1() {
        return net1;
    }

    public void setNet1(BigDecimal net1) {
        this.net1 = net1;
    }

    public String getNet1DD() {
        return "Employee3MonthNetSalary_net1";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="net2">

    @Column(name = "net2")
    private BigDecimal net2;

    public BigDecimal getNet2() {
        return net2;
    }

    public void setNet2(BigDecimal net2) {
        this.net2 = net2;
    }

    public String getNet2DD() {
        return "Employee3MonthNetSalary_net2";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="net3">
    @Column(name = "net3")
    private BigDecimal net3;

    public BigDecimal getNet3() {
        return net3;
    }

    public void setNet3(BigDecimal net3) {
        this.net3 = net3;
    }

    public String getNet3DD() {
        return "Employee3MonthNetSalary_net3";
    }
    //</editor-fold>

}
