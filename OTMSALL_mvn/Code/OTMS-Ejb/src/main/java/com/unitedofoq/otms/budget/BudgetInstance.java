/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.budget;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import javax.persistence.Entity;

/**
 *
 * @author arezk
 */
@Entity
public class BudgetInstance extends BusinessObjectBaseEntity{
    // <editor-fold defaultstate="collapsed" desc="instanceYear">
    private int instanceYear;

    public int getInstanceYear() {
        return instanceYear;
    }

    public void setInstanceYear(int instanceYear) {
        this.instanceYear = instanceYear;
    }
    
    public String getInstanceYearDD(){
        return "BudgetInstance_instanceYear";
    }
    // </editor-fold>
}
