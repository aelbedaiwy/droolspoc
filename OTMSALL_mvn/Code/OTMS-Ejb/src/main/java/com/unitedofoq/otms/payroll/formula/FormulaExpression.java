package com.unitedofoq.otms.payroll.formula;

import javax.persistence.*;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;


/**
 * The persistent class for the formula_expression database table.
 * 
 */
@Entity
@ParentEntity(fields={"formula"})
public class FormulaExpression extends BaseEntity
{
   
     // <editor-fold defaultstate="collapsed" desc="formulaChanged">
	//If the formula is modified changed flag updated
	@Column(length=1)
	private String formulaChanged;
    public String getFormulaChanged() {
        return formulaChanged;
    }
    public void setFormulaChanged(String formulaChanged) {
        this.formulaChanged = formulaChanged;
    }
     public String getFormulaChangedDD(){
        return "FormulaExpression_formulaChanged";
    }
// </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="formulaExpression">
	@Column
	private String formulaExpression;
    public String getFormulaExpression() {
        return formulaExpression;
    }
    public void setFormulaExpression(String formulaExpression) {
        this.formulaExpression = formulaExpression;
    }
      public String getFormulaExpressionDD(){
        return "FormulaExpression_formulaExpression";
    }
// </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="formula">
	//@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	@JoinColumn(nullable=false)
        @OneToOne(fetch = javax.persistence.FetchType.LAZY)
	private Formula formula;
    public Formula getFormula() {
        return formula;
    }
    public void setFormula(Formula formula) {
        this.formula = formula;
    }
     public String getFormulaDD(){
        return "FormulaExpression_formula";
    }
// </editor-fold>

   
   
  
}