/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.paygrade;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import com.unitedofoq.otms.payroll.paygrade.PayGradeBase;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author mmohamed
 */
@Entity
@Table(name="PayGrade")
@DiscriminatorValue(value ="MASTER")
@ParentEntity(fields="company")
public class EDSPayGrade extends PayGradeBase {
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
	private EDSCompany company;
    public EDSCompany getCompany() {
        return company;
    }
    public void setCompany(EDSCompany company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "PayGrade_company";
    }
    // </editor-fold>
}
