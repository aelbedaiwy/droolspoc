/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.edsvalue;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
public class EDSValue extends BusinessObjectBaseEntity{
    @Column(nullable=false)
    @Translatable(translationField="nameTranslated")
    private String name;
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;

    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getNameTranslatedDD()       {  return "EDSValue_name";  }
    public String getDescriptionTranslatedDD(){  return "EDSValue_description";  }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
}
