package com.unitedofoq.otms.payroll.insurance;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;

import java.math.BigDecimal;

/**
 * 
 */
@Entity
@ParentEntity(fields={"insurance"})
public class InsuranceCategory extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="insurance">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Insurance insurance;
    public Insurance getInsurance() {
        return insurance;
    }
    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }
    public String getInsuranceDD() {
        return "InsuranceCategory_insurance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="basicOrVariable">
    @Column(length=1)
    private String basicOrVariable;
    public String getBasicOrVariable() {
        return basicOrVariable;
    }
    public void setBasicOrVariable(String basicOrVariable) {
        this.basicOrVariable = basicOrVariable;
    }
    
    public String getBasicOrVariableDD() {
        return "InsuranceCategory_basicOrVariable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyDeduction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Deduction companyDeduction;
    public Deduction getCompanyDeduction() {
        return companyDeduction;
    }
    public void setCompanyDeduction(Deduction companyDeduction) {
        this.companyDeduction = companyDeduction;
    }
    public String getCompanyDeductionDD() {
        return "InsuranceCategory_companyDeduction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyPerecnt">
    @Column(precision=25, scale=13)
    private BigDecimal companyPerecnt;
    public BigDecimal getCompanyPerecnt() {
        return companyPerecnt;
    }
    public void setCompanyPerecnt(BigDecimal companyPerecnt) {
        this.companyPerecnt = companyPerecnt;
    }
    public String getCompanyPerecntDD() {
        return "InsuranceCategory_companyPerecnt";
    }
    @Transient
    private BigDecimal companyPerecntMask;
    public BigDecimal getCompanyPerecntMask() {
        companyPerecntMask = companyPerecnt;
        return companyPerecntMask;
    }
    public void setCompanyPerecntMask(BigDecimal companyPerecntMask) {
        updateDecimalValue("companyPerecnt",companyPerecntMask);
    }
    public String getCompanyPerecntMaskDD() {
        return "InsuranceCategory_companyPerecntMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "InsuranceCategory_description";
    }
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeDeduction">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Deduction employeeDeduction;
    public Deduction getEmployeeDeduction() {
        return employeeDeduction;
    }
    public void setEmployeeDeduction(Deduction employeeDeduction) {
        this.employeeDeduction = employeeDeduction;
    }
    public String getEmployeeDeductionDD() {
        return "InsuranceCategory_employeeDeduction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeePercent">
    @Column(precision=25, scale=13)
    private BigDecimal employeePercent;
    public BigDecimal getEmployeePercent() {
        return employeePercent;
    }
    public void setEmployeePercent(BigDecimal employeePercent) {
        this.employeePercent = employeePercent;
    }
    public String getEmployeePercentDD() {
        return "InsuranceCategory_employeePercent";
    }
    @Transient
    private BigDecimal employeePercentMask;
    public BigDecimal getEmployeePercentMask() {
        employeePercentMask = employeePercent ;
        return employeePercentMask;
    }
    public void setEmployeePercentMask(BigDecimal employeePercentMask) {
        updateDecimalValue("employeePercent",employeePercentMask);
    }
    public String getEmployeePercentMaskDD() {
        return "InsuranceCategory_employeePercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
	@Column(nullable=false)
    private int sortIndex;
    public int getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "InsuranceCategory_sortIndex";
    }
    // </editor-fold>
}