package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
//Salema[Employee New Design]
//@ChildEntity(fields={"personalInfo"})
public class EmployeeBase extends BusinessObjectBaseEntity {

    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslatedDD() {
        return "Employee_name";
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;

    }
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    // <editor-fold defaultstate="collapsed" desc="nameInitialsTranslated">
    /////////////////////Initials ///////////////////////
    @Transient
    @Translation(originalField = "nameInitials")
    private String nameInitialsTranslated;

    public String getNameInitialsTranslated() {
        return nameInitialsTranslated;
    }

    public void setNameInitialsTranslated(String nameInitialsTranslated) {
        this.nameInitialsTranslated = nameInitialsTranslated;
    }

    public String getNameInitialsTranslatedDD() {
        return "Employee_nameInitialsTranslated";
    }
    @Translatable(translationField = "nameInitialsTranslated")
    private String nameInitials;

    public String getNameInitials() {
        return nameInitials;
    }

    public void setNameInitials(String nameInitials) {
        this.nameInitials = nameInitials;
    }
//////////////////nameInitials/////////////////
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column(nullable = false, unique = true)
    private String code;

    public String getCodeDD() {
        return "Employee_code";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sf">
//    private boolean sf;
//
//    public boolean isSf() {
//        return sf;
//    }
//
//    public void setSf(boolean sf) {
//        this.sf = sf;
//    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC sf;

    public UDC getSf() {
        return sf;
    }

    public void setSf(UDC sf) {
        this.sf = sf;
    }

    public String getSfDD() {
        return "Employee_sf";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="joinOrganizationDate">
    @Temporal(TemporalType.DATE)
    private Date joinOrganizationDate;

    public Date getJoinOrganizationDate() {
        return joinOrganizationDate;
    }

    public void setJoinOrganizationDate(Date joinOrganizationDate) {
        this.joinOrganizationDate = joinOrganizationDate;
    }

    public String getJoinOrganizationDateDD() {
        return "EDSEmployee_joinOrganizationDate";
    }
    // </editor-fold>

    public String getDirectManagerDD() {
        return "EDSEmployee_directManager";
    }
    private String nameEnc;

    public String getNameEnc() {
        return nameEnc;
    }

    public void setNameEnc(String nameEnc) {
        this.nameEnc = nameEnc;
    }

    private String codeEnc;

    public String getCodeEnc() {
        return codeEnc;
    }

    public void setCodeEnc(String codeEnc) {
        this.codeEnc = codeEnc;
    }

}
