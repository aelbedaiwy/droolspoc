package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value="LEVEL")
@ParentEntity(fields="level")
public class EDSCompetencyLevelBehavior extends EDSCompetencyIndicatorBase{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetencyLevel level;

    public String getLevelDD(){     return "EDSCompetencyLevelBehavior_level";  }

    public EDSCompetencyLevel getLevel() {
        return level;
    }

    public void setLevel(EDSCompetencyLevel level) {
        this.level = level;
    }
}
