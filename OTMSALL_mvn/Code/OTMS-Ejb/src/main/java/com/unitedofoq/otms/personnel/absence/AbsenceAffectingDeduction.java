package com.unitedofoq.otms.personnel.absence;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("ABSENCDEDU")
@ParentEntity(fields={"absence"})
//@FABSEntitySpecs(hideInheritedFields={"salaryElement"})
public class AbsenceAffectingDeduction extends AbsenceAffectingSalaryElement {
  
    // <editor-fold defaultstate="collapsed" desc="deduction">
   @JoinColumn//(name="SALARYELEMENT_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
public String getDeductionDD() {
        return "AbsenceAffectingDeduction_deduction";
    }
// </editor-fold>
  
}
