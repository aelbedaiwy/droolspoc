/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.*;

/**
 *
 * @author lahmed
 */
@Entity
public class BankTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(name = "Description")
    private String bankDescription;

    public String getBankDescription() {
        return bankDescription;
    }

    public void setBankDescription(String bankDescription) {
        this.bankDescription = bankDescription;
    }

    public String getBankDescriptionDD() {
        return "BankTranslation_bankDescription";
    }
    // </editor-fold>
}
