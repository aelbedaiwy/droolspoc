/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Unit")
@ParentEntity(fields = "company")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DTYPE", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "MASTER")
public class UnitBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="lastchildCode">

    @Column
    private int lastChildCode;

    public int getLastChildCode() {
        return lastChildCode;
    }

    public void setLastChildCode(int lasChildCode) {
        this.lastChildCode = lasChildCode;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    @Column(nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslatedDD() {
        return "Unit_name";
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Unit_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hierarchycode">
    @Column
    private String hierarchycode;

    public String getHierarchycode() {
        return hierarchycode;
    }

    public String getHierarchycodeDD() {
        return "Unit_hierarchycode";
    }

    public void setHierarchycode(String hierarchycode) {
        this.hierarchycode = hierarchycode;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nameEnc">
    private String nameEnc;

    public String getNameEnc() {
        return nameEnc;
    }

    public void setNameEnc(String nameEnc) {
        this.nameEnc = nameEnc;
    }
    // </editor-fold>
}
