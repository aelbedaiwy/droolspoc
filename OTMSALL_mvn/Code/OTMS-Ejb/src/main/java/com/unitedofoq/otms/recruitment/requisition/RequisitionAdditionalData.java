/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"requisition"})
public class RequisitionAdditionalData extends BusinessObjectBaseEntity {
// <editor-fold defaultstate="collapsed" desc="requisition">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Requisition requisition;
    public Requisition getRequisition() {
        return requisition;
    }
    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }
      public String getRequisitionDD() {
        return "RequisitionAdditionalData_requisition";
    }
         // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column
    private Long maximumValue;
    public Long getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(Long maximumValue) {
        this.maximumValue = maximumValue;
    }
        public String getMaximumValueDD() {
        return "RequisitionAdditionalData_maximumValue";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="minVal">
    @Column
    private Long minVal;
    public Long getMinVal() {
        return minVal;
    }
    public void setMinVal(Long minVal) {
        this.minVal = minVal;
    }
      public String getMinValDD() {
        return "RequisitionAdditionalData_minVal";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="essential">
   @Column
   private boolean  essential;
    public boolean isEssential() {
        return essential;
    }
    public void setEssential(boolean essential) {
        this.essential = essential;
    }
 public String isEssentialDD() {
        return "RequisitionAdditionalData_essential";
    }
     // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="notes">
	@Column(nullable=true)
	private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getNotesDD() {
        return "RequisitionAdditionalData_notes";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="weight">
    @Column
    private Long weight;
    public Long getWeight() {
        return weight;
    }
    public void setWeight(Long weight) {
        this.weight = weight;
    }
    public String getWeightDD() {
        return "RequisitionAdditionalData_weight";
    }
        // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="requiredLevel">
    @Column
    private BigDecimal requiredLevel;
    public BigDecimal getRequiredLevel() {
        return requiredLevel;
    }
    public void setRequiredLevel(BigDecimal requiredLevel) {
        this.requiredLevel = requiredLevel;
    }
    public String getRequiredLevelDD() {
        return "RequisitionAdditionalData_requiredLevel";
    }
    @Transient
    private BigDecimal requiredLevelMask;
    public BigDecimal getRequiredLevelMask() {
        requiredLevelMask = requiredLevel ;
        return requiredLevelMask;
    }
    public void setRequiredLevelMask(BigDecimal requiredLevelMask) {
        updateDecimalValue("requiredLevel", requiredLevelMask);
    }
    public String getRequiredLevelMaskDD() {
        return "RequisitionAdditionalData_requiredLevelMask";
    }
   // </editor-fold>
}
