package com.unitedofoq.otms.foundation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 *
 * @author tbadran
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ETYPE")
public class NodeColor extends BaseEntity {

    private String nodeColor;
    private int nodeLevel;
    private String description;

    public String getNodeColor() {
        return nodeColor;
    }

    public void setNodeColor(String nodeColor) {
        this.nodeColor = nodeColor;
    }

    public String getNodeColorDD() {
        return "NodeColor_nodeColor";
    }

    public int getNodeLevel() {
        return nodeLevel;
    }

    public void setNodeLevel(int nodeLevel) {
        this.nodeLevel = nodeLevel;
    }

    public String getNodeLevelDD() {
        return "NodeColor_nodeLevel";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionDD() {
        return "NodeColor_description";
    }

}
