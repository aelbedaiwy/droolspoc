package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.health.MedicalCategories;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class PersonnelDependenceBase extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="dependence">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dependence;

    public UDC getDependence() {
        return dependence;
    }

    public void setDependence(UDC dependence) {
        this.dependence = dependence;
    }

    public String getDependenceDD() {
        return "PersonnelDependenceBase_dependence";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependenceName">
    @Column(nullable = false)
    @Translatable(translationField = "dependenceNameTranslated")
    private String dependenceName;

    public String getDependenceName() {
        return dependenceName;
    }

    public void setDependenceName(String dependenceName) {
        this.dependenceName = dependenceName;
    }

    public String getDependenceNameDD() {
        return "PersonnelDependenceBase_dependenceName";
    }
    @Transient
    @Translation(originalField = "dependenceName")
    private String dependenceNameTranslated;

    public String getDependenceNameTranslated() {
        return dependenceNameTranslated;
    }

    public void setDependenceNameTranslated(String dependenceNameTranslated) {
        this.dependenceNameTranslated = dependenceNameTranslated;
    }

    public String getDependenceNameTranslatedDD() {
        return "PersonnelDependenceBase_dependenceName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependenceFirstName">
    @Column//(nullable = false)
    @Translatable(translationField = "dependenceFirstNameTranslated")
    private String dependenceFirstName;

    public String getDependenceFirstName() {
        return dependenceFirstName;
    }

    public void setDependenceFirstName(String dependenceFirstName) {
        this.dependenceFirstName = dependenceFirstName;
    }

    public String getDependenceFirstNameDD() {
        return "PersonnelDependenceBase_dependenceFirstName";
    }
    @Transient
    @Translation(originalField = "dependenceFirstName")
    private String dependenceFirstNameTranslated;

    public String getDependenceFirstNameTranslated() {
        return dependenceFirstNameTranslated;
    }

    public void setDependenceFirstNameTranslated(String dependenceFirstNameTranslated) {
        this.dependenceFirstNameTranslated = dependenceFirstNameTranslated;
    }

    public String getDependenceFirstNameTranslatedDD() {
        return "PersonnelDependenceBase_dependenceFirstName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependencesex">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dependencesex;

    public String getDependencesexDD() {
        return "PersonnelDependenceBase_dependencesex";
    }

    public UDC getDependencesex() {
        return dependencesex;
    }

    public void setDependencesex(UDC dependencesex) {
        this.dependencesex = dependencesex;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="depndenceDob">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date depndenceDob;

    public Date getDepndenceDob() {
        return depndenceDob;
    }

    public void setDepndenceDob(Date depndenceDob) {
        this.depndenceDob = depndenceDob;
    }

    public String getDepndenceDobDD() {
        return "PersonnelDependenceBase_depndenceDob";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column(name = "notes")
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNoteseDD() {
        return "PersonnelDependenceBase_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceCover">
    @Column
    private boolean insuranceCover;

    public boolean isInsuranceCover() {
        return insuranceCover;
    }

    public void setInsuranceCover(boolean insuranceCover) {
        this.insuranceCover = insuranceCover;
    }

    public String getInsuranceCoverDD() {
        return "PersonnelDependenceBase_insuranceCover";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxCover">
    @Column
    private boolean taxCover;

    public boolean istaxCover() {
        return taxCover;
    }

    public void setTaxCover(boolean taxCover) {
        this.taxCover = taxCover;
    }

    public String getTaxCoverDD() {
        return "PersonnelDependenceBase_taxCover";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="universityExempted">
    @Column
    private boolean universityExempted;

    public boolean isUniversityExempted() {
        return universityExempted;
    }

    public void setUniversityExempted(boolean universityExempted) {
        this.universityExempted = universityExempted;
    }

    public String getUniversityExemptedDD() {
        return "PersonnelDependenceBase_universityExempted";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="marriedDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date marriedDate;

    public Date getMarriedDate() {
        return marriedDate;
    }

    public void setMarriedDate(Date marriedDate) {
        this.marriedDate = marriedDate;
    }

    public String getMarriedDateDD() {
        return "PersonnelDependenceBase_marriedDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependenceLastName">
    @Column
    @Translatable(translationField = "dependenceLastNameTranslated")
    private String dependenceLastName;

    public String getDependenceLastName() {
        return dependenceLastName;
    }

    public void setDependenceLastName(String dependenceLastName) {
        this.dependenceLastName = dependenceLastName;
    }

    public String getDependenceLastNameDD() {
        return "PersonnelDependenceBase_dependenceLastName";
    }
    @Transient
    @Translation(originalField = "dependenceLastName")
    private String dependenceLastNameTranslated;

    public String getDependenceLastNameTranslated() {
        return dependenceLastNameTranslated;
    }

    public void setDependenceLastNameTranslated(String dependenceLastNameTranslated) {
        this.dependenceLastNameTranslated = dependenceLastNameTranslated;
    }

    public String getDependenceLastNameTranslatedDD() {
        return "PersonnelDependenceBase_dependenceLastName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependanceAddress">
    @Column
    @Translatable(translationField = "dependanceAddressTranslated")
    private String dependanceAddress;

    public String getDependanceAddress() {
        return dependanceAddress;
    }

    public void setDependanceAddress(String dependanceAddress) {
        this.dependanceAddress = dependanceAddress;
    }

    public String getDependanceAddressDD() {
        return "PersonnelDependenceBase_dependanceAddress";
    }
    @Transient
    @Translation(originalField = "dependanceAddress")
    private String dependanceAddressTranslated;

    public String getDependanceAddressTranslated() {
        return dependanceAddressTranslated;
    }

    public void setDependanceAddressTranslated(String dependanceAddressTranslated) {
        this.dependanceAddressTranslated = dependanceAddressTranslated;
    }

    public String getDependanceAddressTranslatedDD() {
        return "PersonnelDependenceBase_dependanceAddress";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="phone">
    @Column
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneDD() {
        return "PersonnelDependenceBase_phone";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="passportNo">
    @Column
    private String passportNo;

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportNoDD() {
        return "PersonnelDependenceBase_passportNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueDateDD() {
        return "PersonnelDependenceBase_issueDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDate;

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewalDateDD() {
        return "PersonnelDependenceBase_renewalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueLocation">
    //@JoinColumn
    //  @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    //  private Location issueLocation;
    //  public Location getIssueLocation() {
    //       return issueLocation;
    //   }
    //   public void setIssueLocation(Location issueLocation) {
    //       this.issueLocation = issueLocation;
    //   }
    //   public String getIssueLocationDD() {
    //       return "PersonnelDependenceBase_issueLocation";
    //  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issuedBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee issuedBy;//(name = "cancelled_by")

    public Employee getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(Employee issuedBy) {
        this.issuedBy = issuedBy;
    }

    public String getIssuedByDD() {
        return "PersonnelDependenceBase_issuedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependancePermanentAddress">
    @Column
    @Translatable(translationField = "dependancePermanentAddressTranslated")
    private String dependancePermanentAddress;

    public String getDependancePermanentAddress() {
        return dependancePermanentAddress;
    }

    public void setDependancePermanentAddress(String dependancePermanentAddress) {
        this.dependancePermanentAddress = dependancePermanentAddress;
    }

    public String getDependancePermanentAddressDD() {
        return "PersonnelDependenceBase_dependancePermanentAddress";
    }
    @Transient
    @Translation(originalField = "dependancePermanentAddress")
    private String dependancePermanentAddressTranslated;

    public String getDependancePermanentAddressTranslated() {
        return dependancePermanentAddressTranslated;
    }

    public void setDependancePermanentAddressTranslated(String dependancePermanentAddressTranslated) {
        this.dependancePermanentAddressTranslated = dependancePermanentAddressTranslated;
    }

    public String getDependancePermanentAddressTranslatedDD() {
        return "PersonnelDependenceBase_dependancePermanentAddress";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependenceMiddleName">
    @Column
    @Translatable(translationField = "dependenceMiddleNameTranslated")
    private String dependenceMiddleName;

    public String getDependenceMiddleName() {
        return dependenceMiddleName;
    }

    public void setDependenceMiddleName(String dependenceMiddleName) {
        this.dependenceMiddleName = dependenceMiddleName;
    }

    public String getDependenceMiddleNameDD() {
        return "PersonnelDependenceBase_dependenceMiddleName";
    }
    @Transient
    @Translation(originalField = "dependenceMiddleName")
    private String dependenceMiddleNameTranslated;

    public String getDependenceMiddleNameTranslated() {
        return dependenceMiddleNameTranslated;
    }

    public void setDependenceMiddleNameTranslated(String dependenceMiddleNameTranslated) {
        this.dependenceMiddleNameTranslated = dependenceMiddleNameTranslated;
    }

    public String getDependenceMiddleNameTranslatedDD() {
        return "PersonnelDependenceBase_dependenceMiddleName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="working">
    @Column
    private boolean working;

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public String getWorkingDD() {
        return "PersonnelDependenceBase_working";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="studingCertification">
    @Column
    private boolean studingCertification;

    public boolean isStudingCertification() {
        return studingCertification;
    }

    public void setStudingCertification(boolean studingCertification) {
        this.studingCertification = studingCertification;
    }

    public String getStudingCertificationDD() {
        return "PersonnelDependenceBase_studingCertification";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maritalStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC maritalStatus;

    public String getMaritalStatusDD() {
        return "PersonnelDependenceBase_maritalStatus";
    }

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="depAge">
    @Column
    private BigDecimal depAge;

    public BigDecimal getDepAge() {
        return depAge;
    }

    public void setDepAge(BigDecimal depAge) {
        this.depAge = depAge;
    }

    public String getDepAgeDD() {
        return "PersonnelDependenceBase_depAge";
    }
    @Transient
    private BigDecimal depAgeMask;

    public BigDecimal getDepAgeMask() {
        depAgeMask = depAge;
        return depAgeMask;
    }

    public void setDepAgeMask(BigDecimal depAgeMask) {
        updateDecimalValue("depAge", depAgeMask);
    }

    public String getDepAgeMaskDD() {
        return "PersonnelDependenceBase_depAgeMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="job">
    @Column
    @Translatable(translationField = "jobTranslated")
    private String job;

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getJobDD() {
        return "PersonnelDependenceBase_job";
    }
    @Transient
    @Translation(originalField = "job")
    private String jobTranslated;

    public String getJobTranslated() {
        return jobTranslated;
    }

    public void setJobTranslated(String jobTranslated) {
        this.jobTranslated = jobTranslated;
    }

    public String getJobTranslatedDD() {
        return "PersonnelDependenceBase_job";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="education">
    //   @JoinColumn
    //   @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    //   private JobAppEducation education;
    //   public JobAppEducation getEducation() {
    //      return education;
    //   }
    //    public void setEducation(JobAppEducation education) {
    //        this.education = education;
    //  }
    //   public String getEducationDD() {
    //      return "PersonnelDependenceBase_education";
    //   }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependenceStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dependenceStatus;

    public String getDependenceStatusDD() {
        return "PersonnelDependenceBase_dependenceStatus";
    }

    public UDC getDependenceStatus() {
        return dependenceStatus;
    }

    public void setDependenceStatus(UDC dependenceStatus) {
        this.dependenceStatus = dependenceStatus;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC paidStatus;

    public String getPaidStatusDD() {
        return "PersonnelDependenceBase_paidStatus";
    }

    public UDC getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(UDC paidSatus) {
        this.paidStatus = paidSatus;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statusChng">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusChng;

    public Date getStatusChng() {
        return statusChng;
    }

    public void setStatusChng(Date statusChng) {
        this.statusChng = statusChng;
    }

    public String getStatusChngDD() {
        return "PersonnelDependenceBase_statusChng";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="visaId">
    @Column
    private String visaId;

    public String getVisaId() {
        return visaId;
    }

    public void setVisaId(String visaId) {
        this.visaId = visaId;
    }

    public String getVisaIdDD() {
        return "PersonnelDependenceBase_visaId";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="iqamaId">
    @Column
    private String iqamaId;

    public String getIqamaId() {
        return iqamaId;
    }

    public void setIqamaId(String iqamaId) {
        this.iqamaId = iqamaId;
    }

    public String getIqamaIdDD() {
        return "PersonnelDependenceBase_iqamaId";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="iqIssueDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date iqIssueDate;

    public Date getIqIssueDate() {
        return iqIssueDate;
    }

    public void setIqIssueDate(Date iqIssueDate) {
        this.iqIssueDate = iqIssueDate;
    }

    public String getIqIssueDateDD() {
        return "PersonnelDependenceBase_iqIssueDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="iqExpiryDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date iqExpiryDate;

    public Date getIqExpiryDate() {
        return iqExpiryDate;
    }

    public void setIqExpiryDate(Date iqExpiryDate) {
        this.iqExpiryDate = iqExpiryDate;
    }

    public String getIqExpiryDateDD() {
        return "PersonnelDependenceBase_iqExpiryDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="iqIssuePlace">
    @Column
    private String iqIssuePlace;

    public String getIqIssuePlace() {
        return iqIssuePlace;
    }

    public void setIqIssuePlace(String iqIssuePlace) {
        this.iqIssuePlace = iqIssuePlace;
    }

    public String getIqIssuePlaceDD() {
        return "PersonnelDependenceBase_iqIssuePlace";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="birthNationality">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC birthNationality;

    public String getBirthNationalityDD() {
        return "PersonnelDependenceBase_birthNationality";
    }

    public UDC getBirthNationality() {
        return birthNationality;
    }

    public void setBirthNationality(UDC birthNationality) {
        this.birthNationality = birthNationality;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nationality">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nationality;

    public String getNationalityDD() {
        return "PersonnelDependenceBase_nationality";
    }

    public UDC getNationality() {
        return nationality;
    }

    public void setNationality(UDC nationality) {
        this.nationality = nationality;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="religion">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC religion;

    public String getReligionDD() {
        return "PersonnelDependenceBase_religion";
    }

    public UDC getReligion() {
        return religion;
    }

    public void setReligion(UDC religion) {
        this.religion = religion;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Medicalinsured">
    @Column(length = 1)
    private String medicalInsured;

    public String getMedicalInsured() {
        return medicalInsured;
    }

    public void setMedicalInsured(String medicalInsured) {
        this.medicalInsured = medicalInsured;
    }

    public String getMedicalInsuredDD() {
        return "PersonnelDependenceBase_medicalInsured";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Medicalamount">
    private BigDecimal medicalAmount;

    public BigDecimal getMedicalAmount() {
        return medicalAmount;
    }

    public String getMedicalAmountDD() {
        return "PersonnelDependenceBase_medicalAmount";
    }

    public void setMedicalAmount(BigDecimal medicalAmount) {
        this.medicalAmount = medicalAmount;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="MedicalCategories">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private MedicalCategories medicalcategories;

    public MedicalCategories getMedicalcategories() {
        return medicalcategories;
    }

    public String getMedicalcategoriesDD() {
        return "PersonnelDependenceBase_medicalcategories";
    }

    public void setMedicalcategories(MedicalCategories medicalcategories) {
        this.medicalcategories = medicalcategories;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalGlassesStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC medicalGlassesStatus;

    public UDC getMedicalGlassesStatus() {
        return medicalGlassesStatus;
    }

    public String getMedicalGlassesStatusDD() {
        return "PersonnelDependenceBase_medicalGlassesStatus";
    }

    public void setMedicalGlassesStatus(UDC medicalGlassesStatus) {
        this.medicalGlassesStatus = medicalGlassesStatus;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="medicalGlassesDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date medicalGlassesDate;

    public Date getMedicalGlassesDate() {
        return medicalGlassesDate;
    }

    public String getMedicalGlassesDateDD() {
        return "PersonnelDependenceBase_medicalGlassesDate";
    }

    public void setMedicalGlassesDate(Date medicalGlassesDate) {
        this.medicalGlassesDate = medicalGlassesDate;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="consumedMedicalAmount">
    private BigDecimal consumedMedicalAmount;

    public BigDecimal getConsumedMedicalAmount() {
        return consumedMedicalAmount;
    }

    public String getConsumedMedicalAmountDD() {
        return "PersonnelDependenceBase_consumedMedicalAmount";
    }

    public void setConsumedMedicalAmount(BigDecimal consumedMedicalAmount) {
        this.consumedMedicalAmount = consumedMedicalAmount;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="MedicalAmountEnc">
    private String medicalAmountEnc;

    public String getMedicalAmountEnc() {
        return medicalAmountEnc;
    }

    public void setMedicalAmountEnc(String medicalAmountEnc) {
        this.medicalAmountEnc = medicalAmountEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="consumedMedicalAmountEnc">    
    private String consumedMedicalAmountEnc;

    public String getConsumedMedicalAmountEnc() {
        return consumedMedicalAmountEnc;
    }

    public void setConsumedMedicalAmountEnc(String consumedMedicalAmountEnc) {
        this.consumedMedicalAmountEnc = consumedMedicalAmountEnc;
    }
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="depndenceDobEnc">        
    private String depndenceDobEnc;

    public String getDepndenceDobEnc() {
        return depndenceDobEnc;
    }

    public void setDepndenceDobEnc(String depndenceDobEnc) {
        this.depndenceDobEnc = depndenceDobEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependenceNameEnc">
    private String dependenceNameEnc;

    public String getDependenceNameEnc() {
        return dependenceNameEnc;
    }

    public void setDependenceNameEnc(String dependenceNameEnc) {
        this.dependenceNameEnc = dependenceNameEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="phoneEnc">
    private String phoneEnc;

    public String getPhoneEnc() {
        return phoneEnc;
    }

    public void setPhoneEnc(String phoneEnc) {
        this.phoneEnc = phoneEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependancePermanentAddressEnc">
    private String dependancePermanentAddressEnc;

    public String getDependancePermanentAddressEnc() {
        return dependancePermanentAddressEnc;
    }

    public void setDependancePermanentAddressEnc(String dependancePermanentAddressEnc) {
        this.dependancePermanentAddressEnc = dependancePermanentAddressEnc;
    }
    //</editor-fold>
}
