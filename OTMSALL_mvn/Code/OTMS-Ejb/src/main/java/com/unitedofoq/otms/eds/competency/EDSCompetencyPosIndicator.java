package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value="COMPPOS")
@ParentEntity(fields="competency")
public class EDSCompetencyPosIndicator extends EDSCompetencyIndicatorBase{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetency competency;
    public String getCompetencyDD()      {    return "EDSCompetencyPosIndicator_competency";  }
    public EDSCompetency getCompetency() {
        return competency;
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }
}
