/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author lap2
 */
@Entity

public class EmployeeOtherInfoTranslation extends BaseEntityTranslation {
    

        // <editor-fold defaultstate="collapsed" desc="educationDegree">
       private String educationDegree;
       public String getEducationDegree() {
           return educationDegree;
       }

       public void setEducationDegree(String educationDegree) {
           this.educationDegree = educationDegree;
       }
       public String getEducationDegreeDD() {
           return "EmployeeOtherInfo_educationDegree";
       }

           // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="grade">
       private String grade;

       public String getGrade() {
           return grade;
       }

       public void setGrade(String grade) {
           this.grade = grade;
       }
       public String getGradeDD() {
           return "EmployeeOtherInfo_grade";
       }
           // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="graduationPlace">
       private  String graduationPlace;
       public String getGraduationPlace() {
           return graduationPlace;
       }

       public void setGraduationPlace(String graduationPlace) {
           this.graduationPlace = graduationPlace;
       }
        public String getGraduationPlaceDD() {
           return "EmployeeOtherInfo_graduationPlace";
       }
           // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="healthCertificate">  
          private String healthCertificate;

        public String getHealthCertificate() {
            return healthCertificate;
        }

        public void setHealthCertificate(String healthCertificate) {
            this.healthCertificate = healthCertificate;
        }
         public String getHealthCertificateDD() {
            return "EmployeeOtherInfo_healthCertificate";
        }
                  // </editor-fold>  

}
