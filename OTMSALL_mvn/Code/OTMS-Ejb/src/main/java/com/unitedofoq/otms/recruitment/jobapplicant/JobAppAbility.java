
package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccuElementGradeBase;
import com.unitedofoq.otms.foundation.occupation.Ability;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobAppAbility extends OccuElementGradeBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
	private JobApplicant jobApplicant;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Ability ability;
	public String getAbilityDD(){
        return "JobAppAbility_ability";
    }
	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant theJobApplicant) {
		jobApplicant = theJobApplicant;
	}

	public Ability getAbility() {
		return ability;
	}

	public void setAbility(Ability theAbility) {
		ability = theAbility;
	}
}
