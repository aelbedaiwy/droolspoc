
package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepRatingSchemaTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="ratingDescription">
    @Column
    private String ratingDescription;

    public void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription;
    }

    public String getRatingDescription() {
        return ratingDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="levelDescription">
    @Column
    private String levelDescription;

    public void setLevelDescription(String levelDescription) {
        this.levelDescription = levelDescription;
    }

    public String getLevelDescription() {
        return levelDescription;
    }
    // </editor-fold>

}
