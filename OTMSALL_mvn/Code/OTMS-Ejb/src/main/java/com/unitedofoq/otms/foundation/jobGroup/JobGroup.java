/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.jobGroup;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@Entity
@DiscriminatorValue("MASTER")
@ParentEntity(fields="company")
//@ChildEntity(fields={"jobs","responsabilitys","tasks","trainings","certificates","educations","competencies","beheviours","values"})
@ChildEntity(fields={"responsabilitys","tasks","trainings","certificates","educations","competencies","beheviours","values"})
public class JobGroup extends BusinessObjectBaseEntity   {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "JobGroup_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobs">
   /* @OneToMany(mappedBy="jobGroup")
	private List<Job> jobs;
     public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

     public String getJobsDD() {
        return "JobGroup_jobs";
    }*/
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "JobGroup_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="responsabilitys">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupResponsability> responsabilitys;
    

    public List<JobGroupResponsability> getResponsabilitys() {
        return responsabilitys;
    }

    public void setResponsabilitys(List<JobGroupResponsability> responsabilitys) {
        this.responsabilitys = responsabilitys;
    }

    public String getResponsabilitysDD() {
        return "JobGroup_responsabilitys";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tasks">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupTask> tasks;

    public List<JobGroupTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<JobGroupTask> tasks) {
        this.tasks = tasks;
    }

    public String getTasksDD() {
        return "JobGroup_tasks";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainings">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupTraining> trainings;

    public List<JobGroupTraining> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<JobGroupTraining> trainings) {
        this.trainings = trainings;
    }


    public String getTrainingsDD() {
        return "JobGroup_trainings";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certificates">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupCertificate> certificates;

    public List<JobGroupCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<JobGroupCertificate> certificates) {
        this.certificates = certificates;
    }

    public String getCertificatesDD() {
        return "JobGroup_certificates";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="educations">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupEducation> educations;

    public List<JobGroupEducation> getEducations() {
        return educations;
    }

    public void setEducations(List<JobGroupEducation> educations) {
        this.educations = educations;
    }

    public String getEducationsDD() {
        return "JobGroup_educations";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competencies">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupCompetency> competencies;

    public List<JobGroupCompetency> getCompetencies() {
        return competencies;
    }

    public void setCompetencies(List<JobGroupCompetency> competencies) {
        this.competencies = competencies;
    }

    public String getCompetenciesDD() {
        return "JobGroup_competencies";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="beheviours">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupBeheviour> beheviours;

    public List<JobGroupBeheviour> getBeheviours() {
        return beheviours;
    }

    public void setBeheviours(List<JobGroupBeheviour> beheviours) {
        this.beheviours = beheviours;
    }

    public String getBehevioursDD() {
        return "JobGroup_beheviours";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="values">
	@OneToMany(mappedBy="jobGroup")
	private List<JobGroupValue> values;

    public List<JobGroupValue> getValues() {
        return values;
    }

    public void setValues(List<JobGroupValue> values) {
        this.values = values;
    }

    public String getValuesDD() {
        return "JobGroup_values";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumExperience">
   @Column(precision=25, scale=13)
   private BigDecimal minimumExperience;

    public BigDecimal getMinimumExperience() {
        return minimumExperience;
    }

    public void setMinimumExperience(BigDecimal minimumExperience) {
        this.minimumExperience = minimumExperience;
    }

   public String getMinimumExperienceDD() {
        return "JobGroup_minimumExperience";
    }
    @Transient
    private BigDecimal minimumExperienceMask = java.math.BigDecimal.ZERO;
    public BigDecimal getMinimumExperienceMask() {
        minimumExperienceMask = minimumExperience ;
        return minimumExperienceMask;
    }
    public void setMinimumExperienceMask(BigDecimal minimumExperienceMask) {
        updateDecimalValue("minimumExperience",minimumExperienceMask);
    }
    public String getMinimumExperienceMaskDD() {
        return "JobGroup_minimumExperienceMask";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumExperience">
   @Column(precision=25, scale=13)
   private BigDecimal maximumExperience;

    public BigDecimal getMaximumExperience() {
        return maximumExperience;
    }

    public void setMaximumExperience(BigDecimal maximumExperience) {
        this.maximumExperience = maximumExperience;
    }

   public String getMaximumExperienceDD() {
        return "JobGroup_maximumExperience";
    }
    @Transient
    private BigDecimal maximumExperienceMask = java.math.BigDecimal.ZERO;
    public BigDecimal getMaximumExperienceMask() {
        maximumExperienceMask = maximumExperience ;
        return maximumExperienceMask;
    }
    public void setMaximumExperienceMask(BigDecimal maximumExperienceMask) {
        updateDecimalValue("maximumExperience",maximumExperienceMask);
    }
    public String getMaximumExperienceMaskDD() {
        return "JobGroup_maximumExperienceMask";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="experienceMeasureUnit">
   @JoinColumn
   @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
   private UDC experienceMeasureUnit;

    public UDC getExperienceMeasureUnit() {
        return experienceMeasureUnit;
    }

    public void setExperienceMeasureUnit(UDC experienceMeasureUnit) {
        this.experienceMeasureUnit = experienceMeasureUnit;
    }

   
    public String getExperienceMeasureUnitDD() {
        return "JobGroup_experienceMeasureUnit";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
   
    @JoinColumn(nullable= false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "JobGroup_company";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="workDurationMeasuerUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC workDurationMeasuerUnit;
    public String getWorkDurationMeasuerUnitDD() {
            return "JobGroup_workDurationMeasuerUnit";
    }
    public UDC getWorkDurationMeasuerUnit() {
         return workDurationMeasuerUnit;
    }
    public void setWorkDurationMeasuerUnit(UDC workDurationMeasuerUnit) {
            this.workDurationMeasuerUnit = workDurationMeasuerUnit;
    }
    // </editor-fold>
}
