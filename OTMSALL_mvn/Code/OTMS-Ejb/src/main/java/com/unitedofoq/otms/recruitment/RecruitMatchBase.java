package com.unitedofoq.otms.recruitment;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.*;

@MappedSuperclass
public class RecruitMatchBase extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="gender">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC gender;

    public void setGender(UDC gender) {
        this.gender = gender;
    }

    public UDC getGender() {
        return gender;
    }

    public String getGenderDD() {
        return "RecruitMatchBase_gender";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC location;

    public void setLocation(UDC location) {
        this.location = location;
    }

    public UDC getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "RecruitMatchBase_location";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="country">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC country;

    public void setCountry(UDC country) {
        this.country = country;
    }

    public UDC getCountry() {
        return country;
    }

    public String getCountryDD() {
        return "RecruitMatchBase_country";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="city">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC city;

    public void setCity(UDC city) {
        this.city = city;
    }

    public UDC getCity() {
        return city;
    }

    public String getCityDD() {
        return "RecruitMatchBase_city";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="area">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC area;

    public void setArea(UDC area) {
        this.area = area;
    }

    public UDC getArea() {
        return area;
    }

    public String getAreaDD() {
        return "RecruitMatchBase_area";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="experienceYears">
    @Column
    private Double experienceYears;

    public void setExperienceYears(Double experienceYears) {
        this.experienceYears = experienceYears;
    }

    public Double getExperienceYears() {
        return experienceYears;
    }

    public String getExperienceYearsDD() {
        return "RecruitMatchBase_experienceYears";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="careerLevel">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC careerLevel;

    public void setCareerLevel(UDC careerLevel) {
        this.careerLevel = careerLevel;
    }

    public UDC getCareerLevel() {
        return careerLevel;
    }

    public String getCareerLevelDD() {
        return "RecruitMatchBase_careerLevel";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="haveCar">
    @Column
    private String haveCar;

    public void setHaveCar(String haveCar) {
        this.haveCar = haveCar;
    }

    public String getHaveCar() {
        return haveCar;
    }

    public String getHaveCarDD() {
        return "RecruitMatchBase_haveCar";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="haveDrivingLicense">
    @Column
    private String haveDriveLic;

    public void setHaveDriveLic(String haveDrivingLicense) {
        this.haveDriveLic = haveDrivingLicense;
    }

    public String getHaveDriveLic() {
        return haveDriveLic;
    }

    public String getHaveDriveLicDD() {
        return "RecruitMatchBase_haveDriveLic";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobTitle">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC jobTitle;

    public void setJobTitle(UDC jobTitle) {
        this.jobTitle = jobTitle;
    }

    public UDC getJobTitle() {
        return jobTitle;
    }

    public String getJobTitleDD() {
        return "RecruitMatchBase_jobTitle";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="keywords">
    private String keywords;

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getKeywordsDD() {
        return "RecruitMatchBase_keywords";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="highestEduLevel">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC highestEduLevel;

    public UDC getHighestEduLevel() {
        return highestEduLevel;
    }

    public void setHighestEduLevel(UDC highestEduLevel) {
        this.highestEduLevel = highestEduLevel;
    }

    public String getHighestEduLevelDD() {
        return "RecruitMatchBase_highestEduLevel";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobCategory">
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    private UDC jobCategory;

    public UDC getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(UDC jobCategory) {
        this.jobCategory = jobCategory;
    }

    public String getJobCategoryDD() {
        return "RecruitMatchBase_jobCategory";
    }
    // </editor-fold>
}