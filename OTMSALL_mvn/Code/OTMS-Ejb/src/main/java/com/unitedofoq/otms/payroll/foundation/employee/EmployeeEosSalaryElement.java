/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeEosSalaryElement extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeEosSalaryElement_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacDays">
    @Column(precision=25,scale=13)
    private BigDecimal vacDays = new BigDecimal(0);

    public BigDecimal getVacDays() {
        return vacDays;
    }

    public void setVacDays(BigDecimal vacDays) {
        this.vacDays = vacDays;
    }
    
    public String getVacDaysDD() {
        return "EmployeeEosSalaryElement_vacDays";
    }
    @Transient
    private BigDecimal vacDaysMask;

    public BigDecimal getVacDaysMask() {
        vacDaysMask = vacDays;
        return vacDaysMask;
    }

    public void setVacDaysMask(BigDecimal vacDaysMask) {
        updateDecimalValue("vacDays", vacDaysMask);
    }
    
    public String getVacDaysMaskDD() {
        return "EmployeeEosSalaryElement_vacDaysMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="calculatedVacDays">
    @Column(precision=25,scale=13)
    private BigDecimal calculatedVacDays= new BigDecimal(0);

    public BigDecimal getCalculatedVacDays() {
        return calculatedVacDays;
    }

    public void setCalculatedVacDays(BigDecimal calculatedVacDays) {
        this.calculatedVacDays = calculatedVacDays;
    }
    
    public String getCalculatedVacDaysDD() {
        return "EmployeeEosSalaryElement_calculatedVacDays";
    }
    @Transient
    private BigDecimal calculatedVacDaysMask;

    public BigDecimal getCalculatedVacDaysMask() {
        calculatedVacDaysMask = calculatedVacDays;
        return calculatedVacDaysMask;
    }

    public void setCalculatedVacDaysMask(BigDecimal calculatedVacDaysMask) {
        updateDecimalValue("calculatedVacDays", calculatedVacDaysMask);
    }
    
    public String getCalculatedVacDaysMaskDD() {
        return "EmployeeEosSalaryElement_calculatedVacDaysMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="vacValue">
    @Column(precision=25,scale=13)
    private BigDecimal vacValue= new BigDecimal(0);

    public BigDecimal getVacValue() {
        return vacValue;
    }

    public void setVacValue(BigDecimal vacValue) {
        this.vacValue = vacValue;
    }
    
    public String getVacValueDD() {
        return "EmployeeEosSalaryElement_vacValue";
    }
    @Transient
    private BigDecimal vacValueMask;

    public BigDecimal getVacValueMask() {
        vacValueMask = vacValue;
        return vacValueMask;
    }

    public void setVacValueMask(BigDecimal vacValueMask) {
        updateDecimalValue("vacValue", vacValueMask);
    }
    
    public String getVacValueMaskDD() {
        return "EmployeeEosSalaryElement_vacValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="servicePeriod">
    @Column(precision=25,scale=13)
    private BigDecimal servicePeriod= new BigDecimal(0);

    public BigDecimal getServicePeriod() {
        return servicePeriod;
    }

    public void setServicePeriod(BigDecimal servicePeriod) {
        this.servicePeriod = servicePeriod;
    }
    
    public String getServicePeriodDD() {
        return "EmployeeEosSalaryElement_servicePeriod";
    }
    @Transient
    private BigDecimal servicePeriodMask;

    public BigDecimal getServicePeriodMask() {
        servicePeriodMask = servicePeriod;
        return servicePeriodMask;
    }

    public void setServicePeriodMask(BigDecimal servicePeriodMask) {
        this.servicePeriod = servicePeriodMask;
    }
    
    public String getServicePeriodMaskDD() {
        return "EmployeeEosSalaryElement_servicePeriodMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calcServicePeriod">
    @Column(precision=25,scale=13)
    private BigDecimal calcServicePeriod= new BigDecimal(0);

    public BigDecimal getCalcServicePeriod() {
        return calcServicePeriod;
    }

    public void setCalcServicePeriod(BigDecimal calcServicePeriod) {
        this.calcServicePeriod = calcServicePeriod;
    }
    
    public String getCalcServicePeriodDD() {
        return "EmployeeEosSalaryElement_calcServicePeriod";
    }
    @Transient
    private BigDecimal calcServicePeriodMask;

    public BigDecimal getCalcServicePeriodMask() {
        calcServicePeriodMask = calcServicePeriod;
        return calcServicePeriodMask;
    }

    public void setCalcServicePeriodMask(BigDecimal calcServicePeriodMask) {
        updateDecimalValue("calcServicePeriod", calcServicePeriodMask);
    }
    
    public String getCalcServicePeriodMaskDD() {
        return "EmployeeEosSalaryElement_calcServicePeriodMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="calcBonus">
    @Column(precision=25,scale=13)
    private BigDecimal calcBonus= new BigDecimal(0);

    public BigDecimal getCalcBonus() {
        return calcBonus;
    }

    public void setCalcBonus(BigDecimal calcBonus) {
        this.calcBonus = calcBonus;
    }
    
    public String getCalcBonusDD() {
        return "EmployeeEosSalaryElement_calcBonus";
    }
    @Transient
    private BigDecimal calcBonusMask;

    public BigDecimal getCalcBonusMask() {
        calcBonusMask = calcBonus;
        return calcBonusMask;
    }

    public void setCalcBonusMask(BigDecimal calcBonusMask) {
        updateDecimalValue("calcBonus", calcBonusMask);
    }
    
    public String getCalcBonusMaskDD() {
        return "EmployeeEosSalaryElement_calcBonusMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="bonus">
    @Column(precision=25,scale=13)
    private BigDecimal bonus= new BigDecimal(0);

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }
    
    public String getBonusDD() {
        return "EmployeeEosSalaryElement_bonus";
    }
    @Transient
    private BigDecimal bonusMask;

    public BigDecimal getBonusMask() {
        bonusMask = bonus;
        return bonusMask;
    }

    public void setBonusMask(BigDecimal bonusMask) {
        updateDecimalValue("bonus", bonusMask);
    }
    
    public String getBonusMaskDD() {
        return "EmployeeEosSalaryElement_bonusMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="bonusPercentage">
    @Column(precision=25,scale=13)
    private BigDecimal bonusPercentage= new BigDecimal(0);

    public BigDecimal getBonusPercentage() {
        return bonusPercentage;
    }

    public void setBonusPercentage(BigDecimal bonusPercentage) {
        this.bonusPercentage = bonusPercentage;
    }
    
    public String getBonusPercentageDD() {
        return "EmployeeEosSalaryElement_bonusPercentage";
    }
    @Transient
    private BigDecimal bonusPercentageMask;

    public BigDecimal getBonusPercentageMask() {
        bonusPercentageMask = bonusPercentage;
        return bonusPercentageMask;
    }

    public void setBonusPercentageMask(BigDecimal bonusPercentageMask) {
        updateDecimalValue("bonusPercentage", bonusPercentageMask);
    }
    
    public String getBonusPercentageMaskDD() {
        return "EmployeeEosSalaryElement_bonusPercentageMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="calcBonusPercentage">
    @Column(precision=25,scale=13)
    private BigDecimal calcBonusPercentage= new BigDecimal(0);

    public BigDecimal getCalcBonusPercentage() {
        return calcBonusPercentage;
    }

    public void setCalcBonusPercentage(BigDecimal calcBonusPercentage) {
        this.calcBonusPercentage = calcBonusPercentage;
    }
    
    public String getCalcBonusPercentageDD() {
        return "EmployeeEosSalaryElement_calcBonusPercentage";
    }
    @Transient
    private BigDecimal calcBonusPercentageMask;

    public BigDecimal getCalcBonusPercentageMask() {
        calcBonusPercentageMask = calcBonusPercentage;
        return calcBonusPercentageMask;
    }

    public void setCalcBonusPercentageMask(BigDecimal calcBonusPercentageMask) {
        updateDecimalValue("calcBonusPercentage", calcBonusPercentageMask);
    }
    
    public String getCalcBonusPercentageMaskDD() {
        return "EmployeeEosSalaryElement_calcBonusPercentageMask";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="settled">
    @Column(nullable=false, length=1) // Y-N
    private String settled = "N" ;

    public String getSettled() {
        return settled;
    }

    public void setSettled(String settled) {
        this.settled = settled;
    }
    
    public String getSettledDD() {
        return "EmployeeEosSalaryElement_settled";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }
   
     public String getCalculatedPeriodDD() {
        return "EmployeeEosSalaryElement_calculatedPeriod";
    }
// </editor-fold>
}