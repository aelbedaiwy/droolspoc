/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll.formula;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Transient;

/**
 *
 * @author nkhalil
 */
@Entity
public class FormulaName extends BaseEntity {
// <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false,unique=true)
    @Translatable(translationField="nameTranslated")
    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getNameTranslatedDD(){

      return "FormulaName_nameTranslated";
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
}
