
package com.unitedofoq.otms.recruitment.vacancy;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class VacancyTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

}
