/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import com.unitedofoq.otms.training.CostItemBase;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"providerActivity"})
@DiscriminatorValue("PROVIDERACTIVITY")
public class ProviderActivityCostItem extends CostItemBase {
    // <editor-fold defaultstate="collapsed" desc="providerActivity">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private ProviderActivity providerActivity;

    public ProviderActivity getProviderActivity() {
        return providerActivity;
    }

    public void setProviderActivity(ProviderActivity providerActivity) {
        this.providerActivity = providerActivity;
    }

    public String getProviderActivityDD() {
        return "ProviderActivityCostItem_providerActivity";
    }
    // </editor-fold>
}