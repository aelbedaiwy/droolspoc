/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.budget.BudgetElement;
import com.unitedofoq.otms.foundation.currency.Currency;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ITEMTYPE")
public class CostItemBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="trainingCostItem">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TrainingCostItem trainingCostItem;

    public TrainingCostItem getTrainingCostItem() {
        return trainingCostItem;
    }

    public void setTrainingCostItem(TrainingCostItem trainingCostItem) {
        this.trainingCostItem = trainingCostItem;
    }

    public String getTrainingCostItemDD() {
        return "CostItemBase_trainingCostItem";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cost">
    @Column( nullable=false ,precision=25, scale=13)

    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCostDD() {
        return "CostItemBase_cost";
    }
    @Transient
    private BigDecimal costMask;

    public BigDecimal getCostMask() {
        costMask = cost ;
        return costMask;
    }

    public void setCostMask(BigDecimal costMask) {
        updateDecimalValue("cost", costMask);
    }

    public String getCostMaskDD() {
        return "CostItemBase_costMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Currency currency;
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getCurrencyDD() {
        return "CostItemBase_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private  BudgetElement budgetElement;
    public BudgetElement getBudgetElement() {
        return budgetElement;
    }
    public void setBudgetElement(BudgetElement budgetElement) {
        this.budgetElement = budgetElement;
    }
    public String getBudgetElementDD() {
        return "CostItemBase_budgetElement";
    }
    // </editor-fold>
}