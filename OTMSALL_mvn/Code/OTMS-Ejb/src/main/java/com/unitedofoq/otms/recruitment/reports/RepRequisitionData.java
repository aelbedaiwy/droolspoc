
package com.unitedofoq.otms.recruitment.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepRequisitionData extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "RepRequisitionData_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "RepRequisitionData_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requiredNumber">
    @Column
    private int requiredNumber;

    public void setRequiredNumber(int requiredNumber) {
        this.requiredNumber = requiredNumber;
    }

    public int getRequiredNumber() {
        return requiredNumber;
    }

    public String getRequiredNumberDD() {
        return "RepRequisitionData_requiredNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ageFrom">
    @Column
    private BigDecimal ageFrom;

    public void setAgeFrom(BigDecimal ageFrom) {
        this.ageFrom = ageFrom;
    }

    public BigDecimal getAgeFrom() {
        return ageFrom;
    }

    public String getAgeFromDD() {
        return "RepRequisitionData_ageFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ageTo">
    @Column
    private BigDecimal ageTo;

    public void setAgeTo(BigDecimal ageTo) {
        this.ageTo = ageTo;
    }

    public BigDecimal getAgeTo() {
        return ageTo;
    }

    public String getAgeToDD() {
        return "RepRequisitionData_ageTo";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="expectedSalary">
    @Column
    private BigDecimal expectedSalary;

    public void setExpectedSalary(BigDecimal expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public BigDecimal getExpectedSalary() {
        return expectedSalary;
    }

    public String getExpectedSalaryDD() {
        return "RepRequisitionData_expectedSalary";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public String getCommentsDD() {
        return "RepRequisitionData_comments";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="shiftBased">
    @Column
    private String shiftBased;

    public void setShiftBased(String shiftBased) {
        this.shiftBased = shiftBased;
    }

    public String getShiftBased() {
        return shiftBased;
    }

    public String getShiftBasedDD() {
        return "RepRequisitionData_shiftBased";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="experienceYears">
    @Column
    private BigDecimal experienceYears;

    public void setExperienceYears(BigDecimal experienceYears) {
        this.experienceYears = experienceYears;
    }

    public BigDecimal getExperienceYears() {
        return experienceYears;
    }

    public String getExperienceYearsDD() {
        return "RepRequisitionData_experienceYears";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="expectedStartDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expectedStartDate;

    public void setExpectedStartDate(Date expectedStartDate) {
        this.expectedStartDate = expectedStartDate;
    }

    public Date getExpectedStartDate() {
        return expectedStartDate;
    }

    public String getExpectedStartDateDD() {
        return "RepRequisitionData_expectedStartDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="certificate1">
    @Column
    private String certificate1;

    public void setCertificate1(String certificate1) {
        this.certificate1 = certificate1;
    }

    public String getCertificate1() {
        return certificate1;
    }

    public String getCertificate1DD() {
        return "RepRequisitionData_certificate1";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="certificate2">
    @Column
    private String certificate2;

    public void setCertificate2(String certificate2) {
        this.certificate2 = certificate2;
    }

    public String getCertificate2() {
        return certificate2;
    }

    public String getCertificate2DD() {
        return "RepRequisitionData_certificate2";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="gender">
    @Column
    private String gender;

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getGenderDD() {
        return "RepRequisitionData_gender";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="location">
    @Column
    private String location;

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "RepRequisitionData_location";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="type">
    @Column
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getTypeDD() {
        return "RepRequisitionData_type";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="educationCategory">
    @Column
    private String educationCategory;

    public void setEducationCategory(String educationCategory) {
        this.educationCategory = educationCategory;
    }

    public String getEducationCategory() {
        return educationCategory;
    }

    public String getEducationCategoryDD() {
        return "RepRequisitionData_educationCategory";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="grade">
    @Column
    private String grade;

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public String getGradeDD() {
        return "RepRequisitionData_grade";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="job">
    @Column
    private String job;

    public void setJob(String job) {
        this.job = job;
    }

    public String getJob() {
        return job;
    }

    public String getJobDD() {
        return "RepRequisitionData_job";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="position">
    @Column
    private String position;

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public String getPositionDD() {
        return "RepRequisitionData_position";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getCompanyIDDD() {
        return "RepRequisitionData_companyID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepRequisitionData_dsDbid";
    }
    // </editor-fold>
}
