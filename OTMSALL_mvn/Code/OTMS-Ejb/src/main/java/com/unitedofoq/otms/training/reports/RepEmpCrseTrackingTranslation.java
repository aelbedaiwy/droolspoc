package com.unitedofoq.otms.training.reports;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class RepEmpCrseTrackingTranslation extends RepEmpCourseTranslationBase {

    // <editor-fold defaultstate="collapsed" desc="status">
    @Column
    private String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="doneBy">
    @Column
    private String doneBy;

    public void setDoneBy(String doneBy) {
        this.doneBy = doneBy;
    }

    public String getDoneBy() {
        return doneBy;
    }
    // </editor-fold>
}
