/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author arezk
 */
@Entity
@ReadOnly
public class RepEmployeeLoanHistory extends RepEmployeeBase {

    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    private String dsDbid;

    public String getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(String dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeLoanHistory_genderDescription";
    }

    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }

    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeLoanHistory_genderDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeactive">
    @Translatable(translationField = "employeeactiveTranslated")
    private String employeeactive;

    public String getEmployeeactive() {
        return employeeactive;
    }

    public void setEmployeeactive(String employeeactive) {
        this.employeeactive = employeeactive;
    }

    public String getEmployeeactiveDD() {
        return "RepEmployeeLoanHistory_employeeactive";
    }

    @Transient
    @Translation(originalField = "employeeactive")
    private String employeeactiveTranslated;

    public String getEmployeeactiveTranslated() {
        return employeeactiveTranslated;
    }

    public void setEmployeeactiveTranslated(String employeeactiveTranslated) {
        this.employeeactiveTranslated = employeeactiveTranslated;
    }

    public String getEmployeeactiveTranslatedDD() {
        return "RepEmployeeLoanHistory_employeeactive";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanDescription">
    @Translatable(translationField = "loanDescriptionTranslated")
    private String loanDescription;

    public String getLoanDescription() {
        return loanDescription;
    }

    public void setLoanDescription(String loanDescription) {
        this.loanDescription = loanDescription;
    }

    public String getLoanDescriptionDD() {
        return "RepEmployeeLoanHistory_loanDescription";
    }

    @Transient
    @Translation(originalField = "loanDescription")
    private String loanDescriptionTranslated;

    public String getLoanDescriptionTranslated() {
        return loanDescriptionTranslated;
    }

    public void setLoanDescriptionTranslated(String loanDescriptionTranslated) {
        this.loanDescriptionTranslated = loanDescriptionTranslated;
    }

    public String getLoanDescriptionTranslatedDD() {
        return "RepEmployeeLoanHistory_loanDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="installmentNumber">
    @Column(precision = 25, scale = 13)
    private BigDecimal installmentNumber;

    public BigDecimal getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(BigDecimal installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public String getInstallmentNumberDD() {
        return "RepEmployeeLoanHistory_installmentNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="installmentNumberEnc">

    @Column
    private String installmentNumberEnc;

    public String getInstallmentNumberEnc() {
        return installmentNumberEnc;
    }

    public void setInstallmentNumberEnc(String installmentNumberEnc) {
        this.installmentNumberEnc = installmentNumberEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal monthValue;

    public BigDecimal getMonthValue() {
        return monthValue;
    }

    public void setMonthValue(BigDecimal monthValue) {
        this.monthValue = monthValue;
    }

    public String getMonthValueDD() {
        return "RepEmployeeLoanHistory_monthValue";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthValueEnc">

    @Column
    private String monthValueEnc;

    public String getMonthValueEnc() {
        return monthValueEnc;
    }

    public void setMonthValueEnc(String monthValueEnc) {
        this.monthValueEnc = monthValueEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deduction">
    private String deduction;

    public String getDeduction() {
        return deduction;
    }

    public void setDeduction(String deduction) {
        this.deduction = deduction;
    }

    public String getDeductedValueDD() {
        return "RepEmployeeLoanHistory_deduction";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deductionDescription">
    @Translatable(translationField = "deductionDescriptionTranslated")
    private String deductionDescription;

    public String getDeductionDescription() {
        return deductionDescription;
    }

    public void setDeductionDescription(String deductionDescription) {
        this.deductionDescription = deductionDescription;
    }

    public String getDeductionDescriptionDD() {
        return "RepEmployeeLoanHistory_deductionDescription";
    }

    @Transient
    @Translation(originalField = "deductionDescription")
    private String deductionDescriptionTranslated;

    public String getDeductionDescriptionTranslated() {
        return deductionDescriptionTranslated;
    }

    public void setDeductionDescriptionTranslated(String deductionDescriptionTranslated) {
        this.deductionDescriptionTranslated = deductionDescriptionTranslated;
    }

    public String getDeductionDescriptionTranslatedDD() {
        return "RepEmployeeLoanHistory_deductionDescription";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriodDesc">
    @Translatable(translationField = "calculatedPeriodDescTranslated")
    private String calculatedPeriodDesc;

    public String getCalculatedPeriodDesc() {
        return calculatedPeriodDesc;
    }

    public void setCalculatedPeriodDesc(String calculatedPeriodDesc) {
        this.calculatedPeriodDesc = calculatedPeriodDesc;
    }

    public String getCalculatedPeriodDescDD() {
        return "RepEmployeeLoanHistory_calculatedPeriodDesc";
    }

    @Transient
    @Translation(originalField = "calculatedPeriodDesc")
    private String calculatedPeriodDescTranslated;

    public String getCalculatedPeriodDescTranslated() {
        return calculatedPeriodDescTranslated;
    }

    public void setCalculatedPeriodDescTranslated(String calculatedPeriodDescTranslated) {
        this.calculatedPeriodDescTranslated = calculatedPeriodDescTranslated;
    }

    public String getCalculatedPeriodDescTranslatedDD() {
        return "RepEmployeeLoanHistory_calculatedPeriodDesc";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriodID">
    private String calculatedPeriodID;

    public String getCalculatedPeriodID() {
        return calculatedPeriodID;
    }

    public void setCalculatedPeriodID(String calculatedPeriodID) {
        this.calculatedPeriodID = calculatedPeriodID;
    }

    public String getCalculatedPeriodIDDD() {
        return "RepEmployeeLoanHistory_calculatedPeriodID";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="amount">
    @Column(precision = 25, scale = 13)
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAmountDD() {
        return "RepEmployeeLoanHistory_amount";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal paidAmount;

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaidAmountDD() {
        return "RepEmployeeLoanHistory_paidAmount";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidAmountEnc">
    @Column

    private String paidAmountEnc;

    public String getPaidAmountEnc() {
        return paidAmountEnc;
    }

    public void setPaidAmountEnc(String paidAmountEnc) {
        this.paidAmountEnc = paidAmountEnc;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidInstallment">
    private BigDecimal paidInstallment;

    public BigDecimal getPaidInstallment() {
        return paidInstallment;
    }

    public void setPaidInstallment(BigDecimal paidInstallment) {
        this.paidInstallment = paidInstallment;
    }

    public String getPaidInstallmentDD() {
        return "RepEmployeeLoanHistory_paidInstallment";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidInstallmentEnc">
    @Column

    private String paidInstallmentEnc;

    public String getPaidInstallmentEnc() {
        return paidInstallmentEnc;
    }

    public void setPaidInstallmentEnc(String paidInstallmentEnc) {
        this.paidInstallmentEnc = paidInstallmentEnc;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanStatus">
    private String loanStatus;

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getLoanStatusDD() {
        return "RepEmployeeLoanHistory_loanStatus";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="transactionType">
    private String transactionType;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionTypeDD() {
        return "RepEmployeeLoanHistory_transactionType";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal loanValue;

    public BigDecimal getLoanValue() {
        return loanValue;
    }

    public void setLoanValue(BigDecimal loanValue) {
        this.loanValue = loanValue;
    }

    public String getLoanValueDD() {
        return "RepEmployeeLoanHistory_loanValue";
    }
    @Transient
    private BigDecimal loanValueMask;

    public BigDecimal getLoanValueMask() {
        loanValueMask = loanValue;
        return loanValueMask;
    }

    public void setLoanValueMask(BigDecimal loanValueMask) {
        updateDecimalValue("loanValue", loanValueMask);
    }

    public String getLoanValueMaskDD() {
        return "RepEmployeeLoanHistory_loanValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="loanValueEnc">
    @Column

    private String loanValueEnc;

    public String getLoanValueEnc() {
        return loanValueEnc;
    }

    public void setLoanValueEnc(String loanValueEnc) {
        this.loanValueEnc = loanValueEnc;
    }

    // </editor-fold >
}
