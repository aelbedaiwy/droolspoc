package com.unitedofoq.otms.recruitment.onlineexam;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class QuestionBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="exam">
    @ManyToOne(fetch = FetchType.LAZY, optional=false)
    private Exam exam;

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public String getExamDD() {
        return "QuestionBase_exam";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="questionStatement">
    private String questionStatement;

    public String getQuestionStatement() {
        return questionStatement;
    }

    public void setQuestionStatement(String questionStatement) {
        this.questionStatement = questionStatement;
    }

    public String getQuestionStatementDD() {
        return "QuestionBase_questionStatement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="serialNumber">
    private int serialNumber;

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public String getSerialNumberDD() {
        return "QuestionBase_serialNumber";
    }
    // </editor-fold>
}
