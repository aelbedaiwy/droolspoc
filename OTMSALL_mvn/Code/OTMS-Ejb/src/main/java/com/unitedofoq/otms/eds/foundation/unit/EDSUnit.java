/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.unit;

//import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import com.unitedofoq.otms.eds.foundation.job.EDSJob;
import com.unitedofoq.otms.foundation.unit.UnitBase;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author arezk
 */
@Entity
@Table(name="Unit")
@ParentEntity(fields={"company"})
@DiscriminatorValue("MASTER")
public class EDSUnit extends UnitBase{


    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSCompany company;

    public EDSCompany getCompany() {
        return company;
    }

    public void setCompany(EDSCompany company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "EDSUnit_company";
    }

    // <editor-fold defaultstate="collapsed" desc="childUnits">
    @OneToMany(mappedBy="parentUnit")
	private List<EDSUnit> childUnits;

    public List<EDSUnit> getChildUnits() {
        return childUnits;
    }

    public void setChildUnits(List<EDSUnit> childUnits) {
        this.childUnits = childUnits;
    }
    public String getChildUnitsDD() {
        return "Unit_childUnits";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="parentUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private EDSUnit parentUnit;

    public EDSUnit getParentUnit() {
        return parentUnit;
    }

    public void setParentUnit(EDSUnit parentUnit) {
        this.parentUnit = parentUnit;
    }
    public String getParentUnitDD() {  return "Unit_parentUnit";  }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positions">
    @OneToMany(mappedBy="unit")
	private List<EDSJob> edsJobs;

    public List getEdsJobs() {
        return edsJobs;
    }

    public void setEdsJobs(List edsJobs) {
        this.edsJobs = edsJobs;
    }
    

    public String getEdsJobsDD() {
        return "Unit_edsJobs";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitLevel">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSUnitLevel unitLevel;

    public EDSUnitLevel getUnitLevel() {
        return unitLevel;
    }

    public void setUnitLevel(EDSUnitLevel unitLevel) {
        this.unitLevel = unitLevel;
    }

   public String getUnitLevelDD() {
        return "Unit_unitLevel";
    }
    // </editor-fold>
	
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSUnitLevelIndex unitLevelindex;

    public EDSUnitLevelIndex getUnitLevelindex() {
        return unitLevelindex;
    }

    public String getUnitLevelindexDD() {
        return "EDSUnit_unitLevelindex";
    }

    public void setUnitLevelindex(EDSUnitLevelIndex unitLevelindex) {
        this.unitLevelindex = unitLevelindex;
    }
}
