package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.otms.eds.foundation.employee.idp.EDSIDP;
import com.unitedofoq.otms.eds.foundation.job.EDSJob;
import com.unitedofoq.otms.foundation.employee.EmployeeBase;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@Table(name="OEmployee")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="ETYPE1")
@DiscriminatorValue(value = "MASTER")
@ChildEntity(fields={"values", "behaviors", "learningStyles",
"idps","personalInfo","employeeProfiler","employeeUser"})
@FABSEntitySpecs(hideInheritedFields={"position"})
public class EDSEmployee  extends EmployeeBase {
    @OneToMany(mappedBy = "assess")
    private List<EmployeeProfilerAssessorEmployee> employeeProfilerAssessorEmployees;

    public List<EmployeeProfilerAssessorEmployee> getEmployeeProfilerAssessorEmployees() {
        return employeeProfilerAssessorEmployees;
    }

    public void setEmployeeProfilerAssessorEmployees(List<EmployeeProfilerAssessorEmployee> employeeProfilerAssessorEmployees) {
        this.employeeProfilerAssessorEmployees = employeeProfilerAssessorEmployees;
    }
    
    @OneToMany(mappedBy="employee")
    private List<EmployeeProfiler> employeeProfiler;

    @Transient
    private EDSEmployee directManager;


    public void setDirectManager(EDSEmployee directManager) {
        this.directManager = directManager;
    }


     public String getEmployeeProfilerDD() {
        return "EDSEmployee_employeeProfiler";
    }

    public List<EmployeeProfiler> getEmployeeProfiler() {
        return employeeProfiler;
    }

    public void setEmployeeProfiler(List<EmployeeProfiler> employeeProfiler) {
        if (latestProfiler == null) {
            if (employeeProfiler != null && employeeProfiler.size() != 0) {
                latestProfiler = employeeProfiler.get(0);
                for (EmployeeProfiler profiler : employeeProfiler) {
                    if (profiler.getCreationDate().after(latestProfiler.getCreationDate()))
                        latestProfiler = profiler;
                }
            }
        }

        this.employeeProfiler = employeeProfiler;
    }

    @OneToMany(mappedBy="employee")
    private List<EDSEmployeeValue> values;
    @OneToMany(mappedBy="employee")
    private List<EDSEmployeeBehavior> behaviors;    
    @OneToMany(mappedBy="employee")
    private List<EDSEmployeeLearningStyle> learningStyles;
    @OneToMany(mappedBy="employee")
    private List<EDSIDP> idps;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="position_dbid")
    EDSJob job;
    public EDSJob getJob() {
        return job;
    }
    public void setJob(EDSJob job) {
        this.job = job;
    }

    @Transient
    private EmployeeProfiler latestProfiler;
    public EmployeeProfiler getLatestProfiler() {
        if (latestProfiler == null) {
            if (employeeProfiler != null && employeeProfiler.size() != 0) {
                latestProfiler = employeeProfiler.get(0);
                for (EmployeeProfiler profiler : employeeProfiler) {
                    if (profiler.getCreationDate().after(latestProfiler.getCreationDate()))
                        latestProfiler = profiler;
                }
            }
        }

        return latestProfiler;
    }
    public String getLatestProfilerDD() {
        return "EDSEmployee_latestProfiler";
    }
    public void setLatestProfiler(EmployeeProfiler latestProfiler) {
        this.latestProfiler = latestProfiler;
    }


    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)    
    private EDSEmployee firstLevelManager;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSEmployee secondLevelManager;

    public EDSEmployee getFirstLevelManager() {
        return firstLevelManager;
    }

    public void setFirstLevelManager(EDSEmployee firstLevelManager) {
        this.firstLevelManager = firstLevelManager;
    }

    public EDSEmployee getSecondLevelManager() {
        return secondLevelManager;
    }

    public void setSecondLevelManager(EDSEmployee secondLevelManager) {
        this.secondLevelManager = secondLevelManager;
    }
    
    
    public String getJobDD()                  {   return "EDSEmployee_job"; }
    public String getDepartmentDD()           {   return "EDSEmployee_department";  }
    public String getSectorDD()               {   return "EDSEmployee_sector";  }
    public String getSectionDD()              {   return "EDSEmployee_section";  }
    public String getFirstLevelManagerDD()    {   return "EDSEmployee_firstLevelManager";  }
    public String getSecondLevelManagerDD()   {   return "EDSEmployee_secondLevelManager";  }
   

    public List<EDSIDP> getIdps() {
        return idps;
    }

    public void setIdps(List<EDSIDP> idps) {
        this.idps = idps;
    }

    public List<EDSEmployeeLearningStyle> getLearningStyles() {
        return learningStyles;
    }

    public void setLearningStyles(List<EDSEmployeeLearningStyle> learningStyles) {
        this.learningStyles = learningStyles;
    }

    public List<EDSEmployeeBehavior> getBehaviors() {
        return behaviors;
    }

    public void setBehaviors(List<EDSEmployeeBehavior> behaviors) {
        this.behaviors = behaviors;
    }    

    public List<EDSEmployeeValue> getValues() {
        return values;
    }

    public void setValues(List<EDSEmployeeValue> values) {
        this.values = values;
    }

    public EDSEmployee getDirectManager() {
        if(getJob()!=null &&  getJob().getParentJob()!=null && getJob().getParentJob().getEmployees()!=null){
            for (EDSEmployee emp : getJob().getParentJob().getEmployees()) {
                if(emp.isInActive()){
                    return  emp;
                }
            }
        }
        return  null;
    }

    // <editor-fold defaultstate="collapsed" desc="personalInfo">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade={CascadeType.ALL}, mappedBy="employee")
    @JoinColumn(nullable=false)
    private EDSEmployeePerson personalInfo;
    public String getPersonalInfoDD() {
        return "Employee_personalInfo";
    }
    public EDSEmployeePerson getPersonalInfo() {
        return personalInfo;
    }
    public void setPersonalInfo(EDSEmployeePerson personalInfo) {
        this.personalInfo = personalInfo;
    }
    // </editor-fold>
    @Override
    public void PrePersist() {
        super.PrePersist();
        if (personalInfo != null)
            if (personalInfo.getEmployee() == null)
                personalInfo.setEmployee(this);
    }

    // <editor-fold defaultstate="collapsed" desc="employeeUser">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy="employee")
    @JoinColumn
    private EDSEmployeeUser employeeUser;
    public EDSEmployeeUser getEmployeeUser() {
        return employeeUser;
    }
    public void setEmployeeUser(EDSEmployeeUser employeeUser) {
        this.employeeUser = employeeUser;
    }
    public String getEmployeeUserDD() {
        return "Employee_employeeUser" ;
    }
    // </editor-fold>
    
}