/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.LegalEntity;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dev
 */
@Entity
@Table(name = "TM_Multientry_import")
public class MultiEntryImport extends BaseEntity {

    Employee employee;
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;
    private String entryTime;
    private String type;
    private boolean posted;
    private int maxSerial;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    LegalEntity legalEntity;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "MultiEntryImport_employee";
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryDateDD() {
        return "MultiEntryImport_entryDate";
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    public String getEntryTimeDD() {
        return "MultiEntryImport_entryTime";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "MultiEntryImport_type";
    }

    public boolean isPosted() {
        return posted;
    }

    public void setPosted(boolean posted) {
        this.posted = posted;
    }

    public String getPostedDD() {
        return "MultiEntryImport_posted";
    }

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public String getLegalEntityDD() {
        return "MultiEntryImport_legalEntity";
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public int getMaxSerial() {
        return maxSerial;
    }

    public void setMaxSerial(int maxSerial) {
        this.maxSerial = maxSerial;
    }

    public String getMaxSerialDD() {
        return "MultiEntryImport_maxSerial";
    }
}
