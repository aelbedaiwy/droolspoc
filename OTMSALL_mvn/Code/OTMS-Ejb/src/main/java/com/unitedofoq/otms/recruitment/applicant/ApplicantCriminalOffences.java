/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author aibrahim
 */
@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantCriminalOffences extends BusinessObjectBaseEntity {
// <editor-fold defaultstate="collapsed" desc="Applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "ApplicantCriminalOffences_applicant";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "ApplicantCriminalOffences_description";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @com.unitedofoq.fabs.core.i18n.Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
     // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="dateFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    public Date getDateFrom() {
        return dateFrom;
    }
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }
    public String getDateFromDD() {
        return "ApplicantCriminalOffences_dateFrom";
    }
 // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="dateTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    public Date getDateTo() {
        return dateTo;
    }
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
    public String getDateToDD() {
        return "ApplicantCriminalOffences_dateTo";
    }
 // </editor-fold>
}
