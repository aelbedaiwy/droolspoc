package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name="repPositionVacancy")
public class RepPositionVacancy extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="headCount">
    @Column
    private Integer headCount;

    public Integer getHeadCount() {
        return headCount;
    }

    public void setHeadCount(Integer headCount) {
        this.headCount = headCount;
    }

    public String getHeadCountDD() {
        return "RepPositionVacancy_headCount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="positionOccupation">
    @Column
    private int positionOccupation;

    public int getPositionOccupation() {
        return positionOccupation;
    }

    public void setPositionOccupation(int positionOccupation) {
        this.positionOccupation = positionOccupation;
    }

    public String getPositionOccupationDD() {
        return "RepPositionVacancy_positionOccupation";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="vacancy">
    @Column
    private int vacancy;

    public int getVacancy() {
        return vacancy;
    }

    public void setVacancy(int vacancy) {
        this.vacancy = vacancy;
    }

    public String getVacancyDD() {
        return "RepPositionVacancy_vacancy";
    }
    //</editor-fold>
}
