package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployee extends RepEmployeeBase {

    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private Long dsDbid;

    public Long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(Long dsDbid) {
        this.dsDbid = dsDbid;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="phone1">
    @Column
    private String phone1;

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone1DD() {
        return "RepEmployee_phone1";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mobileNumber1">
    @Column
    private String mobileNumber1;

    public String getMobileNumber1() {
        return mobileNumber1;
    }

    public void setMobileNumber1(String mobileNumber1) {
        this.mobileNumber1 = mobileNumber1;
    }

    public String getMobileNumber1DD() {
        return "RepEmployee_mobileNumber1";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="address1">
    @Translatable(translationField = "address1Translated")
    @Column
    private String address1;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1DD() {
        return "RepEmployee_address1";
    }

    @Transient
    @Translation(originalField = "address1")
    private String address1Translated;

    public String getAddress1Translated() {
        return address1Translated;
    }

    public void setAddress1Translated(String address1Translated) {
        this.address1Translated = address1Translated;
    }

    public String getAddress1TranslatedDD() {
        return "RepEmployee_address1";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="religionDescription">
    @Translatable(translationField = "religionDescriptionTranslated")
    @Column
    private String religionDescription;

    public String getReligionDescription() {
        return religionDescription;
    }

    public void setReligionDescription(String religionDescription) {
        this.religionDescription = religionDescription;
    }

    public String getReligionDescriptionDD() {
        return "RepEmployee_religionDescription";
    }

    @Transient
    @Translation(originalField = "religionDescription")
    private String religionDescriptionTranslated;

    public String getReligionDescriptionTranslated() {
        return religionDescriptionTranslated;
    }

    public void setReligionDescriptionTranslated(String religionDescriptionTranslated) {
        this.religionDescriptionTranslated = religionDescriptionTranslated;
    }

    public String getReligionDescriptionTranslatedDD() {
        return "RepEmployee_religionDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="address2">
    @Translatable(translationField = "address2Translated")
    @Column
    private String address2;

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress2DD() {
        return "RepEmployee_address2";
    }

    @Transient
    @Translation(originalField = "address2")
    private String address2Translated;

    public String getAddress2Translated() {
        return address2Translated;
    }

    public void setAddress2Translated(String address2Translated) {
        this.address2Translated = address2Translated;
    }

    public String getAddress2TranslatedDD() {
        return "RepEmployee_address2";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="birthDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDateDD() {
        return "RepEmployee_birthDate";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="email2">
    @Column
    private String email2;

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getEmail2DD() {
        return "RepEmployee_email2";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mobileNumber2">
    @Column
    private String mobileNumber2;

    public String getMobileNumber2() {
        return mobileNumber2;
    }

    public void setMobileNumber2(String mobileNumber2) {
        this.mobileNumber2 = mobileNumber2;
    }

    public String getMobileNumber2DD() {
        return "RepEmployee_mobileNumber2";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="email1">
    @Column
    private String email1;

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail1DD() {
        return "RepEmployee_email1";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="additionalInformation">
    @Column
    private String additionalInformation;

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getAdditionalInformationDD() {
        return "RepEmployee_additionalInformation";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="phone2">
    @Column
    private String phone2;

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone2DD() {
        return "RepEmployee_phone2";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="countryDescription">
    @Translatable(translationField = "countryDescriptionTranslated")
    @Column
    private String countryDescription;

    public String getCountryDescription() {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    public String getCountryDescriptionDD() {
        return "RepEmployee_countryDescription";
    }

    @Transient
    @Translation(originalField = "countryDescription")
    private String countryDescriptionTranslated;

    public String getCountryDescriptionTranslated() {
        return countryDescriptionTranslated;
    }

    public void setCountryDescriptionTranslated(String countryDescriptionTranslated) {
        this.countryDescriptionTranslated = countryDescriptionTranslated;
    }

    public String getCountryDescriptionTranslatedDD() {
        return "RepEmployee_countryDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="maritalStatusDescription">
    @Column
    @Translatable(translationField = "maritalStatusDescriptionTranslation")
    private String maritalStatusDescription;

    public String getMaritalStatusDescription() {
        return maritalStatusDescription;
    }

    public void setMaritalStatusDescription(String maritalStatusDescription) {
        this.maritalStatusDescription = maritalStatusDescription;
    }

    public String getMaritalStatusDescriptionDD() {
        return "RepEmployee_maritalStatusDescription";
    }

    @Transient
    @Translation(originalField = "maritalStatusDescription")
    private String maritalStatusDescriptionTranslation;

    public String getMaritalStatusDescriptionTranslation() {
        return maritalStatusDescriptionTranslation;
    }

    public void setMaritalStatusDescriptionTranslation(String maritalStatusDescriptionTranslation) {
        this.maritalStatusDescriptionTranslation = maritalStatusDescriptionTranslation;
    }

    public String getMaritalStatusDescriptionTranslationDD() {
        return "RepEmployee_maritalStatusDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="cityDescription">
    @Column
    @Translatable(translationField = "cityDescriptionTranslated")
    private String cityDescription;

    public String getCityDescription() {
        return cityDescription;
    }

    public void setCityDescription(String cityDescription) {
        this.cityDescription = cityDescription;
    }

    public String getCityDescriptionDD() {
        return "RepEmployee_cityDescription";
    }

    @Transient
    @Translation(originalField = "cityDescription")
    private String cityDescriptionTranslated;

    public String getCityDescriptionTranslated() {
        return cityDescriptionTranslated;
    }

    public void setCityDescriptionTranslated(String cityDescriptionTranslated) {
        this.cityDescriptionTranslated = cityDescriptionTranslated;
    }

    public String getCityDescriptionTranslatedDD() {
        return "RepEmployee_cityDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="militaryStatusDescription">
    @Column
    @Translatable(translationField = "militaryStatusDescriptionTranslated")
    private String militaryStatusDescription;

    public String getMilitaryStatusDescription() {
        return militaryStatusDescription;
    }

    public void setMilitaryStatusDescription(String militaryStatusDescription) {
        this.militaryStatusDescription = militaryStatusDescription;
    }

    public String getMilitaryStatusDescriptionDD() {
        return "RepEmployee_militaryStatusDescription";
    }

    @Transient
    @Translation(originalField = "militaryStatusDescription")
    private String militaryStatusDescriptionTranslated;

    public String getMilitaryStatusDescriptionTranslated() {
        return militaryStatusDescriptionTranslated;
    }

    public void setMilitaryStatusDescriptionTranslated(String militaryStatusDescriptionTranslated) {
        this.militaryStatusDescriptionTranslated = militaryStatusDescriptionTranslated;
    }

    public String getMilitaryStatusDescriptionTranslatedDD() {
        return "RepEmployee_militaryStatusDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployee_genderDescription";
    }

    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }

    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployee_genderDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="nationaltyDescription">
    @Column
    @Translatable(translationField = "nationaltyDescriptionTranslated")
    private String nationaltyDescription;

    public String getNationaltyDescription() {
        return nationaltyDescription;
    }

    public void setNationaltyDescription(String nationaltyDescription) {
        this.nationaltyDescription = nationaltyDescription;
    }

    public String getNationaltyDescriptionDD() {
        return "RepEmployee_nationaltyDescription";
    }

    @Transient
    @Translation(originalField = "nationaltyDescription")
    private String nationaltyDescriptionTranslated;

    public String getNationaltyDescriptionTranslated() {
        return nationaltyDescriptionTranslated;
    }

    public void setNationaltyDescriptionTranslated(String nationaltyDescriptionTranslated) {
        this.nationaltyDescriptionTranslated = nationaltyDescriptionTranslated;
    }

    public String getNationaltyDescriptionTranslatedDD() {
        return "RepEmployee_nationaltyDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String getEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String getEmployeeActiveDD() {
        return "RepEmployee_employeeActive";
    }

    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }

    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployee_employeeActive";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="cashOrBank">
    @Column(length = 1)
    private String cashOrBank;

    public String getCashOrBank() {
        return cashOrBank;
    }

    public void setCashOrBank(String cashOrBank) {
        this.cashOrBank = cashOrBank;
    }

    public String getCashOrBankDD() {
        return "RepEmployee_cashOrBank";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="overtimeEntitled">
    @Column(length = 1)
    private String overtimeEntitled;

    public String getOvertimeEntitled() {
        return overtimeEntitled;
    }

    public void setOvertimeEntitled(String overtimeEntitled) {
        this.overtimeEntitled = overtimeEntitled;
    }

    public String getOvertimeEntitledDD() {
        return "RepEmployee_overtimeEntitled";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentSalary">
    @Column
    private BigDecimal currentSalary;

    public BigDecimal getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(BigDecimal currentSalary) {
        this.currentSalary = currentSalary;
    }

    public String getCurrentSalaryDD() {
        return "RepEmployee_currentSalary";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="currentSalaryEnc">
    @Column
    private String currentSalaryEnc;

    public String getCurrentSalaryEnc() {
        return currentSalaryEnc;
    }

    public void setCurrentSalaryEnc(String currentSalaryEnc) {
        this.currentSalaryEnc = currentSalaryEnc;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="bankCode">
    @Column
    private String bankCode;

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankCodeDD() {
        return "RepEmployee_bankCode";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="accountNo">
    @Column
    private String accountNo;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountNoDD() {
        return "RepEmployee_accountNo";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="allowanceEntitled">
    @Column(length = 1)
    private String allowanceEntitled;

    public String getAllowanceEntitled() {
        return allowanceEntitled;
    }

    public void setAllowanceEntitled(String allowanceEntitled) {
        this.allowanceEntitled = allowanceEntitled;
    }

    public String getAllowanceEntitledDD() {
        return "RepEmployee_allowanceEntitled";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="accrualDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date accrualDate;

    public Date getAccrualDate() {
        return accrualDate;
    }

    public void setAccrualDate(Date accrualDate) {
        this.accrualDate = accrualDate;
    }

    public String getAccrualDateDD() {
        return "RepEmployee_accrualDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "RepEmployee_startDate";
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="variableBasis">
    @Column //(precision=25, scale=2)
    private BigDecimal variableBasis;

    public BigDecimal getVariableBasis() {
        return variableBasis;
    }

    public void setVariableBasis(BigDecimal variableBasis) {
        this.variableBasis = variableBasis;
    }

    public String getVariableBasisDD() {
        return "RepEmployee_variableBasis";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="variableBasisEnc">
    @Column
    private String variableBasisEnc;

    public String getVariableBasisEnc() {
        return variableBasisEnc;
    }

    public void setVariableBasisEnc(String variableBasisEnc) {
        this.variableBasisEnc = variableBasisEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="basicBasis">
    @Column //(precision=25, scale=13)
    private BigDecimal basicBasis;

    public BigDecimal getBasicBasis() {
        return basicBasis;
    }

    public void setBasicBasis(BigDecimal basicBasis) {
        this.basicBasis = basicBasis;
    }

    public String getBasicBasisDD() {
        return "RepEmployee_basicBasis";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="basicBasisEnc">
    private String basicBasisEnc;

    public String getBasicBasisEnc() {
        return basicBasisEnc;
    }

    public void setBasicBasisEnc(String basicBasisEnc) {
        this.basicBasisEnc = basicBasisEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    @Column
    @Translatable(translationField = "costCenterGroupDescriptionTranslated")
    private String costCenterGroupDescription;

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }

    public String getCostCenterGroupDescriptionDD() {
        return "RepEmployee_costCenterGroupDescription";
    }

    @Transient
    @Translation(originalField = "costCenterGroupDescription")
    private String costCenterGroupDescriptionTranslated;

    public String getCostCenterGroupDescriptionTranslated() {
        return costCenterGroupDescriptionTranslated;
    }

    public void setCostCenterGroupDescriptionTranslated(String costCenterGroupDescriptionTranslated) {
        this.costCenterGroupDescriptionTranslated = costCenterGroupDescriptionTranslated;
    }

    public String getCostCenterGroupDescriptionTranslatedDD() {
        return "RepEmployee_costCenterGroupDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="legalEntityDescription">
    @Column
    @Translatable(translationField = "legalEntityDescriptionTranslated")
    private String legalEntityDescription;

    public String getLegalEntityDescription() {
        return legalEntityDescription;
    }

    public void setLegalEntityDescription(String legalEntityDescription) {
        this.legalEntityDescription = legalEntityDescription;
    }

    public String getLegalEntityDescriptionDD() {
        return "RepEmployee_legalEntityDescription";
    }

    @Transient
    @Translation(originalField = "legalEntityDescription")
    private String legalEntityDescriptionTranslated;

    public String getLegalEntityDescriptionTranslated() {
        return legalEntityDescriptionTranslated;
    }

    public void setLegalEntityDescriptionTranslated(String legalEntityDescriptionTranslated) {
        this.legalEntityDescriptionTranslated = legalEntityDescriptionTranslated;
    }

    public String getLegalEntityDescriptionTranslatedDD() {
        return "RepEmployee_legalEntityDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="payGradeStepDescription">
    @Column
    @Translatable(translationField = "payGradeStepDescriptionTranslated")
    private String payGradeStepDescription;

    public String getPayGradeStepDescription() {
        return payGradeStepDescription;
    }

    public void setPayGradeStepDescription(String payGradeStepDescription) {
        this.payGradeStepDescription = payGradeStepDescription;
    }

    public String getPayGradeStepDescriptionDD() {
        return "RepEmployee_payGradeStepDescription";
    }

    @Transient
    @Translation(originalField = "payGradeStepDescription")
    private String payGradeStepDescriptionTranslated;

    public String getPayGradeStepDescriptionTranslated() {
        return payGradeStepDescriptionTranslated;
    }

    public void setPayGradeStepDescriptionTranslated(String payGradeStepDescriptionTranslated) {
        this.payGradeStepDescriptionTranslated = payGradeStepDescriptionTranslated;
    }

    public String getPayGradeStepDescriptionTranslatedDD() {
        return "RepEmployee_payGradeStepDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="taxGroupDescription">
    @Column
    @Translatable(translationField = "taxGroupDescriptionTranslated")
    private String taxGroupDescription;

    public String getTaxGroupDescription() {
        return taxGroupDescription;
    }

    public void setTaxGroupDescription(String taxGroupDescription) {
        this.taxGroupDescription = taxGroupDescription;
    }

    public String getTaxGroupDescriptionDD() {
        return "RepEmployee_taxGroupDescription";
    }

    @Transient
    @Translation(originalField = "taxGroupDescription")
    private String taxGroupDescriptionTranslated;

    public String getTaxGroupDescriptionTranslated() {
        return taxGroupDescriptionTranslated;
    }

    public void setTaxGroupDescriptionTranslated(String taxGroupDescriptionTranslated) {
        this.taxGroupDescriptionTranslated = taxGroupDescriptionTranslated;
    }

    public String getTaxGroupDescriptionTranslatedDD() {
        return "RepEmployee_taxGroupDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="insuranceGroupDescription">
    @Column
    @Translatable(translationField = "insuranceGroupDescriptionTranslated")
    private String insuranceGroupDescription;

    public String getInsuranceGroupDescription() {
        return insuranceGroupDescription;
    }

    public void setInsuranceGroupDescription(String insuranceGroupDescription) {
        this.insuranceGroupDescription = insuranceGroupDescription;
    }

    public String getInsuranceGroupDescriptionDD() {
        return "RepEmployee_insuranceGroupDescription";
    }

    @Transient
    @Translation(originalField = "insuranceGroupDescription")
    private String insuranceGroupDescriptionTranslated;

    public String getInsuranceGroupDescriptionTranslated() {
        return insuranceGroupDescriptionTranslated;
    }

    public void setInsuranceGroupDescriptionTranslated(String insuranceGroupDescriptionTranslated) {
        this.insuranceGroupDescriptionTranslated = insuranceGroupDescriptionTranslated;
    }

    public String getInsuranceGroupDescriptionTranslatedDD() {
        return "RepEmployee_insuranceGroupDescription";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeHiringStartDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date employeeHiringStartDate;

    public Date getEmployeeHiringStartDate() {
        return employeeHiringStartDate;
    }

    public void setEmployeeHiringStartDate(Date employeeHiringStartDate) {
        this.employeeHiringStartDate = employeeHiringStartDate;
    }

    public String getEmployeeHiringStartDateDD() {
        return "RepEmployee_employeeHiringStartDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="joiningDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date joiningDate;

    public Date getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getJoiningDateDD() {
        return "RepEmployee_joiningDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="startSalary">
    @Column //(precision=25, scale=2)
    private BigDecimal startSalary;

    public BigDecimal getStartSalary() {
        return startSalary;
    }

    public void setStartSalary(BigDecimal startSalary) {
        this.startSalary = startSalary;
    }

    public String getStartSalaryDD() {
        return "RepEmployee_startSalary";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="startSalaryEnc">
    @Column
    private String startSalaryEnc;

    public String getStartSalaryEnc() {
        return startSalaryEnc;
    }

    public void setStartSalaryEnc(String startSalaryEnc) {
        this.startSalaryEnc = startSalaryEnc;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="terminationDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date terminationDate;

    public Date getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    public String getTerminationDateDD() {
        return "RepEmployee_terminationDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="leaveReason">
    @Column
    @Translatable(translationField = "leaveReasonTranslated")
    private String leaveReason;

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }

    public String getLeaveReasonDD() {
        return "RepEmployee_leaveReason";
    }

    @Transient
    @Translation(originalField = "leaveReason")
    private String leaveReasonTranslated;

    public String getLeaveReasonTranslated() {
        return leaveReasonTranslated;
    }

    public void setLeaveReasonTranslated(String leaveReasonTranslated) {
        this.leaveReasonTranslated = leaveReasonTranslated;
    }

    public String getLeaveReasonTranslatedDD() {
        return "RepEmployee_leaveReason";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="previousExperienceYears">
    @Column
    private Integer previousExperienceYears;

    public Integer getPreviousExperienceYears() {
        return previousExperienceYears;
    }

    public void setPreviousExperienceYears(Integer previousExperienceYears) {
        this.previousExperienceYears = previousExperienceYears;
    }

    public String getPreviousExperienceYearsDD() {
        return "RepEmployee_previousExperienceYears";
    }

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="leaveDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date leaveDate;

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getLeaveDateDD() {
        return "RepEmployee_leaveDate";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="previousExperienceMonths">
    @Column
    private Integer previousExperienceMonths;

    public Integer getPreviousExperienceMonths() {
        return previousExperienceMonths;
    }

    public void setPreviousExperienceMonths(Integer previousExperienceMonths) {
        this.previousExperienceMonths = previousExperienceMonths;
    }

    public String getPreviousExperienceMonthsDD() {
        return "RepEmployee_previousExperienceMonths";
    }
 // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="contractingDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date contractingDate;

    public Date getContractingDate() {
        return contractingDate;
    }

    public void setContractingDate(Date contractingDate) {
        this.contractingDate = contractingDate;
    }

    public String getContractingDateDD() {
        return "RepEmployee_contractingDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="months">
    @Column
    private int months;

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public String getMonthsDD() {
        return "RepEmployee_months";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="contractExpiringDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date contractExpiringDate;

    public Date getContractExpiringDate() {
        return contractExpiringDate;
    }

    public void setContractExpiringDate(Date contractExpiringDate) {
        this.contractExpiringDate = contractExpiringDate;
    }

    public String getContractExpiringDateDD() {
        return "RepEmployee_contractExpiringDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="workingHours">
    @Column
    private int workingHours;

    public int getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(int workingHours) {
        this.workingHours = workingHours;
    }

    public String getWorkingHoursDD() {
        return "RepEmployee_workingHours";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="contractJobName">
    @Column
    private String contractJobName;

    public String getContractJobName() {
        return contractJobName;
    }

    public void setContractJobName(String contractJobName) {
        this.contractJobName = contractJobName;
    }

    public String getContractJobNameDD() {
        return "RepEmployee_contractJobName";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="insuranceOffice">
    @Column
    private String insuranceOffice;

    public String getInsuranceOffice() {
        return insuranceOffice;
    }

    public void setInsuranceOffice(String insuranceOffice) {
        this.insuranceOffice = insuranceOffice;
    }

    public String getInsuranceOfficeDD() {
        return "RepEmployee_insuranceOffice";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="bankBranch">
    @Column
    private String bankBranch;

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankBranchDD() {
        return "RepEmployee_bankBranch";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="savingBox">
    @Column(precision = 25, scale = 13)
    private BigDecimal savingBox;

    public BigDecimal getSavingBox() {
        return savingBox;
    }

    public void setSavingBox(BigDecimal savingBox) {
        this.savingBox = savingBox;
    }

    public String getSavingBoxDD() {
        return "RepEmployee_savingBox";
    }
    // </editor-fold >

    //<editor-fold defaultstate="collapsed" desc="currencyDescription">
    @Column
    @Translatable(translationField = "currencyDescriptionTranslated")
    private String currencyDescription;

    public String getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(String currencyDescription) {
        this.currencyDescription = currencyDescription;
    }

    public String getCurrencyDescriptionDD() {
        return "RepEmployee_currencyDescription";
    }

    @Transient
    @Translation(originalField = "currencyDescription")
    private String currencyDescriptionTranslated;

    public String getCurrencyDescriptionTranslated() {
        return currencyDescriptionTranslated;
    }

    public void setCurrencyDescriptionTranslated(String currencyDescriptionTranslated) {
        this.currencyDescriptionTranslated = currencyDescriptionTranslated;
    }

    public String getCurrencyDescriptionTranslatedDD() {
        return "RepEmployee_currencyDescriptionTranslated";
    }
       //</editor-fold>
}
