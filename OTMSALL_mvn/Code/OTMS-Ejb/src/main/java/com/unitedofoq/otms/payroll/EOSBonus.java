/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
//@ParentEntity(fields="company")
@ParentEntity(fields="leaveReason")
public class EOSBonus extends BusinessObjectBaseEntity {
    //<editor-fold defaultstate="collapsed" desc="company">
    /*@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "EOSBonus_company";
    }*/
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="leaveReason">
    /*@Column
    private String leaveReason;

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }
    public String getLeaveReasonDD() {
        return "EOSBonus_leaveReason";
    }*/
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="servicePeriodFrom">
    @Column(precision=25,scale=13)
    private BigDecimal servicePeriodFrom = new BigDecimal(0);

    public BigDecimal getServicePeriodFrom() {
        return servicePeriodFrom;
    }

    public void setServicePeriodFrom(BigDecimal servicePeriodFrom) {
        this.servicePeriodFrom = servicePeriodFrom;
    }
    
    public String getServicePeriodFromDD() {
        return "EOSBonus_servicePeriodFrom";
    }
    @Transient
    private BigDecimal servicePeriodFromMask;

    public BigDecimal getServicePeriodFromMask() {
        servicePeriodFromMask = servicePeriodFrom;
        return servicePeriodFromMask;
    }

    public void setServicePeriodFromMask(BigDecimal servicePeriodFromMask) {
        updateDecimalValue("servicePeriodFrom",servicePeriodFromMask);
    }
    
    public String getServicePeriodFromMaskDD() {
        return "EOSBonus_servicePeriodFromMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="servicePeriodTo">
    @Column(precision=25,scale=13)
    private BigDecimal servicePeriodTo= new BigDecimal(0);

    public BigDecimal getServicePeriodTo() {
        return servicePeriodTo;
    }

    public void setServicePeriodTo(BigDecimal servicePeriodTo) {
        this.servicePeriodTo = servicePeriodTo;
    }
    
    public String getServicePeriodToDD() {
        return "EOSBonus_servicePeriodTo";
    }
    @Transient
    private BigDecimal servicePeriodToMask;

    public BigDecimal getServicePeriodToMask() {
        servicePeriodToMask = servicePeriodTo;
        return servicePeriodToMask;
    }

    public void setServicePeriodToMask(BigDecimal servicePeriodToMask) {
        updateDecimalValue("servicePeriodTo",servicePeriodToMask);
    }
    
    public String getServicePeriodToMaskDD() {
        return "EOSBonus_servicePeriodToMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="addPrevious">
    @Column(length=1)
    //Y
    //N
    private String addPrevious="N";

    public String getAddPrevious() {
        return addPrevious;
    }

    public void setAddPrevious(String addPrevious) {
        this.addPrevious = addPrevious;
    }

    public String getAddPreviousDD() {
        return "EOSBonus_addPrevious";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="bonusType">
    @Column
    private String bonusType; // V:Value Or P:Percentage

    public String getBonusType() {
        return bonusType;
    }

    public void setBonusType(String bonusType) {
        this.bonusType = bonusType;
    }
    
    public String getBonusTypeDD() {
        return "EOSBonus_bonusType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="amount">
    @Column(precision=25,scale=13)
    private BigDecimal amount= new BigDecimal(0);//value

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getAmountDD() {
        return "EOSBonus_amount";
    }
    @Transient
    private BigDecimal amountMask;

    public BigDecimal getAmountMask() {
        amountMask = amount ;
        return amountMask;
    }

    public void setAmountMask(BigDecimal amountMask) {
        updateDecimalValue("amount",amountMask);
    }
    public String getAmountMaskDD() {
        return "EOSBonus_amountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="income">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }
    
    public String getIncomeDD() {
        return "EOSBonus_income";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calcFromType">
    @Column
    private String calcFromType; // C:Current Or L:Last Month Or D:Last December

    public String getCalcFromType() {
        return calcFromType;
    }

    public void setCalcFromType(String calcFromType) {
        this.calcFromType = calcFromType;
    }
    
    public String getCalcFromTypeDD() {
        return "EOSBonus_calcFromType";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="internalCode">
    @Column
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }
    
    public String getInternalCodeDD() {
        return "EOSBonus_internalCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="leaveReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    public EOSLeaveReason leaveReason;

    public EOSLeaveReason getLeaveReason() {
        return leaveReason;
    }
    public String getLeaveReasonDD() {
        return "EOSBonus_leaveReason";
    }

    public void setLeaveReason(EOSLeaveReason leaveReason) {
        this.leaveReason = leaveReason;
    }
    //</editor-fold >
}