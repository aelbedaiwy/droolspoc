
package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobAppRejection extends BaseEntity {

    @Column(nullable=false)
	private String reason;
    @Column(nullable=false)
    @Temporal(TemporalType.DATE)
	private Date rejectionDate;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobApplicant_DBID", nullable=false)
    private JobApplicant jobApplicant;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Employee rejectedBy;

	public String getReason() {
		return reason;
	}
 
	public void setReason(String theReason) {
		reason = theReason;
	}
 
	public Date getRejectionDate() {
		return rejectionDate;
	}
  
	public void setRejectionDate(Date theRejectionDate) {
		rejectionDate = theRejectionDate;
	}

	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant theJobapplicant) {
		jobApplicant = theJobapplicant;
	}	

	public Employee getRejectedBy() {
		return rejectedBy;
	}

	public void setRejectedBy(Employee theRejectedBy) {
		rejectedBy = theRejectedBy;
	}
}
