/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.job;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.Date;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="competencyLevel")
public class EDSJobCompetencyHistory extends BaseEntity{
    private String source;
    private String reference;
    @Temporal(TemporalType.DATE)
    private Date evaluationDate;
    private String evaluator;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSJobCompetency competencyLevel;

    public String getSourceDD()         {    return "EDSJobCompetencyHistory_source";  }
    public String getReferenceDD()      {    return "EDSJobCompetencyHistory_reference";  }
    public String getEvaluationDateDD() {    return "EDSJobCompetencyHistory_evaluationDate";  }
    public String getEvaluatorDD()      {    return "EDSJobCompetencyHistory_evaluator";  }
    public String getCompetencyLevelDD()      {    return "EDSJobCompetencyHistory_competencyLevel";  }
    public EDSJobCompetency getCompetencyLevel() {
        return competencyLevel;
    }

    public void setCompetencyLevel(EDSJobCompetency competencyLevel) {
        this.competencyLevel = competencyLevel;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(String evaluator) {
        this.evaluator = evaluator;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
