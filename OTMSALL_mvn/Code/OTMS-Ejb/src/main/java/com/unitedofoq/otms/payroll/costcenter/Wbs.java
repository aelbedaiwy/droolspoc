/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author mostafa
 */
@Entity
public class Wbs extends BaseEntity {
    //<editor-fold defaultstate="collapse" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "Wbs_code";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapse" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionDD() {
        return "Wbs_description";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapse" desc="sapProject">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SapProject sapProject;

    public SapProject getSapProject() {
        return sapProject;
    }

    public void setSapProject(SapProject sapProject) {
        this.sapProject = sapProject;
    }
    public String getSapProjectDD() {
        return "Wbs_sapProject";
    }
    //</editor-fold>
}
