package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.competency.EDSCompetency;
import com.unitedofoq.otms.eds.competency.EDSCompetencyLevel;
import com.unitedofoq.otms.eds.foundation.job.EDSJobCompetency;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields="employeeProfiler")
public class EDSEmployeeCompetency extends BaseEntity{
    @OneToMany(mappedBy="employeeCompetency")
    private List<EDSCompetencyAssessment> competencyAssessments;
    public String getCompetencyAssessmentsDD(){    return "EDSEmployeeCompetency_competencyAssessments";  }
    public List<EDSCompetencyAssessment> getCompetencyAssessments() {
        return competencyAssessments;
    }

    public void setCompetencyAssessments(List<EDSCompetencyAssessment> competencyAssessments) {
        this.competencyAssessments = competencyAssessments;
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private EmployeeProfiler employeeProfiler;

    public EmployeeProfiler getEmployeeProfiler() {
        return employeeProfiler;
    }

    public void setEmployeeProfiler(EmployeeProfiler employeeProfiler) {
        this.employeeProfiler = employeeProfiler;
    }
    public String getEmployeeProfilerDD(){    return "EDSEmployeeCompetency_employeeProfiler";  }

    @Column(name="comments")
    private String comment;
    public String getCommentDD(){    return "EDSEmployeeCompetency_comment";  }
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    EDSCompetency competency;
    public String getCompetencyDD(){    return "EDSEmployeeCompetency_competency";  }
    public EDSCompetency getCompetency() {
        return competency;
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }

    @Transient
    private boolean level;
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    @JoinColumn
//    private EDSIDPBusinessOutcomeForJobGap jobGap;


    @Transient
    EDSJobCompetency jobCompetency;

    public EDSJobCompetency getJobCompetency() {
        if(this.getCompetency()!=null && this.employeeProfiler != null &&this.employeeProfiler.getEmployee() != null &&
                this.employeeProfiler.getEmployee().getJob()!=null && this.employeeProfiler.getEmployee().
                getJob().getJobCompetencies()!=null){
            for (int i = 0; i < employeeProfiler.getEmployee().getJob().getJobCompetencies().size(); i++) {
                if(this.getCompetency().getDbid() == this.employeeProfiler.getEmployee().getJob().
                        getJobCompetencies().get(i).getCompetency().getDbid()){
                    return this.employeeProfiler.getEmployee().getJob().getJobCompetencies().get(i);
                }
            }
            return null;
        }
		return null;
    }

    public String getJobCompetencyDD() {
        return "EDSEmployeeCompetency_jobCompetency";
    }

    public void setJobCompName(EDSJobCompetency jobCompetency) {
        this.jobCompetency = jobCompetency;
    }

    @Transient
    Integer rankGap;

    public Integer getRankGap() {
        if(getJobCompetency()!=null){
           return  getJobCompetency().getCompetencyLevel().getRankValue() - this.getCompetencyLevel().getRankValue();
        }
        return null;
    }

    public String getRankGapDD() {
        return "EDSEmployeeCompetency_rankGap";
    }

    public void setRankGap(Integer rankGap) {
        this.rankGap = rankGap;
    }

  

//    public EDSIDPBusinessOutcomeForJobGap getJobGap() {
//        return jobGap;
//    }
//
//    public void setJobGap(EDSIDPBusinessOutcomeForJobGap jobGap) {
//        this.jobGap = jobGap;
//    }

    @Transient
    private int levelOrder;
    public String getLevelOrderDD(){    return "EDSEmployeeCompetency_levelOrder";  }

   
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetencyLevel competencyLevel;
    public String getCompetencyLevelDD(){    return "EDSEmployeeCompetency_competencyLevel";  }

    public EDSCompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }

    public void setCompetencyLevel(EDSCompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }

    public void setLevelOrder(int levelOrder) {
        this.levelOrder = levelOrder;
    }

    public int getLevelOrder() {
        levelOrder = competencyLevel.getSortIndex();
        return levelOrder;
    }

    public boolean isLevel() {
        return level;
    }

    public void setLevel(boolean level) {
        this.level = level;
    }
    @Override
    public  void PostLoad(){
        super.PostLoad();
        level = true;
    }
}