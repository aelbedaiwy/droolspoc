/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.jobrequisitionapplicant;

import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author abdullahm
 */
@Entity
public class JobReqAppOffer extends BaseEntity {
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "JobRequisitionApplicant")
    private JobRequisitionApplicant jobRequisitionApplicant;
    public JobRequisitionApplicant getJobRequisitionApplicant()
    {
        return jobRequisitionApplicant;
    }
    
    @Column(nullable=false)
    private String offerLetterBody;
    public String getOfferLetterBodyDD(){
        return "JobReqAppOffer_offerLetterBody";
    }
    
    private String acceptanceComment;
    public String getAcceptanceCommentDD(){
        return "JobReqAppOffer_acceptanceComment";
    }
    @Column(nullable=false)
    private Double offeredSalary;
    public String getOfferedSalaryDD(){
        return "JobReqAppOffer_offeredSalary";
    }
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(nullable=false)
    private Date expectedStartingDate;
    public String getExpectedStartingDateDD(){
        return "JobReqAppOffer_expectedStartingDate";
    }
    @Column(nullable=false)
    private boolean sent;// = new Boolean(false);
    public String getSentDD(){
        return "JobReqAppOffer_sent";
    }
    
    private boolean accepted;
    public String getAcceptedDD(){
        return "JobReqAppOffer_accepted";
    }

    //Attributes from direct assosiation relations
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC currency;
    public String getCurrencyDD(){
        return "JobReqAppOffer_currency";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private OUser creator;
    public String getCreatorDD(){
        return "JobReqAppOffer_creator";
    }
    public String getAcceptanceComment() {
        return acceptanceComment;
    }

    public void setAcceptanceComment(String acceptanceComment) {
        this.acceptanceComment = acceptanceComment;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    
    public OUser getCreator() {
        return creator;
    }

    public void setCreator(OUser creator) {
        this.creator = creator;
    }

    public UDC getCurrency() {
        return currency;
    }

    public void setCurrency(UDC currency) {
        this.currency = currency;
    }

    public Date getExpectedStartingDate() {
        return expectedStartingDate;
    }

    public void setExpectedStartingDate(Date expectedStartingDate) {
        this.expectedStartingDate = expectedStartingDate;
    }

    public String getOfferLetterBody() {
        return offerLetterBody;
    }

    public void setOfferLetterBody(String offerLetterBody) {
        this.offerLetterBody = offerLetterBody;
    }

    public Double getOfferedSalary() {
        return offeredSalary;
    }

    public void setOfferedSalary(Double offeredSalary) {
        this.offeredSalary = offeredSalary;
    }

   

}
