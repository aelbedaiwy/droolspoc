/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.company;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.multimedia.OImage;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.currency.Currency;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author mmohamed
 */
@Entity
@Table(name = "Company")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="CTYPE",discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue(value ="MASTER")
public class CompanyBase extends BusinessObjectBaseEntity  {
    @Column(nullable=false,unique=true)
    private String prefix;

    public String getPrefix() {
        return prefix;
    }

    public String getPrefixDD() {
        return "CompanyBase_prefix";
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    // <editor-fold defaultstate="collapsed" desc="address">
    @Column
    private String address;
     public String getAddress() {
        return address;
    }

    @Column
    private String code;    // we need to alter the column in the DB to make it unique

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD()               {     return "EDSCompany_code";  }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getAddressDD()    { return "Company_address";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="daysPerWeek">
    @Column
	private Integer daysPerWeek;
     public Integer getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(Integer daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

     public String getDaysPerWeekDD(){ return "Company_daysPerWeek";  }
	// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="email">
    @Column
    private String email;
     public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmailDD()      { return "Company_email";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fax">
    @Column
	private String fax;
     public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
     }
    public String getFaxDD()        { return "Company_fax";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hoursPerDay">
    @Column
	private Integer hoursPerDay;
    public Integer getHoursPerDay() {
        return hoursPerDay;
    }

    public void setHoursPerDay(Integer hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }
    public String getHoursPerDayDD(){ return "Company_hoursPerDay";  }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="telephone">
    @Column
	private String telephone;
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
     public String getTelephoneDD()        { return "Company_telephone";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="website">
    @Column
	private String website;
     public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
    public String getWebsiteDD()    { return "Company_website";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="holding">
    @Column
	private boolean holding;
     public boolean isHolding() {
        return holding;
    }

    public void setHolding(boolean holding) {
        this.holding = holding;
    }
     public String getHoldingDD()    { return "Company_holding";  }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">

    @Translatable(translationField="nameTranslated")
    @Column(unique=true, nullable=false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getNameTranslatedDD()       { return "Company_name";  }

    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="localCurrency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Currency localCurrency;

    public Currency getLocalCurrency() {
        return localCurrency;
    }

    public void setLocalCurrency(Currency localCurrency) {
        this.localCurrency = localCurrency;
    }
    public String getLocalCurrencyDD() {
        return "company_localCurrency";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="state">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC state;

        public UDC getState() {
            return state;
        }

        public void setState(UDC state) {
            this.state = state;
        }

        public String getStateDD() {
            return "Company_state";
        }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="city">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC city;
        public String getCityDD(){
            return "Company_city";
        }

        public UDC getCity() {
            return city;
        }

        public void setCity(UDC city) {
            this.city = city;
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="country">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC country;
        public String getCountryDD(){
            return "Company_country";
        }

        public UDC getCountry() {
            return country;
        }

        public void setCountry(UDC country) {
            this.country = country;
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ownerName">
    @Translatable(translationField="ownerNameTranslated")
    private String ownerName;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    @Translation(originalField="ownerName")
    @Transient
    private String ownerNameTranslated;

    public String getOwnerNameTranslated() {
        return ownerNameTranslated;
    }

    public void setOwnerNameTranslated(String ownerNameTranslated) {
        this.ownerNameTranslated = ownerNameTranslated;
    }

    public String getOwnerNameTranslatedDD() {
        return "Company_ownerName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="specilization">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        @JoinColumn
        private UDC specialization;

        public UDC getSpecialization() {
            return specialization;
        }

        public void setSpecialization(UDC specialization) {
            this.specialization = specialization;
        }


         public String getSpecilizationDD() {
            return "Company_specilization";
        }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="industry">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC industry;

    public UDC getIndustry() {
        return industry;
    }

    public void setIndustry(UDC industry) {
        this.industry = industry;
    }


    public String getIndustryDD() {
        return "Company_industry";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="numberofEmployees">
        private int    numberofEmployees;

        public int getNumberofEmployees() {
            return numberofEmployees;
        }

        public void setNumberofEmployees(int numberofEmployees) {
            this.numberofEmployees = numberofEmployees;
        }
        public String getNumberofEmployeesDD() {
            return "Company_numberofEmployees";
        }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="numberofBranches">
        private int    numberofBranches;

        public int getNumberofBranches() {
            return numberofBranches;
        }

        public void setNumberofBranches(int numberofBranches) {
            this.numberofBranches = numberofBranches;
        }
        public String getNumberofBranchesDD() {
            return "Company_numberofBranches";
        }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="annualTurnover">
        private int    annualTurnover;

        public int getAnnualTurnover() {
            return annualTurnover;
        }

        public void setAnnualTurnover(int annualTurnover) {
            this.annualTurnover = annualTurnover;
        }
        public String getAnnualTurnoverDD() {
            return "Comapny_annualTurnover";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="establishment">
        @Temporal(javax.persistence.TemporalType.DATE)
        private Date   establishment;

        public Date getEstablishment() {
            return establishment;
        }

        public void setEstablishment(Date establishment) {
            this.establishment = establishment;
        }
        public String getEstablishmentDD() {
            return "Company_establishment";
        }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workNo">
    private String workNo;

    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }
    public String getWorkNoDD() {
        return "Company_workNo";
    }
  // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="subsidiaries">
    private String subsidiaries;

    public String getSubsidiaries() {
        return subsidiaries;
    }

    public void setSubsidiaries(String subsidiaries) {
        this.subsidiaries = subsidiaries;
    }
    public String getSubsidiariesDD() {
        return "Company_subsidiaries";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="photo">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private OImage logo;

    public OImage getLogo() {
        return logo;
    }

    public void setLogo(OImage logo) {
        this.logo = logo;
    }

    public String getLogoDD(){
        return "Company_logo";
    }
 // </editor-fold>
}