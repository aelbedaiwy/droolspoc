package com.unitedofoq.otms.recruitment.jobapplicant;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.OneToOne;

@Entity
@ParentEntity(fields={"jobApplicant"})
public class JobApplicantCV extends BaseEntity {

    @Column(nullable = false)
    private String extension;
    @Column(columnDefinition = "BLOB(8388608)")
    private Byte[] cvFile;
    public String getCvFileDD(){
        return "JobApplicantCV_cvFile";
    }
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private JobApplicant jobApplicant;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String theExtension) {
        extension = theExtension;
    }

    public Byte[] getCvFile() {
        return cvFile;
    }

    public void setCvFile(Byte[] cvFile) {
        this.cvFile = cvFile;
    }

    public JobApplicant getJobApplicant() {
        return jobApplicant;
    }

    public void setJobApplicant(JobApplicant theJobApplicant) {
        jobApplicant = theJobApplicant;
    }
}
