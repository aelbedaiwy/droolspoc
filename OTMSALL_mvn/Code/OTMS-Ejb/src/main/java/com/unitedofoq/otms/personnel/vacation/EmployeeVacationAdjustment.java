package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class EmployeeVacationAdjustment extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeVacationAdjustment_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="creationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDateDD() {
        return "EmployeeVacationAdjustment_creationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="adjustmentValue">
    private BigDecimal adjustmentValue;

    public BigDecimal getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(BigDecimal adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    public String getAdjustmentValueDD() {
        return "EmployeeVacationAdjustment_adjustmentValue";
    }
    @Transient
    private BigDecimal adjustmentValueMask;

    public BigDecimal getAdjustmentValueMask() {
        adjustmentValueMask = adjustmentValue;
        return adjustmentValueMask;
    }

    public void setAdjustmentValueMask(BigDecimal adjustmentValueMask) {
        updateDecimalValue("adjustmentValue", adjustmentValueMask);
    }

    public String getAdjustmentValueMaskDD() {
        return "EmployeeVacationAdjustment_adjustmentValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "EmployeeVacationAdjustment_comments";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacation">
    @ManyToOne(fetch = FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "EmployeeVacationAdjustment_vacation";
    }
    // </editor-fold>
}
