/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="repemployeepenalty")
public class RepEmployeePenalty extends RepEmployeeBase{
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeePenalty_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeePenalty_genderDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepEmployeePenalty_employeeActive";
    }
    
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeePenalty_employeeActive";
    }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="penaltyValue">
    @Column(precision=18,scale=3)
    private BigDecimal penaltyValue;

    public BigDecimal getPenaltyValue() {
        return penaltyValue;
    }

    public void setPenaltyValue(BigDecimal penaltyValue) {
        this.penaltyValue = penaltyValue;
    }
    public String getPenaltyValueDD() {
        return "RepEmployeePenalty_penaltyValue";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="givenDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date givenDate;

    public Date getGivenDate() {
        return givenDate;
    }

    public void setGivenDate(Date givenDate) {
        this.givenDate = givenDate;
    }

    public String getGivenDateDD() {
        return "RepEmployeePenalty_givenDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="penaltyNo">
    private Long penaltyNo;

    public Long getPenaltyNo() {
        return penaltyNo;
    }

    public void setPenaltyNo(Long penaltyNo) {
        this.penaltyNo = penaltyNo;
    }
    public String getPenaltyNoDD() {
        return "RepEmployeePenalty_penaltyNo";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="canceledBy">
    @Column
    @Translatable(translationField = "canceledByTranslated")
    private String canceledBy;

    public String getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(String canceledBy) {
        this.canceledBy = canceledBy;
    }

    public String getCanceledByDD() {
        return "RepEmployeePenalty_canceledBy";
    }
    
    @Transient
    @Translation(originalField = "canceledBy")
    private String canceledByTranslated;

    public String getCanceledByTranslated() {
        return canceledByTranslated;
    }

    public void setCanceledByTranslated(String canceledByTranslated) {
        this.canceledByTranslated = canceledByTranslated;
    }
    
    public String getCanceledByTranslatedDD() {
        return "RepEmployeePenalty_canceledBy";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="approvedBy">
    @Column
    @Translatable(translationField = "approvedByTranslated")
    private String approvedBy;

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "RepEmployeePenalty_approvedBy";
    }
    
    @Transient
    @Translation(originalField = "approvedBy")
    private String approvedByTranslated;

    public String getApprovedByTranslated() {
        return approvedByTranslated;
    }

    public void setApprovedByTranslated(String approvedByTranslated) {
        this.approvedByTranslated = approvedByTranslated;
    }
    
    public String getApprovedByTranslatedDD() {
        return "RepEmployeePenalty_approvedBy";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="givenBy">
    @Column
    @Translatable(translationField = "givenByTranslated")
    private String givenBy;

    public String getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }

    public String getGivenByDD() {
        return "RepEmployeePenalty_givenBy";
    }
    
    @Transient
    @Translation(originalField = "givenBy")
    private String givenByTranslated;

    public String getGivenByTranslated() {
        return givenByTranslated;
    }

    public void setGivenByTranslated(String givenByTranslated) {
        this.givenByTranslated = givenByTranslated;
    }
    
    public String getGivenByTranslatedDD() {
        return "RepEmployeePenalty_givenBy";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="penaltyReason">
    @Column
    @Translatable(translationField = "penaltyReasonTranslated")
    private String penaltyReason;

    public String getPenaltyReason() {
        return penaltyReason;
    }

    public void setPenaltyReason(String penaltyReason) {
        this.penaltyReason = penaltyReason;
    }

    public String getPenaltyReasonDD() {
        return "RepEmployeePenalty_penaltyReason";
    }
    
    @Transient
    @Translation(originalField = "penaltyReason")
    private String penaltyReasonTranslated;

    public String getPenaltyReasonTranslated() {
        return penaltyReasonTranslated;
    }

    public void setPenaltyReasonTranslated(String penaltyReasonTranslated) {
        this.penaltyReasonTranslated = penaltyReasonTranslated;
    }
    
    public String getPenaltyReasonTranslatedDD() {
        return "RepEmployeePenalty_penaltyReason";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="previousPenaltyDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date previousPenaltyDate;
    
    public Date getPreviousPenaltyDate() {
        return previousPenaltyDate;
    }

    public void setPreviousPenaltyDate(Date previousPenaltyDate) {
        this.previousPenaltyDate = previousPenaltyDate;
    }

     public String getPreviousPenaltyDateDD() {
        return "RepEmployeePenalty_previousPenaltyDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="cancelled">
    @Column(length=1)
    private String cancelled;

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

     public String getCancelledDD() {
        return "RepEmployeePenalty_cancelled";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="cancelReason">
    @Column
    @Translatable(translationField = "cancelReasonTranslated")
    private String cancelReason;

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelReasonDD() {
        return "RepEmployeePenalty_cancelReason";
    }
    
    @Transient
    @Translation(originalField = "cancelReason")
    private String cancelReasonTranslated;

    public String getCancelReasonTranslated() {
        return cancelReasonTranslated;
    }

    public void setCancelReasonTranslated(String cancelReasonTranslated) {
        this.cancelReasonTranslated = cancelReasonTranslated;
    }
    
    public String getCancelReasonTranslatedDD() {
        return "RepEmployeePenalty_cancelReason";
    }
     //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="cancelComment">
    @Column
    private String cancelComment;

    public String getCancelComment() {
        return cancelComment;
    }

    public void setCancelComment(String cancelComment) {
        this.cancelComment = cancelComment;
    }

    public String getCancelCommentDD() {
        return "RepEmployeePenalty_cancelComment";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="givenNotes">
    @Column
    private String givenNotes;

    public String getGivenNotes() {
        return givenNotes;
    }

    public void setGivenNotes(String givenNotes) {
        this.givenNotes = givenNotes;
    }

    public String getGivenNotesDD() {
        return "RepEmployeePenalty_givenNotes";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="occurrence">
    @Column(precision=18,scale=3)
    private BigDecimal occurrence;

    public BigDecimal getOccurrence() {
        return occurrence;
    }

    public void setOccurrence(BigDecimal occurrence) {
        this.occurrence = occurrence;
    }

    public String getOccurrenceDD() {
        return "RepEmployeePenalty_occurrence";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="countNumber">
    @Column
    private Short countNumber;

    public Short getCountNumber() {
        return countNumber;
    }

    public void setCountNumber(Short countNumber) {
        this.countNumber = countNumber;
    }

    public String getCountNumberDD() {
        return "RepEmployeePenalty_countNumber";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="postponedValues">
    @Column(precision=18,scale=3)
    private BigDecimal postponedValues;

    public BigDecimal getPostponedValues() {
        return postponedValues;
    }

    public void setPostponedValues(BigDecimal postponedValues) {
        this.postponedValues = postponedValues;
    }

    public String getPostponedValuesDD() {
        return "RepEmployeePenalty_postponedValues";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="total">
    @Column(precision=18,scale=3)
    private BigDecimal total;

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

     public String getTotalDD() {
        return "RepEmployeePenalty_total";
     }
     //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="totalDays">
    @Column(precision=18,scale=3)
    private BigDecimal totalDays;//(name = "penalty_total_days")

    public BigDecimal getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(BigDecimal totalDays) {
        this.totalDays = totalDays;
    }

     public String getTotalDaysDD() {
        return "RepEmployeePenalty_totalDays";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="refreshStartFrom">
     @Column
     @Temporal(TemporalType.TIMESTAMP)
     private Date refreshStartFrom;

    public Date getRefreshStartFrom() {
        return refreshStartFrom;
    }

    public void setRefreshStartFrom(Date refreshStartFrom) {
        this.refreshStartFrom = refreshStartFrom;
    }

     public String getRefreshStartFromDD() {
        return "RepEmployeePenalty_refreshStartFrom";
    }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="penalty">
    private String penalty;

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }
     
    public String getPenaltyDD() {
        return "RepEmployeePenalty_penalty";
    }
    //</editor-fold>
}
