package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.vacation.VacationBase;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("VACATIONGROUP")
@ParentEntity(fields = "company")
@ChildEntity(fields = {"affectingDeductions", "affectingIncomes", "penalties",
    "members", "formulas"})
public class VacationGroup extends VacationBase {
    // <editor-fold defaultstate="collapsed" desc="advancedPay">

    @Column
    private String advancedPay;//advanced_pay

    public String getAdvancedPay() {
        return advancedPay;
    }

    public void setAdvancedPay(String advancedPay) {
        this.advancedPay = advancedPay;
    }

    public String getAdvancedPayDD() {
        return "VacationGroup_advancedPay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payrollCutoffUse">
    @Column
    private String payrollCutoffUse;//pay_use

    public String getPayrollCutoffUse() {
        return payrollCutoffUse;
    }

    public void setPayrollCutoffUse(String payrollCutoffUse) {
        this.payrollCutoffUse = payrollCutoffUse;
    }

    public String getPayrollCutoffUseDD() {
        return "VacationGroup_payrollCutoffUse";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumToPost">
    @Column(precision = 18, scale = 3)
    private BigDecimal minimumToPost;//min_to_post

    public BigDecimal getMinimumToPost() {
        return minimumToPost;
    }

    public void setMinimumToPost(BigDecimal minimumToPost) {
        this.minimumToPost = minimumToPost;
    }

    public String getMinimumToPostDD() {
        return "VacationGroup_minimumToPost";
    }
    @Transient
    private BigDecimal minimumToPostMask;

    public BigDecimal getMinimumToPostMask() {
        minimumToPostMask = minimumToPost;
        return minimumToPostMask;
    }

    public void setMinimumToPostMask(BigDecimal minimumToPostMask) {
        updateDecimalValue("minimumToPost", minimumToPostMask);
    }

    public String getMinimumToPostMaskDD() {
        return "VacationGroup_minimumToPostMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startMonth">
    @Column
    private Short startMonth;//month_to_start

    public Short getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Short startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartMonthDD() {
        return "VacationGroup_startMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="continious">
    @Column(length = 1)
    // Y - N
    private String continious;//continious

    public String getContinious() {
        return continious;
    }

    public void setContinious(String continious) {
        this.continious = continious;
    }

    public String getContiniousDD() {
        return "VacationGroup_continious";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectActualWorkPeriod">
    @Column(length = 1)
    //Y - N
    private String affectActualWorkPeriod;//affect_actual_work_period

    public String getAffectActualWorkPeriod() {
        return affectActualWorkPeriod;
    }

    public void setAffectActualWorkPeriod(String affectActualWorkPeriod) {
        this.affectActualWorkPeriod = affectActualWorkPeriod;
    }

    public String getAffectActualWorkPeriodDD() {
        return "VacationGroup_affectActualWorkPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="members">
    @OneToMany(mappedBy = "vacationGroup")
    private List<VacationGroupMember> members;

    public List<VacationGroupMember> getMembers() {
        return members;
    }

    public void setMembers(List<VacationGroupMember> members) {
        this.members = members;
    }

    public String getMembersDD() {
        return "VacationGroup_members";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationAffectingDeductions">
    @OneToMany(mappedBy = "vacationGroup")
    private List<VacationGroupAffectingDeduction> affectingDeductions;

    public List<VacationGroupAffectingDeduction> getAffectingDeductions() {
        return affectingDeductions;
    }

    public void setAffectingDeductions(List<VacationGroupAffectingDeduction> affectingDeductions) {
        this.affectingDeductions = affectingDeductions;
    }

    public String getAffectingDeductionsDD() {
        return "VacationGroup_affectingDeductions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationAffectingIncomes">
    @OneToMany(mappedBy = "vacationGroup")
    private List<VacationGroupAffectingIncome> affectingIncomes;

    public List<VacationGroupAffectingIncome> getAffectingIncomes() {
        return affectingIncomes;
    }

    public void setAffectingIncomes(List<VacationGroupAffectingIncome> affectingIncomes) {
        this.affectingIncomes = affectingIncomes;
    }

    public String getAffectingIncomesDD() {
        return "VacationGroup_affectingIncomes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penalties">
    @OneToMany(mappedBy = "vacationGroup")
    private List<VacationGroupPenalty> penalties;

    public List<VacationGroupPenalty> getPenalties() {
        return penalties;
    }

    public void setPenalties(List<VacationGroupPenalty> penalties) {
        this.penalties = penalties;
    }

    public String getPenaltiesDD() {
        return "VacationGroup_penalties";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PrePersist">

    @Override
    public void PrePersist() {
        super.PrePersist();
        if (this.getIsGroup() == null || this.getIsGroup().compareTo("N") == 0 || this.getIsGroup().compareTo("") == 0) {
            this.setIsGroup("Y");
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="formulas">
    @OneToMany(mappedBy = "vacation")
    private List<VacationGroupFormula> formulas;

    public List<VacationGroupFormula> getFormulas() {
        return formulas;
    }

    public void setFormulas(List<VacationGroupFormula> formulas) {
        this.formulas = formulas;
    }

    public String getFormulasDD() {
        return "VacationGroup_formulas";
    }
    // </editor-fold>
}
