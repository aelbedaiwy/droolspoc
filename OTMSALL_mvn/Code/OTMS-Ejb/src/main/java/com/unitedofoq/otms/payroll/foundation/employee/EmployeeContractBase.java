/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author bassem
 */
@MappedSuperclass
public class EmployeeContractBase extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="contractExpiringDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date contractExpiringDate;

    public Date getContractExpiringDate() {
        return contractExpiringDate;
    }

    public void setContractExpiringDate(Date contractExpiringDate) {
        this.contractExpiringDate = contractExpiringDate;
    }
    public String getContractExpiringDateDD() {
        return "EmployeeContract_contractExpiringDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="contractCode">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EmployeeContract_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="contractJob">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job contractJob;

    public Job getContractJob() {
        return contractJob;
    }

    public void setContractJob(Job contractJob) {
        this.contractJob = contractJob;
    }

    public String getContractJobDD() {
        return "EmployeeContract_contractJob";
    }


// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="grade">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade grade;

    public PayGrade getGrade() {
        return grade;
    }

    public void setGrade(PayGrade grade) {
        this.grade = grade;
    }

    public String getGradeDD() {
        return "EmployeeContract_grade";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="contractingDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date contractingDate;

    public Date getContractingDate() {
        return contractingDate;
    }

    public void setContractingDate(Date contractingDate) {
        this.contractingDate = contractingDate;
    }

    public String getContractingDateDD() {
        return "EmployeeContract_contractingDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="months">
    @Column
    private Integer months;

    public Integer getMonths() {
        return months;
    }

    public void setMonths(Integer months) {
        this.months = months;
    }

     public String getMonthsDD() {
        return "EmployeeContract_months";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="providentFundStartDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date providentFundStartDate;

    public Date getProvidentFundStartDate() {
        return providentFundStartDate;
    }

    public void setProvidentFundStartDate(Date providentFundStartDate) {
        this.providentFundStartDate = providentFundStartDate;
    }

    public String getProvidentFundStartDateDD() {
        return "EmployeeContract_providentFundStartDate";
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingHours">
    @Column(precision=25, scale=13)
    private BigDecimal workingHours;

    public BigDecimal getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(BigDecimal workingHours) {
        this.workingHours = workingHours;
    }
    public String getWorkingHoursDD() {
        return "EmployeeContract_workingHours";
    }
    @Transient
    private BigDecimal workingHoursMask;

    public BigDecimal getWorkingHoursMask() {
        workingHoursMask = workingHours ;
        return workingHoursMask;
    }

    public void setWorkingHoursMask(BigDecimal workingHoursMask) {
        updateDecimalValue("workingHours",workingHoursMask);
    }
    public String getWorkingHoursMaskDD() {
        return "EmployeeContract_workingHoursMask";
    }
    // </editor-fold>
}
