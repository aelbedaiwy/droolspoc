package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class AlstomCCAllocation extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="calcPeriod">

    @ManyToOne(fetch = FetchType.LAZY)
    private CalculatedPeriod calcPeriod;

    public CalculatedPeriod getCalcPeriod() {
        return calcPeriod;
    }

    public void setCalcPeriod(CalculatedPeriod calcPeriod) {
        this.calcPeriod = calcPeriod;
    }

    public String getCalcPeriodDD() {
        return "AlstomCCAllocation_calcPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "AlstomCCAllocation_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = FetchType.LAZY)
    private CostCodeFunction costCenter;

    public CostCodeFunction getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCodeFunction costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterDD() {
        return "AlstomCCAllocation_costCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ccFunction">
    @ManyToOne(fetch = FetchType.LAZY)
    private CostCodeFunction ccFunction;

    public CostCodeFunction getCcFunction() {
        return ccFunction;
    }

    public void setCcFunction(CostCodeFunction ccFunction) {
        this.ccFunction = ccFunction;
    }

    public String getCcFunctionDD() {
        return "AlstomCCAllocation_ccFunction";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="percentage">
    private BigDecimal percentage;

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public String getPercentageDD() {
        return "AlstomCCAllocation_percentage";
    }
    @Transient
    private BigDecimal percentageMask;

    public BigDecimal getPercentageMask() {
        percentageMask = percentage;
        return percentageMask;
    }

    public void setPercentageMask(BigDecimal percentageMask) {
        updateDecimalValue("percentage", percentageMask);
    }

    public String getPercentageMaskDD() {
        return "AlstomCCAllocation_percentageMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedValue">
    private BigDecimal calculatedValue;

    public BigDecimal getCalculatedValue() {
        return calculatedValue;
    }

    public void setCalculatedValue(BigDecimal calculatedValue) {
        this.calculatedValue = calculatedValue;
    }

    public String getCalculatedValueDD() {
        return "AlstomCCAllocation_calculatedValue";
    }
    @Transient
    private BigDecimal calculatedValueMask;

    public BigDecimal getCalculatedValueMask() {
        calculatedValueMask = calculatedValue;
        return calculatedValueMask;
    }

    public void setCalculatedValueMask(BigDecimal calculatedValueMask) {
        updateDecimalValue("calculatedValue", calculatedValueMask);
    }

    public String getCalculatedValueMaskDD() {
        return "AlstomCCAllocation_calculatedValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "AlstomCCAllocation_salaryElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="credit">
    private String credit;

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getCreditDD() {
        return "AlstomCCAllocation_credit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="debit">
    private String debit;

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getDebitDD() {
        return "AlstomCCAllocation_debit";
    }
    // </editor-fold>
}