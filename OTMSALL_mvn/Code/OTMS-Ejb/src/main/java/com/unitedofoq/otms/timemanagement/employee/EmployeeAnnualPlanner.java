package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@ParentEntity(fields = {"employee"})
@ChildEntity(fields={"attendances"})
public class EmployeeAnnualPlanner extends TMEmpAnnualBase {

    //<editor-fold defaultstate="collapsed" desc="attendances">
    @OneToMany(mappedBy="annualPlanner")
    private List<EmployeeDailyAttendance> attendances;

    public List<EmployeeDailyAttendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(List<EmployeeDailyAttendance> attendances) {
        this.attendances = attendances;
    }
    
    public String getAttendancesDD() {
        return "EmployeeAnnualPlanner_attendances";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeAnnualPlanner_employee";
    }
    //</editor-fold>
    
    @Override
    protected void PostLoad() {        
        
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) +1;
        int day = Calendar.getInstance().get(Calendar.DATE) +1;

        int startDay = 0;
        int startMonth = 0;

        if (Integer.parseInt(getPlannerYear()) == year) {
            startDay = day;
            startMonth = month;
        } else if (Integer.parseInt(getPlannerYear()) == year) {
            startDay = 1;
            startMonth = 1;
        }

        if (startDay > 0 && startMonth > 0) {
            for (int m = startMonth; m <= 12; m++) {
                int monthEnd = 0;
                                
                int monthStart = 1;
                
                if(Integer.parseInt(getPlannerYear()) == year && m == month){
                    monthStart = day;
                }
                
                if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) {
                    monthEnd = 31;
                } else if (m == 2) {
                    if (year % 4 == 0) {
                        monthEnd = 29;
                    } else {
                        monthEnd = 28;
                    }
                    
                } else if (m == 4 || m == 6 || m == 9 || m == 11) {
                    monthEnd = 30;
                }
                String fieldName="";
                for (int d = monthStart; d <= monthEnd; d++) {
                    fieldName = "m"+m+"D"+d;
                    try {
                        setValueInEntity(this, fieldName, getValueFromEntity(this, fieldName).toString().replace("A", "NA"));
                    } catch (Exception ex) {
                    }
                }                
            }
        }


    }

    @Override
    protected void PreUpdate() {
        super.PreUpdate();
        int startDay = 1;
        int startMonth = 1;

        if (startDay > 0 && startMonth > 0) {
            for (int m = startMonth; m <= 12; m++) {
                int monthEnd = 0;
                int monthStart = 1;                               
                if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) {
                    monthEnd = 31;
                } else if (m == 2) {
                    if (Integer.parseInt(getPlannerYear())
                         % 4 == 0) {
                        monthEnd = 29;
                    } else {
                        monthEnd = 28;
                    }
                } else if (m == 4 || m == 6 || m == 9 || m == 11) {
                    monthEnd = 30;
                }
                String fieldName="";
                for (int d = monthStart; d <= monthEnd; d++) {
                    fieldName = "m"+m+"D"+d;
                    try {
                        setValueInEntity(this, fieldName, getValueFromEntity(this, fieldName).toString().replace("NA", "A"));
                    } catch (Exception ex) {
                    }
                }                
            }
        }
    }
}
