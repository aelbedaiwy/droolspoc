/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.competency;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;


/**
 *
 * @author abayomy
 */
//@Entity

@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@MappedSuperclass
public class CompetencyBase  extends BusinessObjectBaseEntity  {
   // <editor-fold defaultstate="collapsed" desc="custPositiveIndicators">
    @Column(length=1000)
    private String custPositiveIndicators;
    public String getCustPositiveIndicators() {
        return custPositiveIndicators;
    }

    public void setCustPositiveIndicators(String custPositiveIndicators) {
        this.custPositiveIndicators = custPositiveIndicators;
    }
    public String getCustPositiveIndicatorsDD()
    {    return "CompetencyBase_custPositiveIndicators";    }
// </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="custNigativeIndicators">
    @Column(length=1000)
    private String custNigativeIndicators;
    public String getCustNigativeIndicators() {
        return custNigativeIndicators;
    }
    public void setCustNigativeIndicators(String custNigativeIndicators) {
        this.custNigativeIndicators = custNigativeIndicators;
    }
    public String getCustNigativeIndicatorsDD()
    {
            return "CompetencyBase_custNigativeIndicators";
    }
// </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="competencyGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CompetencyGroup competencyGroup;
    public CompetencyGroup getCompetencyGroup() {
        return competencyGroup;
    }
    public String getCompetencyGroupDD() {
        return "CompetencyBase_competencyGroup";
    }
    public void setCompetencyGroup(CompetencyGroup competencyGroup) {
        this.competencyGroup = competencyGroup;
    }
// </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false)
    @Translatable(translationField="nameTranslated")
    private String name;
    public String getName() {
        return name;
    }
    public String getNameTranslatedDD() {
        return "CompetencyBase_nameTranslated";
    }
    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
// </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    public String getDescription() {
        return description;
    }
    public String getDescriptionTranslatedDD() {
        return "CompetencyBase_description";
    }
    public void setDescription(String description) {
        this.description = description;
    }
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="levels">

    @OneToMany(mappedBy="competency")
    private List<CompetencyLevel> levels;
     public List<CompetencyLevel> getLevels() {
        return levels;
    }
    public String getLevelsDD() {
        return "CompetencyBase_levels";
    }
    public void setLevels(List<CompetencyLevel> levels) {
        this.levels = levels;
    }
   // </editor-fold>
  

}
