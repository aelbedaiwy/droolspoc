/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.position.*;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ashienawy
 */


@MappedSuperclass
public class EmployeePositionBase extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position ;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    public String getPositionDD() {
        return "EmployeePositionBase_position";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fromDate">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date fromDate;

        public Date getFromDate() {
            return fromDate;
        }

        public void setFromDate(Date fromDate) {
            this.fromDate = fromDate;
        }

         public String getFromDateDD() {
            return "EmployeePositionBase_fromDate";
        }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
        @Column
        @Temporal(TemporalType.TIMESTAMP)
        private Date toDate;

        public Date getToDate() {
            return toDate;
        }

        public void setToDate(Date toDate) {
            this.toDate = toDate;
        }

         public String getToDateDD() {
            return "EmployeePositionBase_toDate";
        }

    // </editor-fold>
    

  
}
