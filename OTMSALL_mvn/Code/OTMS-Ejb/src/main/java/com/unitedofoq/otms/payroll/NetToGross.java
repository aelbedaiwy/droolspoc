/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields={"company"})
public class NetToGross extends BaseEntity{
    

    // <editor-fold defaultstate="collapsed" desc="NET">
    @Column(precision=18, scale=3)
    private BigDecimal net;

    public BigDecimal getNet() {
        return net;
    }

    public void setNet(BigDecimal net) {
        this.net = net;
    }

    public String getNetDD() {
        return "NetToGross_net";
    }
    @Transient
    private BigDecimal netMask;

    public BigDecimal getNetMask() {
        netMask = net;
        return netMask;
    }
    
    public String getNetMaskDD() {
        return "NetToGross_netMask";
    }

    public void setNetMask(BigDecimal netMask) {
        updateDecimalValue("net", netMask);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Gross">
     @Column(precision=18, scale=3)
    private BigDecimal gross;

    public BigDecimal getGross() {
        return gross;
    }

    public void setGross(BigDecimal gross) {
        this.gross = gross;
    }
    public String getGrossDD() {
        return "NetToGross_gross";
    }
    
    @Transient
    private BigDecimal grossMask;

    public BigDecimal getGrossMask() {
        grossMask = gross;
        return grossMask;
    }
    
    public String getGrossMaskDD() {
        return "NetToGross_netMask";
    }

    public void setGrossMask(BigDecimal grossMask) {
        updateDecimalValue("gross", grossMask);
    }
         
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Variance">
     @Column(precision=18, scale=3)
    private BigDecimal variance;

    public BigDecimal getVariance() {
        return variance;
    }
    
    public String getVarianceDD() {
        return "NetToGross_variance";
    }

    public void setVariance(BigDecimal variance) {
        this.variance = variance;
    }
    
    @Transient
    private BigDecimal varianceMask;

    public BigDecimal getVarianceMask() {
        varianceMask = variance;
        return varianceMask;
    }
    
    public String getVarianceMaskDD() {
        return "NetToGross_varianceMask";
    }

    public void setVarianceMask(BigDecimal varianceMask) {
        updateDecimalValue("variance", varianceMask);
    }

    // </editor-fold>
     
    // <editor-fold defaultstate="collapsed" desc="company">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Company company;

    public Company getCompany() {
        return company;
    }
    
    public String getCompanyDD() {
        return "NetToGross_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    // </editor-fold>
}
