package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeTranslation extends RepCompanyLevelBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="address1">
    @Column
    private String address1;

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="religionDescription">
    @Column
    private String religionDescription;

    public void setReligionDescription(String religionDescription) {
        this.religionDescription = religionDescription;
    }

    public String getReligionDescription() {
        return religionDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="address2">
    @Column
    private String address2;

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress2() {
        return address2;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="countryDescription">
    @Column
    private String countryDescription;

    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    public String getCountryDescription() {
        return countryDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="maritalStatusDescription">
    @Column
    private String maritalStatusDescription;

    public void setMaritalStatusDescription(String maritalStatusDescription) {
        this.maritalStatusDescription = maritalStatusDescription;
    }

    public String getMaritalStatusDescription() {
        return maritalStatusDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="cityDescription">
    @Column
    private String cityDescription;

    public void setCityDescription(String cityDescription) {
        this.cityDescription = cityDescription;
    }

    public String getCityDescription() {
        return cityDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="militaryStatusDescription">
    @Column
    private String militaryStatusDescription;

    public void setMilitaryStatusDescription(String militaryStatusDescription) {
        this.militaryStatusDescription = militaryStatusDescription;
    }

    public String getMilitaryStatusDescription() {
        return militaryStatusDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    private String genderDescription;

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescription() {
        return genderDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nationaltyDescription">
    @Column
    private String nationaltyDescription;

    public void setNationaltyDescription(String nationaltyDescription) {
        this.nationaltyDescription = nationaltyDescription;
    }

    public String getNationaltyDescription() {
        return nationaltyDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column
    private String employeeActive;

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String getEmployeeActive() {
        return employeeActive;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    @Column
    private String costCenterGroupDescription;

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="legalEntityDescription">
    @Column
    private String legalEntityDescription;

    public void setLegalEntityDescription(String legalEntityDescription) {
        this.legalEntityDescription = legalEntityDescription;
    }

    public String getLegalEntityDescription() {
        return legalEntityDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeStepDescription">
    @Column
    private String payGradeStepDescription;

    public void setPayGradeStepDescription(String payGradeStepDescription) {
        this.payGradeStepDescription = payGradeStepDescription;
    }

    public String getPayGradeStepDescription() {
        return payGradeStepDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="taxGroupDescription">
    @Column
    private String taxGroupDescription;

    public void setTaxGroupDescription(String taxGroupDescription) {
        this.taxGroupDescription = taxGroupDescription;
    }

    public String getTaxGroupDescription() {
        return taxGroupDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="insuranceGroupDescription">
    @Column
    private String insuranceGroupDescription;

    public void setInsuranceGroupDescription(String insuranceGroupDescription) {
        this.insuranceGroupDescription = insuranceGroupDescription;
    }

    public String getInsuranceGroupDescription() {
        return insuranceGroupDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    private String firstName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    private String middleName;

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    private String lastName;

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fullName">
    @Column
    private String fullName;

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionName">
    @Column
    private String positionName;

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionName() {
        return positionName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    private String locationDescription;

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescription() {
        return locationDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    @Column
    private String payGradeDescription;

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }

    public String getPayGradeDescription() {
        return payGradeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitName">
    @Column
    private String unitName;

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobName">
    @Column
    private String jobName;

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @Column
    private String hiringType;

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringType() {
        return hiringType;
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="currencyDescription">    
     private String currencyDescription;

    public String getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(String currencyDescription) {
        this.currencyDescription = currencyDescription;
    }
     //</editor-fold> 
       
    //<editor-fold defaultstate="collapsed" desc="leaveReason">
    @Column
    private String leaveReason;

    public String getLeaveReason() {
        return leaveReason;
    }
    
    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }
    //</editor-fold>
}
