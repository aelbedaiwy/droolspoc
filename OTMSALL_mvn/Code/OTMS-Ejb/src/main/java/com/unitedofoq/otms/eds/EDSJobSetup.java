/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds;

import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.eds.foundation.job.EDSJob;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
public class EDSJobSetup extends BaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSJob job;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC jobFamily;

    public String getJobDD()        {   return "EDSJobSetup_job";  }
    public String getJobFamilyDD()  {   return "EDSJobSetup_jobFamily";  }

    public EDSJob getJob() {
        return job;
    }

    public void setJob(EDSJob job) {
        this.job = job;
    }

    public UDC getJobFamily() {
        return jobFamily;
    }

    public void setJobFamily(UDC jobFamily) {
        this.jobFamily = jobFamily;
    }
}
