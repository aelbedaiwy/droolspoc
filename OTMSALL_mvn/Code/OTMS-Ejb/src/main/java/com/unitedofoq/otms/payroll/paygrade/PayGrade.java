package com.unitedofoq.otms.payroll.paygrade;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.health.MedicalCategories;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import com.unitedofoq.otms.payroll.PaymentMethod;
import com.unitedofoq.otms.payroll.salaryelement.GradeSalaryElement;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * The persistent class for the pay_grades database table.
 *
 */
@Entity
@ChildEntity(fields = {"steps", "gradeSalaryElements"})
@ParentEntity(fields = "company")
@DiscriminatorValue(value = "MASTER")
public class PayGrade extends PayGradeBase {
    // <editor-fold defaultstate="collapsed" desc="upValue">

    @Column(precision = 25, scale = 13)
    private BigDecimal upValue;

    public BigDecimal getUpValue() {
        return upValue;
    }

    public void setUpValue(BigDecimal gradeUpVal) {
        this.upValue = gradeUpVal;
    }

    public String getUpValueDD() {
        return "PayGrade_upValue";
    }
    @Transient
    private BigDecimal upValueMask;

    public BigDecimal getUpValueMask() {
        upValueMask = upValue;
        return upValueMask;
    }

    public void setUpValueMask(BigDecimal upValueMask) {
        updateDecimalValue("upValue", upValueMask);
    }

    public String getUpValueMaskDD() {
        return "PayGrade_upValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="netSalary">
    @Column(precision = 25, scale = 13)
    private BigDecimal netSalary;

    public BigDecimal getNetSalary() {
        return netSalary;
    }

    public void setNetSalary(BigDecimal netSalary) {
        this.netSalary = netSalary;
    }

    public String getNetSalaryDD() {
        return "PayGrade_netSalary";
    }
    @Transient
    private BigDecimal netSalaryMask;

    public BigDecimal getNetSalaryMask() {
        return netSalaryMask;
    }

    public void setNetSalaryMask(BigDecimal netSalaryMask) {
        this.netSalary = netSalaryMask;
    }

    public String getNetSalaryMaskDD() {
        return "PayGrade_netSalaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nextGrade">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade nextGrade;

    public PayGrade getNextGrade() {
        return nextGrade;
    }

    public void setNextGrade(PayGrade nextGrade) {
        this.nextGrade = nextGrade;
    }

    public String getNextGradeDD() {
        return "PayGrade_nextGrade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "PayGrade_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private PaymentMethod payMethod;

    public PaymentMethod getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(PaymentMethod payMethod) {
        this.payMethod = payMethod;
    }

    public String getPayMethodDD() {
        return "PayGrade_payMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="stepEqualDist">
    @Column(length = 1)
    private String stepEqualDist;

    public String getStepEqualDist() {
        return stepEqualDist;
    }

    public void setStepEqualDist(String stepEqualDist) {
        this.stepEqualDist = stepEqualDist;
    }

    public String getStepEqualDistDD() {
        return "PayGrade_stepEqualDist";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="stepRateAmount">
    @Column(length = 1)
    private String stepRateAmount;

    public String getStepRateAmount() {
        return stepRateAmount;
    }

    public void setStepRateAmount(String stepRateAmount) {
        this.stepRateAmount = stepRateAmount;
    }

    public String getStepRateAmountDD() {
        return "PayGrade_stepRateAmount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="stepUpVal">
    @Column(precision = 25, scale = 13)
    private BigDecimal stepUpVal;

    public BigDecimal getStepUpVal() {
        return stepUpVal;
    }

    public void setStepUpVal(BigDecimal stepUpVal) {
        this.stepUpVal = stepUpVal;
    }

    public String getStepUpValDD() {
        return "PayGrade_stepUpVal";
    }
    @Transient
    private BigDecimal stepUpValMask;

    public BigDecimal getStepUpValMask() {
        stepUpValMask = stepUpVal;
        return stepUpValMask;
    }

    public void setStepUpValMask(BigDecimal stepUpValMask) {
        updateDecimalValue("stepUpVal", stepUpValMask);
    }

    public String getStepUpValMaskDD() {
        return "PayGrade_stepUpValMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="noOfSteps">
    @Column
    private int noOfSteps; //FIXME: what is this number? Active, Not-Deleted, etc... set in PrePersist

    public int getNoOfSteps() {
        return noOfSteps;
    }

    public void setNoOfSteps(int noOfSteps) {
        this.noOfSteps = noOfSteps;
    }

    public String getNoOfStepsDD() {
        return "PayGrade_noOfSteps";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="steps">
    @OneToMany(mappedBy = "payGrade")
    private List<PayGradeStep> steps;

    public List<PayGradeStep> getSteps() {
        return steps;
    }

    public void setSteps(List<PayGradeStep> steps) {
        this.steps = steps;
    }

    public String getStepsDD() {
        return "PayGrade_steps";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gradeSalaryElements">
    @OneToMany(mappedBy = "payGrade")
    private List<GradeSalaryElement> gradeSalaryElements;

    public List<GradeSalaryElement> getGradeSalaryElements() {
        return gradeSalaryElements;
    }

    public void setGradeSalaryElements(List<GradeSalaryElement> gradeSalaryElements) {
        this.gradeSalaryElements = gradeSalaryElements;
    }

    public String getGradeSalaryElementsDD() {
        return "PayGrade_gradeSalaryElements";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="glAccount">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }

    public String getGlAccountDD() {
        return "PayGrade_glAccount";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="medCategory">
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicalCategories medCategory;

    public MedicalCategories getMedCategory() {
        return medCategory;
    }

    public void setMedCategory(MedicalCategories medCategory) {
        this.medCategory = medCategory;
    }

    public String getMedCategoryDD() {
        return "PayGrade_medCategory";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "PayGrade_legalEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "Nursery_legalEntityGroup";
    }
    //</editor-fold>
}
