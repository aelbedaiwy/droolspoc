/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.investigation;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author lahmed
 */
@Entity
public class InvestigationSetupTranslation extends BaseEntityTranslation {
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
     // </editor-fold>
}