/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.currency.Currency;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "EXCEPTIONTYPE")
public class TrainingExceptionBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="fromDay">
    @Column
    private Short fromDay;

    public Short getFromDay() {
        return fromDay;
    }

    public void setFromDay(Short fromDay) {
        this.fromDay = fromDay;
    }

    public String getFromDayDD() {
        return "TrainingExceptionBase_fromDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromMonth">
    @Column
    private Short fromMonth;

    public Short getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(Short fromMonth) {
        this.fromMonth = fromMonth;
    }

    public String getFromMonthDD() {
        return "TrainingExceptionBase_fromMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromYear">
    @Column
    private Short fromYear;

    public Short getFromYear() {
        return fromYear;
    }

    public void setFromYear(Short fromYear) {
        this.fromYear = fromYear;
    }

    public String getFromYearDD() {
        return "TrainingExceptionBase_fromYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDay">
    @Column
    private Short toDay;

    public Short getToDay() {
        return toDay;
    }

    public void setToDay(Short toDay) {
        this.toDay = toDay;
    }

    public String getToDayDD() {
        return "TrainingExceptionBase_toDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toMonth">
    @Column
    private Short toMonth;

    public Short getToMonth() {
        return toMonth;
    }

    public void setToMonth(Short toMonth) {
        this.toMonth = toMonth;
    }

    public String getToMonthDD() {
        return "TrainingExceptionBase_toMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toYear">
    @Column
    private Short toYear;

    public Short getToYear() {
        return toYear;
    }

    public void setToYear(Short toYear) {
        this.toYear = toYear;
    }

    public String getToYearDD() {
        return "TrainingExceptionBase_toYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDayInWeek">
    @Column
    private Short fromDayInWeek;

    public Short getFromDayInWeek() {
        return fromDayInWeek;
    }

    public void setFromDayInWeek(Short fromDayInWeek) {
        this.fromDayInWeek = fromDayInWeek;
    }

    public String getFromDayInWeekDD() {
        return "TrainingExceptionBase_fromDayInWeek";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDayInWeek">
    @Column
    private Short toDayInWeek;

    public Short getToDayInWeek() {
        return toDayInWeek;
    }

    public void setToDayInWeek(Short toDayInWeek) {
        this.toDayInWeek = toDayInWeek;
    }

    public String getToDayInWeekDD() {
        return "TrainingExceptionBase_toDayInWeek";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromTime;

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public String getFromTimeDD() {
        return "TrainingExceptionBase_fromTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toTime;

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public String getToTimeDD() {
        return "TrainingExceptionBase_toTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cost">
    @Column (precision=25, scale=13)
    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCostDD() {
        return "TrainingExceptionBase_cost";
    }
    @Transient
    private BigDecimal costMask;

    public BigDecimal getCostMask() {
        costMask = cost ;
        return costMask;
    }

    public void setCostMask(BigDecimal costMask) {
        updateDecimalValue("cost", costMask);
    }

    public String getCostMaskDD() {
        return "TrainingExceptionBase_costMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getCurrencyDD() {
        return "TrainingExceptionBase_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC costPeriod;

    public UDC getCostPeriod() {
        return costPeriod;
    }

    public void setCostPeriod(UDC costPeriod) {
        this.costPeriod = costPeriod;
    }

    public String getCostPeriodDD(){
        return "TrainingExceptionBase_costPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="available">
    @Column
    private boolean available;

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getAvailableDD() {
        return "TrainingExceptionBase_available";
    }
    // </editor-fold>
}