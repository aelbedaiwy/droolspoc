/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.job;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.edsbehavior.EDSBehavior;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="job")
public class EDSJobBehavior extends BaseEntity{
    @Column(nullable=false)
    private double score;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSJob job;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSBehavior behavior;

    public String getScoreDD(){ return "EDSJobBehavior_score";  }

    public EDSBehavior getBehavior() {
        return behavior;
    }

    public void setBehavior(EDSBehavior behavior) {
        this.behavior = behavior;
    }

    public EDSJob getJob() {
        return job;
    }

    public void setJob(EDSJob job) {
        this.job = job;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
