/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.job;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Value;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"job"})
public class JobValue extends BusinessObjectBaseEntity {
   // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Job job;

    public Job getJob() {
        return job;
    }
    public String getJobDD() {
        return "JobValue_job";
    }
    public void setJob(Job job) {
        this.job = job;
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="value">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Value value;

    public Value getValue() {
        return value;
    }
    public String getValueDD() {
        return "JobValue_value";
    }
    public void setValue(Value value) {
        this.value = value;
    }
    // </editor-fold>

        @Column
     private Double score;

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getScoreDD() {
        return "JobValue_score";
    }
}
