package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;

@Entity
@ParentEntity(fields = {"employee"})
public class Others4 extends OthersBase {
}
