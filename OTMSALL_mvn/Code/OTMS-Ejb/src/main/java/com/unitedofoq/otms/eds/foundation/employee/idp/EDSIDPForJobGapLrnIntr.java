/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.training.EDSChannel;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields="employeeCompetencyGap")
@ChildEntity(fields="actions")
public class EDSIDPForJobGapLrnIntr extends BaseEntity {
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeCompetencyGap employeeCompetencyGap;

    public String getEmployeeCompetencyGapDD() {
        return "EDSIDPBusinessOutcomeForJobGapLrnIntr_employeeCompetencyGap";
    }


    public EmployeeCompetencyGap getEmployeeCompetencyGap() {
        return employeeCompetencyGap;
    }

    public void setEmployeeCompetencyGap(EmployeeCompetencyGap employeeCompetencyGap) {
        this.employeeCompetencyGap = employeeCompetencyGap;
    }    

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSIDPBusinessOutcomeForJobGapAAC gapAAC;

    public String getGapAACDD() {
        return "EDSIDPBusinessOutcomeForJobGapLrnIntr_gapAAC";
    }

    public EDSIDPBusinessOutcomeForJobGapAAC getGapAAC() {
        return gapAAC;
    }

    public void setGapAAC(EDSIDPBusinessOutcomeForJobGapAAC gapAAC) {
        this.gapAAC = gapAAC;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private EDSChannel channel;

    public String getChannelDD() {
        return "EDSIDPBusinessOutcomeForJobGapLrnIntr_channel";
    }

    public EDSChannel getChannel() {
        return channel;
    }

    public void setChannel(EDSChannel channel) {
        this.channel = channel;
    }

    private String description;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date completionDate;
    private String achievementComment;

    public String getAchievementCommentDD() {
        return "EDSIDPBusinessOutcomeForJobGapLrnIntr_achievementComment";
    }

    public String getAchievementComment() {
        return achievementComment;
    }

    public void setAchievementComment(String achievementComment) {
        this.achievementComment = achievementComment;
    }

    public String getCompletionDateDD() {
        return "EDSIDPBusinessOutcomeForJobGapLrnIntr_completionDate";
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getDescriptionDD() {
        return "EDSIDPBusinessOutcomeForJobGapLrnIntr_description";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy="jobGapLrnIntr")
    private List<EDSIDPActionForJobGap> actions;

    public List<EDSIDPActionForJobGap> getActions() {
        return actions;
    }

    public void setActions(List<EDSIDPActionForJobGap> actions) {
        this.actions = actions;
    }
}
