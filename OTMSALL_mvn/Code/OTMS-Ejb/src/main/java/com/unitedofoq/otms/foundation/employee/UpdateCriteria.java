package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class UpdateCriteria extends BaseEntity{
  @ManyToOne(fetch = javax.persistence.FetchType.LAZY)  

// <editor-fold defaultstate="collapsed" desc="Update Field">  
    private UDC updateField;

    public UDC getUpdateField() {
        return updateField;
    }

    public void setUpdateField(UDC updateField) {
        this.updateField = updateField;
    }
    
    public String getUpdateFieldDD() {
        return "UpdateCriteria_updateField";
    }
// </editor-fold>




}
