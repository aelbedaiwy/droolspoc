package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.otms.personnel.dayoff.EmployeeDayOff;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class EmployeeVacationBase extends EmployeeDayOff {
    // <editor-fold defaultstate="collapsed" desc="lastYearBalance">

    @Column
    private Short lastYearBalance;//name = "last_year_balance"

    public Short getLastYearBalance() {
        return lastYearBalance;
    }

    public void setLastYearBalance(Short lastYearBalance) {
        this.lastYearBalance = lastYearBalance;
    }

    public String getLastYearBalanceDD() {
        return "EmployeeVacationBase_lastYearBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="posted">
    @Column(precision = 18, scale = 3)
    private BigDecimal posted;//name = "posted_vacation"

    public BigDecimal getPosted() {
        return posted;
    }

    public void setPosted(BigDecimal posted) {
        this.posted = posted;
    }

    public String getPostedDD() {
        return "EmployeeVacationBase_posted";
    }
    @Transient
    private BigDecimal postedMask;

    public BigDecimal getPostedMask() {
        postedMask = posted;
        return postedMask;
    }

    public void setPostedMask(BigDecimal postedMask) {
        updateDecimalValue("postedMask", postedMask);
    }

    public String getPostedMaskDD() {
        return "EmployeeVacationBase_postedMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastPostedDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPostedDate;//name = "last_posted_date"

    public Date getLastPostedDate() {
        return lastPostedDate;
    }

    public void setLastPostedDate(Date lastPostedDate) {
        this.lastPostedDate = lastPostedDate;
    }

    public String getLastPostedDateDD() {
        return "EmployeeVacationBase_lastPostedDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal initialBalance;

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public String getInitialBalanceDD() {
        return "EmployeeVacationBase_initialBalance";
    }
    @Transient
    private BigDecimal initialBalanceMask;

    public BigDecimal getInitialBalanceMask() {
        initialBalanceMask = initialBalance;
        return initialBalanceMask;
    }

    public void setInitialBalanceMask(BigDecimal initialBalanceMask) {
        updateDecimalValue("initialBalance", initialBalanceMask);
    }

    public String getInitialBalanceMaskDD() {
        return "EmployeeVacationBase_initialBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal currentBalance;

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getCurrentBalanceDD() {
        return "EmployeeVacationBase_currentBalance";
    }
    @Transient
    private BigDecimal currentBalanceMask;

    public BigDecimal getCurrentBalanceMask() {
        currentBalanceMask = currentBalance;
        return currentBalanceMask;
    }

    public void setCurrentBalanceMask(BigDecimal currentBalanceMask) {
        updateDecimalValue("currentBalance", currentBalanceMask);
    }

    public String getCurrentBalanceMaskDD() {
        return "EmployeeVacationBase_currentBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postedBalance">
    @Column(precision = 18, scale = 3)
    private BigDecimal postedBalance;

    public BigDecimal getPostedBalance() {
        return postedBalance;
    }

    public void setPostedBalance(BigDecimal postedBalance) {
        this.postedBalance = postedBalance;
    }

    public String getPostedBalanceDD() {
        return "EmployeeVacationBase_postedBalance";
    }
    @Transient
    private BigDecimal postedBalanceMask;

    public BigDecimal getPostedBalanceMask() {
        postedBalanceMask = postedBalance;
        return postedBalanceMask;
    }

    public void setPostedBalanceMask(BigDecimal postedBalanceMask) {
        updateDecimalValue("postedBalance", postedBalanceMask);
    }

    public String getPostedBalanceMaskDD() {
        return "EmployeeVacationBase_postedBalanceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationTaken">
    @Column(precision = 18, scale = 3)
    private BigDecimal vacationTaken;

    public BigDecimal getVacationTaken() {
        return vacationTaken;
    }

    public void setVacationTaken(BigDecimal vacationTaken) {
        this.vacationTaken = vacationTaken;
    }

    public String getVacationTakenDD() {
        return "EmployeeVacationBase_vacationTaken";
    }
    @Transient
    private BigDecimal vacationTakenMask;

    public BigDecimal getVacationTakenMask() {
        vacationTakenMask = vacationTaken;
        return vacationTakenMask;
    }

    public void setVacationTakenMask(BigDecimal vacationTakenMask) {
        updateDecimalValue("vacationTaken", vacationTakenMask);
    }

    public String getVacationTakenMaskDD() {
        return "EmployeeVacationBase_vacationTakenMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postedProcess">
    @Column(precision = 18, scale = 3)
    private BigDecimal postedProcess;

    public BigDecimal getPostedProcess() {
        return postedProcess;
    }

    public String getPostedProcessDD() {
        return "EmployeeVacationBase_postedProcess";
    }

    public void setPostedProcess(BigDecimal postedProcess) {
        this.postedProcess = postedProcess;
    }
    // </editor-fold>
}
