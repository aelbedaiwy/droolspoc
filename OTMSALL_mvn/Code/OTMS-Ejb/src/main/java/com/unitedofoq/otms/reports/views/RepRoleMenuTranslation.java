
package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class RepRoleMenuTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="menu1">
    @Column
    private String menu1;

    public void setMenu1(String menu1) {
        this.menu1 = menu1;
    }

    public String getMenu1() {
        return menu1;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="menu2">
    @Column
    private String menu2;

    public void setMenu2(String menu2) {
        this.menu2 = menu2;
    }

    public String getMenu2() {
        return menu2;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="menu3">
    @Column
    private String menu3;

    public void setMenu3(String menu3) {
        this.menu3 = menu3;
    }

    public String getMenu3() {
        return menu3;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="functionName">
    @Column
    private String functionName;

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionName() {
        return functionName;
    }
    // </editor-fold>

}
