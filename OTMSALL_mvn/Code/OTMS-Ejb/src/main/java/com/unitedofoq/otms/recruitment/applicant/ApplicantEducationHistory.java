package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantEducationHistory extends PersonnelEducationBase
{
    // <editor-fold defaultstate="collapsed" desc="Applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Applicant applicant;
    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    public String getApplicantDD() {
        return "ApplicantEducationHistory_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="degreeLevel">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC degreeLevel; // High School - Masters - Bachelor etc.

    public UDC getDegreeLevel() {
        return degreeLevel;
    }

    public void setDegreeLevel(UDC degreeLevel) {
        this.degreeLevel = degreeLevel;
    }

    public String getDegreeLevelDD() {
        return "ApplicantEducationHistory_degreeLevel";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="degreeTakenIn">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC degreeTakenIn; // Can be faculty or academy (Arts - Computer Science - etc.)

    public UDC getDegreeTakenIn() {
        return degreeTakenIn;
    }

    public void setDegreeTakenIn(UDC degreeTakenIn) {
        this.degreeTakenIn = degreeTakenIn;
    }

    public String getDegreeTakenInDD() {
        return "ApplicantEducationHistory_degreeTakenIn";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="instituteOrUniv">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC instituteOrUniv; // Institution or university name

    public UDC getInstituteOrUniv() {
        return instituteOrUniv;
    }

    public void setInstituteOrUniv(UDC instituteOrUniv) {
        this.instituteOrUniv = instituteOrUniv;
    }

    public String getInstituteOrUnivDD() {
        return "ApplicantEducationHistory_instituteOrUniv";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldOfStudy">
    private String fieldOfStudy; // Major

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public String getFieldOfStudyDD() {
        return "ApplicantEducationHistory_fieldOfStudy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="studiedSubjects">
    private String studiedSubjects; // Free text to add the studied subjects

    public String getStudiedSubjects() {
        return studiedSubjects;
    }

    public void setStudiedSubjects(String studiedSubjects) {
        this.studiedSubjects = studiedSubjects;
    }

    public String getStudiedSubjectsDD() {
        return "ApplicantEducationHistory_studiedSubjects";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="grade">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC grade; // A - B- C - etc. such that each of them is a range of %

    public UDC getGrade() {
        return grade;
    }

    public void setGrade(UDC grade) {
        this.grade = grade;
    }

    public String getGradeDD() {
        return "ApplicantEducationHistory_grade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certificateType">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC certificateType;// in case of high school entered as degree only

    public UDC getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(UDC certificateType) {
        this.certificateType = certificateType;
    }

    public String getCertificateTypeDD() {
        return "ApplicantEducationHistory_certificateType";
    }
    // </editor-fold>
}
