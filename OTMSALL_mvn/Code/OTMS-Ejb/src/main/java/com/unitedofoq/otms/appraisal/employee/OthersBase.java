package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class OthersBase extends EmployeeKPA {
    //<editor-fold defaultstate="collapsed" desc="description">

    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "OthersBase_description";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "OthersBase_description";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "OthersBase_code";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="gradingSchema">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private ScaleType gradingSchema;

    public ScaleType getGradingSchema() {
        return gradingSchema;
    }

    public void setGradingSchema(ScaleType gradingSchema) {
        this.gradingSchema = gradingSchema;
    }

    public String getGradingSchemaDD() {
        return "OthersBase_gradingSchema";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    @Override
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "OthersBase_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="description2">
    private String description2;

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getDescription2DD() {
        return "OthersBase_description2";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="description3">
    private String description3;

    public String getDescription3() {
        return description3;
    }

    public void setDescription3(String description3) {
        this.description3 = description3;
    }

    public String getDescription3DD() {
        return "OthersBase_description3";
    }
    //</editor-fold>
    // for recruitment:
    // =================
    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "OthersBase_applicant";
    }
    //</editor-fold>
}
