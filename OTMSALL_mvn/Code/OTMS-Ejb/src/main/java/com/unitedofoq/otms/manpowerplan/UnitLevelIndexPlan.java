
package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.*;

@Entity
@Table(name="UnitLevelIndex")
public class UnitLevelIndexPlan extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "UnitLevelIndexPlan_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="company_dbid">
    @Column
    private Long company_dbid;

    public void setCompany_dbid(Long company_dbid) {
        this.company_dbid = company_dbid;
    }

    public Long getCompany_dbid() {
        return company_dbid;
    }

    public String getCompany_dbidDD() {
        return "UnitLevelIndexPlan_company_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="levelIndex">
    @Column
    private int levelIndex;

    public void setLevelIndex(int levelIndex) {
        this.levelIndex = levelIndex;
    }

    public int getLevelIndex() {
        return levelIndex;
    }

    public String getLevelIndexDD() {
        return "UnitLevelIndexPlan_levelIndex";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="originalLevelIndex_dbid">
    @Column
    private Long originalLevelIndex_dbid;

    public void setOriginalLevelIndex_dbid(Long originalLevelIndex_dbid) {
        this.originalLevelIndex_dbid = originalLevelIndex_dbid;
    }

    public Long getOriginalLevelIndex_dbid() {
        return originalLevelIndex_dbid;
    }

    public String getOriginalLevelIndex_dbidDD() {
        return "UnitLevelIndexPlan_originalLevelIndex_dbid";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="DTYPE">
    private String DTYPE = "MASTER";

    public String getDTYPE() {
        return DTYPE;
    }

    public void setDTYPE(String DTYPE) {
        this.DTYPE = DTYPE;
    }
    
    public String getDTYPEDD() {
        return "UnitLevelIndexPlan_DTYPE";
    }
    //</editor-fold>

}
