/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepAppraisalSummary extends RepEmployeeBase {
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepAppraisalSummary_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="purpose">
    @Column
    private String purpose;

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getPurposeDD() {
        return "RepAppraisalSummary_purpose";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    @Translatable(translationField = "templateTranslated")
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public String getTemplateDD() {
        return "RepAppraisalSummary_template";
    }
    
    @Transient
    @Translation(originalField = "template")
    private String templateTranslated;

    public String getTemplateTranslated() {
        return templateTranslated;
    }

    public void setTemplateTranslated(String templateTranslated) {
        this.templateTranslated = templateTranslated;
    }
    
    public String getTemplateTranslatedDD() {
        return "RepAppraisalSummary_template";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="element">
    @Translatable(translationField = "elementTranslated")
    private String element;

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }
    
    public String getElementDD() {
        return "RepAppraisalSummary_element";
    }
    
    @Transient
    @Translation(originalField = "element")
    private String elementTranslated;

    public String getElementTranslated() {
        return elementTranslated;
    }

    public void setElementTranslated(String elementTranslated) {
        this.elementTranslated = elementTranslated;
    }
    
    public String getElementTranslatedDD() {
        return "RepAppraisalSummary_element";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="weight">
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
    
    public String getWeightDD() {
        return "RepAppraisalSummary_weight";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="totalScore">
    @Column
    private BigDecimal totalScore;

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public String getTotalScoreDD() {
        return "RepAppraisalSummary_totalScore";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="averageScore">
    @Column
    private BigDecimal averageScore;

    public void setAverageScore(BigDecimal averageScore) {
        this.averageScore = averageScore;
    }

    public BigDecimal getAverageScore() {
        return averageScore;
    }

    public String getAverageScoreDD() {
        return "RepAppraisalSummary_averageScore";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="previousYearScore">
    @Column
    private BigDecimal previousYearScore;

    public BigDecimal getPreviousYearScore() {
        return previousYearScore;
    }

    public void setPreviousYearScore(BigDecimal previousYearScore) {
        this.previousYearScore = previousYearScore;
    }
    
    public String getPreviousYearScoreDD() {
        return "RepAppraisalSummary_previousYearScore";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="beforePreviousTotalScore">
    private String beforePreviousTotalScore;

    public String getBeforePreviousTotalScore() {
        return beforePreviousTotalScore;
    }

    public void setBeforePreviousTotalScore(String beforePreviousTotalScore) {
        this.beforePreviousTotalScore = beforePreviousTotalScore;
    }
    
    public String getBeforePreviousTotalScoreDD() {
        return "RepAppraisalSummary_beforePreviousTotalScore";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="currentSalary">
    private BigDecimal currentSalary;

    public BigDecimal getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(BigDecimal currentSalary) {
        this.currentSalary = currentSalary;
    }
    
    public String getCurrentSalaryDD() {
        return "RepAppraisalSummary_currentSalary";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="suggestedIncrease">
    private BigDecimal suggestedIncrease;

    public BigDecimal getSuggestedIncrease() {
        return suggestedIncrease;
    }

    public void setSuggestedIncrease(BigDecimal suggestedIncrease) {
        this.suggestedIncrease = suggestedIncrease;
    }
    
    public String getSuggestedIncreaseDD() {
        return "RepAppraisalSummary_suggestedIncrease";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="newSuggestedSalary">
    private BigDecimal newSuggestedSalary;

    public BigDecimal getNewSuggestedSalary() {
        return newSuggestedSalary;
    }

    public void setNewSuggestedSalary(BigDecimal newSuggestedSalary) {
        this.newSuggestedSalary = newSuggestedSalary;
    }
    
    public String getNewSuggestedSalaryDD() {
        return "RepAppraisalSummary_newSuggestedSalary";
    }
    //</editor-fold>
}
