/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields = {"calculatedPeriod"})
public class PersonnelEffectonPayroll extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }

    public String getCalculatedPeriodDD() {
        return "PersonnelAffectonPayroll_calculatedPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "PersonnelAffectonPayroll_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "PersonnelAffectonPayroll_salaryElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthlyElementValue">
    @Column
    private BigDecimal monthlyElementValue;

    public BigDecimal getMonthlyElementValue() {
        return monthlyElementValue;
    }

    public void setMonthlyElementValue(BigDecimal monthlyElementValue) {
        this.monthlyElementValue = monthlyElementValue;
    }

    public String getMonthlyElementValueDD() {
        return "PersonnelAffectonPayroll_monthlyElementValue";
    }
    @Transient
    private BigDecimal monthlyElementValueMask;

    public BigDecimal getMonthlyElementValueMask() {
        monthlyElementValueMask = monthlyElementValue;
        return monthlyElementValueMask;
    }

    public void setMonthlyElementValueMask(BigDecimal monthlyElementValueMask) {
        updateDecimalValue("monthlyElementValue", monthlyElementValueMask);
    }

    public String getMonthlyElementValueMaskDD() {
        return "PersonnelAffectonPayroll_monthlyElementValueMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="monthlyElementValueEnc">
    @Column

    private String monthlyElementValueEnc;

    public String getMonthlyElementValueEnc() {
        return monthlyElementValueEnc;
    }

    public void setMonthlyElementValueEnc(String monthlyElementValueEnc) {
        this.monthlyElementValueEnc = monthlyElementValueEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="elementValue">
    @Column
    private BigDecimal elementValue;

    public BigDecimal getElementValue() {
        return elementValue;
    }

    public void setElementValue(BigDecimal elementValue) {
        this.elementValue = elementValue;
    }

    public String getElementValueDD() {
        return "PersonnelAffectonPayroll_elementValue";
    }
    @Transient
    private BigDecimal elementValueMask;

    public BigDecimal getElementValueMask() {
        elementValueMask = elementValue;
        return elementValueMask;
    }

    public void setElementValueMask(BigDecimal elementValueMask) {
        updateDecimalValue("elementValue", elementValueMask);
    }

    public String getElementValueMaskDD() {
        return "PersonnelAffectonPayroll_elementValueMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="elementValueEnc">
    @Column

    private String elementValueEnc;

    public String getElementValueEnc() {
        return elementValueEnc;
    }

    public void setElementValueEnc(String elementValueEnc) {
        this.elementValueEnc = elementValueEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="pElementValue">
    @Column
    private BigDecimal pElementValue;

    public BigDecimal getPElementValue() {
        return pElementValue;
    }

    public void setPElementValue(BigDecimal pElementValue) {
        this.pElementValue = pElementValue;
    }

    public String getPElementValueDD() {
        return "PersonnelAffectonPayroll_pElementValue";
    }
    @Transient
    private BigDecimal pElementValueMask;

    public BigDecimal getPElementValueMask() {
        pElementValueMask = pElementValue;
        return pElementValueMask;
    }

    public void setPElementValueMask(BigDecimal pElementValueMask) {
        updateDecimalValue("pElementValue", pElementValueMask);
    }

    public String getPElementValueMaskDD() {
        return "PersonnelAffectonPayroll_pElementValueMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="pElementValueEnc">
    @Column

    private String pElementValueEnc;

    public String getpElementValueEnc() {
        return pElementValueEnc;
    }

    public void setpElementValueEnc(String pElementValueEnc) {
        this.pElementValueEnc = pElementValueEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="pMonthlyElementValue">
    @Column
    private BigDecimal pMonthlyElementValue;

    public BigDecimal getPMonthlyElementValue() {
        return pMonthlyElementValue;
    }

    public void setPMonthlyElementValue(BigDecimal pMonthlyElementValue) {
        this.pMonthlyElementValue = pMonthlyElementValue;
    }

    public String getPMonthlyElementValueDD() {
        return "PersonnelAffectonPayroll_pMonthlyElementValue";
    }
    @Transient
    private BigDecimal pMonthlyElementValueMask;

    public BigDecimal getPMonthlyElementValueMask() {
        pMonthlyElementValueMask = pMonthlyElementValue;
        return pMonthlyElementValueMask;
    }

    public void setPMonthlyElementValueMask(BigDecimal pMonthlyElementValueMask) {
        updateDecimalValue("pMonthlyElementValue", pMonthlyElementValueMask);
    }

    public String getPMonthlyElementValueMaskDD() {
        return "PersonnelAffectonPayroll_pMonthlyElementValueMask";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="pMonthlyElementValueEnc">
    @Column

    private String pMonthlyElementValueEnc;

    public String getpMonthlyElementValueEnc() {
        return pMonthlyElementValueEnc;
    }

    public void setpMonthlyElementValueEnc(String pMonthlyElementValueEnc) {
        this.pMonthlyElementValueEnc = pMonthlyElementValueEnc;
    }
    // </editor-fold>
}
