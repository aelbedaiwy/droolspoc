/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll.salaryelement;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"salaryElement"})
public class SalaryElementAffect extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
     public String getSalaryElementDD() {
        return "SalaryElementAffect_salaryElement";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectingsalaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private SalaryElement affectingsalaryElement;

    public SalaryElement getAffectingsalaryElement() {
        return affectingsalaryElement;
    }

    public void setAffectingsalaryElement(SalaryElement affectingsalaryElement) {
        this.affectingsalaryElement = affectingsalaryElement;
    }
     public String getAffectingsalaryElementDD() {
        return "SalaryElementAffect_affectingsalaryElement";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectinPercent">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal affectinPercent;

    public BigDecimal getAffectinPercent() {
        return affectinPercent;
    }

    public void setAffectinPercent(BigDecimal affectinPercent) {
        this.affectinPercent = affectinPercent;
    }
    public String getAffectinPercentDD() {
        return "SalaryElementAffect_affectinPercent";
    }
    @Transient
    private BigDecimal affectinPercentMask;

    public BigDecimal getAffectinPercentMask() {
        affectinPercentMask = affectinPercent;
        return affectinPercentMask;
    }

    public void setAffectinPercentMask(BigDecimal affectinPercentMask) {
        updateDecimalValue("affectinPercent",affectinPercentMask);
    }
    public String getAffectinPercentMaskDD() {
        return "SalaryElementAffect_affectinPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectinMaximumValue">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal affectinMaximumValue;

    public BigDecimal getAffectinMaximumValue() {
        return affectinMaximumValue;
    }

    public void setAffectinMaximumValue(BigDecimal affectinMaximumValue) {
        this.affectinMaximumValue = affectinMaximumValue;
    }
    public String getAffectinMaximumValueDD() {
        return "SalaryElementAffect_affectinMaximumValue";
    }
    @Transient
    private BigDecimal affectinMaximumValueMask;

    public BigDecimal getAffectinMaximumValueMask() {
        affectinMaximumValueMask = affectinMaximumValue ;
        return affectinMaximumValueMask;
    }

    public void setAffectinMaximumValueMask(BigDecimal affectinMaximumValueMask) {
        updateDecimalValue("affectinMaximumValue",affectinMaximumValueMask);
    }
     public String getAffectinMaximumValueMaskDD() {
        return "SalaryElementAffect_affectinMaximumValueMask";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectinMinimumValue">
    @Column(precision=25, scale=13)
    private BigDecimal affectinMinimumValue;

    public BigDecimal getAffectinMinimumValue() {
        return affectinMinimumValue;
    }

    public void setAffectinMinimumValue(BigDecimal affectinMinimumValue) {
        this.affectinMinimumValue = affectinMinimumValue;
    }

    public String getAffectinMinimumValueDD() {
        return "SalaryElementAffect_affectinMinimumValue";
    }
    @Transient
    private BigDecimal affectinMinimumValueMask;

    public BigDecimal getAffectinMinimumValueMask() {
        affectinMinimumValueMask = affectinMinimumValue ;
        return affectinMinimumValueMask;
    }

    public void setAffectinMinimumValueMask(BigDecimal affectinMinimumValueMask) {
        updateDecimalValue("affectinMinimumValue",affectinMinimumValueMask);
    }

    public String getAffectinMinimumValueMaskDD() {
        return "SalaryElementAffect_affectinMinimumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElementType">
    @Transient
    String salaryElementType;

    public String getSalaryElementType() {
        return salaryElementType;
    }

    public void setSalaryElementType(String salaryElementType) {
        this.salaryElementType = salaryElementType;
    }
    // </editor-fold>
    @Override
    protected void PostLoad() {
        super.PostLoad();
        if(getAffectingsalaryElement()!=null){
            if(getAffectingsalaryElement() instanceof Deduction){
                salaryElementType = Deduction.class.getSimpleName();
            }else if(getAffectingsalaryElement() instanceof Income){
                salaryElementType = Income.class.getSimpleName();
            }
        }
    }
}
