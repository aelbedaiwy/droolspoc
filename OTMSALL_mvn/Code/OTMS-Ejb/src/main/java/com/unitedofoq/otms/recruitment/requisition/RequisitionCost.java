/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.training.CostItemBase;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"requisition"})
public class RequisitionCost extends CostItemBase {
    // <editor-fold defaultstate="collapsed" desc="requisition">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Requisition requisition;
    public Requisition getRequisition() {
        return requisition;
    }
    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }
      public String getRequisitionDD() {
        return "RequisitionCost_requisition";
    }
         // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costValue">
    @Column
    private BigDecimal costValue;

    public BigDecimal getCostValue() {
        return costValue;
    }

    public void setCostValue(BigDecimal costValue) {
        this.costValue = costValue;
    }
    public String getCostValueDD() {
        return "RequisitionCost_costValue";
    }
    @Transient
    private BigDecimal costValueMask;

    public BigDecimal getCostValueMask() {
        costValueMask = costValue ;
        return costValueMask;
    }

    public void setCostValueMask(BigDecimal costValueMask) {
        updateDecimalValue("costValue", costValueMask);
    }
    public String getCostValueMaskDD() {
        return "RequisitionCost_costValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column(name = "notes")
    private String notes;
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
   public String getNotesDD() {
        return "RequisitionCost_notes";
    }
       // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Currency currency;
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getCurrencyDD() {
        return "RequisitionCost_currency";
    }
    // </editor-fold>
}
