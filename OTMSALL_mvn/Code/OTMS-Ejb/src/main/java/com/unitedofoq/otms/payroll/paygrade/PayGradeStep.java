package com.unitedofoq.otms.payroll.paygrade;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.PaymentMethod;
import javax.persistence.*;

/**
 * 
 */
@Entity
@ParentEntity(fields={"payGrade"})
public class PayGradeStep extends BusinessObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "PayGradeStep_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nextStep">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
	private PayGradeStep nextStep;
    public PayGradeStep getNextStep() {
        return nextStep;
    }
    public void setNextStep(PayGradeStep nextGradeStep) {
        this.nextStep = nextGradeStep;
    }
    public String getNextStepDD() {
        return "PayGradeStep_nextStep";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "PayGradeStep_company";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGrade">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	@JoinColumn(nullable=false)
	private PayGrade payGrade;
    public PayGrade getPayGrade() {
        return payGrade;
    }
    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }
    public String getPayGradeDD() {
        return "PayGradeStep_payGrade";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payMethod">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private PaymentMethod payMethod;
    public PaymentMethod getPayMethod() {
        return payMethod;
    }
    public void setPayMethod(PaymentMethod payMethod) {
        this.payMethod = payMethod;
    }
    public String getPayMethodDD() {
        return "PayGradeStep_payMethod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
	@Column(nullable=false)
	private int sortIndex;
    public int getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "PayGradeStep_sortIndex";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "PayGradeStep_code";
    }
    // </editor-fold>
}