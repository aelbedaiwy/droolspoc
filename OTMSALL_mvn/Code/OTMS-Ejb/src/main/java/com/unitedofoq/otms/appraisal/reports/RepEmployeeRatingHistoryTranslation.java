package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name = "repemprathistranslation")
public class RepEmployeeRatingHistoryTranslation extends RepAppraisalSheetTranslationBase {
}
