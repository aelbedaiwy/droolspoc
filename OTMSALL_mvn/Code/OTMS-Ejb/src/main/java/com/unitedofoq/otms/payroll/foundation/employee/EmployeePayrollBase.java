package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.payroll.Bank;
import com.unitedofoq.otms.payroll.BankBranches;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.insurance.InsuranceGroup;
import com.unitedofoq.otms.payroll.insurance.InsuranceOffices;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.payroll.paygrade.PayGradeStep;
import com.unitedofoq.otms.payroll.tax.TaxGroup;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class EmployeePayrollBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="accountNo">

    @Column //FIXME: what's the difference with accountNumber
    private String accountNo;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountNoDD() {
        return "EmployeePayroll_accountNo";
    }

    private String accountNoEnc;

    public String getAccountNoEnc() {
        return accountNoEnc;
    }

    public void setAccountNoEnc(String accountNoEnc) {
        this.accountNoEnc = accountNoEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="bank">
    @JoinColumn
    @ManyToOne //FIXME: is it FK?
    private Bank bank;

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getBankDD() {
        return "EmployeePayroll_bank";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cashOrBank">
    @Column(length = 1)
    private String cashOrBank;

    public String getCashOrBank() {
        return cashOrBank;
    }

    public void setCashOrBank(String cashOrBank) {
        this.cashOrBank = cashOrBank;
    }

    public String getCashOrBankDD() {
        return "EmployeePayroll_cashOrBank";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterDD() {
        return "EmployeePayroll_costCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentSalary">
    @Column(precision = 25, scale = 13)
    private BigDecimal currentSalary;

    public BigDecimal getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(BigDecimal currentSalary) {
        this.currentSalary = currentSalary;
    }

    public String getCurrentSalaryDD() {
        return "EmployeePayroll_currentSalary";
    }
    @Transient
    private BigDecimal currentSalaryMask;

    public BigDecimal getCurrentSalaryMask() {
        currentSalaryMask = currentSalary;
        return currentSalaryMask;
    }

    public void setCurrentSalaryMask(BigDecimal currentSalaryMask) {
        updateDecimalValue("currentSalary", currentSalaryMask);
    }

    public String getCurrentSalaryMaskDD() {
        return "EmployeePayroll_currentSalaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "EmployeePayroll_legalEntity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade payGrade;

    public PayGrade getPayGrade() {
        return payGrade;
    }

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public String getPayGradeDD() {
        return "EmployeePayroll_payGrade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeStep">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGradeStep payGradeStep;

    public PayGradeStep getPayGradeStep() {
        return payGradeStep;
    }

    public void setPayGradeStep(PayGradeStep payGradeStep) {
        this.payGradeStep = payGradeStep;
    }

    public String getPayGradeStepDD() {
        return "EmployeePayroll_payGradeStep";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TaxGroup taxGroup;

    public TaxGroup getTaxGroup() {
        return taxGroup;
    }

    public void setTaxGroup(TaxGroup taxGroup) {
        this.taxGroup = taxGroup;
    }

    public String getTaxGroupDD() {
        return "EmployeePayroll_taxGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="overtimeEntitled">
    @Column(length = 1)
    private String overtimeEntitled;

    public String getOvertimeEntitled() {
        return overtimeEntitled;
    }

    public void setOvertimeEntitled(String overtimeEntitled) {
        this.overtimeEntitled = overtimeEntitled;
    }

    public String getOvertimeEntitledDD() {
        return "EmployeePayroll_overtimeEntitled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="allowanceEntitled">
    @Column(length = 1)
    private String allowanceEntitled;

    public String getAllowanceEntitled() {
        return allowanceEntitled;
    }

    public void setAllowanceEntitled(String allowanceEntitled) {
        this.allowanceEntitled = allowanceEntitled;
    }

    public String getAllowanceEntitledDD() {
        return "EmployeePayroll_allowanceEntitled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "EmployeePayroll_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="branch">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private BankBranches branch;

    public BankBranches getBranch() {
        return branch;
    }

    public void setBranch(BankBranches branch) {
        this.branch = branch;
    }

    public String getBranchDD() {
        return "EmployeePayroll_branch";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingHours">
    //Salema[Employee New Design]
//    @Column(precision = 25, scale = 13)
//    private BigDecimal workingHours;
//
//    public BigDecimal getWorkingHours() {
//        return workingHours;
//    }
//
//    public void setWorkingHours(BigDecimal workingHours) {
//        this.workingHours = workingHours;
//    }
//
//    public String getWorkingHoursDD() {
//        return "EmployeePayrollBase_workingHours";
//    }
//    @Transient
//    private BigDecimal workingHoursMask;
//
//    public BigDecimal getWorkingHoursMask() {
//        workingHoursMask = workingHours;
//        return workingHoursMask;
//    }
//
//    public void setWorkingHoursMask(BigDecimal workingHoursMask) {
//        updateDecimalValue("workingHours", workingHoursMask);
//    }
//
//    public String getWorkingHoursMaskDD() {
//        return "EmployeePayrollBase_workingHoursMask";
//    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="dayValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal dayValue;

    public BigDecimal getDayValue() {
        return dayValue;
    }

    public void setDayValue(BigDecimal dayValue) {
        this.dayValue = dayValue;
    }

    public String getDayValueDD() {
        return "EmployeePayrollBase_dayValue";
    }
    @Transient
    private BigDecimal dayValueMask;

    public BigDecimal getDayValueMask() {
        dayValueMask = dayValue;
        return dayValueMask;
    }

    public void setDayValueMask(BigDecimal dayValueMask) {
        updateDecimalValue("dayValue", dayValueMask);
    }

    public String getDayValueMaskDD() {
        return "EmployeePayrollBase_dayValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="hourValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal hourValue;

    public BigDecimal getHourValue() {
        return hourValue;
    }

    public void setHourValue(BigDecimal hourValue) {
        this.hourValue = hourValue;
    }

    public String getHourValueDD() {
        return "EmployeePayrollBase_hourValue";
    }
    @Transient
    private BigDecimal hourValueMask;

    public BigDecimal getHourValueMask() {
        hourValueMask = hourValue;
        return hourValueMask;
    }

    public void setHourValueMask(BigDecimal hourValueMask) {
        updateDecimalValue("hourValue", hourValueMask);
    }

    public String getHourValueMaskDD() {
        return "EmployeePayrollBase_hourValueMask";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="basicBasis">
    @Column(precision = 25, scale = 13)
    private BigDecimal basicBasis;

    public BigDecimal getBasicBasis() {
        return basicBasis;
    }

    public void setBasicBasis(BigDecimal basicBasis) {
        this.basicBasis = basicBasis;
    }

    public String getBasicBasisDD() {
        return "EmployeePayrollBase_basicBasis";
    }
    @Transient
    private BigDecimal basicBasisMask;

    public BigDecimal getBasicBasisMask() {
        basicBasisMask = this.getBasicBasis();
        return basicBasisMask;
    }

    public void setBasicBasisMask(BigDecimal basicBasisMask) {
        updateDecimalValue("basicBasis", basicBasisMask);
    }

    public String getBasicBasisMaskDD() {
        return "EmployeePayrollBase_basicBasisMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cls">
    @Column //FIXME: to be UDC
    private String cls;

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getClsDD() {
        return "EmployeePayrollBase_cls";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="removeDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date removeDate;

    public Date getRemoveDate() {
        return removeDate;
    }

    public void setRemoveDate(Date removeDate) {
        this.removeDate = removeDate;
    }

    public String getRemoveDateDD() {
        return "EmployeePayrollBase_removeDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="InsuranceGroup">
    //FIXME: Insurance to InsuranceGroupMember is ManyToOne, should this relation
    // be with the insurance directly? Why is it with the memebr since nothing
    // is special neighter about the group nor about the member for the insurance
    @ManyToOne
    private InsuranceGroup insuranceGroup;

    public InsuranceGroup getInsuranceGroup() {
        return insuranceGroup;
    }

    public void setInsuranceGroup(InsuranceGroup insuranceGroup) {
        this.insuranceGroup = insuranceGroup;
    }

    public String getInsuranceGroupDD() {
        return "EmployeePayrollBase_insuranceGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceoOffice">
    @JoinColumn
    @ManyToOne
    private InsuranceOffices insuranceOffice;

    public InsuranceOffices getInsuranceOffice() {
        return insuranceOffice;
    }

    public void setInsuranceOffice(InsuranceOffices insuranceOffice) {
        this.insuranceOffice = insuranceOffice;
    }

    public String getInsuranceOfficeDD() {
        return "EmployeePayrollBase_insuranceOffice";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "EmployeePayrollBase_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="variableBasis">
    @Column(precision = 25, scale = 13)
    private BigDecimal variableBasis;

    public BigDecimal getVariableBasis() {
        return variableBasis;
    }

    public void setVariableBasis(BigDecimal variableBasis) {
        this.variableBasis = variableBasis;
    }

    public String getVariableBasisDD() {
        return "EmployeePayrollBase_variableBasis";
    }
    @Transient
    private BigDecimal variableBasisMask;

    public BigDecimal getVariableBasisMask() {
        variableBasisMask = this.getVariableBasis();
        return variableBasisMask;
    }

    public void setVariableBasisMask(BigDecimal variableBasisMask) {
        updateDecimalValue("variableBasis", variableBasisMask);
    }

    public String getVariableBasisMaskDD() {
        return "EmployeeInsurance_variableBasisMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="accrualDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date accrualDate;

    public Date getAccrualDate() {
        return accrualDate;
    }

    public void setAccrualDate(Date accrualDate) {
        this.accrualDate = accrualDate;
    }

    public String getAccrualDateDD() {
        return "EmployeePayrollBase_accrualDate";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="companyInsuranceNumber">
    @Column
    private Integer companyInsuranceNumber;

    public Integer getCompanyInsuranceNumber() {
        return companyInsuranceNumber;
    }

    public void setCompanyInsuranceNumber(Integer companyInsuranceNumber) {
        this.companyInsuranceNumber = companyInsuranceNumber;
    }

    public String getCompanyInsuranceNumberDD() {
        return "EmployeePayrollBase_companyInsuranceNumber";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="glAccount">
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }

    public String getGlAccountDD() {
        return "EmployeePayrollBase_glAccount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startSalary">
    @Column(precision = 25, scale = 13)
    private BigDecimal startSalary;

    public BigDecimal getStartSalary() {
        return startSalary;
    }

    public void setStartSalary(BigDecimal startSalary) {
        this.startSalary = startSalary;
    }

    public String getStartSalaryDD() {
        return "EmployeePayrollBase_startSalary";
    }
    @Transient
    private BigDecimal startSalaryMask;

    public BigDecimal getStartSalaryMask() {
        startSalaryMask = startSalary;
        return startSalaryMask;
    }

    public void setStartSalaryMask(BigDecimal startSalaryMask) {
        updateDecimalValue("startSalary", startSalaryMask);
    }

    public String getStartSalaryMaskDD() {
        return "EmployeePayrollBase_startSalaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loanAccountNo">
    @Column
    private String loanAccountNo;

    public String getLoanAccountNo() {
        return loanAccountNo;
    }

    public void setLoanAccountNo(String loanAccountNo) {
        this.loanAccountNo = loanAccountNo;
    }

    public String getLoanAccountNoDD() {
        return "EmployeePayrollBase_loanAccountNo";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="savingBox">
    @Column(precision = 25, scale = 13)
    private BigDecimal savingBox;

    public BigDecimal getSavingBox() {
        return savingBox;
    }

    public void setSavingBox(BigDecimal savingBox) {
        this.savingBox = savingBox;
    }

    public String getSavingBoxDD() {
        return "EmployeePayrollBase_savingBox";
    }
    @Transient
    private BigDecimal savingBoxMask;

    public BigDecimal getSavingBoxMask() {
        savingBoxMask = savingBox;
        return savingBoxMask;
    }

    public void setSavingBoxMask(BigDecimal savingBoxMask) {
        updateDecimalValue("savingBox", savingBoxMask);
    }

    public String getSavingBoxMaskDD() {
        return "EmployeePayrollBase_savingBoxMask";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="insuranceNumber">
    private String insuranceNumber;

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getInsuranceNumberDD() {
        return "EmployeePayrollBase_insuranceNumber";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="suspended">
    @Column
    private boolean suspended;

    public boolean isSuspended() {
        return suspended;
    }

    public String getSuspendedDD() {
        return "EmployeePayrollBase_suspended";
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="tmEntitled">
    // Loubna - Porta TM - 06/02/2014
    @Column(length = 1)
    private String tmEntitled;

    public String getTmEntitled() {
        return tmEntitled;
    }

    public void setTmEntitled(String tmEntitled) {
        this.tmEntitled = tmEntitled;
    }

    public String getTmEntitledDD() {
        return "EmployeePayrollBase_tmEntitled";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="outstandingBalance">
    // payslip for seadrill
    @Column(precision = 25, scale = 13)
    private BigDecimal outstandingBalance;

    public BigDecimal getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(BigDecimal outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public String getOutstandingBalanceDD() {
        return "EmployeePayrollBase_outstandingBalance";
    }
    @Transient
    private BigDecimal outstandingBalanceMask;

    public BigDecimal getOutstandingBalanceMask() {
        outstandingBalanceMask = outstandingBalance;
        return outstandingBalanceMask;
    }

    public void setOutstandingBalanceMask(BigDecimal outstandingBalanceMask) {
        updateDecimalValue("outstandingBalance", outstandingBalanceMask);
    }

    public String getOutstandingBalanceMaskDD() {
        return "EmployeePayrollBase_outstandingBalanceMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="accountno2">
    private String accountNo2;

    public String getAccountNo2() {
        return accountNo2;
    }

    public void setAccountNo2(String accountNo2) {
        this.accountNo2 = accountNo2;
    }

    private String accountNo2Enc;

    public String getAccountNo2Enc() {
        return accountNo2Enc;
    }

    public void setAccountNo2Enc(String accountNo2Enc) {
        this.accountNo2Enc = accountNo2Enc;
    }

    public String getAccountNo2DD() {
        return "EmployeePayrollBase_accountNo2";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="basicBasisEnc">
    private String basicBasisEnc;

    public String getBasicBasisEnc() {
        return basicBasisEnc;
    }

    public void setBasicBasisEnc(String basicBasisEnc) {
        this.basicBasisEnc = basicBasisEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="variableBasisEnc">
    private String variableBasisEnc;

    public String getVariableBasisEnc() {
        return variableBasisEnc;
    }

    public void setVariableBasisEnc(String variableBasisEnc) {
        this.variableBasisEnc = variableBasisEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startSalaryEnc">
    private String startSalaryEnc;

    public String getStartSalaryEnc() {
        return startSalaryEnc;
    }

    public void setStartSalaryEnc(String startSalaryEnc) {
        this.startSalaryEnc = startSalaryEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="currentSalaryEnc">
    private String currentSalaryEnc;

    public String getCurrentSalaryEnc() {
        return currentSalaryEnc;
    }

    public void setCurrentSalaryEnc(String currentSalaryEnc) {
        this.currentSalaryEnc = currentSalaryEnc;
    }
    // </editor-fold >
}