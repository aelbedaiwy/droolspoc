package com.unitedofoq.otms.appraisal.scaletype;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@ParentEntity(fields = {"company"})
@ChildEntity(fields = {"rules"})
public class ScaleType extends GradingSchema {

    // <editor-fold defaultstate="collapsed" desc="rules">
    @OneToMany(mappedBy = "scaleType")
    private List<ScaleTypeRule> rules;

    public List<ScaleTypeRule> getRules() {
        return rules;
    }

    public void setRules(List<ScaleTypeRule> rules) {
        this.rules = rules;
    }

    public String getRulesDD() {
        return "ScaleType_rules";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="internalCode">
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getInternalCodeDD() {
        return "ScaleType_internalCode";
    }
    //</editor-fold>
}
