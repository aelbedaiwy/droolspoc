
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepCourseCostItemTranslation extends BaseEntityTranslation{

    //<editor-fold defaultstate="collapsed" desc="course">
    private String course;

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="provider">
    private String provider;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="courseClass">
    private String courseClass;

    public String getCourseClass() {
        return courseClass;
    }

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }
    //</editor-fold>
}
