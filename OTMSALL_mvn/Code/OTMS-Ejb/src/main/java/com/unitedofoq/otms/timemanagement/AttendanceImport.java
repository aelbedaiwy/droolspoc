
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.*;

@Entity
public class AttendanceImport extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "AttendanceImport_employee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="attendanceDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date attendanceDate;

    public void setAttendanceDate(Date attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public Date getAttendanceDate() {
        return attendanceDate;
    }

    public String getAttendanceDateDD() {
        return "AttendanceImport_attendanceDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeIn">
    @Column
    private String timeIn;

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public String getTimeInDD() {
        return "AttendanceImport_timeIn";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeOut">
    @Column
    private String timeOut;

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public String getTimeOutDD() {
        return "AttendanceImport_timeOut";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="machine">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TimeManagementMachine machine;

    public void setMachine(TimeManagementMachine machine) {
        this.machine = machine;
    }

    public TimeManagementMachine getMachine() {
        return machine;
    }

    public String getMachineDD() {
        return "AttendanceImport_machine";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "AttendanceImport_status";
    }
    // </editor-fold>
}