/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.scheduleevent;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.training.activity.ActivityBase;
import com.unitedofoq.otms.training.activity.Provider;
import com.unitedofoq.otms.training.activity.ProviderActivity;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"providerActivity"})
@ChildEntity(fields={"days","costIems","cancellationRegulations","attendanceRegulations"})
public class ScheduleEvent extends ActivityBase {
    // <editor-fold defaultstate="collapsed" desc="providerActivity">
	@JoinColumn(nullable=false)
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private ProviderActivity providerActivity;

    public ProviderActivity getProviderActivity() {
        return providerActivity;
    }

    public void setProviderActivity(ProviderActivity providerActivity) {
        this.providerActivity = providerActivity;
    }

    public String getProviderActivityDD() {
        return "ScheduleEvent_providerActivity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="eventSequence">
	@Column(nullable=false)
	private int eventSequence;

    public int getEventSequence() {
        return eventSequence;
    }

    public void setEventSequence(int eventSequence) {
        this.eventSequence = eventSequence;
    }

    public String getEventSequenceDD() {
        return "ScheduleEvent_eventSequence";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "ScheduleEvent_provider";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn (nullable=false)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "ScheduleEvent_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDate">
    @Column
    @Temporal( TemporalType.TIMESTAMP)
    private Date fromDate;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDateDD() {
        return "ScheduleEvent_fromDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getToDateDD() {
        return "ScheduleEvent_toDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    
    public String getStartTimeDD() {
        return "ScheduleEvent_startTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getEndTimeDD() {
        return "ScheduleEvent_endTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendeesNumber">
    @Column
    private Integer attendeesNumber;

    public Integer getAttendeesNumber() {
        return attendeesNumber;
    }

    public void setAttendeesNumber(Integer attendeesNumber) {
        this.attendeesNumber = attendeesNumber;
    }
    
    public String getAttendeesNumberDD() {
        return "ScheduleEvent_attendeesNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="topics">
    @Column
    private String topics;

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getTopicsDD() {
        return "ScheduleEvent_topics";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "ScheduleEvent_comments";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="days">
    @OneToMany(mappedBy="scheduleEvent")
    private List<ScheduleDay> days;

    public List<ScheduleDay> getDays() {
        return days;
    }

    public void setDays(List<ScheduleDay> days) {
        this.days = days;
    }

    public String getDaysDD() {
        return "ScheduleEvent_days";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costIems">
    @OneToMany(mappedBy="scheduleEvent")
	private List<ScheduleEventCostItem> costIems;

    public List<ScheduleEventCostItem> getCostIems() {
        return costIems;
    }

    public void setCostIems(List<ScheduleEventCostItem> costIems) {
        this.costIems = costIems;
    }

    public String getCostIemsDD() {
        return "ScheduleEvent_costIems";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancellationRegulations">
    @OneToMany(mappedBy="scheduleEvent")
    private List<ScheduleEventCancellationRegulation> cancellationRegulations;

    public List<ScheduleEventCancellationRegulation> getCancellationRegulations() {
        return cancellationRegulations;
    }

    public void setCancellationRegulations(List<ScheduleEventCancellationRegulation> cancellationRegulations) {
        this.cancellationRegulations = cancellationRegulations;
    }

    public String getCancellationRegulationsDD() {
        return "ScheduleEvent_cancellationRegulations";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendanceRegulations">
    @OneToMany(mappedBy="scheduleEvent")
    private List<ScheduleEventAttendanceRegulation> attendanceRegulations;

    public List<ScheduleEventAttendanceRegulation> getAttendanceRegulations() {
        return attendanceRegulations;
    }

    public void setAttendanceRegulations(List<ScheduleEventAttendanceRegulation> attendanceRegulations) {
        this.attendanceRegulations = attendanceRegulations;
    }

    public String getAttendanceRegulationsDD() {
        return "ScheduleEvent_attendanceRegulations";
    }
    // </editor-fold>
}
