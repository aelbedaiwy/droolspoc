package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.competency.EDSCompetency;
import com.unitedofoq.otms.eds.competency.EDSCompetencyLevel;
import com.unitedofoq.otms.eds.foundation.job.EDSJobCompetency;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields={"assessor"})
public class EDSCompetencyAssessment extends BaseEntity {
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    EDSEmployeeCompetency employeeCompetency;
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    EDSCompetency competency;
    public String getCompetencyDD(){    return "EDSCompetencyAssessment_competency";  }
    public EDSCompetency getCompetency() {
        return competency;
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private EmployeeProfilerAssessor assessor;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSCompetencyLevel competencyLevel;

    public String getAssessorDD() {
        return "EDSCompetencyAssessment_assessor";
    }

    public EmployeeProfilerAssessor getAssessor() {
        return assessor;
    }

    public void setAssessor(EmployeeProfilerAssessor assessor) {
        this.assessor = assessor;
    }

    public String getCompetencyLevelDD() {
        return "EDSCompetencyAssessment_competencyLevel";
    }

    public EDSCompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }

    public void setCompetencyLevel(EDSCompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }
    @Transient
    private EDSJobCompetency jobCompetency;

    public EDSJobCompetency getJobCompetency() {
        if(this.getCompetency()!=null && this.assessor != null &&this.assessor.getEmployeeProfiler().getEmployee() != null &&
                this.assessor.getEmployeeProfiler().getEmployee().getJob()!=null && this.assessor.getEmployeeProfiler().getEmployee().
                getJob().getJobCompetencies()!=null){
            for (int i = 0; i < this.assessor.getEmployeeProfiler().getEmployee().getJob().getJobCompetencies().size(); i++) {
                if(this.getCompetency().getDbid() == this.assessor.getEmployeeProfiler().getEmployee().getJob().
                        getJobCompetencies().get(i).getCompetency().getDbid() &&
                        !this.assessor.getEmployeeProfiler().getEmployee().getJob().getJobCompetencies().get(i).isInActive()){
                	jobCompetency =this.assessor.getEmployeeProfiler().getEmployee().getJob().getJobCompetencies().get(i); 
                    return jobCompetency ;
                }
            }
        }
        return null;
    }

    public String getJobCompetencyDD() {
        return "EDSCompetencyAssessment_jobCompetency";
    }
    public void setJobCompName(EDSJobCompetency jobCompetency) {
        this.jobCompetency = jobCompetency;
    }

    @Transient
    Integer rankGap;

    public Integer getRankGap() {
        if(getJobCompetency()!=null){
           return  getJobCompetency().getCompetencyLevel().getRankValue() - this.getCompetencyLevel().getRankValue();
        }
        return null;
    }

    public String getRankGapDD() {
        return "EDSCompetencyAssessment_rankGap";
    }

    public void setRankGap(Integer rankGap) {
        this.rankGap = rankGap;
    }

}
