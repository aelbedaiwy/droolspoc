/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.jobGroup;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Value;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"jobGroup"})
public class JobGroupValue extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="jobGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private JobGroup jobGroup;

    public JobGroup getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getJobGroupDD() {
        return "JobGroupValue_jobGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="value">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Value value;

    public Value getValue() {
        return value;
    }
    public String getValueDD() {
        return "JobGroupValue_value";
    }
    public void setValue(Value value) {
        this.value = value;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="score">
    @Column
    private Double score;

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getScoreDD() {
        return "JobGroupValue_score";
    }
    // </editor-fold >
}
