package com.unitedofoq.otms.timemanagement.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
public class EmployeeWorkingCalHistory extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeWorkingCalHistory_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentWorkingCal">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TMCalendar currentWorkingCal;

    public void setCurrentWorkingCal(TMCalendar currentWorkingCal) {
        this.currentWorkingCal = currentWorkingCal;
    }

    public TMCalendar getCurrentWorkingCal() {
        return currentWorkingCal;
    }

    public String getCurrentWorkingCalDD() {
        return "EmployeeWorkingCalHistory_currentWorkingCal";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentEffectiveDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date currentEffectiveDate;

    public void setCurrentEffectiveDate(Date currentEffectiveDate) {
        this.currentEffectiveDate = currentEffectiveDate;
    }

    public Date getCurrentEffectiveDate() {
        return currentEffectiveDate;
    }

    public String getCurrentEffectiveDateDD() {
        return "EmployeeWorkingCalHistory_currentEffectiveDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentEffectiveEndDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date currentEffectiveEndDate;

    public void setCurrentEffectiveEndDate(Date currentEffectiveEndDate) {
        this.currentEffectiveEndDate = currentEffectiveEndDate;
    }

    public Date getCurrentEffectiveEndDate() {
        return currentEffectiveEndDate;
    }

    public String getCurrentEffectiveEndDateDD() {
        return "EmployeeWorkingCalHistory_currentEffectiveEndDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="originalCalendar">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TMCalendar originalCalendar;

    public TMCalendar getOriginalCalendar() {
        return originalCalendar;
    }

    public void setOriginalCalendar(TMCalendar originalCalendar) {
        this.originalCalendar = originalCalendar;
    }

    public String getOriginalCalendarDD() {
        return "EmployeeWorkingCalHistory_originalCalendar";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="originalCalSeq">
    private String originalCalSeq;

    public String getOriginalCalSeq() {
        return originalCalSeq;
    }

    public void setOriginalCalSeq(String originalCalSeq) {
        this.originalCalSeq = originalCalSeq;
    }

    public String getOriginalCalSeqDD() {
        return "EmployeeWorkingCalHistory_originalCalSeq";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="changeDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date changeDate;

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public String getChangeDateDD() {
        return "EmployeeWorkingCalHistory_changeDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="who">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee who;

    public void setWho(Employee who) {
        this.who = who;
    }

    public Employee getWho() {
        return who;
    }

    public String getWhoDD() {
        return "EmployeeWorkingCalHistory_who";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rotSequence">
    private String rotSequence;

    public String getRotSequence() {
        return rotSequence;
    }

    public void setRotSequence(String rotSequence) {
        this.rotSequence = rotSequence;
    }

    public String getRotSequenceDD() {
        return "EmployeeWorkingCalHistory_rotSequence";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    private boolean cancelled;

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getCancelledDD() {
        return "EmployeeWorkingCalHistory_cancelled";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="isDone">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "EmployeeWorkingCalHistory_done";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="fromImport">
    private boolean fromImport;

    public boolean isFromImport() {
        return fromImport;
    }

    public String getFromImportDD() {
        return "EmployeeWorkingCalHistory_fromImport";
    }

    public void setFromImport(boolean fromImport) {
        this.fromImport = fromImport;
    }
    //</editor-fold >
    // <editor-fold defaultstate="collapsed" desc="decisionNo">
    private String decisionNo;

    public String getDecisionNo() {
        return decisionNo;
    }

    public String getDecisionNoDD() {
        return "EmployeeWorkingCalHistory_decisionNo";
    }

    public void setDecisionNo(String decisionNo) {
        this.decisionNo = decisionNo;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="rotationShift">
    @ManyToOne(fetch = FetchType.LAZY)
    private NormalWorkingCalendar rotationShift;

    public NormalWorkingCalendar getRotationShift() {
        return rotationShift;
    }

    public void setRotationShift(NormalWorkingCalendar rotationShift) {
        this.rotationShift = rotationShift;
    }

    public String getRotationShiftDD() {
        return "EmployeeWorkingCalHistory_rotationShift";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="shiftSequence">
    private int shiftSequence;

    public int getShiftSequence() {
        return shiftSequence;
    }

    public void setShiftSequence(int shiftSequence) {
        this.shiftSequence = shiftSequence;
    }

    public String getShiftSequenceDD() {
        return "EmployeeWorkingCalHistory_shiftSequence";
    }
    // </editor-fold>

}
