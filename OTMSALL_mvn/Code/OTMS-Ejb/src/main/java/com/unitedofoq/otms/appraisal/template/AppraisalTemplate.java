package com.unitedofoq.otms.appraisal.template;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.MeasurableField;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = "company")
@ChildEntity(fields = {"elements", "sequences"})
public class AppraisalTemplate extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "AppraisalTemplate_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "AppraisalTemplate_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scope">
    //Appraisal Model Or Training Model Or Applicant Model
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC scope;

    public UDC getScope() {
        return scope;
    }

    public void setScope(UDC scope) {
        this.scope = scope;
    }

    public String getScopeDD() {
        return "AppraisalTemplate_scope";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="appraisalType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC appraisalType;

    public UDC getAppraisalType() {
        return appraisalType;
    }

    public void setAppraisalType(UDC appraisalType) {
        this.appraisalType = appraisalType;
    }

    public String getAppraisalTypeDD() {
        return "AppraisalTemplate_appraisalType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculationType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC calculationType;

    public UDC getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(UDC calculationType) {
        this.calculationType = calculationType;
    }

    public String getCalculationTypeDD() {
        return "AppraisalTemplate_calculationType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="feedback">

    private boolean feedback;

    public boolean isFeedback() {
        return feedback;
    }

    public void setFeedback(boolean feedback) {
        this.feedback = feedback;
    }

    public String getFeedbackDD() {
        return "AppraisalTemplate_feedback";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="appraiseeNotification">

    private boolean appraiseeNotification;

    public boolean isAppraiseeNotification() {
        return appraiseeNotification;
    }

    public void setAppraiseeNotification(boolean appraiseeNotification) {
        this.appraiseeNotification = appraiseeNotification;
    }

    public String getAppraiseeNotification() {
        return "AppraisalTemplate_appraiseeNotification";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="appraisersNumber">

    private Long appraisersNumber;

    public Long getAppraisersNumber() {
        return appraisersNumber;
    }

    public void setAppraisersNumber(Long appraisersNumber) {
        this.appraisersNumber = appraisersNumber;
    }

    public String getAppraisersNumberDD() {
        return "AppraisalTemplate_appraisersNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumScore">

    private BigDecimal maximumScore;

    public BigDecimal getMaximumScore() {
        return maximumScore;
    }

    public void setMaximumScore(BigDecimal maximumScore) {
        this.maximumScore = maximumScore;
    }

    public String getMaximumScoreDD() {
        return "AppraisalTemplate_maximumScore";
    }
    @Transient
    private BigDecimal maximumScoreMask;

    public BigDecimal getMaximumScoreMask() {
        maximumScoreMask = maximumScore;
        return maximumScoreMask;
    }

    public void setMaximumScoreMask(BigDecimal maximumScoreMask) {
        updateDecimalValue("maximumScore", maximumScoreMask);
    }

    public String getMaximumScoreMaskDD() {
        return "AppraisalTemplate_maximumScoreMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumScore">

    private BigDecimal minimumScore;

    public BigDecimal getMinimumScore() {
        return minimumScore;
    }

    public void setMinimumScore(BigDecimal minimumScore) {
        this.minimumScore = minimumScore;
    }

    public String getMinimumScoreDD() {
        return "AppraisalTemplate_minimumScore";
    }
    @Transient
    private BigDecimal minimumScoreMask;

    public BigDecimal getMinimumScoreMask() {
        minimumScoreMask = minimumScore;
        return minimumScoreMask;
    }

    public void setMinimumScoreMask(BigDecimal minimumScoreMask) {
        updateDecimalValue("minimumScore", minimumScoreMask);
    }

    public String getMinimumScoreMaskDD() {
        return "AppraisalTemplate_minimumScoreMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scaleType">
    //@Transient
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleType scaleType;

    public ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    public String getScaleTypeDD() {
        return "AppraisalTemplate_scaleType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="templateScale">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC templateScale;

    public UDC getTemplateScale() {
        return templateScale;
    }

    public void setTemplateScale(UDC templateScale) {
        this.templateScale = templateScale;
    }

    public String getTemplateScaleDD() {
        return "AppraisalTemplate_templateScale";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sequences">
    @OneToMany(mappedBy = "appraisalTemplate")
    private List<AppraisalTemplateSequence> sequences;

    public List<AppraisalTemplateSequence> getSequences() {
        return sequences;
    }

    public void setSequences(List<AppraisalTemplateSequence> sequences) {
        this.sequences = sequences;
    }

    public String getSequencesDD() {
        return "AppraisalTemplate_sequences";
    }
    // </editor-fold>

    //____________________
    // <editor-fold defaultstate="collapsed" desc="periodUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC periodUnit;

    public UDC getPeriodUnit() {
        return periodUnit;
    }

    public void setPeriodUnit(UDC periodUnit) {
        this.periodUnit = periodUnit;
    }

    public String getPeriodUnitDD() {
        return "AppraisalTemplate_periodUnit";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="period">
    @Column(name = "AppraisalPeriod")
    @MeasurableField(name = "periodUnit.value")
    private Integer period;

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getPeriodDD() {
        return "AppraisalTemplate_period";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="purpose">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC purpose;

    public UDC getPurpose() {
        return purpose;
    }

    public void setPurpose(UDC purpose) {
        this.purpose = purpose;
    }

    public String getPurposeDD() {
        return "AppraisalTemplate_purpose";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="effectiveDate">

    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveDate;

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEffectiveDateDD() {
        return "AppraisalTemplate_effectiveDate";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="appraisalTemplate">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplate appraisalTemplate;

    public AppraisalTemplate getAppraisalTemplate() {
        return appraisalTemplate;
    }

    public void setAppraisalTemplate(AppraisalTemplate appraisalTemplate) {
        this.appraisalTemplate = appraisalTemplate;
    }

    public String getAppraisalTemplateDD() {
        return "AppraisalTemplate_appraisalTemplate";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="elements">
    @OneToMany(mappedBy = "appraisalTemplate")
    private List<AppraisalElement> elements;

    public List<AppraisalElement> getElements() {
        return elements;
    }

    public void setElements(List<AppraisalElement> elements) {
        this.elements = elements;
    }

    public String getElementsDD() {
        return "AppraisalTemplate_elements";
    }
    //</editor-fold>

    public AppraisalElement getElement(String udcCode) {
        if (elements == null) {
            return null;
        }
        for (AppraisalElement element : elements) {
            UDC elementType = element.getType();
            if (elementType != null) // Note it's not mandatory, could be null
            {
                if (elementType.getCode().equals(udcCode)) {
                    return element;
                }
            }
        }
        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="quantativeElement">
    @Transient
    private AppraisalElement quantativeElement;

    public AppraisalElement getQuantativeElement() {
        quantativeElement = getElement("001");
        return quantativeElement;
    }

    public String getQuantativeElementDD() {
        return "AppraisalTemplate_quantativeElement";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="quantativeElement">
    @Transient
    private AppraisalElement employeeGoal;

    public AppraisalElement getEmployeeGoal() {
        employeeGoal = getElement("0016");
        return employeeGoal;
    }

    public String getEmployeeGoalDD() {
        return "AppraisalTemplate_employeeGoal";
    }

    public void setEmployeeGoal(AppraisalElement employeeGoal) {
        this.employeeGoal = employeeGoal;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="qualitativeElement">
    @Transient
    private AppraisalElement qualitativeElement;

    public AppraisalElement getQualitativeElement() {
        qualitativeElement = getElement("002");
        return qualitativeElement;
    }

    public String getQualitativeElementDD() {
        return "AppraisalTemplate_qualitativeElement";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="competencies">
    @Transient
    private AppraisalElement competencies;

    public AppraisalElement getCompetencies() {
        competencies = getElement("003");
        return competencies;
    }

    public String getCompetenciesDD() {
        return "AppraisalTemplate_competencies";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="objective">
    @Transient
    private AppraisalElement objective;

    public AppraisalElement getObjective() {
        objective = getElement("004");
        return objective;
    }

    public String getObjectiveDD() {
        return "AppraisalTemplate_objective";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="questionaire1">
    @Transient
    private AppraisalElement questionaire1;

    public AppraisalElement getQuestionaire1() {
        questionaire1 = getElement("0010");
        return questionaire1;
    }

    public String getQuestionaire1DD() {
        return "AppraisalTemplate_questionaire1";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="questionaire2">
    @Transient
    private AppraisalElement questionaire2;

    public AppraisalElement getQuestionaire2() {
        questionaire2 = getElement("0011");
        return questionaire2;
    }

    public String getQuestionaire2DD() {
        return "AppraisalTemplate_questionaire2";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="questionaire3">
    @Transient
    private AppraisalElement questionaire3;

    public AppraisalElement getQuestionaire3() {
        questionaire3 = getElement("0012");
        return questionaire3;
    }

    public String getQuestionaire3DD() {
        return "AppraisalTemplate_questionaire3";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="questionaire4">
    @Transient
    private AppraisalElement questionaire4;

    public AppraisalElement getQuestionaire4() {
        questionaire4 = getElement("0013");
        return questionaire4;
    }

    public String getQuestionaire4DD() {
        return "AppraisalTemplate_questionaire4";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="questionaire5">
    @Transient
    private AppraisalElement questionaire5;

    public AppraisalElement getQuestionaire5() {
        questionaire5 = getElement("0014");
        return questionaire5;
    }

    public String getQuestionaire5DD() {
        return "AppraisalTemplate_questionaire5";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="others1">
    @Transient
    private AppraisalElement others1;

    public AppraisalElement getOthers1() {
        others1 = getElement("005");
        return others1;
    }

    public String getOthers1DD() {
        return "AppraisalTemplate_others1";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="others2">
    @Transient
    private AppraisalElement others2;

    public AppraisalElement getOthers2() {
        others2 = getElement("006");
        return others2;
    }

    public String getOthers2DD() {
        return "AppraisalTemplate_others2";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="others3">
    @Transient
    private AppraisalElement others3;

    public AppraisalElement getOthers3() {
        others3 = getElement("007");
        return others3;
    }

    public String getOthers3DD() {
        return "AppraisalTemplate_others3";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="others4">
    @Transient
    private AppraisalElement others4;

    public AppraisalElement getOthers4() {
        others4 = getElement("008");
        return others4;
    }

    public String getOthers4DD() {
        return "AppraisalTemplate_others4";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="others5">
    @Transient
    private AppraisalElement others5;

    public AppraisalElement getOthers5() {
        others5 = getElement("009");
        return others5;
    }

    public String getOthers5DD() {
        return "AppraisalTemplate_others5";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="goal">
    @Transient
    private AppraisalElement goal;

    public AppraisalElement getGoal() {
        goal = getElement("0016");
        return goal;
    }

    public String getGoalDD() {
        return "AppraisalTemplate_goal";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="internalCode">
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getInternalCodeDD() {
        return "AppraisalTemplate_internalCode";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="elementsPercentage">
    private int elementsPercentage;

    public int getElementsPercentage() {
        return elementsPercentage;
    }

    public String getElementsPercentageDD() {
        return "AppraisalTemplate_elementsPercentage";
    }

    public void setElementsPercentage(int elementsPercentage) {
        this.elementsPercentage = elementsPercentage;
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="templatesPercentage">
    private int templatesPercentage;

    public int getTemplatesPercentage() {
        return templatesPercentage;
    }

    public String getTemplatesPercentageDD() {
        return "AppraisalTemplate_templatesPercentage";
    }

    public void setTemplatesPercentage(int templatesPercentage) {
        this.templatesPercentage = templatesPercentage;
    }
    //</editor-fold >

}
