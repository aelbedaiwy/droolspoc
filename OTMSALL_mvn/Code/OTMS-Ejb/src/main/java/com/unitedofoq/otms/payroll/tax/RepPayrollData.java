package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepCompanyLevelBase;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class RepPayrollData extends RepCompanyLevelBase {
    //<editor-fold defaultstate="collapsed" desc="accountNo">

    @Column
    protected String accountNo;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountNoDD() {
        return "RepPayrollData_accountNo";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="netSalaryDescription">
    @Column
    private String netSalaryDescription;

    public String getNetSalaryDescription() {
        return netSalaryDescription;
    }

    public void setNetSalaryDescription(String netSalaryDescription) {
        this.netSalaryDescription = netSalaryDescription;
    }

    public String getNetSalaryDescriptionDD() {
        return "RepPayrollData_netSalaryDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="salarySignDescription">
    @Column
    private String salarySignDescription;

    public String getSalarySignDescription() {
        return salarySignDescription;
    }

    public void setSalarySignDescription(String salarySignDescription) {
        this.salarySignDescription = salarySignDescription;
    }

    public String getSalarySignDescriptionDD() {
        return "RepPayrollData_salarySignDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="bankDescription">
    @Column
    protected String bankDescription;

    public String getBankDescription() {
        return bankDescription;
    }

    public void setBankDescription(String bankDescription) {
        this.bankDescription = bankDescription;
    }

    public String getBankDescriptionDD() {
        return "RepPayrollData_bankDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculatedPeriodCode">
    @Column
    private String calculatedPeriodCode;

    public String getCalculatedPeriodCode() {
        return calculatedPeriodCode;
    }

    public void setCalculatedPeriodCode(String calculatedPeriodCode) {
        this.calculatedPeriodCode = calculatedPeriodCode;
    }

    public String getCalculatedPeriodCodeDD() {
        return "RepPayrollData_calculatedPeriodCode";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceGroupDescription">
    @Column
    @Translatable(translationField = "insuranceGroupDescriptionTranslated")
    private String insuranceGroupDescription;

    public String getInsuranceGroupDescription() {
        return insuranceGroupDescription;
    }

    public void setInsuranceGroupDescription(String insuranceGroupDescription) {
        this.insuranceGroupDescription = insuranceGroupDescription;
    }

    public String getInsuranceGroupDescriptionDD() {
        return "RepPayrollData_insuranceGroupDescription";
    }
    @Transient
    @Translation(originalField = "insuranceGroupDescription")
    private String insuranceGroupDescriptionTranslated;

    public String getInsuranceGroupDescriptionTranslated() {
        return insuranceGroupDescriptionTranslated;
    }

    public void setInsuranceGroupDescriptionTranslated(String insuranceGroupDescriptionTranslated) {
        this.insuranceGroupDescriptionTranslated = insuranceGroupDescriptionTranslated;
    }

    public String getInsuranceGroupDescriptionTranslatedDD() {
        return "RepPayrollData_insuranceGroupDescription";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="insuranceofficesdescription">
    @Column
    protected String insuranceOfficesDescription;

    public String getInsuranceOfficesDescription() {
        return insuranceOfficesDescription;
    }

    public void setInsuranceOfficesDescription(String insuranceOfficesDescription) {
        this.insuranceOfficesDescription = insuranceOfficesDescription;
    }

    public String getInsuranceOfficesDescriptionDD() {
        return "RepPayrollData_insuranceOfficesDescription";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxGroupDescription">
    @Column
    @Translatable(translationField = "taxGroupDescriptionTranslated")
    private String taxGroupDescription;

    public String getTaxGroupDescription() {
        return taxGroupDescription;
    }

    public void setTaxGroupDescription(String taxGroupDescription) {
        this.taxGroupDescription = taxGroupDescription;
    }

    public String getTaxGroupDescriptionDD() {
        return "RepPayrollData_taxGroupDescription";
    }
    @Transient
    @Translation(originalField = "taxGroupDescription")
    private String taxGroupDescriptionTranslated;

    public String getTaxGroupDescriptionTranslated() {
        return taxGroupDescriptionTranslated;
    }

    public void setTaxGroupDescriptionTranslated(String taxGroupDescriptionTranslated) {
        this.taxGroupDescriptionTranslated = taxGroupDescriptionTranslated;
    }

    public String getTaxGroupDescriptionTranslatedDD() {
        return "RepPayrollData_taxGroupDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalEntityDescription">
    @Column
    @Translatable(translationField = "legalEntityDescriptionTranslated")
    private String legalEntityDescription;

    public String getLegalEntityDescription() {
        return legalEntityDescription;
    }

    public void setLegalEntityDescription(String legalEntityDescription) {
        this.legalEntityDescription = legalEntityDescription;
    }

    public String getLegalEntityDescriptionDD() {
        return "RepPayrollData_legalEntityDescription";
    }
    @Transient
    @Translation(originalField = "legalEntityDescription")
    private String legalEntityDescriptionTranslated;

    public String getLegalEntityDescriptionTranslated() {
        return legalEntityDescriptionTranslated;
    }

    public void setLegalEntityDescriptionTranslated(String legalEntityDescriptionTranslated) {
        this.legalEntityDescriptionTranslated = legalEntityDescriptionTranslated;
    }

    public String getLegalEntityDescriptionTranslatedDD() {
        return "RepPayrollData_legalEntityDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    @Column
    @Translatable(translationField = "costCenterGroupDescriptionTranslated")
    private String costCenterGroupDescription;

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }

    public String getCostCenterGroupDescriptionDD() {
        return "RepPayrollData_costCenterGroupDescription";
    }
    @Transient
    @Translation(originalField = "costCenterGroupDescription")
    private String costCenterGroupDescriptionTranslated;

    public String getCostCenterGroupDescriptionTranslated() {
        return costCenterGroupDescriptionTranslated;
    }

    public void setCostCenterGroupDescriptionTranslated(String costCenterGroupDescriptionTranslated) {
        this.costCenterGroupDescriptionTranslated = costCenterGroupDescriptionTranslated;
    }

    public String getCostCenterGroupDescriptionTranslatedDD() {
        return "RepPayrollData_costCenterGroupDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeStepDescription">
    @Column
    @Translatable(translationField = "payGradeStepDescriptionTranslated")
    private String payGradeStepDescription;

    public String getPayGradeStepDescription() {
        return payGradeStepDescription;
    }

    public void setPayGradeStepDescription(String payGradeStepDescription) {
        this.payGradeStepDescription = payGradeStepDescription;
    }

    public String getPayGradeStepDescriptionDD() {
        return "RepPayrollData_payGradeStepDescription";
    }
    @Transient
    @Translation(originalField = "payGradeStepDescription")
    private String payGradeStepDescriptionTranslated;

    public String getPayGradeStepDescriptionTranslated() {
        return payGradeStepDescriptionTranslated;
    }

    public void setPayGradeStepDescriptionTranslated(String payGradeStepDescriptionTranslated) {
        this.payGradeStepDescriptionTranslated = payGradeStepDescriptionTranslated;
    }

    public String getPayGradeStepDescriptionTranslatedDD() {
        return "RepPayrollData_payGradeStepDescription";
    }
    // </editor-fold>
}
