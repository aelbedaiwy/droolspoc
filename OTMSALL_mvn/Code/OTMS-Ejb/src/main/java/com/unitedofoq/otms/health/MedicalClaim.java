/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.EmployeeDependance;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields = {"employee"})
public class MedicalClaim extends BaseEntity {

// <editor-fold defaultstate="collapsed" desc="transactionnumber">    
    private Integer transactionnumber;

    public Integer getTransactionnumber() {
        return transactionnumber;
    }

    public void setTransactionnumber(Integer transactionnumber) {
        this.transactionnumber = transactionnumber;
    }

    public String getTransactionnumberDD() {
        return "MedicalClaim_transactionnumber";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="employee">   
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeMedical employee;

    public EmployeeMedical getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeMedical employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "MedicalClaim_employee";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="beneficiary"> 
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC beneficiary;

    public UDC getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(UDC beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getBeneficiaryDD() {
        return "MedicalClaim_beneficiary";
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="medicalGroup"> 
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC medicalGroup;

    public UDC getMedicalGroup() {
        return medicalGroup;
    }

    public String getMedicalGroupDD() {
        return "MedicalClaim_medicalGroup";
    }

    public void setMedicalGroup(UDC medicalGroup) {
        this.medicalGroup = medicalGroup;
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="medicalTreatment"> 
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private MedicalTreatmentTypes medicalTreatment;

    public MedicalTreatmentTypes getMedicalTreatment() {
        return medicalTreatment;
    }

    public String getMedicalTreatmentDD() {
        return "MedicalClaim_medicalStatus";
    }

    public void setMedicalTreatment(MedicalTreatmentTypes medicalTreatment) {
        this.medicalTreatment = medicalTreatment;
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="cash">    
    private String cash;

    public String getCash() {
        return cash;
    }

    public String getCashDD() {
        return "MedicalClaim_cash";
    }

    public void setCash(String cash) {
        this.cash = cash;
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="medicalStatusCash">    
    private String medicalStatusCash;

    public String getMedicalStatusCash() {
        return medicalStatusCash;
    }

    public String getMedicalStatusCashDD() {
        return "MedicalClaim_medicalStatusCash";
    }

    public void setMedicalStatusCash(String medicalStatusCash) {
        this.medicalStatusCash = medicalStatusCash;
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="amount">    
    @Column(precision = 18, scale = 3)
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public String getAmountDD() {
        return "MedicalClaim_amount";
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    @Transient
    private BigDecimal amountMask;

    public BigDecimal getAmountMask() {
        amountMask = amount;
        return amountMask;
    }

    public String getAmountMaskDD() {
        return "MedicalClaim_amountMask";
    }

    public void setAmountMask(BigDecimal amountMask) {
        updateDecimalValue("amount", amountMask);
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="transactionDate">    
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionDateDD() {
        return "MedicalClaim_transactionDate";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "MedicalClaim_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="dependence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeDependance dependence;

    public EmployeeDependance getDependence() {
        return dependence;
    }

    public void setDependence(EmployeeDependance dependence) {
        this.dependence = dependence;
    }

    public String getDependenceDD() {
        return "MedicalClaim_dependence";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collpased" desc="medicalProvider">
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicalProvider provider;

    public MedicalProvider getMedicalProvider() {
        return provider;
    }

    public void setMedicalProvider(MedicalProvider provider) {
        this.provider = provider;
    }

    public String getMedicalProviderDD() {
        return "MedicalClaim_provider";
    }
    //</editor-fold>
}
