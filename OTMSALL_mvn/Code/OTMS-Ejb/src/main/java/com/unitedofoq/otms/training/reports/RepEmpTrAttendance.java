
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
public class RepEmpTrAttendance extends RepEmployeeBase  {
    
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmpTrAttendance_dsDbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="plannedDaysAtt">
    @Column
    private int plannedDaysAtt;

    public int getPlannedDaysAtt() {
        return plannedDaysAtt;
    }

    public void setPlannedDaysAtt(int plannedDaysAtt) {
        this.plannedDaysAtt = plannedDaysAtt;
    }

    public String getPlannedDaysAttDD() {
        return "RepEmpTrAttendance_plannedDaysAtt";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualDaysAtt">
    @Column
    private int actualDaysAtt;

    public int getActualDaysAtt() {
        return actualDaysAtt;
    }

    public void setActualDaysAtt(int actualDaysAtt) {
        this.actualDaysAtt = actualDaysAtt;
    }

    public String getActualDaysAttDD() {
        return "RepEmpTrAttendance_actualDaysAtt";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }

    public String getCourseClass() {
        return courseClass;
    }

    public String getCourseClassDD() {
        return "RepEmpTrAttendance_courseClass";
    }
    
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;

    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }

    public String getCourseClassTranslatedDD() {
        return "RepEmpTrAttendance_courseClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="course">
    @Column
    @Translatable(translationField = "courseTranslated")
    private String course;

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourse() {
        return course;
    }

    public String getCourseDD() {
        return "RepEmpTrAttendance_course";
    }
    
    @Transient
    @Translation(originalField = "course")
    private String courseTranslated;

    public String getCourseTranslated() {
        return courseTranslated;
    }

    public void setCourseTranslated(String courseTranslated) {
        this.courseTranslated = courseTranslated;
    }

    public String getCourseTranslatedDD() {
        return "RepEmpTrAttendance_courses";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="attendancePercent">
    @Column
    private BigDecimal attendancePercent;

    public void setAttendancePercent(BigDecimal attendancePercent) {
        this.attendancePercent = attendancePercent;
    }

    public BigDecimal getAttendancePercent() {
        return attendancePercent;
    }

    public String getAttendancePercentDD() {
        return "RepEmpTrAttendance_attendancePercent";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="completions">
    @Column
    private String completions;

    public void setCompletions(String completions) {
        this.completions = completions;
    }

    public String getCompletions() {
        return completions;
    }

    public String getCompletionsDD() {
        return "RepEmpTrAttendance_completions";
    }
    // </editor-fold>
}