package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.*;

/**
 * 
 */
@Entity
@ChildEntity(fields={"periods"})
public class PaymentMethod extends BusinessObjectBaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "PaymentMethod_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calcInsuranceEnabled">
	@Column
	private String calcInsuranceEnabled;
    public String getCalcInsuranceEnabled() {
        return calcInsuranceEnabled;
    }
    public void setCalcInsuranceEnabled(String calcInsuranceEnabled) {
        this.calcInsuranceEnabled = calcInsuranceEnabled;
    }
    public String getCalcInsuranceEnabledDD() {
        return "PaymentMethod_calcInsuranceEnabled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calcTaxesEnabled">
	@Column
	private String calcTaxesEnabled;
    public String getCalcTaxesEnabled() {
        return calcTaxesEnabled;
    }
    public void setCalcTaxesEnabled(String calcTaxesEnabled) {
        this.calcTaxesEnabled = calcTaxesEnabled;
    }
    public String getCalcTaxesEnabledDD() {
        return "PaymentMethod_calcTaxesEnabled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "PaymentMethod_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @Column(nullable=false)
    @Translatable(translationField="unitTranslated")
    private String unit;
    public String getUnit() {
        return unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
    @Translation(originalField="unit")
    @Transient
    private String unitTranslated;

    public String getUnitTranslated() {
        return unitTranslated;
    }

    public void setUnitTranslated(String unitTranslated) {
        this.unitTranslated = unitTranslated;
    }

    public String getUnitTranslatedDD() {
        return "PaymentMethod_unitTranslated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="periodsCountPerYear">
	@Column(nullable=false)
	private int periodsCountPerYear;
    public int getPeriodsCountPerYear() {
        return periodsCountPerYear;
    }
    public void setPeriodsCountPerYear(int periodsCountPerYear) {
        this.periodsCountPerYear = periodsCountPerYear;
    }
    public String getPeriodsCountPerYearDD() {
        return "PaymentMethod_periodsCountPerYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="relatedPayMethod">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PaymentMethod relatedPayMethod;
    public PaymentMethod getRelatedPayMethod() {
        return relatedPayMethod;
    }
    public void setRelatedPayMethod(PaymentMethod relatedPayMethod) {
        this.relatedPayMethod = relatedPayMethod;
    }
    public String getRelatedPayMethodDD() {
        return "PaymentMethod_relatedPayMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="relatedPayPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private PaymentPeriod relatedPayPeriod;
    public PaymentPeriod getRelatedPayPeriod() {
        return relatedPayPeriod;
    }
    public void setRelatedPayPeriod(PaymentPeriod relatedPayPeriod) {
        this.relatedPayPeriod = relatedPayPeriod;
    }
    public String getRelatedPayPeriodDD() {
        return "PaymentMethod_relatedPayPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="periods">
    @OneToMany(mappedBy = "paymentMethod")
    private List<PaymentPeriod > periods;

    public List<PaymentPeriod> getPeriods() {
        return periods;
    }

    public void setPeriods(List<PaymentPeriod> periods) {
        this.periods = periods;
    }

    public String getPeriodsDD() {
        return "PaymentMethod_periods";
    }
    // </editor-fold >
}