
package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.*;

@Entity
@Table(name="company")
public class CompanyPlan extends BaseEntity  {

    //<editor-fold defaultstate="collapsed" desc="instance">
    private Long instance;

    public Long getInstance() {
        return instance;
    }

    public void setInstance(Long instance) {
        this.instance = instance;
    }
    
    public String getInstanceDD() {
        return "CompanyPlan_instance";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "CompanyPlan_name";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "CompanyPlan_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="originalCompany_dbid">
    @Column
    private Long originalCompany_dbid;

    public void setOriginalCompany_dbid(Long originalCompany_dbid) {
        this.originalCompany_dbid = originalCompany_dbid;
    }

    public Long getOriginalCompany_dbid() {
        return originalCompany_dbid;
    }

    public String getOriginalCompany_dbidDD() {
        return "CompanyPlan_originalCompany_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="prefix">
    @Column
    private String prefix;

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getPrefixDD() {
        return "CompanyPlan_prefix";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="companyOrPlan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC companyOrPlan;

    public UDC getCompanyOrPlan() {
        return companyOrPlan;
    }

    public void setCompanyOrPlan(UDC companyOrPlan) {
        this.companyOrPlan = companyOrPlan;
    }
    
    public String getCompanyOrPlanDD() {
        return "CompanyPlan_companyOrPlan";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="email">
    @Column
    private String email;
     public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmailDD()      { return "CompanyPlan_email";  }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="CTYPE">
    private String CTYPE = "MASTER";

    public String getCTYPE() {
        return CTYPE;
    }

    public void setCTYPE(String CTYPE) {
        this.CTYPE = CTYPE;
    }
    
    public String getCTYPEDD() {
        return "CompanyPlan_CTYPE";
    }
    //</editor-fold>
}