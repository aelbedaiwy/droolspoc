package com.unitedofoq.otms.payroll.salaryelement;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import com.unitedofoq.otms.payroll.PaymentMethod;
import com.unitedofoq.otms.payroll.PaymentPeriod;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.formula.FormulaDistinctName;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = "company")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "STYPE")
@ChildEntity(fields = {"affect", "formulas"})
public class SalaryElement extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="accountNumber">
    @Column
    private String accountNumber;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumberDD() {
        return "SalaryElement_accountNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activeFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date activeFrom;

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public String getActiveFromDD() {
        return "SalaryElement_activeFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activeTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date activeTo;

    public Date getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(Date activeTo) {
        this.activeTo = activeTo;
    }

    public String getActiveToDD() {
        return "SalaryElement_activeTo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="addExcess">
    @Column(length = 1)
    private String addExcess = "N";

    public String getAddExcess() {
        return addExcess;
    }

    public void setAddExcess(String addExcess) {
        this.addExcess = addExcess;
    }

    public String getAddExcessDD() {
        return "SalaryElement_addExcess";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="adjustMonthly">
    @Column(length = 1)
    private String adjustMonthly = "N";

    public String getAdjustMonthly() {
        return adjustMonthly;
    }

    public void setAdjustMonthly(String adjustMonthly) {
        this.adjustMonthly = adjustMonthly;
    }

    public String getAdjustMonthlyDD() {
        return "SalaryElement_adjustMonthly";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="beforeCalc">
    @Column(length = 1)
    private String beforeCalc = "N";

    public String getBeforeCalc() {
        return beforeCalc;
    }

    public void setBeforeCalc(String beforeCalc) {
        this.beforeCalc = beforeCalc;
    }

    public String getBeforeCalcDD() {
        return "SalaryElement_beforeCalc";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="forEmployee">
    @Column(length = 1)
    private String forEmployee = "N";    //previously: companyFlag

    public String getForEmployee() {
        return forEmployee;
    }

    public void setForEmployee(String forEmployee) {
        this.forEmployee = forEmployee;
    }

    public String getForEmployeeDD() {
        return "SalaryElement_forEmployee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="defaultValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal defaultValue = java.math.BigDecimal.ZERO;

    public BigDecimal getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(BigDecimal defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValueDD() {
        return "SalaryElement_defaultValue";
    }
    @Transient
    private BigDecimal defaultValueMask = java.math.BigDecimal.ZERO;

    public BigDecimal getDefaultValueMask() {
        defaultValueMask = defaultValue;
        return defaultValueMask;
    }

    public void setDefaultValueMask(BigDecimal defaultValueMask) {
        updateDecimalValue("defaultValue", defaultValueMask);
    }

    public String getDefaultValueMaskDD() {
        return "SalaryElement_defaultValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable = false)
    @Translatable(translationField = "descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "SalaryElement_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="distributedOnCostCenter">
    @Column(length = 1)
    private String distributedOnCostCenter = "N";

    public String getDistributedOnCostCenter() {
        return distributedOnCostCenter;
    }

    public void setDistributedOnCostCenter(String distributedOnCostCenter) {
        this.distributedOnCostCenter = distributedOnCostCenter;
    }

    public String getDistributedOnCostCenterDD() {
        return "SalaryElement_distributedOnCostCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factor">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal factor = java.math.BigDecimal.ONE;

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public String getFactorDD() {
        return "SalaryElement_factor";
    }
    @Transient
    private BigDecimal factorMask = java.math.BigDecimal.ONE;

    public BigDecimal getFactorMask() {
        factorMask = factor;
        return factorMask;
    }

    public void setFactorMask(BigDecimal factorMask) {
        updateDecimalValue("factor", factorMask);
    }

    public String getFactorMaskDD() {
        return "SalaryElement_factorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nature">
    @Column(length = 1, nullable = false)
    private String nature = "1";

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getNatureDD() {
        return "SalaryElement_nature";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fixedVariable">
    @Column(length = 1)
    private String fixedVariable = "N";

    public String isFixedVariable() {
        return fixedVariable;
    }

    public void setFixedVariable(String fixedVariable) {
        this.fixedVariable = fixedVariable;
    }

    public String getFixedVariableDD() {
        return "SalaryElement_fixedVariable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="formulaName">
    @JoinColumn //FIXME: should be FK to Formula
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName formula;

    public FormulaDistinctName getFormula() {
        return formula;
    }

    public void setFormula(FormulaDistinctName formula) {
        this.formula = formula;
    }

    public String getFormulaDD() {
        return "SalaryElement_formula";
    }

    @Column //FIXME: to be udc
    private String formulaRelation = "M";

    public String getFormulaRelation() {
        return formulaRelation;
    }

    public void setFormulaRelation(String formulaRelation) {
        this.formulaRelation = formulaRelation;
    }

    public String getFormulaRelationDD() {
        return "SalaryElement_formulaRelation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applyInsurance">
    @Column(length = 1)
    private String applyInsurance;

    public String getApplyInsurance() {
        return applyInsurance;
    }

    public void setApplyInsurance(String applyInsurance) {
        this.applyInsurance = applyInsurance;
    }

    public String getApplyInsuranceDD() {
        return "SalaryElement_applyInsurance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxPercent">
    @Column(precision = 25, scale = 13)
    private BigDecimal maxPercent = java.math.BigDecimal.ZERO;

    public BigDecimal getMaxPercent() {
        return maxPercent;
    }

    public void setMaxPercent(BigDecimal maxPercent) {
        this.maxPercent = maxPercent;
    }

    public String getMaxPercentDD() {
        return "SalaryElement_maxPercent";
    }
    @Transient
    private BigDecimal maxPercentMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMaxPercentMask() {
        maxPercentMask = maxPercent;
        return maxPercentMask;
    }

    public void setMaxPercentMask(BigDecimal maxPercentMask) {
        updateDecimalValue("maxPercent", maxPercentMask);
    }

    public String getMaxPercentMaskDD() {
        return "SalaryElement_maxPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxSalElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement maxSalElement;

    public SalaryElement getMaxSalElement() {
        return maxSalElement;
    }

    public void setMaxSalElement(SalaryElement maxSalElement) {
        this.maxSalElement = maxSalElement;
    }

    public String getMaxSalElementDD() {
        return "SalaryElement_maxSalElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal maximumValue = java.math.BigDecimal.ZERO;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "SalaryElement_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue", maximumValueMask);
    }

    public String getMaximumValueMaskDD() {
        return "SalaryElement_maximumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minPercent">
    @Column(precision = 25, scale = 13)
    private BigDecimal minPercent = java.math.BigDecimal.ZERO;

    public BigDecimal getMinPercent() {
        return minPercent;
    }

    public void setMinPercent(BigDecimal minPercent) {
        this.minPercent = minPercent;
    }

    public String getMinPercentDD() {
        return "SalaryElement_minPercent";
    }
    @Transient
    private BigDecimal minPercentMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMinPercentMask() {
        return minPercentMask;
    }

    public void setMinPercentMask(BigDecimal minPercentMask) {
        this.minPercent = minPercentMask;
    }

    public String getMinPercentMaskDD() {
        return "SalaryElement_minPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minSalElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement minSalElement;

    public SalaryElement getMinSalElement() {
        return minSalElement;
    }

    public void setMinSalElement(SalaryElement minSalElement) {
        this.minSalElement = minSalElement;
    }

    public String getMinSalElementDD() {
        return "SalaryElement_minSalElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal minValue = java.math.BigDecimal.ZERO;

    public BigDecimal getMinValue() {
        return minValue;
    }

    public void setMinValue(BigDecimal minValue) {
        this.minValue = minValue;
    }

    public String getMinValueDD() {
        return "SalaryElement_minValue";
    }
    @Transient
    private BigDecimal minValueMask = java.math.BigDecimal.ZERO;

    public BigDecimal getMinValueMask() {
        minValueMask = minValue;
        return minValueMask;
    }

    public void setMinValueMask(BigDecimal minValueMask) {
        updateDecimalValue("minValue", minValueMask);
    }

    public String getMinValueMaskDD() {
        return "SalaryElement_minValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthly">
    @Column(length = 1, nullable = false)
    private String monthly = "Y";

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getMonthlyDD() {
        return "SalaryElement_monthly";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="perDay">
    @Column(length = 1, nullable = false)
    private String perDay = "N"; //Number of days flag

    public String getPerDay() {
        return perDay;
    }

    public void setPerDay(String perDay) {
        this.perDay = perDay;
    }

    public String getPerDayDD() {
        return "SalaryElement_perDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oppositeAccountNumber">
    @Column
    private String oppositeAccountNumber;

    public String getOppositeAccountNumber() {
        return oppositeAccountNumber;
    }

    public void setOppositeAccountNumber(String oppositeAccountNumber) {
        this.oppositeAccountNumber = oppositeAccountNumber;
    }

    public String getOppositeAccountNumberDD() {
        return "SalaryElement_oppositeAccountNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oppositeCostCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter oppositeCostCenter;

    public CostCenter getOppositeCostCenter() {
        return oppositeCostCenter;
    }

    public void setOppositeCostCenter(CostCenter oppositeCostCenter) {
        this.oppositeCostCenter = oppositeCostCenter;
    }

    public String getOppositeCostCenterDD() {
        return "SalaryElement_oppositeCostCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "SalaryElement_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="personalClose">
    @Column(length = 1)
    private String personalClose;

    public String getPersonalClose() {
        return personalClose;
    }

    public void setPersonalClose(String personalClose) {
        this.personalClose = personalClose;
    }

    public String getPersonalCloseDD() {
        return "SalaryElement_personalClose";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="periodDistributed">
    @Column(length = 1)
    private String periodDistributed = "N";

    public String getPeriodDistributed() {
        return periodDistributed;
    }

    public void setPeriodDistributed(String periodDistributed) {
        this.periodDistributed = periodDistributed;
    }

    public String getPeriodDistributedDD() {
        return "SalaryElement_periodDistributed";
    }

    public String getFixedVariable() {
        return fixedVariable;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="printSortIndex">
    @Column
    private int printSortIndex;

    public int getPrintSortIndex() {
        return printSortIndex;
    }

    public void setPrintSortIndex(int printSortIndex) {
        this.printSortIndex = printSortIndex;
    }

    public String getPrintSortIndexDD() {
        return "SalaryElement_printSortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approximation">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal approximation = java.math.BigDecimal.valueOf(3);

    public BigDecimal getApproximation() {
        return approximation;
    }

    public void setApproximation(BigDecimal approximation) {
        this.approximation = approximation;
    }

    public String getApproximationDD() {
        return "SalaryElement_approximation";
    }
    @Transient
    private BigDecimal approximationMask = java.math.BigDecimal.valueOf(3);

    public BigDecimal getApproximationMask() {
        approximationMask = approximation;
        return approximationMask;
    }

    public void setApproximationMask(BigDecimal approximationMask) {
        updateDecimalValue("approximation", approximationMask);
    }

    public String getApproximationMaskDD() {
        return "SalaryElement_approximationMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "SalaryElement_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable = false)
    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "SalaryElement_sortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxable">
    @Column(length = 1)
    private String taxable = "N";

    public String getTaxable() {
        return taxable;
    }

    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }

    public String getTaxableDD() {
        return "SalaryElement_taxable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private SalaryElementType type;

    public SalaryElementType getType() {
        return type;
    }

    public void setType(SalaryElementType type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "SalaryElement_type";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterDD() {
        return "SalaryElement_costCenter";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "SalaryElement_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affect">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "salaryElement")
    SalaryElementAffect affect;

    public SalaryElementAffect getAffect() {
        return affect;
    }

    public void setAffect(SalaryElementAffect affect) {
        this.affect = affect;
    }

    public String getAffectDD() {
        return "SalaryElement_affect";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mappedColumn">
    @Column
    private String mappedColumn;

    public String getMappedColumn() {
        return mappedColumn;
    }

    public void setMappedColumn(String mappedColumn) {
        this.mappedColumn = mappedColumn;
    }

    public String getMappedColumnDD() {
        return "SalaryElement_mappedColumn";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="internalCode">
    @Column(unique = true)
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getInternalCodeDD() {
        return "SalaryElement_internalCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="glAccount">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }

    public String getGlAccountDD() {
        return "SalaryElement_glAccount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="formulas">
    @OneToMany(mappedBy = "salaryElement")
    private List<SalaryElementFormula> formulas;

    public List<SalaryElementFormula> getFormulas() {
        return formulas;
    }

    public void setFormulas(List<SalaryElementFormula> formulas) {
        this.formulas = formulas;
    }

    public String getFormulasDD() {
        return "SalaryElement_formulas";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="generalCreditAccount">
    private boolean generalCreditAccount;

    public boolean isGeneralCreditAccount() {
        return generalCreditAccount;
    }

    public void setGeneralCreditAccount(boolean generalCreditAccount) {
        this.generalCreditAccount = generalCreditAccount;
    }

    public String getGeneralCreditAccountDD() {
        return "SalaryElement_generalCreditAccount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="generalDebitAccount">
    private boolean generalDebitAccount;

    public boolean isGeneralDebitAccount() {
        return generalDebitAccount;
    }

    public void setGeneralDebitAccount(boolean generalDebitAccount) {
        this.generalDebitAccount = generalDebitAccount;
    }

    public String getGeneralDebitAccountDD() {
        return "SalaryElement_generalDebitAccount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="debitAccount">
    private String debitAccount;

    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    public String getDebitAccountDD() {
        return "SalaryElement_debitAccount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="creditAccount">
    private String creditAccount;

    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    public String getCreditAccountDD() {
        return "SalaryElement_creditAccount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="costCenterDistribution">
    private boolean costCenterDistribution;

    public boolean isCostCenterDistribution() {
        return costCenterDistribution;
    }

    public void setCostCenterDistribution(boolean costCenterDistribution) {
        this.costCenterDistribution = costCenterDistribution;
    }

    public String getCostCenterDistributionDD() {
        return "SalaryElement_costCenterDistribution";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="paymentMethod">
    @ManyToOne
    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodDD() {
        return "SalaryElement_paymentMethod";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="paymentPeriod">
    @ManyToOne
    private PaymentPeriod paymentPeriod;

    public PaymentPeriod getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(PaymentPeriod paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public String getPaymentPeriodDD() {
        return "SalaryElement_paymentPeriod";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="dependOnInsuranceTaxCal">
    private boolean dependOnInsuranceTaxCal = false;

    public boolean isDependOnInsuranceTaxCal() {
        return dependOnInsuranceTaxCal;
    }

    public String getDependOnInsuranceTaxCalDD() {
        return "SalaryElement_dependOnInsuranceTaxCal";
    }

    public void setDependOnInsuranceTaxCal(boolean dependOnInsuranceTaxCal) {
        this.dependOnInsuranceTaxCal = dependOnInsuranceTaxCal;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "SalaryElement_legalEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "SalaryElement_legalEntityGroup";
    }
    //</editor-fold>
}
