package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantIqama extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantIqama_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="iqamaNum">
    private String iqamaNum;

    public String getIqamaNum() {
        return iqamaNum;
    }

    public void setIqamaNum(String iqamaNum) {
        this.iqamaNum = iqamaNum;
    }

    public String getIqamaNumDD() {
        return "ApplicantIqama_iqamaNum";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueDateDD() {
        return "ApplicantIqama_issueDate";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDate">

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDate;

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewalDateDD() {
        return "ApplicantIqama_renewalDate";
    }

      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueLocation">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC issueLocation;

    public UDC getIssueLocation() {
        return issueLocation;
    }

    public void setIssueLocation(UDC issueLocation) {
        this.issueLocation = issueLocation;
    }

    public String getIssueLocationDD() {
        return "ApplicantIqama_issueLocation";
    }
          // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entranceDate;

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public String getEntranceDateDD() {
        return "ApplicantIqama_entranceDate";
    }

      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entrancePort">
    @Column
    private String entrancePort;

    public String getEntrancePort() {
        return entrancePort;
    }

    public void setEntrancePort(String entrancePort) {
        this.entrancePort = entrancePort;
    }

    public String getEntrancePortDD() {
        return "ApplicantIqama_entrancePort";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="changeData">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date changeData;

    public Date getChangeData() {
        return changeData;
    }

    public void setChangeData(Date changeData) {
        this.changeData = changeData;
    }

    public String getChangeDataDD() {
        return "ApplicantIqama_changeData";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="remarks">
    @Column
    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarksDD() {
        return "ApplicantIqama_remarks";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "ApplicantIqama_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="labourOfficeNumber">
    @Column
    private String labourOfficeNumber;

    public String getLabourOfficeNumber() {
        return labourOfficeNumber;
    }

    public void setLabourOfficeNumber(String labourOfficeNumber) {
        this.labourOfficeNumber = labourOfficeNumber;
    }

    public String getLabourOfficeNumberDD() {
        return "ApplicantIqama_labourOfficeNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expiryDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expiryDate;

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getExpiryDateDD() {
        return "ApplicantIqama_expiryDate";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueDateHijri">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date issueDateHijri;

    public Date getIssueDateHijri() {
        return issueDateHijri;
    }

    public void setIssueDateHijri(Date issueDateHijri) {
        this.issueDateHijri = issueDateHijri;
    }

    public String getIssueDateHijriDD() {
        return "ApplicantIqama_issueDateHijri";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expiryDateHijri">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expiryDateHijri;

    public Date getExpiryDateHijri() {
        return expiryDateHijri;
    }

    public void setExpiryDateHijri(Date expiryDateHijri) {
        this.expiryDateHijri = expiryDateHijri;
    }

    public String getExpiryDateHijriDD() {
        return "ApplicantIqama_expiryDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDateHijri">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date renewalDateHijri;

    public Date getRenewalDateHijri() {
        return renewalDateHijri;
    }

    public void setRenewalDateHijri(Date renewalDateHijri) {
        this.renewalDateHijri = renewalDateHijri;
    }

    public String getRenewalDateHijriDD() {
        return "ApplicantIqama_renewalDateHijri";
    }
    // </editor-fold>
}