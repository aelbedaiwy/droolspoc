package com.unitedofoq.otms.foundation.occupation;


import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Task extends BaseEntity {

    @Column(unique = true, nullable = false)
    private String taskID;
    @Translatable(translationField="nameTranslated")
    private String name;
    private boolean onet;
    private String taskDate;
    private String domainSource;
    private Integer incumbentsResponding;
    //attributes come from Direct Assosiation.
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC type;

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getDomainSource() {
        return domainSource;
    }

    public void setDomainSource(String domainSource) {
        this.domainSource = domainSource;
    }

    public Integer getIncumbentsResponding() {
        return incumbentsResponding;
    }

    public void setIncumbentsResponding(Integer incumbentsResponding) {
        this.incumbentsResponding = incumbentsResponding;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOnet() {
        return onet;
    }

    public void setOnet(boolean onet) {
        this.onet = onet;
    }

    

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getNameTranslatedDD() {
        return "Task_nameTranslated";
    }

    public String getOnetDD() {
        return "Task_onet";
    }

    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
}
