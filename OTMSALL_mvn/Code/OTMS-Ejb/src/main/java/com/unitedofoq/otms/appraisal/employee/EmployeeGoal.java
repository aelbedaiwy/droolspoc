package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeGoal extends EmployeeKPA {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(optional = false)
    private Employee employee;

    @Override
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeGoal_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionGoal">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PositionGoal positionGoal;

    public PositionGoal getPositionGoal() {
        return positionGoal;
    }

    public void setPositionGoal(PositionGoal positionGoal) {
        this.positionGoal = positionGoal;
    }

    public String getPositionGoalDD() {
        return "EmployeeGoal_positionGoal";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actionFreeText">
    private String actionFreeText;

    public String getActionFreeText() {
        return actionFreeText;
    }

    public void setActionFreeText(String actionFreeText) {
        this.actionFreeText = actionFreeText;
    }

    public String getActionFreeTextDD() {
        return "EmployeeGoal_actionFreeText";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tasksFreeText">
    private String tasksFreeText;

    public String getTasksFreeText() {
        return tasksFreeText;
    }

    public void setTasksFreeText(String tasksFreeText) {
        this.tasksFreeText = tasksFreeText;
    }

    public String getTasksFreeTextDD() {
        return "EmployeeGoal_tasksFreeText";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="commentByManager">
    private String commentByManager;

    public String getCommentByManager() {
        return commentByManager;
    }

    public void setCommentByManager(String commentByManager) {
        this.commentByManager = commentByManager;
    }

    public String getCommentByManagerDD() {
        return "EmployeeGoal_commentByManager";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="valueTo">
    private double valueTo;

    public double getValueTo() {
        return valueTo;
    }

    public void setValueTo(double valueTo) {
        this.valueTo = valueTo;
    }

    public String getValueToDD() {
        return "GoalBase_valueTo";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="valueFrom">
    private double valueFrom;

    public double getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(double valueFrom) {
        this.valueFrom = valueFrom;
    }

    public String getValueFromDD() {
        return "GoalBase_valueFrom";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dueDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date dueDate;

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getDueDateDD() {
        return "GoalBase_dueDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "EmployeeGoal_description";
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "EmployeeQuantativeMeasures_applicant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="managerConfirmation">
    private boolean managerConfirmation;

    public boolean isManagerConfirmation() {
        return managerConfirmation;
    }

    public String getManagerConfirmationDD() {
        return "EmployeeGoal_managerConfirmation";
    }

    public void setManagerConfirmation(boolean managerConfirmation) {
        this.managerConfirmation = managerConfirmation;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeAgree">
    private boolean employeeAgree;

    public boolean isEmployeeAgree() {
        return employeeAgree;
    }

    public String getEmployeeAgreeDD() {
        return "EmployeeGoal_employeeAgree";
    }

    public void setEmployeeAgree(boolean employeeAgree) {
        this.employeeAgree = employeeAgree;
    }
    //</editor-fold >
    // <editor-fold defaultstate="collapsed" desc="employeeAgreement">
    private String employeeAgreement; // drop down: Agree-Disagree-Empty

    public String getEmployeeAgreement() {
        return employeeAgreement;
    }

    public void setEmployeeAgreement(String employeeAgreement) {
        this.employeeAgreement = employeeAgreement;
    }

    public String getEmployeeAgreementDD() {
        return "EmployeeGoal_employeeAgreement";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeComments">
    private String employeeComments;

    public String getEmployeeComments() {
        return employeeComments;
    }

    public String getEmployeeCommentsDD() {
        return "EmployeeGoal_employeeComments";
    }

    public void setEmployeeComments(String employeeComments) {
        this.employeeComments = employeeComments;
    }
    //</editor-fold >
}
