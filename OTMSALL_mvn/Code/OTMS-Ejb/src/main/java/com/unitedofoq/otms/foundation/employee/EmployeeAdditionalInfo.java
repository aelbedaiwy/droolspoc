/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeAdditionalInfo extends AdditionalInfo {
   
    // <editor-fold defaultstate="collapsed" desc="employee">

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeAdditionalInfo_employee";
    }
    // </editor-fold>
}
