/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepTemplateElement extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepTemplateElement_dsDbid";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="template">
    @Translatable(translationField = "templateTranslated")
    private String template;
    
    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
    
    public String getTemplateDD() {
        return "RepTemplateElement_template";
    }
    
    @Transient
    @Translation(originalField = "template")
    private String templateTranslated;

    public String getTemplateTranslated() {
        return templateTranslated;
    }

    public void setTemplateTranslated(String templateTranslated) {
        this.templateTranslated = templateTranslated;
    }
    
    public String getTemplateTranslatedDD() {
        return "RepTemplateElement_template";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="templatePurpose">
    private String templatePurpose;
    
    public String getTemplatePurpose() {
        return templatePurpose;
    }

    public void setTemplatePurpose(String templatePurpose) {
        this.templatePurpose = templatePurpose;
    }
    
    public String getTemplatePurposeDD() {
        return "RepTemplateElement_templatePurpose";
    }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="element">
    @Translatable(translationField = "elementTranslated")
    private String element;
    
    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }
    
    public String getElementDD() {
        return "RepTemplateElement_element";
    }
    
    @Transient
    @Translation(originalField = "element")
    private String elementTranslated;

    public String getElementTranslated() {
        return elementTranslated;
    }

    public void setElementTranslated(String elementTranslated) {
        this.elementTranslated = elementTranslated;
    }
    
    public String getElementTranslatedDD() {
        return "RepTemplateElement_element";
    }
    //</editor-fold>
      
    //<editor-fold defaultstate="collapsed" desc="weight">
    private BigDecimal weight;

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
    
    public String getWeightDD() {
        return "RepTemplateElement_weight";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="procedures">
    private String procedures;
    
    public String getProcedures() {
        return procedures;
    }

    public void setProcedures(String procedures) {
        this.procedures = procedures;
    }
    
    public String getProceduresDD() {
        return "RepTemplateElement_procedures";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ratingDescription">
    @Translatable(translationField = "ratingDescriptionTranslated")
    private String ratingDescription;

    public String getRatingDescription() {
        return ratingDescription;
    }

    public void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription;
    }
    
    public String getRatingDescriptionDD() {
        return "RepTemplateElement_ratingDescription";
    }
    
    @Transient
    @Translation(originalField = "ratingDescription")
    private String ratingDescriptionTranslated;

    public String getRatingDescriptionTranslated() {
        return ratingDescriptionTranslated;
    }

    public void setRatingDescriptionTranslated(String ratingDescriptionTranslated) {
        this.ratingDescriptionTranslated = ratingDescriptionTranslated;
    }
    
    public String getRatingDescriptionTranslatedDD() {
        return "RepTemplateElement_ratingDescription";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="elementPurpose">
    private String elementPurpose;

    public String getElementPurpose() {
        return elementPurpose;
    }

    public void setElementPurpose(String elementPurpose) {
        this.elementPurpose = elementPurpose;
    }
    
    public String getElementPurposeDD() {
        return "RepTemplateElement_elementPurpose";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="elementType">
    @Translatable(translationField = "elementTypeTranslated")
    private String elementType;

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }
    
    public String getElementTypeDD() {
        return "RepTemplateElement_elementType";
    }
    
    @Transient
    @Translation(originalField = "elementType")
    private String elementTypeTranslated;

    public String getElementTypeTranslated() {
        return elementTypeTranslated;
    }

    public void setElementTypeTranslated(String elementTypeTranslated) {
        this.elementTypeTranslated = elementTypeTranslated;
    }
    
    public String getElementTypeTranslatedDD() {
        return "RepTemplateElement_elementType";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    
    public String getCommentsDD() {
        return "RepTemplateElement_comments";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="outOfPoints">
    private BigDecimal outOfPoints;

    public BigDecimal getOutOfPoints() {
        return outOfPoints;
    }

    public void setOutOfPoints(BigDecimal outOfPoints) {
        this.outOfPoints = outOfPoints;
    }
    
    public String getOutOfPointsDD() {
        return "RepTemplateElement_outOfPoints";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyIDDD() {
        return "RepTemplateElement_companyID";
    }
    // </editor-fold>
}
