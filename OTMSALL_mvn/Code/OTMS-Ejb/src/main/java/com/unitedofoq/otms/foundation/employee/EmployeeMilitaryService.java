/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;
import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeMilitaryService extends BusinessObjectBaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

     public String getEmployeeDD() {
        return "EmployeeMilitaryService_employee";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDate">


    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fromDate;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }
     public String getFromDateDD() {
        return "EmployeeMilitaryService_fromDate";
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
        @Temporal(javax.persistence.TemporalType.DATE)
        private Date toDate;

        public Date getToDate() {
            return toDate;
        }

        public void setToDate(Date toDate) {
            this.toDate = toDate;
        }
        public String getToDateDD() {
            return "EmployeeMilitaryService_toDate";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="completationDate">
        @Temporal(javax.persistence.TemporalType.DATE)
        private Date completationDate;

        public Date getCompletationDate() {
            return completationDate;
        }

        public void setCompletationDate(Date completationDate) {
            this.completationDate = completationDate;
        }
         public String getCompletationDateDD() {
            return "EmployeeMilitaryService_completationDate";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">

        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC status;

        public UDC getStatus() {
            return status;
        }

        public void setStatus(UDC status) {
            this.status = status;
        }
         public String getStatusDD() {
            return "EmployeeMilitaryService_status";
        }
         // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="form">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC form;

        public UDC getForm() {
            return form;
        }

        public void setForm(UDC form) {
            this.form = form;
        }
          public String getFormDD() {
            return "EmployeeMilitaryService_form";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueOffice">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC issueOffice;

        public UDC getIssueOffice() {
            return issueOffice;
        }

        public void setIssueOffice(UDC issueOffice) {
            this.issueOffice = issueOffice;
        }
         public String getIssueOfficeDD() {
            return "EmployeeMilitaryService_issueOffice";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="examinedDate">

        @Temporal(javax.persistence.TemporalType.DATE)
        private Date examinedDate;

        public Date getExaminedDate() {
            return examinedDate;
        }

        public void setExaminedDate(Date examinedDate) {
            this.examinedDate = examinedDate;
        }
        public String getExaminedDateDD() {
            return "EmployeeMilitaryService_examinedDate";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reserveDate">

        @Temporal(javax.persistence.TemporalType.DATE)
        private Date reserveDate;

        public Date getReserveDate() {
            return reserveDate;
        }

        public void setReserveDate(Date reserveDate) {
            this.reserveDate = reserveDate;
        }
        public String getReserveDateDD() {
            return "EmployeeMilitaryService_reserveDate";
        }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="addToServicePeriod">
        private String addToServicePeriod;

        public String getAddToServicePeriod() {
            return addToServicePeriod;
        }

        public void setAddToServicePeriod(String addToServicePeriod) {
            this.addToServicePeriod = addToServicePeriod;
        }
        public String getAddToServicePeriodDD() {
            return "EmployeeMilitaryService_addToServicePeriod";
        }
        // </editor-fold>
}
