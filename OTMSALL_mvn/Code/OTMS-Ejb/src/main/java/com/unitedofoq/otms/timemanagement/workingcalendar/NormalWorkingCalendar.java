/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.timemanagement.Rule;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
//@ParentEntity(fields={"company"})
@ParentEntity(fields = {"calendar"})
@ChildEntity(fields = {"dateCalendarExceptions", "daysCalendarExceptions", "rules", "otSegments"})
public class NormalWorkingCalendar extends BusinessObjectBaseEntity {
//    //<editor-fold defaultstate="collapsed" desc="company">
//    @JoinColumn(nullable=false)
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private Company company;
//
//    public Company getCompany() {
//        return company;
//    }
//
//    public void setCompany(Company company) {
//        this.company = company;
//    }
//
//    public String getCompanyDD() {
//        return "NormalWorkingCalendar_company";
//    }
//    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "NormalWorkingCalendar_code";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    @Column
    private String name;
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslatedDD() {
        return "NormalWorkingCalendar_name";
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="startTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getStartTimeDD() {
        return "NormalWorkingCalendar_startTime";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="numberOfHours">
    private BigDecimal numberOfHours;

    public BigDecimal getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(BigDecimal numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public String getNumberOfHoursDD() {
        return "NormalWorkingCalendar_numberOfHours";
    }

    @Transient
    private BigDecimal numberOfHoursMask;

    public BigDecimal getNumberOfHoursMask() {
        numberOfHoursMask = numberOfHours;
        return numberOfHoursMask;
    }

    public void setNumberOfHoursMask(BigDecimal numberOfHoursMask) {
        updateDecimalValue("numberOfHours", numberOfHoursMask);
    }

    public String getNumberOfHoursMaskDD() {
        return "NormalWorkingCalendar_numberOfHoursMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="numberOfDays">
    private Integer numberOfDays;

    public Integer getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(Integer numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public String getNumberOfDaysDD() {
        return "NormalWorkingCalendar_numberOfDays";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="calendar">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private TMCalendar calendar;

    public TMCalendar getCalendar() {
        return calendar;
    }

    public void setCalendar(TMCalendar calendar) {
        this.calendar = calendar;
    }

    public String getCalendarDD() {
        return "NormalWorkingCalendar_calendar";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="timeIn">
    private String timeIn;

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeInDD() {
        return "NormalWorkingCalendar_timeIn";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="timeOut">
    private String timeOut;

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getTimeOutDD() {
        return "NormalWorkingCalendar_timeOut";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="breakFrom">
//    @Column
//    @Temporal(TemporalType.TIMESTAMP)
    private String breakFrom;

    public String getBreakFrom() {
        return breakFrom;
    }

    public void setBreakFrom(String breakFrom) {
        this.breakFrom = breakFrom;
    }

    public String getBreakFromDD() {
        return "NormalWorkingCalendar_breakFrom";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="breakTo">
    //@Temporal(TemporalType.TIMESTAMP)
    private String breakTo;

    public String getBreakTo() {
        return breakTo;
    }

    public void setBreakTo(String breakTo) {
        this.breakTo = breakTo;
    }

    public String getBreakToDD() {
        return "NormalWorkingCalendar_breakTo";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="break2From">
//    @Column
//    @Temporal(TemporalType.TIMESTAMP)
    private String break2From;

    public String getBreak2From() {
        return break2From;
    }

    public void setBreak2From(String breakFrom) {
        this.break2From = breakFrom;
    }

    public String getBreak2FromDD() {
        return "NormalWorkingCalendar_break2From";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="break2To">
    //@Temporal(TemporalType.TIMESTAMP)
    private String break2to;

    public String getBreak2to() {
        return break2to;
    }

    public void setBreak2to(String break2to) {
        this.break2to = break2to;
    }

    public String getBreak2toDD() {
        return "NormalWorkingCalendar_break2to";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOff1">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dayOff1;

    public void setDayOff1(UDC dayOff1) {
        this.dayOff1 = dayOff1;
    }

    public UDC getDayOff1() {
        return dayOff1;
    }

    public String getDayOff1DD() {
        return "NormalWorkingCalendar_dayOff1";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOff2">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC dayOff2;

    public void setDayOff2(UDC dayOff2) {
        this.dayOff2 = dayOff2;
    }

    public UDC getDayOff2() {
        return dayOff2;
    }

    public String getDayOff2DD() {
        return "NormalWorkingCalendar_dayOff2";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="dateCalendarExceptions">
    @OneToMany(mappedBy = "calendar")
    private List<DateCalendarException> dateCalendarExceptions;

    public List<DateCalendarException> getDateCalendarExceptions() {
        return dateCalendarExceptions;
    }

    public void setDateCalendarExceptions(List<DateCalendarException> dateCalendarExceptions) {
        this.dateCalendarExceptions = dateCalendarExceptions;
    }

    public String getDateCalendarExceptionsDD() {
        return "NormalWorkingCalendar_dateCalendarExceptions";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="daysCalendarExceptions">
    @OneToMany(mappedBy = "calendar")
    private List<DaysCalendarException> daysCalendarExceptions;

    public List<DaysCalendarException> getDaysCalendarExceptions() {
        return daysCalendarExceptions;
    }

    public void setDaysCalendarExceptions(List<DaysCalendarException> daysCalendarExceptions) {
        this.daysCalendarExceptions = daysCalendarExceptions;
    }

    public String getDaysCalendarExceptionsDD() {
        return "NormalWorkingCalendar_daysCalendarExceptions";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rules">
    @OneToMany(mappedBy = "workingCalendar")
    private List<Rule> rules;

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public String getRulesDD() {
        return "NormalWorkingCalendar_rules";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="dayStart">
    String dayStart;

    public String getDayStart() {
        return dayStart;
    }

    public String getDayStartDD() {
        return "NormalWorkingCalendar_dayStart";
    }

    public void setDayStart(String dayStart) {
        this.dayStart = dayStart;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="dayEnd">
    String dayEnd;

    public String getDayEnd() {
        return dayEnd;
    }

    public String getDayEndDD() {
        return "NormalWorkingCalendar_dayEnd";
    }

    public void setDayEnd(String dayEnd) {
        this.dayEnd = dayEnd;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="nightStart">
    String nightStart;

    public String getNightStart() {
        return nightStart;
    }

    public String getNightStartDD() {
        return "NormalWorkingCalendar_nightStart";
    }

    public void setNightStart(String nightStart) {
        this.nightStart = nightStart;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="nightEnd">
    String nightEnd;

    public String getNightEnd() {
        return nightEnd;
    }

    public String getNightEndDD() {
        return "NormalWorkingCalendar_nightEnd";
    }

    public void setNightEnd(String nightEnd) {
        this.nightEnd = nightEnd;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="tolerence">
    private BigDecimal tolerence;

    public BigDecimal getTolerence() {
        return tolerence;
    }

    public void setTolerence(BigDecimal tolerence) {
        this.tolerence = tolerence;
    }

    public String getTolerenceDD() {
        return "NormalWorkingCalendar_tolerence";
    }

    @Transient
    private BigDecimal tolerenceMask;

    public BigDecimal getTolerenceMask() {
        tolerenceMask = tolerence;
        return tolerenceMask;
    }

    public void setTolerenceMask(BigDecimal tolerenceMask) {
        updateDecimalValue("tolerence", tolerenceMask);
    }

    public String getTolerenceMaskDD() {
        return "NormalWorkingCalendar_tolerenceMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ignoreTolerence">
    private boolean ignoreTolerence;

    public boolean isIgnoreTolerence() {
        return ignoreTolerence;
    }

    public void setIgnoreTolerence(boolean ignoreTolerence) {
        this.ignoreTolerence = ignoreTolerence;
    }

    public String getIgnoreTolerenceDD() {
        return "NormalWorkingCalendar_ignoreTolerence";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="sequence">
    private Integer shiftSequence;

    public Integer getShiftSequence() {
        return shiftSequence;
    }

    public void setShiftSequence(Integer shiftSequence) {
        this.shiftSequence = shiftSequence;
    }

    public String getShiftSequenceDD() {
        return "NormalWorkingCalendar_shiftSequence";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="dayNumber">
    private String startDayTime;

    public String getStartDayTime() {
        return startDayTime;
    }

    public void setStartDayTime(String startDayTime) {
        this.startDayTime = startDayTime;
    }

    public String getStartDayTimeDD() {
        return "NormalWorkingCalendar_startDayTime";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="otSegments">
    @OneToMany(mappedBy = "calendar")
    private List<OTSegment> otSegments;

    public List<OTSegment> getOtSegments() {
        return otSegments;
    }

    public void setOtSegments(List<OTSegment> otSegments) {
        this.otSegments = otSegments;
    }

    public String getOtSegmentsDD() {
        return "NormalWorkingCalendar_otSegments";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="minimumOT">
    private String minimumOT;

    public String getMinimumOT() {
        return minimumOT;
    }

    public void setMinimumOT(String minimumOT) {
        this.minimumOT = minimumOT;
    }

    public String getMinimumOTDD() {
        return "NormalWorkingCalendar_minimumOT";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public String getLegalEntityDD() {
        return "NormalWorkingCalendar_legalEntity";
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="earlyLeaveTolerence">
    private BigDecimal earlyLeaveTolerence;

    public BigDecimal getEarlyLeaveTolerence() {
        return earlyLeaveTolerence;
    }

    public String getEarlyLeaveTolerenceDD() {
        return "NormalWorkingCalendar_earlyLeaveTolerence";
    }

    public void setEarlyLeaveTolerence(BigDecimal earlyLeaveTolerence) {
        this.earlyLeaveTolerence = earlyLeaveTolerence;
    }

    @Transient
    private BigDecimal earlyLeaveTolerenceMask;

    public BigDecimal getEarlyLeaveTolerenceMask() {
        earlyLeaveTolerenceMask = earlyLeaveTolerence;
        return earlyLeaveTolerenceMask;
    }

    public void setEarlyLeaveTolerenceMask(BigDecimal earlyLeaveTolerenceMask) {
        updateDecimalValue("earlyLeaveTolerence", earlyLeaveTolerenceMask);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ignoreEarlyLeaveTolerence">
    private boolean ignoreEarlyLeaveTolerence;

    public boolean isIgnoreEarlyLeaveTolerence() {
        return ignoreEarlyLeaveTolerence;
    }

    public String getIgnoreEarlyLeaveTolerenceDD() {
        return "ignoreEarlyLeaveTolerence";
    }

    public void setIgnoreEarlyLeaveTolerence(boolean ignoreEarlyLeaveTolerence) {
        this.ignoreEarlyLeaveTolerence = ignoreEarlyLeaveTolerence;
    }
    //</editor-fold>

    private BigDecimal dayLessWorkTolerence;

    public BigDecimal getDayLessWorkTolerence() {
        return dayLessWorkTolerence;
    }

    public String getDayLessWorkTolerenceDD() {
        return "NormalWorkingCalendar_dayLessWorkTolerence";
    }

    public void setDayLessWorkTolerence(BigDecimal dayLessWorkTolerence) {
        this.dayLessWorkTolerence = dayLessWorkTolerence;
    }

    @Transient
    private BigDecimal dayLessWorkTolerenceMask;

    public BigDecimal getDayLessWorkTolerenceMask() {
        dayLessWorkTolerenceMask = dayLessWorkTolerence;
        return dayLessWorkTolerenceMask;
    }

    public void setDayLessWorkTolerenceMask(BigDecimal dayLessWorkTolerenceMask) {
        updateDecimalValue("dayLessWorkTolerence", dayLessWorkTolerenceMask);
    }

    private boolean ignoreDayLessWorkTolerence;

    public boolean isIgnoreDayLessWorkTolerence() {
        return ignoreDayLessWorkTolerence;
    }

    public void setIgnoreDayLessWorkTolerence(boolean ignoreDayLessWorkTolerence) {
        this.ignoreDayLessWorkTolerence = ignoreDayLessWorkTolerence;
    }

    public String getIgnoreDayLessWorkTolerenceDD() {
        return "NormalWorkingCalendar_ignoreTolerence";
    }
}
