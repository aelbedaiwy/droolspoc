/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.training.activity.CatalogActivity;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@MappedSuperclass
public class CertificateBase extends BaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="isCatalogOrUser">
    @Column(length=1 , nullable= false )
    private String isCatalogOrUser;

    public String getIsCatalogOrUser() {
        return isCatalogOrUser;
    }

    public void setIsCatalogOrUser(String isCatalogOrUser) {
        this.isCatalogOrUser = isCatalogOrUser;
    }
    
    public String getIsCatalogOrUserDD() {
        return "CertificateBase_isCatalogOrUser";
    }

    public CertificateBase() {
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private CatalogActivity activity;

    public CatalogActivity getActivity() {
        return activity;
    }

    public void setActivity(CatalogActivity activity) {
        this.activity = activity;
    }
    public String getActivityDD() {
        return "CertificateBase_activity";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="certificateName">
    private String certificateName;

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }
public String getCertificateNameDD() {
        return "CertificateBase_certificateName";
    }

      @Transient
    private String certificateNameTranslated;

    public String getCertificateNameTranslated() {
        return certificateNameTranslated;
    }

    public void setCertificateNameTranslated(String certificateNameTranslated) {
        this.certificateNameTranslated = certificateNameTranslated;
    }
public String getCertificateNameTranslatedDD() {
        return "CertificateBase_certificateNameTranslated";
    }
      // </editor-fold>
   
}
