/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"penalty"})
public class PenaltyRule extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="penalty">
@JoinColumn(nullable=false)
@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
private Penalty penalty;

public Penalty getPenalty() {
    return penalty;
}

public void setPenalty(Penalty penalty) {
    this.penalty = penalty;
}

public String getPenaltyDD() {
    return "PenaltyRule_penalty";
}
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
@Column(nullable=false)
private short sortIndex;//penalty_d_seq

public short getSortIndex() {
    return sortIndex;
}

public void setSortIndex(short sortIndex) {
    this.sortIndex = sortIndex;
}

public String getSortIndexDD() {
    return "PenaltyRule_sortIndex";
}

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="occuranceFrom">
    @Column(precision=18,scale=3)
    private BigDecimal occuranceFrom;//(name = "occurance_from")

    public BigDecimal getOccuranceFrom() {
        return occuranceFrom;
    }

    public void setOccuranceFrom(BigDecimal occuranceFrom) {
        this.occuranceFrom = occuranceFrom;
    }
    public String getOccuranceFromDD() {
        return "PenaltyRule_occuranceFrom";
    }
    @Transient
    private BigDecimal occuranceFromMask;

    public BigDecimal getOccuranceFromMask() {
        occuranceFromMask = occuranceFrom ;
        return occuranceFromMask;
    }

    public void setOccuranceFromMask(BigDecimal occuranceFromMask) {
        updateDecimalValue("occuranceFrom",occuranceFromMask);
    }
    public String getOccuranceFromMaskDD() {
        return "PenaltyRule_occuranceFromMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="occuranceTo">
        @Column(precision=18,scale=3)
        private BigDecimal occuranceTo;//(name = "occurance_to")

        public BigDecimal getOccuranceTo() {
            return occuranceTo;
        }

        public void setOccuranceTo(BigDecimal occuranceTo) {
            this.occuranceTo = occuranceTo;
        }
        public String getOccuranceToDD() {
            return "PenaltyRule_occuranceTo";
        }
        @Transient
        private BigDecimal occuranceToMask;

        public BigDecimal getOccuranceToMask() {
            occuranceToMask = occuranceTo;
            return occuranceToMask;
        }

        public void setOccuranceToMask(BigDecimal occuranceToMask) {
            updateDecimalValue("occuranceTo",occuranceToMask);
        }
        public String getOccuranceToMaskDD() {
            return "PenaltyRule_occuranceToMask";
        }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factor">
    @Column(precision=18,scale=3)
    private BigDecimal factor;//(name = "pbi_factor")

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }
    public String getFactorDD() {
        return "PenaltyRule_factor";
    }
    @Transient
    private BigDecimal factorMask;

    public BigDecimal getFactorMask() {
        factorMask = factor;
        return factorMask;
    }

    public void setFactorMask(BigDecimal factorMask) {
        updateDecimalValue("factor",factorMask);
    }
    public String getFactorMaskDD() {
        return "PenaltyRule_factorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="amount">
    @Column(precision=18,scale=3)
    private BigDecimal amount;//name = "amount")

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAmountDD() {
        return "PenaltyRule_amount";
    }
    @Transient
    private BigDecimal amountMask;

    public BigDecimal getAmountMask() {
        amountMask = amount ;
        return amountMask;
    }

    public void setAmountMask(BigDecimal amountMask) {
        updateDecimalValue("amount",amountMask);
    }

    public String getAmountMaskDD() {
        return "PenaltyRule_amountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="addValue">
@Column(length=1)
//1-0
private String addValue;//name = "add_value")

    public String getAddValue() {
        return addValue;
    }

    public void setAddValue(String addValue) {
        this.addValue = addValue;
    }
public String getAddValueDD() {
        return "PenaltyRule_addValue";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decrease">
    @Column(length=1)
    //Y-N
    private String decrease;

    public String getDecrease() {
        return decrease;
    }

    public void setDecrease(String decrease) {
        this.decrease = decrease;
    }
    public String getDecreaseDD() {
        return "PenaltyRule_decrease";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decreasePercent">
    @Column(precision=18,scale=3)
    private BigDecimal  decreasePercent;

    public BigDecimal getDecreasePercent() {
        return decreasePercent;
    }

    public void setDecreasePercent(BigDecimal decreasePercent) {
        this.decreasePercent = decreasePercent;
    }
    public String getDecreasePercentDD() {
        return "PenaltyRule_decreasePercent";
    }
    @Transient
    private BigDecimal  decreasePercentMask;

    public BigDecimal getDecreasePercentMask() {
        decreasePercentMask = decreasePercent;
        return decreasePercentMask;
    }

    public void setDecreasePercentMask(BigDecimal decreasePercentMask) {
        updateDecimalValue("decreasePercent",decreasePercentMask);
    }
    public String getDecreasePercentMaskDD() {
        return "PenaltyRule_decreasePercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="upgradeReason">
    @Column
    private String upgradeReason;

    public String getUpgradeReason() {
        return upgradeReason;
    }

    public void setUpgradeReason(String upgradeReason) {
        this.upgradeReason = upgradeReason;
    }
    public String getUpgradeReasonDD() {
        return "PenaltyRule_upgradeReason";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decFlag">
    @Column(length=1)
    private String decFlag;

    public String getDecFlag() {
        return decFlag;
    }

    public void setDecFlag(String decFlag) {
        this.decFlag = decFlag;
    }
    
    public String getDecFlagDD() {
        return "PenaltyRule_decFlag";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decPercent">
    @Column(precision=18,scale=3)
    private BigDecimal decPercent;//(name = "decPercent")

    public BigDecimal getDecPercent() {
        return decPercent;
    }

    public void setDecPercent(BigDecimal decPercent) {
        this.decPercent = decPercent;
    }
    
    public String getDecPercentDD() {
        return "PenaltyRule_decPercent";
    }
    @Transient
    private BigDecimal decPercentMask;

    public BigDecimal getDecPercentMask() {
        decPercentMask = decPercent;
        return decPercentMask;
    }

    public void setDecPercentMask(BigDecimal decPercentMask) {
        updateDecimalValue("decPercent",decPercentMask);
    }

    public String getDecPercentMaskDD() {
        return "PenaltyRule_decPercentMask";
    }
    // </editor-fold>
}
