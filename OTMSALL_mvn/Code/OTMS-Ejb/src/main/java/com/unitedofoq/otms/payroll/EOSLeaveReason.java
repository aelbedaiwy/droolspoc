/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields = "company")
@ChildEntity(fields = {"eosBonuses"})
public class EOSLeaveReason extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "EOSLeaveReason_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column(unique = true)
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EOSLeaveReason_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable = false)
    @Translatable(translationField = "descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "EOSLeaveReason_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="removeVacation">
    private boolean removeVacation;

    public boolean isRemoveVacation() {
        return removeVacation;
    }

    public String getRemoveVacationDD() {
        return "EOSBonus_removeVacation";
    }

    public void setRemoveVacation(boolean removeVacation) {
        this.removeVacation = removeVacation;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="defaultLR">
    private boolean defaultLR;

    public boolean isDefaultLR() {
        return defaultLR;
    }

    public String getDefaultLRDD() {
        return "EOSBonus_defaultLR";
    }

    public void setDefaultLR(boolean defaultLR) {
        this.defaultLR = defaultLR;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="eosBonuses">
    @OneToMany(mappedBy = "leaveReason")
    private List<EOSBonus> eosBonuses;

    public List<EOSBonus> getEosBonuses() {
        return eosBonuses;
    }

    public String getEosBonusesDD() {
        return "EOSLeaveReason_eosBonuses";
    }

    public void setEosBonuses(List<EOSBonus> eosBonuses) {
        this.eosBonuses = eosBonuses;
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="minExpYears">
    private BigDecimal minExpYears;

    public BigDecimal getMinExpYears() {
        return minExpYears;
    }

    public void setMinExpYears(BigDecimal minExpYears) {
        this.minExpYears = minExpYears;
    }

    public String getMinExpYearsDD() {
        return "EOSLeaveReason_minExpYears";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="dependOnCondition">
    private boolean dependOnCondition=false;

    public boolean isDependOnCondition() {
        return dependOnCondition;
    }

    public void setDependOnCondition(boolean dependOnCondition) {
        this.dependOnCondition = dependOnCondition;
    }

    public String isDependOnConditionDD() {
        return "EOSLeaveReason_dependOnCondition";
    }
    //</editor-fold>
}
