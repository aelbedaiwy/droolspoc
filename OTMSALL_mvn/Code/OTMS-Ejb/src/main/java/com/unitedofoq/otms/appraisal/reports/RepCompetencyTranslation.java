
package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class RepCompetencyTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="competencyDescription">
    @Column
    private String competencyDescription;

    public void setCompetencyDescription(String competencyDescription) {
        this.competencyDescription = competencyDescription;
    }

    public String getCompetencyDescription() {
        return competencyDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="competencyName">
    @Column
    private String competencyName;

    public void setCompetencyName(String competencyName) {
        this.competencyName = competencyName;
    }

    public String getCompetencyName() {
        return competencyName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="competencyGroupName">
    @Column
    private String competencyGroupName;

    public void setCompetencyGroupName(String competencyGroupName) {
        this.competencyGroupName = competencyGroupName;
    }

    public String getCompetencyGroupName() {
        return competencyGroupName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ratingSchema">
    @Column
    private String ratingSchema;

    public void setRatingSchema(String ratingSchema) {
        this.ratingSchema = ratingSchema;
    }

    public String getRatingSchema() {
        return ratingSchema;
    }
    // </editor-fold>

}
