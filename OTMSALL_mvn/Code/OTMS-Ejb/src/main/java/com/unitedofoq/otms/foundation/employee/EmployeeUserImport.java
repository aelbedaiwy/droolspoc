package com.unitedofoq.otms.foundation.employee;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

/**
 *
 * @author mragab
 */
@Entity
public class EmployeeUserImport extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="empCode">
    private String empCode;

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getEmpCodeDD() {
        return "EmployeeUserImport_empCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userCode">
    private String userCode;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserCodeDD() {
        return "EmployeeUserImport_userCode";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="userPassword">
    private String userPassword;

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserPasswordDD() {
        return "EmployeeUserImport_userPassword";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="selfOrManager">
    private String selfOrManager;

    public String getSelfOrManager() {
        return selfOrManager;
    }

    public void setSelfOrManager(String selfOrManager) {
        this.selfOrManager = selfOrManager;
    }

    public String getSelfOrManagerDD() {
        return "EmployeeUserImport_selfOrManager";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="notes">
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeeUserImport_notes";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "EmployeeUserImport_done";
    }
    //</editor-fold>
}
