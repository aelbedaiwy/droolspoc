
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepCostCenter extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private long dsDBID;

    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }

    public long getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepCostCenter_dsDBID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "RepCostCenter_description";
    }
    
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "RepCostCenter_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    @Column
    @Translatable(translationField = "costCenterGroupDescriptionTranslated")
    private String costCenterGroupDescription;

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }

    public String getCostCenterGroupDescriptionDD() {
        return "RepCostCenter_costCenterGroupDescription";
    }
    
    @Transient
    @Translation(originalField = "costCenterGroupDescription")
    private String costCenterGroupDescriptionTranslated;

    public String getCostCenterGroupDescriptionTranslated() {
        return costCenterGroupDescriptionTranslated;
    }

    public void setCostCenterGroupDescriptionTranslated(String costCenterGroupDescriptionTranslated) {
        this.costCenterGroupDescriptionTranslated = costCenterGroupDescriptionTranslated;
    }
    
    public String getCostCenterGroupDescriptionTranslatedDD() {
        return "RepCostCenter_costCenterGroupDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="legalEntityDescription">
    @Column
    @Translatable(translationField = "legalEntityDescriptionTranslated")
    private String legalEntityDescription;

    public void setLegalEntityDescription(String legalEntityDescription) {
        this.legalEntityDescription = legalEntityDescription;
    }

    public String getLegalEntityDescription() {
        return legalEntityDescription;
    }

    public String getLegalEntityDescriptionDD() {
        return "RepCostCenter_legalEntityDescription";
    }
    @Transient
    @Translation(originalField = "legalEntityDescription")
    private String legalEntityDescriptionTranslated;

    public String getLegalEntityDescriptionTranslated() {
        return legalEntityDescriptionTranslated;
    }

    public void setLegalEntityDescriptionTranslated(String legalEntityDescriptionTranslated) {
        this.legalEntityDescriptionTranslated = legalEntityDescriptionTranslated;
    }
    
    public String getLegalEntityDescriptionTranslatedDD() {
        return "RepCostCenter_legalEntityDescription";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companyID">
    @Column
    private String companyID;

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getCompanyIDDD() {
        return "RepCostCenter_companyID";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD() {
        return "RepCostCenter_code";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="groupCode">
    private String groupCode;

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }
    
    public String getGroupCodeDD() {
        return "RepCostCenter_groupCode";
    }
    //</editor-fold>
}
