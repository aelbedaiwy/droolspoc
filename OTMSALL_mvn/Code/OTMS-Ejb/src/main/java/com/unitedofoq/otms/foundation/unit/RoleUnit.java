/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.ORole;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author mmohamed
 */
@Entity
public class RoleUnit extends BaseEntity {
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ORole orole;
    
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;

    public ORole getOrole() {
        return orole;
    }
    
    public String getOroleDD() {
        return "RoleUnit_orole";
    }

    public void setOrole(ORole orole) {
        this.orole = orole;
    }

    
    public Unit getUnit() {
        return unit;
    }
    
    public String getUnitDD() {
        return "RoleUnit_unit";
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
    
    
    
}
