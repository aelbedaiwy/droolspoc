package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ReqSelectedAppHistory")
public class RequisitionSelectedApplicantHistory extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="commment">

    private String commment;

    public String getCommment() {
        return commment;
    }

    public void setCommment(String commment) {
        this.commment = commment;
    }

    public String getCommmentDD() {
        return "RequisitionSelectedApplicantHistory_commment";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="requisitionSelectedApplicant">
    @JoinColumn(name = "ReqSelApp_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private RequisitionSelectedApplicant requisitionSelectedApplicant;

    public RequisitionSelectedApplicant getRequisitionSelectedApplicant() {
        return requisitionSelectedApplicant;
    }

    public void setRequisitionSelectedApplicant(RequisitionSelectedApplicant requisitionSelectedApplicant) {
        this.requisitionSelectedApplicant = requisitionSelectedApplicant;
    }

    public String getRequisitionSelectedApplicantDD() {
        return "RequisitionSelectedApplicantHistory_requisitionSelectedApplicant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "RequisitionSelectedApplicantHistory_status";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="who">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee who;

    public Employee getWho() {
        return who;
    }

    public void setWho(Employee who) {
        this.who = who;
    }

    public String getWhoDD() {
        return "RequisitionSelectedApplicantHistory_who";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="statusDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date statusDate;

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusDateDD() {
        return "RequisitionSelectedApplicantHistory_statusDate";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="what">
    private String what;

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhatDD() {
        return "RequisitionSelectedApplicantHistory_what";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="statusResult">
    private String statusResult;

    public String getStatusResult() {
        return statusResult;
    }

    public void setStatusResult(String statusResult) {
        this.statusResult = statusResult;
    }

    public String getStatusResultDD() {
        return "RequisitionSelectedApplicantHistory_statusResult";
    }
    //</editor-fold>
}