package com.unitedofoq.otms.payroll.costcenter;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
// @ParentEntity (fields="employee")
@Table(name = "empsapintcostcode")
public class EmployeeSapIntegerationCostCode extends BaseEntity {

   // <editor-fold defaultstate="collapse" desc="employee">
    //  @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeSapIntegerationCostCode_employee";
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapse" desc="sapCostCode">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SapCostCode sapCostCode;

    public SapCostCode getSapCostCode() {
        return sapCostCode;
    }

    public void setSapCostCode(SapCostCode sapCostCode) {
        this.sapCostCode = sapCostCode;
    }

    public String getSapCostCodeDD() {
        return "EmployeeSapIntegerationCostCode_sapCostCode";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="costCodeFunctions">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)

    private CostCodeFunction costCodeFunction;

    public CostCodeFunction getCostCodeFunction() {
        return costCodeFunction;
    }

    public void setCostCodeFunction(CostCodeFunction costCodeFunction) {
        this.costCodeFunction = costCodeFunction;
    }

    public String getCostCodeFunctionDD() {
        return "EmployeeSapIntegerationCostCode_costCodeFunction";
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapse" desc="precentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal precentage;

    public BigDecimal getPrecentage() {
        return precentage;
    }

    public void setPrecentage(BigDecimal precentage) {
        this.precentage = precentage;
    }

    public String getPrecentageDD() {
        return "EmployeeSapIntegerationCostCode_precentage";
    }

    @Transient
    private BigDecimal precentageMask;

    public BigDecimal getPrecentageMask() {
        precentageMask = precentage;
        return precentageMask;
    }

    public void setPrecentageMask(BigDecimal precentageMask) {
        updateDecimalValue("precentage", precentageMask);
    }

    public String getPrecentageMaskDD() {
        return "EmployeeSapIntegerationCostCode_precentageMask";
    }

    // </editor-fold>
}
