/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author lap2
 */
@Entity
@ParentEntity(fields="company")
public class GLMappedAccount extends BaseEntity  {
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "GLMappedAccount_company";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="unit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
    
    public String getUnitDD() {
        return "GLMappedAccount_unit";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="costCenter">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CostCenter costCenter;

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }
    
    public String getCostCenterDD() {
        return "GLMappedAccount_costCenter";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }
    
    public String getLegalEntityDD() {
        return "GLMappedAccount_legalEntity";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade grade;

    public PayGrade getGrade() {
        return grade;
    }

    public void setGrade(PayGrade grade) {
        this.grade = grade;
    }
    
    public String getGradeDD() {
        return "GLMappedAccount_grade";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    
    public String getSalaryElementDD() {
        return "GLMappedAccount_salaryElement";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD() {
        return "GLMappedAccount_employee";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="nationality">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nationality;

    public UDC getNationality() {
        return nationality;
    }

    public void setNationality(UDC nationality) {
        this.nationality = nationality;
    }
    
    public String getNationalityDD() {
        return "GLMappedAccount_nationality";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="hiringType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC hiringType;

    public UDC getHiringType() {
        return hiringType;
    }

    public void setHiringType(UDC hiringType) {
        this.hiringType = hiringType;
    }
    
    public String getHiringTypeDD() {
        return "GLMappedAccount_hiringType";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }
    
    public String getLocationDD() {
        return "GLMappedAccount_location";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="debitAccount">
    private String debitAccount;

    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }
    
    public String getDebitAccountDD() {
        return "GLMappedAccount_debitAccount";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="creditAccount">
    private String creditAccount;

    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }
    
    public String getCreditAccountDD() {
        return "GLMappedAccount_creditAccount";
    }
    //</editor-fold>
    } 
