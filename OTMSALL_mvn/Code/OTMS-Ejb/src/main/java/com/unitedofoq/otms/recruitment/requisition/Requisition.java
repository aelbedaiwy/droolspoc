package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.recruitment.RecruitMatchBase;
import com.unitedofoq.otms.recruitment.vacancy.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"vacancy"})
@ChildEntity(fields = {"costs", "requisitionAdditionalDatas", "requisitionPreferreds", "shortLists"})
public class Requisition extends RecruitMatchBase {

    // <editor-fold defaultstate="collapsed" desc="requisitionAdditionalDatas">
    @OneToMany(mappedBy = "requisition")
    private List<RequisitionAdditionalData> requisitionAdditionalDatas;

    public List<RequisitionAdditionalData> getRequisitionAdditionalDatas() {
        return requisitionAdditionalDatas;
    }

    public void setRequisitionAdditionalDatas(List<RequisitionAdditionalData> requisitionAdditionalDatas) {
        this.requisitionAdditionalDatas = requisitionAdditionalDatas;
    }

    public String getRequisitionAdditionalDatasDD() {
        return "Requisition_requisitionAdditionalDatas";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requisitionPreferreds">
    @OneToMany(mappedBy = "requisition")
    private List<RequisitionPreferred> requisitionPreferreds;

    public List<RequisitionPreferred> getRequisitionPreferreds() {
        return requisitionPreferreds;
    }

    public void setRequisitionPreferreds(List<RequisitionPreferred> requisitionPreferreds) {
        this.requisitionPreferreds = requisitionPreferreds;
    }

    public String getRequisitionPreferredsDD() {
        return "Requisition_requisitionPreferreds";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costs">
    @JoinColumn
    @OneToMany(mappedBy = "requisition")
    private List<RequisitionCost> costs;

    public List<RequisitionCost> getCosts() {
        return costs;
    }

    public void setCosts(List<RequisitionCost> costs) {
        this.costs = costs;
    }

    public String getCostsDD() {
        return "Requisition_costs";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvalDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvalDate;

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalDateDD() {
        return "Requisition_approvalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvedHeadcount">
    @Transient
    private Long approvedHeadcount;

    public Long getApprovedHeadcount() {
        return approvedHeadcount;
    }

    public void setApprovedHeadcount(Long approvedHeadcount) {
        this.approvedHeadcount = approvedHeadcount;
    }

    public String getApprovedHeadcountDD() {
        return "Requisition_approvedHeadcount";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    //@Column(nullable=true)
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "Requisition_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelDate;

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelDateDD() {
        return "Requisition_cancelDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee cancelledBy;//(name = "cancelled_by")

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancelledByDD() {
        return "Requisition_cancelledBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC cancelledReason;

    public UDC getCancelledReason() {
        return cancelledReason;
    }

    public void setCancelledReason(UDC cancelledReason) {
        this.cancelledReason = cancelledReason;
    }

    public String getCancelledReasonDD() {
        return "Requisition_cancelledReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="closeDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date closeDate;

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getCloseDateDD() {
        return "Requisition_closeDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reopenDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date reopenDate;

    public Date getReopenDate() {
        return reopenDate;
    }

    public void setReopenDate(Date reopenDate) {
        this.reopenDate = reopenDate;
    }

    public String getReopenDateDD() {
        return "Requisition_reopenDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="closeReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC closeReason;

    public UDC getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(UDC closeReason) {
        this.closeReason = closeReason;
    }

    public String getCloseReasonDD() {
        return "Requisition_closeReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reopenReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC reopenReason;

    public UDC getReopenReason() {
        return reopenReason;
    }

    public void setReopenReason(UDC reopenReason) {
        this.reopenReason = reopenReason;
    }

    public String getReopenReasonDD() {
        return "Requisition_reopenReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Requisition_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "Requisition_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacancy">
    @JoinColumn//(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Vacancy vacancy;

    public Vacancy getVacancy() {
        return vacancy;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }

    public String getVacancyDD() {
        return "Requisition_vacancy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="headcount">
    @Transient
    private Long headcount;

    public Long getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Long headcount) {
        this.headcount = headcount;
    }

    public String getHeadcountDD() {
        return "Requisition_headcount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expectedSalary">
    @Column(precision = 25, scale = 13)
    private BigDecimal expectedSalary;

    public BigDecimal getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(BigDecimal expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public String getExpectedSalaryDD() {
        return "Requisition_expectedSalary";
    }
    @Transient
    private BigDecimal expectedSalaryMask;

    public BigDecimal getExpectedSalaryMask() {
        expectedSalaryMask = expectedSalary;
        return expectedSalaryMask;
    }

    public void setExpectedSalaryMask(BigDecimal expectedSalaryMask) {
        updateDecimalValue("expectedSalary", expectedSalaryMask);
    }

    public String getExpectedSalaryMaskDD() {
        return "Requisition_expectedSalaryMask";
    }

//        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//        @MeasurableField(name = "expectedSalaryMask.value")
//    private Currency salaryCurrency;
//
//    public Currency getSalaryCurrency() {
//        return salaryCurrency;
//    }
//
//    public void setSalaryCurrency(Currency salaryCurrency) {
//        this.salaryCurrency = salaryCurrency;
//    }
//
//    public String getSalaryCurrencyDD() {
//        return "Requisition_salaryCurrency";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expectedStartDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date expectedStartDate;

    public Date getExpectedStartDate() {
        return expectedStartDate;
    }

    public void setExpectedStartDate(Date expectedStartDate) {
        this.expectedStartDate = expectedStartDate;
    }

    public String getExpectedStartDateDD() {
        return "Requisition_expectedStartDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="acceptancePoints">
    @Column
    private BigDecimal acceptancePoints;

    public BigDecimal getAcceptancePoints() {
        return acceptancePoints;
    }

    public void setAcceptancePoints(BigDecimal acceptancePoints) {
        this.acceptancePoints = acceptancePoints;
    }

    public String getAcceptancePointsDD() {
        return "Requisition_acceptancePoints";
    }
    @Transient
    private BigDecimal acceptancePointsMask;

    public BigDecimal getAcceptancePointsMask() {
        acceptancePointsMask = acceptancePoints;
        return acceptancePointsMask;
    }

    public void setAcceptancePointsMask(BigDecimal acceptancePointsMask) {
        updateDecimalValue("acceptancePoints", acceptancePointsMask);
    }

    public String getAcceptancePointsMaskDD() {
        return "Requisition_acceptancePointsMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDateDD() {
        return "Requisition_fromDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getToDateDD() {
        return "Requisition_toDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestedBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee requestedBy;//(name = "cancelled_by")

    public Employee getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(Employee requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRequestedByDD() {
        return "Requisition_requestedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="closedBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee closedBy;//(name = "cancelled_by")

    public Employee getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(Employee closedBy) {
        this.closedBy = closedBy;
    }

    public String getClosedByDD() {
        return "Requisition_closedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reopenedBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee reopenedBy;//(name = "cancelled_by")

    public Employee getReopenedBy() {
        return reopenedBy;
    }

    public void setReopenedBy(Employee reopenedBy) {
        this.reopenedBy = reopenedBy;
    }

    public String getReopenedByDD() {
        return "Requisition_reopenedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiredHeadcount">
    @Transient
    private Integer hiredHeadcount;

    public Integer getHiredHeadcount() {
        return hiredHeadcount;
    }

    public void setHiredHeadcount(Integer hiredHeadcount) {
        this.hiredHeadcount = hiredHeadcount;
    }

    public String getHiredHeadcountDD() {
        return "Requisition_hiredHeadcount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cost">
    @Column(precision = 25, scale = 13)
    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCostDD() {
        return "Requisition_cost";
    }
    @Transient
    private BigDecimal costMask;

    public BigDecimal getCostMask() {
        costMask = cost;
        return costMask;
    }

    public void setCostMask(BigDecimal costMask) {
        updateDecimalValue("cost", costMask);
    }

    public String getCostMaskDD() {
        return "Requisition_costMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @Transient
    Unit unit;

    public Unit getUnit() {
        Position position = vacancy.getPosition();
        return position.getUnit();
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitDD() {
        return "Requisition_unit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "Requisition_type";
    }
    // </editor-fold>//1-Budgeted,2-Unbudgeted,3-Replacement,4-Unutilized
    // <editor-fold defaultstate="collapsed" desc="fullTime">
    @Column
    private boolean fullTime;

    public boolean isFullTime() {
        return fullTime;
    }

    public void setFullTime(boolean fullTime) {
        this.fullTime = fullTime;
    }

    public String isFullTimeDD() {
        return "Requisition_fullTime";
    }
    // </editor-fold>//Y-FullTime,N-PartTime
    // <editor-fold defaultstate="collapsed" desc="requiredDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date requiredDate;

    public Date getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(Date requiredDate) {
        this.requiredDate = requiredDate;
    }

    public String getRequiredDateDD() {
        return "Requisition_requiredDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="contractYears">
    @Column
    private BigDecimal contractYears;

    public BigDecimal getContractYears() {
        return contractYears;
    }

    public void setContractYears(BigDecimal contractYears) {
        this.contractYears = contractYears;
    }

    public String getContractYearsDD() {
        return "Requisition_contractYears";
    }
    @Transient
    private BigDecimal contractYearsMask;

    public BigDecimal getContractYearsMask() {
        contractYearsMask = contractYears;
        return contractYearsMask;
    }

    public void setContractYearsMask(BigDecimal contractYearsMask) {
        updateDecimalValue("contractYears", contractYearsMask);
    }

    public String getContractYearsMaskDD() {
        return "Requisition_contractYearsMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="partTimeDays">
    @Column
    private BigDecimal partTimeDays;

    public BigDecimal getPartTimeDays() {
        return partTimeDays;
    }

    public void setPartTimeDays(BigDecimal partTimeDays) {
        this.partTimeDays = partTimeDays;
    }

    public String getPartTimeDaysDD() {
        return "Requisition_partTimeDays";
    }
    @Transient
    private BigDecimal partTimeDaysMask;

    public BigDecimal getPartTimeDaysMask() {
        partTimeDaysMask = partTimeDays;
        return partTimeDaysMask;
    }

    public void setPartTimeDaysMask(BigDecimal partTimeDaysMask) {
        updateDecimalValue("partTimeDays", partTimeDaysMask);
    }

    public String getPartTimeDaysMaskDD() {
        return "Requisition_partTimeDaysMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hoursPerDay">
    @Column
    private BigDecimal hoursPerDay;

    public BigDecimal getHoursPerDay() {
        return hoursPerDay;
    }

    public void setHoursPerDay(BigDecimal hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }

    public String getHoursPerDayDD() {
        return "Requisition_hoursPerDay";
    }
    @Transient
    private BigDecimal hoursPerDayMask;

    public BigDecimal getHoursPerDayMask() {
        hoursPerDayMask = hoursPerDay;
        return hoursPerDayMask;
    }

    public void setHoursPerDayMask(BigDecimal hoursPerDayMask) {
        updateDecimalValue("hoursPerDay", hoursPerDayMask);
    }

    public String getHoursPerDayMaskDD() {
        return "Requisition_hoursPerDayMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hoursPerWeek">
    @Column
    private BigDecimal hoursPerWeek;

    public BigDecimal getHoursPerWeek() {
        return hoursPerWeek;
    }

    public void setHoursPerWeek(BigDecimal hoursPerWeek) {
        this.hoursPerWeek = hoursPerWeek;
    }

    public String getHoursPerWeekDD() {
        return "Requisition_hoursPerWeek";
    }
    @Transient
    private BigDecimal hoursPerWeekMask;

    public BigDecimal getHoursPerWeekMask() {
        hoursPerWeekMask = hoursPerWeek;
        return hoursPerWeekMask;
    }

    public void setHoursPerWeekMask(BigDecimal hoursPerWeekMask) {
        updateDecimalValue("hoursPerWeek", hoursPerWeekMask);
    }

    public String getHoursPerWeekMaskDD() {
        return "Requisition_hoursPerWeekMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="secondmentFlag">
    @Column
    private boolean secondmentFlag;

    public boolean isSecondmentFlag() {
        return secondmentFlag;
    }

    public void setSecondmentFlag(boolean secondmentFlag) {
        this.secondmentFlag = secondmentFlag;
    }

    public String isSecondmentFlagDD() {
        return "Requisition_secondmentFlag";
    }
    // </editor-fold>//Y-Yes,N-No
    // <editor-fold defaultstate="collapsed" desc="hostCompany">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company hostCompany;

    public Company getHostCompany() {
        return hostCompany;
    }

    public void setHostCompany(Company hostCompany) {
        this.hostCompany = hostCompany;
    }

    public String getHostCompanyDD() {
        return "Requisition_hostCompany";
    }
        // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="homeCompany">
    //@JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)

    private Company homeCompany;

    public Company getHomeCompany() {
        return homeCompany;
    }

    public void setHomeCompany(Company homeCompany) {
        this.homeCompany = homeCompany;
    }

    public String getHomeCompanyDD() {
        return "Requisition_homeCompany";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="secondmentLength">
    @Column
    private BigDecimal secondmentLength;

    public BigDecimal getSecondmentLength() {
        return secondmentLength;
    }

    public void setSecondmentLength(BigDecimal secondmentLength) {
        this.secondmentLength = secondmentLength;
    }

    public String getSecondmentLengthDD() {
        return "Requisition_secondmentLenth";
    }
    @Transient
    private BigDecimal secondmentLengthMask;

    public BigDecimal getSecondmentLengthMask() {
        secondmentLengthMask = secondmentLength;
        return secondmentLengthMask;
    }

    public void setSecondmentLengthMask(BigDecimal secondmentLengthMask) {
        updateDecimalValue("secondmentLength", secondmentLengthMask);
    }

    public String getSecondmentLengthMaskDD() {
        return "Requisition_secondmentLenthMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costSharing">
    private boolean costSharing;

    public boolean isCostSharing() {
        return costSharing;
    }

    public void setCostSharing(boolean costSharing) {
        this.costSharing = costSharing;
    }

    public String isCostSharingDD() {
        return "Requisition_costSharing";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sumJustification">
    private String sumJustification;

    public String getSumJustification() {
        return sumJustification;
    }

    public void setSumJustification(String sumJustification) {
        this.sumJustification = sumJustification;
    }

    public String getSumJustificationDD() {
        return "Requisition_sumJustification";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="grade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade grade;

    public PayGrade getGrade() {
        return grade;
    }

    public void setGrade(PayGrade grade) {
        this.grade = grade;
    }

    public String getGradeDD() {
        return "Requisition_grade";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryFrom">
    @Column(precision = 25, scale = 13)
    private BigDecimal salaryFrom;

    public BigDecimal getSalaryFrom() {
        return salaryFrom;
    }

    public void setSalaryFrom(BigDecimal salaryFrom) {
        this.salaryFrom = salaryFrom;
    }

    public String getSalaryFromDD() {
        return "Requisition_salaryFrom";
    }
    @Transient
    private BigDecimal salaryFromMask;

    public BigDecimal getSalaryFromMask() {
        salaryFromMask = salaryFrom;
        return salaryFromMask;
    }

    public void setSalaryFromMask(BigDecimal salaryFromMask) {
        updateDecimalValue("salaryFrom", salaryFromMask);
    }

    public String getSalaryFromMaskDD() {
        return "Requisition_salaryFromMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryTo">
    @Column(precision = 25, scale = 13)
    private BigDecimal salaryTo;

    public BigDecimal getSalaryTo() {
        return salaryTo;
    }

    public void setSalaryTo(BigDecimal salaryTo) {
        this.salaryTo = salaryTo;
    }

    public String getSalaryToDD() {
        return "Requisition_salaryTo";
    }
    @Transient
    private BigDecimal salaryToMask;

    public BigDecimal getSalaryToMask() {
        salaryToMask = salaryTo;
        return salaryToMask;
    }

    public void setSalaryToMask(BigDecimal salaryToMask) {
        updateDecimalValue("salaryTo", salaryToMask);
    }

    public String getSalaryToMaskDD() {
        return "Requisition_salaryToMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="strategy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC strategy;

    public UDC getStrategy() {
        return strategy;
    }

    public void setStrategy(UDC strategy) {
        this.strategy = strategy;
    }

    public String getStrategyDD() {
        return "Requisition_strategy";
    }
    // </editor-fold>//E-External,I-Internal
    // <editor-fold defaultstate="collapsed" desc="externalType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC externalType;

    public UDC getExternalType() {
        return externalType;
    }

    public void setExternalType(UDC externalType) {
        this.externalType = externalType;
    }

    public String getExternalTypeDD() {
        return "Requisition_externalType";
    }
    // </editor-fold>//1-Retained Search,2-Contingent Search,3-Advertising
    // <editor-fold defaultstate="collapsed" desc="internalType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC internalType;

    public UDC getInternalType() {
        return internalType;
    }

    public void setInternalType(UDC internalType) {
        this.internalType = internalType;
    }

    public String getInternalTypeDD() {
        return "Requisition_internalType";
    }
    // </editor-fold>//1-Within Own Entity,2-Across Group
    // <editor-fold defaultstate="collapsed" desc="anticipated">
    @Column
    private BigDecimal anticipated;

    public BigDecimal getAnticipated() {
        return anticipated;
    }

    public void setAnticipated(BigDecimal anticipated) {
        this.anticipated = anticipated;
    }

    public String getAnticipatedDD() {
        return "Requisition_anticipated";
    }
    @Transient
    private BigDecimal anticipatedMask;

    public BigDecimal getAnticipatedMask() {
        anticipatedMask = anticipated;
        return anticipatedMask;
    }

    public void setAnticipatedMask(BigDecimal anticipatedMask) {
        updateDecimalValue("anticipated", anticipatedMask);
    }

    public String getAnticipatedMaskDD() {
        return "Requisition_anticipatedMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approved">
    @Column
    private boolean approved;

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getApprovedDD() {
        return "Requisition_approved";
    }
    // </editor-fold>//Y-Yes,N-No
    // <editor-fold defaultstate="collapsed" desc="replacement">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date replacement;

    public Date getReplacement() {
        return replacement;
    }

    public void setReplacement(Date replacement) {
        this.replacement = replacement;
    }

    public String getReplacementDD() {
        return "Requisition_replacementDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "Requisition_status";
    }
    // </editor-fold>//N-New,A-approve,R-reoapen,C-close,E-cancel
    // <editor-fold defaultstate="collapsed" desc="hiringPriority">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC hiringPriority;

    public UDC getHiringPriority() {
        return hiringPriority;
    }

    public void setHiringPriority(UDC hiringPriority) {
        this.hiringPriority = hiringPriority;
    }

    public String getHiringPriorityDD() {
        return "Requisition_hiringPriority";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reportToPosition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position reportToPosition;

    public Position getReportToPosition() {
        return reportToPosition;
    }

    public void setReportToPosition(Position reportToPosition) {
        this.reportToPosition = reportToPosition;
    }

    public String getReportToPositionDD() {
        return "Requisition_reportToPosition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="replacmentFlag">
    @Column
    private String replacmentFlag;

    public String getReplacmentFlag() {
        return replacmentFlag;
    }

    public void setReplacmentFlag(String replacmentFlag) {
        this.replacmentFlag = replacmentFlag;
    }

    public String getReplacmentFlagDD() {
        return "Requisition_replacmentFlag";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="shortLists">

    @OneToMany(mappedBy = "requisition")
    private List<ShortList> shortLists;

    public List<ShortList> getShortLists() {
        return shortLists;
    }

    public void setShortLists(List<ShortList> shortLists) {
        this.shortLists = shortLists;
    }

    public String getShortListsDD() {
        return "Requisition_shortLists";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="shiftBased">
    private boolean shiftBased;

    public boolean isShiftBased() {
        return shiftBased;
    }

    public void setShiftBased(boolean shiftBased) {
        this.shiftBased = shiftBased;
    }

    public String getShiftBasedDD() {
        return "Requisition_shiftBased";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "Requisition_currency";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="certificate1">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC certificate1;

    public UDC getCertificate1() {
        return certificate1;
    }

    public void setCertificate1(UDC certificate1) {
        this.certificate1 = certificate1;
    }

    public String getCertificate1DD() {
        return "Requisition_certificate1";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="certificate2">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC certificate2;

    public UDC getCertificate2() {
        return certificate2;
    }

    public void setCertificate2(UDC certificate2) {
        this.certificate2 = certificate2;
    }

    public String getCertificate2DD() {
        return "Requisition_certificate2";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="residence1">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC residence1;

    public UDC getResidence1() {
        return residence1;
    }

    public void setResidence1(UDC residence1) {
        this.residence1 = residence1;
    }

    public String getResidence1DD() {
        return "Requisition_residence1";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="residence2">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC residence2;

    public UDC getResidence2() {
        return residence2;
    }

    public void setResidence2(UDC residence2) {
        this.residence2 = residence2;
    }

    public String getResidence2DD() {
        return "Requisition_residence2";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="requiredNumber">
    private int requiredNumber;

    public int getRequiredNumber() {
        return requiredNumber;
    }

    public void setRequiredNumber(int requiredNumber) {
        this.requiredNumber = requiredNumber;
    }

    public String getRequiredNumberDD() {
        return "Requisition_requiredNumber";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "Requisition_comments";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ageFrom">
    private BigDecimal ageFrom;

    public BigDecimal getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(BigDecimal ageFrom) {
        this.ageFrom = ageFrom;
    }

    public String getAgeFromDD() {
        return "Requisition_ageFrom";
    }
    @Transient
    private BigDecimal ageFromMask;

    public BigDecimal getAgeFromMask() {
        ageFromMask = ageFrom;
        return ageFromMask;
    }

    public void setAgeFromMask(BigDecimal ageFromMask) {
        updateDecimalValue("ageFrom", ageFromMask);
    }

    public String getAgeFromMaskDD() {
        return "Requisition_ageFromMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ageTo">
    private BigDecimal ageTo;

    public BigDecimal getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(BigDecimal ageTo) {
        this.ageTo = ageTo;
    }

    public String getAgeToDD() {
        return "Requisition_ageTo";
    }

    @Transient
    private BigDecimal ageToMask;

    public BigDecimal getAgeToMask() {
        ageToMask = ageTo;
        return ageToMask;
    }

    public void setAgeToMask(BigDecimal ageToMask) {
        updateDecimalValue("ageTo", ageToMask);
    }

    public String getAgeToMaskDD() {
        return "Requisition_ageToMask";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="educationCategory">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC educationCategory;

    public UDC getEducationCategory() {
        return educationCategory;
    }

    public void setEducationCategory(UDC educationCategory) {
        this.educationCategory = educationCategory;
    }

    public String getEducationCategoryDD() {
        return "Requisition_educationCategory";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="appliedAppNum">
    private Integer appliedAppNum;

    public Integer getAppliedAppNum() {
        return appliedAppNum;
    }

    public void setAppliedAppNum(Integer appliedAppNum) {
        this.appliedAppNum = appliedAppNum;
    }

    public String getAppliedAppNumDD() {
        return "Requisition_appliedAppNum";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="budgetedSalary">
    @Column(precision = 25, scale = 13)
    private BigDecimal budgetedSalary;

    public BigDecimal getBudgetedSalary() {
        return budgetedSalary;
    }

    public void setBudgetedSalary(BigDecimal budgetedSalary) {
        this.budgetedSalary = budgetedSalary;
    }

    public String getBudgetedSalaryDD() {
        return "Requisition_budgetedSalary";
    }

    @Transient
    private BigDecimal budgetedSalaryMask = java.math.BigDecimal.ZERO;

    public BigDecimal getBudgetedSalaryMask() {
        budgetedSalaryMask = budgetedSalary;
        return budgetedSalaryMask;
    }

    public void setBudgetedSalaryMask(BigDecimal budgetedSalaryMask) {
        updateDecimalValue("budgetedSalary", budgetedSalaryMask);
    }

    public String getBudgetedSalaryMaskDD() {
        return "Requisition_budgetedSalaryMask";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="internal start date">
    @Temporal(TemporalType.DATE)
    private Date internalStartDate;

    public Date getInternalStartDate() {
        return internalStartDate;
    }

    public void setInternalStartDate(Date internalStartDate) {
        this.internalStartDate = internalStartDate;
    }

    public String getInternalStartDateDD() {
        return "Requisition_internalStartDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="internal end date">
    @Temporal(TemporalType.DATE)
    private Date internalEndDate;

    public Date getInternalEndDate() {
        return internalEndDate;
    }

    public void setInternalEndDate(Date internalEndDate) {
        this.internalEndDate = internalEndDate;
    }

    public String getInternalEndDateDD() {
        return "Requisition_internalEndDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="external start date">
    @Temporal(TemporalType.DATE)
    private Date externalStartDate;

    public Date getExternalStartDate() {
        return externalStartDate;
    }

    public void setExternalStartDate(Date externalStartDate) {
        this.externalStartDate = externalStartDate;
    }

    public String getExternalStartDateDD() {
        return "Requisition_externalStartDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="external end date">
    @Temporal(TemporalType.DATE)
    private Date externalEndDate;

    public Date getExternalEndDate() {
        return externalEndDate;
    }

    public void setExternalEndDate(Date externalEndDate) {
        this.externalEndDate = externalEndDate;
    }

    public String getExternalEndDateDD() {
        return "Requisition_externalEndDate";
    }
//</editor-fold>
}
