package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.otms.reports.views.RepEmployeeBaseTranslation;
import javax.persistence.*;

@Entity
@Table(name = "repempelerattranslation")
public class RepEmployeeElementRatingTranslation extends RepEmployeeBaseTranslation {

    // <editor-fold defaultstate="collapsed" desc="element">
    @Column
    private String element;

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }
    // </editor-fold>
}
