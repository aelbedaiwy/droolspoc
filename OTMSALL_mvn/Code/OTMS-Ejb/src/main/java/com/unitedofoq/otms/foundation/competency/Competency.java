/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.competency;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author arezk
 */

@Entity
@ChildEntity(fields={"levels","competencyTrainingCourses"})
@ParentEntity(fields="competencyGroup")
//@Table(name="EDSCOMPETENCY")
public class Competency extends CompetencyBase{

    @Override
    protected void PostLoad() {
        super.PostLoad();
        setLevelIndex();
    }
    
    // <editor-fold defaultstate="collapsed" desc="competencyTrainingCourses">
    @OneToMany(mappedBy = "competency")
    private List<CompetencyTrainingCourse> competencyTrainingCourses;

    public List<CompetencyTrainingCourse> getCompetencyTrainingCourses() {
        return competencyTrainingCourses;
    }

    public void setCompetencyTrainingCourses(List<CompetencyTrainingCourse> competencyTrainingCourses) {
        this.competencyTrainingCourses = competencyTrainingCourses;
    }

    public String getCompetencyTrainingCoursesDD() {
        return "Competency_competencyTrainingCourses";
    }

    @OneToMany(mappedBy="competency")
    private List<CompetencyLearningResource> competencyLearningResources;
    
    public List<CompetencyLearningResource> getCompetencyLearningResources() {
        return competencyLearningResources;
    }

    public void setCompetencyLearningResources(List<CompetencyLearningResource> competencyLearningResources) {
        this.competencyLearningResources = competencyLearningResources;
    }
    // </editor-fold>
    /*
    // <editor-fold defaultstate="collapsed" desc="gradingSchema">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private GradingSchema gradingSchema;

    public GradingSchema getGradingSchema() {
        return gradingSchema;
    }

    public void setGradingSchema(GradingSchema gradingSchema) {
        this.gradingSchema = gradingSchema;
    }

    public String getGradingSchemaDD() {
        return "Competency_gradingSchema";
    }
    // </editor-fold>
    */
    
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "CompetencyBase_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gradingSchema">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private ScaleType gradingSchema;

    public ScaleType getGradingSchema() {
        return gradingSchema;
    }

    public void setGradingSchema(ScaleType gradingSchema) {
        this.gradingSchema = gradingSchema;
    }

   
    public String getGradingSchemaDD() {
        return "CompetencyBase_gradingSchema";
    } 
 // </editor-fold>

    private void setLevelIndex(){
        for (CompetencyLevel level : getLevels()) {
            switch(level.getSortIndex()){
                    case 1:
                     ///   level1 = level;
                    case 2:
                      //  level2 = level;
                    case 3:
                      //  level3 = level;
                    case 4:
                      //  level4 = level;
                    case 5:
                      //  level5 = level;
            }
        }
    }

    public int getLevelOrder(CompetencyLevel level){
        return level.getSortIndex();
    }
    
    private Double weight;

    public Double getWeight() {
        return weight;
}

    public String getWeightDD() {
        return "Competency_weight";
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
    
    @ManyToOne
    private UDC competencyType;

    public UDC getCompetencyType() {
        return competencyType;
    }

    public String getCompetencyTypeDD() {
        return "Competency_competencyType";
    }

    public void setCompetencyType(UDC competencyType) {
        this.competencyType = competencyType;
    }
    
    
    
    
}
