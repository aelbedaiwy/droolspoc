/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.eds.foundation.employee.EDSCompetencyAssessment;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="competency")
@ChildEntity(fields="indicators")
@Table(name="CompetencyLevelBase")
public class EDSCompetencyLevel extends BaseEntity{
    @OneToMany(mappedBy = "competencyLevel")
    private List<EDSCompetencyAssessment> edsCompetencyAssessments;

    public List<EDSCompetencyAssessment> getEdsCompetencyAssessments() {
        return edsCompetencyAssessments;
    }

    public void setEdsCompetencyAssessments(List<EDSCompetencyAssessment> edsCompetencyAssessments) {
        this.edsCompetencyAssessments = edsCompetencyAssessments;
    }

    public String getEdsCompetencyAssessmentsDD() {
        return "EDSCompetencyLevel_edsCompetencyAssessments";
    }


    @Column(length=1000)
    private String behaviorIndicators;
    public String getBehaviorIndicatorsDD() {
        return "EDSCompetencyLevel_behaviorIndicators";
    }

    public String getBehaviorIndicators() {
        return behaviorIndicators;
    }

    public void setBehaviorIndicators(String behaviorIndicators) {
        this.behaviorIndicators = behaviorIndicators;
    }

    @Column(nullable=false)
    private int sortIndex;
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;

    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetency competency;
    @OneToMany(mappedBy="level")
    private List<EDSCompetencyLevelBehavior> indicators;

    private int rankValue;

    public String getRankValueDD() {
        return "EDSCompetencyLevel_rankValue";
    }

    public int getRankValue() {
        return rankValue;
    }

    public void setRankValue(int rankValue) {
        this.rankValue = rankValue;
    }


    public String getNameTranslatedDD()       {  return "EDSCompetencyLevel_name";  }
    public String getDescriptionTranslatedDD(){  return "EDSCompetencyLevel_description";  }
    public String getSortIndexDD()  {  return "EDSCompetencyLevel_sortIndex";  }
    public String getCompetencyDD() {  return "EDSCompetencyLevel_competency";  }

    public EDSCompetency getCompetency() {
        return competency;
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EDSCompetencyLevelBehavior> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<EDSCompetencyLevelBehavior> indicators) {
        this.indicators = indicators;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
}
