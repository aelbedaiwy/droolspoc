/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.job;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Behavior;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"job"})
public class JobBeheviour extends BusinessObjectBaseEntity {
   // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Job job;

    public Job getJob() {
        return job;
    }
    public String getJobDD() {
        return "JobBeheviour_job";
    }
    public void setJob(Job job) {
        this.job = job;
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="behavior">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Behavior behavior;

    public Behavior getBehavior() {
        return behavior;
    }
    public String getBehaviorDD() {
        return "JobBeheviour_behavior";
    }
    public void setBehavior(Behavior behavior) {
        this.behavior = behavior;
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="score">
   @Column
    private double score;

    public double getScore() {
        return score;
    }
    public String getScoreDD() {
        return "JobBeheviour_score";
    }
    public void setScore(double score) {
        this.score = score;
    }

   // </editor-fold>
}
