/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author abayomy
 */
@Entity
@DiscriminatorValue("DEDUCTION")
@ParentEntity(fields={"penalty"})
//@FABSEntitySpecs(hideInheritedFields={"salaryElement"})
//@Table(name = "penalty_affecting_deduction")
public class PenaltyAffectingDeduction extends PenaltyAffectingSalaryElement {

  @JoinColumn//(name="SALARYELEMENT_DBID")
   @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
   private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
   public String getDeductionDD() {
        return "PenaltyAffectingDeduction_deduction";
    }

}
