package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.otms.recruitment.onlineexam.Answer;
import com.unitedofoq.otms.recruitment.onlineexam.MCQuestion;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class ApplicantMCQAnswer extends ApplicantAnswer {

    // <editor-fold defaultstate="collapsed" desc="question">

    @OneToOne
    private MCQuestion question;

    public MCQuestion getQuestion() {
        return question;
    }

    public void setQuestion(MCQuestion question) {
        this.question = question;
    }

    public String getQuestionDD() {
        return "ApplicantAnswer_question";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="mcqAnswer">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Answer mcqAnswer;

    public Answer getMcqAnswer() {
        return mcqAnswer;
    }

    public void setMcqAnswer(Answer mcqAnswer) {
        this.mcqAnswer = mcqAnswer;
    }

    public String getMcqAnswerDD() {
        return "ApplicantAnswer_mcqAnswer";
    }
    // </editor-fold>

}
