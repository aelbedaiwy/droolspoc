package com.unitedofoq.otms.recruitment.vacancy;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.recruitment.RecruitMatchBase;
import com.unitedofoq.otms.recruitment.requisition.Requisition;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"position"})
@ChildEntity(fields = {"requisitions"})
public class Vacancy extends RecruitMatchBase {
    // <editor-fold defaultstate="collapsed" desc="requisitions">

    @OneToMany
    private List<Requisition> requisitions;

    public List<Requisition> getRequisitions() {
        return requisitions;
    }

    public String getRequisitionsDD() {
        return "Vacancy_requisitions";
    }

    public void setRequisitions(List<Requisition> requisitions) {
        this.requisitions = requisitions;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "Vacancy_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="headcount">
    @Column(nullable = false)
    private Long headcount;

    public Long getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Long headcount) {
        this.headcount = headcount;
    }

    public String getHeadcountDD() {
        return "Vacancy_headcount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="creationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDateDD() {
        return "Vacancy_creationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastModificationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationDate;

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public String getLastModificationDateDD() {
        return "Vacancy_lastModificationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="modificationType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC modificationType;

    public UDC getModificationType() {
        return modificationType;
    }

    public void setModificationType(UDC modificationType) {
        this.modificationType = modificationType;
    }

    public String getModificationTypeDD() {
        return "Vacancy_modificationType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="modificationReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC modificationReason;

    public UDC getModificationReason() {
        return modificationReason;
    }

    public void setModificationReason(UDC modificationReason) {
        this.modificationReason = modificationReason;
    }

    public String getModificationReasonDD() {
        return "Vacancy_modificationReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="acceptedApplicants">
    @Transient
    private Long acceptedApplicants;

    public Long getAcceptedApplicants() {
        return acceptedApplicants;
    }

    public void setAcceptedApplicants(Long acceptedApplicants) {
        this.acceptedApplicants = acceptedApplicants;
    }

    public String getAcceptedApplicantsDD() {
        return "Vacancy_acceptedApplicants";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="job">
    @Transient
    private Job job;

    public Job getJob() {
        job = position.getJob();
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getJobDD() {
        return "Vacancy_job";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    // @JoinColumn (nullable=false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "Vacancy_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reservedHeadcount">
    @Transient
    private Long reservedHeadcount;

    public Long getReservedHeadcount() {
        return reservedHeadcount;
    }

    public void setReservedHeadcount(Long reservedHeadcount) {
        this.reservedHeadcount = reservedHeadcount;
    }

    public String getReservedHeadcountDD() {
        return "Vacancy_reservedHeadcount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="acceptancePoints">
    @Column
    private BigDecimal acceptancePoints;

    public BigDecimal getAcceptancePoints() {
        return acceptancePoints;
    }

    public void setAcceptancePoints(BigDecimal acceptancePoints) {
        this.acceptancePoints = acceptancePoints;
    }

    public String getAcceptancePointsDD() {
        return "Vacancy_acceptancePoints";
    }
    @Transient
    private BigDecimal acceptancePointsMask;

    public BigDecimal getAcceptancePointsMask() {
        acceptancePointsMask = acceptancePoints;
        return acceptancePointsMask;
    }

    public void setAcceptancePointsMask(BigDecimal acceptancePointsMask) {
        updateDecimalValue("acceptancePoints", acceptancePointsMask);
        this.acceptancePoints = acceptancePointsMask;
    }

    public String getAcceptancePointsMaskDD() {
        return "Vacancy_acceptancePointsMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="source">
    // @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC source;

    public UDC getSource() {
        return source;
    }

    public void setSource(UDC source) {
        this.source = source;
    }

    public String getSourceDD() {
        return "Vacancy_source";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reason">
    // @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC reason;

    public UDC getReason() {
        return reason;
    }

    public void setReason(UDC reason) {
        this.reason = reason;
    }

    public String getReasonDD() {
        return "Vacancy_reason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "Vacancy_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "Vacancy_type";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="openedReqNum">
    private Integer openedReqNum;

    public Integer getOpenedReqNum() {
        return openedReqNum;
    }

    public void setOpenedReqNum(Integer openedReqNum) {
        this.openedReqNum = openedReqNum;
    }

    public String getOpenedReqNumDD() {
        return "Vacancy_openedReqNum";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="appliedAppNum">
    private Integer appliedAppNum;

    public Integer getAppliedAppNum() {
        return appliedAppNum;
    }

    public void setAppliedAppNum(Integer appliedAppNum) {
        this.appliedAppNum = appliedAppNum;
    }

    public String getAppliedAppNumDD() {
        return "Vacancy_appliedAppNum";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date hiringDate;

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getHiringDateDD() {
        return "Vacancy_hiringDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeName">
    private String employeeName;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNameDD() {
        return "Vacancy_employeeName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeCode">
    private String employeeCode;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeCodeDD() {
        return "Vacancy_employeeCode";
    }
    // </editor-fold>
    // Loubna - MedMark Reqruitment - 26/12/2013
    // <editor-fold defaultstate="collapsed" desc="notes">
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "Vacancy_notes";
    }
    // </editor-fold>
}
