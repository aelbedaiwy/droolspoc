/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mostafa
 */
@Entity
public class BPAttachment extends BaseEntity {
    
   private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionDD() {
        return "BPAttachment_description";
    }
    
   @Column
   @Temporal(TemporalType.TIMESTAMP)
   private Date attachDate;

    public Date getAttachDate() {
        return attachDate;
    }

    public void setAttachDate(Date attachDate) {
        this.attachDate = attachDate;
    }
    
     public String getAttachDateDD() {
        return "BPAttachment_attachDate";
    }
   
    
}
