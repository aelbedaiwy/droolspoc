
package com.unitedofoq.otms.appraisal.reports;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepTemplateElementTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="element">
    @Column
    private String element;

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ratingDescription">
    @Column
    private String ratingDescription;

    public void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription;
    }

    public String getRatingDescription() {
        return ratingDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="elementType">
    @Column
    private String elementType;

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementType() {
        return elementType;
    }
    // </editor-fold>

}
