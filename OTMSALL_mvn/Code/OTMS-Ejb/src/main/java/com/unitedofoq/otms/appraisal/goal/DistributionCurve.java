package com.unitedofoq.otms.appraisal.goal;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplateSequence;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class DistributionCurve extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="sequence">
    @ManyToOne
    private AppraisalTemplateSequence sequence;

    public AppraisalTemplateSequence getSequence() {
        return sequence;
    }

    public String getSequenceDD() {
        return "GoalDistributionCurve_sequence";
    }

    public void setSequence(AppraisalTemplateSequence sequence) {
        this.sequence = sequence;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="level">
    @ManyToOne
    private ScaleTypeRule level;

    public ScaleTypeRule getLevel() {
        return level;
    }

    public String getLevelDD() {
        return "GoalDistributionCurve_level";
    }

    public void setLevel(ScaleTypeRule level) {
        this.level = level;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="setup">
    private double setup;

    public double getSetup() {
        return setup;
    }

    public String getSetupDD() {
        return "GoalDistributionCurve_setup";
    }

    public void setSetup(double setup) {
        this.setup = setup;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="bonus">
    private double bonus;

    public double getBonus() {
        return bonus;
    }

    public String getBonusDD() {
        return "GoalDistributionCurve_bonus";
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "SequenceRatingLevel_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "SequenceRatingLevel_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="comments">
    @Column
    private String comments;

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public String getCommentsDD() {
        return "SequenceRatingLevel_comments";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="equivalentScore">
    @Column
    private BigDecimal equivalentScore;

    public void setEquivalentScore(BigDecimal equivalentScore) {
        this.equivalentScore = equivalentScore;
    }

    public BigDecimal getEquivalentScore() {
        return equivalentScore;
    }

    public String getEquivalentScoreDD() {
        return "SequenceRatingLevel_equivalentScore";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="finalScoreFrom">
    @Column
    private BigDecimal finalScoreFrom;

    public void setFinalScoreFrom(BigDecimal finalScoreFrom) {
        this.finalScoreFrom = finalScoreFrom;
    }

    public BigDecimal getFinalScoreFrom() {
        return finalScoreFrom;
    }

    public String getFinalScoreFromDD() {
        return "SequenceRatingLevel_finalScoreFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="finalScoreTo">
    @Column
    private BigDecimal finalScoreTo;

    public void setFinalScoreTo(BigDecimal finalScoreTo) {
        this.finalScoreTo = finalScoreTo;
    }

    public BigDecimal getFinalScoreTo() {
        return finalScoreTo;
    }

    public String getFinalScoreToDD() {
        return "SequenceRatingLevel_finalScoreTo";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="distribution">
    @Column
    private Double distribution;

    public void setDistribution(Double distribution) {
        this.distribution = distribution;
    }

    public Double getDistribution() {
        return distribution;
    }

    public String getDistributionDD() {
        return "SequenceRatingLevel_distribution";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="meritIncreasePercentage">
    @Column
    private BigDecimal meritIncreasePercentage;

    public void setMeritIncreasePercentage(BigDecimal meritIncreasePercentage) {
        this.meritIncreasePercentage = meritIncreasePercentage;
    }

    public BigDecimal getMeritIncreasePercentage() {
        return meritIncreasePercentage;
    }

    public String getMeritIncreasePercentageDD() {
        return "SequenceRatingLevel_meritIncreasePercentage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="meritIncreaseVal">
    @Column
    private BigDecimal meritIncreaseVal;

    public void setMeritIncreaseVal(BigDecimal meritIncreaseVal) {
        this.meritIncreaseVal = meritIncreaseVal;
    }

    public BigDecimal getMeritIncreaseVal() {
        return meritIncreaseVal;
    }

    public String getMeritIncreaseValDD() {
        return "SequenceRatingLevel_meritIncreaseVal";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualDistribution">
    @Column
    private BigDecimal actualDistribution;

    public void setActualDistribution(BigDecimal actualDistribution) {
        this.actualDistribution = actualDistribution;
    }

    public BigDecimal getActualDistribution() {
        return actualDistribution;
    }

    public String getActualDistributionDD() {
        return "SequenceRatingLevel_actualDistribution";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualDistributionNumber">
    @Column
    private Integer actualDistributionNumber;

    public void setActualDistributionNumber(Integer actualDistributionNumber) {
        this.actualDistributionNumber = actualDistributionNumber;
    }

    public Integer getActualDistributionNumber() {
        return actualDistributionNumber;
    }

    public String getActualDistributionNumberDD() {
        return "SequenceRatingLevel_actualDistributionNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualMeritIncreasePercentage">
    @Column
    private BigDecimal actualMeritIncreasePercentage;

    public void setActualMeritIncreasePercentage(BigDecimal actualMeritIncreasePercentage) {
        this.actualMeritIncreasePercentage = actualMeritIncreasePercentage;
    }

    public BigDecimal getActualMeritIncreasePercentage() {
        return actualMeritIncreasePercentage;
    }

    public String getActualMeritIncreasePercentageDD() {
        return "SequenceRatingLevel_actualMeritIncreasePercentage";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualMeritIncreaseVal">
    @Column
    private BigDecimal actualMeritIncreaseVal;

    public void setActualMeritIncreaseVal(BigDecimal actualMeritIncreaseVal) {
        this.actualMeritIncreaseVal = actualMeritIncreaseVal;
    }

    public BigDecimal getActualMeritIncreaseVal() {
        return actualMeritIncreaseVal;
    }

    public String getActualMeritIncreaseValDD() {
        return "SequenceRatingLevel_actualMeritIncreaseVal";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="scaleType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleType scaleType;

    public ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    public String getScaleTypeDD() {
        return "SequenceRatingLevel_scaleType";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="bonusPercentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal bonusPercentage;

    public BigDecimal getBonusPercentage() {
        return bonusPercentage;
    }

    public void setBonusPercentage(BigDecimal bonusPercentage) {
        this.bonusPercentage = bonusPercentage;
    }

    public String getBonusPercentageDD() {
        return "SequenceRatingLevel_bonusPercentage";
    }

    @Transient
    private BigDecimal bonusPercentageMask;

    public BigDecimal getBonusPercentageMask() {
        bonusPercentageMask = bonusPercentage;
        return bonusPercentageMask;
    }

    public void setBonusPercentageMask(BigDecimal bonusPercentageMask) {
        updateDecimalValue("bonusPercentage", bonusPercentageMask);
    }

    public String getBonusPercentageMaskDD() {
        return "SalaryElement_bonusPercentageMask";
    }
    //</editor-fold>

    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeKPA_m2mCheck";
    }

    @Override
    public void PostLoad() {
        super.PostLoad();
        setM2mCheck(true);
    }

}
