
package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.*;

@Entity
@Table(name="UnitLevel")
public class UnitLevelPlan extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "UnitLevelPlan_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="company_dbid">
    @Column
    private Long company_dbid;

    public void setCompany_dbid(Long company_dbid) {
        this.company_dbid = company_dbid;
    }

    public Long getCompany_dbid() {
        return company_dbid;
    }

    public String getCompany_dbidDD() {
        return "UnitLevelPlan_company_dbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="levelId">
    @Column
    private String levelId;

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getLevelId() {
        return levelId;
    }

    public String getLevelIdDD() {
        return "UnitLevelPlan_levelId";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="originalLevel_dbid">
    @Column
    private Long originalLevel_dbid;

    public void setOriginalLevel_dbid(Long originalLevel_dbid) {
        this.originalLevel_dbid = originalLevel_dbid;
    }

    public Long getOriginalLevel_dbid() {
        return originalLevel_dbid;
    }

    public String getOriginalLevel_dbidDD() {
        return "UnitLevelPlan_originalLevel_dbid";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="DTYPE">
    private String DTYPE = "MASTER";

    public String getDTYPE() {
        return DTYPE;
    }

    public void setDTYPE(String DTYPE) {
        this.DTYPE = DTYPE;
    }
    
    public String getDTYPEDD() {
        return "UnitLevelPlan_DTYPE";
    }
    //</editor-fold>
}