/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.eds.foundation.employee.idp.EDSIDPBusinessOutcomeChannel;
import com.unitedofoq.otms.eds.foundation.employee.idp.EDSIDPBusinessOutcomeForOutLrnIntr;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
public class EDSChannel extends BusinessObjectBaseEntity{
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    private String samples;
    private String tutorial;
    @OneToMany(mappedBy="channel")
    private List<EDSIDPBusinessOutcomeChannel> businessOutcomes;
    @OneToMany(mappedBy="channel")
    private List<EDSIDPBusinessOutcomeForOutLrnIntr> lrnIntrs;
    public String getLrnIntrsDD()    {  return "EDSChannel_lrnIntrs";  }

    public List<EDSIDPBusinessOutcomeForOutLrnIntr> getLrnIntrs() {
        return lrnIntrs;
    }

    public void setLrnIntrs(List<EDSIDPBusinessOutcomeForOutLrnIntr> lrnIntrs) {
        this.lrnIntrs = lrnIntrs;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD()    {  return "EDSChannel_name";  }
    public String getSamplesDD() {  return "EDSChannel_samples";  }
    public String getTutorialDD(){  return "EDSChannel_tutorial";  }
    public String getBusinessOutcomesDD() {  return "EDSChannel_businessOutcomes";  }

    public List<EDSIDPBusinessOutcomeChannel> getBusinessOutcomes() {
        return businessOutcomes;
    }

    public void setBusinessOutcomes(List<EDSIDPBusinessOutcomeChannel> businessOutcomes) {
        this.businessOutcomes = businessOutcomes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSamples() {
        return samples;
    }

    public void setSamples(String samples) {
        this.samples = samples;
    }

    public String getTutorial() {
        return tutorial;
    }

    public void setTutorial(String tutorial) {
        this.tutorial = tutorial;
    }
    
    private String code;

    public String getCode() {
        return code;
    }
    public String getCodeDD() {
        return "EDSChannel_code";
    }

    public void setCode(String code) {
        this.code = code;
    }
        
}
