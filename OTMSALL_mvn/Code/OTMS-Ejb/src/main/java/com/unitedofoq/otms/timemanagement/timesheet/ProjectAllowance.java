/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields="projectNature")
public class ProjectAllowance extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="projectNature">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProjectNature projectNature;

    public ProjectNature getProjectNature() {
        return projectNature;
    }
    public String getProjectNatureDD() {
        return "ProjectAllowance_projectNature";
    }

    public void setProjectNature(ProjectNature projectNature) {
        this.projectNature = projectNature;
    }    
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="allowance">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Allowance allowance;

    public Allowance getAllowance() {
        return allowance;
    }
    public String getAllowanceDD() {
        return "ProjectAllowance_allowance";
    }

    public void setAllowance(Allowance allowance) {
        this.allowance = allowance;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="defaultValue">
    @Column(precision=25, scale=13)
    private BigDecimal defaultValue;

    public BigDecimal getDefaultValue() {
        return defaultValue;
    }
    public String getDefaultValueDD() {
        return "ProjectAllowance_defaultValue";
    }

    public void setDefaultValue(BigDecimal defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    @Transient
    private BigDecimal defaultValueMask;

    public BigDecimal getDefaultValueMask() {
        defaultValueMask = defaultValue;
        return defaultValueMask;
    }
    public String getDefaultValueMaskDD() {
        return "ProjectAllowance_defaultValueMask";
    }

    public void setDefaultValueMask(BigDecimal defaultValueMask) {
        updateDecimalValue("defaultValue", defaultValueMask);
    }
    // </editor-fold>
}
