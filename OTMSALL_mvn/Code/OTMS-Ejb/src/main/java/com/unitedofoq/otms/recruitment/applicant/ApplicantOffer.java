package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"applicant"})
public class ApplicantOffer extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppliedApplicant applicant;

    public AppliedApplicant getApplicant() {
        return applicant;
    }

    public void setApplicant(AppliedApplicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantOffer_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="offerDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date offerDate;

    public Date getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(Date offerDate) {
        this.offerDate = offerDate;
    }

    public String getOfferDateDD() {
        return "ApplicantOffer_offerDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="offeredBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee offeredBy;

    public Employee getOfferedBy() {
        return offeredBy;
    }

    public void setOfferedBy(Employee offeredBy) {
        this.offeredBy = offeredBy;
    }

    public String getOfferedByDD() {
        return "ApplicantOffer_offeredBy";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="offerStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC offerStatus; // Draft - Negotiation - Finalized

    public String getOfferStatusDD() {
        return "ApplicantOffer_offerStatus";
    }

    public UDC getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(UDC offerStatus) {
        this.offerStatus = offerStatus;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activeFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date activeFrom;

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public String getActiveFromDD() {
        return "ApplicantOffer_activeFrom";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salary">
    @Column(precision = 25, scale = 13)
    private BigDecimal salary;

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public String getSalaryDD() {
        return "ApplicantOffer_salary";
    }
    @Transient
    private BigDecimal salaryMask;

    public BigDecimal getSalaryMask() {
        salaryMask = salary;
        return salaryMask;
    }

    public void setSalaryMask(BigDecimal salaryMask) {
        updateDecimalValue("salary", salaryMask);
    }

    public String getSalaryMaskDD() {
        return "ApplicantOffer_salaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="respondBeforeDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date respondBeforeDate;

    public Date getRespondBeforeDate() {
        return respondBeforeDate;
    }

    public void setRespondBeforeDate(Date respondBeforeDate) {
        this.respondBeforeDate = respondBeforeDate;
    }

    public String getRespondBeforeDateDD() {
        return "ApplicantOffer_respondBeforeDate";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column(nullable = true)
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "ApplicantOffer_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingHours">
    private float workingHours;

    public float getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(float workingHours) {
        this.workingHours = workingHours;
    }

    public String getWorkingHoursDD() {
        return "ApplicantOffer_workingHours";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="probationPeriod">
    private float probationPeriod;

    public float getProbationPeriod() {
        return probationPeriod;
    }

    public void setProbationPeriod(float probationPeriod) {
        this.probationPeriod = probationPeriod;
    }

    public String getProbationPeriodDD() {
        return "ApplicantOffer_probationPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reportingTo">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee reportingTo;

    public Employee getReportingTo() {
        return reportingTo;
    }

    public void setReportingTo(Employee reportingTo) {
        this.reportingTo = reportingTo;
    }

    public String getReportingToDD() {
        return "ApplicantOffer_reportingTo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="benifits">
    private String benifits;

    public String getBenifits() {
        return benifits;
    }

    public void setBenifits(String benifits) {
        this.benifits = benifits;
    }

    public String getBenifitsDD() {
        return "ApplicantOffer_benifits";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="responsibilities">
    private String responsibilities;

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getResponsibilitiesDD() {
        return "ApplicantOffer_responsibilities";
    }
    // </editor-fold>
}
