
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.*;

@Entity
@ParentEntity(fields="company")
public class TimeManagementMachine extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "TimeManagementMachine_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "TimeManagementMachine_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="secuirtyOrAttendance">
    @Column
    private String secuirtyOrAttendance;

    public void setSecuirtyOrAttendance(String secuirtyOrAttendance) {
        this.secuirtyOrAttendance = secuirtyOrAttendance;
    }

    public String getSecuirtyOrAttendance() {
        return secuirtyOrAttendance;
    }

    public String getSecuirtyOrAttendanceDD() {
        return "TimeManagementMachine_secuirtyOrAttendance";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="inOrOut">
    @Column
    private String inOrOut;

    public void setInOrOut(String inOrOut) {
        this.inOrOut = inOrOut;
    }

    public String getInOrOut() {
        return inOrOut;
    }

    public String getInOrOutDD() {
        return "TimeManagementMachine_inOrOut";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public void setLocation(UDC location) {
        this.location = location;
    }

    public UDC getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "TimeManagementMachine_location";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "TimeManagementMachine_company";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="portNumber">
    private String portNumber;

    public String getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(String portNumber) {
        this.portNumber = portNumber;
    }

    public String getPortNumberDD() {
        return "TimeManagementMachine_portNumber";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="IPAddress">
    private String IPAddress;

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public String getIPAddressDD() {
        return "TimeManagementMachine_IPAddress";
    }
    // </editor-fold>
}
