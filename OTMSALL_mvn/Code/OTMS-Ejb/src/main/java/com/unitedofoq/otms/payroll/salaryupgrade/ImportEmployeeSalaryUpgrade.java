/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.salaryupgrade;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author lahmed
 */
@Entity
public class ImportEmployeeSalaryUpgrade extends BaseEntity
{
    //<editor-fold defaultstate="collapsed" desc="salaryElement">
    private String income;

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getIncomeDD() {
        return "ImportEmployeeSalaryUpgrade_income";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employee">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String employee;

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "ImportImportEmployeeSalaryUpgrade_employee";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="valueAfter">
    private String valueAfter;
    public String getValueAfter() {
        return valueAfter;
    }
    public void setValueAfter(String valueAfter) {
        this.valueAfter = valueAfter;
    }
    public String getValueAfterDD() {
        return "ImportEmployeeSalaryUpgrade_valueAfter";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="difference">
    private String difference;

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getDifferenceDD() {
        return "ImportEmployeeSalaryUpgrade_difference";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="valueBefore">
    private String valueBefore;
    public String getValueBefore() {
        return valueBefore;
    }
    public void setValueBefore(String valueBefore) {
        this.valueBefore = valueBefore;
    }
    public String getValueBeforeDD() {
        return "ImportEmployeeSalaryUpgrade_valueBefore";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="upgradeFactor">
    private String upgradeFactor;
    public String getUpgradeFactor() {
        return upgradeFactor;
    }
    public void setUpgradeFactor(String upgradeFactor) {
        this.upgradeFactor = upgradeFactor;
    }
    public String getUpgradeFactorDD() {
        return "ImportImportEmployeeSalaryUpgrade_upgradeFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="upgradeReason">
    private String upgradeReason;
    public String getUpgradeReason() {
        return upgradeReason;
    }
    public void setUpgradeReason(String upgradeReason) {
        this.upgradeReason = upgradeReason;
    }
    public String getUpgradeReasonDD() {
        return "EmployeeSalaryUpgrade_upgradeReason";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="givenBy">
    private String givenBy;
    public String getGivenBy() {
        return givenBy;
    }
    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }
    public String getGivenByDD() {
        return "EmployeeSalaryUpgrade_givenBy";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fromCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod fromCalculatedPeriod;
    public CalculatedPeriod getFromCalculatedPeriod() {
        return fromCalculatedPeriod;
    }
    public void setFromCalculatedPeriod(CalculatedPeriod fromCalculatedPeriod) {
        this.fromCalculatedPeriod = fromCalculatedPeriod;
    }
    public String getFromCalculatedPeriodDD() {
        return "ImportEmployeeSalaryUpgrade_fromCalculatedPeriod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="toCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod toCalculatedPeriod;
    public CalculatedPeriod getToCalculatedPeriod() {
        return toCalculatedPeriod;
    }
    public void setToCalculatedPeriod(CalculatedPeriod toCalculatedPeriod) {
        this.toCalculatedPeriod = toCalculatedPeriod;
    }
    public String getToCalculatedPeriodDD() {
        return "ImportEmployeeSalaryUpgrade_toCalculatedPeriod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="applyDate">
    @Column(nullable=false)
    private String applyDate;
    public String getApplyDate() {
        return applyDate;
    }
    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }
    public String getApplyDateDD() {
        return "ImportEmployeeSalaryUpgrade_applyDate";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="done">
    boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "ImportEmployeeSalaryUpgrade_done";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="valueBeforeEnc">
    private String valueBeforeEnc;

    public String getValueBeforeEnc() {
        return valueBeforeEnc;
    }

    public void setValueBeforeEnc(String valueBeforeEnc) {
        this.valueBeforeEnc = valueBeforeEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="valueAfterEnc">
    private String valueAfterEnc;

    public String getValueAfterEnc() {
        return valueAfterEnc;
    }

    public void setValueAfterEnc(String valueAfterEnc) {
        this.valueAfterEnc = valueAfterEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="differenceEnc">
    private String differenceEnc;

    public String getDifferenceEnc() {
        return differenceEnc;
    }

    public void setDifferenceEnc(String differenceEnc) {
        this.differenceEnc = differenceEnc;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="upgradeFactorEnc">
    private String upgradeFactorEnc;

    public String getUpgradeFactorEnc() {
        return upgradeFactorEnc;
    }

    public void setUpgradeFactorEnc(String upgradeFactorEnc) {
        this.upgradeFactorEnc = upgradeFactorEnc;
    }
    // </editor-fold>
}