/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.requisition;
import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"requisition"})
public class RequisitionPreferred extends BusinessObjectBaseEntity {
      // <editor-fold defaultstate="collapsed" desc="preferred">
    @Column
    private String preferred;
    public String getPreferred() {
        return preferred;
    }
    public void setPreferred(String preferred) {
        this.preferred = preferred;
    }
        public String getPreferredDD() {
        return "RequisitionPreferred_preferred";
    }
    // </editor-fold>
      // <editor-fold defaultstate="collapsed" desc="requisition">
    @JoinColumn(nullable=false)
     @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Requisition requisition;
    public Requisition getRequisition() {
        return requisition;
    }
    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }
      public String getRequisitionDD() {
        return "RequisitionPreferred_requisition";
    }
         // </editor-fold>
}
