/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
//@MappedSuperclass
@Entity
@ParentEntity(fields={"penalty"})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "STYPE")
//@Table(name = "penalty_affecting_deduction")
public class PenaltyAffectingSalaryElement extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="penalty">
    @JoinColumn (nullable= false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
  
    private Penalty penalty;

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }

    public String getPenaltyDD() {
        return "PenaltyAffectingSalaryElement_penalty";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @Transient
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
  public String getSalaryElementDD() {
        return "PenaltyAffectingSalaryElement_salaryElement";
    }
  // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factor">
    @Column(precision=18,scale=3)
    private BigDecimal factor;//(name = "factor")

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public String getFactorDD() {
        return "PenaltyAffectingSalaryElement_factor";
    }
    @Transient
    private BigDecimal factorMask;

    public BigDecimal getFactorMask() {
        factorMask = factor;
        return factorMask;
    }

    public void setFactorMask(BigDecimal factorMask) {
        updateDecimalValue("factor",factorMask);
    }

    public String getFactorMaskDD() {
        return "PenaltyAffectingSalaryElement_factorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximum">
    @Column(precision=18,scale=3)
    private BigDecimal maximum;//(name = "maximum")

    public BigDecimal getMaximum() {
        return maximum;
    }

    public void setMaximum(BigDecimal maximum) {
        this.maximum = maximum;
    }

    public String getMaximumDD() {
        return "PenaltyAffectingSalaryElement_maximum";
    }
    @Transient
    private BigDecimal maximumMask;

    public BigDecimal getMaximumMask() {
        maximumMask = maximum ;
        return maximumMask;
    }

    public void setMaximumMask(BigDecimal maximumMask) {
        updateDecimalValue("maximum",maximumMask);
    }

    public String getMaximumMaskDD() {
        return "PenaltyAffectingSalaryElement_maximumMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimum">
    @Column(precision=18,scale=3)
    private BigDecimal minimum;//(name = "minimum")

    public BigDecimal getMinimum() {
        return minimum;
    }

    public void setMinimum(BigDecimal minimum) {
        this.minimum = minimum;
    }
    public String getMinimumDD() {
        return "PenaltyAffectingSalaryElement_minimum";
    }
    @Transient
    private BigDecimal minimumMask;

    public BigDecimal getMinimumMask() {
        minimumMask = minimum;
        return minimumMask;
    }

    public void setMinimumMask(BigDecimal minimumMask) {
        updateDecimalValue("minimum",minimumMask);
    }
    public String getMinimumMaskDD() {
        return "PenaltyAffectingSalaryElement_minimumMask";
    }
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="dype">
    @Transient
    private String dtype;

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    
    public String getDtypeDD() {
        return "PenaltyAffectingSalaryElement_dtype";
    }
 // </editor-fold>

}
