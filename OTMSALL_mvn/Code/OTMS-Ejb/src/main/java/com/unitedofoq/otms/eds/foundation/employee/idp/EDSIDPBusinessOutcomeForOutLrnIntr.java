/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.training.EDSChannel;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields="businessOutcome")
@ChildEntity(fields={"businessOutcomes"})
@Table(name= "edsidpbusoutforoutlrnint")
public class EDSIDPBusinessOutcomeForOutLrnIntr extends BaseEntity {
    @OneToMany(mappedBy="channelLrnIntr")
    private List<EDSIDPActionForBusinessOutcome> businessOutcomes;
    public String getBusinessOutcomesDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_businessOutcomes";
    }
    public List<EDSIDPActionForBusinessOutcome> getBusinessOutcomes() {
        return businessOutcomes;
    }

    public void setBusinessOutcomes(List<EDSIDPActionForBusinessOutcome> businessOutcomes) {
        this.businessOutcomes = businessOutcomes;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private EDSIDPBusinessOutcome businessOutcome;

    public String getBusinessOutcomeDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_businessOutcome";
    }

    public EDSIDPBusinessOutcome getBusinessOutcome() {
        return businessOutcome;
    }

    public void setBusinessOutcome(EDSIDPBusinessOutcome businessOutcome) {
        this.businessOutcome = businessOutcome;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSIDPBusinessOutcomeAcc outcomeAcc;

    public String getOutcomeAccDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_outcomeAcc";
    }

    public EDSIDPBusinessOutcomeAcc getOutcomeAcc() {
        return outcomeAcc;
    }

    public void setOutcomeAcc(EDSIDPBusinessOutcomeAcc outcomeAcc) {
        this.outcomeAcc = outcomeAcc;
    }


    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private EDSChannel channel;

    public String getChannelDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_channel";
    }

    public EDSChannel getChannel() {
        return channel;
    }

    public void setChannel(EDSChannel channel) {
        this.channel = channel;
    }

    private String description;
    public String getDescriptionDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_description";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date whenIntr;

    public String getWhenIntrDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_whenIntr";
    }

    public Date getWhenIntr() {
        return whenIntr;
    }

    public void setWhenIntr(Date whenIntr) {
        this.whenIntr = whenIntr;
    }
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date completionDate;

    public String getCompletionDateDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_completionDate";
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    private double completionProgress;
    public String getCompletionProgressDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_completionProgress";
    }

    public double getCompletionProgress() {
        return completionProgress;
    }

    public void setCompletionProgress(double completionProgress) {
        this.completionProgress = completionProgress;
    }

    private String acheivmentCommit;

    public String getAcheivmentCommitDD() {
        return "EDSIDPBusinessOutcomeForOutLrnIntr_acheivmentCommit";
    }

    public String getAcheivmentCommit() {
        return acheivmentCommit;
    }

    public void setAcheivmentCommit(String acheivmentCommit) {
        this.acheivmentCommit = acheivmentCommit;
    }


}