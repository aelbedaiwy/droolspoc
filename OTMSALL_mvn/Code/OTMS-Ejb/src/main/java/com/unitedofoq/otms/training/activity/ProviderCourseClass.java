
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.training.company.CompanyCourseClass;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"provider"})
public class ProviderCourseClass extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Provider getProvider() {
        return provider;
    }

    public String getProviderDD() {
        return "ProviderCourseClass_provider";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "ProviderCourseClass_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "ProviderCourseClass_description";
    }
    
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "ProviderCourseClass_description";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="companyCourseClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY) 
    private CompanyCourseClass companyCourseClass;

    public CompanyCourseClass getCompanyCourseClass() {
        return companyCourseClass;
    }

    public void setCompanyCourseClass(CompanyCourseClass companyCourseClass) {
        this.companyCourseClass = companyCourseClass;
    }
    
    public String getCompanyCourseClassDD() {
        return "ProviderCourseClass_companyCourseClass";
    }

    //</editor-fold>
}