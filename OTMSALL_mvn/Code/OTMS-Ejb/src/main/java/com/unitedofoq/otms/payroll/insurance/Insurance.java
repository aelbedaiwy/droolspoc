package com.unitedofoq.otms.payroll.insurance;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the insurance database table.
 *
 */
@Entity
@Table(name = "OInsurance")
@ParentEntity(fields = "company")
@ChildEntity(fields = {"categories", "incomes", "insuranceGroupMembers"})
public class Insurance extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Insurance_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="addExceedBasicToVar">
    @Column(length = 1)
    private String addExceedBasicToVar;

    public String getAddExceedBasicToVar() {
        return addExceedBasicToVar;
    }

    public void setAddExceedBasicToVar(String addExceedBasicToVar) {
        this.addExceedBasicToVar = addExceedBasicToVar;
    }

    public String getAddExceedBasicToVarDD() {
        return "Insurance_addExceedBasicToVar";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "Insurance_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hireDay">
    @Column
    private Integer hireDay;

    public Integer getHireDay() {
        return hireDay;
    }

    public void setHireDay(Integer hireDay) {
        this.hireDay = hireDay;
    }

    public String getHireDayDD() {
        return "Insurance_hireDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hirePercentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal hirePercentage;

    public BigDecimal getHirePercentage() {
        return hirePercentage;
    }

    public void setHirePercentage(BigDecimal hirePercentage) {
        this.hirePercentage = hirePercentage;
    }

    public String getHirePercentageDD() {
        return "Insurance_hirePercentage";
    }
    @Transient
    private BigDecimal hirePercentageMask;

    public BigDecimal getHirePercentageMask() {
        hirePercentageMask = hirePercentage;
        return hirePercentageMask;
    }

    public void setHirePercentageMask(BigDecimal hirePercentageMask) {
        updateDecimalValue("hirePercentage", hirePercentageMask);
    }

    public String getHirePercentageMaskDD() {
        return "Insurance_hirePercentageMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hireWorkingdays">
    @Column(length = 1)
    private String hireWorkingdays;

    public String getHireWorkingdays() {
        return hireWorkingdays;
    }

    public void setHireWorkingdays(String hireWorkingdays) {
        this.hireWorkingdays = hireWorkingdays;
    }

    public String getHireWorkingdaysDD() {
        return "Insurance_hireWorkingdays";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Insurance_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="leaveDay">
    @Column
    //@FABSDataValidation(validationType=FABSDataValidation.FABSDataValidationType.DaysPerMonth)
    private int leaveDay;

    public int getLeaveDay() {
        return leaveDay;
    }

    public void setLeaveDay(int leaveDay) {
        this.leaveDay = leaveDay;
    }

    public String getLeaveDayDD() {
        return "Insurance_leaveDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="leavePercentage">
    @Column(precision = 25, scale = 13)
    private BigDecimal leavePercentage;

    public BigDecimal getLeavePercentage() {
        return leavePercentage;
    }

    public void setLeavePercentage(BigDecimal leavePercentage) {
        this.leavePercentage = leavePercentage;
    }

    public String getLeavePercentageDD() {
        return "Insurance_leavePercentage";
    }
    @Transient
    private BigDecimal leavePercentageMask;

    public BigDecimal getLeavePercentageMask() {
        leavePercentageMask = leavePercentage;
        return leavePercentageMask;
    }

    public void setLeavePercentageMask(BigDecimal leavePercentageMask) {
        updateDecimalValue("leavePercentage", leavePercentageMask);
    }

    public String getLeavePercentageMaskDD() {
        return "Insurance_leavePercentageMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="leaveWorkingdays">
    @Column(length = 1)
    private String leaveWorkingdays;

    public String getLeaveWorkingdays() {
        return leaveWorkingdays;
    }

    public void setLeaveWorkingdays(String leaveWorkingdays) {
        this.leaveWorkingdays = leaveWorkingdays;
    }

    public String getLeaveWorkingdaysDD() {
        return "Insurance_leaveWorkingdays";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxAge">
    @Column(precision = 25, scale = 13)
    private BigDecimal maxAge;

    public BigDecimal getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(BigDecimal maxAge) {
        this.maxAge = maxAge;
    }

    public String getMaxAgeDD() {
        return "Insurance_maxAge";
    }
    @Transient
    private BigDecimal maxAgeMask;

    public BigDecimal getMaxAgeMask() {
        maxAgeMask = maxAge;
        return maxAgeMask;
    }

    public void setMaxAgeMask(BigDecimal maxAgeMask) {
        updateDecimalValue("maxAge", maxAgeMask);
    }

    public String getMaxAgeMaskDD() {
        return "Insurance_maxAgeMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minAge">
    @Column(precision = 25, scale = 13)
    private BigDecimal minAge;

    public BigDecimal getMinAge() {
        return minAge;
    }

    public void setMinAge(BigDecimal minAge) {
        this.minAge = minAge;
    }

    public String getMinAgeDD() {
        return "Insurance_minAge";
    }
    @Transient
    private BigDecimal minAgeMask;

    public BigDecimal getMinAgeMask() {
        minAgeMask = minAge;
        return minAgeMask;
    }

    public void setMinAgeMask(BigDecimal minAgeMask) {
        updateDecimalValue("minAge", minAgeMask);
    }

    public String getMinAgeMaskDD() {
        return "Insurance_minAgeMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxBasicBasis">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal maxBasicBasis;

    public BigDecimal getMaxBasicBasis() {
        return maxBasicBasis;
    }

    public void setMaxBasicBasis(BigDecimal maxBasicBasis) {
        this.maxBasicBasis = maxBasicBasis;
    }

    public String getMaxBasicBasisDD() {
        return "Insurance_maxBasicBasis";
    }
    @Transient
    private BigDecimal maxBasicBasisMask;

    public BigDecimal getMaxBasicBasisMask() {
        maxBasicBasisMask = maxBasicBasis;
        return maxBasicBasisMask;
    }

    public void setMaxBasicBasisMask(BigDecimal maxBasicBasisMask) {
        updateDecimalValue("maxBasicBasis", maxBasicBasisMask);
    }

    public String getMaxBasicBasisMaskDD() {
        return "Insurance_maxBasicBasisMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxVariableBasis">
    @Column(precision = 25, scale = 13, nullable = false)
    private BigDecimal maxVariableBasis;

    public BigDecimal getMaxVariableBasis() {
        return maxVariableBasis;
    }

    public void setMaxVariableBasis(BigDecimal maxVariableBasis) {
        this.maxVariableBasis = maxVariableBasis;
    }

    public String getMaxVariableBasisDD() {
        return "Insurance_maxVariableBasis";
    }
    @Transient
    private BigDecimal maxVariableBasisMask;

    public BigDecimal getMaxVariableBasisMask() {
        maxVariableBasisMask = maxVariableBasis;
        return maxVariableBasisMask;
    }

    public void setMaxVariableBasisMask(BigDecimal maxVariableBasisMask) {
        updateDecimalValue("maxVariableBasis", maxVariableBasisMask);
    }

    public String getMaxVariableBasisMaskDD() {
        return "Insurance_maxVariableBasisMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minBasicBasis">
    @Column(precision = 25, scale = 13)
    private BigDecimal minBasicBasis;

    public BigDecimal getMinBasicBasis() {
        return minBasicBasis;
    }

    public void setMinBasicBasis(BigDecimal minBasicBasis) {
        this.minBasicBasis = minBasicBasis;
    }

    public String getMinBasicBasisDD() {
        return "Insurance_minBasicBasis";
    }
    @Transient
    private BigDecimal minBasicBasisMask;

    public BigDecimal getMinBasicBasisMask() {
        minBasicBasisMask = minBasicBasis;
        return minBasicBasisMask;
    }

    public void setMinBasicBasisMask(BigDecimal minBasicBasisMask) {
        updateDecimalValue("minBasicBasis", minBasicBasisMask);
    }

    public String getMinBasicBasisMaskDD() {
        return "Insurance_minBasicBasisMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minVariableBasis">
    @Column(precision = 25, scale = 13)
    private BigDecimal minVariableBasis;

    public BigDecimal getMinVariableBasis() {
        return minVariableBasis;
    }

    public void setMinVariableBasis(BigDecimal minVariableBasis) {
        this.minVariableBasis = minVariableBasis;
    }

    public String getMinVariableBasisDD() {
        return "Insurance_minVariableBasis";
    }
    @Transient
    private BigDecimal minVariableBasisMask;

    public BigDecimal getMinVariableBasisMask() {
        minVariableBasisMask = minVariableBasis;
        return minVariableBasisMask;
    }

    public void setMinVariableBasisMask(BigDecimal minVariableBasisMask) {
        updateDecimalValue("minVariableBasis", minVariableBasisMask);
    }

    public String getMinVariableBasisMaskDD() {
        return "Insurance_minVariableBasisMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="recalcMonth">
    @Column(nullable = false)
    private int recalcMonth;

    public int getRecalcMonth() {
        return recalcMonth;
    }

    public void setRecalcMonth(int recalcMonth) {
        this.recalcMonth = recalcMonth;
    }

    public String getRecalcMonthDD() {
        return "Insurance_recalcMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="recalcBasis">
    @Column(length = 1)
    String recalcBasis;

    public String getRecalcBasis() {
        return recalcBasis;
    }

    public void setRecalcBasis(String recalcBasis) {
        this.recalcBasis = recalcBasis;
    }

    public String getRecalcBasisDD() {
        return "Insurance_recalcBasis";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceGroupMembers">
    @OneToMany(mappedBy = "insurance")
    private List<InsuranceGroupMember> insuranceGroupMembers;

    public List<InsuranceGroupMember> getInsuranceGroupMembers() {
        return insuranceGroupMembers;
    }

    public void setInsuranceGroupMembers(List<InsuranceGroupMember> insuranceGroupMembers) {
        this.insuranceGroupMembers = insuranceGroupMembers;
    }

    public String getInsuranceGroupMembersDD() {
        return "Insurance_insuranceGroupMembers";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="categories">
    @OneToMany(mappedBy = "insurance")
    private List<InsuranceCategory> categories;

    public List<InsuranceCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<InsuranceCategory> categories) {
        this.categories = categories;
    }

    public String getCategoriesDD() {
        return "Insurance_categories";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="incomes">
    @OneToMany(mappedBy = "insurance")
    private List<InsuranceIncome> incomes;

    public List<InsuranceIncome> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<InsuranceIncome> incomes) {
        this.incomes = incomes;
    }

    public String getIncomesDD() {
        return "Insurance_incomes";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "Insurance_legalEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "Insurance_legalEntityGroup";
    }
    //</editor-fold>
}
