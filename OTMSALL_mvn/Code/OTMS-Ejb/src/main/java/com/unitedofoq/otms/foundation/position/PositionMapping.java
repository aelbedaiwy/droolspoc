/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author lahmed
 */
@Entity
public class PositionMapping extends BaseEntity  {
    //<editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionDD() {
        return "PositionMapping_description";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    
    public String getPositionDD() {
        return "PositionMapping_position";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD() {
        return "PositionMapping_code";
    }
    //</editor-fold>
}
