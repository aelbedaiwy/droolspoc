/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="repemployeetraining")
public class RepEmployeeTraining extends RepEmployeeBase{
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeTraining_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeTraining_genderDescription";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }
    
    public String isEmployeeActiveDD() {
        return "RepEmployeeTraining_employeeActive";
    }
    
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeeTraining_employeeActive";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "RepEmployeeTraining_startDate";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "RepEmployeeTraining_endDate";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="finishDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date finishDate;

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getFinishDateDD() {
        return "RepEmployeeTraining_finishDate";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="evaluationDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date evaluationDate;

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluationDateDD() {
        return "RepEmployeeTraining_evaluationDate";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="providerDescription">
    @Column
    private String providerDescription;

    public String getProviderDescription() {
        return providerDescription;
    }

    public void setProviderDescription(String providerDescription) {
        this.providerDescription = providerDescription;
    }

    public String getProviderDescriptionDD() {
        return "RepEmployeeTraining_providerDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="providerActivity">
    @Column
    private String providerActivity;

    public String getProviderActivity() {
        return providerActivity;
    }

    public void setProviderActivity(String providerActivity) {
        this.providerActivity = providerActivity;
    }

    public String getProviderActivityDD() {
        return "RepEmployeeTraining_providerActivity";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="trainingLocation">
    @Column
    private String trainingLocation;

    public String getTrainingLocation() {
        return trainingLocation;
    }

    public void setTrainingLocation(String trainingLocation) {
        this.trainingLocation = trainingLocation;
    }

    public String getTrainingLocationDD() {
        return "RepEmployeeTraining_trainingLocation";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="attendancePercent">
    @Column
    private BigDecimal attendancePercent;

    public BigDecimal getAttendancePercent() {
        return attendancePercent;
    }

    public void setAttendancePercent(BigDecimal attendancePercent) {
        this.attendancePercent = attendancePercent;
    }

    public String getAttendancePercentDD() {
        return "RepEmployeeTraining_attendancePercent";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="trainingLocationDescription">
    @Column
    private String trainingLocationDescription;

    public String getTrainingLocationDescription() {
        return trainingLocationDescription;
    }

    public void setTrainingLocationDescription(String trainingLocationDescription) {
        this.trainingLocationDescription = trainingLocationDescription;
    }

    public String getTrainingLocationDescriptionDD() {
        return "RepEmployeeTraining_trainingLocationDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="evaluationScore">
    @Column
    private BigDecimal evaluationScore;

    public BigDecimal getEvaluationScore() {
        return evaluationScore;
    }

    public void setEvaluationScore(BigDecimal evaluationScore) {
        this.evaluationScore = evaluationScore;
    }

    public String getEvaluationScoreDD() {
        return "RepEmployeeTraining_evaluationScore";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "RepEmployeeTraining_notes";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="cost">
    @Column(precision=25, scale=13)
    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCostDD() {
        return "RepEmployeeTraining_cost";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="currency">
    @Column
    private String currency;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "RepEmployeeTraining_currency";
    }

    //</editor-fold>
}
