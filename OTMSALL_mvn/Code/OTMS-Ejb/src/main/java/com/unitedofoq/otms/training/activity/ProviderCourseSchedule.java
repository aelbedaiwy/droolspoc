
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Entity
public class ProviderCourseSchedule extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="date">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getDateDD() {
        return "ProviderCourseSchedule_date";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeFrom">
    @Column
    private String timeFrom;

    public void setTimeFrom(String from) {
        this.timeFrom = from;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public String getTimeFromDD() {
        return "ProviderCourseSchedule_timeFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="timeTo">
    @Column
    private String timeTo;

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public String getTimeToDD() {
        return "ProviderCourseSchedule_timeTo";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="breakTo">
    @Column
    private String breakTo;

    public void setBreakTo(String breakTo) {
        this.breakTo = breakTo;
    }

    public String getBreakTo() {
        return breakTo;
    }

    public String getBreakToDD() {
        return "ProviderCourseSchedule_breakTo";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="breakFrom">
    @Column
    private String breakFrom;

    public void setBreakFrom(String breakFrom) {
        this.breakFrom = breakFrom;
    }

    public String getBreakFrom() {
        return breakFrom;
    }

    public String getBreakFromDD() {
        return "ProviderCourseSchedule_breakFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public String getNotesDD() {
        return "ProviderCourseSchedule_notes";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseInformation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation courseInformation;

    public void setCourseInformation(ProviderCourseInformation courseInformation) {
        this.courseInformation = courseInformation;
    }

    public ProviderCourseInformation getCourseInformation() {
        return courseInformation;
    }

    public String getCourseInformationDD() {
        return "ProviderCourseSchedule_courseInformation";
    }
    // </editor-fold>
}