/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name = "reppersonneleffectonpayroll")
public class RepPersonnelEffectOnPayroll extends RepEmployeeBase {

    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepPersonnelEffectOnPayroll_employeeActive";
    }

    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }

    public String getEmployeeActiveTranslatedDD() {
        return "RepPersonnelEffectOnPayroll_employeeActive";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepPersonnelEffectOnPayroll_genderDescription";
    }

    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }

    public String getGenderDescriptionTranslatedDD() {
        return "RepPersonnelEffectOnPayroll_genderDescription";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="salaryElementName">
    @Column
    @Translatable(translationField = "salaryElementNameTranslated")
    private String salaryElementName;

    public String getSalaryElementName() {
        return salaryElementName;
    }

    public void setSalaryElementName(String salaryElementName) {
        this.salaryElementName = salaryElementName;
    }

    public String getSalaryElementNameDD() {
        return "RepPersonnelEffectOnPayroll_salaryElementName";
    }

    @Transient
    @Translation(originalField = "salaryElementName")
    private String salaryElementNameTranslated;

    public String getSalaryElementNameTranslated() {
        return salaryElementNameTranslated;
    }

    public void setSalaryElementNameTranslated(String salaryElementNameTranslated) {
        this.salaryElementNameTranslated = salaryElementNameTranslated;
    }

    public String getSalaryElementNameTranslatedDD() {
        return "RepPersonnelEffectOnPayroll_salaryElementName";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="value">
    @Column(precision = 25, scale = 13)
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getValueDD() {
        return "RepPersonnelEffectOnPayroll_value";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="valueEnc">
    @Column
    private String valueEnc;

    public String getValueEnc() {
        return valueEnc;
    }

    public void setValueEnc(String valueEnc) {
        this.valueEnc = valueEnc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="monthlyvalue">
    @Column(precision = 25, scale = 13)
    private BigDecimal monthlyvalue;

    public BigDecimal getMonthlyvalue() {
        return monthlyvalue;
    }

    public void setMonthlyvalue(BigDecimal monthlyvalue) {
        this.monthlyvalue = monthlyvalue;
    }

    public String getMonthlyvalueDD() {
        return "RepPersonnelEffectOnPayroll_monthlyvalue";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="monthlyvalueEnc">
    @Column
    private String monthlyvalueEnc;

    public String getMonthlyvalueEnc() {
        return monthlyvalueEnc;
    }

    public void setMonthlyvalueEnc(String monthlyvalueEnc) {
        this.monthlyvalueEnc = monthlyvalueEnc;
        //</editor-fold>
    }
}
