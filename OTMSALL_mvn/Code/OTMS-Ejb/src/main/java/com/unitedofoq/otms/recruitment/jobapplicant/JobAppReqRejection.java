package com.unitedofoq.otms.recruitment.jobapplicant;

import com.unitedofoq.otms.recruitment.jobrequisition.JobRequisition;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class JobAppReqRejection extends JobAppRejection {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private JobRequisition jobRequisition;

	public JobRequisition getJobRequisition() {
		return jobRequisition;
	}

	public void setJobRequisition(JobRequisition theJobRequisition) {
		jobRequisition = theJobRequisition;
	}
}
