package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class ApplicantAnswer extends BaseEntity {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppliedApplicant applicant;

    public AppliedApplicant getApplicant() {
        return applicant;
    }

    public void setApplicant(AppliedApplicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantAnswer_applicant";
    }

    // <editor-fold defaultstate="collapsed" desc="result">
    private float resultScore;

    public float getResultScore() {
        return resultScore;
    }

    public void setResultScore(float resultScore) {
        this.resultScore = resultScore;
    }

    public String getResultScoreDD() {
        return "ApplicantAnswer_resultScore";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="correctAnswer">
    private boolean correctAnswer;

    public boolean isCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(boolean correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getCorrectAnswerDD() {
        return "ApplicantAnswer_correctAnswer";
    }
    // </editor-fold>

    @Transient
    Integer rankGap;

    public Integer getRankGap() {
        return null;
    }

    public String getRankGapDD() {
        return "ApplicantAnswer_rankGap";
    }

    public void setRankGap(Integer rankGap) {
        this.rankGap = rankGap;
    }

}
