/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.jobGroup;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;


import com.unitedofoq.otms.foundation.training.TrainingBase;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"jobGroup"})
public class JobGroupTraining extends TrainingBase {
  // <editor-fold defaultstate="collapsed" desc="jobGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private JobGroup jobGroup;

    public JobGroup getJobGroup() {
        return jobGroup;
    }
    public String getJobGroupDD() {
        return "JobGroupTraining_jobGroup";
    }
    public void setJobGroup(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }
    // </editor-fold>
}
