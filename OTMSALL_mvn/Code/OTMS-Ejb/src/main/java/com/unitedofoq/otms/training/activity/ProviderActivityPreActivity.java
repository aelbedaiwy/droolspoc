/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"providerActivity"})
public class ProviderActivityPreActivity extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="providerActivity">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private ProviderActivity providerActivity;

    public ProviderActivity getProviderActivity() {
        return providerActivity;
    }

    public void setProviderActivity(ProviderActivity providerActivity) {
        this.providerActivity = providerActivity;
    }

    public String getProviderActivityDD() {
        return "ProviderActivityPreActivity_providerActivity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="preActivity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Activity preActivity;

    public Activity getPreActivity() {
        return preActivity;
    }

    public void setPreActivity(Activity preActivity) {
        this.preActivity = preActivity;
    }

    public String getPreActivityDD() {
        return "ProviderActivityPreActivity_preActivity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="operator">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC operator;

    public UDC getOperator() {
        return operator;
    }

    public void setOperator(UDC operator) {
        this.operator = operator;
    }

    public String getOperatorDD() {
        return "ProviderActivityPreActivity_operator";
    }
    // </editor-fold>
}