/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author lap
 */
@Entity
public class GoalTranslation  extends BaseEntityTranslation {
    
       // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>
    
}
