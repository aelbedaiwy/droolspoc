/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.otms.reports.views.RepEmployeeBaseTranslation;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class RepAppraisalSheetTranslationBase extends RepEmployeeBaseTranslation {
    // <editor-fold defaultstate="collapsed" desc="template">

    @Column
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }
    // </editor-fold>
}
