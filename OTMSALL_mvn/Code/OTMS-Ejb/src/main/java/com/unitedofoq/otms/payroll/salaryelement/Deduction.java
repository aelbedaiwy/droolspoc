package com.unitedofoq.otms.payroll.salaryelement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("DEDUCTION")
public class Deduction extends SalaryElement {
}
