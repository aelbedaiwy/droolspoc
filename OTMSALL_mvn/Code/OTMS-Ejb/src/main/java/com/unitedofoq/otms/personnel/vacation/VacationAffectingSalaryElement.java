/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.personnel.dayoff.DayOffAffectingSalaryElement;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
public class VacationAffectingSalaryElement extends DayOffAffectingSalaryElement {
    // <editor-fold defaultstate="collapsed" desc="vacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn (nullable= false)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }
    public String getVacationDD() {
        return "VacationAffectingSalaryElement_vacation";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dtype">
   /* @Transient
    
    private String dtype;

    public String getDType() {
        return this.getEntityDiscrminatorValue();
    }

    public void setDType(String dType) {
        this.dtype = dType;
    }
    public String getDTypeDD() {
        return "VacationAffectingSalaryElement_dtype";
    }*/
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @Transient
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
     public String getSalaryElementDD() {
        return "VacationAffectingSalaryElement_salaryElement";
    }
 // </editor-fold>
}
