/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lahmed
 */
@Entity
@Table(name="OTSegment")
@ParentEntity(fields={"calendar"})
public class OTSegment extends OTSegmentBase {
    
    //<editor-fold defaultstate="collapsed" desc="calendar">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private NormalWorkingCalendar calendar;

    public NormalWorkingCalendar getCalendar() {
        return calendar;
    }

    public void setCalendar(NormalWorkingCalendar calendar) {
        this.calendar = calendar;
    }
    
    public String getCalendarDD() {
        return "OTSegment_calendar";
    }
    //</editor-fold>
}