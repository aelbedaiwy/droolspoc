package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class AdditionalInfo extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="attrStr1">

    private String attrStr1;

    public String getAttrStr1() {
        return attrStr1;
    }

    public String getAttrStr1DD() {
        return "AdditionalInfo_attrStr1";
    }

    public void setAttrStr1(String attrStr1) {
        this.attrStr1 = attrStr1;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrStr2">
    private String attrStr2;

    public String getAttrStr2() {
        return attrStr2;
    }

    public String getAttrStr2DD() {
        return "AdditionalInfo_attrStr2";
    }

    public void setAttrStr2(String attrStr2) {
        this.attrStr2 = attrStr2;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrStr3">
    private String attrStr3;

    public String getAttrStr3() {
        return attrStr3;
    }

    public String getAttrStr3DD() {
        return "AdditionalInfo_attrStr3";
    }

    public void setAttrStr3(String attrStr3) {
        this.attrStr3 = attrStr3;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrStr4">
    private String attrStr4;

    public String getAttrStr4() {
        return attrStr4;
    }

    public String getAttrStr4DD() {
        return "AdditionalInfo_attrStr4";
    }

    public void setAttrStr4(String attrStr4) {
        this.attrStr4 = attrStr4;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrStr5">
    private String attrStr5;

    public String getAttrStr5() {
        return attrStr5;
    }

    public String getAttrStr5DD() {
        return "AdditionalInfo_attrStr5";
    }

    public void setAttrStr5(String attrStr5) {
        this.attrStr5 = attrStr5;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDate1">
    @Temporal(TemporalType.TIMESTAMP)
    private Date attrDate1;

    public Date getAttrDate1() {
        return attrDate1;
    }

    public String getAttrDate1DD() {
        return "AdditionalInfo_attrDate1";
    }

    public void setAttrDate1(Date attrDate1) {
        this.attrDate1 = attrDate1;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDate2">
    @Temporal(TemporalType.TIMESTAMP)
    private Date attrDate2;

    public Date getAttrDate2() {
        return attrDate2;
    }

    public String getAttrDate2DD() {
        return "AdditionalInfo_attrDate2";
    }

    public void setAttrDate2(Date attrDate2) {
        this.attrDate2 = attrDate2;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDate3">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date attrDate3;

    public Date getAttrDate3() {
        return attrDate3;
    }

    public String getAttrDate3DD() {
        return "AdditionalInfo_attrDate3";
    }

    public void setAttrDate3(Date attrDate3) {
        this.attrDate3 = attrDate3;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDate4">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date attrDate4;

    public Date getAttrDate4() {
        return attrDate4;
    }

    public String getAttrDate4DD() {
        return "AdditionalInfo_attrDate4";
    }

    public void setAttrDate4(Date attrDate4) {
        this.attrDate4 = attrDate4;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDate5">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date attrDate5;

    public Date getAttrDate5() {
        return attrDate5;
    }

    public String getAttrDate5DD() {
        return "AdditionalInfo_attrDate5";
    }

    public void setAttrDate5(Date attrDate5) {
        this.attrDate5 = attrDate5;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrUdc1">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC attrUdc1;

    public UDC getAttrUdc1() {
        return attrUdc1;
    }

    public String getAttrUdc1DD() {
        return "AdditionalInfo_attrUdc1";
    }

    public void setAttrUdc1(UDC attrUdc1) {
        this.attrUdc1 = attrUdc1;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrUdc2">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC attrUdc2;

    public UDC getAttrUdc2() {
        return attrUdc2;
    }

    public String getAttrUdc2DD() {
        return "AdditionalInfo_attrUdc2";
    }

    public void setAttrUdc2(UDC attrUdc2) {
        this.attrUdc2 = attrUdc2;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrUdc3">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC attrUdc3;

    public UDC getAttrUdc3() {
        return attrUdc3;
    }

    public String getAttrUdc3DD() {
        return "AdditionalInfo_attrUdc3";
    }

    public void setAttrUdc3(UDC attrUdc3) {
        this.attrUdc3 = attrUdc3;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrUdc4">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC attrUdc4;

    public UDC getAttrUdc4() {
        return attrUdc4;
    }

    public String getAttrUdc4DD() {
        return "AdditionalInfo_attrUdc4";
    }

    public void setAttrUdc4(UDC attrUdc4) {
        this.attrUdc4 = attrUdc4;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrUdc5">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC attrUdc5;

    public UDC getAttrUdc5() {
        return attrUdc5;
    }

    public String getAttrUdc5DD() {
        return "AdditionalInfo_attrUdc5";
    }

    public void setAttrUdc5(UDC attrUdc5) {
        this.attrUdc5 = attrUdc5;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDec1">
    private BigDecimal attrDec1;

    public BigDecimal getAttrDec1() {
        return attrDec1;
    }

    public String getAttrDec1DD() {
        return "AdditionalInfo_attrDec1";
    }

    public void setAttrDec1(BigDecimal attrDec1) {
        this.attrDec1 = attrDec1;
    }
    @Transient
    private BigDecimal attrDec1Mask;

    public BigDecimal getAttrDec1Mask() {
        attrDec1Mask = attrDec1;
        return attrDec1Mask;
    }

    public String getAttrDec1MaskDD() {
        return "AdditionalInfo_attrDec1Mask";
    }

    public void setAttrDec1Mask(BigDecimal attrDec1Mask) {
        updateDecimalValue("attrDec1", attrDec1Mask);
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDec2">
    private BigDecimal attrDec2;

    public BigDecimal getAttrDec2() {
        return attrDec2;
    }

    public String getAttrDec2DD() {
        return "AdditionalInfo_attrDec2";
    }

    public void setAttrDec2(BigDecimal attrDec2) {
        this.attrDec2 = attrDec2;
    }
    @Transient
    private BigDecimal attrDec2Mask;

    public BigDecimal getAttrDec2Mask() {
        attrDec2Mask = attrDec2;
        return attrDec2Mask;
    }

    public String getAttrDec2MaskDD() {
        return "AdditionalInfo_attrDec2Mask";
    }

    public void setAttrDec2Mask(BigDecimal attrDec2Mask) {
        updateDecimalValue("attrDec2", attrDec2Mask);
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDec3">
    private BigDecimal attrDec3;

    public BigDecimal getAttrDec3() {
        return attrDec3;
    }

    public String getAttrDec3DD() {
        return "AdditionalInfo_attrDec3";
    }

    public void setAttrDec3(BigDecimal attrDec3) {
        this.attrDec3 = attrDec3;
    }
    @Transient
    private BigDecimal attrDec3Mask;

    public BigDecimal getAttrDec3Mask() {
        attrDec3Mask = attrDec3;
        return attrDec3Mask;
    }

    public String getAttrDec3MaskDD() {
        return "AdditionalInfo_attrDec3Mask";
    }

    public void setAttrDec3Mask(BigDecimal attrDec3Mask) {
        updateDecimalValue("attrDec3", attrDec3Mask);
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDec4">
    private BigDecimal attrDec4;

    public BigDecimal getAttrDec4() {
        return attrDec4;
    }

    public String getAttrDec4DD() {
        return "AdditionalInfo_attrDec4";
    }

    public void setAttrDec4(BigDecimal attrDec4) {
        this.attrDec4 = attrDec4;
    }
    @Transient
    private BigDecimal attrDec4Mask;

    public BigDecimal getAttrDec4Mask() {
        attrDec4Mask = attrDec4;
        return attrDec4Mask;
    }

    public String getAttrDec4MaskDD() {
        return "AdditionalInfo_attrDec4Mask";
    }

    public void setAttrDec4Mask(BigDecimal attrDec4Mask) {
        updateDecimalValue("attrDec4", attrDec4Mask);
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrDec5">
    private BigDecimal attrDec5;

    public BigDecimal getAttrDec5() {
        return attrDec5;
    }

    public String getAttrDec5DD() {
        return "AdditionalInfo_attrDec5";
    }

    public void setAttrDec5(BigDecimal attrDec5) {
        this.attrDec5 = attrDec5;
    }
    @Transient
    private BigDecimal attrDec5Mask;

    public BigDecimal getAttrDec5Mask() {
        attrDec5Mask = attrDec5;
        return attrDec5Mask;
    }

    public String getAttrDec5MaskDD() {
        return "AdditionalInfo_attrDec5Mask";
    }

    public void setAttrDec5Mask(BigDecimal attrDec5Mask) {
        updateDecimalValue("attrDec5", attrDec5Mask);
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrInt1">
    private Integer attrInt1;

    public Integer getAttrInt1() {
        return attrInt1;
    }

    public String getAttrInt1DD() {
        return "AdditionalInfo_attrInt1";
    }

    public void setAttrInt1(Integer attrInt1) {
        this.attrInt1 = attrInt1;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrInt2">
    private Integer attrInt2;

    public Integer getAttrInt2() {
        return attrInt2;
    }

    public String getAttrInt2DD() {
        return "AdditionalInfo_attrInt2";
    }

    public void setAttrInt2(Integer attrInt2) {
        this.attrInt2 = attrInt2;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrInt3">
    private Integer attrInt3;

    public Integer getAttrInt3() {
        return attrInt3;
    }

    public String getAttrInt3DD() {
        return "AdditionalInfo_attrInt3";
    }

    public void setAttrInt3(Integer attrInt3) {
        this.attrInt3 = attrInt3;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrInt4">
    private Integer attrInt4;

    public Integer getAttrInt4() {
        return attrInt4;
    }

    public String getAttrInt4DD() {
        return "AdditionalInfo_attrInt4";
    }

    public void setAttrInt4(Integer attrInt4) {
        this.attrInt4 = attrInt4;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="attrInt5">
    private Integer attrInt5;

    public Integer getAttrInt5() {
        return attrInt5;
    }

    public String getAttrInt5DD() {
        return "AdditionalInfo_attrInt5";
    }

    public void setAttrInt5(Integer attrInt5) {
        this.attrInt5 = attrInt5;
    }
    // </editor-fold >
}
