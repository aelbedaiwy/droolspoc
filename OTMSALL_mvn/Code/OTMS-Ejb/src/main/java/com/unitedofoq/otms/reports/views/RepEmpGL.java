
package com.unitedofoq.otms.reports.views;

import java.math.BigDecimal;
import javax.persistence.*;

@Entity
public class RepEmpGL extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="account">
    @Column
    private String account;

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }

    public String getAccountDD() {
        return "RepEmpGL_account";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="calcPeriod">
    @Column
    private String calcPeriod;

    public void setCalcPeriod(String calcPeriod) {
        this.calcPeriod = calcPeriod;
    }

    public String getCalcPeriod() {
        return calcPeriod;
    }

    public String getCalcPeriodDD() {
        return "RepEmpGL_calcPeriod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="amount">
    @Column
    private BigDecimal amount;

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getAmountDD() {
        return "RepEmpGL_amount";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="debitOrCredit">
    @Column
    private String debitOrCredit;

    public void setDebitOrCredit(String debitOrCredit) {
        this.debitOrCredit = debitOrCredit;
    }

    public String getDebitOrCredit() {
        return debitOrCredit;
    }

    public String getDebitOrCreditDD() {
        return "RepEmpGL_debitOrCredit";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ccGrp">
    @Column
    private String ccGrp;

    public void setCcGrp(String ccGrp) {
        this.ccGrp = ccGrp;
    }

    public String getCcGrp() {
        return ccGrp;
    }

    public String getCcGrpDD() {
        return "RepEmpGL_ccGrp";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="legalEntity">
    @Column
    private String legalEntity;

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public String getLegalEntityDD() {
        return "RepEmpGL_legalEntity";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="salaryElementCode">
    @Column
    private String salaryElementCode;

    public void setSalaryElementCode(String salaryElementCode) {
        this.salaryElementCode = salaryElementCode;
    }

    public String getSalaryElementCode() {
        return salaryElementCode;
    }

    public String getSalaryElementCodeDD() {
        return "RepEmpGL_salaryElementCode";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @Column
    private String salaryElement;

    public void setSalaryElement(String salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElement() {
        return salaryElement;
    }

    public String getSalaryElementDD() {
        return "RepEmpGL_salaryElement";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="currencyDescription">
    private String currencyDescription;

    public String getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(String currencyDescription) {
        this.currencyDescription = currencyDescription;
    }
    
    public String getCurrencyDescriptionDD() {
        return "RepEmpGL_currencyDescription";
    }
    //</editor-fold>
}