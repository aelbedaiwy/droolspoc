package com.unitedofoq.otms.personnel.dayoff;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;
import com.unitedofoq.otms.payroll.formula.FormulaDistinctName;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
//@ParentEntity(fields={"dayOff"})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DOSTYPE")//DayOff Type(vacation,absence) Salary Element type (I,D)
public class DayOffAffectingSalaryElement extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="dayOff">
    @Transient
    private DayOff dayOff;

    public DayOff getDayOff() {
        return dayOff;
    }

    public void setDayOff(DayOff dayOff) {
        this.dayOff = dayOff;
    }

    public String getDayOffDD() {
        return "DayOffAffectingSalaryElement_dayOff";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectingPercent">
    @Column(precision = 18, scale = 3)
    private BigDecimal affectingPercent;

    public BigDecimal getAffectingPercent() {
        return affectingPercent;
    }

    public void setAffectingPercent(BigDecimal affectingPercent) {
        this.affectingPercent = affectingPercent;
    }

    public String getAffectingPercentDD() {
        return "DayOffAffectingSalaryElement_affectingPercent";
    }
    @Transient
    private BigDecimal affectingPercentMask;

    public BigDecimal getAffectingPercentMask() {
        affectingPercentMask = affectingPercent;
        return affectingPercentMask;
    }

    public void setAffectingPercentMask(BigDecimal affectingPercentMask) {
        updateDecimalValue("affectingPercent", affectingPercentMask);
    }

    public String getAffectingPercentMaskDD() {
        return "DayOffAffectingSalaryElement_affectingPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision = 18, scale = 3)
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "DayOffAffectingSalaryElement_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue", maximumValueMask);
    }

    public String getMaximumValueMaskDD() {
        return "DayOffAffectingSalaryElement_maximumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumValue">
    @Column(precision = 18, scale = 3)
    private BigDecimal minimumValue;

    public BigDecimal getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(BigDecimal minimumValue) {
        this.minimumValue = minimumValue;
    }

    public String getMinimumValueDD() {
        return "DayOffAffectingSalaryElement_minimumValue";
    }
    @Transient
    private BigDecimal minimumValueMask;

    public BigDecimal getMinimumValueMask() {
        minimumValueMask = minimumValue;
        return minimumValueMask;
    }

    public void setMinimumValueMask(BigDecimal minimumValueMask) {
        updateDecimalValue("minimumValue", minimumValueMask);
    }

    public String getMinimumValueMaskDD() {
        return "DayOffAffectingSalaryElement_minimumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculateOffDay">
    @Column
    private String calculateOffDay;

    public String getCalculateOffDay() {
        return calculateOffDay;
    }

    public void setCalculateOffDay(String calculateOffDay) {
        this.calculateOffDay = calculateOffDay;
    }

    public String getCalculateOffDayDD() {
        return "DayOffAffectingSalaryElement_calculateOffDay";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="halfDay">
    @Column
    private String halfDay;

    public String getHalfDay() {
        return halfDay;
    }

    public void setHalfDay(String halfDay) {
        this.halfDay = halfDay;
    }

    public String getHalfDayDD() {
        return "DayOffAffectingSalaryElement_halfDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fixedAmount">
    @Column(length = 1)
    //Y - N
    private String fixedAmount;

    public String getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(String fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public String getFixedAmountDD() {
        return "DayOffAffectingSalaryElement_fixedAmount";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="officialHolidayEffect">
    @Column(precision = 18, scale = 3)
    private BigDecimal officialHolidayEffect;

    public BigDecimal getOfficialHolidayEffect() {
        return officialHolidayEffect;
    }

    public void setOfficialHolidayEffect(BigDecimal officialHolidayEffect) {
        this.officialHolidayEffect = officialHolidayEffect;
    }

    public String getOfficialHolidayEffectDD() {
        return "DayOffAffectingSalaryElement_officialHolidayEffect";
    }
    @Transient
    private BigDecimal officialHolidayEffectMask;

    public BigDecimal getOfficialHolidayEffectMask() {
        officialHolidayEffectMask = officialHolidayEffect;
        return officialHolidayEffectMask;
    }

    public void setOfficialHolidayEffectMask(BigDecimal officialHolidayEffectMask) {
        updateDecimalValue("officialHolidayEffect", officialHolidayEffectMask);
    }

    public String getOfficialHolidayEffectMaskDD() {
        return "DayOffAffectingSalaryElement_officialHolidayEffectMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromValue">
    @Column(precision = 18, scale = 3)
    private BigDecimal fromValue;

    public BigDecimal getFromValue() {
        return fromValue;
    }

    public void setFromValue(BigDecimal fromValue) {
        this.fromValue = fromValue;
    }

    public String getFromValueDD() {
        return "DayOffAffectingSalaryElement_fromValue";
    }
    @Transient
    private BigDecimal fromValueMask;

    public BigDecimal getFromValueMask() {
        fromValueMask = fromValue;
        return fromValueMask;
    }

    public void setFromValueMask(BigDecimal fromValueMask) {
        updateDecimalValue("fromValue", fromValueMask);
    }

    public String getFromValueMaskDD() {
        return "DayOffAffectingSalaryElement_fromValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toValue">
    @Column(precision = 18, scale = 3)
    private BigDecimal toValue;

    public BigDecimal getToValue() {
        return toValue;
    }

    public void setToValue(BigDecimal toValue) {
        this.toValue = toValue;
    }

    public String getToValueDD() {
        return "DayOffAffectingSalaryElement_toValue";
    }
    @Transient
    private BigDecimal toValueMask;

    public BigDecimal getToValueMask() {
        toValueMask = toValue;
        return toValueMask;
    }

    public void setToValueMask(BigDecimal toValueMask) {
        updateDecimalValue("toValue", toValueMask);
    }

    public String getToValueMaskDD() {
        return "DayOffAffectingSalaryElement_toValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dtype">
    @Transient
    private String dtype;

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getDtypeDD() {
        return "DayOffAffectingSalaryElement_dtype";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="percentFormula">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName percentFormula;

    public FormulaDistinctName getPercentFormula() {
        return percentFormula;
    }

    public String getPercentFormulaDD() {
        return "DayOffAffectingSalaryElement_percentFormula";
    }

    public void setPercentFormula(FormulaDistinctName percentFormula) {
        this.percentFormula = percentFormula;
    }
    // </editor-fold >
}
