/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.scheduleevent.ScheduleEvent;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author mragab
 */
@Entity
@ChildEntity(fields={"preSkills", "preActivities","upgradedSkills","scheduleEvents","costIems","cancellationRegulations"})
@ParentEntity(fields={"provider"})
public class ProviderActivity extends ActivityBase {
    // <editor-fold defaultstate="collapsed" desc="activity">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getActivityDD() {
        return "ProviderActivity_activity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
	private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "ProviderActivity_provider";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minAttendPercent">
    @Column
    private Short minAttendPercent;

    public Short getMinAttendPercent() {
        return minAttendPercent;
    }

    public void setMinAttendPercent(Short minAttendPercent) {
        this.minAttendPercent = minAttendPercent;
    }

    public String getMinAttendPercentDD() {
        return "ProviderActivity_minAttendPercent";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendanceEvaluation">
    @Column
    private boolean attendanceEvaluation;

    public boolean isAttendanceEvaluation() {
        return attendanceEvaluation;
    }

    public void setAttendanceEvaluation(boolean attendanceEvaluation) {
        this.attendanceEvaluation = attendanceEvaluation;
    }

    public String getAttendanceEvaluationDD() {
        return "ProviderActivity_attendanceEvaluation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="performanceEvaluation">
    @Column
    private boolean performanceEvaluation;

    public boolean isPerformanceEvaluation() {
        return performanceEvaluation;
    }

    public void setPerformanceEvaluation(boolean performanceEvaluation) {
        this.performanceEvaluation = performanceEvaluation;
    }
    
    public String getPerformanceEvaluationDD() {
        return "ProviderActivity_performanceEvaluation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="topics">
	private String topics;

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getTopicsDD() {
        return "ProviderActivity_topics";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="preSkills">
    @OneToMany(mappedBy="providerActivity")
	private List<ProviderActivityPreSkill> preSkills;

    public List<ProviderActivityPreSkill> getPreSkills() {
        return preSkills;
    }

    public void setPreSkills(List<ProviderActivityPreSkill> preSkills) {
        this.preSkills = preSkills;
    }

    public String getPreSkillsDD() {
        return "ProviderActivity_preSkills";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="preProviderActivities">
    @OneToMany(mappedBy="providerActivity")
	private List<ProviderActivityPreActivity> preActivities;

    public List<ProviderActivityPreActivity> getPreActivities() {
        return preActivities;
    }

    public void setPreActivities(List<ProviderActivityPreActivity> preActivities) {
        this.preActivities = preActivities;
    }

    public String getPreActivitiesDD() {
        return "ProviderActivity_preActivities";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="upgradedSkills">
    @OneToMany(mappedBy="providerActivity")
	private List<ProviderActivityUpgradeSkill> upgradedSkills;

    public List<ProviderActivityUpgradeSkill> getUpgradedSkills() {
        return upgradedSkills;
    }

    public void setUpgradedSkills(List<ProviderActivityUpgradeSkill> upgradedSkills) {
        this.upgradedSkills = upgradedSkills;
    }

    public String getUpgradedSkillsDD() {
        return "ProviderActivity_upgradedSkills";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costIems">
    @OneToMany(mappedBy="providerActivity")
	private List<ProviderActivityCostItem> costIems;

    public List<ProviderActivityCostItem> getCostIems() {
        return costIems;
    }

    public void setCostIems(List<ProviderActivityCostItem> costIems) {
        this.costIems = costIems;
    }

    public String getCostIemsDD() {
        return "ProviderActivity_costIems";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scheduleEvents">
    @OneToMany(mappedBy="providerActivity")
	private List<ScheduleEvent> scheduleEvents;

    public List<ScheduleEvent> getScheduleEvents() {
        return scheduleEvents;
    }

    public void setScheduleEvents(List<ScheduleEvent> scheduleEvents) {
        this.scheduleEvents = scheduleEvents;
    }

    public String getScheduleEventsDD() {
        return "ProviderActivity_scheduleEvents";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancellationRegulations">
    @OneToMany(mappedBy="providerActivity")
	private List<ProviderActivityCancellationRegulation> cancellationRegulations;

    public List<ProviderActivityCancellationRegulation> getCancellationRegulations() {
        return cancellationRegulations;
    }

    public void setCancellationRegulations(List<ProviderActivityCancellationRegulation> cancellationRegulations) {
        this.cancellationRegulations = cancellationRegulations;
    }

    public String getCancellationRegulationsDD() {
        return "ProviderActivity_cancellationRegulations";
    }
    // </editor-fold>
}
