/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author lap2
 */
@Entity
public class RepEmployeeOtherInfo extends RepEmployeeBase {
    // <editor-fold defaultstate="collapsed" desc="educationDegree">
    @Transient
    @Translation(originalField = "educationDegree")
    private String educationDegreeTranslated;

    public String getEducationDegreeTranslated() {
        return educationDegreeTranslated;
    }

    public void setEducationDegreeTranslated(String educationDegreeTranslated) {
        this.educationDegreeTranslated = educationDegreeTranslated;
    }

    public String getEducationDegreeTranslatedDD() {
        return "RepEmployeeOtherInfo_educationDegreeTranslated";
    }
    @Translatable(translationField = "educationDegreeTranslated")
    @Column
    private String educationDegree;

    public String getEducationDegree() {
        return educationDegree;
    }

    public void setEducationDegree(String educationDegree) {
        this.educationDegree = educationDegree;
    }

    public String getEducationDegreeDD() {
        return "RepEmployeeOtherInfo_educationDegree";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="grade">
    @Transient
    @Translation(originalField = "grade")
    private String gradeTranslated;

    public String getGradeTranslated() {
        return gradeTranslated;
    }

    public void setGradeTranslated(String gradeTranslated) {
        this.gradeTranslated = gradeTranslated;
    }

    public String getGradeTranslatedDD() {
        return "RepEmployeeOtherInfo_gradeTranslated";
    }
    @Translatable(translationField = "gradeTranslated")
    @Column
    private String grade;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGradeDD() {
        return "RepEmployeeOtherInfo_grade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="graduationPlace">
    @Transient
    @Translation(originalField = "graduationPlace")
    private String graduationPlaceTranslated;

    public String getGraduationPlaceTranslated() {
        return graduationPlaceTranslated;
    }

    public void setGraduationPlaceTranslated(String graduationPlaceTranslated) {
        this.graduationPlaceTranslated = graduationPlaceTranslated;
    }

    public String getGraduationPlaceTranslatedDD() {
        return "RepEmployeeOtherInfo_graduationPlaceTranslated";
    }
    @Translatable(translationField = "graduationPlaceTranslated")
    @Column
    private String graduationPlace;

    public String getGraduationPlace() {
        return graduationPlace;
    }

    public void setGraduationPlace(String graduationPlace) {
        this.graduationPlace = graduationPlace;
    }

    public String getGraduationPlaceDD() {
        return "RepEmployeeOtherInfo_graduationPlace";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="graduationYear">
    private int graduationYear;

    public int getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }

    public String getGraduationYearDD() {
        return "RepEmployeeOtherInfo_graduationYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="militaryStatus">
    private String militaryStatus;

    public String getMilitaryStatus() {
        return militaryStatus;
    }

    public void setMilitaryStatus(String militaryStatus) {
        this.militaryStatus = militaryStatus;
    }

    public String getMilitaryStatusDD() {
        return "RepEmployeeOtherInfo_militaryStatus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postponeDate">  
    @Temporal(TemporalType.TIMESTAMP)
    private Date postponeDate;

    public Date getPostponeDate() {
        return postponeDate;
    }

    public void setPostponeDate(Date postponeDate) {
        this.postponeDate = postponeDate;
    }

    public String getPostponeDateDD() {
        return "RepEmployeeOtherInfo_postponeDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="healthCertificate">  
    @Transient
    @Translation(originalField = "healthCertificate")
    private String healthCertificateTranslated;

    public String getHealthCertificateTranslated() {
        return healthCertificateTranslated;
    }

    public void setHealthCertificateTranslated(String healthCertificateTranslated) {
        this.healthCertificateTranslated = healthCertificateTranslated;
    }

    public String getHealthCertificateTranslatedDD() {
        return "RepEmployeeOtherInfo_healthCertificateTranslated";
    }
    @Translatable(translationField = "healthCertificateTranslated")
    @Column
    private String healthCertificate;

    public String getHealthCertificate() {
        return healthCertificate;
    }

    public void setHealthCertificate(String healthCertificate) {
        this.healthCertificate = healthCertificate;
    }

    public String getHealthCertificateDD() {
        return "RepEmployeeOtherInfo_healthCertificate";
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="healthCertificateEndDate"> 
    @Temporal(TemporalType.TIMESTAMP)
    private Date healthCertificateEndDate;

    public Date getHealthCertificateEndDate() {
        return healthCertificateEndDate;
    }

    public void setHealthCertificateEndDate(Date healthCertificateEndDate) {
        this.healthCertificateEndDate = healthCertificateEndDate;
    }

    public String getHealthCertificateEndDateDD() {
        return "RepEmployeeOtherInfo_healthCertificateEndDate";
    }
    // </editor-fold> 
}
