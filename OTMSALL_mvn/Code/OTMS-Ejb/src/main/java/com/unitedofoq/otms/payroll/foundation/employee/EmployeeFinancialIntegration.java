package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.otms.payroll.*;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;

import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.math.BigDecimal;


/**
 * 
 */
@Entity
@ParentEntity(fields="employee")
public class EmployeeFinancialIntegration extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="amount">
    @Column(precision=25, scale=13)
    private BigDecimal amount;
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getAmountDD() {
        return "EmployeeFinancialIntegration_amount";
    }
    @Transient
    private BigDecimal amountMask;
    public BigDecimal getAmountMask() {
        amountMask = amount;
        return amountMask;
    }
    public void setAmountMask(BigDecimal amountMask) {
        updateDecimalValue("amount",amountMask);
    }
    public String getAmountMaskDD() {
        return "EmployeeFinancialIntegration_amountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	@JoinColumn(nullable=false)
	private CalculatedPeriod payCalculatedPeriod;
    public CalculatedPeriod getPayCalculatedPeriod() {
        return payCalculatedPeriod;
    }
    public void setPayCalculatedPeriod(CalculatedPeriod payCalculatedPeriod) {
        this.payCalculatedPeriod = payCalculatedPeriod;
    }
    public String getPayCalculatedPeriodDD() {
        return "EmployeeFinancialIntegration_payCalculatedPeriod";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;  //Copy of the salary element currency
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getCurrencyDD() {
        return "EmployeeFinancialIntegration_currency";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;
    public Employee getEmployee() {
        return employee;
    }
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeFinancialIntegration_employee";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private SalaryElement salaryElement;
    public SalaryElement getSalaryElement() {
        return salaryElement;
    }
    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    public String getSalaryElementDD() {
        return "EmployeeFinancialIntegration_salaryElement";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="creditOrDebit">
    @Column(length=1)
    private String creditOrDebit;

    public String getCreditOrDebit() {
        return creditOrDebit;
    }

    public void setCreditOrDebit(String creditOrDebit) {
        this.creditOrDebit = creditOrDebit;
    }
    
    public String getCreditOrDebitDD() {
        return "EmployeeFinancialIntegration_creditOrDebit";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="glAccount">
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }
    
    public String getGlAccountDD() {
        return "EmployeeFinancialIntegration_glAccount";
    }
    //</editor-fold>

}