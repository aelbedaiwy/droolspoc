/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author lahmed
 */
@Entity
public class VacationEntitlement extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="vacation">

    @ManyToOne
    @JoinColumn(nullable = false)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "VacationEntitlement_vacation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="religion">
    @ManyToOne
    private UDC religion;

    public UDC getReligion() {
        return religion;
    }

    public void setReligion(UDC religion) {
        this.religion = religion;
    }

    public String getReligionDD() {
        return "VacationEntitlement_religion";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gender">
    @ManyToOne
    private UDC gender;

    public UDC getGender() {
        return gender;
    }

    public void setGender(UDC gender) {
        this.gender = gender;
    }

    public String getGenderDD() {
        return "VacationEntitlement_gender";
    }
    // </editor-fold>
}
