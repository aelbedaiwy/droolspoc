/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.jobrequisitionapplicant;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arezk
 */
@Entity
public class JobReqAppEventNotification extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String title;
    public String getTitleDD(){
        return "JobReqAppEventNotification_title";
    }
    @Column(nullable = false, unique = true)
    private String toEmail;
    public String getToEmailDD(){
        return "JobReqAppEventNotification_toEmail";
    }
    @Column(nullable = false, unique = true)
    private String body;
    public String getBodyDD(){
        return "JobReqAppEventNotification_body";
    }
    @Temporal(TemporalType.DATE)
    @Column(nullable = false, unique = true)
    private Date sentDate;
    public String getSentDateDD(){
        return "JobReqAppEventNotification_sentDate";
    }
    @Column(nullable = false, unique = true)
    private boolean notigyApplicant;// = new Boolean(false);
    public String getNotigyApplicantDD(){
        return "JobReqAppEventNotification_notigyApplicant";
    }
    @Column(nullable = false, unique = true)
    private boolean notifyOwner;// = new Boolean(false);
    public String getNotifyOwnerDD(){
        return "JobReqAppEventNotification_notifyOwner";
    }
    //Attributes from direct assosiation relations
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "notification", optional = false)
    private JobReqAppEvent jobEvent;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public JobReqAppEvent getJobEvent() {
        return jobEvent;
    }

    public void setJobEvent(JobReqAppEvent jobEvent) {
        this.jobEvent = jobEvent;
    }

    public boolean isNotifyOwner() {
        return notifyOwner;
    }

    public void setNotifyOwner(boolean notifyOwner) {
        this.notifyOwner = notifyOwner;
    }

    public boolean isNotigyApplicant() {
        return notigyApplicant;
    }

    public void setNotigyApplicant(boolean notigyApplicant) {
        this.notigyApplicant = notigyApplicant;
    }

   

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }
}
