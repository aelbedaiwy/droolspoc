/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.otms.foundation.CostItemMember;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class RequisitionCostItem extends CostItemMember{
    // <editor-fold defaultstate="collapsed" desc="requisition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Requisition requisition;

    public Requisition getRequisition() {
        return requisition;
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }
    
    public String getRequisitionDD(){
        return "RequisitionCostItem_requisition";
    }
    // </editor-fold>
}
