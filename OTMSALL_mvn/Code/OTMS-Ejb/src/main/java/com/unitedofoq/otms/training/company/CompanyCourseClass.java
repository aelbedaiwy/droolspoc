
package com.unitedofoq.otms.training.company;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.competency.Competency;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"company"})
public class CompanyCourseClass extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "CompanyCourseClass_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "CompanyCourseClass_description";
    }
    
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "CompanyCourseClass_description";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "CompanyCourseClass_company";
    }

    //</editor-fold>
    
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }
    
    public String getCompetencyDD() {
        return "CompanyCourseClass_competency";
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    
    
    
}