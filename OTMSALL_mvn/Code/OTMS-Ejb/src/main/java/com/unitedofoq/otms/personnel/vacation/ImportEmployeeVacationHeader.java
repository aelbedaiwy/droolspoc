/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

/**
 *
 * @author lahmed
 */
@Entity
public class ImportEmployeeVacationHeader extends BaseEntity{
    //<editor-fold defaultstate="collapsed" desc="empCode">
    private String empCode;

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }
    
    public String getEmpCodeDD() {
        return "ImportEmployeeVacationHeader_empCode";
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="vacCode">
    private String vacCode;

    public String getVacCode() {
        return vacCode;
    }

    public void setVacCode(String vacCode) {
        this.vacCode = vacCode;
    }
    
    public String getVacCodeDD() {
        return "ImportEmployeeVacationHeader_vacCode";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="postedBalance">
    private String postedBalance;

    public String getPostedBalance() {
        return postedBalance;
    }

    public void setPostedBalance(String postedBalance) {
        this.postedBalance = postedBalance;
    }
    
    public String getPostedBalanceDD() {
        return "ImportEmployeeVacationHeader_postedBalance";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="takenBalance">
    private String takenBalance;

    public String getTakenBalance() {
        return takenBalance;
    }

    public void setTakenBalance(String takenBalance) {
        this.takenBalance = takenBalance;
    }
    
    public String getTakenBalanceDD() {
        return "ImportEmployeeVacationHeader_takenBalance";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="balance">
    private String balance;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
    
    public String getBalanceDD() {
        return "ImportEmployeeVacationHeader_balance";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="lastPostedDate">
    private String lastPostedDate;

    public String getLastPostedDate() {
        return lastPostedDate;
    }
    public String getLastPostedDateDD() {
        return "ImportEmployeeVacationHeader_lastPostedDate";
    }

    public void setLastPostedDate(String lastPostedDate) {
        this.lastPostedDate = lastPostedDate;
    }
    //</editor-fold >
}
