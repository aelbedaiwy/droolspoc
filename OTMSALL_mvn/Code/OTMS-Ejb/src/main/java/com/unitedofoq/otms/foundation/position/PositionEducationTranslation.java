
package com.unitedofoq.otms.foundation.position;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class PositionEducationTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="schDescription">
    @Column
    private String schDescription;

    public void setSchDescription(String schDescription) {
        this.schDescription = schDescription;
    }

    public String getSchDescription() {
        return schDescription;
    }
    // </editor-fold>

}
