/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.holiday;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@Table(name="OHoliday")
@ParentEntity(fields="company")
@ChildEntity(fields = {"dates"})
//@Table(name = "holiday")
public class Holiday extends BusinessObjectBaseEntity  {
 
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
     public String getCodeDD() {
        return "Holiday_code";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "Holiday_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="natinal">
    @Column(length=1)
    private String natinal;//(name = "natinal")

    public String getNatinal() {
        return natinal;
    }

    public void setNatinal(String natinal) {
        this.natinal = natinal;
    }
    public String getNatinalDD() {
        return "Holiday_natinal";
    }

   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="religion">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC religion;//(name = "religion")

    public UDC getReligion() {
        return religion;
    }

    public void setReligion(UDC religion) {
        this.religion = religion;
    }
    public String getReligionDD() {
        return "Holiday_religion";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="religionDependent">

    @Column(length=1)
    private String religionDependent;//(name = "religion_dependent")

    public String getReligionDependent() {
        return religionDependent;
    }

    public void setReligionDependent(String religionDependent) {
        this.religionDependent = religionDependent;
    }

    public String getReligionDependentDD() {
        return "Holiday_religionDependent";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="timeManagemenetAffect">
    @Column(length=1)
    private String timeManagemenetAffect;//(name = "tm_flag")

    public String getTimeManagemenetAffect() {
        return timeManagemenetAffect;
    }

    public void setTimeManagemenetAffect(String timeManagemenetAffect) {
        this.timeManagemenetAffect = timeManagemenetAffect;
    }
    public String getTimeManagemenetAffectDD() {
        return "Holiday_timeManagemenetAffect";
    }

     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dates">
    @JoinColumn
    @OneToMany(mappedBy = "holiday")
    private List<HolidayDates> dates;

    public List<HolidayDates> getDates() {
        return dates;
    }

    public void setDates(List<HolidayDates> dates) {
        this.dates = dates;
    }
     public String getDatesDD() {
        return "Holiday_dates";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
     @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)

     private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    public String getCompanyDD() {
        return "Holiday_company";
    }

    // </editor-fold>

}
