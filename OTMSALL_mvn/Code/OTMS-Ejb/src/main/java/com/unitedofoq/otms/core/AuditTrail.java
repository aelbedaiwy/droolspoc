
package com.unitedofoq.otms.core;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.*;

@Entity
public class AuditTrail extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="user">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser user;

    public void setUser(OUser user) {
        this.user = user;
    }

    public OUser getUser() {
        return user;
    }

    public String getUserDD() {
        return "AuditTrail_user";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="operationDone">
    @Column
    private String operationDone;

    public String getOperationDone() {
        return operationDone;
    }

    public void setOperationDone(String operationDone) {
        this.operationDone = operationDone;
    }
    
    public String getOperationDoneDD() {
        return "AuditTrail_operationDone";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="entity">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String entity;

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEntity() {
        return entity;
    }

    public String getEntityDD() {
        return "AuditTrail_entity";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="screen">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String screen;

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getScreen() {
        return screen;
    }

    public String getScreenDD() {
        return "AuditTrail_screen";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="field">
    @Column
    private String field;

    public void setField(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public String getFieldDD() {
        return "AuditTrail_field";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="auditDateTime">
    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date auditDateTime;

    public Date getAuditDateTime() {
        return auditDateTime;
    }

    public void setAuditDateTime(Date auditDateTime) {
        this.auditDateTime = auditDateTime;
    }

    public String getAuditDateTimeDD() {
        return "AuditTrail_auditDateTime";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="newValue">
    @Column
    private String newValue;

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public String getNewValueDD() {
        return "AuditTrail_newValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oldValue">
    @Column
    private String oldValue;

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getOldValue() {
        return oldValue;
    }

    public String getOldValueDD() {
        return "AuditTrail_oldValue";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="entityKey">
    @Column
    private String entityKey;

    public String getEntityKey() {
        return entityKey;
    }

    public void setEntityKey(String entityKey) {
        this.entityKey = entityKey;
    }
    
    public String getEntityKeyDD() {
        return "AuditTrail_entityKey";
    }
    // </editor-fold>

    //    // <editor-fold defaultstate="collapsed" desc="mappedColumn">
//    @Column
//    private String mappedColumn;
//
//    public void setMappedColumn(String mappedColumn) {
//        this.mappedColumn = mappedColumn;
//    }
//
//    public String getMappedColumn() {
//        return mappedColumn;
//    }
//
//    public String getMappedColumnDD() {
//        return "AuditTrail_mappedColumn";
//    }
//    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch= FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD() {
        return "AuditTrail_employee";
    }
    //</editor-fold>
}