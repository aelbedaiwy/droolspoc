
package com.unitedofoq.otms.recruitment.jobapplicant;

import com.unitedofoq.otms.foundation.position.Position;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class JobAppPosRejection extends JobAppRejection {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
	private Position position;

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position thePosition) {
		position = thePosition;
	}
}
