package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name="repemployeevacationbalance")
public class RepEmployeeVacationBalance extends RepEmployeeBase {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeVacationBalance_genderDescription";
    }

    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }

    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeVacationBalance_genderDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepEmployeeVacationBalance_employeeActive";
    }

    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }

    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeeVacationBalance_employeeActive";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="name">
    @Column
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "RepEmployeeVacationBalance_name";
    }

    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "RepEmployeeVacationBalance_name";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialBalance">
    @Column(precision=18, scale=3)
    private BigDecimal initialBalance;

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }
     public String getInitialBalanceDD() {
        return "RepEmployeeVacationBalance_initialBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentBalance">
    @Column(precision=18, scale=3)
    private BigDecimal currentBalance;

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }
     public String getCurrentBalanceDD() {
        return "RepEmployeeVacationBalance_currentBalance";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualBalance">
    @Column(precision=18, scale=3)
    private BigDecimal actualBalance;

    public BigDecimal getActualBalance() {
        return actualBalance;
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }
     public String getActualBalanceDD() {
        return "RepEmployeeVacationBalance_actualBalance";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="takenBalance">
    @Column(precision=18, scale=3)
    private BigDecimal takenBalance;

    public BigDecimal getTakenBalance() {
        return takenBalance;
    }

    public void setTakenBalance(BigDecimal takenBalance) {
        this.takenBalance = takenBalance;
    }
     public String getTakenBalanceDD() {
        return "RepEmployeeVacationBalance_takenBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postedBalance">
    @Column(precision=18, scale=3)
    private BigDecimal postedBalance;

    public BigDecimal getPostedBalance() {
        return postedBalance;
    }

    public void setPostedBalance(BigDecimal postedBalance) {
        this.postedBalance = postedBalance;
    }
     public String getPostedBalanceDD() {
        return "RepEmployeeVacationBalance_postedBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastPostingDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date lastPostingDate;

    public Date getLastPostingDate() {
        return lastPostingDate;
    }

    public void setLastPostingDate(Date lastPostingDate) {
        this.lastPostingDate = lastPostingDate;
    }
    public String getLastPostingDateDD() {
        return "RepEmployeeVacationBalance_lastPostingDate";
    }
      // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="secondPostingDate, secondPostedBalance, cancelComments *** datatypes r to be changed ***">
    //<editor-fold defaultstate="collapsed" desc="secondPostingDate">
    @Column
    private String secondPostingDate;

    public String getSecondPostingDate() {
        return secondPostingDate;
    }

    public void setSecondPostingDate(String secondPostingDate) {
        this.secondPostingDate = secondPostingDate;
    }

    public String getSecondPostingDateDD() {
        return "RepEmployeeVacationBalance_secondPostingDate";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="secondPostedBalance">
    @Column
    private String secondPostedBalance;

    public String getSecondPostedBalance() {
        return secondPostedBalance;
    }

    public void setSecondPostedBalance(String secondPostedBalance) {
        this.secondPostedBalance = secondPostedBalance;
    }

    public String getSecondPostedBalanceDD() {
        return "RepEmployeeVacationBalance_secondPostedBalance";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cancelComments">
    @Column
    private String cancelComments;

    public String getCancelComments() {
        return cancelComments;
    }

    public void setCancelComments(String cancelComments) {
        this.cancelComments = cancelComments;
    }

    public String getCancelCommentsDD() {
        return "RepEmployeeVacationBalance_cancelComments";
    }
    //</editor-fold>
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="adjustmentValue">
    private BigDecimal adjustmentValue;

    public BigDecimal getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(BigDecimal adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    public String getAdjustmentValueDD() {
        return "RepEmployeeVacationBalance_adjustmentValue";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="adjustmentDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date adjustmentDate;

    public Date getAdjustmentDate() {
        return adjustmentDate;
    }

    public void setAdjustmentDate(Date adjustmentDate) {
        this.adjustmentDate = adjustmentDate;
    }

    public String getAdjustmentDateDD() {
        return "RepEmployeeVacationBalance_adjustmentDate";
    }
    // </editor-fold>
}
