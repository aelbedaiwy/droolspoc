package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantReference extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantLanguage_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="referenceeName">
    private String referenceeName;

    public String getReferenceeName() {
        return referenceeName;
    }

    public void setReferenceeName(String referenceeName) {
        this.referenceeName = referenceeName;
    }

    public String getReferenceeNameDD() {
        return "ApplicantLanguage_referenceeName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="referenceePosition">
    private String referenceePosition;

    public String getReferenceePosition() {
        return referenceePosition;
    }

    public void setReferenceePosition(String referenceePosition) {
        this.referenceePosition = referenceePosition;
    }

    public String getReferenceePositionDD() {
        return "ApplicantLanguage_referenceePosition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="referenceeCompany">
    private String referenceeCompany;

    public String getReferenceeCompany() {
        return referenceeCompany;
    }

    public void setReferenceeCompany(String referenceeCompany) {
        this.referenceeCompany = referenceeCompany;
    }

    public String getReferenceeCompanyDD() {
        return "ApplicantLanguage_referenceeCompany";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="referenceeNumber">
    private String referenceeNumber;

    public String getReferenceeNumber() {
        return referenceeNumber;
    }

    public void setReferenceeNumber(String referenceeNumber) {
        this.referenceeNumber = referenceeNumber;
    }

    public String getReferenceeNumberDD() {
        return "ApplicantReference_referenceeNumber";
    }
    // </editor-fold>
}