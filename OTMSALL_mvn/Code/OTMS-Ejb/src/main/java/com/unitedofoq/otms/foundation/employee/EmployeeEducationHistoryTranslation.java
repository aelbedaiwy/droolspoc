/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author mostafa
 */
@Entity
@Table(name= "empeduhistranslation")
public class EmployeeEducationHistoryTranslation extends BaseEntityTranslation{
//<editor-fold defaultstate="collapsed" desc="description translation"> 
    @Column
    private String schDescription;

    public String getSchDescription() {
        return schDescription;
    }

    public void setSchDescription(String schDescription) {
        this.schDescription = schDescription;
    }
//</editor-fold> 

}