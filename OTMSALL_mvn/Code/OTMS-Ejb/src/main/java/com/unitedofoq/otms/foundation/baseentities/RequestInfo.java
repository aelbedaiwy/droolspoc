package com.unitedofoq.otms.foundation.baseentities;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class RequestInfo extends BaseEntity {

    @Temporal(TemporalType.DATE)
    protected Date approvalDate;
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    protected Date requestDate;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected Employee approvedBy;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected Employee toBeApprovedBy;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    protected Employee requestedBy;
}
