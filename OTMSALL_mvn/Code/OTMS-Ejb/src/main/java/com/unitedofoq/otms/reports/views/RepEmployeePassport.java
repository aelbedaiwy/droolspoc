/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="RepEmployeePassport")
public class RepEmployeePassport extends RepEmployeeBase {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeePassport_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeePassport_genderDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepEmployeePassport_employeeActive";
    }
    
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeePassport_employeeActive";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="renewalDate">
     @Column
     @Temporal(javax.persistence.TemporalType.DATE)
     private Date renewalDate;

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

     public String getRenewalDateDD() {
        return "RepEmployeePassport_renewalDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="entrancePort">
    @Column
    private String entrancePort;

    public String getEntrancePort() {
        return entrancePort;
    }

    public void setEntrancePort(String entrancePort) {
        this.entrancePort = entrancePort;
    }

    public String getEntrancePortDD() {
        return "RepEmployeePassport_entrancePort";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="passportNumber">
    @Column
    private String passportNumber;

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportNumberDD() {
        return "RepEmployeePassport_passportNumber";
    }

    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="entranceDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date entranceDate;

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public String getEntranceDateDD() {
        return "RepEmployeePassport_entranceDate";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="requestNotes">
    @Column
    private String requestNotes;

    public String getRequestNotes() {
        return requestNotes;
    }

    public void setRequestNotes(String requestNotes) {
        this.requestNotes = requestNotes;
    }

    public String getRequestNotesDD() {
        return "RepEmployeePassport_requestNotes";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="issueDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date issueDate;

        public Date getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(Date issueDate) {
            this.issueDate = issueDate;
        }
        public String getIssueDateDD() {
            return "RepEmployeePassport_issueDate";
        }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="remarks">
    private String remarks;

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

         public String getRemarksDD() {
            return "RepEmployeePassport_remarks";
        }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @Column
    @Translatable(translationField = "statusTranslated")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
     public String getStatusDD() {
        return "RepEmployeePassport_status";
    }
     
    @Transient
    @Translation(originalField = "status")
    private String statusTranslated;

    public String getStatusTranslated() {
        return statusTranslated;
    }

    public void setStatusTranslated(String statusTranslated) {
        this.statusTranslated = statusTranslated;
    }
    
    public String getStatusTranslatedDD() {
        return "RepEmployeePassport_status";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="passportJobFamily">
    @Column
    @Translatable(translationField = "passportJobFamilyTranslated")
    private String passportJobFamily;

    public String getPassportJobFamily() {
        return passportJobFamily;
    }

    public void setPassportJobFamily(String passportJobFamily) {
        this.passportJobFamily = passportJobFamily;
    }
    public String getPassportJobFamilyDD() {
        return "RepEmployeePassport_passportJobFamily";
    }
    
    @Transient
    @Translation(originalField = "passportJobFamily")
    private String passportJobFamilyTranslated;

    public String getPassportJobFamilyTranslated() {
        return passportJobFamilyTranslated;
    }

    public void setPassportJobFamilyTranslated(String passportJobFamilyTranslated) {
        this.passportJobFamilyTranslated = passportJobFamilyTranslated;
    }
    
    public String getPassportJobFamilyTranslatedDD() {
        return "RepEmployeePassport_passportJobFamily";
    }
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="followupBy">
    @Column
    @Translatable(translationField = "followupByTranslated")
    private String followupBy;

    public String getFollowupBy() {
        return followupBy;
    }

    public void setFollowupBy(String followupBy) {
        this.followupBy = followupBy;
    }
    public String getFollowupByDD() {
        return "RepEmployeePassport_followupBy";
    }
    
    @Transient
    @Translation(originalField = "followupBy")
    private String followupByTranslated;

    public String getFollowupByTranslated() {
        return followupByTranslated;
    }

    public void setFollowupByTranslated(String followupByTranslated) {
        this.followupByTranslated = followupByTranslated;
    }
    
    public String getFollowupByTranslatedDD() {
        return "RepEmployeePassport_followupBy";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="issueLocation">
    @Translatable(translationField = "issueLocationTranslated")
    private String issueLocation;

    public String getIssueLocation() {
        return issueLocation;
    }

    public void setIssueLocation(String issueLocation) {
        this.issueLocation = issueLocation;
    }

    public String getIssueLocationDD() {
        return "RepEmployeePassport_issueLocation";
    }
    
    @Transient
    @Translation(originalField = "issueLocation")
    private String issueLocationTranslated;
    
    public String getIssueLocationTranslated() {
        return issueLocationTranslated;
    }

    public void setIssueLocationTranslated(String issueLocationTranslated) {
        this.issueLocationTranslated = issueLocationTranslated;
    }
    
    public String getIssueLocationTranslatedDD() {
        return "RepEmployeePassport_issueLocation";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="type">
    @Translatable(translationField = "typeTranslated")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "RepEmployeePassport_type";
    }
    
    @Transient
    @Translation(originalField = "type")
    private String typeTranslated;

    public String getTypeTranslated() {
        return typeTranslated;
    }

    public void setTypeTranslated(String typeTranslated) {
        this.typeTranslated = typeTranslated;
    }
    
    public String getTypeTranslatedDD() {
        return "RepEmployeePassport_type";
    }
    //</editor-fold>
}
