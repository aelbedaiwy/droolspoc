package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "APPLYON")
@ParentEntity(fields = "applicant")
@ChildEntity(fields = {"histories"})
public class AppliedApplicant extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public String getApplicantDD() {
        return "AppliedApplicant_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "AppliedApplicant_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public void setComment(String comments) {
        this.comments = comments;
    }

    public String getComment() {
        return comments;
    }

    public String getCommentDD() {
        return "AppliedApplicant_comments";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="applicationDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date applicationDate;

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getApplicationDateDD() {
        return "AppliedApplicant_applicationDate";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentAction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC currentAction;

    public UDC getCurrentAction() {
        return currentAction;
    }

    public void setCurrentAction(UDC currentAction) {
        this.currentAction = currentAction;
    }

    public String getCurrentActionDD() {
        return "AppliedApplicant_currentAction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentActionWhen">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date currentActionWhen;

    public Date getCurrentActionWhen() {
        return currentActionWhen;
    }

    public void setCurrentActionWhen(Date currentActionWhen) {
        this.currentActionWhen = currentActionWhen;
    }

    public String getCurrentActionWhenDD() {
        return "AppliedApplicant_currentActionWhen";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentActionWhat">
    private String currentActionWhat;

    public String getCurrentActionWhat() {
        return currentActionWhat;
    }

    public void setCurrentActionWhat(String currentActionWhat) {
        this.currentActionWhat = currentActionWhat;
    }

    public String getCurrentActionWhatDD() {
        return "AppliedApplicant_currentActionWhat";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentActionWho">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee currentActionWho;

    public Employee getCurrentActionWho() {
        return currentActionWho;
    }

    public void setCurrentActionWho(Employee currentActionWho) {
        this.currentActionWho = currentActionWho;
    }

    public String getCurrentActionWhoDD() {
        return "AppliedApplicant_currentActionWho";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentActionResult">
    private String currentActionResult;

    public String getCurrentActionResult() {
        return currentActionResult;
    }

    public void setCurrentActionResult(String currentActionResult) {
        this.currentActionResult = currentActionResult;
    }

    public String getCurrentActionResultDD() {
        return "AppliedApplicant_currentActionResult";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentActionComment">
    private String currentActionComment;

    public String getCurrentActionComment() {
        return currentActionComment;
    }

    public void setCurrentActionComment(String currentActionComment) {
        this.currentActionComment = currentActionComment;
    }

    public String getCurrentActionCommentDD() {
        return "AppliedApplicant_currentActionComment";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competenciesMatching">
    @Column(precision = 25, scale = 13)
    private BigDecimal competenciesMatching;

    public BigDecimal getCompetenciesMatching() {
        return competenciesMatching;
    }

    public String getCompetenciesMatchingDD() {
        return "AppliedApplicant_competenciesMatching";
    }

    public void setCompetenciesMatching(BigDecimal competenciesMatching) {
        this.competenciesMatching = competenciesMatching;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="matchingResult">
    private double matchingResult;

    public double getMatchingResult() {
        return matchingResult;
    }

    public void setMatchingResult(double matchingResult) {
        this.matchingResult = matchingResult;
    }

    public String getMatchingResultDD() {
        return "AppliedApplicant_matchingResult";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="histories">
    @OneToMany(mappedBy = "appliedApplicant")
    private List<AppliedApplicantHistory> histories;

    public List<AppliedApplicantHistory> getHistories() {
        return histories;
    }

    public String getHistoriesDD() {
        return "AppliedApplicant_histories";
    }

    public void setHistories(List<AppliedApplicantHistory> histories) {
        this.histories = histories;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="m2mCheck">
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeKPA_m2mCheck";
    }

    // </editor-fold>
    @Override
    public void PostLoad() {
        super.PostLoad();
        setM2mCheck(true);
    }
}
