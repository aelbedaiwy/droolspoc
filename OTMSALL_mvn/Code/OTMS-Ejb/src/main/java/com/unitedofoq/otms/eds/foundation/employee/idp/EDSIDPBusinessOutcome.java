package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields="idp")
@ChildEntity(fields={"neededIndicators", "accountabilities", "channels", "outcomeForOutLrnIntrs"})
public class EDSIDPBusinessOutcome extends BaseEntity{
    
    private String achievementIndicator;

    public String getAchievementIndicator() {
        return achievementIndicator;
    }

    public void setAchievementIndicator(String achievementIndicator) {
        this.achievementIndicator = achievementIndicator;
    }

    public String getAchievementIndicatorDD(){
        return "EDSIDPBusinessOutcome_achievementIndicator";
    }

    private String possibilityLimmitation;

    public String getPossibilityLimmitation() {
        return possibilityLimmitation;
    }

    public void setPossibilityLimmitation(String possibilityLimmitation) {
        this.possibilityLimmitation = possibilityLimmitation;
    }

    public String getPossibilityLimmitationDD(){
        return "EDSIDPBusinessOutcome_possibilityLimmitation";
    }
    
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    private double contributionPercentage;
    private double confidencePercentage;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSIDPForBusinessOutcome idp;
    @OneToMany(mappedBy="businessOutcome")
    private List<EDSIDPBusinessOutcomeBehavior> neededIndicators;
    @OneToMany(mappedBy="businessOutcome")
    private List<EDSIDPBusinessOutcomeAcc> accountabilities;
    @OneToMany(mappedBy="businessOutcome")
    private List<EDSIDPBusinessOutcomeChannel> channels;
    @OneToMany(mappedBy="businessOutcome")
    private List<EDSIDPBusinessOutcomeForOutLrnIntr> outcomeForOutLrnIntrs;
    public String getOutcomeForOutLrnIntrsDD() {return "EDSIDPBusinessOutcome_outcomeForOutLrnIntrs";}
    public List<EDSIDPBusinessOutcomeForOutLrnIntr> getOutcomeForOutLrnIntrs() {
        return outcomeForOutLrnIntrs;
    }

    public void setOutcomeForOutLrnIntrs(List<EDSIDPBusinessOutcomeForOutLrnIntr> outcomeForOutLrnIntrs) {
        this.outcomeForOutLrnIntrs = outcomeForOutLrnIntrs;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="STARTDATE")
    private Date when;

    @Column(length=1000)
    private String behaviorNeeded;

    public String getBehaviorNeeded() {
        return behaviorNeeded;
    }

    public String getBehaviorNeededDD() {
        return "EDSIDPBusinessOutcome_behaviorNeeded";
    }

    public void setBehaviorNeeded(String behaviorNeeded) {
        this.behaviorNeeded = behaviorNeeded;
    }

    
    public String getWhenDD()                  {   return "EDSIDPBusinessOutcome_when";  }
    public String getChannelsDD()              {   return "EDSIDPBusinessOutcome_channels";  }
    public String getAccountabilitiesDD()      {   return "EDSIDPBusinessOutcome_accountabilities";  }
    public String getNeededIndicatorsDD()      {   return "EDSIDPBusinessOutcome_neededIndicators";  }
    public String getIdpDD()                   {   return "EDSIDPBusinessOutcome_idp";  }
    public String getConfidencePercentageDD()  {   return "EDSIDPBusinessOutcome_confidencePercentage";  }
    public String getContributionPercentageDD(){   return "EDSIDPBusinessOutcome_contributionPercentage";  }
    public String getDescriptionTranslatedDD()           {   return "EDSIDPBusinessOutcome_description";  }

    public List<EDSIDPBusinessOutcomeAcc> getAccountabilities() {
        return accountabilities;
    }

    public void setAccountabilities(List<EDSIDPBusinessOutcomeAcc> accountabilities) {
        this.accountabilities = accountabilities;
    }

    public List<EDSIDPBusinessOutcomeChannel> getChannels() {
        return channels;
    }

    public void setChannels(List<EDSIDPBusinessOutcomeChannel> channels) {
        this.channels = channels;
    }

    public EDSIDPForBusinessOutcome getIdp() {
        return idp;
    }

    public void setIdp(EDSIDPForBusinessOutcome idp) {
        this.idp = idp;
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }

    public double getConfidencePercentage() {
        return confidencePercentage;
    }

    public void setConfidencePercentage(double confidencePercentage) {
        this.confidencePercentage = confidencePercentage;
    }

    public double getContributionPercentage() {
        return contributionPercentage;
    }

    public void setContributionPercentage(double contributionPercentage) {
        this.contributionPercentage = contributionPercentage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EDSIDPBusinessOutcomeBehavior> getNeededIndicators() {
        return neededIndicators;
    }

    public void setNeededIndicators(List<EDSIDPBusinessOutcomeBehavior> neededIndicators) {
        this.neededIndicators = neededIndicators;
    }

    private double compeletionProgress;

    public String getCompeletionProgressDD() {
        return "EDSIDPBusinessOutcome_compeletionProgress";
    }

    public double getCompeletionProgress() {
        return compeletionProgress;
    }

    public void setCompeletionProgress(double compeletionProgress) {
        this.compeletionProgress = compeletionProgress;
    }

    private double evaluation;

    public String getEvaluationDD() {
        return "EDSIDPBusinessOutcome_evaluation";
    }

    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }

    private String evaluationDescription;

    public String getEvaluationDescriptionDD() {
        return "EDSIDPBusinessOutcome_evaluationDescription";
    }

    public String getEvaluationDescription() {
        return evaluationDescription;
    }

    public void setEvaluationDescription(String evaluationDescription) {
        this.evaluationDescription = evaluationDescription;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date evaluationDate;

    public String getEvaluationDateDD() {
        return "EDSIDPBusinessOutcome_evaluationDate";
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }
    private String approvalComment;

    public String getApprovalComment() {
        return approvalComment;
    }

    public void setApprovalComment(String approvalComment) {
        this.approvalComment = approvalComment;
    }
    public String getApprovalCommentDD() {
        return "EDSIDPBusinessOutcome_approvalcomment";
    }

    @Column(name="comments")
    private String evaluationComment;
    public String getEvaluationCommentDD() {
        return "EDSIDPBusinessOutcome_evaluationComment";
    }

    

    public String getEvaluationComment() {
        return evaluationComment;
    }

    public void setEvaluationComment(String evaluationComment) {
        this.evaluationComment = evaluationComment;
    }

    

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSIDPBusinessOutcomeAcc evaluatedBy;

    public EDSIDPBusinessOutcomeAcc getEvaluatedBy() {
        return evaluatedBy;
    }

    public String getEvaluatedByDD() {
        return "EDSIDPBusinessOutcome_evaluatedBy";
    }

    public void setEvaluatedBy(EDSIDPBusinessOutcomeAcc evaluatedBy) {
        this.evaluatedBy = evaluatedBy;
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
}
