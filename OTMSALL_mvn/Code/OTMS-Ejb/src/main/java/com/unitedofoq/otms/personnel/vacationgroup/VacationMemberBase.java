package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.payroll.formula.FormulaDistinctName;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class VacationMemberBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column
    private BigDecimal sortIndex;//vacation_sequence

    public BigDecimal getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(BigDecimal sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "VacationMemberBase_sortIndex";
    }
    @Transient
    private BigDecimal sortIndexMask;

    public BigDecimal getSortIndexMask() {
        sortIndexMask = sortIndex ;
        return sortIndexMask;
    }

    public void setSortIndexMask(BigDecimal sortIndexMask) {
        updateDecimalValue("sortIndex",sortIndexMask);
    }

    public String getSortIndexMaskDD() {
        return "VacationMemberBase_sortIndexMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="amount">
    @Column(precision=18,scale=3)
    private BigDecimal amount;//value

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getAmountDD() {
        return "VacationMemberBase_amount";
    }
    @Transient
    private BigDecimal amountMask;

    public BigDecimal getAmountMask() {
        amountMask = amount ;
        return amountMask;
    }

    public void setAmountMask(BigDecimal amountMask) {
        updateDecimalValue("amount",amountMask);
    }
    public String getAmountMaskDD() {
        return "VacationMemberBase_amountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ageExperience">
    @Column(length=1)
    //Age - Y
    //experience - N
    //Period - P
    private String ageExperience;//age_experience

    public String getAgeExperience() {
        return ageExperience;
    }

    public void setAgeExperience(String ageExperience) {
        this.ageExperience = ageExperience;
    }
    public String getAgeExperienceDD() {
        return "VacationMemberBase_ageExperience";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringPeriodFrom">
    @Column(precision=18,scale=3)
    private BigDecimal hiringPeriodFrom;//hiring_period_from

    public BigDecimal getHiringPeriodFrom() {
        return hiringPeriodFrom;
    }

    public void setHiringPeriodFrom(BigDecimal hiringPeriodFrom) {
        this.hiringPeriodFrom = hiringPeriodFrom;
    }
    public String getHiringPeriodFromDD() {
        return "VacationMemberBase_hiringPeriodFrom";
    }
    @Transient
    private BigDecimal hiringPeriodFromMask;

    public BigDecimal getHiringPeriodFromMask() {
        hiringPeriodFromMask = hiringPeriodFrom ;
        return hiringPeriodFromMask;
    }

    public void setHiringPeriodFromMask(BigDecimal hiringPeriodFromMask) {
        updateDecimalValue("hiringPeriodFrom",hiringPeriodFromMask);
    }
    public String getHiringPeriodFromMaskDD() {
        return "VacationMemberBase_hiringPeriodFromMask";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringPeriodTo">
    @Column(precision=18,scale=3)
    private BigDecimal hiringPeriodTo;//hiring_period_to

    public BigDecimal getHiringPeriodTo() {
        return hiringPeriodTo;
    }

    public void setHiringPeriodTo(BigDecimal hiringPeriodTo) {
        this.hiringPeriodTo = hiringPeriodTo;
    }
    public String getHiringPeriodToDD() {
        return "VacationMemberBase_hiringPeriodTo";
    }
    @Transient
    private BigDecimal hiringPeriodToMask;

    public BigDecimal getHiringPeriodToMask() {
        hiringPeriodToMask = hiringPeriodTo ;
        return hiringPeriodToMask;
    }

    public void setHiringPeriodToMask(BigDecimal hiringPeriodToMask) {
        updateDecimalValue("hiringPeriodTo",hiringPeriodToMask);
    }
    public String getHiringPeriodToMaskDD() {
        return "VacationMemberBase_hiringPeriodToMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="addPreviousExp">
    @Column(length=1)
    //Y
    //N
    private String addPreviousExp;//add_other_exp

    public String getAddPreviousExp() {
        return addPreviousExp;
    }

    public void setAddPreviousExp(String addPreviousExp) {
        this.addPreviousExp = addPreviousExp;
    }

    public String getAddPreviousExpDD() {
        return "VacationMemberBase_addPreviousExp";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factorBefore">
    @Column(precision=18,scale=3)
    private BigDecimal factorBefore;//factor_before

    public BigDecimal getFactorBefore() {
        return factorBefore;
    }

    public void setFactorBefore(BigDecimal factorBefore) {
        this.factorBefore = factorBefore;
    }

    public String getFactorBeforeDD() {
        return "VacationMemberBase_factorBefore";
    }
    @Transient
    private BigDecimal factorBeforeMask;//factor_before

    public BigDecimal getFactorBeforeMask() {
        factorBeforeMask = factorBefore ;
        return factorBeforeMask;
    }

    public void setFactorBeforeMask(BigDecimal factorBeforeMask) {
        updateDecimalValue("factorBeforeMask",factorBeforeMask);
    }

    public String getFactorBeforeMaskDD() {
        return "VacationMemberBase_factorBeforeMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factoreAfter">
    @Column(precision=18,scale=3)
    private BigDecimal factoreAfter;//factore_after

    public BigDecimal getFactoreAfter() {
        return factoreAfter;
    }

    public void setFactoreAfter(BigDecimal factoreAfter) {
        this.factoreAfter = factoreAfter;
    }
    public String getFactoreAfterDD() {
        return "VacationMemberBase_factoreAfter";
    }
    @Transient
    private BigDecimal factoreAfterMask;

    public BigDecimal getFactoreAfterMask() {
        factoreAfterMask = factoreAfter ;
        return factoreAfterMask;
    }

    public void setFactoreAfterMask(BigDecimal factoreAfterMask) {
        updateDecimalValue("factoreAfter",factoreAfterMask);
    }
    public String getFactoreAfterMaskDD() {
        return "VacationMemberBase_factoreAfterMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="formula">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName formula;//formula_name

    public FormulaDistinctName getFormula() {
        return formula;
    }

    public void setFormula(FormulaDistinctName formula) {
        this.formula = formula;
    }
    public String getFormulaDD() {
        return "VacationMemberBase_formula";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingCalender">
    @Column(length=1)
    //Y - N
    private String workingCalender;//working_calender_flag

    public String getWorkingCalender() {
        return workingCalender;
    }

    public void setWorkingCalender(String workingCalender) {
        this.workingCalender = workingCalender;
    }
    public String getWorkingCalenderDD() {
        return "VacationMemberBase_workingCalender";
    }
    // </editor-fold>
}