/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Value;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"position"})
//@Table(name="EDSJobValue")
public class PositionValue extends BusinessObjectBaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "PositionValue_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="value">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Value value;

    public Value getValue() {
        return value;
    }
    public String getValueDD() {
        return "PositionValue_value";
    }
    public void setValue(Value value) {
        this.value = value;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="score">
    @Column
    private Double score;

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getScoreDD() {
        return "PositionValue_score";
    }
    // </editor-fold >
}
