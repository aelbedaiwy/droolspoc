package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ApplicantTranslation extends BaseEntityTranslation {
    // <editor-fold defaultstate="collapsed" desc="name">

    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="address">
    @Column
    private String address;

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="address1">
    @Column
    private String address1;

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="additionalInformation">
    @Column
    private String additionalInformation;

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }
    // </editor-fold>
}
