/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.core;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;

/**
 *
 * @author mostafa
 */
@Entity
public class RepAuditTrail extends BaseEntity{
    
    // <editor-fold defaultstate="collapsed" desc="entity">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String entity;

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEntity() {
        return entity;
    }

    public String getEntityDD() {
        return "RepAuditTrail_entity";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="screen">
    //@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private String screen;

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getScreen() {
        return screen;
    }

    public String getScreenDD() {
        return "RepAuditTrail_screen";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="field">
    @Column
    private String field;

    public void setField(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public String getFieldDD() {
        return "RepAuditTrail_field";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="auditDateTime">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date auditDateTime;

    public Date getAuditDateTime() {
        return auditDateTime;
    }

    public void setAuditDateTime(Date auditDateTime) {
        this.auditDateTime = auditDateTime;
    }

    public String getAuditDateTimeDD() {
        return "RepAuditTrail_auditDateTime";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="newValue">
    @Column
    private String newValue;

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public String getNewValueDD() {
        return "RepAuditTrail_newValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="oldValue">
    @Column
    private String oldValue;

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getOldValue() {
        return oldValue;
    }

    public String getOldValueDD() {
        return "RepAuditTrail_oldValue";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="entityKey">
    @Column
    private String entityKey;

    public String getEntityKey() {
        return entityKey;
    }

    public void setEntityKey(String entityKey) {
        this.entityKey = entityKey;
    }
    
    public String getEntityKeyDD() {
        return "RepAuditTrail_entityKey";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="empUserName">
    private String empUserName;

    public String getEmpUserName() {
        return empUserName;
    }

    public void setEmpUserName(String empUserName) {
        this.empUserName = empUserName;
    }
    
    public String getEmpUserNameDD() {
        return "RepAuditTrail_empUserName";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="userName">
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getUserNameDD() {
        return "RepAuditTrail_userName";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="companyID">
    private String companyID;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }
    
    public String getCompanyIDDD() {
        return "RepAuditTrail_companyID";
    }
    //</editor-fold>
}