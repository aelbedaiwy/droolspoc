
package com.unitedofoq.otms.training.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name= "empunregcoureq")
public class EmployeeUnRegisteredCourseRequest extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public String getRequestDateDD() {
        return "EmployeeUnRegisteredCourseRequest_requestDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approved">
    @Column
    private boolean approved;

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getApprovedDD() {
        return "EmployeeUnRegisteredCourseRequest_approved";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvedByWhom">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee approvedByWhom;

    public void setApprovedByWhom(Employee approvedByWhom) {
        this.approvedByWhom = approvedByWhom;
    }

    public Employee getApprovedByWhom() {
        return approvedByWhom;
    }

    public String getApprovedByWhomDD() {
        return "EmployeeUnRegisteredCourseRequest_approvedByWhom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="approvalDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date approvalDate;

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public String getApprovalDateDD() {
        return "EmployeeUnRegisteredCourseRequest_approvalDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="reason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC reason;

    public void setReason(UDC reason) {
        this.reason = reason;
    }

    public UDC getReason() {
        return reason;
    }

    public String getReasonDD() {
        return "EmployeeUnRegisteredCourseRequest_reason";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "EmployeeUnRegisteredCourseRequest_startDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getEndDateDD() {
        return "EmployeeUnRegisteredCourseRequest_endDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="price">
    @Column
    private BigDecimal price;

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getPriceDD() {
        return "EmployeeUnRegisteredCourseRequest_price";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getCurrencyDD() {
        return "EmployeeUnRegisteredCourseRequest_currency";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public String getNotesDD() {
        return "EmployeeUnRegisteredCourseRequest_notes";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="status">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC status;

    public void setStatus(UDC status) {
        this.status = status;
    }

    public UDC getStatus() {
        return status;
    }

    public String getStatusDD() {
        return "EmployeeUnRegisteredCourseRequest_status";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public void setLocation(UDC location) {
        this.location = location;
    }

    public UDC getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "EmployeeUnRegisteredCourseRequest_location";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseInformation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation courseInformation;

    public void setCourseInformation(ProviderCourseInformation courseInformation) {
        this.courseInformation = courseInformation;
    }

    public ProviderCourseInformation getCourseInformation() {
        return courseInformation;
    }

    public String getCourseInformationDD() {
        return "EmployeeUnRegisteredCourseRequest_courseInformation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeUnRegisteredCourseRequest_employee";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="courseTitle">
    @Translatable(translationField = "courseTitleTranslated")
    private String courseTitle;

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }
    
    public String getCourseTitleDD() {
        return "EmployeeUnRegisteredCourseRequest_courseTitle";
    }
    @Transient
    @Translation(originalField = "courseTitle")
    private String courseTitleTranslated;

    public String getCourseTitleTranslated() {
        return courseTitleTranslated;
    }

    public void setCourseTitleTranslated(String courseTitleTranslated) {
        this.courseTitleTranslated = courseTitleTranslated;
    }
    
    public String getCourseTitleTranslatedDD() {
        return "EmployeeUnRegisteredCourseRequest_courseTitle";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="providerName">
    @Translatable(translationField = "providerNameTranslated")
    private String providerName;

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
    
    public String getProviderNameDD() {
        return "EmployeeUnRegisteredCourseRequest_providerName";
    }
    @Transient
    @Translation(originalField = "providerName")
    private String providerNameTranslated;

    public String getProviderNameTranslated() {
        return providerNameTranslated;
    }

    public void setProviderNameTranslated(String providerNameTranslated) {
        this.providerNameTranslated = providerNameTranslated;
    }
    
    public String getProviderNameTranslatedDD() {
        return "EmployeeUnRegisteredCourseRequest_providerName";
    }
    //</editor-fold>
}