/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.loan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author 3dly
 */
@Entity
@ParentEntity(fields = "employeeLoan")
public class LoanBalanceSheet extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="employeeLoan">
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private EmployeeLoan employeeLoan;

    public EmployeeLoan getEmployeeLoan() {
        return employeeLoan;
    }

    public void setEmployeeLoan(EmployeeLoan employeeLoan) {
        this.employeeLoan = employeeLoan;
    }

    public String getEmployeeLoanDD() {
        return "LoanBalanceSheet_employeeLoan";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="year">
    private Integer startYear;

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public String getStartYearDD() {
        return "LoanBalanceSheet_startYear";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="month">
    private Integer startMonth;

    public Integer getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartMonthDD() {
        return "LoanBalanceSheet_startMonth";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="installementValue">
    private BigDecimal installementValue;

    public BigDecimal getInstallementValue() {
        return installementValue;
    }

    public void setInstallementValue(BigDecimal installementValue) {
        this.installementValue = installementValue;
    }

    public String getInstallementValueDD() {
        return "LoanBalanceSheet_installementValue";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="loanBalance">
    private BigDecimal loanBalance;

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getLoanBalanceDD() {
        return "LoanBalanceSheet_loanBalance";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collpased" desc="status">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC status;

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "LoanBalanceSheet_status";
    }
    //</editor-fold>
}
