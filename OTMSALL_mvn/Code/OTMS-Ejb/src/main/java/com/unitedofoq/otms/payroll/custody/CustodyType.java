
package com.unitedofoq.otms.payroll.custody;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"company"})
public class CustodyType extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "CustodyType_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "CustodyType_description";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "CustodyType_description";
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
     // </editor-fold>

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Company company;

    public Company getCompany() {
        return company;
    }
    
    public String getCompanyDD() {
        return "CustodyType_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
