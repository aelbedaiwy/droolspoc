/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.employee.EDSEmployeeCompetency;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields={"jobGap"})
@ChildEntity(fields="learningIntrs")
public class EmployeeCompetencyGap extends BaseEntity {

    public String getGapCompetencyDD() {
        return "EmployeeCompetencyGap_gapCompetency";
    }

    public EDSEmployeeCompetency getGapCompetency() {
        return gapCompetency;
    }

    public void setGapCompetency(EDSEmployeeCompetency gapCompetency) {
        this.gapCompetency = gapCompetency;
    }

    @Column(length=1000)
    private String behaviorNeeded;

    public String getBehaviorNeeded() {
        return behaviorNeeded;
    }

    public String getBehaviorNeededDD() {
        return "EmployeeCompetencyGap_behaviorNeeded";
    }

    public void setBehaviorNeeded(String behaviorNeeded) {
        this.behaviorNeeded = behaviorNeeded;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSEmployeeCompetency gapCompetency;
   
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private EDSIDPForJobGap jobGap;
    
    @OneToMany(mappedBy = "employeeCompetencyGap")
    private List<EDSIDPForJobGapLrnIntr> learningIntrs;

    public String getJobGapDD() {
        return "EmployeeCompetencyGap_jobGap";
    }

    public EDSIDPForJobGap getJobGap() {
        return jobGap;
    }

    public void setJobGap(EDSIDPForJobGap jobGap) {
        this.jobGap = jobGap;
    }

    public String getLearningIntrsDD() {
        return "EmployeeCompetencyGap_learningIntrs";
    }
    public List<EDSIDPForJobGapLrnIntr> getLearningIntrs() {
        return learningIntrs;
    }

    public void setLearningIntrs(List<EDSIDPForJobGapLrnIntr> learningIntrs) {
        this.learningIntrs = learningIntrs;
    }
}
