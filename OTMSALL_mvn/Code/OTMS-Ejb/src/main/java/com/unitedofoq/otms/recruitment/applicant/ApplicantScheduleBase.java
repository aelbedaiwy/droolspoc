package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class ApplicantScheduleBase extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppliedApplicant applicant;

    public AppliedApplicant getApplicant() {
        return applicant;
    }

    public void setApplicant(AppliedApplicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantScheduleBase_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scheduleDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleDate;

    public Date getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getScheduleDateDD() {
        return "ApplicantScheduleBase_scheduleDate";
    }

    @Transient
    private String scheduleTime;

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getScheduleTimeDD() {
        return "ApplicantScheduleBase_scheduleTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "ApplicantScheduleBase_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column(nullable = true)
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "ApplicantScheduleBase_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="status">
    private String status; //Done - Not Done

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDD() {
        return "ApplicantScheduleBase_status";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="score">
    @Column
    private BigDecimal score;

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public String getScoreDD() {
        return "ApplicantScheduleBase_score";
    }
    @Transient
    private BigDecimal scoreMask;

    public BigDecimal getScoreMask() {
        scoreMask = score;
        return scoreMask;
    }

    public void setScoreMask(BigDecimal scoreMask) {
        updateDecimalValue("score", scoreMask);
    }

    public String getScoreMaskDD() {
        return "ApplicantScheduleBase_scoreMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mcqScore">
    @Column
    private BigDecimal mcqScore;

    public BigDecimal getMcqScore() {
        return mcqScore;
    }

    public void setMcqScore(BigDecimal mcqScore) {
        this.mcqScore = mcqScore;
    }

    public String getMcqScoreDD() {
        return "ApplicantScheduleBase_mcqScore";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="essayScore">
    @Column
    private BigDecimal essayScore;

    public BigDecimal getEssayScore() {
        return essayScore;
    }

    public void setEssayScore(BigDecimal essayScore) {
        this.essayScore = essayScore;
    }

    public String getEssayScoreDD() {
        return "ApplicantScheduleBase_essayScore";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="resultDescription">
    private String resultDescription;

    public String getResultDescription() {
        return resultDescription;
    }

    public String getResultDescriptionDD() {
        return "ApplicantScheduleBase_resultDescription";
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }
    // </editor-fold>
}
