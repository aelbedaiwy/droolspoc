/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import javax.persistence.Entity;

/**
 *
 * @author lap2
 */
@Entity
public class BusinessUDC extends BusinessObjectBaseEntity  {
    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD() {
        return "BusinessUDC_code";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="value">
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public String getValueDD() {
        return "BusinessUDC_value";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="parentCode">
    private String parentCode;

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }
    
    public String getParentCodeDD() {
        return "BusinessUDC_parentCode";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="parentValue">
    private String parentValue;

    public String getParentValue() {
        return parentValue;
    }

    public void setParentValue(String parentValue) {
        this.parentValue = parentValue;
    }
    
    public String getParentValueDD() {
        return "BusinessUDC_parentValue";
    }
    //</editor-fold>
}
