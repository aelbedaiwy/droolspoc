/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.company;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.otms.eds.foundation.job.EDSJobFamily;
import com.unitedofoq.otms.eds.foundation.unit.EDSUnit;
import com.unitedofoq.otms.eds.foundation.unit.EDSUnitLevel;
import com.unitedofoq.otms.eds.foundation.unit.EDSUnitLevelIndex;
import com.unitedofoq.otms.foundation.company.CompanyBase;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author arezk
 */
@Entity
@Table(name="Company")
@DiscriminatorValue(value="MASTER")
@ChildEntity(fields={"units","unitLevels","jobfamilies","unitLevelIndexes"})
public class EDSCompany extends CompanyBase{
          
    // <editor-fold defaultstate="collapsed" desc="units">
    @OneToMany(mappedBy="company")
	private List<EDSUnit> units;

    public List<EDSUnit> getUnits() {
        return units;
    }

    public void setUnits(List<EDSUnit> units) {
        this.units = units;
    }
     public String getUnitsDD() {
        return "Company_units";
    }
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="jobs">
    @OneToMany(mappedBy="company")
    @JoinColumn
    private List<EDSJobFamily> jobfamilies;

    public List<EDSJobFamily> getJobfamilies() {
        return jobfamilies;
    }

    public void setJobfamilies(List<EDSJobFamily> jobfamilies) {
        this.jobfamilies = jobfamilies;
    }

    public String getJobfamiliesDD() {
        return "Company_jobs";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitLevels">
    @OneToMany(mappedBy="company")
	private List<EDSUnitLevel> unitLevels;

    public List<EDSUnitLevel> getUnitLevels() {
        return unitLevels;
    }

    public void setUnitLevels(List<EDSUnitLevel> unitLevels) {
        this.unitLevels = unitLevels;
    }
     public String getUnitLevelsDD() {
        return "Company_unitLevels";
    }
    // </editor-fold>
	@OneToMany(mappedBy="company")
    private List<EDSUnitLevelIndex> unitLevelIndexes;

    public List<EDSUnitLevelIndex> getUnitLevelIndexes() {
        return unitLevelIndexes;
    }

    public String getUnitLevelIndexesDD() {
        return "EDSCompany_unitLevelIndexes";
    }

    public void setUnitLevelIndexes(List<EDSUnitLevelIndex> unitLevelIndexes) {
        this.unitLevelIndexes = unitLevelIndexes;
    }
}
