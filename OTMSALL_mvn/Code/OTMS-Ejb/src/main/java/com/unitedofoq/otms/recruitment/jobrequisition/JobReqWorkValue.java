 
package com.unitedofoq.otms.recruitment.jobrequisition;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccupationElementRatingBase;
import com.unitedofoq.otms.foundation.occupation.WorkValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"jobRequisition"})
public class JobReqWorkValue extends OccupationElementRatingBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="workValue_DBID", nullable=false)
	private WorkValue workValue;
    public String getWorkValueDD(){
        return "JobReqWorkValue_workValue";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobRequisition_DBID", nullable=false)
    private JobRequisition jobRequisition;

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public WorkValue getWorkValue() {
        return workValue;
    }

    public void setWorkValue(WorkValue workValue) {
        this.workValue = workValue;
    }
}