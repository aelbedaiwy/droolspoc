package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.otms.reports.views.RepCompanyLevelBaseTranslation;
import javax.persistence.Entity;

@Entity
public class EmployeeTaxTranslation extends RepCompanyLevelBaseTranslation {
    
    // <editor-fold defaultstate="collapsed" desc="employeeActive">
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fullname">
    private String fullname;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionName">
    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    private String locationDescription;

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    private String costCenterDescription;

    public String getCostCenterDescription() {
        return costCenterDescription;
    }

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="costCenterGroupDescription">
    private String costCenterGroupDescription;

    public String getCostCenterGroupDescription() {
        return costCenterGroupDescription;
    }

    public void setCostCenterGroupDescription(String costCenterGroupDescription) {
        this.costCenterGroupDescription = costCenterGroupDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="legalEntityDescription">
    private String legalEntityDescription;

    public String getLegalEntityDescription() {
        return legalEntityDescription;
    }

    public void setLegalEntityDescription(String legalEntityDescription) {
        this.legalEntityDescription = legalEntityDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeStepDescription">
    private String payGradeStepDescription;

    public String getPayGradeStepDescription() {
        return payGradeStepDescription;
    }

    public void setPayGradeStepDescription(String payGradeStepDescription) {
        this.payGradeStepDescription = payGradeStepDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="payGradeDescription">
    private String payGradeDescription;

    public String getPayGradeDescription() {
        return payGradeDescription;
    }

    public void setPayGradeDescription(String payGradeDescription) {
        this.payGradeDescription = payGradeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="hiringTypeDescription">
    private String hiringTypeDescription;

    public String getHiringTypeDescription() {
        return hiringTypeDescription;
    }

    public void setHiringTypeDescription(String hiringTypeDescription) {
        this.hiringTypeDescription = hiringTypeDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unitName">
    private String unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="jobName">
    private String jobName;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }


    // </editor-fold>    
    
    // <editor-fold defaultstate="collapsed" desc="insuranceGroupDescription">
    private String insuranceGroupDescription;

    public String getInsuranceGroupDescription() {
        return insuranceGroupDescription;
    }

    public void setInsuranceGroupDescription(String insuranceGroupDescription) {
        this.insuranceGroupDescription = insuranceGroupDescription;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="taxGroupDescription">
    private String taxGroupDescription;

    public String getTaxGroupDescription() {
        return taxGroupDescription;
    }

    public void setTaxGroupDescription(String taxGroupDescription) {
        this.taxGroupDescription = taxGroupDescription;
    }

    // </editor-fold>
    
}
