/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"position"})
public class PositionWfException extends BusinessObjectBaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    public String getPositionDD() {
        return "PositionWfException_position";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="wfExceptionType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private UDC wfExceptionType;
    public String getWfExceptionTypeDD() {
            return "PositionWfException_wfExceptionType";
    }
    public UDC getWfExceptionType() {
         return wfExceptionType;
    }
    public void setWfExceptionType(UDC wfExceptionType) {
            this.wfExceptionType = wfExceptionType;
    }
    // </editor-fold>
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position workflowPosition;

    public Position getWorkflowPosition() {
        return workflowPosition;
    }

    public void setWorkflowPosition(Position workflowPosition) {
        this.workflowPosition = workflowPosition;
    }
    public String getWorkflowPositionDD() {
        return "PositionWfException_workflowPosition";
    }

    
}
