/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.employee.idp.EDSConsultant;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author nkhalil
 */
@Entity
@DiscriminatorValue(value = "CON")
@ParentEntity(fields={"employeeProfiler"})
public class EmployeeProfilerAssessorConsultant extends EmployeeProfilerAssessor {
    
    @JoinColumn(name="assossr_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSConsultant assossr;

    public String getAssossrDD() {
        return "EmployeeProfilerAssessorConsultant_assossr";
    }

    public EDSConsultant getAssossr() {
        return assossr;
    }

    public void setAssossr(EDSConsultant assossr) {
        this.assossr = assossr;
    }
}
