package com.unitedofoq.otms.foundation.currency;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;


/**
 * The persistent class for the exchange_rates database table.
 * 
 */
@Entity
@ParentEntity(fields="company")
public class ExchangeRate extends BusinessObjectBaseEntity   {
    // <editor-fold defaultstate="collapsed" desc="isdefault">
	@Column(length=1)
	private String isdefault;

    public String getIsdefault() {
        return isdefault;
    }

    public void setIsdefault(String isdefault) {
        this.isdefault = isdefault;
    }
 public String getIsdefaultDD() {
        return "ExchangeRate_isdefault";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exchangeRate">
    @Column(nullable=false, precision=25, scale=13)
    private BigDecimal exchangeRate;

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getExchangeRateDD() {
        return "ExchangeRate_exchangeRate";
    }
    @Transient
    private BigDecimal exchangeRateMask;

    public BigDecimal getExchangeRateMask() {
        exchangeRateMask = exchangeRate;
        return exchangeRateMask;
    }

    public void setExchangeRateMask(BigDecimal exchangeRateMask) {
        updateDecimalValue("exchangeRate",exchangeRateMask);
    }

    public String getExchangeRateMaskDD() {
        return "ExchangeRate_exchangeRateMask";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exchangeRateDate">
    @Temporal( TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date exchangeRateDate;

    public Date getExchangeRateDate() {
        return exchangeRateDate;
    }

    public void setExchangeRateDate(Date exchangeRateDate) {
        this.exchangeRateDate = exchangeRateDate;
    }

     public String getExchangeRateDateDD() {
        return "ExchangeRate_exchangeRateDate";
    }

 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
	
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "ExchangeRate_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortOrder">
	@Column
	private int sortOrder;

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getSortOrderDD() {
        return "ExchangeRate_sortOrder";
    }

 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "ExchangeRate_currency";
    }
 // </editor-fold>


    }
