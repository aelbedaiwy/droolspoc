/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
public class EmpTreatHistory extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch= FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public String getEmployeeDD() {
        return "EmpTreatHistory_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="treatmentType">
    @ManyToOne(fetch= FetchType.LAZY)
    private MedicalTreatmentTypes treatmentType;

    public MedicalTreatmentTypes getTreatmentType() {
        return treatmentType;
    }

    public void setTreatmentTypes(MedicalTreatmentTypes treatmentType) {
        this.treatmentType = treatmentType;
    }
    
    public String getTreatmentTypeDD() {
        return "EmpTreatHistory_treatmentType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxAmountType">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC maxAmountType;

    public UDC getMaxAmountType() {
        return maxAmountType;
    }

    public void setMaxAmountType(UDC maxAmountType) {
        this.maxAmountType = maxAmountType;
    }
    
    public String getMaxAmountTypeDD() {
        return "EmpTreatHistory_maxAmountType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="treatmentYear">
    private String treatmentYear;

    public String getTreatmentYear() {
        return treatmentYear;
    }

    public void setTreatmentYear(String treatmentYear) {
        this.treatmentYear = treatmentYear;
    }
    
    public String getTreatmentYearDD() {
        return "EmpTreatHistory_treatmentYear";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxValue">
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }
    public String getMaximumValueDD() {
        return "EmpTreatHistory_maximumValue";
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }
    @Transient
    private BigDecimal maximumValueMask;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue", maximumValueMask);
    }    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="totalTakenVal">
    private BigDecimal totalTakenVal;

    public BigDecimal getTotalTakenVal() {
        return totalTakenVal;
    }

    public void setTotalTakenVal(BigDecimal totalTakenVal) {
        this.totalTakenVal = totalTakenVal;
    }
    
    public String getTotalTakenValDD() {
        return "EmpTreatHistory_totalTakenVal";
    }
    
    @Transient
    private BigDecimal totalTakenValMask;

    public BigDecimal getTotalTakenValMask() {
        totalTakenValMask = totalTakenVal;
        return totalTakenValMask;
    }
    
    public String getTotalTakenValMaskDD() {
        return "EmpTreatHistory_totalTakenValMask";
    }

    public void setTotalTakenValMask(BigDecimal totalTakenValMask) {
        updateDecimalValue("totalTakenVal", totalTakenValMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxCoverage">
    private BigDecimal maxCoverage;

    public BigDecimal getMaxCoverage() {
        return maxCoverage;
    }

    public void setMaxCoverage(BigDecimal maxCoverage) {
        this.maxCoverage = maxCoverage;
    }
    
    public String getMaxCoverageDD() {
        return "EmpTreatHistory_maxCoverage";
    }
    @Transient
    private BigDecimal maxCoverageMask;

    public BigDecimal getMaxCoverageMask() {
        maxCoverageMask = maxCoverage;
        return maxCoverageMask;
    }
    
    public String getMaxValueCoverageDD() {
        return "EmpTreatHistory_maxCoverageMask";
    }

    public void setMaxValueCoverage(BigDecimal maxCoverageMask) {
        updateDecimalValue("maxCoverage", maxCoverageMask);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="depMaxValue">
    private BigDecimal depMaxValue;

    public BigDecimal getDepMaxValue() {
        return depMaxValue;
    }

    public void setDepMaxValue(BigDecimal depMaxValue) {
        this.depMaxValue = depMaxValue;
    }
    
    public String getDepMaxValueDD() {
        return "EmpTreatHistory_depMaxValue";
    }
    @Transient
    private BigDecimal depMaxValueMask;

    public BigDecimal getDepMaxValueMask() {
        depMaxValueMask = depMaxValue;
        return depMaxValueMask;
    }
    
    public String getDepMaxValueMaskDD() {
        return "EmpTreatHistory_depMaxValueMask";
    }

    public void setDepMaxValueMask(BigDecimal depMaxValueMask) {
        updateDecimalValue("depMaxValue", depMaxValueMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="depTotalTakenVal">
    private BigDecimal depTotalTakenVal;

    public BigDecimal getDepTotalTakenVal() {
        return depTotalTakenVal;
    }

    public void setDepTotalTakenVal(BigDecimal depTotalTakenVal) {
        this.depTotalTakenVal = depTotalTakenVal;
    }
    
    public String getDepTotalTakenValDD() {
        return "EmpTreatHistory_depTotalTakenVal";
    }
    
    @Transient
    private BigDecimal depTotalTakenValMask;

    public BigDecimal getDepTotalTakenValMask() {
        depTotalTakenValMask = depTotalTakenVal;
        return depTotalTakenValMask;
    }
    
    public String getDepTotalTakenValMaskDD() {
        return "EmpTreatHistory_totalTakenValMask";
    }

    public void setDepTotalTakenValMask(BigDecimal depTotalTakenValMask) {
        updateDecimalValue("depTotalTakenVal", depTotalTakenValMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxCoverage">
    private BigDecimal depMaxCoverage;

    public BigDecimal getDepMaxCoverage() {
        return depMaxCoverage;
    }

    public void setDepMaxCoverage(BigDecimal depMaxCoverage) {
        this.depMaxCoverage = depMaxCoverage;
    }
    
    public String getDepMaxCoverageDD() {
        return "EmpTreatHistory_depMaxCoverage";
    }
    @Transient
    private BigDecimal depMaxCoverageMask;

    public BigDecimal getDepMaxCoverageMask() {
        depMaxCoverageMask = depMaxCoverage;
        return depMaxCoverageMask;
    }
    
    public String getDepMaxCoverageMaskDD() {
        return "EmpTreatHistory_depMaxCoverageMask";
    }

    public void setDepMaxCoverageMask(BigDecimal depMaxCoverageMask) {
        updateDecimalValue("depMaxCoverage", depMaxCoverageMask);
    }
    // </editor-fold>
}