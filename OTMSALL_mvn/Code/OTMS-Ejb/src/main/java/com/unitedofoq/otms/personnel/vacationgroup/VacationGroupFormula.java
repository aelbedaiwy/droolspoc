package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.vacation.VacationFormulaBase;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "VacationFormula")
@ParentEntity(fields = {"vacation"})
public class VacationGroupFormula extends VacationFormulaBase {
    // <editor-fold defaultstate="collapsed" desc="vacation">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private VacationGroup vacation;

    public VacationGroup getVacation() {
        return vacation;
    }

    public void setVacation(VacationGroup vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "VacationFormula_vacation";
    }
    // </editor-fold>
}
