package com.unitedofoq.otms.personnel.dayoff;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
//@ParentEntity(fields={"dayOff"})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DAYOFFTYPE")
public class DayOffPenalty extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="dtype">
    @Transient
    private String dtype;

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

   public String getDtypeDD() {
        return "DayOffPenalty_dtype";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOff">
    @Transient
   
    private DayOff dayOff;

    public DayOff getDayOff() {
        return dayOff;
    }

    public void setDayOff(DayOff dayOff) {
        this.dayOff = dayOff;
    }
    public String getDayOffDD() {
        return "DayOffPenalty_dayOff";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Penalty penalty;

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }
    public String getPenaltyDD() {
        return "DayOffPenalty_penalty";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column
    private Integer sortIndex;

    public Integer getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "DayOffPenalty_sortIndex";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="occuranceOrValue">
    @Column(length=1)
    //O - V
    private String occuranceOrValue;

    public String getOccuranceOrValue() {
        return occuranceOrValue;
    }

    public void setOccuranceOrValue(String occuranceOrValue) {
        this.occuranceOrValue = occuranceOrValue;
    }
    public String getOccuranceOrValueDD() {
        return "DayOffPenalty_occuranceOrValue";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueFrom">
    @Column(precision=18, scale=3)
    private BigDecimal valueFrom;

    public BigDecimal getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(BigDecimal valueFrom) {
        this.valueFrom = valueFrom;
    }
    public String getValueFromDD() {
        return "DayOffPenalty_valueFrom";
    }
    @Transient
    private BigDecimal valueFromMask;

    public BigDecimal getValueFromMask() {
        valueFromMask = valueFrom ;
        return valueFromMask;
    }

    public void setValueFromMask(BigDecimal valueFromMask) {
        updateDecimalValue("valueFrom",valueFromMask);
    }
    public String getValueFromMaskDD() {
        return "DayOffPenalty_valueFromMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueTo">
    @Column(precision=18, scale=3)
    private BigDecimal valueTo;

    public BigDecimal getValueTo() {
        return valueTo;
    }

    public void setValueTo(BigDecimal valueTo) {
        this.valueTo = valueTo;
    }

    public String getValueToDD() {
        return "DayOffPenalty_valueTo";
    }
    @Transient
    private BigDecimal valueToMask;

    public BigDecimal getValueToMask() {
        valueToMask = valueTo ;
        return valueToMask;
    }

    public void setValueToMask(BigDecimal valueToMask) {
        updateDecimalValue("valueTo",valueToMask);
    }

    public String getValueToMaskDD() {
        return "DayOffPenalty_valueToMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factor">
    @Column(precision=18, scale=3)
    private BigDecimal factor;

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public String getFactorDD() {
        return "DayOffPenalty_factor";
    }
    @Transient
    private BigDecimal factorMask;

    public BigDecimal getFactorMask() {
        factorMask = factor;
        return factorMask;
    }

    public void setFactorMask(BigDecimal factorMask) {
        updateDecimalValue("factor",factorMask);
    }

    public String getFactorMaskDD() {
        return "DayOffPenalty_factorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectedMonth">
    @Column(length=1)
    //Current C
    //Next N
    //Both B

    private String affectedMonth;

    public String getAffectedMonth() {
        return affectedMonth;
    }

    public void setAffectedMonth(String affectedMonth) {
        this.affectedMonth = affectedMonth;
    }
     public String getAffectedMonthDD() {
        return "DayOffPenalty_affectedMonth";
    }
    // </editor-fold>


}
