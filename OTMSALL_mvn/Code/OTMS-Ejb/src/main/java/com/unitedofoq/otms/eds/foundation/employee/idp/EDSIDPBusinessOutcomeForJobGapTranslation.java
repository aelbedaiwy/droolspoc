
package com.unitedofoq.otms.eds.foundation.employee.idp;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
@Table(name= "edsidpbusoutforjgtranslation")
public class EDSIDPBusinessOutcomeForJobGapTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

}