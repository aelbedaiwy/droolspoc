/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.reports.views;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lahmed
 */
@Entity
@ReadOnly
@Table(name="repemployeevisa")
public class RepEmployeeVisa extends RepEmployeeBase {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeVisa_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeVisa_genderDescription";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepEmployeeVisa_employeeActive";
    }
    
    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }
    
    public String getEmployeeActiveTranslatedDD() {
        return "RepEmployeeVisa_employeeActive";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="lastRenewalDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date lastRenewalDate;

    public Date getLastRenewalDate() {
        return lastRenewalDate;
    }

    public void setLastRenewalDate(Date lastRenewalDate) {
        this.lastRenewalDate = lastRenewalDate;
    }

    public String getLastRenewalDateDD() {
        return "RepEmployeeVisa_lastRenewalDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="changeDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date changeDate;

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public String getChangeDateDD() {
        return "RepEmployeeVisa_changeDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="visaNumber">
    @Column
    private String visaNumber;

    public String getVisaNumber() {
        return visaNumber;
    }

    public void setVisaNumber(String visaNumber) {
        this.visaNumber = visaNumber;
    }

    public String getVisaNumberDD() {
        return "RepEmployeeVisa_visaNumber";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="issueDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date issueDate;

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueDateDD() {
        return "RepEmployeeVisa_issueDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="issueFrom">
    @Column
    private String issueFrom;

    public String getIssueFrom() {
        return issueFrom;
    }

    public void setIssueFrom(String issueFrom) {
        this.issueFrom = issueFrom;
    }

    public String getIssueFromDD() {
        return "RepEmployeeVisa_issueFrom";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="entryDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date entryDate;

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryDateDD() {
        return "RepEmployeeVisa_entryDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="used">
    @Column
    private String used;

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getUsedDD() {
        return "RepEmployeeVisa_used";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="value">
    @Column(precision=25, scale=13)
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getValueDD() {
        return "RepEmployeeVisa_value";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="currency">
    @Column
    private String currency;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "RepEmployeeVisa_currency";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "RepEmployeeVisa_notes";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="expiryDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expiryDate;

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getExpiryDateDd() {
        return "RepEmployeeVisa_expiryDate";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="visaJobFamily">
    @Column
    @Translatable(translationField = "visaJobFamilyTranslated")
    private String visaJobFamily;

    public String getVisaJobFamily() {
        return visaJobFamily;
    }

    public void setVisaJobFamily(String visaJobFamily) {
        this.visaJobFamily = visaJobFamily;
    }

    public String getVisaJobFamilyDD() {
        return "RepEmployeeVisa_visaJobFamily";
    }
    
    @Transient
    @Translation(originalField = "visaJobFamily")
    private String visaJobFamilyTranslated;

    public String getVisaJobFamilyTranslated() {
        return visaJobFamilyTranslated;
    }

    public void setVisaJobFamilyTranslated(String visaJobFamilyTranslated) {
        this.visaJobFamilyTranslated = visaJobFamilyTranslated;
    }
    
    public String getVisaJobFamilyTranslatedDD() {
        return "RepEmployeeVisa_visaJobFamily";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="validPeriod">
    @Column
    private String validPeriod;

    public String getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(String validPeriod) {
        this.validPeriod = validPeriod;
    }

    public String getValidPeriodDD() {
        return "RepEmployeeVisa_validPeriod";
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="visaNationality">
    @Column
    @Translatable(translationField = "visaNationalityTranslated")
    private String visaNationality;

    public String getVisaNationality() {
        return visaNationality;
    }

    public void setVisaNationality(String visaNationality) {
        this.visaNationality = visaNationality;
    }

    public String getVisaNationalityDD() {
        return "RepEmployeeVisa_visaNationality";
    }
    
    @Transient
    @Translation(originalField = "visaNationality")
    private String visaNationalityTranslated;

    public String getVisaNationalityTranslated() {
        return visaNationalityTranslated;
    }

    public void setVisaNationalityTranslated(String visaNationalityTranslated) {
        this.visaNationalityTranslated = visaNationalityTranslated;
    }
    
    public String getVisaNationalityTranslatedDD() {
        return "RepEmployeeVisa_visaNationality";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="requestNotes">
    @Column
    private String requestNotes;

    public String getRequestNotes() {
        return requestNotes;
    }

    public void setRequestNotes(String requestNotes) {
        this.requestNotes = requestNotes;
    }

    public String getRequestNotesDD() {
        return "RepEmployeeVisa_requestNotes";
    }
    //</editor-fold>
}
