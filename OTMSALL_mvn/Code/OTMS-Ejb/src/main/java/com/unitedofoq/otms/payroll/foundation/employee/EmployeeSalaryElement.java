package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import javax.persistence.*;


@Entity
@ParentEntity(fields={"employee"})
public class EmployeeSalaryElement extends EmployeeSalaryElementBase
{
    // <editor-fold defaultstate="collapsed" desc="employee">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Employee employee;
    public Employee getEmployee() {
        return employee;
    }
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeSalaryElement_employee";
    }

    @Transient
    String salaryElementType;

    public String getSalaryElementType() {
        return salaryElementType;
    }

    public String getSalaryElementTypeDD() {
        return "EmployeeSalaryElement_salaryElementType";
    }

    public void setSalaryElementType(String salaryElementType) {
        this.salaryElementType = salaryElementType;
    }

    @Override
    protected void PostLoad() {
        super.PostLoad();
        if(getSalaryElement()!=null){
            if(getSalaryElement() instanceof Deduction){
                salaryElementType = Deduction.class.getSimpleName();
            }else if(getSalaryElement() instanceof Income){
                salaryElementType = Income.class.getSimpleName();
            }
        }
    }




    // </editor-fold>
}