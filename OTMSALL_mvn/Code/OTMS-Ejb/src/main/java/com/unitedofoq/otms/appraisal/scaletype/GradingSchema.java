package com.unitedofoq.otms.appraisal.scaletype;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class GradingSchema extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "GradingSchema_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    @Column(nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTranslatedDD() {
        return "GradingSchema_nameTranslated";
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)

    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "GradingSchema_company";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="outOfScore">
    private BigDecimal outOfScore;

    public BigDecimal getOutOfScore() {
        return outOfScore;
    }

    public String getOutOfScoreDD() {
        return "GradingSchema_outOfScore";
    }

    public void setOutOfScore(BigDecimal outOfScore) {
        this.outOfScore = outOfScore;
    }
    @Transient
    private BigDecimal outOfScoreMask;

    public BigDecimal getOutOfScoreMask() {
        outOfScoreMask = outOfScore;
        return outOfScoreMask;
    }

    public String getOutOfScoreMaskDD() {
        return "GradingSchema_outOfScoreMask";
    }

    public void setOutOfScoreMask(BigDecimal outOfScoreMask) {
        updateDecimalValue("outOfScore", outOfScoreMask);
    }
    // </editor-fold >
}
