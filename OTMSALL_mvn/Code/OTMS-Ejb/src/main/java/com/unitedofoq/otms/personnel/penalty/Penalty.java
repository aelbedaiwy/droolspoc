/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.IncentivesPenaltySetup;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.LegalEntityGroup;
import com.unitedofoq.otms.payroll.formula.FormulaDistinctName;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields = "company")
@ChildEntity(fields = {"penaltyRules", "affectingDeductions", "affectingIncomes", "penaltyAffectingSalaryElements", "incentives"})
public class Penalty extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Penalty_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "Penalty_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="descriptionTranslated">
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitMeasure">
    //Hour H - Day D - Value V
    @Column(length = 1)
    private String unitMeasure;//(name = "unit_measure")

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getUnitMeasureDD() {
        return "Penalty_unitMeasure";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="defaultValue">
    @Column(precision = 18, scale = 3)
    private BigDecimal defaultValue;//(name = "penalty_default_value")

    public BigDecimal getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(BigDecimal defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValueDD() {
        return "Penalty_defaultValue";
    }
    @Transient
    private BigDecimal defaultValueMask;

    public BigDecimal getDefaultValueMask() {
        defaultValueMask = defaultValue;
        return defaultValueMask;
    }

    public void setDefaultValueMask(BigDecimal defaultValueMask) {
        updateDecimalValue("defaultValue", defaultValueMask);
    }

    public String getDefaultValueMaskDD() {
        return "Penalty_defaultValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="pointsFactor">
    @Column(precision = 18, scale = 3)
    private BigDecimal pointsFactor;//(name = "points_factor")

    public BigDecimal getPointsFactor() {
        return pointsFactor;
    }

    public void setPointsFactor(BigDecimal pointsFactor) {
        this.pointsFactor = pointsFactor;
    }

    public String getPointsFactorDD() {
        return "Penalty_pointsFactor";
    }
    @Transient
    private BigDecimal pointsFactorMask;

    public BigDecimal getPointsFactorMask() {
        pointsFactorMask = pointsFactor;
        return pointsFactorMask;
    }

    public void setPointsFactorMask(BigDecimal pointsFactorMask) {
        updateDecimalValue("pointsFactor", pointsFactorMask);
    }

    public String getPointsFactorMaskDD() {
        return "Penalty_pointsFactorMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshAfterPeriod">
    @Column
    private BigDecimal refreshAfterPeriod;

    public BigDecimal getRefreshAfterPeriod() {
        return refreshAfterPeriod;
    }

    public void setRefreshAfterPeriod(BigDecimal refreshAfterPeriod) {
        this.refreshAfterPeriod = refreshAfterPeriod;
    }

    public String getRefreshAfterPeriodDD() {
        return "Penalty_refreshAfterPeriod";
    }
    @Transient
    private BigDecimal refreshAfterPeriodMask;

    public BigDecimal getRefreshAfterPeriodMask() {
        refreshAfterPeriodMask = refreshAfterPeriod;
        return refreshAfterPeriodMask;
    }

    public void setRefreshAfterPeriodMask(BigDecimal refreshAfterPeriodMask) {
        updateDecimalValue("refreshAfterPeriod", refreshAfterPeriodMask);
    }

    public String getRefreshAfterPeriodMaskDD() {
        return "Penalty_refreshAfterPeriodMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshUnitMeasure">
    @Column(length = 1)
    //Month M - Year Y
    private String refreshUnitMeasure;//(name = "refresh_unit_measure")

    public String getRefreshUnitMeasure() {
        return refreshUnitMeasure;
    }

    public void setRefreshUnitMeasure(String refreshUnitMeasure) {
        this.refreshUnitMeasure = refreshUnitMeasure;
    }

    public String getRefreshUnitMeasureDD() {
        return "Penalty_refreshUnitMeasure";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshStartFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date refreshStartFrom;//(name = "refresh_start_from")

    public Date getRefreshStartFrom() {
        return refreshStartFrom;
    }

    public void setRefreshStartFrom(Date refreshStartFrom) {
        this.refreshStartFrom = refreshStartFrom;
    }

    public String getRefreshStartFromDD() {
        return "Penalty_refreshStartFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="occuranceOrValue">
    @Column(length = 1)
    //Occurance O - Accumulated V - Value L
    private String occuranceOrValue;//(name = "occurance_or_value")

    public String getOccuranceOrValue() {
        return occuranceOrValue;
    }

    public void setOccuranceOrValue(String occuranceOrValue) {
        this.occuranceOrValue = occuranceOrValue;
    }

    public String getOccuranceOrValueDD() {
        return "Penalty_occuranceOrValue";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="formula">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private FormulaDistinctName formula;//(name = "formula_name")

    public FormulaDistinctName getFormula() {
        return formula;
    }

    public void setFormula(FormulaDistinctName formula) {
        this.formula = formula;
    }

    public String getFormulaDD() {
        return "Penalty_formula";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshMethod">
    @Column
    //Hiring Date H - First Penalty Date P
    private String refreshMethod;//(name = "refresh_method")

    public String getRefreshMethod() {
        return refreshMethod;
    }

    public void setRefreshMethod(String refreshMethod) {
        this.refreshMethod = refreshMethod;
    }

    public String getRefreshMethodDD() {
        return "Penalty_refreshMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="accumulate">
    @Column(length = 1)
    //Y - N
    private String accumulate;//(name = "accumulate_penalty")

    public String getAccumulate() {
        return accumulate;
    }

    public void setAccumulate(String accumulate) {
        this.accumulate = accumulate;
    }

    public String getAccumulateDD() {
        return "Penalty_accumulate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyRules">
    @JoinColumn
    @OneToMany(mappedBy = "penalty")
    private List<PenaltyRule> penaltyRules;

    public List<PenaltyRule> getPenaltyRules() {
        return penaltyRules;
    }

    public void setPenaltyRules(List<PenaltyRule> penaltyRules) {
        this.penaltyRules = penaltyRules;
    }

    public String getPenaltyRulesDD() {
        return "Penalty_penaltyRules";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectingDeductions">
    @JoinColumn
    @OneToMany(mappedBy = "penalty")
    private List<PenaltyAffectingDeduction> affectingDeductions;

    public List<PenaltyAffectingDeduction> getAffectingDeductions() {
        return affectingDeductions;
    }

    public void setAffectingDeductions(List<PenaltyAffectingDeduction> affectingDeductions) {
        this.affectingDeductions = affectingDeductions;
    }

    public String getAffectingDeductionsDD() {
        return "Penalty_affectingDeductions";
    }
    // </editor-fold>
    //  <editor-fold defaultstate="collapsed" desc="affectingIncomes">
    @JoinColumn
    @OneToMany(mappedBy = "penalty")
    private List<PenaltyAffectingIncome> affectingIncomes;

    public List<PenaltyAffectingIncome> getAffectingIncomes() {
        return affectingIncomes;
    }

    public void setAffectingIncomes(List<PenaltyAffectingIncome> affectingIncomes) {
        this.affectingIncomes = affectingIncomes;
    }

    public String getAffectingIncomesDD() {
        return "Penalty_affectingIncomes";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyIncentives">
    @OneToMany(mappedBy = "penalty")
    private List<IncentivesPenaltySetup> incentives;

    public List<IncentivesPenaltySetup> getIncentives() {
        return incentives;
    }

    public void setIncentives(List<IncentivesPenaltySetup> incentives) {
        this.incentives = incentives;
    }

    public String getIncentivesDD() {
        return "Penalty_incentives";
    }
 // </editor-fold>
    //  <editor-fold defaultstate="collapsed" desc="company">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Penalty_company";
    }
// </editor-fold>
    //  <editor-fold defaultstate="collapsed" desc="penaltyAffectingSalaryElements">
    @OneToMany(mappedBy = "penalty")
    private List<PenaltyAffectingSalaryElement> penaltyAffectingSalaryElements;

    public List<PenaltyAffectingSalaryElement> getPenaltyAffectingSalaryElements() {
        return penaltyAffectingSalaryElements;
    }

    public void setPenaltyAffectingSalaryElements(List<PenaltyAffectingSalaryElement> penaltyAffectingSalaryElements) {
        this.penaltyAffectingSalaryElements = penaltyAffectingSalaryElements;
    }

    public String getPenaltyAffectingSalaryElementsDD() {
        return "Penalty_penaltyAffectingSalaryElements";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshOnYearBegin">
    @Column(length = 1)
    private String refreshOnYearBegin;//(name = "refreshOnYearBegin")

    public String getRefreshOnYearBegin() {
        return refreshOnYearBegin;
    }

    public void setRefreshOnYearBegin(String refreshOnYearBegin) {
        this.refreshOnYearBegin = refreshOnYearBegin;
    }

    public String getRefreshOnYearBeginDD() {
        return "Penalty_refreshOnYearBegin";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="internalCode">
    @Column(unique = true)
    private String internalCode;

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getInternalCodeDD() {
        return "Penalty_internalCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntity legalEntity;

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getLegalEntityDD() {
        return "Penalty_legalEntity";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="legalEntityGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private LegalEntityGroup legalEntityGroup;

    public LegalEntityGroup getLegalEntityGroup() {
        return legalEntityGroup;
    }

    public void setLegalEntityGroup(LegalEntityGroup legalEntityGroup) {
        this.legalEntityGroup = legalEntityGroup;
    }

    public String getLegalEntityGroupDD() {
        return "Penalty_legalEntityGroup";
    }
    //</editor-fold>
}
