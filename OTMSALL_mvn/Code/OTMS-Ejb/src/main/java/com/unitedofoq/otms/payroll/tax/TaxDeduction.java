package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;

import com.unitedofoq.otms.payroll.salaryelement.Deduction;

import java.math.BigDecimal;


/**
 * The persistent class for the tax_deduction database table.
 * 
 */
@Entity
@ParentEntity(fields={"tax"})
public class TaxDeduction extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="tax">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Tax tax;
    public Tax getTax() {
        return tax;
    }
    public void setTax(Tax tax) {
        this.tax = tax;
    }
    public String getTaxDD() {
        return "TaxDeduction_tax";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deductionPercentage">
    @Column(precision=10, scale=3,nullable=false)
    private BigDecimal deductionPercentage;
    public BigDecimal getDeductionPercentage() {
        return deductionPercentage;
    }
    public void setDeductionPercentage(BigDecimal deductionPercentage) {
        this.deductionPercentage = deductionPercentage;
    }
    public String getDeductionPercentageDD() {
        return "TaxDeduction_deductionPercentage";
    }
    @Transient
    private BigDecimal deductionPercentageMask;
    public BigDecimal getDeductionPercentageMask() {
        deductionPercentageMask = deductionPercentage ;
        return deductionPercentageMask;
    }
    public void setDeductionPercentageMask(BigDecimal deductionPercentageMask) {
        updateDecimalValue("deductionPercentage",deductionPercentageMask);
    }
    public String getDeductionPercentageMaskDD() {
        return "TaxDeduction_deductionPercentageMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxSalaryPercent">
    @Column(precision=10, scale=3, nullable=false)
    private BigDecimal maxSalaryPercent;
    public BigDecimal getMaxSalaryPercent() {
        return maxSalaryPercent;
    }
    public void setMaxSalaryPercent(BigDecimal maxSalaryPercent) {
        this.maxSalaryPercent = maxSalaryPercent;
    }
    public String getMaxSalaryPercentDD() {
        return "TaxDeduction_maxSalaryPercent";
    }
    @Transient
    private BigDecimal maxSalaryPercentMask;
    public BigDecimal getMaxSalaryPercentMask() {
        maxSalaryPercentMask = maxSalaryPercent ;
        return maxSalaryPercentMask;
    }
    public void setMaxSalaryPercentMask(BigDecimal maxSalaryPercentMask) {
        updateDecimalValue("maxSalaryPercent",maxSalaryPercentMask);
    }
    public String getMaxSalaryPercentMaskDD() {
        return "TaxDeduction_maxSalaryPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxDeduction">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal maxDeduction;
    public BigDecimal getMaxDeduction() {
        return maxDeduction;
    }
    public void setMaxDeduction(BigDecimal maximumDeduction) {
        this.maxDeduction = maximumDeduction;
    }
    public String getMaxDeductionDD() {
        return "TaxDeduction_maxDeduction";
    }
    @Transient
    private BigDecimal maxDeductionMask;
    public BigDecimal getMaxDeductionMask() {
        maxDeductionMask = maxDeduction ;
        return maxDeductionMask;
    }
    public void setMaxDeductionMask(BigDecimal maxDeductionMask) {
        updateDecimalValue("maximumDeduction",maxDeductionMask);
    }
    public String getMaxDeductionMaskDD() {
        return "TaxDeduction_maxDeductionMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deduction">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deduction;
    public Deduction getDeduction() {
        return deduction;
    }
    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
    public String getDeductionDD() {
        return "TaxDeduction_deduction";
    }
    // </editor-fold>
}