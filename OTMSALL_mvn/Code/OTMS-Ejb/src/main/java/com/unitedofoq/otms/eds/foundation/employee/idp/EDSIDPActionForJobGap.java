package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.competency.EDSCompetencyLevelBehavior;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields="jobGapLrnIntr")
public class EDSIDPActionForJobGap extends EDSIDPAction{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSIDPForJobGapLrnIntr jobGapLrnIntr;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetencyLevelBehavior indicator;

    public String getJobGapLrnIntrDD() {
        return "EDSIDPActionForJobGap_jobGapLrnIntr";
    }

    public EDSIDPForJobGapLrnIntr getJobGapLrnIntr() {
        return jobGapLrnIntr;
    }

    public void setJobGapLrnIntr(EDSIDPForJobGapLrnIntr jobGapLrnIntr) {
        this.jobGapLrnIntr = jobGapLrnIntr;
    }
   

    public EDSCompetencyLevelBehavior getIndicator() {
        return indicator;
    }

    public void setIndicator(EDSCompetencyLevelBehavior indicator) {
        this.indicator = indicator;
    }

    
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EDSIDPAccountability accountability;

    public String getAccountabilityDD() {
        return "EDSIDPActionForJobGap_accountability";
    }

    public EDSIDPAccountability getAccountability() {
        return accountability;
    }

    public void setAccountability(EDSIDPAccountability accountability) {
        this.accountability = accountability;
    }

   

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSIDPBusinessOutcomeForJobGapAAC byWho;

    public String getByWhoDD() {
        return "EDSIDPActionForJobGap_byWho";
    }

    public EDSIDPBusinessOutcomeForJobGapAAC getByWho() {
        return byWho;
    }

    public void setByWho(EDSIDPBusinessOutcomeForJobGapAAC byWho) {
        this.byWho = byWho;
    }

    
}
