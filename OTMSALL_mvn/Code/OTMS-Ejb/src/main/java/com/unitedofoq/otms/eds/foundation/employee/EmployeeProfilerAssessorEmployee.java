/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author nkhalil
 */
@Entity
@DiscriminatorValue(value = "EMP")
@ParentEntity(fields={"employeeProfiler"})
public class EmployeeProfilerAssessorEmployee extends EmployeeProfilerAssessor {

    @JoinColumn(name="assess_DBID", nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee assess;

    public String getAssessDD() {
        return "EmployeeProfilerAssessorEmployee_assess";
    }

    public Employee getAssess() {
        return assess;
    }

    public void setAssess(Employee assess) {
        this.assess = assess;
    }
}
