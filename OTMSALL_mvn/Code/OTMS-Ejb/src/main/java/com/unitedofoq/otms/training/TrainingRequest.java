/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.training.activity.Activity;
import com.unitedofoq.otms.training.activity.ProviderActivity;
import com.unitedofoq.otms.training.scheduleevent.ScheduleEvent;
import com.unitedofoq.otms.training.scheduleevent.ScheduleEventAttendanceRegulation;
import com.unitedofoq.otms.training.scheduleevent.ScheduleEventCancellationRegulation;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ChildEntity(fields={"trainingAttendances","checkLists"})
public class TrainingRequest extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="trainingAttendances">
    @OneToMany(mappedBy="trainingRequest")
    private List<TrainingAttendance> trainingAttendances;

    public List<TrainingAttendance> getTrainingAttendances() {
        return trainingAttendances;
    }

    public void setTrainingAttendances(List<TrainingAttendance> trainingAttendances) {
        this.trainingAttendances = trainingAttendances;
    }

    public String getTrainingAttendancesDD() {
        return "TrainingRequest_attendance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="checkLists">
    @OneToMany(mappedBy = "trainingRequest")
    private List<TrainingRequestCheckList> checkLists;

    public List<TrainingRequestCheckList> getCheckLists() {
        return checkLists;
    }

    public void setCheckLists(List<TrainingRequestCheckList> checkLists) {
        this.checkLists = checkLists;
    }

    public String getCheckListsDD() {
        return "TrainingRequest_checkLists";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "TrainingRequest_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getActivityDD() {
        return "TrainingRequest_activity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="providerActivity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderActivity providerActivity;

    public ProviderActivity getProviderActivity() {
        return providerActivity;
    }

    public void setProviderActivity(ProviderActivity providerActivity) {
        this.providerActivity = providerActivity;
    }

    public String getProviderActivityDD() {
        return "TrainingRequest_providerActivity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scheduleEvent">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEvent scheduleEvent;

    public ScheduleEvent getScheduleEvent() {
        return scheduleEvent;
    }

    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
        this.scheduleEvent = scheduleEvent;
    }

    public String getScheduleEventDD() {
        return "TrainingRequest_scheduleEvent";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestId">
    @Column(nullable=false)
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestIdDD() {
        return "TrainingRequest_requestId";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }
    
    public String getRequestDateDD() {
        return "TrainingRequest_requestDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requester">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
	private Employee requester;

    public Employee getRequester() {
        return requester;
    }

    public void setRequester(Employee requester) {
        this.requester = requester;
    }

    public String getRequesterDD() {
        return "TrainingRequest_requester";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestNotes">
    @Column
    private String requestNotes;

    public String getRequestNotes() {
        return requestNotes;
    }

    public void setRequestNotes(String requestNotes) {
        this.requestNotes = requestNotes;
    }

    public String getRequestNotesDD() {
        return "TrainingRequest_requestNotes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestAttachementPath">
    @Column
    private String requestAttachementPath;

    public String getRequestAttachementPath() {
        return requestAttachementPath;
    }

    public void setRequestAttachementPath(String requestAttachementPath) {
        this.requestAttachementPath = requestAttachementPath;
    }

    public String getRequestAttachementPathDD() {
        return "TrainingRequest_requestAttachementPath";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requestSource">
    //Appraisal OR Career Or ....
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private UDC requestSource;

    public UDC getRequestSource() {
        return requestSource;
    }

    public void setRequestSource(UDC requestSource) {
        this.requestSource = requestSource;
    }

    public String getRequestSourceDD() {
        return "TrainingRequest_requestSource";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approved">
    @Column
    private boolean approved;

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getApprovedDD() {
        return "TrainingRequest_approved";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee approvedBy;

    public Employee getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Employee approvedBy) {
        this.approvedBy = approvedBy;
    }
    
    public String getApprovedByDD() {
        return "TrainingRequest_approvedBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvalDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvalDate;

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalDateDD() {
        return "TrainingRequest_approvalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="approvalNotes">
    @Column
    private String approvalNotes;

    public String getApprovalNotes() {
        return approvalNotes;
    }

    public void setApprovalNotes(String approvalNotes) {
        this.approvalNotes = approvalNotes;
    }

    public String getApprovalNotesDD() {
        return "TrainingRequest_approvalNotes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    @Column
    private boolean cancelled;

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getCancelledDD() {
        return "TrainingRequest_cancelled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainingCancelReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC trainingCancelReason;

    public UDC getTrainingCancelReason() {
        return trainingCancelReason;
    }

    public void setTrainingCancelReason(UDC trainingCancelReason) {
        this.trainingCancelReason = trainingCancelReason;
    }

    public String getTrainingCancelReasonDD() {
        return "TrainingRequest_trainingCancelReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee cancelledBy;

    public Employee getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Employee cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancelledByDD() {
        return "TrainingRequest_cancelledBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelNotes">
    @Column
    private String cancelNotes;

    public String getCancelNotes() {
        return cancelNotes;
    }

    public void setCancelNotes(String cancelNotes) {
        this.cancelNotes = cancelNotes;
    }

    public String getCancelNotesDD() {
        return "TrainingRequest_cancelNotes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancellationRegulation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEventCancellationRegulation cancellationRegulation;

    public ScheduleEventCancellationRegulation getCancellationRegulation() {
        return cancellationRegulation;
    }

    public void setCancellationRegulation(ScheduleEventCancellationRegulation cancellationRegulation) {
        this.cancellationRegulation = cancellationRegulation;
    }

    public String getCancellationRegulationDD() {
        return "TrainingRequest_cancellationRegulation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeDeductedValue">
    @Column (precision=25, scale=13)
    private BigDecimal employeeDeductedValue;

    public BigDecimal getEmployeeDeductedValue() {
        return employeeDeductedValue;
    }

    public void setEmployeeDeductedValue(BigDecimal employeeDeductedValue) {
        this.employeeDeductedValue = employeeDeductedValue;
    }

    public String getEmployeeDeductedValueDD() {
        return "TrainingRequest_employeeDeductedValue";
    }
    @Transient
    private BigDecimal employeeDeductedValueMask;

    public BigDecimal getEmployeeDeductedValueMask() {
        employeeDeductedValueMask = employeeDeductedValue ;
        return employeeDeductedValueMask;
    }

    public void setEmployeeDeductedValueMask(BigDecimal employeeDeductedValueMask) {
        updateDecimalValue("employeeDeductedValue", employeeDeductedValueMask);
    }

    public String getEmployeeDeductedValueMaskDD() {
        return "TrainingRequest_employeeDeductedValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mangerDeductedValue">
    @Column (precision=25, scale=13)
    private BigDecimal mangerDeductedValue;

    public BigDecimal getMangerDeductedValue() {
        return mangerDeductedValue;
    }

    public void setMangerDeductedValue(BigDecimal mangerDeductedValue) {
        this.mangerDeductedValue = mangerDeductedValue;
    }


    public String getMangerDeductedValueDD() {
        return "TrainingRequest_mangerDeductedValue";
    }
    @Transient
    private BigDecimal mangerDeductedValueMask;

    public BigDecimal getMangerDeductedValueMask() {
        mangerDeductedValueMask = mangerDeductedValue ;
        return mangerDeductedValueMask;
    }

    public void setMangerDeductedValueMask(BigDecimal mangerDeductedValueMask) {
        updateDecimalValue("mangerDeductedValue", mangerDeductedValueMask);
    }


    public String getMangerDeductedValueMaskDD() {
        return "TrainingRequest_mangerDeductedValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="waiting">
    @Column
    private boolean waiting;

    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean waiting) {
        this.waiting = waiting;
    }

    public String getIsWaitingDD() {
        return "TrainingRequest_waiting";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="waitingPriority">
    @Column
    private Short waitingPriority;

    public Short getWaitingPriority() {
        return waitingPriority;
    }

    public void setWaitingPriority(Short waitingPriority) {
        this.waitingPriority = waitingPriority;
    }

    public String getWaitingPriorityDD() {
        return "TrainingRequest_waitingPriority";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reservationNumber">
    @Column
    private Long reservationNumber;

    public Long getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(Long reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public String getReservationNumberDD() {
        return "TrainingRequest_reservationNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reservationDate">
    @Column(name = "reservation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reservationDate;

    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getReservationDateDD() {
        return "TrainingRequest_reservationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="manPowerPlan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ManPowerPlan manPowerPlan;

    public ManPowerPlan getManPowerPlan() {
        return manPowerPlan;
    }

    public void setManPowerPlan(ManPowerPlan manPowerPlan) {
        this.manPowerPlan = manPowerPlan;
    }

    public String getManPowerPlanDD() {
        return "TrainingRequest_manPowerPlan";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetValidated">
    @Column
    private boolean budgetValidated;

    public boolean isBudgetValidated() {
        return budgetValidated;
    }

    public void setBudgetValidated(boolean budgetValidated) {
        this.budgetValidated = budgetValidated;
    }

    public String getBudgetValidatedDD() {
        return "TrainingRequest_isBudgetValidated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="overwriteBudget">
    @Column
    private boolean overwriteBudget;

    public boolean isOverwriteBudget() {
        return overwriteBudget;
    }

    public void setOverwriteBudget(boolean overwriteBudget) {
        this.overwriteBudget = overwriteBudget;
    }

    public String getIsOverwriteBudgetDD() {
        return "TrainingRequest_overwriteBudget";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="planed">
    @Column
    private boolean planed;

    public boolean isPlaned() {
        return planed;
    }

    public void setPlaned(boolean planed) {
        this.planed = planed;
    }

    public String getPlanedDD() {
        return "TrainingRequest_planed";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainingPlan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TrainingPlan trainingPlan;

    public TrainingPlan getTrainingPlan() {
        return trainingPlan;
    }

    public void setTrainingPlan(TrainingPlan trainingPlan) {
        this.trainingPlan = trainingPlan;
    }

    public String getTrainingPlanDD() {
        return "TrainingRequest_trainingPlan";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="actualPrice">
    @Column (precision=25, scale=13)
    private BigDecimal actualPrice;

    public BigDecimal getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getActualPriceDD() {
        return "TrainingRequest_actualPrice";
    }
    @Transient
    private BigDecimal actualPriceMask;

    public BigDecimal getActualPriceMask() {
        actualPriceMask = actualPrice ;
        return actualPriceMask;
    }

    public void setActualPriceMask(BigDecimal actualPriceMask) {
        updateDecimalValue("actualPrice", actualPriceMask);
    }

    public String getActualPriceMaskDD() {
        return "TrainingRequest_actualPriceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paidAmount">
    @Column (precision=25, scale=13)
    private BigDecimal paidAmount;

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }
    
    public String getPaidAmountDD() {
        return "TrainingRequest_paidAmount";
    }
    @Transient
    private BigDecimal paidAmountMask;

    public BigDecimal getPaidAmountMask() {
        paidAmountMask = paidAmount ;
        return paidAmountMask;
    }

    public void setPaidAmountMask(BigDecimal paidAmountMask) {
        updateDecimalValue("paidAmount", paidAmountMask);
    }

    public String getPaidAmountMaskDD() {
        return "TrainingRequest_paidAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetReserved">
    @Column
    private boolean budgetReserved;

    public boolean isBudgetReserved() {
        return budgetReserved;
    }

    public void setBudgetReserved(boolean budgetReserved) {
        this.budgetReserved = budgetReserved;
    }

    public String getBudgetReservedDD() {
        return "TrainingRequest_budgetReserved";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reservedAmountOfBudget">
    @Column (precision=25, scale=13)
    private BigDecimal reservedAmountOfBudget;

    public BigDecimal getReservedAmountOfBudget() {
        return reservedAmountOfBudget;
    }

    public void setReservedAmountOfBudget(BigDecimal reservedAmountOfBudget) {
        this.reservedAmountOfBudget = reservedAmountOfBudget;
    }

    public String getReservedAmountOfBudgetDD() {
        return "TrainingRequest_reservedAmountOfBudget";
    }
    @Transient
    private BigDecimal reservedAmountOfBudgetMask;

    public BigDecimal getReservedAmountOfBudgetMask() {
        reservedAmountOfBudgetMask = reservedAmountOfBudget ;
        return reservedAmountOfBudgetMask;
    }

    public void setReservedAmountOfBudgetMask(BigDecimal reservedAmountOfBudgetMask) {
        updateDecimalValue("reservedAmountOfBudget", reservedAmountOfBudgetMask);
    }

    public String getReservedAmountOfBudgetMaskDD() {
        return "TrainingRequest_reservedAmountOfBudgetMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="reservationNotes">
    @Column
    private String reservationNotes;

    public String getReservationNotes() {
        return reservationNotes;
    }

    public void setReservationNotes(String reservationNotes) {
        this.reservationNotes = reservationNotes;
    }

    public String getReservationNotesDD() {
        return "TrainingRequest_reservationNotes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attandanceDaysNumber">
    @Column
    private BigDecimal attandanceDaysNumber;

    public BigDecimal getAttandanceDaysNumber() {
        return attandanceDaysNumber;
    }

    public void setAttandanceDaysNumber(BigDecimal attandanceDaysNumber) {
        this.attandanceDaysNumber = attandanceDaysNumber;
    }

    public String getAttandanceDaysNumberDD() {
        return "TrainingRequest_attandanceDaysNumber";
    }
    @Transient
    private BigDecimal attandanceDaysNumberMask;

    public BigDecimal getAttandanceDaysNumberMask() {
        attandanceDaysNumberMask = attandanceDaysNumber ;
        return attandanceDaysNumberMask;
    }

    public void setAttandanceDaysNumberMask(BigDecimal attandanceDaysNumberMask) {
        updateDecimalValue("attandanceDaysNumber", attandanceDaysNumberMask);
    }

    public String getAttandanceDaysNumberMaskDD() {
        return "TrainingRequest_attandanceDaysNumberMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attandancePercent">
    @Column
    private BigDecimal attandancePercent;

    public BigDecimal getAttandancePercent() {
        return attandancePercent;
    }

    public void setAttandancePercent(BigDecimal attandancePercent) {
        this.attandancePercent = attandancePercent;
    }

    public String getAttandancePercentDD() {
        return "TrainingRequest_attandancePercent";
    }
    @Transient
    private BigDecimal attandancePercentMask;

    public BigDecimal getAttandancePercentMask() {
        attandancePercentMask = attandancePercent ;
        return attandancePercentMask;
    }

    public void setAttandancePercentMask(BigDecimal attandancePercentMask) {
        updateDecimalValue("attandancePercent",attandancePercentMask);
    }

    public String getAttandancePercentMaskDD() {
        return "TrainingRequest_attandancePercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendanceRegulation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEventAttendanceRegulation attendanceRegulation;

    public ScheduleEventAttendanceRegulation getAttendanceRegulation() {
        return attendanceRegulation;
    }

    public void setAttendanceRegulation(ScheduleEventAttendanceRegulation attendanceRegulation) {
        this.attendanceRegulation = attendanceRegulation;
    }

    public String getAttendanceRegulationDD() {
        return "TrainingRequest_attendanceRegulation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendanceEmployeeValue">
    @Column (precision=25, scale=13)
    private BigDecimal attendanceEmployeeValue;

    public BigDecimal getAttendanceEmployeeValue() {
        return attendanceEmployeeValue;
    }

    public void setAttendanceEmployeeValue(BigDecimal attendanceEmployeeValue) {
        this.attendanceEmployeeValue = attendanceEmployeeValue;
    }

    public String getAttendanceEmployeeValueDD() {
        return "TrainingRequest_attendanceEmployeeValue";
    }
    @Transient
    private BigDecimal attendanceEmployeeValueMask;

    public BigDecimal getAttendanceEmployeeValueMask() {
        attendanceEmployeeValueMask = attendanceEmployeeValue ;
        return attendanceEmployeeValueMask;
    }

    public void setAttendanceEmployeeValueMask(BigDecimal attendanceEmployeeValueMask) {
        updateDecimalValue("attendanceEmployeeValue", attendanceEmployeeValueMask);
    }

    public String getAttendanceEmployeeValueMaskDD() {
        return "TrainingRequest_attendanceEmployeeValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="evaluationScore">
    @Column
    private BigDecimal evaluationScore;

    public BigDecimal getEvaluationScore() {
        return evaluationScore;
    }

    public void setEvaluationScore(BigDecimal evaluationScore) {
        this.evaluationScore = evaluationScore;
    }

    public String getEvaluationScoreDD() {
        return "TrainingRequest_evaluationScore";
    }
    @Transient
    private BigDecimal evaluationScoreMask;

    public BigDecimal getEvaluationScoreMask() {
        evaluationScoreMask = evaluationScore ;
        return evaluationScoreMask;
    }

    public void setEvaluationScoreMNask(BigDecimal evaluationScoreMask) {
        updateDecimalValue("evaluationScore", evaluationScoreMask);
    }

    public String getEvaluationScoreMaskDD() {
        return "TrainingRequest_evaluationScoreMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="evaluationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaluationDate;

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluationDateDD() {
        return "TrainingRequest_evaluationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="evaluationNotes">
    @Column
    private String evaluationNotes;

    public String getEvaluationNotes() {
        return evaluationNotes;
    }

    public void setEvaluationNotes(String evaluationNotes) {
        this.evaluationNotes = evaluationNotes;
    }

    public String getEvaluationNotesDD() {
        return "TrainingRequest_evaluationNotes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="participent">
    @Column
    private Short participent;

    public Short getParticipent() {
        return participent;
    }

    public void setParticipent(Short participent) {
        this.participent = participent;
    }
    
    public String getParticipentDD() {
        return "TrainingRequest_participent";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
	private Currency currency;
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getCurrencyDD() {
        return "TrainingRequest_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="inviteMail">
    @Column
    private String inviteMail;

    public String getInviteMail() {
        return inviteMail;
    }

    public void setInviteMail(String inviteMail) {
        this.inviteMail = inviteMail;
    }
    
    public String getInviteMailDD() {
        return "TrainingRequest_inviteMail";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="invited">
    @Column
    private boolean invited;

    public boolean isInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    public String getIsInvitedDD() {
        return "TrainingRequest_invited";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="inviteStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC inviteStatus;

    public UDC getInviteStatus() {
        return inviteStatus;
    }

    public void setInviteStatus(UDC inviteStatus) {
        this.inviteStatus = inviteStatus;
    }
    
    public String getInviteStatusDD() {
        return "TrainingRequest_inviteStatus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="inviteDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date inviteDate;

    public Date getInviteDate() {
        return inviteDate;
    }

    public void setInviteDate(Date inviteDate) {
        this.inviteDate = inviteDate;
    }
    
    public String getInviteDateDD() {
        return "TrainingRequest_inviteDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="inviteReceiveDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date inviteReceiveDate;

    public Date getInviteReceiveDate() {
        return inviteReceiveDate;
    }

    public void setInviteReceiveDate(Date inviteReceiveDate) {
        this.inviteReceiveDate = inviteReceiveDate;
    }
    
    public String getInviteReceiveDateDD() {
        return "TrainingRequest_inviteReceiveDate";
    }
    // </editor-fold>
}