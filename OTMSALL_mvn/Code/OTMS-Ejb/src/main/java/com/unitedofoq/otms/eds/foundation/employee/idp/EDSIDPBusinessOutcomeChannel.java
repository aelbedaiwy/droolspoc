package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.training.EDSChannel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields="businessOutcome")
public class EDSIDPBusinessOutcomeChannel extends BaseEntity{
    @Column(name="comments")
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSIDPBusinessOutcome businessOutcome;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSChannel channel;

    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    

    public String getCommentDD()           {   return "EDSIDPBusinessOutcomeChannel_comment";  }
    public String getBusinessOutcomeDD()   {   return "EDSIDPBusinessOutcomeChannel_businessOutcome";  }
    public String getChannelDD()           {   return "EDSIDPBusinessOutcomeChannel_channel";  }

    public EDSIDPBusinessOutcome getBusinessOutcome() {
        return businessOutcome;
    }

    public void setBusinessOutcome(EDSIDPBusinessOutcome BusinessOutcome) {
        this.businessOutcome = BusinessOutcome;
    }

    public EDSChannel getChannel() {
        return channel;
    }

    public void setChannel(EDSChannel channel) {
        this.channel = channel;
    }

    @Override
    protected void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
    
}
