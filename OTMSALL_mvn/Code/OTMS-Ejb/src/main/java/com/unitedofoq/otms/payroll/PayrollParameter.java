package com.unitedofoq.otms.payroll;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.*;


/**
 * The persistent class for the system_setup database table.
 *
 */
@Entity
@Table
@ParentEntity(fields="company")
public class PayrollParameter extends BusinessObjectBaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "PayrollParameter_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="valueData">
	@Column
	private String valueData;

    public String getValueData() {
        return valueData;
    }

    public void setValueData(String valueData) {
        this.valueData = valueData;
    }

    public String getValueDataDD() {
        return "PayrollParameter_valueData";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="auxiliary">
	@Column
	private String auxiliary;

    public String getAuxiliary() {
        return auxiliary;
    }

    public void setAuxiliary(String auxiliary) {
        this.auxiliary = auxiliary;
    }
    public String getAuxiliaryDD() {
        return "PayrollParameter_auxiliary";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
   private  Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
      public String getCompanyDD() {
        return "PayrollParameter_company";
    }
      // </editor-fold>
}