package com.unitedofoq.otms.personnel.absence;
import com.unitedofoq.otms.personnel.dayoff.DayOffAffectingSalaryElement;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AbsenceAffectingSalaryElement extends DayOffAffectingSalaryElement {
    // <editor-fold defaultstate="collapsed" desc="absence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn (nullable= false)
    private Absence absence;

    public Absence getAbsence() {
        return absence;
    }

    public void setAbsence(Absence absence) {
        this.absence = absence;
    }

    public String getAbsenceDD() {
        return "AbsenceAffectingSalaryElement_absence";
    }
    
// </editor-fold>
   

}
