/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.holiday;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dev
 */
@Entity
public class EmployeeHoliday extends BaseEntity {

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeHoliday_employee";
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date holidayDate;

    public Date getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(Date holidayDate) {
        this.holidayDate = holidayDate;
    }

    public String getHolidayDateDD() {
        return "EmployeeHoliday_holidayDate";
    }

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    HolidayDates holidayDates;

    public HolidayDates getHolidayDates() {
        return holidayDates;
    }

    public String getHolidayDatesDD() {
        return "EmployeeHoliday_holidayDates";
    }

    public void setHolidayDates(HolidayDates holidayDates) {
        this.holidayDates = holidayDates;
    }
}
