/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


//framwork for ajule bussiness solution
/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeePenalty  extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn (nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeePenalty_employee";
    }
     // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penalty">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty penalty;

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }

    public String getPenaltyDD() {
        return "EmployeePenalty_penalty";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="countNumber">
    @Column
    private Short countNumber;//(name = "count_no")

    public Short getCountNumber() {
        return countNumber;
    }

    public void setCountNumber(Short countNumber) {
        this.countNumber = countNumber;
    }

     public String getCountNumberDD() {
        return "EmployeePenalty_countNumber";
    }

 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastPenaltyDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPenaltyDate;//(name = "last_penalty_date")

    public Date getLastPenaltyDate() {
        return lastPenaltyDate;
    }

    public void setLastPenaltyDate(Date lastPenaltyDate) {
        this.lastPenaltyDate = lastPenaltyDate;
    }

    public String getLastPenaltyDateDD() {
        return "EmployeePenalty_lastPenaltyDate";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="pointsSum">
    @Column(precision=18,scale=3)
    private BigDecimal pointsSum;//(name = "points_sum")

    public BigDecimal getPointsSum() {
        return pointsSum;
    }

    public void setPointsSum(BigDecimal pointsSum) {
        this.pointsSum = pointsSum;
    }

    public String getPointsSumDD() {
        return "EmployeePenalty_pointsSum";
    }
    @Transient
    private BigDecimal pointsSumMask;

    public BigDecimal getPointsSumMask() {
        pointsSumMask = pointsSum ;
        return pointsSumMask;
    }

    public void setPointsSumMask(BigDecimal pointsSumMask) {
        updateDecimalValue("pointsSum",pointsSumMask);
    }

    public String getPointsSumMaskDD() {
        return "EmployeePenalty_pointsSumMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="total">
    @Column(precision=18,scale=3)
    private BigDecimal total;//(name = "penalty_total")

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getTotalDD() {
        return "EmployeePenalty_total";
    }
    @Transient
    private BigDecimal totalMask;

    public BigDecimal getTotalMask() {
        totalMask = total ;
        return totalMask;
    }

    public void setTotalMask(BigDecimal totalMask) {
        updateDecimalValue("total",totalMask);
    }

    public String getTotalMaskDD() {
        return "EmployeePenalty_totalMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="totalDays">
    @Column(precision=18,scale=3)
    private BigDecimal totalDays;//(name = "penalty_total_days")

    public BigDecimal getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(BigDecimal totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalDaysDD() {
        return "EmployeePenalty_totalDays";
    }
    @Transient
    private BigDecimal totalDaysMask;

    public BigDecimal getTotalDaysMask() {
        totalDaysMask = totalDays;
        return totalDaysMask;
    }

    public void setTotalDaysMask(BigDecimal totalDaysMask) {
        updateDecimalValue("totalDays",totalDaysMask);
    }

    public String getTotalDaysMaskDD() {
        return "EmployeePenalty_totalDaysMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentMonthOccurance">
    @Column(precision=18,scale=3)
    private BigDecimal currentMonthOccurance;//(name = "current_month_occurance")

    public BigDecimal getCurrentMonthOccurance() {
        return currentMonthOccurance;
    }

    public void setCurrentMonthOccurance(BigDecimal currentMonthOccurance) {
        this.currentMonthOccurance = currentMonthOccurance;
    }

    public String getCurrentMonthOccuranceDD() {
        return "EmployeePenalty_currentMonthOccurance";
    }
    @Transient
    private BigDecimal currentMonthOccuranceMask;

    public BigDecimal getCurrentMonthOccuranceMask() {
        currentMonthOccuranceMask = currentMonthOccurance ;
        return currentMonthOccuranceMask;
    }

    public void setCurrentMonthOccuranceMask(BigDecimal currentMonthOccuranceMask) {
        updateDecimalValue("currentMonthOccurance",currentMonthOccuranceMask);
    }

    public String getCurrentMonthOccuranceMaskDD() {
        return "EmployeePenalty_currentMonthOccuranceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshStartFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date refreshStartFrom;//(name = "refresh_start_from")

    public Date getRefreshStartFrom() {
        return refreshStartFrom;
    }

    public void setRefreshStartFrom(Date refreshStartFrom) {
        this.refreshStartFrom = refreshStartFrom;
    }

     public String getRefreshStartFromDD() {
        return "EmployeePenalty_refreshStartFrom";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postponedValues">
    @Column(precision=18,scale=3)
    private BigDecimal postponedValues;//(name = "postponed_penalty_values")

    public BigDecimal getPostponedValues() {
        return postponedValues;
    }

    public void setPostponedValues(BigDecimal postponedValues) {
        this.postponedValues = postponedValues;
    }
    public String getPostponedValuesDD() {
        return "EmployeePenalty_postponedValues";
    }
    @Transient
    private BigDecimal postponedValuesMask;

    public BigDecimal getPostponedValuesMask() {
        postponedValuesMask = postponedValues;
        return postponedValuesMask;
    }

    public void setPostponedValuesMask(BigDecimal postponedValuesMask) {
        updateDecimalValue("postponedValues",postponedValuesMask);
    }
    public String getPostponedValuesMaskDD() {
        return "EmployeePenalty_postponedValuesMask";
    }
    // </editor-fold>
}
