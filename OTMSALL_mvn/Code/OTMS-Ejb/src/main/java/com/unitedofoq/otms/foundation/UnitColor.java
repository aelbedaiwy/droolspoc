package com.unitedofoq.otms.foundation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author tbadran
 */
@Entity
@DiscriminatorValue("Unit")
public class UnitColor extends NodeColor {

}
