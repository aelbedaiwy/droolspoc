
package com.unitedofoq.otms.foundation.job;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.*;

@Entity
@ParentEntity(fields={"job"})
public class JobCertificateNew extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Job job;

    public void setJob(Job job) {
        this.job = job;
    }

    public Job getJob() {
        return job;
    }

    public String getJobDD() {
        return "JobCertificateNew_job";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="certificate">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC certificate;

    public void setCertificate(UDC certificate) {
        this.certificate = certificate;
    }

    public UDC getCertificate() {
        return certificate;
    }

    public String getCertificateDD() {
        return "JobCertificateNew_certificate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="otherCertificate">
    @Column
    private String otherCertificate;

    public void setOtherCertificate(String otherCertificate) {
        this.otherCertificate = otherCertificate;
    }

    public String getOtherCertificate() {
        return otherCertificate;
    }

    public String getOtherCertificateDD() {
        return "JobCertificateNew_otherCertificate";
    }
    // </editor-fold>

}
