/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.investigation;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author lahmed
 */
@Entity
public class InvestigationStatusTranslation extends BaseEntityTranslation {
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
     // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="salaryEffect">
    private String salaryEffect;

    public String getSalaryEffect() {
        return salaryEffect;
    }

    public void setSalaryEffect(String salaryEffect) {
        this.salaryEffect = salaryEffect;
    }
    //</editor-fold>
}