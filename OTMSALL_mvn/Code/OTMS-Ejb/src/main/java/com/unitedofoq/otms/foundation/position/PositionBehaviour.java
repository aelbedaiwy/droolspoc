/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Behavior;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"position"})
//@Table(name="EDSJobBehavior")
public class PositionBehaviour extends BusinessObjectBaseEntity {
   // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "PositionBehaviour_position";
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="behavior">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Behavior behavior;

    public Behavior getBehavior() {
        return behavior;
    }

    public void setBehavior(Behavior behavior) {
        this.behavior = behavior;
    }

    public String getBehaviorDD() {
        return "PositionBehaviour_behavior";
    }
    // </editor-fold>
   // <editor-fold defaultstate="collapsed" desc="score">
   @Column
   private double score;

    public double getScore() {
        return score;
    }
    public String getScoreDD() {
        return "PositionBehaviour_score";
    }
    public void setScore(double score) {
        this.score = score;
    }
   // </editor-fold>
}
