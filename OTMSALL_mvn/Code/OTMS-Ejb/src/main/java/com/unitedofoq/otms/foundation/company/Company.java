package com.unitedofoq.otms.foundation.company;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.employee.Goal;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplate;
import com.unitedofoq.otms.foundation.CostItem;
import com.unitedofoq.otms.foundation.competency.CompetencyGroup;
import com.unitedofoq.otms.foundation.currency.ExchangeRate;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.jobGroup.JobGroup;
import com.unitedofoq.otms.foundation.training.University;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.foundation.unit.UnitLevel;
import com.unitedofoq.otms.foundation.unit.UnitLevelIndex;
import com.unitedofoq.otms.health.MedicalCategories;
import com.unitedofoq.otms.health.MedicalProvider;
import com.unitedofoq.otms.health.MedicalTreatmentTypes;
import com.unitedofoq.otms.payroll.Bank;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.payroll.NetToGross;
import com.unitedofoq.otms.payroll.PayrollParameter;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.costcenter.CostCenterGroup;
import com.unitedofoq.otms.payroll.costcenter.SapProject;
import com.unitedofoq.otms.payroll.gl.GLAccount;
import com.unitedofoq.otms.payroll.gl.GLAccountSegment;
import com.unitedofoq.otms.payroll.gl.GLAccountUDC;
import com.unitedofoq.otms.payroll.gl.GLMappedAccount;
import com.unitedofoq.otms.payroll.insurance.Insurance;
import com.unitedofoq.otms.payroll.insurance.InsuranceGroup;
import com.unitedofoq.otms.payroll.insurance.InsuranceOffices;
import com.unitedofoq.otms.payroll.loan.Loan;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElementType;
import com.unitedofoq.otms.payroll.tax.Tax;
import com.unitedofoq.otms.payroll.tax.TaxGroup;
import com.unitedofoq.otms.personnel.absence.Absence;
import com.unitedofoq.otms.personnel.dayoff.DayOff;
import com.unitedofoq.otms.personnel.holiday.Holiday;
import com.unitedofoq.otms.personnel.investigation.InvestigationSetup;
import com.unitedofoq.otms.personnel.investigation.InvestigationStatus;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.personnel.vacation.Vacation;
import com.unitedofoq.otms.personnel.vacationgroup.VacationGroup;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import com.unitedofoq.otms.recruitment.requisition.Requisition;
import com.unitedofoq.otms.timemanagement.TimeManagementMachine;
import com.unitedofoq.otms.timemanagement.timesheet.Allowance;
import com.unitedofoq.otms.timemanagement.timesheet.Item;
import com.unitedofoq.otms.timemanagement.timesheet.Project;
import com.unitedofoq.otms.training.activity.ProviderTrainingPlan;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue(value = "MASTER")
@ChildEntity(fields = {"penaltys", "units", "unitLevels", "unitLevelIndexes", "jobs",
    "payGrades", "vacations", "taxGroups",
    "salaryElements", "absences", "exchangeRates", "costCenters",
    "costCenterGroups", "universitys", "holidays",
    "legalEntitys", "applicants", "scaleTypes", "insuranceGroups",
    "dayOffs", "payrollParameters", "salaryElementTypes", "goal",
    "jobGroups", "competencyGroups", "vacationGroups", "insurances", "taxes",
    "banks", "insuranceOffices", "appraisalTemplates", "loans",
    "timeManagementMachines", "costs", "glAccounts", "glMappedAccounts",
    "glSegments", "glAccUDCs", "companyCostCenters", "investigations", "investigationStats", "netToGross",
    "medicalTreatmentTypess", "medicalCategoriess", "requisitionRequests",/*"successionPlans",*/ "projects", "allowances", "item", "sapProjects", "medicalProvider", "trainingPlans"})
public class Company extends CompanyBase {

    // <editor-fold defaultstate="collapsed" desc="goal">
    @OneToMany(mappedBy = "company")

    private List<Goal> goal;

    public List<Goal> getGoal() {
        return goal;
    }

    public void setGoal(List<Goal> goal) {
        this.goal = goal;
    }

    public String getGoalDD() {
        return "Company_goal";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitLevels">
    @OneToMany(mappedBy = "company")
    private List<UnitLevel> unitLevels;

    public List<UnitLevel> getUnitLevels() {
        return unitLevels;
    }

    public void setUnitLevels(List<UnitLevel> unitLevels) {
        this.unitLevels = unitLevels;
    }

    public String getUnitLevelsDD() {
        return "Company_unitLevels";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="units">
    @OneToMany(mappedBy = "company")
    private List<Unit> units;

    public List<Unit> getUnits() {
        return units;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    public String getUnitsDD() {
        return "Company_units";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobs">
    @OneToMany(mappedBy = "company")
    @JoinColumn
    private List<Job> jobs;

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public String getJobsDD() {
        return "Company_jobs";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="shortName">
    @Column
    private String shortName;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getShortNameDD() {
        return "Company_shortName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="creationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDateDD() {
        return "Company_creationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="effectiveFrom">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveFrom;

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public String getEffectiveFromDD() {
        return "Company_effectiveFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="effectiveTo">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveTo;

    public Date getEffectiveTo() {
        return effectiveTo;
    }

    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }

    public String getEffectiveToDD() {
        return "Company_effectiveTo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="createdBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee createdBy;

    public Employee getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByDD() {
        return "Company_createdBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="updateDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateDateDD() {
        return "Company_updateDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="updatedBy">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee updatedBy;

    public Employee getUpdatedBy() {
        return updatedBy;
    }

    public String getUpdatedByDD() {
        return "Company_updatedBy";
    }

    public void setUpdatedBy(Employee updatedBy) {
        this.updatedBy = updatedBy;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="isChartViewable">
    private boolean chartViewable;

    public boolean isChartViewable() {
        return chartViewable;
    }

    public void setChartViewable(boolean chartViewable) {
        this.chartViewable = chartViewable;
    }

    public String getChartViewableDD() {
        return "Company_chartViewable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterCode">
    private String costCenterCode;

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public String getCostCenterCodeDD() {
        return "Company_costCenterCode";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Translatable(translationField = "costCenterDescriptionTranslated")
    private String costCenterDescription;

    public String getCostCenterDescription() {
        return costCenterDescription;
    }

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }
    @Translation(originalField = "costCenterDescription")
    @Transient
    private String costCenterDescriptionTranslated;

    public String getCostCenterDescriptionTranslated() {
        return costCenterDescriptionTranslated;
    }

    public void setCostCenterDescriptionTranslated(String costCenterDescriptionTranslated) {
        this.costCenterDescriptionTranslated = costCenterDescriptionTranslated;
    }

    public String getCostCenterDescriptionTranslatedDD() {
        return "Company_costCenterDescriptionTranslated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionDescription">
    @Translatable(translationField = "positionDescriptionTranslated")
    private String positionDescription;

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }
    @Translation(originalField = "positionDescription")
    @Transient
    private String positionDescriptionTranslated;

    public String getPositionDescriptionTranslated() {
        return positionDescriptionTranslated;
    }

    public void setPositionDescriptionTranslated(String positionDescriptionTranslated) {
        this.positionDescriptionTranslated = positionDescriptionTranslated;
    }

    public String getPositionDescriptionTranslatedDD() {
        return "Company_positionDescriptionTranslated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unitLevelIndexes">
    @OneToMany(mappedBy = "company")
    private List<UnitLevelIndex> unitLevelIndexes;

    public List<UnitLevelIndex> getUnitLevelIndexes() {
        return unitLevelIndexes;
    }

    public String getUnitLevelIndexesDD() {
        return "Company_unitLevelIndexes";
    }

    public void setUnitLevelIndexes(List<UnitLevelIndex> unitLevelIndexes) {
        this.unitLevelIndexes = unitLevelIndexes;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGrades">
    @OneToMany(mappedBy = "company")
    private List<PayGrade> payGrades;

    public List<PayGrade> getPayGrades() {
        return payGrades;
    }

    public void setPayGrades(List<PayGrade> payGrades) {
        this.payGrades = payGrades;
    }

    public String getPayGradesDD() {
        return "Company_payGrades";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacations">
    @OneToMany(mappedBy = "company")
    @JoinColumn
    private List<Vacation> vacations;

    public List<Vacation> getVacations() {
        return vacations;
    }

    public void setVacations(List<Vacation> vacations) {
        this.vacations = vacations;
    }

    public String getVacationsDD() {
        return "Company_vacations";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxGroups">
    @OneToMany(mappedBy = "company")
    private List<TaxGroup> taxGroups;

    public List<TaxGroup> getTaxGroups() {
        return taxGroups;
    }

    public void setTaxGroups(List<TaxGroup> taxGroups) {
        this.taxGroups = taxGroups;
    }

    public String getTaxGroupsDD() {
        return "Company_taxGroups";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxes">
    @OneToMany(mappedBy = "company")
    private List<Tax> taxes;

    public List<Tax> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Tax> taxes) {
        this.taxes = taxes;
    }

    public String getTaxesDD() {
        return "Company_taxes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElements">
    @OneToMany(mappedBy = "company")
    private List<SalaryElement> salaryElements;

    public List<SalaryElement> getSalaryElements() {
        return salaryElements;
    }

    public void setSalaryElements(List<SalaryElement> salaryElements) {
        this.salaryElements = salaryElements;
    }

    public String getSalaryElementsDD() {
        return "Company_salaryElements";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payrollParameters">
    @OneToMany(mappedBy = "company")
    private List<PayrollParameter> payrollParameters;

    public List<PayrollParameter> getPayrollParameters() {
        return payrollParameters;
    }

    public void setPayrollParameters(List<PayrollParameter> payrollParameters) {
        this.payrollParameters = payrollParameters;
    }

    public String getPayrollParametersDD() {
        return "Company_payrollParameters";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="absences">
    @OneToMany(mappedBy = "company")
    private List<Absence> absences;

    public List<Absence> getAbsences() {
        return absences;
    }

    public void setAbsences(List<Absence> absences) {
        this.absences = absences;
    }

    public String getAbsencesDD() {
        return "Company_absences";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exchangeRates">
    @OneToMany(mappedBy = "company")
    private List<ExchangeRate> exchangeRates;

    public List<ExchangeRate> getExchangeRates() {
        return exchangeRates;
    }

    public void setExchangeRates(List<ExchangeRate> exchangeRates) {
        this.exchangeRates = exchangeRates;
    }

    public String getExchangeRatesDD() {
        return "Company_exchangeRates";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenters">
    @OneToMany(mappedBy = "company")
    private List<CostCenter> costCenters;

    public List<CostCenter> getCostCenters() {
        return costCenters;
    }

    public void setCostCenters(List<CostCenter> costCenters) {
        this.costCenters = costCenters;
    }

    public String getCostCentersDD() {
        return "Company_costCenters";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costCenterGroups">
    @OneToMany(mappedBy = "company")
    private List<CostCenterGroup> costCenterGroups;

    public List<CostCenterGroup> getCostCenterGroups() {
        return costCenterGroups;
    }

    public void setCostCenterGroups(List<CostCenterGroup> costCenterGroups) {
        this.costCenterGroups = costCenterGroups;
    }

    public String getCostCenterGroupsDD() {
        return "Company_costCenterGroups";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="universitys">
    @OneToMany(mappedBy = "company")
    private List<University> universitys;

    public List<University> getUniversitys() {
        return universitys;
    }

    public void setUniversitys(List<University> universitys) {
        this.universitys = universitys;
    }

    public String getUniversitysDD() {
        return "Company_universitys";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="holidays">
    @OneToMany(mappedBy = "company")
    private List<Holiday> holidays;

    public List<Holiday> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<Holiday> holidays) {
        this.holidays = holidays;
    }

    public String getHolidaysDD() {
        return "Company_holidays";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalEntitys">
    @OneToMany(mappedBy = "company")
    private List<LegalEntity> legalEntitys;

    public List<LegalEntity> getLegalEntitys() {
        return legalEntitys;
    }

    public void setLegalEntitys(List<LegalEntity> legalEntitys) {
        this.legalEntitys = legalEntitys;
    }

    public String getLegalEntitysDD() {
        return "Company_legalEntitys";

    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applicants">
    @OneToMany(mappedBy = "company")
    private List<Applicant> applicants;

    public List<Applicant> getApplicants() {
        return applicants;
    }

    public void setApplicants(List<Applicant> applicants) {
        this.applicants = applicants;
    }

    public String getApplicantsDD() {
        return "Company_applicants";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requisitions">
    @OneToMany(mappedBy = "homeCompany")
    private List<Requisition> requisitions;

    public List<Requisition> getRequisitions() {
        return requisitions;
    }

    public void setRequisitions(List<Requisition> requisitions) {
        this.requisitions = requisitions;
    }

    public String getRequisitionsDD() {
        return "Company_requisitions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scaleTypes">
    @OneToMany(mappedBy = "company")
    private List<ScaleType> scaleTypes;

    public List<ScaleType> getScaleTypes() {
        return scaleTypes;
    }

    public void setScaleTypes(List<ScaleType> scaleTypes) {
        this.scaleTypes = scaleTypes;
    }

    public String getScaleTypesDD() {
        return "Company_scaleTypes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltys">
    @OneToMany(mappedBy = "company")
    private List<Penalty> penaltys;

    public List<Penalty> getPenaltys() {
        return penaltys;
    }

    public void setPenaltys(List<Penalty> penaltys) {
        this.penaltys = penaltys;
    }

    public String getPenaltysDD() {
        return "Company_penaltys";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insuranceGroups">
    @OneToMany(mappedBy = "company")
    private List<InsuranceGroup> insuranceGroups;

    public List<InsuranceGroup> getInsuranceGroups() {
        return insuranceGroups;
    }

    public void setInsuranceGroups(List<InsuranceGroup> insuranceGroups) {
        this.insuranceGroups = insuranceGroups;
    }

    public String getInsuranceGroupsDD() {
        return "Company_insuranceGroups";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="insurances">
    @OneToMany(mappedBy = "company")
    private List<Insurance> insurances;

    public List<Insurance> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<Insurance> insurances) {
        this.insurances = insurances;
    }

    public String getInsurancesDD() {
        return "Company_insurances";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOffs">
    @OneToMany(mappedBy = "company")
    private List<DayOff> dayOffs;

    public List<DayOff> getDayOffs() {
        return dayOffs;
    }

    public void setDayOffs(List<DayOff> dayOffs) {
        this.dayOffs = dayOffs;
    }

    public String getDayOffsDD() {
        return "Company_dayOffs";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElementTypes">
    @OneToMany(mappedBy = "company")
    private List<SalaryElementType> salaryElementTypes;

    public List<SalaryElementType> getSalaryElementTypes() {
        return salaryElementTypes;
    }

    public void setSalaryElementTypes(List<SalaryElementType> salaryElementTypes) {
        this.salaryElementTypes = salaryElementTypes;
    }

    public String getSalaryElementTypesDD() {
        return "Company_salaryElementTypes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobGroups">
    @OneToMany(mappedBy = "company")
    private List<JobGroup> jobGroups;

    public List<JobGroup> getJobGroups() {
        return jobGroups;
    }

    public void setJobGroups(List<JobGroup> jobGroups) {
        this.jobGroups = jobGroups;
    }

    public String getJobGroupsDD() {
        return "Company_jobGroups";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competencyGroups">
    @OneToMany(mappedBy = "company")
    private List<CompetencyGroup> competencyGroups;

    public List<CompetencyGroup> getCompetencyGroups() {
        return competencyGroups;
    }

    public void setCompetencyGroups(List<CompetencyGroup> competencyGroups) {
        this.competencyGroups = competencyGroups;
    }

    public String getCompetencyGroupsDD() {
        return "Company_competencyGroups";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationGroups">
    @OneToMany(mappedBy = "company")
    @JoinColumn
    private List<VacationGroup> vacationGroups;

    public List<VacationGroup> getVacationGroups() {
        return vacationGroups;
    }

    public void setVacationGroups(List<VacationGroup> vacationGroups) {
        this.vacationGroups = vacationGroups;
    }

    public String getVacationGroupsDD() {
        return "Company_vacationGroups";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="banks">
    @OneToMany(mappedBy = "company")
    private List<Bank> banks;

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public String getBanksDD() {
        return "Company_banks";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="insuranceOffices">
    @OneToMany(mappedBy = "company")
    private List<InsuranceOffices> insuranceOffices;

    public List<InsuranceOffices> getInsuranceOffices() {
        return insuranceOffices;
    }

    public void setInsuranceOffices(List<InsuranceOffices> insuranceOffices) {
        this.insuranceOffices = insuranceOffices;
    }

    public String getInsuranceOfficesDD() {
        return "Company_insuranceOffices";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="appraisalTemplates">
    @OneToMany
    private List<AppraisalTemplate> appraisalTemplates;

    public List<AppraisalTemplate> getAppraisalTemplates() {
        return appraisalTemplates;
    }

    public void setAppraisalTemplates(List<AppraisalTemplate> appraisalTemplates) {
        this.appraisalTemplates = appraisalTemplates;
    }

    public String getAppraisalTemplatesDD() {
        return "Company_appraisalTemplates";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="loans">
    @OneToMany(mappedBy = "company")
    @JoinColumn
    private List<Loan> loans;

    public List<Loan> getLoans() {
        return loans;
    }

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }

    public String getLoansDD() {
        return "Company_loans";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="timeManagementMachines">
    @OneToMany(mappedBy = "company")
    private List<TimeManagementMachine> timeManagementMachines;

    public List<TimeManagementMachine> getTimeManagementMachines() {
        return timeManagementMachines;
    }

    public void setTimeManagementMachines(List<TimeManagementMachine> timeManagementMachines) {
        this.timeManagementMachines = timeManagementMachines;
    }

    public String getTimeManagementMachinesDD() {
        return "Company_timeManagementMachines";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="bonus">
    /*@OneToMany(mappedBy="company")
     private List<EOSBonus> bonus;

     public List<EOSBonus> getBonus() {
     return bonus;
     }

     public void setBonus(List<EOSBonus> bonus) {
     this.bonus = bonus;
     }

     public String getBonusDD() {
     return "Company_bonus";
     }*/
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="costs">
    @OneToMany(mappedBy = "company")
    private List<CostItem> costs;

    public List<CostItem> getCosts() {
        return costs;
    }

    public void setCosts(List<CostItem> costs) {
        this.costs = costs;
    }

    public String getCostsDD() {
        return "Company_costs";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyInsuranceNumber">
    @Column
    private Integer companyInsuranceNumber;

    public Integer getCompanyInsuranceNumber() {
        return companyInsuranceNumber;
    }

    public void setCompanyInsuranceNumber(Integer companyInsuranceNumber) {
        this.companyInsuranceNumber = companyInsuranceNumber;
    }

    public String getCompanyInsuranceNumberDD() {
        return "Company_companyInsuranceNumber";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalView">
    @Column
    private String legalView;

    public String getLegalView() {
        return legalView;
    }

    public void setLegalView(String legalView) {
        this.legalView = legalView;
    }

    public String getLegalViewDD() {
        return "Company_legalView";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="companyOrPlan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC companyOrPlan;

    public UDC getCompanyOrPlan() {
        return companyOrPlan;
    }

    public void setCompanyOrPlan(UDC companyOrPlan) {
        this.companyOrPlan = companyOrPlan;
    }

    public String getCompanyOrPlanDD() {
        return "Company_companyOrPlan";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="glAccounts">
    @OneToMany(mappedBy = "company")
    private List<GLAccount> glAccounts;

    public List<GLAccount> getGlAccounts() {
        return glAccounts;
    }

    public void setGlAccounts(List<GLAccount> glAccounts) {
        this.glAccounts = glAccounts;
    }

    public String getGlAccountsDD() {
        return "Company_glAccounts";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="glMappedAccounts">
    @OneToMany(mappedBy = "company")
    private List<GLMappedAccount> glMappedAccounts;

    public List<GLMappedAccount> getGlMappedAccounts() {
        return glMappedAccounts;
    }

    public void setGlMappedAccounts(List<GLMappedAccount> glMappedAccounts) {
        this.glMappedAccounts = glMappedAccounts;
    }

    public String getGlMappedAccountsDD() {
        return "Company_glMappedAccounts";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="glSegments">
    @OneToMany(mappedBy = "company")
    private List<GLAccountSegment> glSegments;

    public List<GLAccountSegment> getGlSegments() {
        return glSegments;
    }

    public void setGlSegments(List<GLAccountSegment> glSegments) {
        this.glSegments = glSegments;
    }

    public String getGlSegmentsDD() {
        return "Company_glSegments";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="glAccUDCs">
    @OneToMany(mappedBy = "company")
    private List<GLAccountUDC> glAccUDCs;

    public List<GLAccountUDC> getGlAccUDCs() {
        return glAccUDCs;
    }

    public void setGlAccUDCs(List<GLAccountUDC> glAccUDCs) {
        this.glAccUDCs = glAccUDCs;
    }

    public String getGlAccUDCsDD() {
        return "Company_glAccUDCs";
    }
    //</editor-fold >
    // <editor-fold defaultstate="collapsed" desc="positions">
    @OneToMany(mappedBy = "company")
    private List<CompanyCostCenter> companyCostCenters;

    public List<CompanyCostCenter> getCompanyCostCenters() {
        return companyCostCenters;
    }

    public void setCompanyCostCenters(List<CompanyCostCenter> companyCostCenters) {
        this.companyCostCenters = companyCostCenters;
    }

    public String getCompanyCostCentersDD() {
        return "Company_companyCostCenters";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="investigations">
    @OneToMany(mappedBy = "company")
    private List<InvestigationSetup> investigations;

    public List<InvestigationSetup> getInvestigations() {
        return investigations;
    }

    public void setInvestigations(List<InvestigationSetup> investigations) {
        this.investigations = investigations;
    }

    public String getInvestigationsDD() {
        return "Company_investigations";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="netToGross">
    @OneToMany(mappedBy = "company")
    private List<NetToGross> netToGrosses;

    public List<NetToGross> getNetToGrosses() {
        return netToGrosses;
    }

    public void setNetToGrosses(List<NetToGross> netToGrosses) {
        this.netToGrosses = netToGrosses;
    }

    public String getNetToGrossesDD() {
        return "Company_netToGrosses";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="MedicalCategories">
    @OneToMany(mappedBy = "company")
    private List<MedicalCategories> medicalCategoriess;

    public List<MedicalCategories> getMedicalCategoriess() {
        return medicalCategoriess;
    }

    public String getMedicalCategoriessDD() {
        return "Company_medicalCategoriess";
    }

    public void setMedicalCategoriess(List<MedicalCategories> medicalCategoriess) {
        this.medicalCategoriess = medicalCategoriess;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="MedicalTreatmentTypes">
    @OneToMany(mappedBy = "company")
    private List<MedicalTreatmentTypes> medicalTreatmentTypess;

    public List<MedicalTreatmentTypes> getMedicalTreatmentTypess() {
        return medicalTreatmentTypess;
    }

    public String getMedicalTreatmentTypessDD() {
        return "Company_medicalTreatmentTypess";
    }

    public void setMedicalTreatmentTypess(List<MedicalTreatmentTypes> medicalTreatmentTypess) {
        this.medicalTreatmentTypess = medicalTreatmentTypess;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="investigationStats">
    @OneToMany(mappedBy = "company")
    private List<InvestigationStatus> investigationStats;

    public List<InvestigationStatus> getInvestigationStats() {
        return investigationStats;
    }

    public void setInvestigationStats(List<InvestigationStatus> investigationStats) {
        this.investigationStats = investigationStats;
    }

    public String getInvestigationStatsDD() {
        return "Company_investigationStats";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="projects">
    @OneToMany(mappedBy = "company")
    private List<Project> projects;

    public List<Project> getProjects() {
        return projects;
    }

    public String getProjectsDD() {
        return "Company_projects";
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="allowances">
    @OneToMany(mappedBy = "company")
    private List<Allowance> allowances;

    public List<Allowance> getAllowances() {
        return allowances;
    }

    public String getAllowancesDD() {
        return "Company_allowances";
    }

    public void setAllowances(List<Allowance> allowances) {
        this.allowances = allowances;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="item">
    @OneToMany
    private List<Item> item;

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    public String getItemDD() {
        return "Company_item";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="sapProjects">
    @OneToMany(mappedBy = "company")
    private List<SapProject> sapProjects;

    public List<SapProject> getSapProjects() {
        return sapProjects;
    }

    public void setSapProjects(List<SapProject> sapProjects) {
        this.sapProjects = sapProjects;
    }

    public String getSapProjectsDD() {
        return "Company_sapProjects";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="medicalProvider">
    @OneToMany(mappedBy = "company")
    private List<MedicalProvider> medicalProvider;

    public List<MedicalProvider> getMedicalProvider() {
        return medicalProvider;
    }

    public void setMedicalProvider(List<MedicalProvider> medicalProvider) {
        this.medicalProvider = medicalProvider;
    }

    public String getMedicalProviderDD() {
        return "Company_medicalProvider";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="trainingPlans">
    @OneToMany(mappedBy = "company")
    private List<ProviderTrainingPlan> trainingPlans;

    public List<ProviderTrainingPlan> getTrainingPlans() {
        return trainingPlans;
    }

    public void setTrainingPlans(List<ProviderTrainingPlan> trainingPlans) {
        this.trainingPlans = trainingPlans;
    }

    public String getTrainingPlansDD() {
        return "Company_trainingPlans";
    }
    //</editor-fold>
}
