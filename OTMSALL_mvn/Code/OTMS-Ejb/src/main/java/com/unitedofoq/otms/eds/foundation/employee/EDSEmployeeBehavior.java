/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.edsbehavior.EDSBehavior;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="employee")
public class EDSEmployeeBehavior extends BaseEntity{
    @Column(nullable=false)
    private double score;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private Employee employee;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSBehavior behavior;

    public String getScoreDD()       {  return "EDSEmployeeBehavior_score";  }

    public EDSBehavior getBehavior() {
        return behavior;
    }

    public void setBehavior(EDSBehavior behavior) {
        this.behavior = behavior;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
