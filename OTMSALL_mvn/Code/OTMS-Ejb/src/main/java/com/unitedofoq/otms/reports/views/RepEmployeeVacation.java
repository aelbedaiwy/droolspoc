package com.unitedofoq.otms.reports.views;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name="repemployeevacation")
public class RepEmployeeVacation extends RepEmpVacationAbsenceBase {

        //<editor-fold defaultstate="collapsed" desc="actualBalance">
    @Column(precision=18, scale=3)
    private BigDecimal actualBalance;

    public BigDecimal getActualBalance() {
        return actualBalance;
    }

    public void setActualBalance(BigDecimal actualBalance) {
        this.actualBalance = actualBalance;
    }

    public String getActualBalanceDD() {
        return "RepEmployeeVacation_actualBalance";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="adjustmentValue">
    private BigDecimal adjustmentValue;

    public BigDecimal getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(BigDecimal adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    public String getAdjustmentValueDD() {
        return "RepEmployeeVacation_adjustmentValue";
    }
    // </editor-fold>
}
