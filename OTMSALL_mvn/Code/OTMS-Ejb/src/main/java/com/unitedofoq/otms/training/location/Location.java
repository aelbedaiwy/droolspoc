package com.unitedofoq.otms.training.location;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.training.activity.Provider;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@ChildEntity(fields = {"exceptions", "costIems"})
public class Location extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Location_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "Instructor_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "Location_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cost">
    @Column(precision = 25, scale = 13)
    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCostDD() {
        return "Location_cost";
    }
    @Transient
    private BigDecimal costMask;

    public BigDecimal getCostMask() {
        costMask = cost;
        return costMask;
    }

    public void setCostMask(BigDecimal costMask) {
        updateDecimalValue("cost", costMask);
    }

    public String getCostMaskDD() {
        return "Location_costMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costPeriod">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC costPeriod;

    public UDC getCostPeriod() {
        return costPeriod;
    }

    public void setCostPeriod(UDC costPeriod) {
        this.costPeriod = costPeriod;
    }

    public String getCostPeriodDD() {
        return "Location_costPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="address">
    @Column(name = "address")
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDD() {
        return "Location_address";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitDD() {
        return "Location_unit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "Location_provider";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="seatsNumber">
    private int seatsNumber;

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public String getSeatsNumberDD() {
        return "Location_seatsNumber";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="telephone">
    @Column
    private String telephone;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephoneDD() {
        return "Location_telephone";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exceptions">
    @OneToMany(mappedBy = "location")
    private List<LocationException> exceptions;

    public List<LocationException> getExceptions() {
        return exceptions;
    }

    public void setExceptions(List<LocationException> exceptions) {
        this.exceptions = exceptions;
    }

    public String getExceptionsDD() {
        return "Location_exceptions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costIems">
    @OneToMany(mappedBy = "location")
    private List<LocationCostItem> costIems;

    public List<LocationCostItem> getCostIems() {
        return costIems;
    }

    public void setCostIems(List<LocationCostItem> costIems) {
        this.costIems = costIems;
    }

    public String getCostIemsDD() {
        return "Location_costIems";
    }
    // </editor-fold>
}
