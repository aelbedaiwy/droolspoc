/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.List;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author nkhalil
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ATYPE",
discriminatorType = DiscriminatorType.STRING)
@ParentEntity(fields={"employeeProfiler"})
@ChildEntity(fields={"edsCompetencyAssessments"})
public class EmployeeProfilerAssessor extends BaseEntity {
    @Transient
    private String assessorName;

    public String getAssessorNameDD() {
        return "EmployeeProfilerAssessor_assessorName";
    }

    public String getAssessorName() {
        if(this instanceof EmployeeProfilerAssessorConsultant)
            if(((EmployeeProfilerAssessorConsultant)this).getAssossr()!=null)
            return ((EmployeeProfilerAssessorConsultant)this).getAssossr().getConsultantPerson().getName();
        if(this instanceof EmployeeProfilerAssessorEmployee)
            if(((EmployeeProfilerAssessorEmployee)this).getAssess()!=null)
            //Salema[Emloyee New Design]   
            //return ((EmployeeProfilerAssessorEmployee)this).getAssess().getPersonalInfo().getName();
            return ((EmployeeProfilerAssessorEmployee)this).getAssess().getName(); 
        return assessorName;
    }

    public void setAssessorName(String assessorName) {
        this.assessorName = assessorName;
    }

    @OneToMany(mappedBy = "assessor")
    private List<EDSCompetencyAssessment> edsCompetencyAssessments;

    public List<EDSCompetencyAssessment> getEdsCompetencyAssessments() {
        return edsCompetencyAssessments;
    }

    public void setEdsCompetencyAssessments(List<EDSCompetencyAssessment> edsCompetencyAssessments) {
        this.edsCompetencyAssessments = edsCompetencyAssessments;
    }

   public String getEdsCompetencyAssessmentsDD() {
        return "EmployeeProfilerAssessor_edsCompetencyAssessments";
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="employeeProfiler_DBID", nullable=false)
    private EmployeeProfiler employeeProfiler;

    public String getEmployeeProfilerDD() {
        return "EmployeeProfilerAssessor_employeeProfiler";
    }

    public EmployeeProfiler getEmployeeProfiler() {
        return employeeProfiler;
    }

    public void setEmployeeProfiler(EmployeeProfiler employeeProfiler) {
        this.employeeProfiler = employeeProfiler;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC type;

    public String getTypeDD() {
        return "EmployeeProfilerAssessor_type";
    }

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }
}
