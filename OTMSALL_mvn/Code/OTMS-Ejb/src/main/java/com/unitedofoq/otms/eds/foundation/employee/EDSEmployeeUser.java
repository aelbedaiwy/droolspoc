/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue("EMPLOYEE1")
@ParentEntity(fields={"employee"})
@Table(name="EmployeeUser")
public class EDSEmployeeUser extends OUser {

    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="Employee_DBID")
	private EDSEmployee employee;

    public EDSEmployee getEmployee() {
        return employee;
    }

    public void setEmployee(EDSEmployee employee) {
        this.employee = employee;
    }

    boolean selfRoleEnabled = true;

    public boolean isSelfRoleEnabled() {
        return selfRoleEnabled;
    }

    public void setSelfRoleEnabled(boolean selfRoleEnabled) {
        this.selfRoleEnabled = selfRoleEnabled;
    }




    public String getSelfRoleEnabledDD() {
        return "EmployeeUser_selfRoleEnabled";
    }


    

}
