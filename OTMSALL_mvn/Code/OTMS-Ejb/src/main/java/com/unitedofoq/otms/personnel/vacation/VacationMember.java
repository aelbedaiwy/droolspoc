package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.personnel.vacationgroup.VacationMemberBase;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@ParentEntity(fields={"vacation"})
public class VacationMember extends VacationMemberBase{
    // <editor-fold defaultstate="collapsed" desc="vacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }
    public String getVacationDD() {
        return "VacationMember_vacation";
    }
   
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="validationExpression">
    @Column
    private String validationExpression;
    
    public String getValidationExpression() {
        return validationExpression;
    }

    public void setValidationExpression(String validationExpression) {
        this.validationExpression = validationExpression;
    }
    
    public String getValidationExpressionDD() {
        return "VacationMember_validationExpression";
    }
    // </editor-fold>
}