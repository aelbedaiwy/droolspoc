/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.penalty;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
public class EmployeePenaltyBalance extends BusinessObjectBaseEntity  {
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
     public String getEmployeeDD() {
        return "EmployeePenaltyBalance_employee";
    }

    @Column(nullable=false,precision=18,scale=3)
    private BigDecimal penaltyBalance;//(name = "penalty_balance")

    public BigDecimal getPenaltyBalance() {
        return penaltyBalance;
    }

    public void setPenaltyBalance(BigDecimal penaltyBalance) {
        this.penaltyBalance = penaltyBalance;
    }
     public String getPenaltyBalanceDD() {
        return "EmployeePenaltyBalance_penaltyBalance";
    }
    @Transient
    private BigDecimal penaltyBalanceMask;

    public BigDecimal getPenaltyBalanceMask() {
        penaltyBalanceMask = penaltyBalance ;
        return penaltyBalanceMask;
    }

    public void setPenaltyBalanceMask(BigDecimal penaltyBalanceMask) {
        updateDecimalValue("penaltyBalance",penaltyBalanceMask);
    }
    public String getPenaltyBalanceMaskDD() {
        return "EmployeePenaltyBalance_penaltyBalanceMask";
    }
}
