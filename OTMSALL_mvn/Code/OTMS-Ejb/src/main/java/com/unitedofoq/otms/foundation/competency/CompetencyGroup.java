package com.unitedofoq.otms.foundation.competency;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;

@Entity
@ParentEntity(fields={"company"})
@ChildEntity(fields={"competencies"})
//@ChildEntity(fields={"competency"})
public class CompetencyGroup extends CompetencyGroupBase{

   
}
