/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"unit"})
public class UnitHistory extends BaseEntity {
    
    private String action;
    
    private int sortIndex;
    
    private String hierarchyCode;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit parentUnit;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UnitLevelIndex unitLevelIndex;

    public String getAction() {
        return action;
    }
    
    public String getActionDD() {
        return "UnitHistory_action";
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHierarchyCode() {
        return hierarchyCode;
    }
    
    public String getHierarchyCodeDD() {
        return "UnitHistory_hierarchyCode";
    }

    public void setHierarchyCode(String hierarchyCode) {
        this.hierarchyCode = hierarchyCode;
    }

    public Unit getParentUnit() {
        return parentUnit;
    }
    
    public String getParentUnitDD() {
        return "UnitHistory_parentUnit";
    }

    public void setParentUnit(Unit parentUnit) {
        this.parentUnit = parentUnit;
    }

    public Unit getUnit() {
        return unit;
    }
    
    public String getUnitDD() {
        return "UnitHistory_unit";
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public UnitLevelIndex getUnitLevelIndex() {
        return unitLevelIndex;
    }
    
    public String getUnitLevelIndexDD() {
        return "UnitHistory_unitLevelIndex";
    }

    public void setUnitLevelIndex(UnitLevelIndex unitLevelIndex) {
        this.unitLevelIndex = unitLevelIndex;
    }

    public int getSortIndex() {
        return sortIndex;
    }
    
    public String getSortIndexDD() {
        return "UnitHistory_sortIndex";
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OUser user;

    public OUser getUser() {
        return user;
    }
    
    public String getUserDD() {
        return "UnitHistory_user";
    }

    public void setUser(OUser user) {
        this.user = user;
    }
    
    private String name;

    public String getName() {
        return name;
    }
    
    public String getNameDD() {
        return "UnitHistory_name";
    }

    public void setName(String name) {
        this.name = name;
    }
        
}
