package com.unitedofoq.otms.foundation.competency;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "CTYPE")
@DiscriminatorValue("MASTER")
public class CompetencyLevelBase extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "CompetencyLevelBase_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="behaviorIndicators">
    @Column(length=1000)
    private String behaviorIndicators;
    public String getBehaviorIndicatorsDD() {
        return "CompetencyLevelBase_behaviorIndicators";
    }
    public String getBehaviorIndicators() {
        return behaviorIndicators;
    }
    public void setBehaviorIndicators(String behaviorIndicators) {
        this.behaviorIndicators = behaviorIndicators;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column(nullable=false)
    private int sortIndex;
    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD()  {
     return "CompetencyLevelBase_sortIndex";  }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
     public String getName() {
        return name;
    }
    public String getNameTranslatedDD() {
        return "CompetencyLevelBase_nameTranslated";
    }
    public void setName(String name) {
        this.name = name;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
     public String getDescriptionTranslatedDD() {
        return "CompetencyLevel_description";
    }
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }
    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold> 
    
  
  
}
