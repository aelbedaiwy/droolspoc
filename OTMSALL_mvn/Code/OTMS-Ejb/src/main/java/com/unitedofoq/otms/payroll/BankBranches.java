/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.payroll;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author lahmed
 */
@Entity
@ParentEntity(fields="bank")
public class BankBranches extends BusinessObjectBaseEntity {
    //<editor-fold defaultstate="collapsed" desc="bank">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Bank bank;

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getBankDD() {
        return "BankBranches_bank";
    }
    //</editor-fold>

    /*
     //<editor-fold defaultstate="collapsed" desc="employeePayrolls">
    @OneToMany(mappedBy="branch")
    private List<EmployeePayrollBase> employeePayrolls;

    public List<EmployeePayrollBase> getEmployeePayrolls() {
        return employeePayrolls;
    }

    public void setEmployeePayrolls(List<EmployeePayrollBase> employeePayrolls) {
        this.employeePayrolls = employeePayrolls;
    }

    public String getEmployeePayrollsDD() {
        return "BankBraches_employeePayrolls";
    }
    //</editor-fold>
     *
     */

    //<editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "BankBranches_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    //</editor-fold>
}
