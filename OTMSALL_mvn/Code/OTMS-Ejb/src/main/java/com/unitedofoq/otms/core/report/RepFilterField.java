package com.unitedofoq.otms.core.report;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name = "RepFilterFields")
@NamedQueries(value = {
    @NamedQuery(name = "getFilterFields",
            query = " SELECT    repFilterField "
            + " FROM      RepFilterField repFilterField "
            + " WHERE     repFilterField.langId = :langDBID"
            + " AND       repFilterField.dsDBID = :dsDBID")})
public class RepFilterField implements Serializable {
    // <editor-fold defaultstate="collapsed" desc="fieldDBID">

    @Id
    private long fieldDBID;

    /**
     * @return the fieldDBID
     */
    public long getFieldDBID() {
        return fieldDBID;
    }

    /**
     * @param fieldDBID the fieldDBID to set
     */
    public void setFieldDBID(long fieldDBID) {
        this.fieldDBID = fieldDBID;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fieldExpression">
    private String fieldExpression;

    /**
     * @return the fieldExpression
     */
    public String getFieldExpression() {
        return fieldExpression;
    }

    /**
     * @param fieldExpression the fieldExpression to set
     */
    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyExpression">
    private String companyExpression;

    /**
     * @return the companyExpression
     */
    public String getCompanyExpression() {
        return companyExpression;
    }

    /**
     * @param companyExpression the companyExpression to set
     */
    public void setCompanyExpression(String companyExpression) {
        this.companyExpression = companyExpression;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="distinctFilterList">
    private boolean distinctFilterList;

    /**
     * @return the distinctFilterList
     */
    public boolean isDistinctFilterList() {
        return distinctFilterList;
    }

    /**
     * @param distinctFilterList the distinctFilterList to set
     */
    public void setDistinctFilterList(boolean distinctFilterList) {
        this.distinctFilterList = distinctFilterList;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="columnName">
    private String columnName;

    /**
     * @return the columnName
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * @param columnName the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="udcDBID">
    private long udcDBID;

    /**
     * @return the udcDBID
     */
    public long getUdcDBID() {
        return udcDBID;
    }

    /**
     * @param udcDBID the udcDBID to set
     */
    public void setUdcDBID(long udcDBID) {
        this.udcDBID = udcDBID;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oentityDBID">
    private long oentityDBID;

    /**
     * @return the oentityDBID
     */
    public long getOentityDBID() {
        return oentityDBID;
    }

    /**
     * @param oentityDBID the oentityDBID to set
     */
    public void setOentityDBID(long oentityDBID) {
        this.oentityDBID = oentityDBID;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="langId">
    private long langId;

    /**
     * @return the langId
     */
    public long getLangId() {
        return langId;
    }

    /**
     * @param langId the langId to set
     */
    public void setLangId(long langId) {
        this.langId = langId;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="oentityClassPath">
    private String oentityClassPath;

    /**
     * @return the oentityClassPath
     */
    public String getOentityClassPath() {
        return oentityClassPath;
    }

    /**
     * @param oentityClassPath the oentityClassPath to set
     */
    public void setOentityClassPath(String oentityClassPath) {
        this.oentityClassPath = oentityClassPath;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddLabel">
    private String ddLabel;

    /**
     * @return the ddLabel
     */
    public String getDdLabel() {
        return ddLabel;
    }

    /**
     * @param ddLabel the ddLabel to set
     */
    public void setDdLabel(String ddLabel) {
        this.ddLabel = ddLabel;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    private long dsDBID;

    /**
     * @return the dsDBID
     */
    public long getDsDBID() {
        return dsDBID;
    }

    /**
     * @param dsDBID the dsDBID to set
     */
    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }
    // </editor-fold>
}
