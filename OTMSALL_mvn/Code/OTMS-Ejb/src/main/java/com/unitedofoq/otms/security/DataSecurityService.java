package com.unitedofoq.otms.security;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.uiframework.screen.SingleEntityScreen;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.core.CustomDataSecurity;
import com.unitedofoq.otms.core.RoleDataSecurity;
import com.unitedofoq.otms.foundation.unit.Unit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.security.DataSecurityServiceLocal", beanInterface = DataSecurityServiceLocal.class)
public class DataSecurityService implements DataSecurityServiceLocal {

    @EJB
    private UIFrameworkServiceRemote uIFrameworkService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    DataTypeServiceRemote dataTypeService;

    @Override
    public OFunctionResult applySecurity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            SingleEntityScreen oscreen = (SingleEntityScreen) oDM.getData().get(3);
            OEntity entity = oscreen.getOactOnEntity();
            List initConditions = (List) oDM.getData().get(0);
            List returnedConditions;
            CustomDataSecurity customDataSecurity = (CustomDataSecurity) oem.loadEntity(CustomDataSecurity.class.getSimpleName(), Collections.singletonList("oscreen.dbid = " + oscreen.getDbid()), null, loggedUser);
            if (customDataSecurity == null) {
                List securityConditions;
                securityConditions = getDataSecurityCondition(entity, loggedUser);

                if (securityConditions != null && !securityConditions.isEmpty()) {
                    List appliedConditions;
                    appliedConditions = getAppliedScreenConditions(initConditions);
                    Class cls = Class.forName(entity.getEntityClassPath());
                    String entityName = cls.getSimpleName();
                    returnedConditions = getFinalAppliedSecurityCondition(entityName, securityConditions, appliedConditions);
                } else {
                    returnedConditions = initConditions;
                }
            } else {
                returnedConditions = initConditions;
            }

            OFunctionResult constructFR = uIFrameworkService.constructScreenDataLoadingUEConditionsOFR(returnedConditions, loggedUser);
            ofr.append(constructFR);
            ofr.setReturnedDataMessage(constructFR.getReturnedDataMessage());
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    @Override
    public OFunctionResult applySecurityByEmployee(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            SingleEntityScreen oscreen = (SingleEntityScreen) oDM.getData().get(3);
            OEntity entity = oscreen.getOactOnEntity();
            List initConditions = (List) oDM.getData().get(0);
            List<String> employeeDbidDataSecurityList = getEmployeeSecurityDbids(loggedUser);
            List returnedConditions;
            if (!employeeDbidDataSecurityList.isEmpty()) {
                List appliedConditions;
                appliedConditions = getAppliedScreenConditions(initConditions);
                if (appliedConditions.isEmpty()) {
                    appliedConditions = getDefaultAppliedCondition();
                }
                String employeeDbids = employeeDbidDataSecurityList.toString().replace("[", "(").replace("]", ")");
                List securityConditions = new ArrayList();
                if (entity.getTitle().equals("EmployeeTicket")) {
                    securityConditions.add("e.requester.dbid in " + employeeDbids);
                } else {
                    securityConditions.add("e.employee.dbid in " + employeeDbids);
                }
                Class cls = Class.forName(entity.getEntityClassPath());
                String entityName = cls.getSimpleName();
                returnedConditions = getFinalAppliedSecurityCondition(entityName, securityConditions, appliedConditions);
            } else {
                returnedConditions = initConditions;
            }

            OFunctionResult constructFR = uIFrameworkService.constructScreenDataLoadingUEConditionsOFR(returnedConditions, loggedUser);
            ofr.append(constructFR);
            ofr.setReturnedDataMessage(constructFR.getReturnedDataMessage());

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    @Override
    public OFunctionResult assignUserExitToEntity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            RoleDataSecurity roleDataSecurity = (RoleDataSecurity) oDM.getData().get(0);
            assignUserExit(roleDataSecurity, loggedUser);
            ofr.addSuccess(userMessageServiceRemote.getUserMessage("Success", loggedUser));
        } catch (Exception e) {
            ofr.addError(userMessageServiceRemote.getUserMessage("Internal Error", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult updateUserExitToEntity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            RoleDataSecurity roleDataSecurity = (RoleDataSecurity) oDM.getData().get(0);
            ArrayList cond = new ArrayList();
            cond.add("dbid = " + roleDataSecurity.getDbid());
            RoleDataSecurity previousDataSecurity = (RoleDataSecurity) oem.loadEntity(RoleDataSecurity.class.getSimpleName(), cond, null, loggedUser);
            if (previousDataSecurity.getOentity().getDbid() != roleDataSecurity.getOentity().getDbid()) {
                if (getRolesCountOnEntity(previousDataSecurity.getOentity(), loggedUser) <= 1) {
                    deleteUserExit(previousDataSecurity, loggedUser);
                }
                assignUserExit(roleDataSecurity, loggedUser);
                ofr.addSuccess(userMessageServiceRemote.getUserMessage("Success", loggedUser));
            }
        } catch (Exception e) {
            ofr.addError(userMessageServiceRemote.getUserMessage("Internal Error", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult deleteUserExitToEntity(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            RoleDataSecurity roleDataSecurity = (RoleDataSecurity) oDM.getData().get(0);
            deleteUserExit(roleDataSecurity, loggedUser);
            ofr.addSuccess(userMessageServiceRemote.getUserMessage("Success", loggedUser));

        } catch (Exception e) {
            ofr.addError(userMessageServiceRemote.getUserMessage("Internal Error", loggedUser));
        }
        return ofr;
    }

    @Override
    public List<String> getVacationSecurityDbids(OUser loggedUser) {
        List<String> dbids = new ArrayList<String>();
        try {
            List securityConditions;
            securityConditions = getVacationDataSecurityCondition(loggedUser);
            List returnedConditions = getFinalAppliedSecurityCondition("Vacation", securityConditions, getDefaultAppliedCondition());
            dbids = getSecurityDbids("Vacation", returnedConditions, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return dbids;
    }

    @Override
    public List<String> getEmployeeSecurityDbids(OUser loggedUser) {
        List<String> dbids = new ArrayList<String>();
        try {
            List securityConditions;
            securityConditions = getEmployeeDataSecurityCondition(loggedUser);
            List returnedConditions = getFinalAppliedSecurityCondition("Employee", securityConditions, getDefaultAppliedCondition());
            dbids = getSecurityDbids("Employee", returnedConditions, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return dbids;
    }

    @Override
    public List<String> getPositionSecurityDbids(OUser loggedUser) {
        List<String> dbids = new ArrayList<String>();
        try {
            List securityConditions;
            securityConditions = getPositionDataSecurityCondition(loggedUser);
            List defaultAppliedConditions;
            defaultAppliedConditions = getDefaultAppliedCondition();
            List returnedConditions = getFinalAppliedSecurityCondition("Position", securityConditions, defaultAppliedConditions);
            dbids = getSecurityDbids("Position", returnedConditions, loggedUser);

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return dbids;
    }

    @Override
    public List<String> getUnitSecurityDbids(OUser loggedUser) {
        List<String> dbids = new ArrayList<String>();
        try {
            List securityConditions;
            securityConditions = getUnitDataSecurityCondition(loggedUser);
            List returnedConditions = getFinalAppliedSecurityCondition("Unit", securityConditions, getDefaultAppliedCondition());
            dbids = getSecurityDbids("Unit", returnedConditions, loggedUser);

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return dbids;
    }

    private List<RoleDataSecurity> getUserDataSecurity(OEntity entity, OUser loggedUser) {
        List<RoleDataSecurity> roleDataSecurities = null;
        try {
            Long entityDbid = (entity == null) ? Long.valueOf("0") : entity.getDbid();
            List objs = getUserDataSecurity(entityDbid, loggedUser);
            if (!objs.isEmpty()) {
                String dbids = objs.toString();
                dbids = dbids.replace("[", "(");
                dbids = dbids.replace("]", ")");
                List conditions = new ArrayList();
                conditions.add("dbid in " + dbids);
                List sorts = new ArrayList();
                sorts.add("sortIndex asc");
                sorts.add("oentity.title asc");
                roleDataSecurities = oem.loadEntityList("RoleDataSecurity", conditions, null, sorts, loggedUser);
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return roleDataSecurities;

    }

    private List<RoleDataSecurity> getUserDataSecurity(OUser loggedUser) {
        List<RoleDataSecurity> roleDataSecurities = null;
        try {
            OEntity entity = null;
            roleDataSecurities = getUserDataSecurity(entity, loggedUser);

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return roleDataSecurities;

    }

    private List<Object> getUserDataSecurity(Long entitydbid, OUser loggedUser) {
        List objs = null;
        try {
            String sql;
            sql = "Select RDS.dbid"
                    + " From roleuser RU , RoleDataSecurity RDS"
                    + " Where RU.OROLE_DBID = RDS.role_dbid "
                    + " AND RU.OUSER_DBID =" + loggedUser.getDbid();
            if (!entitydbid.equals(Long.valueOf(0))) {
                sql += " AND RDS.oentity_dbid = " + entitydbid;
            }
            objs = oem.executeEntityListNativeQuery(sql, loggedUser);

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return objs;

    }

    private List<OEntity> getEntityListForUser(OUser loggedUser) {
        List<OEntity> entitys = new ArrayList<>();
        try {
            String sql = "SELECT distinct RDS.oentity_dbid"
                    + " FROM roledatasecurity RDS"
                    + ", roleuser RU"
                    + " WHERE RDS.role_dbid = RU.ORole_DBID"
                    + " AND RU.OUSER_DBID = " + loggedUser.getDbid();
            List list = oem.executeEntityListNativeQuery(sql, loggedUser);
            if (!list.isEmpty()) {
                String dbids = getDbidFromList(list);
                ArrayList cond = new ArrayList();
                cond.add("dbid in " + dbids);
                entitys = oem.loadEntityList(OEntity.class.getSimpleName(), cond, null, null, loggedUser);
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return entitys;
    }

    private String getDbidFromList(List list) {
        String resString = list.toString();
        resString = resString.replace("[", "(");
        resString = resString.replace("]", ")");
        return resString;
    }

    @Override
    public Map<String, List<String>> getDataSecurityCondition(OUser loggedUser) {
        Map<String, List<String>> resConditions = new HashMap<>();
        List<String> conditions = new ArrayList<>();
        try {
            List<OEntity> entitys = getEntityListForUser(loggedUser);
            if (!entitys.isEmpty()) {
                for (int i = 0; i < entitys.size(); i++) {
                    OEntity entity = entitys.get(i);
                    List<RoleDataSecurity> dataSecurities = getUserDataSecurity(entity, loggedUser);
                    String entityName = entity.getEntityClassPath();
                    conditions = new ArrayList<>();
                    if (!dataSecurities.isEmpty()) {
                        List cond = getDataSecurityCondition(dataSecurities, loggedUser);
                        entityName = entityName.substring(entityName.lastIndexOf(".") + 1);
                        cond = getFinalAppliedSecurityCondition(entityName, cond, getDefaultAppliedCondition());
                        cond = getSecurityDbids(entityName, cond, loggedUser);
                        if (!cond.isEmpty()) {
                            conditions.add(getDbidFromList(cond));
                        }
                    }
                    resConditions.put(entityName, conditions);
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return resConditions;
    }

    private List<String> getDataSecurityCondition(OEntity entity, OUser loggedUser) {
        List<String> conditions = null;
        try {
            List<RoleDataSecurity> dataSecurities = getUserDataSecurity(entity, loggedUser);
            if (dataSecurities != null && !dataSecurities.isEmpty()) {
                conditions = getDataSecurityCondition(dataSecurities, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return conditions;

    }

    private List<String> getDataSecurityCondition(List<RoleDataSecurity> dataSecurities, OUser loggedUser) {
        List<String> conditions = new ArrayList();
        try {
            if (dataSecurities != null && !dataSecurities.isEmpty()) {
                String fieldExpression, value;
                String startCondition, endCondition;
                String condition, conditionalOperator, logicalOperator;
                int securitiesSize = dataSecurities.size(), i = 0;
                for (RoleDataSecurity dataSecurity : dataSecurities) {
                    fieldExpression = "e." + dataSecurity.getFieldExpression();
                    conditionalOperator = dataSecurity.getConditionalOperator();
                    startCondition = dataSecurity.getStartCondition() == null ? "(" : dataSecurity.getStartCondition();
                    endCondition = dataSecurity.getEndCondition() == null ? ")" : dataSecurity.getEndCondition();
                    if (i == securitiesSize - 1) {
                        logicalOperator = "";
                    } else {
                        logicalOperator = dataSecurity.getLogicalOperator() == null ? "" : dataSecurity.getLogicalOperator();
                    }
                    value = dataSecurity.getValue();
                    if (conditionalOperator.equalsIgnoreCase("in") || conditionalOperator.equalsIgnoreCase("not in")) {
                        String tmp = value;
                        value = "";
                        String newValue = "";
                        while (tmp.contains(",")) {
                            value = tmp.substring(0, tmp.indexOf(","));
                            newValue += "'" + value + "',";
                            tmp = tmp.substring(value.length() + 1);
                        }
                        newValue += "'" + tmp + "'";
                        value = "(" + newValue + ")";
                    } else {
                        value = " '" + value + "' ";
                    }
                    condition = startCondition + fieldExpression + " " + conditionalOperator + value + endCondition + " " + logicalOperator;
                    conditions.add(condition);
                    i++;
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return conditions;
    }

    private List<String> getEmployeeDataSecurityCondition(OUser loggedUser) {
        List<String> conditions = null;
        try {
            OEntity entity = getEmployeeOEntity(loggedUser);
            conditions = getDataSecurityCondition(entity, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return conditions;

    }

    private List<String> getVacationDataSecurityCondition(OUser loggedUser) {
        List<String> conditions = null;
        try {
            OEntity entity = getVacationOEntity(loggedUser);
            conditions = getDataSecurityCondition(entity, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return conditions;

    }

    private List<String> getUnitDataSecurityCondition(OUser loggedUser) {
        List<String> conditions = null;
        try {
            OEntity entity = getUnitOEntity(loggedUser);
            List<RoleDataSecurity> dataSecurities = getUserDataSecurity(entity, loggedUser);
            if (dataSecurities != null && !dataSecurities.isEmpty()) {
                conditions = getDataSecurityCondition(dataSecurities, loggedUser);
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return conditions;

    }

    private List<String> getPositionDataSecurityCondition(OUser loggedUser) {
        List<String> conditions = new ArrayList();
        try {
            OEntity entity = getPositionOEntity(loggedUser);
            List<RoleDataSecurity> dataSecurities = getUserDataSecurity(entity, loggedUser);
            if (dataSecurities != null && !dataSecurities.isEmpty()) {
                conditions = getDataSecurityCondition(dataSecurities, loggedUser);
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return conditions;

    }

    private List<String> getAppliedScreenConditions(List initConditions) {
        List<String> appliedConditions = new ArrayList();
        if (initConditions != null && !initConditions.isEmpty()) {
            for (Object obj : initConditions) {
                if (!(obj.toString().equals("##ACTIVEANDINACTIVE") || obj.toString().equals("##AB"))) {
                    appliedConditions.add(obj.toString());
                }
            }
        }
        return appliedConditions;
    }

    private List<String> getFinalAppliedSecurityCondition(String entityName, List<String> securityConditions, List<String> appliedConditions) {
        List returnedConditions = new ArrayList();
        if (securityConditions != null && !securityConditions.isEmpty()) {
            String sql;
            int i = 0;
            sql = "dbid in (Select e.dbid From " + entityName + " e Where (";
            for (Object obj : securityConditions) {
                sql += obj.toString();
            }
            sql += ")";
            if (appliedConditions != null && !appliedConditions.isEmpty()) {
                sql += " AND (";
                i = 0;
                for (Object obj : appliedConditions) {
                    if (obj != null) {
                        String value = obj.toString().trim();
                        if (value.toUpperCase().contains("LIKE")) {
                            sql += "(LOWER(e." + value.replace("LIKE '", ") LIKE LOWER('") + "))";
                        } else {
                            sql += "(e." + value + ")";
                        }
                        if (!(i == appliedConditions.size() - 1)) {
                            sql += " AND ";
                        }
                        i++;
                    }
                }
                sql += " )";
            }
            sql += ")";
            returnedConditions.add(sql);
        }
        return returnedConditions;
    }

    private List<String> getSecurityDbids(String entityName, List<String> securityConditions, OUser loggedUser) {
        List dbids = new ArrayList();
        try {
            if (securityConditions != null && !securityConditions.isEmpty()) {
                String sql;
                sql = "Select e.dbid From " + entityName + " e Where ";
                int size = securityConditions.size();
                int i = 0;
                for (String condition : securityConditions) {
                    sql += "e." + condition;
                    if (i != size - 1) {
                        sql += " AND ";
                    }
                    i++;
                }
                dbids = oem.executeEntityListQuery(sql, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return dbids;
    }

    private OEntity getEmployeeOEntity(OUser loggedUser) {
        OEntity entity = null;
        try {
            String classPath = getEmployeeEntityClassPath();
            entity = getOEntityByClassPath(classPath, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return entity;
    }

    private OEntity getVacationOEntity(OUser loggedUser) {
        OEntity entity = null;
        try {
            String classPath = getVacationEntityClassPath();
            entity = getOEntityByClassPath(classPath, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return entity;
    }

    private String getEmployeeEntityClassPath() {
        String clssPath = "com.unitedofoq.otms.foundation.employee.Employee";
        return clssPath;
    }

    private String getVacationEntityClassPath() {
        String clssPath = "com.unitedofoq.otms.personnel.vacation.Vacation";
        return clssPath;
    }

    private OEntity getPositionOEntity(OUser loggedUser) {
        OEntity entity = null;
        try {
            String classPath = getPositionEntityClassPath();
            entity = getOEntityByClassPath(classPath, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return entity;
    }

    private String getPositionEntityClassPath() {
        String clssPath = "com.unitedofoq.otms.foundation.position.Position";
        return clssPath;
    }

    private OEntity getUnitOEntity(OUser loggedUser) {
        OEntity entity = null;
        try {
            String classPath = getUnitEntityClassPath();
            entity = getOEntityByClassPath(classPath, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return entity;
    }

    private String getUnitEntityClassPath() {
        String clssPath = "com.unitedofoq.otms.foundation.unit.Unit";
        return clssPath;
    }

    private OEntity getOEntityByClassPath(String classPath, OUser loggedUser) {
        OEntity entity = null;
        try {
            List conditions = new ArrayList();
            conditions.add("entityClassPath = '" + classPath + "'");
            entity = (OEntity) oem.loadEntity("OEntity", conditions, null, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return entity;
    }

    private List getDefaultAppliedCondition() {
        List defaultAppliedConditions = new ArrayList();
        defaultAppliedConditions.add("inActive = false");
        return defaultAppliedConditions;
    }

    private void assignUserExit(RoleDataSecurity roleDataSecurity, OUser loggedUser) {
        try {
            OEntity entity = roleDataSecurity.getOentity();

            String sql = "SELECT DBID FROM javafunction WHERE FUNCTIONNAME in (SELECT VALUEDATA FROM payrollparameter WHERE description = 'ApplySecurityFunctionCode')";
            Object javaFunctionDbid = oem.executeEntityNativeQuery(sql, loggedUser);

            sql = "UPDATE OEntity SET DATALOADINGUSEREXIT_dbid = " + javaFunctionDbid + " WHERE DBID = " + entity.getDbid();
            oem.executeEntityUpdateNativeQuery(sql, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private void deleteUserExit(RoleDataSecurity roleDataSecurity, OUser loggedUser) {
        OEntity entity = roleDataSecurity.getOentity();
        if (getRolesCountOnEntity(entity, loggedUser) <= 1) {
            String sql = "UPDATE OEntity SET DATALOADINGUSEREXIT_dbid = null WHERE DBID = " + entity.getDbid();
            try {
                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
            } catch (Exception ex) {
                OLog.logException(ex, loggedUser);
            }
        }
    }

    private int getRolesCountOnEntity(OEntity entity, OUser loggedUser) {
        try {
            String sql = "SELECT COUNT(DBID) FROM RoleDataSecurity WHERE oentity_dbid = " + entity.getDbid();
            Object result = oem.executeEntityNativeQuery(sql, loggedUser);
            if (result != null) {
                return Integer.valueOf(result.toString());
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return 0;
    }

    @Override
    public OFunctionResult applySecurityOnUnitTreeScreen(ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List unitDbidList = getUnitSecurityDbids(loggedUser);
            List initConditions = (List) oDM.getData().get(0);
            if (!unitDbidList.isEmpty()) {
                Unit rootUnit = (Unit) oem.loadEntity(Unit.class.getSimpleName(), Collections.singletonList("parentUnit is null"), null, loggedUser);
                StringBuilder finalSecurityDbids = new StringBuilder("dbid in (");
                finalSecurityDbids.append(rootUnit.getDbid());
                for (Object unitDbid : unitDbidList) {
                    Unit selectedUnit = (Unit) oem.loadEntity(Unit.class.getSimpleName(), Collections.singletonList("dbid = " + unitDbid), null, loggedUser);
                    while (selectedUnit.getParentUnit() != null) {
                        finalSecurityDbids.append(", ").append(selectedUnit.getDbid());
                        selectedUnit = selectedUnit.getParentUnit();
                    }
                }
                finalSecurityDbids.append(")");
                if (initConditions == null) {
                    initConditions = new ArrayList();
                }
                initConditions.add(finalSecurityDbids.toString());
            }
            OFunctionResult constructFR = uIFrameworkService.constructScreenDataLoadingUEConditionsOFR(initConditions, loggedUser);
            ofr.append(constructFR);
            ofr.setReturnedDataMessage(constructFR.getReturnedDataMessage());
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }

}
