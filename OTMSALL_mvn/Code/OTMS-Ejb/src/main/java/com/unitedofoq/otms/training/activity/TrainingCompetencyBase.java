/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import com.unitedofoq.otms.foundation.competency.CompetencyLevel;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "COMPETENCYTYPE")
public class TrainingCompetencyBase extends BusinessObjectBaseEntity{
    // <editor-fold defaultstate="collapsed" desc="competency">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }

    public String getCompetencyDD() {
        return "TrainingCompetencyBase_competency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumLevel">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private CompetencyLevel maximumLevel;
    public CompetencyLevel getMaximumLevel() {
        return maximumLevel;
    }
    public void setMaximumLevel(CompetencyLevel maximumLevel) {
        this.maximumLevel = maximumLevel;
    }
    public String getMaximumLevelDD() {
        return "TrainingCompetencyBase_maximumLevel";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumLevel">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private CompetencyLevel minimumLevel;
    public CompetencyLevel getMinimumLevel() {
        return minimumLevel;
    }
    public void setMinimumLevel(CompetencyLevel minimumLevel) {
        this.minimumLevel = minimumLevel;
    }
    public String getMinLevelDD() {
        return "TrainingCompetencyBase_minimumLevel";
    }
    // </editor-fold>
}