package com.unitedofoq.otms.payroll.tax;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * The persistent class for the tax_breaks database table.
 * 
 */
@Entity
@ParentEntity(fields={"tax"})
public class TaxBreak extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="tax">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Tax tax;
    public Tax getTax() {
        return tax;
    }
    public void setTax(Tax tax) {
        this.tax = tax;
    }
    public String getTaxDD() {
        return "TaxBreak_tax";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }
    public String getMaximumValueDD() {
        return "TaxBreak_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue",maximumValueMask);
    }
    public String getMaximumValueMaskDD() {
        return "TaxBreak_maximumValueMask";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minValue">
    @Column(precision=25, scale=13)
    private BigDecimal minValue;
    public BigDecimal getMinValue() {
        return minValue;
    }
    public void setMinValue(BigDecimal minimumValue) {
        this.minValue = minimumValue;
    }
    public String getMinValueDD() {
        return "TaxBreak_minValue";
    }
    @Transient
    private BigDecimal minValueMask;
    public BigDecimal getMinValueMask() {
        minValueMask = minValue ;
        return minValueMask;
    }
    public void setMinValueMask(BigDecimal minValueMask) {
        updateDecimalValue("minValue",minValueMask);
    }
    public String getMinValueMaskDD() {
        return "TaxBreak_minValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="previousBreakValue">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal previousBreakValue;

    public BigDecimal getPreviousBreakValue() {
        return previousBreakValue;
    }

    public void setPreviousBreakValue(BigDecimal previousBreakValue) {
        this.previousBreakValue = previousBreakValue;
    }

    public String getPreviousBreakValueDD() {
        return "TaxBreak_previousBreakValue";
    }
    @Transient
    private BigDecimal previousBreakValueMask;

    public BigDecimal getPreviousBreakValueMask() {
        previousBreakValueMask = previousBreakValue ;
        return previousBreakValueMask;
    }

    public void setPreviousBreakValueMask(BigDecimal previousBreakValueMask) {
        updateDecimalValue("previousBreakValue",previousBreakValueMask);
    }

    public String getPreviousBreakValueMaskDD() {
        return "TaxBreak_previousBreakValueMask";
    }
	
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
	@Column(nullable=false)
	private int sortIndex;
    public int getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
    public String getSortIndexDD() {
        return "TaxBreak_sortIndex";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxPercent">
    @Column(precision=25, scale=13, nullable=false)
    private BigDecimal taxPercent;
    public BigDecimal getTaxPercent() {
        return taxPercent;
    }
    public void setTaxPercent(BigDecimal taxPercent) {
        this.taxPercent = taxPercent;
    }
    public String getTaxPercentDD() {
        return "TaxBreak_taxPercent";
    }
    @Transient
    private BigDecimal taxPercentMask;
    public BigDecimal getTaxPercentMask() {
        taxPercentMask = taxPercent;
        return taxPercentMask;
    }
    public void setTaxPercentMask(BigDecimal taxPercentMask) {
        updateDecimalValue("taxPercent",taxPercentMask);
    }
    public String getTaxPercentMaskDD() {
        return "TaxBreak_taxPercentMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factor1">
    @Column(precision=25, scale=13)
    private BigDecimal factor1;

    public BigDecimal getFactor1() {
        return factor1;
    }
    public String getFactor1DD() {
        return "TaxBreak_factor1";
    }

    public void setFactor1(BigDecimal factor1) {
        this.factor1 = factor1;
    }
    
    @Transient
    private BigDecimal factor1Mask;

    public BigDecimal getFactor1Mask() {
        factor1Mask = factor1;
        return factor1Mask;
    }
    public String getFactor1MaskDD() {
        return "TaxBreak_factor1Mask";
    }

    public void setFactor1Mask(BigDecimal factor1Mask) {
        updateDecimalValue("factor1", factor1Mask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factor2">
    @Column(precision=25, scale=13)
    private BigDecimal factor2;

    public BigDecimal getFactor2() {
        return factor2;
    }
    public String getFactor2DD() {
        return "TaxBreak_factor2";
    }

    public void setFactor2(BigDecimal factor2) {
        this.factor2 = factor2;
    }
    
    @Transient
    private BigDecimal factor2Mask;

    public BigDecimal getFactor2Mask() {
        factor2Mask = factor2;
        return factor2Mask;
    }
    public String getFactor2MaskDD() {
        return "TaxBreak_factor2Mask";
    }

    public void setFactor2Mask(BigDecimal factor2Mask) {
        updateDecimalValue("factor2", factor2Mask);
    }
    // </editor-fold>
}