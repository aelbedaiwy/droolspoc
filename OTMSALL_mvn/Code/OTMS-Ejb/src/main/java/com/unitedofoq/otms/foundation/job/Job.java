/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.job;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.jobGroup.JobGroup;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.payroll.PaymentMethod;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.payroll.paygrade.PayGradeStep;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@DiscriminatorValue("MASTER")
@ParentEntity(fields="company")
@ChildEntity(fields={"responsibilities","tasks","trainings","certificates",
"educations","competencies","beheviours","values","jobCertificatesNew"})
public class Job extends JobBase {
    // <editor-fold defaultstate="collapsed" desc="positions">
    @OneToMany(mappedBy="job")
	private List<Position> positions;

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }
    public String getPositionsDD() {
        return "Job_positions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "Job_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tasks">
	@OneToMany(mappedBy="job")
	private List<JobTask> tasks;

    public List<JobTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<JobTask> tasks) {
        this.tasks = tasks;
    }
   
    public String getTasksDD() {
        return "Job_tasks";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="paymentMethod">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PaymentMethod paymentMethod;
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }
    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    public String getPaymentMethodDD() {
        return "Job_paymentMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGrade">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade payGrade;
    public PayGrade getPayGrade() {
        return payGrade;
    }
    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }
    public String getPayGradeDD() {
        return "Job_payGrade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGradeStep">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGradeStep payGradeStep;
    public PayGradeStep getPayGradeStep() {
        return payGradeStep;
    }
    public void setPayGradeStep(PayGradeStep payGradeStep) {
        this.payGradeStep = payGradeStep;
    }
    public String getPayGradeStepDD() {
        return "Job_payGradeStep";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="rights">
	@Column
    private String rights;
    public String getRights() {
        return rights;
    }
    public void setRights(String rights) {
        this.rights = rights;
    }
    public String getRightsDD() {
        return "Job_rights";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumWorkDuration">
    @Column(precision=25, scale=13)
    private BigDecimal minimumWorkDuration;

    public BigDecimal getMinimumWorkDuration() {
        return minimumWorkDuration;
    }

    public void setMinimumWorkDuration(BigDecimal minimumWorkDuration) {
        this.minimumWorkDuration = minimumWorkDuration;
    }
    
    public String getMinimumWorkDurationDD() {
        return "Job_minimumWorkDuration";
    }
    
    @Transient
    private BigDecimal minimumWorkDurationMask = java.math.BigDecimal.ZERO;
    public BigDecimal getMinimumWorkDurationMask() {
        minimumWorkDurationMask = minimumWorkDuration ;
        return minimumWorkDurationMask;
    }
    public void setMinimumWorkDurationMask(BigDecimal minimumWorkDurationMask) {
        updateDecimalValue("minimumWorkDuration",minimumWorkDurationMask);
    }
    public String getMinimumWorkDurationMaskDD() {
        return "Job_minimumWorkDurationMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumWorkDuration">
    @Column(precision=25, scale=13)
    private BigDecimal maximumWorkDuration;

    public BigDecimal getMaximumWorkDuration() {
        return maximumWorkDuration;
    }

    public void setMaximumWorkDuration(BigDecimal maximumWorkDuration) {
        this.maximumWorkDuration = maximumWorkDuration;
    }

   public String getMaximumWorkDurationDD() {
        return "Job_maximumWorkDuration";
    }
   
    @Transient
    private BigDecimal maximumWorkDurationMask = java.math.BigDecimal.ZERO;
    public BigDecimal getMaximumWorkDurationMask() {
        maximumWorkDurationMask = maximumWorkDuration ;
        return maximumWorkDurationMask;
    }
    public void setMaximumWorkDurationMask(BigDecimal maximumWorkDurationMask) {
        updateDecimalValue("maximumWorkDuration",maximumWorkDurationMask);
    }
    public String getMaximumWorkDurationMaskDD() {
        return "Job_maximumWorkDurationMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workDurationMeasuerUnit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC workDurationMeasuerUnit;
    public String getWorkDurationMeasuerUnitDD() {
            return "Job_workDurationMeasuerUnit";
    }
    public UDC getWorkDurationMeasuerUnit() {
         return workDurationMeasuerUnit;
    }
    public void setWorkDurationMeasuerUnit(UDC workDurationMeasuerUnit) {
            this.workDurationMeasuerUnit = workDurationMeasuerUnit;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="keyJob">
    //ibr 10/10/2011 12:00
    @Column
    //ibr 10/10/2011 12:03
    private boolean keyJob;

    public boolean isKeyJob() {
        return keyJob;
    }

    public void setKeyJob(boolean keyJob) {
        this.keyJob = keyJob;
    }
    
    public String getKeyJobDD() {
        return "Job_keyJob";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainings">
	@OneToMany(mappedBy="job")
	private List<JobTraining> trainings;

    public List<JobTraining> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<JobTraining> trainings) {
        this.trainings = trainings;
    }

    
    public String getTrainingsDD() {
        return "Job_trainings";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certificates">
	@OneToMany(mappedBy="job")
	private List<JobCertificate> certificates;

    public List<JobCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<JobCertificate> certificates) {
        this.certificates = certificates;
    }
   
    public String getCertificatesDD() {
        return "Job_certificates";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="educations">
	@OneToMany(mappedBy="job")
	private List<JobEducation> educations;

    public List<JobEducation> getEducations() {
        return educations;
    }

    public void setEducations(List<JobEducation> educations) {
        this.educations = educations;
    }
    
    public String getEducationsDD() {
        return "Job_educations";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competencies">
	@OneToMany(mappedBy="job")
	private List<JobCompetency> competencies;

    public List<JobCompetency> getCompetencies() {
        return competencies;
    }

    public void setCompetencies(List<JobCompetency> competencies) {
        this.competencies = competencies;
    }
    
    public String getCompetenciesDD() {
        return "Job_competencies";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="beheviours">
	@OneToMany(mappedBy="job")
	private List<JobBeheviour> beheviours;

    public List<JobBeheviour> getBeheviours() {
        return beheviours;
    }

    public void setBeheviours(List<JobBeheviour> beheviours) {
        this.beheviours = beheviours;
    }
   
    public String getBehevioursDD() {
        return "Job_beheviours";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="values">
	@OneToMany(mappedBy="job")
	private List<JobValue> values;

    public List<JobValue> getValues() {
        return values;
    }

    public void setValues(List<JobValue> values) {
        this.values = values;
    }
   
    public String getValuesDD() {
        return "Job_values";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumExperience">
   @Column(precision=25, scale=13)
   private BigDecimal minimumExperience;

    public BigDecimal getMinimumExperience() {
        return minimumExperience;
    }

    public void setMinimumExperience(BigDecimal minimumExperience) {
        this.minimumExperience = minimumExperience;
    }
   
   public String getMinimumExperienceDD() {
        return "Job_minimumExperience";
    }
   
    @Transient
    private BigDecimal minimumExperienceMask = java.math.BigDecimal.ZERO;
    public BigDecimal getMinimumExperienceMask() {
        minimumExperienceMask = minimumExperience ;
        return minimumExperienceMask;
    }
    public void setMinimumExperienceMask(BigDecimal minimumExperienceMask) {
        updateDecimalValue("minimumExperience",minimumExperienceMask);
    }
    public String getMinimumExperienceMaskDD() {
        return "Job_minimumExperienceMask";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumExperience">
   @Column(precision=25, scale=13)
   private BigDecimal maximumExperience;

    public BigDecimal getMaximumExperience() {
        return maximumExperience;
    }

    public void setMaximumExperience(BigDecimal maximumExperience) {
        this.maximumExperience = maximumExperience;
    }
  
   public String getMaximumExperienceDD() {
        return "Job_maximumExperience";
    }
   
    @Transient
    private BigDecimal maximumExperienceMask = java.math.BigDecimal.ZERO;
    public BigDecimal getMaximumExperienceMask() {
        maximumExperienceMask = maximumExperience ;
        return maximumExperienceMask;
    }
    public void setMaximumExperienceMask(BigDecimal maximumExperienceMask) {
        updateDecimalValue("maximumExperience",maximumExperienceMask);
    }
    public String getMaximumExperienceMaskDD() {
        return "Job_maximumExperienceMask";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="experienceMeasureUnit">
   @JoinColumn
   @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
   private UDC experienceMeasureUnit;

    public UDC getExperienceMeasureUnit() {
        return experienceMeasureUnit;
    }

    public void setExperienceMeasureUnit(UDC experienceMeasureUnit) {
        this.experienceMeasureUnit = experienceMeasureUnit;
    }

    public String getExperienceMeasureUnitDD() {
        return "Job_experienceMeasureUnit";
    }
   
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="jobGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private JobGroup jobGroup;
    public JobGroup getJobGroup() {
        return jobGroup;
    }
    public void setJobGroup(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }
    public String getJobGroupDD() {
        return "Job_jobGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="responsibilities">
    @OneToMany(mappedBy="job")
    private List<JobResponsability> responsibilities;

    public List<JobResponsability> getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(List<JobResponsability> responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getResponsibilitiesDD() {
        return "Job_responsibilities";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="jobCertificatesNew">
    @OneToMany
    private List<JobCertificateNew> jobCertificatesNew;

    public List<JobCertificateNew> getJobCertificatesNew() {
        return jobCertificatesNew;
    }

    public void setJobCertificatesNew(List<JobCertificateNew> jobCertificatesNew) {
        this.jobCertificatesNew = jobCertificatesNew;
    }
    
    public String getJobCertificatesNewDD() {
        return "Job_jobCertificatesNew";
    }
    //</editor-fold>
}
