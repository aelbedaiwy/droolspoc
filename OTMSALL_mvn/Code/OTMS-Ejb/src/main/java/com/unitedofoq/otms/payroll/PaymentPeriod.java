package com.unitedofoq.otms.payroll;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import javax.persistence.*;


/**
 * The persistent class for the pay_period database table.
 * 
 */
@Entity
@ParentEntity(fields={"paymentMethod"})
public class PaymentPeriod extends BusinessObjectBaseEntity 
{
    // <editor-fold defaultstate="collapsed" desc="paymentMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	@JoinColumn(nullable=false)
	private PaymentMethod paymentMethod;
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }
    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    public String getPaymentMethodDD() {
        return "PaymentPeriod_paymentMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="daysPerPeriod">
	@Column(nullable=false)
	private int daysPerPeriod;
    public int getDaysPerPeriod() {
        return daysPerPeriod;
    }
    public void setDaysPerPeriod(int daysPerPeriod) {
        this.daysPerPeriod = daysPerPeriod;
    }
    public String getDaysPerPeriodDD() {
        return "PaymentPeriod_daysPerPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "PaymentPeriod_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDay">
	@Column
	private int endDay;
    public int getEndDay() {
        return endDay;
    }
    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }
    public String getEndDayDD() {
        return "PaymentPeriod_endDay";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endOfMonth">
	@Column(length=1)
	private String endOfMonth;
    public String getEndOfMonth() {
        return endOfMonth;
    }
    public void setEndOfMonth(String endOfMonth) {
        this.endOfMonth = endOfMonth;
    }
    public String getEndOfMonthDD() {
        return "PaymentPeriod_endOfMonth";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hoursPerDay">
    @Column(precision=25, scale=13,nullable=false)
    private BigDecimal hoursPerDay;
    public BigDecimal getHoursPerDay() {
        return hoursPerDay;
    }
    public void setHoursPerDay(BigDecimal hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }
    public String getHoursPerDayDD() {
        return "PaymentPeriod_hoursPerDay";
    }
    @Transient
    private BigDecimal hoursPerDayMask;
    public BigDecimal getHoursPerDayMask() {
        hoursPerDayMask = hoursPerDay;
        return hoursPerDayMask;
    }
    public void setHoursPerDayMask(BigDecimal hoursPerDayMask) {
        updateDecimalValue("hoursPerDay",hoursPerDayMask);
    }
    public String getHoursPerDayMaskDD() {
        return "PaymentPeriod_hoursPerDayMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "PaymentPeriod_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDay">
	@Column(nullable=false)
	private int startDay;
    public int getStartDay() {
        return startDay;
    }
    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }
    public String getStartDayDD() {
        return "PaymentPeriod_startDay";
    }
    // </editor-fold>
}