/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.MeasurableField;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplateSequence;
import com.unitedofoq.otms.foundation.employee.Employee;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields="employee")
@ChildEntity(fields={"accountability"})
public class EDSIDP extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="Review Frequency">
    @Transient
    public final static int RF_MONTHLY = 36951;

    @Transient
    public final static int RF_QUARTERLY = 39101;
    // </editor-fold>

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC reviewFrequency;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC purpose;
    @Column(nullable=false)
    private boolean hrManager;
    @Column(nullable=false)
    private boolean hrStaff;
    @Column(nullable=false)
    private boolean self;
    @Column(nullable=false)
    private boolean consultant;
    @Column(nullable=false)
    private boolean firstManager;
    @Column(nullable=false)
    private boolean secondManager;
    @Column(nullable=false)
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    @Column(nullable=false)
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Transient
    private Date reviewDate;
    @Column(nullable=false)
    @MeasurableField(name = "planDurationUnit.value")
    private String planDuration;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC planDurationUnit;
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private Employee employee;
    @OneToMany(mappedBy="idp")
    private List<EDSIDPAccountability> accountability;

    public String getNameTranslatedDD()            {    return "EDSIDP_name";   }
    public String getPurposeDD()         {    return "EDSIDP_purpose";   }
    public String getEffectiveDateDD()   {    return "EDSIDP_effectiveDate";   }
    public String getReviewDateDD()      {    return "EDSIDP_reviewDate";   }
    public String getPlanDurationDD()    {    return "EDSIDP_planDuration";   }
    public String getPlanDurationUnitDD(){    return "EDSIDP_planDurationUnit";   }
    public String getHrManagerDD()       {    return "EDSIDP_hrManager";   }
    public String getHrStaffDD()         {    return "EDSIDP_hrStaff";   }
    public String getSelfDD()            {    return "EDSIDP_self";   }
    public String getConsultantDD()      {    return "EDSIDP_consultant";   }
    public String getFirstManagerDD()    {    return "EDSIDP_firstManager";   }
    public String getSecondManagerDD()   {    return "EDSIDP_secondManager";   }
    public String getCreationDateDD()    {    return "EDSIDP_creationDate";   }
    public String getReviewFrequencyDD() {    return "EDSIDP_reviewFrequency";   }
    public String getEmployeeDD()        {    return "EDSIDP_employee";   }

    public UDC getPurpose() {
        return purpose;
    }

    public void setPurpose(UDC purpose) {
        this.purpose = purpose;
    }

    public UDC getPlanDurationUnit() {
        return planDurationUnit;
    }

    public UDC getReviewFrequency() {
        return reviewFrequency;
    }

    public void setReviewFrequency(UDC reviewFrequency) {
        this.reviewFrequency = reviewFrequency;

        if (reviewFrequency != null && effectiveDate != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(effectiveDate);
            if (reviewFrequency.getDbid() == RF_MONTHLY)
                cal.add(Calendar.MONTH, 1);
            else
                cal.add(Calendar.MONTH, 3);
            reviewDate = cal.getTime();

            Date currentDate = new Date();
            while (reviewDate.before(currentDate)) {
                if (reviewFrequency.getDbid() == RF_MONTHLY)
                    cal.add(Calendar.MONTH, 1);
                else
                    cal.add(Calendar.MONTH, 3);
                reviewDate = cal.getTime();
            }
        }
    }

    public void setPlanDurationUnit(UDC planDurationUnit) {
        this.planDurationUnit = planDurationUnit;
    }

    public List<EDSIDPAccountability> getAccountability() {
        return accountability;
    }

    public void setAccountability(List<EDSIDPAccountability> accountability) {
        this.accountability = accountability;
    }



    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;

        if (reviewFrequency != null && effectiveDate != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(effectiveDate);
            if (reviewFrequency.getDbid() == RF_MONTHLY)
                cal.add(Calendar.MONTH, 1);
            else
                cal.add(Calendar.MONTH, 3);
            reviewDate = cal.getTime();

            Date currentDate = new Date();
            while (reviewDate.before(currentDate)) {
                if (reviewFrequency.getDbid() == RF_MONTHLY)
                    cal.add(Calendar.MONTH, 1);
                else
                    cal.add(Calendar.MONTH, 3);
                reviewDate = cal.getTime();
            }
        }
    }

    public Date getReviewDate() {
        if (reviewDate == null) {
            if (reviewFrequency != null && effectiveDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(effectiveDate);
                if (reviewFrequency.getDbid() == RF_MONTHLY)
                    cal.add(Calendar.MONTH, 1);
                else
                    cal.add(Calendar.MONTH, 3);
                reviewDate = cal.getTime();

                Date currentDate = new Date();
                while (reviewDate.before(currentDate)) {
                    if (reviewFrequency.getDbid() == RF_MONTHLY)
                        cal.add(Calendar.MONTH, 1);
                    else
                        cal.add(Calendar.MONTH, 3);
                    reviewDate = cal.getTime();
                }
            }
        }

        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlanDuration() {
        return planDuration;
    }

    public void setPlanDuration(String planDuration) {
        this.planDuration = planDuration;
    }

    public boolean isConsultant() {
        return consultant;
    }

    public void setConsultant(boolean consultant) {
        this.consultant = consultant;
    }

    public boolean isFirstManager() {
        return firstManager;
    }

    public void setFirstManager(boolean firstManager) {
        this.firstManager = firstManager;
    }

    public boolean isHrManager() {
        return hrManager;
    }

    public void setHrManager(boolean hrManager) {
        this.hrManager = hrManager;
    }

    public boolean isHrStaff() {
        return hrStaff;
    }

    public void setHrStaff(boolean hrStaff) {
        this.hrStaff = hrStaff;
    }

    public boolean isSecondManager() {
        return secondManager;
    }

    public void setSecondManager(boolean secondManager) {
        this.secondManager = secondManager;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
    @Transient
    private Date endDate;

    public Date getEndDate() {
        if(planDurationUnit != null){
            if(planDurationUnit.getValue().equalsIgnoreCase("month")){
                Calendar endCalendar = Calendar.getInstance();
                endCalendar.setTime(effectiveDate);
                endCalendar.add(Calendar.MONTH, Integer.parseInt(planDuration));
                return endCalendar.getTime();
            }else if(planDurationUnit.getValue().equalsIgnoreCase("day")){
                Calendar endCalendar = Calendar.getInstance();
                endCalendar.setTime(effectiveDate);
                endCalendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(planDuration));
                return endCalendar.getTime();
            }else if(planDurationUnit.getValue().equalsIgnoreCase("year")){
                Calendar endCalendar = Calendar.getInstance();
                endCalendar.setTime(effectiveDate);
                endCalendar.add(Calendar.YEAR, Integer.parseInt(planDuration));
                return endCalendar.getTime();
            }
//            else if(planDurationUnit.getValue().equalsIgnoreCase("annual")){
//                Calendar endCalendar = Calendar.getInstance();
//                endCalendar.setTime(effectiveDate);
//                endCalendar.add(Calendar.YEAR, Integer.parseInt(planDuration));
//                return endCalendar.getTime();
//            }
        }
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplateSequence instance;

    public AppraisalTemplateSequence getInstance() {
        return instance;
    }
    
    public String getInstanceDD() {
        return "EDSIDP_instance";
    }

    public void setInstance(AppraisalTemplateSequence instance) {
        this.instance = instance;
    }
}
