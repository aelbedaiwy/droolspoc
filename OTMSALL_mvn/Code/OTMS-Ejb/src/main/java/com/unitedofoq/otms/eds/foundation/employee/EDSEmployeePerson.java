package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.data.Person;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "EmployeePerson")
@DiscriminatorValue("EMP")
@ParentEntity(fields={"employee"})
public class EDSEmployeePerson extends Person {
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)

    private EDSEmployee employee;
    public EDSEmployee getEmployee() {
        return employee;
    }
    
    public void setEmployee(EDSEmployee employee) {
        this.employee = employee;
    }
}
