/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.training.activity.ProviderActivity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeTraining extends PersonnelTrainingBase {
    // <editor-fold defaultstate="collapsed" desc="employee">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeTraining_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="providerActivity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderActivity providerActivity;

    public ProviderActivity getProviderActivity() {
        return providerActivity;
    }

    public void setProviderActivity(ProviderActivity providerActivity) {
        this.providerActivity = providerActivity;
    }

    public String getProviderActivityDD() {
        return "EmployeeTraining_providerActivity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attandancePercent">
    @Column
    private BigDecimal attandancePercent;

    public BigDecimal getAttandancePercent() {
        return attandancePercent;
    }

    public void setAttandancePercent(BigDecimal attandancePercent) {
        this.attandancePercent = attandancePercent;
    }
    
    public String getAttandancePercentDD() {
        return "EmployeeTraining_attandancePercent";
    }
    @Transient
    private BigDecimal attandancePercentMask;

    public BigDecimal getAttandancePercentMask() {
        attandancePercentMask = attandancePercent ;
        return attandancePercentMask;
    }

    public void setAttandancePercentMask(BigDecimal attandancePercentMask) {
        updateDecimalValue("attandancePercent", attandancePercentMask);
    }

    public String getAttandancePercentMaskDD() {
        return "EmployeeTraining_attandancePercentMask";
    }
    // </editor-fold>
}
