package com.unitedofoq.otms.personnel.contract;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.position.Position;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeContract extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeDD() {
        return "EmployeeContract_employee";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="contract">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Contract contract;

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Contract getContract() {
        return contract;
    }

    public String getContractDD() {
        return "EmployeeContract_contract";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getEndDateDD() {
        return "EmployeeContract_endDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateDD() {
        return "EmployeeContract_startDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="period">
    private Integer period;

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Integer getPeriod() {
        return period;
    }

    public String getPeriodDD() {
        return "EmployeeContract_period";
    }

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="probationPeriod">
    @Column(precision = 25, scale = 13)
    private BigDecimal probationPeriod; // days

    public BigDecimal getProbationPeriod() {
        return probationPeriod;
    }

    public void setProbationPeriod(BigDecimal probationPeriod) {
        this.probationPeriod = probationPeriod;
    }

    public String getProbationPeriodDD() {
        return "EmployeeContract_probationPeriod";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="workingHours">
    @Column(precision = 25, scale = 13)
    private BigDecimal workingHours;

    public BigDecimal getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(BigDecimal workingHours) {
        this.workingHours = workingHours;
    }

    public String getWorkingHoursDD() {
        return "EmployeeContract_workingHours";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="contractSalary">
    @Column(precision = 25, scale = 13)
    private BigDecimal contractSalary;

    public BigDecimal getContractSalary() {
        return contractSalary;
    }

    public void setContractSalary(BigDecimal contractSalary) {
        this.contractSalary = contractSalary;
    }

    public String getContractSalaryDD() {
        return "EmployeeContract_contractSalary";
    }

    @Transient
    private BigDecimal contractSalaryMask;

    public BigDecimal getContractSalaryMask() {
        contractSalaryMask = contractSalary;
        return contractSalaryMask;
    }

    public void setContractSalaryMask(BigDecimal contractSalaryMask) {
        updateDecimalValue("contractSalary", contractSalaryMask);
    }

    public String getContractSalaryMaskDD() {
        return "EmployeeContract_contractSalaryMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="numOfTickets">
    private Integer numOfTickets;

    public Integer getNumOfTickets() {
        return numOfTickets;
    }

    public void setNumOfTickets(Integer numOfTickets) {
        this.numOfTickets = numOfTickets;
    }

    public String getNumOfTicketsDD() {
        return "EmployeeContract_numOfTickets";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="contractSalaryEnc">
    private String contractSalaryEnc;

    public String getContractSalaryEnc() {
        return contractSalaryEnc;
    }

    public void setContractSalaryEnc(String contractSalaryEnc) {
        this.contractSalaryEnc = contractSalaryEnc;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="contractPosition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "contractposition_dbid")
    private Position contractPosition;

    public Position getContractPosition() {
        return contractPosition;
    }

    public void setContractPosition(Position contractPosition) {
        this.contractPosition = contractPosition;
    }

    public String getContractPositionDD() {
        return "EmployeeContract_contractPosition";
    }
    // </editor-fold >
}
