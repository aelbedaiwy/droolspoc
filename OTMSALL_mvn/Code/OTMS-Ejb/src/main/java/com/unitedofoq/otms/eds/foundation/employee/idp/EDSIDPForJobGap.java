/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.foundation.employee.EmployeeProfiler;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author arezk
 */
@Entity
@ChildEntity(fields={"employeeCompetencyGaps"})
@ParentEntity(fields="employee")
public class EDSIDPForJobGap extends EDSIDP{
    @OneToMany(mappedBy = "jobGap")
    private List<EmployeeCompetencyGap> employeeCompetencyGaps;

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeProfiler employeeProfiler;

    public EmployeeProfiler getEmployeeProfiler() {
        return employeeProfiler;
    }

    public String getEmployeeProfilerDD() {
        return "EDSIDPForJobGap_employeeProfiler";
    }

    public void setEmployeeProfiler(EmployeeProfiler employeeProfiler) {
        this.employeeProfiler = employeeProfiler;
    }

    public List<EmployeeCompetencyGap> getEmployeeCompetencyGaps() {
        return employeeCompetencyGaps;
    }

    public void setEmployeeCompetencyGaps(List<EmployeeCompetencyGap> employeeCompetencyGaps) {
        this.employeeCompetencyGaps = employeeCompetencyGaps;
    }        
}
