
package com.unitedofoq.otms.training;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.training.location.Location;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@MappedSuperclass
public class PersonnelTrainingBase extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "EmployeeTraining_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "EmployeeTraining_endDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="finishDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getFinishDateDD() {
        return "EmployeeTraining_finishDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Location location;
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "EmployeeTraining_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cost">
    @Column(precision=25, scale=13)
    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCostDD() {
        return "EmployeeTraining_cost";
    }
    @Transient
    private BigDecimal costMask;

    public BigDecimal getCostMask() {
        costMask = cost ;
        return costMask;
    }

    public void setCostMask(BigDecimal costMask) {
        updateDecimalValue("cost", costMask);
    }

    public String getCostMaskDD() {
        return "EmployeeTraining_costMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="evaluationScore">
    @Column
    private BigDecimal evaluationScore;

    public BigDecimal getEvaluationScore() {
        return evaluationScore;
    }

    public void setEvaluationScore(BigDecimal evaluationScore) {
        this.evaluationScore = evaluationScore;
    }

    public String getEvaluationScoreDD() {
        return "EmployeeTraining_evaluationScore";
    }
    @Transient
    private BigDecimal evaluationScoreMask;

    public BigDecimal getEvaluationScoreMask() {
        evaluationScoreMask = evaluationScore;
        return evaluationScoreMask;
    }

    public void setEvaluationScoreMask(BigDecimal evaluationScoreMask) {
        updateDecimalValue("evaluationScore", evaluationScoreMask);
    }

    public String getEvaluationScoreMaskDD() {
        return "EmployeeTraining_evaluationScoreMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="evaluationDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaluationDate;

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public void setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
    }

    public String getEvaluationDateDD() {
        return "EmployeeTraining_evaluationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "EmployeeTraining_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="courseDescription">
    @Column
    private String courseDescription;

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public String getCourseDescriptionDD() {
        return "EmployeeTraining_courseDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="providerDescription">
    @Column
    private String providerDescription;

    public String getProviderDescription() {
        return providerDescription;
    }

    public void setProviderDescription(String providerDescription) {
        this.providerDescription = providerDescription;
    }

    public String getProviderDescriptionDD() {
        return "EmployeeTraining_providerDescription";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="locationDescription">
    @Column
    private String locationDescription;

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationDescriptionDD() {
        return "EmployeeTraining_locationDescription";
    }
    // </editor-fold >
}
