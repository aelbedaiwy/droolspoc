package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.health.MedicalClaim;
import com.unitedofoq.otms.recruitment.applicant.PersonnelDependenceBase;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeDependance extends PersonnelDependenceBase {

    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeDependance_employee";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="formulaApplied">
    @Column(length = 1)
    private String formulaApplied = null;

    public String getFormulaApplied() {
        return formulaApplied;
    }

    public void setFormulaApplied(String formulaApplied) {
        this.formulaApplied = formulaApplied;
    }

    public String getFormulaAppliedDD() {
        return "EmployeeDependance_formulaApplied";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="medicalClaim">
    @OneToMany(mappedBy = "dependence")
    private List<MedicalClaim> medicalClaim;

    public List<MedicalClaim> getMedicalClaim() {
        return medicalClaim;
    }

    public void setMedicalClaim(List<MedicalClaim> medicalClaim) {
        this.medicalClaim = medicalClaim;
    }

    public String getMedicalClaimDD() {
        return "EmployeeDependance_medicalClaim";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nationalIDIssueDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date nationalIDIssueDate;

    public Date getNationalIDIssueDate() {
        return nationalIDIssueDate;
    }

    public void setNationalIDIssueDate(Date nationalIDIssueDate) {
        this.nationalIDIssueDate = nationalIDIssueDate;
    }

    public String getNationalIDIssueDateDD() {
        return "EmployeeDependance_nationalIDIssueDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nationalIDExpirationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date nationalIDExpirationDate;

    public Date getNationalIDExpirationDate() {
        return nationalIDExpirationDate;
    }

    public void setNationalIDExpirationDate(Date nationalIDExpirationDate) {
        this.nationalIDExpirationDate = nationalIDExpirationDate;
    }

    public String getNationalIDExpirationDateDD() {
        return "EmployeeDependance_nationalIDExpirationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="medicalClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC medicalClass;

    public String getMedicalClassDD() {
        return "EmployeeDependance_medicalClass";
    }

    public UDC getMedicalClass() {
        return medicalClass;
    }

    public void setMedicalClass(UDC medicalClass) {
        this.medicalClass = medicalClass;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nationalID">
    @Column
    private String nationalID;

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    public String getNationalIDDD() {
        return "EmployeeDependance_nationalID";
    }
    // </editor-fold>
}
