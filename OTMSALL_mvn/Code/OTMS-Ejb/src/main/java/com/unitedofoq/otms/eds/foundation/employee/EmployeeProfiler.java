/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author nkhalil
 */
@Entity
@ParentEntity(fields={"employee"})
@ChildEntity(fields={"employeeProfilerAssessors", "edsEmployeeCompetencies"})
public class EmployeeProfiler extends BaseEntity {
    @OneToMany(mappedBy = "employeeProfiler")
    private List<EDSEmployeeCompetency> edsEmployeeCompetencies;

    public List<EDSEmployeeCompetency> getEdsEmployeeCompetencies() {
        return edsEmployeeCompetencies;
    }

    public void setEdsEmployeeCompetencies(List<EDSEmployeeCompetency> edsEmployeeCompetencies) {
        this.edsEmployeeCompetencies = edsEmployeeCompetencies;
    }
    public String getEdsEmployeeCompetenciesDD() {return "EmployeeProfiler_edsEmployeeCompetencies";}

    @OneToMany(mappedBy = "employeeProfiler")
    private List<EmployeeProfilerAssessor> employeeProfilerAssessors;
    public String getEmployeeProfilerAssessorsDD() {return "EmployeeProfiler_employeeProfilerAssessors";}

    public List<EmployeeProfilerAssessor> getEmployeeProfilerAssessors() {
        return employeeProfilerAssessors;
    }

    public void setEmployeeProfilerAssessors(List<EmployeeProfilerAssessor> employeeProfilerAssessors) {
        this.employeeProfilerAssessors = employeeProfilerAssessors;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date creationDate;

    public String getCreationDateDD() {
        return "EmployeeProfiler_creationDate";
    }
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC purpose;

    public String getPurposeDD() {
        return "EmployeeProfiler_purpose";
    }

    public UDC getPurpose() {
        return purpose;
    }

    public void setPurpose(UDC purpose) {
        this.purpose = purpose;
    }

    private boolean empAssossorSelf;
    private boolean empAssossorLineManger;
    private boolean empAssossorHrManger;
    private boolean empAssossorConsultant;
    private boolean empAssossorSecondManger;

    public String getEmpAssossorConsultantDD() {
        return "EmployeeProfiler_empAssossorConsultant";
    }

    public boolean isEmpAssossorConsultant() {
        return empAssossorConsultant;
    }

    public void setEmpAssossorConsultant(boolean empAssossorConsultant) {
        this.empAssossorConsultant = empAssossorConsultant;
    }

    public String getEmpAssossorHrMangerDD() {
        return "EmployeeProfiler_empAssossorHrManger";
    }

    public boolean isEmpAssossorHrManger() {
        return empAssossorHrManger;
    }

    public void setEmpAssossorHrManger(boolean empAssossorHrManger) {
        this.empAssossorHrManger = empAssossorHrManger;
    }

    public String getEmpAssossorLineMangerDD() {
        return "EmployeeProfiler_empAssossorLineManger";
    }
    public boolean isEmpAssossorLineManger() {
        return empAssossorLineManger;
    }

    public void setEmpAssossorLineManger(boolean empAssossorLineManger) {
        this.empAssossorLineManger = empAssossorLineManger;
    }

    public String getEmpAssossorSecondMangerDD() {
        return "EmployeeProfiler_empAssossorSecondManger";
    }

    public boolean isEmpAssossorSecondManger() {
        return empAssossorSecondManger;
    }

    public void setEmpAssossorSecondManger(boolean empAssossorSecondManger) {
        this.empAssossorSecondManger = empAssossorSecondManger;
    }

    public String getEmpAssossorSelfDD() {
        return "EmployeeProfiler_empAssossorSelf";
    }

    public boolean isEmpAssossorSelf() {
        return empAssossorSelf;
    }

    public void setEmpAssossorSelf(boolean empAssossorSelf) {
        this.empAssossorSelf = empAssossorSelf;
    }
    private String description;

    public String getDescriptionDD() {
        return "EmployeeProfiler_description";
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(nullable=false)
    private EDSEmployee employee;

    public String getEmployeeDD() {
        return "EmployeeProfiler_employee";
    }

    public EDSEmployee getEmployee() {
        return employee;
    }

    public void setEmployee(EDSEmployee employee) {
        this.employee = employee;
    }
    
}
