/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author arezk
 */
@Entity
@ParentEntity(fields = {"employee"})
public class EmployeePhoto extends BaseEntity {

    @OneToOne
    @JoinColumn(nullable = false, unique = true)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
     public String getEmployeeDD() {
        return "EmployeePhoto_employee";
    }
    
    @Column(columnDefinition = "BLOB(2097152)") // 2MB size
    private byte[] photoData;

    public byte[] getPhotoData() {
        return photoData;
    }

    public void setPhotoData(byte[] photoData) {
        this.photoData = photoData;
    }

    public String getPhotoDataDD() {
        return "EmployeePhoto_photoData";
    }
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "EmployeePhoto_name";
    }
//    @Transient
//    private GraphicImage personelPhotoData;
//
//    /**
//     * @return the personelPhotoData
//     */
//    public GraphicImage getPersonelPhotoData() {
//        GraphicImage image = new GraphicImage();
//        // call creation of byte array to image file
//        return image;
//    }
//
//    /**
//     * @param personelPhotoData the personelPhotoData to set
//     */
//    public void setPersonelPhotoData(GraphicImage personelPhotoData) {
//        this.personelPhotoData = personelPhotoData;
//    }
}
