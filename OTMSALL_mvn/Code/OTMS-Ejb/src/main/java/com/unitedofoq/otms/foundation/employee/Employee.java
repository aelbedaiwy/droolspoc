package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ComputedField;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.appraisal.employee.EmployeeGoal;
import com.unitedofoq.otms.appraisal.employee.EmployeeIncident;
import com.unitedofoq.otms.appraisal.succession.SuccessionPlan;
import com.unitedofoq.otms.eds.foundation.employee.PerformanceEvaluation;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.health.EmployeeMedical;
import com.unitedofoq.otms.payroll.EOSLeaveReason;
import com.unitedofoq.otms.payroll.Factory;
import com.unitedofoq.otms.payroll.costcenter.EmpCostCenterException;
import com.unitedofoq.otms.payroll.costcenter.EmployeeSapIntegerationCostCode;
import com.unitedofoq.otms.payroll.costcenter.EmployeeSapIntegerationProject;
import com.unitedofoq.otms.payroll.custody.EmployeeCustody;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeBenifit;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeBill;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeCostCenter;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeEosLoan;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeEosSalaryElement;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeePayroll;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeePeriodicSalaryElement;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryElement;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryUpgrade;
import com.unitedofoq.otms.payroll.loan.EmployeeLoan;
import com.unitedofoq.otms.personnel.EmployeeRequest;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsence;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.personnel.contract.EmployeeContract;
import com.unitedofoq.otms.personnel.dayoff.EmployeeDayOff;
import com.unitedofoq.otms.personnel.dayoff.EmployeeDayOffRequest;
import com.unitedofoq.otms.personnel.penalty.EmployeePenalty;
import com.unitedofoq.otms.personnel.penalty.EmployeePenaltyRequest;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacation;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationHistory;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationRequest;
import com.unitedofoq.otms.personnel.vacation.VacationSettlement;
import com.unitedofoq.otms.timemanagement.employee.EmployeeAnnualPlanner;
import com.unitedofoq.otms.timemanagement.employee.EmployeeTimeSheet;
import com.unitedofoq.otms.timemanagement.timesheet.EmployeeProject;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import com.unitedofoq.otms.training.employee.EmployeeCourse;
import com.unitedofoq.otms.training.employee.EmployeeCourseRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "OEmployee")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ETYPE")
@DiscriminatorValue("MASTER")
@ChildEntity(fields = {"payroll", "periodicSalaryElements", "employeePhoto",
    "salaryUpgrades", "salaryElements", "contract",
    "historyImages", "vacations", "vacationRequests", "penaltyRequests",
    "penalties", "absences", "absenceRequests", "dayOffRequests", "dayOffs",
    "employeePositions", "employeeIqamas", "employeeMilitaryServices", "employeePassports",
    "employeeVisas", "employeeUser", "employeeLoans",
    "employeeCourses", "courseRequests", "employeeRequests", "bills", "employeeBenifit", "profileHistory",
    "eosLoans", "eosElements", "employeePassportGovs", "employeeContracts", "employeeCustodies", "dependances", "otherInfo",
    "employeeplanners", "employeeCostCenters", "empCostCenterExceptions", "empVacHists", "employeeMedical", "successionPlans", "timeSheets", "employeeProject", "employeeSapIntegerationProject", "qualifications", "employeePerformanceEvaluations",
    "employeeEducationHistories", "employeeCertificates", "additionalInfo", "vacationSettlements", "employeeGoal", "employeeIncident"})
@NamedQuery(
        name = "getEmployeeForClosing",
        query = " SELECT    e "
        + " FROM      Employee e"
        + " WHERE     e.leaveDate >= :from and e.leaveDate <= :to")
//Salema[Emloyee New Design]
@ParentEntity(fields = "positionSimpleMode")
public class Employee extends EmployeeBase {

    // <editor-fold defaultstate="collapsed" desc="directManager">
    //to be modified now it get first employee
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee directManager;
    //Salema[Emloyee New Design]
//    public void setDirectManager(Employee directManager) {
//        this.directManager = directManager;
//    }
//     public Employee getDirectManager() {
//         Position position = getEmployeeMainPosition().getPosition();
//         List<EmployeeMainPosition> employeeParents = position.getPositionDirectParent().getPosition().getMainEmployees();
//
//        if ( position != null && position.getPositionDirectParent() != null && position.getPositionDirectParent().getPosition().getMainEmployees() != null) {
//          //  for (Employee emp : position.getParentPosition().getMainEmployees().getEmployee()) {
//            for (EmployeeMainPosition employeeParent: employeeParents){
//                if (employeeParent.getEmployee().isInActive()) {
//                    return employeeParent.getEmployee();
//                }
//            }
//        }
//
//        return null;
//    }

    public void setDirectManager(Employee directManager) {
        this.directManager = directManager;
    }

    public Employee getDirectManager() {
        return directManager;
    }

    public String getDirectManagerDD() {
        return "Employee_directManager";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeMainPosition">
    //Salema[Employee new design]
//    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE},mappedBy="employee")
//    private EmployeeMainPosition employeeMainPosition;
//
//    public EmployeeMainPosition getEmployeeMainPosition() {
//        return employeeMainPosition;
//    }
//
//    public void setEmployeeMainPosition(EmployeeMainPosition employeeMainPosition) {
//        this.employeeMainPosition = employeeMainPosition;
//    }
//
//     public String getEmployeeMainPositionDD() {
//        return "Employee_employeeMainPosition";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @Transient
    //Salema[Employee New Design]
    @ComputedField(filterLHSExpression = {"positionSimpleMode.unit"})
    Unit unit;

    public Unit getUnit() {
        try {
            //Salema[Employee New Design]
//            if (getEmployeeMainPosition() != null)
//            {
//             if (getEmployeeMainPosition().getPosition() != null)
//             {
//                return getEmployeeMainPosition().getPosition().getUnit();
//             }
//            }
            if (getPositionSimpleMode() != null) {
                return getPositionSimpleMode().getUnit();
            }
            return null;
        } catch (Exception Ex) {
            return null;
        }

    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitDD() {
        return "EDSEmployee_unit";
    }
    @Transient
    @ComputedField(filterLHSExpression = {"positionSimpleMode.unit"})
    Unit unitFilter;

    public Unit getUnitFilter() {
        return unitFilter;
    }

    public void setUnitFilter(Unit unitFilter) {
        this.unitFilter = unitFilter;
    }

    @Override
    protected void PostLoad() {
        super.PostLoad();
        unitFilter = getUnit();
    }

    public String getUnitFilterDD() {
        return "EDSEmployee_unit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElements">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeSalaryElement> salaryElements;

    public List<EmployeeSalaryElement> getSalaryElements() {
        return salaryElements;
    }

    public void setSalaryElements(List<EmployeeSalaryElement> salaryElements) {
        this.salaryElements = salaryElements;
    }

    public String getSalaryElementsDD() {
        return "Employee_salaryElements";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payroll">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "employee")
    private EmployeePayroll payroll;

    public EmployeePayroll getPayroll() {
        return payroll;
    }

    public void setPayroll(EmployeePayroll payroll) {
        this.payroll = payroll;
    }

    public String getPayrollDD() {
        return "Employee_payroll";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="contract">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "employee")
    private EmployeeContract contract;

    public EmployeeContract getContract() {
        return contract;
    }

    public void setContract(EmployeeContract contract) {
        this.contract = contract;
    }

    public String getContractDD() {
        return "Employee_contract";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryUpgrades">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeSalaryUpgrade> salaryUpgrades;

    public List<EmployeeSalaryUpgrade> getSalaryUpgrades() {
        return salaryUpgrades;
    }

    public void setSalaryUpgrades(List<EmployeeSalaryUpgrade> salaryUpgrades) {
        this.salaryUpgrades = salaryUpgrades;
    }

    public String getSalaryUpgradesDD() {
        return "Employee_salaryUpgrades";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="periodicSalaryElements">
    @OneToMany(mappedBy = "employee")
    private List<EmployeePeriodicSalaryElement> periodicSalaryElements;

    public List<EmployeePeriodicSalaryElement> getPeriodicSalaryElements() {
        return periodicSalaryElements;
    }

    public void setPeriodicSalaryElements(List<EmployeePeriodicSalaryElement> periodicSalaryElements) {
        this.periodicSalaryElements = periodicSalaryElements;
    }

    public String getPeriodicSalaryElementsDD() {
        return "Employee_periodicSalaryElements";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeUser">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "employee")
    @JoinColumn
    private EmployeeUser employeeUser;

    public EmployeeUser getEmployeeUser() {
        return employeeUser;
    }

    public void setEmployeeUser(EmployeeUser employeeUser) {
        this.employeeUser = employeeUser;
    }

    public String getEmployeeUserDD() {
        return "Employee_employeeUser";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeIncident">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeIncident> employeeIncident;

    public List<EmployeeIncident> getEmployeeIncident() {
        return employeeIncident;
    }

    public void setEmployeeIncident(List<EmployeeIncident> employeeIncident) {
        this.employeeIncident = employeeIncident;
    }

    public String getEmployeeIncidentDD() {
        return "Employee_employeeIncident";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeGoal">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeGoal> employeeGoal;

    public List<EmployeeGoal> getEmployeeGoal() {
        return employeeGoal;
    }

    public void setEmployeeGoal(List<EmployeeGoal> employeeGoal) {
        this.employeeGoal = employeeGoal;
    }

    public String getEmployeeGoalDD() {
        return "Employee_employeeGoal";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacations">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeVacation> vacations;

    public List<EmployeeVacation> getVacations() {
        return vacations;
    }

    public void setVacations(List<EmployeeVacation> vacations) {
        this.vacations = vacations;
    }

    public String getVacationsDD() {
        return "Employee_vacations";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationRequests">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeVacationRequest> vacationRequests;

    public List<EmployeeVacationRequest> getVacationRequests() {
        return vacationRequests;
    }

    public void setVacationRequests(List<EmployeeVacationRequest> vacationRequests) {
        this.vacationRequests = vacationRequests;
    }

    public String getVacationRequestsDD() {
        return "Employee_vacationRequests";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penaltyRequests">
    @OneToMany(mappedBy = "employee")
    private List<EmployeePenaltyRequest> penaltyRequests;

    public List<EmployeePenaltyRequest> getPenaltyRequests() {
        return penaltyRequests;
    }

    public void setPenaltyRequests(List<EmployeePenaltyRequest> penaltyRequests) {
        this.penaltyRequests = penaltyRequests;
    }

    public String getPenaltyRequestsDD() {
        return "Employee_penaltyRequests";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="penalties">
    @OneToMany(mappedBy = "employee")
    private List<EmployeePenalty> penalties;

    public List<EmployeePenalty> getPenalties() {
        return penalties;
    }

    public void setPenalties(List<EmployeePenalty> penalties) {
        this.penalties = penalties;
    }

    public String getPenaltiesDD() {
        return "Employee_penalties";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="absences">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeAbsence> absences;

    public List<EmployeeAbsence> getAbsences() {
        return absences;
    }

    public void setAbsences(List<EmployeeAbsence> absences) {
        this.absences = absences;
    }

    public String getAbsencesDD() {
        return "Employee_absences";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="absenceRequests">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeAbsenceRequest> absenceRequests;

    public List<EmployeeAbsenceRequest> getAbsenceRequests() {
        return absenceRequests;
    }

    public void setAbsenceRequests(List<EmployeeAbsenceRequest> absenceRequests) {
        this.absenceRequests = absenceRequests;
    }

    public String getAbsenceRequestsDD() {
        return "Employee_absenceRequests";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOffRequests">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeDayOffRequest> dayOffRequests;

    public List<EmployeeDayOffRequest> getDayOffRequests() {
        return dayOffRequests;
    }

    public void setDayOffRequests(List<EmployeeDayOffRequest> dayOffRequests) {
        this.dayOffRequests = dayOffRequests;
    }

    public String getDayOffRequestsDD() {
        return "Employee_dayOffRequests";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayOffs">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeDayOff> dayOffs;

    public List<EmployeeDayOff> getDayOffs() {
        return dayOffs;
    }

    public void setDayOffs(List<EmployeeDayOff> dayOffs) {
        this.dayOffs = dayOffs;
    }

    public String getDayOffsDD() {
        return "Employee_dayOffs";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeePositions">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeIndirectPosition> employeePositions;

    public List<EmployeeIndirectPosition> getEmployeePositions() {
        return employeePositions;
    }

    public void setEmployeePositions(List<EmployeeIndirectPosition> employeePositions) {
        this.employeePositions = employeePositions;
    }

    public String getEmployeePositionsDD() {
        return "Employee_employeePositions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "Employee_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeIqamas">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeIqama> employeeIqamas;

    public List<EmployeeIqama> getEmployeeIqamas() {
        return employeeIqamas;
    }

    public void setEmployeeIqamas(List<EmployeeIqama> employeeIqamas) {
        this.employeeIqamas = employeeIqamas;
    }

    public String getEmployeeIqamasDD() {
        return "Employee_employeeIqamas";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeVisas">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeVisa> employeeVisas;

    public List<EmployeeVisa> getEmployeeVisas() {
        return employeeVisas;
    }

    public void setEmployeeVisas(List<EmployeeVisa> employeeVisas) {
        this.employeeVisas = employeeVisas;
    }

    public String getEmployeeVisasDD() {
        return "Employee_employeeVisas";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeePassports">
    @OneToMany(mappedBy = "employee")
    private List<EmployeePassport> employeePassports;

    public List<EmployeePassport> getEmployeePassports() {
        return employeePassports;
    }

    public void setEmployeePassports(List<EmployeePassport> employeePassports) {
        this.employeePassports = employeePassports;
    }

    public String getEmployeePassportsDD() {
        return "Employee_employeePassports";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employeeMilitaryServices">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeMilitaryService> employeeMilitaryServices;

    public List<EmployeeMilitaryService> getEmployeeMilitaryServices() {
        return employeeMilitaryServices;
    }

    public void setEmployeeMilitaryServices(List<EmployeeMilitaryService> employeeMilitaryServices) {
        this.employeeMilitaryServices = employeeMilitaryServices;
    }

    public String getEmployeeMilitaryServicesDD() {
        return "Employee_employeeMilitaryServices";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="positionSimpleMode">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "position_dbid")
    private Position positionSimpleMode;

    public Position getPositionSimpleMode() {
        return positionSimpleMode;
    }

    public void setPositionSimpleMode(Position positionSimpleMode) {
        this.positionSimpleMode = positionSimpleMode;
    }

    public String getPositionSimpleModeDD() {
        return "Employee_positionSimpleMode";
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="employeeLoans">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeLoan> employeeLoans;

    public List<EmployeeLoan> getEmployeeLoans() {
        return employeeLoans;
    }

    public void setEmployeeLoans(List<EmployeeLoan> employeeLoans) {
        this.employeeLoans = employeeLoans;
    }

    public String getEmployeeLoansDD() {
        return "Employee_employeeLoans";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="loginName">
    //@Transient
    private String loginName;

    public String getLoginName() {
        return loginName;
    }

    public String getLoginNameDD() {
        return "Employee_loginName";
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
    //</editor-fold>
    ///New Migrated Files
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private NormalWorkingCalendar workingCalendar;

    public NormalWorkingCalendar getWorkingCalendar() {
        return workingCalendar;
    }

    public void setWorkingCalendar(NormalWorkingCalendar workingCalendar) {
        this.workingCalendar = workingCalendar;
    }

    public String getWorkingCalendarDD() {
        return "Employee_workingCalendar";
    }
    //<editor-fold defaultstate="collapsed" desc="employeeCourses">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeCourse> employeeCourses;

    public List<EmployeeCourse> getEmployeeCourses() {
        return employeeCourses;
    }

    public void setEmployeeCourses(List<EmployeeCourse> employeeCourses) {
        this.employeeCourses = employeeCourses;
    }

    public String getEmployeeCoursesDD() {
        return "Employee_employeeCourses";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="courseRequests">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeCourseRequest> courseRequests;

    public List<EmployeeCourseRequest> getCourseRequests() {
        return courseRequests;
    }

    public void setCourseRequests(List<EmployeeCourseRequest> courseRequests) {
        this.courseRequests = courseRequests;
    }

    public String getCourseRequestsDD() {
        return "Employee_courseRequests";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date hiringDate;

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getHiringDateDD() {
        return "Employee_hiringDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC hiringType;

    public UDC getHiringType() {
        return hiringType;
    }

    public void setHiringType(UDC hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringTypeDD() {
        return "Employee_hiringType";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeRequests">
    @OneToMany(mappedBy = "employee")
    List<EmployeeRequest> employeeRequests;

    public List<EmployeeRequest> getEmployeeRequests() {
        return employeeRequests;
    }

    public void setEmployeeRequests(List<EmployeeRequest> employeeRequests) {
        this.employeeRequests = employeeRequests;
    }

    public String getEmployeeRequestsDD() {
        return "Employee_employeeRequests";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="natID">
    private String natID;

    public String getNatID() {
        return natID;
    }

    public void setNatID(String natID) {
        this.natID = natID;
    }

    public String getNatIDDD() {
        return "Employee_natID";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeBenifit">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "employee")
    private EmployeeBenifit employeeBenifit;

    public EmployeeBenifit getEmployeeBenifit() {
        return employeeBenifit;
    }

    public void setEmployeeBenifit(EmployeeBenifit employeeBenifit) {
        this.employeeBenifit = employeeBenifit;
    }

    public String getEmployeeBenifitDD() {
        return "Employee_employeeBenifit";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="bills">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeBill> bills;

    public List<EmployeeBill> getBills() {
        return bills;
    }

    public void setBills(List<EmployeeBill> bills) {
        this.bills = bills;
    }

    public String getBillsDD() {
        return "Employee_bills";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="arabicName">
    //@Transient
    private String arabicName;

    public String getArabicName() {
        return arabicName;
    }

    public String getArabicNameDD() {
        return "Employee_arabicName";
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EnglishName">
    //@Transient
    private String englishName;

    public String getEnglishName() {
        return englishName;
    }

    public String getEnglishNameDD() {
        return "Employee_englishName";
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="profileHistory">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeProfileHistory> profileHistory;

    public List<EmployeeProfileHistory> getProfileHistory() {
        return profileHistory;
    }

    public void setProfileHistory(List<EmployeeProfileHistory> profileHistory) {
        this.profileHistory = profileHistory;
    }

    public String getProfileHistoryDD() {
        return "Employee_profileHistory";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="eosLoans">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeEosLoan> eosLoans;

    public List<EmployeeEosLoan> getEosLoans() {
        return eosLoans;
    }

    public void setEosLoans(List<EmployeeEosLoan> eosLoans) {
        this.eosLoans = eosLoans;
    }

    public String getEosLoansDD() {
        return "Employee_eosLoans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="eosElements">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeEosSalaryElement> eosElements;

    public List<EmployeeEosSalaryElement> getEosElements() {
        return eosElements;
    }

    public void setEosElements(List<EmployeeEosSalaryElement> eosElements) {
        this.eosElements = eosElements;
    }

    public String getEosElementsDD() {
        return "Employee_eosElements";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeePassportGovs">
    @OneToMany(mappedBy = "employee")
    private List<EmployeePassportGov> employeePassportGovs;

    public List<EmployeePassportGov> getEmployeePassportGovs() {
        return employeePassportGovs;
    }

    public void setEmployeePassportGovs(List<EmployeePassportGov> employeePassportGovs) {
        this.employeePassportGovs = employeePassportGovs;
    }

    public String getEmployeePassportGovsDD() {
        return "Employee_employeePassportGovs";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeContracts">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeContract> employeeContracts;

    public List<EmployeeContract> getEmployeeContracts() {
        return employeeContracts;
    }

    public void setEmployeeContracts(List<EmployeeContract> employeeContracts) {
        this.employeeContracts = employeeContracts;
    }

    public String getEmployeeContractsDD() {
        return "Employee_employeeContracts";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeCustodies">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeCustody> employeeCustodies;

    public List<EmployeeCustody> getEmployeeCustodies() {
        return employeeCustodies;
    }

    public void setEmployeeCustodies(List<EmployeeCustody> employeeCustodies) {
        this.employeeCustodies = employeeCustodies;
    }

    public String getEmployeeCustodiesDD() {
        return "Employee_employeeCustodies";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calendar">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TMCalendar calendar;

    public TMCalendar getCalendar() {
        return calendar;
    }

    public void setCalendar(TMCalendar calendar) {
        this.calendar = calendar;
    }

    public String getCalendarDD() {
        return "Employee_calendar";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeType">
    private boolean employeeType = false;

    public boolean isEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(boolean employeeType) {
        this.employeeType = employeeType;
    }

    public String getEmployeeTypeDD() {
        return "Employee_employeeType";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="dependances">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeDependance> dependances;

    public List<EmployeeDependance> getDependances() {
        return dependances;
    }

    public void setDependances(List<EmployeeDependance> dependances) {
        this.dependances = dependances;
    }

    public String getDependancesDD() {
        return "Employee_dependances";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="leaveDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date leaveDate;

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getLeaveDateDD() {
        return "Employee_leaveDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="terminationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date terminationDate;

    public Date getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    public String getTerminationDateDD() {
        return "Employee_terminationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prevousExperienceYears">
    @Column
    private Integer prevousExperienceYears;

    public Integer getPrevousExperienceYears() {
        return prevousExperienceYears;
    }

    public void setPrevousExperienceYears(Integer prevousExperienceYears) {
        this.prevousExperienceYears = prevousExperienceYears;
    }

    public String getPrevousExperienceYearsDD() {
        return "Employee_prevousExperienceYears";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="prevousExperienceMonths">
    @Column
    private Integer prevousExperienceMonths;

    public Integer getPrevousExperienceMonths() {
        return prevousExperienceMonths;
    }

    public void setPrevousExperienceMonths(Integer prevousExperienceMonths) {
        this.prevousExperienceMonths = prevousExperienceMonths;
    }

    public String getPrevousExperienceMonthsDD() {
        return "Employee_prevousExperienceMonths";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="joiningDate">
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date joiningDate;

    public Date getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getJoiningDateDD() {
        return "Employee_joiningDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="groupJoiningData">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date groupJoiningDate;//name = "last_vacation_date"

    public Date getGroupJoiningDate() {
        return groupJoiningDate;
    }

    public void setGroupJoiningDate(Date groupJoiningDate) {
        this.groupJoiningDate = groupJoiningDate;
    }

    public String getGroupJoiningDateDD() {
        return "Employee_groupJoiningDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="workingHours">
    //Salema[Employee New Design]==> from Nairouz
    @Column(precision = 25, scale = 13)
    private BigDecimal workingHours;

    public BigDecimal getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(BigDecimal workingHours) {
        this.workingHours = workingHours;
    }

    public String getWorkingHoursDD() {
        return "Employee_workingHours";
    }
    @Transient
    private BigDecimal workingHoursMask;

    public BigDecimal getWorkingHoursMask() {
        workingHoursMask = workingHours;
        return workingHoursMask;
    }

    public void setWorkingHoursMask(BigDecimal workingHoursMask) {
        updateDecimalValue("workingHours", workingHoursMask);
    }

    public String getWorkingHoursMaskDD() {
        return "Employee_workingHoursMask";
    }
    // </editor-fold >
    
    //The following attributes are added from PersonBase to match new employee design
    @Translatable(translationField = "addressTranslated")
    private String address;

    public String getAddressTranslatedDD() {
        return "Employee_address";
    }
    @Transient
    @Translation(originalField = "address")
    private String addressTranslated;

    public String getAddressTranslated() {
        return addressTranslated;
    }

    public void setAddressTranslated(String addressTranslated) {
        this.addressTranslated = addressTranslated;
    }
    @Translatable(translationField = "address1Translated")
    private String address1;

    public String getAddress1TranslatedDD() {
        return "Employee_address1";
    }
    @Transient
    @Translation(originalField = "address1")
    private String address1Translated;

    public String getAddress1Translated() {
        return address1Translated;
    }

    public void setAddress1Translated(String address1Translated) {
        this.address1Translated = address1Translated;
    }
    private String phone;

    public String getPhoneDD() {
        return "Employee_phone";
    }
    private String phone1;

    public String getPhone1DD() {
        return "Employee_phone1";
    }
    public String phone1Enc;

    public String getPhone1Enc() {
        return phone1Enc;
    }

    public void setPhone1Enc(String phone1Enc) {
        this.phone1Enc = phone1Enc;
    }

    private String mobile;

    public String getMobileDD() {
        return "Employee_mobile";
    }
    private String mobile1;

    public String getMobile1DD() {
        return "Employee_mobile1";
    }

    private String mobile1Enc;

    public String getMobile1Enc() {
        return mobile1Enc;
    }

    public void setMobile1Enc(String mobile1Enc) {
        this.mobile1Enc = mobile1Enc;
    }

    private String email;

    public String getEmailDD() {
        return "Employee_email";
    }
    private String email1;

    public String getEmail1DD() {
        return "Employee_email1";
    }
    @Translatable(translationField = "additionalInformationTranslated")
    private String additionalInformation;

    public String getAdditionalInformationTranslatedDD() {
        return "Employee_additionalInformation";
    }
    @Transient
    @Translation(originalField = "additionalInformation")
    private String additionalInformationTranslated;

    public String getAdditionalInformationTranslated() {
        return additionalInformationTranslated;
    }

    public void setAdditionalInformationTranslated(String additionalInformationTranslated) {
        this.additionalInformationTranslated = additionalInformationTranslated;
    }
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthdate;

    public String getBirthdateDD() {
        return "Employee_birthdate";
    }
    // attributes from direct assosiation.
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC birthPlace;

    public UDC getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(UDC birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthPlaceDD() {
        return "Employee_birthPlace";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC religion;

    public UDC getReligion() {
        return religion;
    }

    public void setReligion(UDC religion) {
        this.religion = religion;
    }

    public String getReligionDD() {
        return "Employee_religion";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC gender;

    public String getGenderDD() {
        return "Employee_gender";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nationality;

    public String getNationalityDD() {
        return "Employee_nationality";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC city;

    public String getCityDD() {
        return "Employee_city";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC country;

    public String getCountryDD() {
        return "Employee_country";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC maritalStatus;

    public String getMaritalStatusDD() {
        return "Employee_maritalStatus";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC militaryStatus;

    public String getMilitaryStatusDD() {
        return "Employee_militaryStatus";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC salute;

    public String getSaluteDD() {
        return "Employee_salute";
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public UDC getCity() {
        return city;
    }

    public void setCity(UDC city) {
        this.city = city;
    }

    public UDC getCountry() {
        return country;
    }

    public void setCountry(UDC country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;

    }

    public UDC getGender() {
        return gender;
    }

    public void setGender(UDC gender) {
        this.gender = gender;
    }

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public UDC getMilitaryStatus() {
        return militaryStatus;
    }

    public void setMilitaryStatus(UDC militaryStatus) {
        this.militaryStatus = militaryStatus;
    }

    public String getMobile() {
        return mobile;

    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile1() {
        return mobile1;

    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
    }

    public UDC getNationality() {
        return nationality;
    }

    public void setNationality(UDC nationality) {
        this.nationality = nationality;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public UDC getSalute() {
        return salute;
    }

    public void setSalute(UDC salute) {
        this.salute = salute;
    }
    // <editor-fold defaultstate="collapsed" desc="firstName">
    @Column
    @Translatable(translationField = "firstNameTranslated")
    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstNameDD() {
        return "Employee_firstName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="middleName">
    @Column
    @Translatable(translationField = "middleNameTranslated")
    private String middleName;

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleNameDD() {
        return "Employee_middleName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastName">
    @Column
    @Translatable(translationField = "lastNameTranslated")
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastNameDD() {
        return "Employee_lastName";
    }
    // </editor-fold>

    public String getFirstNameTranslatedDD() {
        return "Employee_firstName";
    }
    @Transient
    @Translation(originalField = "firstName")
    private String firstNameTranslated;

    public String getFirstNameTranslated() {
        return firstNameTranslated;
    }

    public void setFirstNameTranslated(String firstNameTranslated) {
        this.firstNameTranslated = firstNameTranslated;

    }
    @Transient
    @Translation(originalField = "middleName")
    private String middleNameTranslated;

    public String getMiddleNameTranslated() {
        return middleNameTranslated;
    }

    public void setMiddleNameTranslated(String middleNameTranslated) {
        this.middleNameTranslated = middleNameTranslated;

    }

    public String getMiddleNameTranslatedDD() {
        return "Employee_middleName";
    }

    public String getLastNameTranslatedDD() {
        return "Employee_lastName";
    }
    @Transient
    @Translation(originalField = "lastName")
    private String lastNameTranslated;

    public String getLastNameTranslated() {
        return lastNameTranslated;
    }

    public void setLastNameTranslated(String lastNameTranslated) {
        this.lastNameTranslated = lastNameTranslated;

    }
    //New attribute EmployeePhoto instead of PersonPhoto to match new employee design
    @OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
    private EmployeePhoto employeePhoto;

    public EmployeePhoto getEmployeePhoto() {
        return employeePhoto;
    }

    public void setEmployeePhoto(EmployeePhoto employeePhoto) {
        this.employeePhoto = employeePhoto;
    }

    public String getEmployeePhotoDD() {
        return "Employee_employeePhoto";
    }
    //<editor-fold defaultstate="collapsed" desc="otherInfo">
    @OneToOne(mappedBy = "employee", fetch = FetchType.LAZY)
    private EmployeeOtherInfo otherInfo;

    public EmployeeOtherInfo getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(EmployeeOtherInfo otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getOtherInfoDD() {
        return "Employee_otherInfo";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="employeeplanners">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeAnnualPlanner> employeeplanners;

    public List<EmployeeAnnualPlanner> getEmployeeplanners() {
        return employeeplanners;
    }

    public void setEmployeeplanners(List<EmployeeAnnualPlanner> employeeplanners) {
        this.employeeplanners = employeeplanners;
    }

    public String getEmployeeplannersDD() {
        return "Employee_employeeplanners";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="timeSheets">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeTimeSheet> timeSheets;

    public List<EmployeeTimeSheet> getTimeSheets() {
        return timeSheets;
    }

    public void setTimeSheets(List<EmployeeTimeSheet> timeSheets) {
        this.timeSheets = timeSheets;
    }

    public String getTimeSheetsDD() {
        return "Employee_timeSheets";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="employeeCostCenters">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeCostCenter> employeeCostCenters;

    public List<EmployeeCostCenter> getEmployeeCostCenters() {
        return employeeCostCenters;
    }

    public void setEmployeeCostCenters(List<EmployeeCostCenter> employeeCostCenters) {
        this.employeeCostCenters = employeeCostCenters;
    }

    public String getEmployeeCostCentersDD() {
        return "Employee_employeeCostCenters";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="empCostCenterExceptions">
    @OneToMany(mappedBy = "employee")
    private List<EmpCostCenterException> empCostCenterExceptions;

    public List<EmpCostCenterException> getEmpCostCenterExceptions() {
        return empCostCenterExceptions;
    }

    public void setEmpCostCenterExceptions(List<EmpCostCenterException> empCostCenterExceptions) {
        this.empCostCenterExceptions = empCostCenterExceptions;
    }

    public String getEmpCostCenterExceptionsDD() {
        return "Employee_empCostCenterExceptions";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empVacHists">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeVacationHistory> empVacHists;

    public List<EmployeeVacationHistory> getEmpVacHists() {
        return empVacHists;
    }

    public void setEmpVacHists(List<EmployeeVacationHistory> empVacHists) {
        this.empVacHists = empVacHists;
    }

    public String getEmpVacHistsDD() {
        return "Employee_empVacHists";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="formulaApplied">
    @Column(length = 1)
    private String formulaApplied = null;

    public String getFormulaApplied() {
        return formulaApplied;
    }

    public void setFormulaApplied(String formulaApplied) {
        this.formulaApplied = formulaApplied;
    }

    public String getFormulaAppliedDD() {
        return "Employee_formulaApplied";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeMedical">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "employee")
    private EmployeeMedical employeeMedical;

    public EmployeeMedical getEmployeeMedical() {
        return employeeMedical;
    }

    public void setEmployeeMedical(EmployeeMedical employeeMedical) {
        this.employeeMedical = employeeMedical;
    }

    public String getEmployeeMedicalDD() {
        return "Employee_employeeMedical";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="isManager">
    private boolean manager;

    public boolean isManager() {
        return manager;
    }

    public String GetManagerDD() {
        return "Employee_manager";
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="leaveReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    public EOSLeaveReason leaveReason;

    public EOSLeaveReason getLeaveReason() {
        return leaveReason;
    }

    public String getLeaveReasonDD() {
        return "Employee_leaveReason";
    }

    public void setLeaveReason(EOSLeaveReason leaveReason) {
        this.leaveReason = leaveReason;
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapsed" desc="successionPlans">
    @OneToMany(mappedBy = "employee")
    private List<SuccessionPlan> successionPlans;

    public List<SuccessionPlan> getSuccessionPlans() {
        return successionPlans;
    }

    public void setSuccessionPlans(List<SuccessionPlan> successionPlans) {
        this.successionPlans = successionPlans;
    }

    public String getSuccessionPlansDD() {
        return "Employee_successionPlans";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeProject">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeProject> employeeProject;

    public List<EmployeeProject> getEmployeeProject() {
        return employeeProject;
    }

    public void setEmployeeProject(List<EmployeeProject> employeeProject) {
        this.employeeProject = employeeProject;
    }

    public String getEmployeePorjectDD() {
        return "Employee_employeeProjectDD";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="employeeSapIntegerationProject">
    @OneToMany(fetch = javax.persistence.FetchType.LAZY)
    private List<EmployeeSapIntegerationProject> employeeSapIntegerationProject;

    public List<EmployeeSapIntegerationProject> getEmployeeSapIntegerationProject() {
        return employeeSapIntegerationProject;
    }

    public void setEmployeeSapIntegerationProject(List<EmployeeSapIntegerationProject> employeeSapIntegerationProject) {
        this.employeeSapIntegerationProject = employeeSapIntegerationProject;
    }

    public String getEmployeeSapIntegerationProjectDD() {
        return "Employee_employeeSapIntegerationProject";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="employeeSapIntegerationCostCode">
    @OneToMany(fetch = javax.persistence.FetchType.LAZY)
    private List<EmployeeSapIntegerationCostCode> employeeSapIntegerationCostCode;

    public List<EmployeeSapIntegerationCostCode> getEmployeeSapIntegerationCostCode() {
        return employeeSapIntegerationCostCode;
    }

    public void setEmployeeSapIntegerationCostCode(List<EmployeeSapIntegerationCostCode> employeeSapIntegerationCostCode) {
        this.employeeSapIntegerationCostCode = employeeSapIntegerationCostCode;
    }

    public String getEmployeeSapIntegerationCostCodeDD() {
        return "Employee_employeeSapIntegerationCostCode";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="additionalInfo">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            mappedBy = "employee")
    private EmployeeAdditionalInfo additionalInfo;

    public void setAdditionalInfo(EmployeeAdditionalInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public EmployeeAdditionalInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public String getAdditionalInfoDD() {
        return "Employee_additionalInfo";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="employeeEducationHistories">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeEducationHistory> employeeEducationHistories;

    public List<EmployeeEducationHistory> getEmployeeEducationHistories() {
        return employeeEducationHistories;
    }

    public void setEmployeeEducationHistories(List<EmployeeEducationHistory> employeeEducationHistories) {
        this.employeeEducationHistories = employeeEducationHistories;
    }

    public String getEmployeeEducationHistoriesDD() {
        return "Employee_employeeEducationHistories";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapse" desc="employeePerformanceEvaluation">
    @OneToMany(mappedBy = "employee")
    private List<PerformanceEvaluation> employeePerformanceEvaluations;

    public List<PerformanceEvaluation> getEmployeePerformanceEvaluations() {
        return employeePerformanceEvaluations;
    }

    public void setEmployeePerformanceEvaluations(List<PerformanceEvaluation> employeePerformanceEvaluations) {
        this.employeePerformanceEvaluations = employeePerformanceEvaluations;
    }

    public String getEmployeePerformanceEvaluationsDD() {
        return "Employee_employeePerformanceEvaluations";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapse" desc="employeeCertificate">
    @OneToMany(mappedBy = "employee")
    private List<EmployeeCertificate> employeeCertificates;

    public List<EmployeeCertificate> getEmployeeCertificates() {
        return employeeCertificates;
    }

    public void setEmployeeCertificates(List<EmployeeCertificate> employeeCertificates) {
        this.employeeCertificates = employeeCertificates;
    }

    public String getEmployeeCertificatesDD() {
        return "Employee_employeeCertificates";
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Qualifications">
    @OneToMany(mappedBy = "employee")
    private List<Qualification> qualifications;

    public void setQualifications(List<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    public List<Qualification> getQualifications() {
        return qualifications;
    }

    public String getQualificationsDD() {
        return "Employee_qualifications";
    }
    //</editor-fold >
    //<editor-fold defaultstate="collapse" desc="employeeVacationSettlements">
    @OneToMany(mappedBy = "employee")
    private List<VacationSettlement> vacationSettlements;

    public List<VacationSettlement> getVacationSettlements() {
        return vacationSettlements;
    }

    public void setVacationSettlements(List<VacationSettlement> vacationSettlements) {
        this.vacationSettlements = vacationSettlements;
    }

    public String getVacationSettlementsDD() {
        return "Employee_vacationSettlements";
    }
    //</editor-fold>
    private String employeePIN;

    public String getEmployeePIN() {
        return employeePIN;
    }

    public void setEmployeePIN(String employeePIN) {
        this.employeePIN = employeePIN;

    }

    public String getEmployeePINDD() {
        return "Employee_employeePIN";

    }
    // <editor-fold defaultstate="collapsed" desc="hiringDateEnc">
    private String hiringDateEnc;

    public String getHiringDateEnc() {
        return hiringDateEnc;
    }

    public void setHiringDateEnc(String hiringDateEnc) {
//        this.hiringDateEnc = hiringDateEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="birthDateEnc">
    private String birthDateEnc;

    public String getBirthDateEnc() {
        return birthDateEnc;
    }

    public void setBirthDateEnc(String birthDateEnc) {
        this.birthDateEnc = birthDateEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="arabicNameEnc">
    private String arabicNameEnc;

    public String getArabicNameEnc() {
        return arabicNameEnc;
    }

    public void setArabicNameEnc(String arabicNameEnc) {
        this.arabicNameEnc = arabicNameEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="englishNameEnc">
    private String englishNameEnc;

    public String getEnglishNameEnc() {
        return englishNameEnc;
    }

    public void setEnglishNameEnc(String englishNameEnc) {
        this.englishNameEnc = englishNameEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="natIDEnc">
    private String natIDEnc;

    public String getNatIDEnc() {
        return natIDEnc;
    }

    public void setNatIDEnc(String natIDEnc) {
        this.natIDEnc = natIDEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="phoneEnc">
    private String phoneEnc;

    public String getPhoneEnc() {
        return phoneEnc;
    }

    public void setPhoneEnc(String phoneEnc) {
        this.phoneEnc = phoneEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mobileEnc">
    private String mobileEnc;

    public String getMobileEnc() {
        return mobileEnc;
    }

    public void setMobileEnc(String mobileEnc) {
        this.mobileEnc = mobileEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="emailEnc">
    private String emailEnc;

    public String getEmailEnc() {
        return emailEnc;
    }

    public void setEmailEnc(String emailEnc) {
        this.emailEnc = emailEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="email1Enc">
    private String email1Enc;

    public String getEmail1Enc() {
        return email1Enc;
    }

    public void setEmail1Enc(String email1Enc) {
        this.email1Enc = email1Enc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="addressEnc">
    private String addressEnc;

    public String getAddressEnc() {
        return addressEnc;
    }

    public void setAddressEnc(String addressEnc) {
        this.addressEnc = addressEnc;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ancestor">
    @Transient
    //@ManyToOne
    private Position ancestor;

    public Position getAncestor() {
        Position tempPosition;
        tempPosition = getPositionSimpleMode().getParentPositionSimpleMode();
        if (tempPosition == null) {
            ancestor = getPositionSimpleMode();
        } else if (tempPosition.getHierarchycode().equals("AA0")) {
            ancestor = getPositionSimpleMode();
        } else {
            while (!tempPosition.getHierarchycode().equals("AA0")) {
                ancestor = tempPosition;
                tempPosition = tempPosition.getParentPositionSimpleMode();
            }
        }
        return ancestor;
    }

    public String getAncestorDD() {
        return "Employee_ancestor";
    }

    public void setAncestor(Position ancestor) {
        this.ancestor = ancestor;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="secondManager">
    @ManyToOne
    private Employee secondManager;

    public Employee getSecondManager() {
        return secondManager;
    }

    public void setSecondManager(Employee secondManager) {
        this.secondManager = secondManager;
    }

    public String getSecondManagerDD() {
        return "Employee_secondManager";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="thirdManager">
    @ManyToOne
    private Employee thirdManager;

    public Employee getThirdManager() {
        return thirdManager;
    }

    public void setThirdManager(Employee thirdManager) {
        this.thirdManager = thirdManager;
    }

    public String getThirdManagerDD() {
        return "Employee_thirdManager";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fourthManager">
    @ManyToOne
    private Employee fourthManager;

    public Employee getFourthManager() {
        return fourthManager;
    }

    public void setFourthManager(Employee fourthManager) {
        this.fourthManager = fourthManager;
    }

    public String getFourthManagerDD() {
        return "Employee_fourthManager";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fifthManager">
    @ManyToOne
    private Employee fifthManager;

    public Employee getFifthManager() {
        return fifthManager;
    }

    public void setFifthManager(Employee fifthManager) {
        this.fifthManager = fifthManager;
    }

    public String getFifthManagerDD() {
        return "Employee_fifthManager";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "Employee_type";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="driverType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC driverType;

    public UDC getDriverType() {
        return driverType;
    }

    public void setDriverType(UDC driverType) {
        this.driverType = driverType;
    }

    public String getDriverTypeDD() {
        return "Employee_driverType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="freezeReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC freezeReason;

    public UDC getFreezeReason() {
        return freezeReason;
    }

    public void setFreezeReason(UDC freezeReason) {
        this.freezeReason = freezeReason;
    }

    public String getFreezeReasonDD() {
        return "Employee_freezeReason";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="freeze">
    @Column
    private String freeze;

    public String getFreeze() {
        return freeze;
    }

    public void setFreeze(String freeze) {
        this.freeze = freeze;
    }

    public String getFreezeDD() {
        return "Employee_freeze";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="freezeDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date freezeDate;

    public Date getFreezeDate() {
        return freezeDate;
    }

    public void setFreezeDate(Date freezeDate) {
        this.freezeDate = freezeDate;
    }

    public String getFreezeDateDD() {
        return "Employee_freezeDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="freezeFactor">
    @Column(precision = 20, scale = 13)
    private BigDecimal freezeFactor;

    public BigDecimal getFreezeFactor() {
        return freezeFactor;
    }

    public void setFreezeFactor(BigDecimal freezeFactor) {
        this.freezeFactor = freezeFactor;
    }

    public String getFreezeFactorDD() {
        return "Employee_freezeFactor";
    }
    @Transient
    private BigDecimal freezeFactorMask;

    public BigDecimal getFreezeFactorMask() {
        freezeFactorMask = freezeFactor;
        return freezeFactorMask;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="housingAllowanceEligibility">
    @Column(length = 1)
    private String housingAllowanceEligibility;

    public String getHousingAllowanceEligibility() {
        return housingAllowanceEligibility;
    }

    public void setHousingAllowanceEligibility(String housingAllowanceEligibility) {
        this.housingAllowanceEligibility = housingAllowanceEligibility;
    }

    public String getHousingAllowanceEligibilityDD() {
        return "Employee_housingAllowanceEligibility";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nationalIDIssueDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date nationalIDIssueDate;

    public Date getNationalIDIssueDate() {
        return nationalIDIssueDate;
    }

    public void setNationalIDIssueDate(Date nationalIDIssueDate) {
        this.nationalIDIssueDate = nationalIDIssueDate;
    }

    public String getNationalIDIssueDateDD() {
        return "Employee_nationalIDIssueDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nationalIDExpirationDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date nationalIDExpirationDate;

    public Date getNationalIDExpirationDate() {
        return nationalIDExpirationDate;
    }

    public void setNationalIDExpirationDate(Date nationalIDExpirationDate) {
        this.nationalIDExpirationDate = nationalIDExpirationDate;
    }

    public String getNationalIDExpirationDateDD() {
        return "Employee_nationalIDExpirationDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="actualAddress">
    private String actualAddress;

    public String getActualAddress() {
        return actualAddress;
    }

    public void setActualAddress(String actualAddress) {
        this.actualAddress = actualAddress;
    }

    public String getActualAddressDD() {
        return "Employee_actualAddress";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="militaryCardNumber">
    private String militaryCardNumber;

    public String getMilitaryCardNumber() {
        return militaryCardNumber;
    }

    public void setMilitaryCardNumber(String militaryCardNumber) {
        this.militaryCardNumber = militaryCardNumber;
    }

    public String getMilitaryCardNumberDD() {
        return "Employee_militaryCardNumber";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="militaryEndDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date militaryEndDate;

    public Date getMilitaryEndDate() {
        return militaryEndDate;
    }

    public void setMilitaryEndDate(Date militaryEndDate) {
        this.militaryEndDate = militaryEndDate;
    }

    public String getMilitaryEndDateDD() {
        return "Employee_militaryEndDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="passportNumber">
    private String passportNumber;

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportNumberDD() {
        return "Employee_passportNumber";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="passportIssueDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date passportIssueDate;

    public Date getPassportIssueDate() {
        return passportIssueDate;
    }

    public void setPassportIssueDate(Date passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }

    public String getPassportIssueDateDD() {
        return "Employee_passportIssueDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="passportExpirationDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date passportExpirationDate;

    public Date getPassportExpirationDate() {
        return passportExpirationDate;
    }

    public void setPassportExpirationDate(Date passportExpirationDate) {
        this.passportExpirationDate = passportExpirationDate;
    }

    public String getPassportExpirationDateDD() {
        return "Employee_passportExpirationDate";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="handicap">
    private boolean handicap;

    public boolean isHandicap() {
        return handicap;
    }

    public void setHandicap(boolean handicap) {
        this.handicap = handicap;
    }

    public String getHandicapDD() {
        return "Employee_handicap";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="bloodType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC bloodType;

    public UDC getBloodType() {
        return bloodType;
    }

    public void setBloodType(UDC bloodType) {
        this.bloodType = bloodType;
    }

    public String getBloodTypeDD() {
        return "Employee_bloodType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="professionalEmail">
    private String professionalEmail;

    public String getProfessionalEmail() {
        return professionalEmail;
    }

    public void setProfessionalEmail(String professionalEmail) {
        this.professionalEmail = professionalEmail;
    }

    public String getProfessionalEmailDD() {
        return "Employee_professionalEmail";
    }
    // </editor-fold >

    // <editor-fold defaultstate="collapsed" desc="groupHiringDate">
    @Temporal(TemporalType.TIMESTAMP)
    private Date groupHiringDate;

    public Date getGroupHiringDate() {
        return groupHiringDate;
    }

    public void setGroupHiringDate(Date groupHiringDate) {
        this.groupHiringDate = groupHiringDate;
    }

    public String getGroupHiringDateDD() {
        return "Employee_groupHiringDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="busRoute">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC busRoute;

    public UDC getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(UDC busRoute) {
        this.busRoute = busRoute;
    }

    public String getBusRouteDD() {
        return "Employee_busRoute";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="medicalCategory">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC medicalCategory;

    public UDC getMedicalCategory() {
        return medicalCategory;
    }

    public void setMedicalCategory(UDC medicalCategory) {
        this.medicalCategory = medicalCategory;
    }

    public String getMedicalCategoryDD() {
        return "Employee_medicalCategory";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="familyName">
    @Column
    @Translatable(translationField = "familyNameTranslated")
    private String familyName;

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = firstName;
    }

    public String getFamilyNameDD() {
        return "Employee_familyName";
    }
    // </editor-fold>
    
    @Transient
    @Translation(originalField = "familyName")
    private String familyNameTranslated;

    public String getFamilyNameTranslated() {
        return familyNameTranslated;
    }

    public void setFamilyNameTranslated(String familyNameTranslated) {
        this.familyNameTranslated = familyNameTranslated;

    }

    public String getFamilyNameTranslatedDD() {
        return "Employee_familyNameTranslated";
    }
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Factory factory;

    public Factory getFactory() {
        return factory;
    }

    public void setFactory(Factory factory) {
        this.factory = factory;
    }

    public String getFactoryDD() {
        return "Employee_factory";
    }


    private String mobile2;

    public String getMobile2DD() {
        return "Employee_mobile2";
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

}
