/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"activity"})
public class ActivityPreActivity extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="activity">
	@JoinColumn(nullable=false)
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getActivityDD() {
        return "ActivityPreActivity_activity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="preActivity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Activity preActivity;

    public Activity getPreActivity() {
        return preActivity;
    }

    public void setPreActivity(Activity preActivity) {
        this.preActivity = preActivity;
    }

    public String getPreActivityDD() {
        return "ActivityPreActivity_preActivity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="operator">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC operator;

    public UDC getOperator() {
        return operator;
    }

    public void setOperator(UDC operator) {
        this.operator = operator;
    }

    public String getOperatorDD() {
        return "ActivityPreActivity_operator";
    }
    // </editor-fold>
}