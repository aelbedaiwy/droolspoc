/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"trainingRequest"})
public class TrainingAttendance extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="trainingRequest">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private TrainingRequest trainingRequest;

    public TrainingRequest getTrainingRequest() {
        return trainingRequest;
    }

    public void setTrainingRequest(TrainingRequest trainingRequest) {
        this.trainingRequest = trainingRequest;
    }

    public String getTrainingRequestDD() {
        return "TrainingAttendance_trainingRequest";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dayDate;

    public Date getDayDate() {
        return dayDate;
    }

    public void setDayDate(Date dayDate) {
        this.dayDate = dayDate;
    }
    
    public String getDayDateDD() {
        return "TrainingAttendance_dayDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromTime">
    @Column(name = "from_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromTime;

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }
    
    public String getFromTimeDD() {
        return "TrainingAttendance_fromTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toTime;

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public String getToTimeDD() {
        return "TrainingAttendance_toTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attended">
    @Column
    private boolean attended;

    public boolean isAttended() {
        return attended;
    }

    public void setAttended(boolean attended) {
        this.attended = attended;
    }
    public String getAttended() {
        return "TrainingAttendance_attended";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="absenceReason">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC absenceReason;

    public UDC getAbsenceReason() {
        return absenceReason;
    }

    public void setAbsenceReason(UDC absenceReason) {
        this.absenceReason = absenceReason;
    }

    public String getAbsenceReasonDD() {
        return "TrainingAttendance_absenceReason";
    }
    // </editor-fold>
}
