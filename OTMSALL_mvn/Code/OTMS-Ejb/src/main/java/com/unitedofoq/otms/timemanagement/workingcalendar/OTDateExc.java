/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lahmed
 */
@Entity
@Table(name="OTSegment")
@ParentEntity(fields={"dateException"})
public class OTDateExc extends OTSegmentBase {
    
    
    //<editor-fold defaultstate="collapsed" desc="dateException">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private DateCalendarException dateException;

    public DateCalendarException getDateException() {
        return dateException;
    }

    public void setDateException(DateCalendarException dateException) {
        this.dateException = dateException;
    }

    public String getDateExceptionDD() {
        return "OTDateExc_dateException";
    }
    //</editor-fold>
    
}