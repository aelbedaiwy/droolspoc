
package com.unitedofoq.otms.foundation.currency;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class CurrencyTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>

}
