package com.unitedofoq.otms.recruitment.onlineexam;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@ChildEntity(fields = {"mcqQuestions", "essayQuestions"})
public class Exam extends BusinessObjectBaseEntity {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getDescriptionTranslatedDD() {
        return "Exam_description";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    @Column(nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "Exam_name";
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="duration">
    private int duration;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDurationDD() {
        return "Exam_duration";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "Exam_type";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="company">

    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Exam_company";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="mcqQuestions">
    @OneToMany(mappedBy = "exam")
    private List<MCQuestion> mcqQuestions;

    public List<MCQuestion> getMcqQuestions() {
        return mcqQuestions;
    }

    public void setMcqQuestions(List<MCQuestion> mcqQuestions) {
        this.mcqQuestions = mcqQuestions;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="essayQuestions">
    @OneToMany(mappedBy = "exam")
    private List<EssayQuestion> essayQuestions;

    public List<EssayQuestion> getEssayQuestions() {
        return essayQuestions;
    }

    public void setEssayQuestions(List<EssayQuestion> essayQuestions) {
        this.essayQuestions = essayQuestions;
    }
    //</editor-fold>
}
