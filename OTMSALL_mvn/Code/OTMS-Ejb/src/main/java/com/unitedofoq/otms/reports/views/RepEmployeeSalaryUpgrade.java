
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.util.Date;
import javax.persistence.*;
import java.math.BigDecimal;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepEmployeeSalaryUpgrade extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmployeeSalaryUpgrade_genderDescription";
    }
    
    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }
    
    public String getGenderDescriptionTranslatedDD() {
        return "RepEmployeeSalaryUpgrade_genderDescription";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="valueBefore">
    @Column
    private BigDecimal valueBefore;

    public void setValueBefore(BigDecimal valueBefore) {
        this.valueBefore = valueBefore;
    }

    public BigDecimal getValueBefore() {
        return valueBefore;
    }

    public String getValueBeforeDD() {
        return "RepEmployeeSalaryUpgrade_valueBefore";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="valueAfter">
    @Column
    private BigDecimal valueAfter;

    public void setValueAfter(BigDecimal valueAfter) {
        this.valueAfter = valueAfter;
    }

    public BigDecimal getValueAfter() {
        return valueAfter;
    }

    public String getValueAfterDD() {
        return "RepEmployeeSalaryUpgrade_valueAfter";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="income">
    @Column
    @Translatable(translationField = "incomeTranslated")
    private String income;

    public void setIncome(String income) {
        this.income = income;
    }

    public String getIncome() {
        return income;
    }

    public String getIncomeDD() {
        return "RepEmployeeSalaryUpgrade_income";
    }
    
    @Transient
    @Translation(originalField = "income")
    private String incomeTranslated;

    public String getIncomeTranslated() {
        return incomeTranslated;
    }

    public void setIncomeTranslated(String incomeTranslated) {
        this.incomeTranslated = incomeTranslated;
    }
    
    public String getIncomeTranslatedDD() {
        return "RepEmployeeSalaryUpgrade_income";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="upgradeFactor">
    @Column
    private BigDecimal upgradeFactor;

    public void setUpgradeFactor(BigDecimal upgradeFactor) {
        this.upgradeFactor = upgradeFactor;
    }

    public BigDecimal getUpgradeFactor() {
        return upgradeFactor;
    }

    public String getUpgradeFactorDD() {
        return "RepEmployeeSalaryUpgrade_upgradeFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="applied">
    @Column
    private String applied;

    public void setApplied(String applied) {
        this.applied = applied;
    }

    public String getApplied() {
        return applied;
    }

    public String getAppliedDD() {
        return "RepEmployeeSalaryUpgrade_applied";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fromCalcPeriodDescription">
    @Column
    //@Translatable(translationField = "fromCalcPeriodDescriptionTranslated")
    private String fromCalcPeriodDescription;

    public void setFromCalcPeriodDescription(String fromCalcPeriodDescription) {
        this.fromCalcPeriodDescription = fromCalcPeriodDescription;
    }

    public String getFromCalcPeriodDescription() {
        return fromCalcPeriodDescription;
    }

    public String getFromCalcPeriodDescriptionDD() {
        return "RepEmployeeSalaryUpgrade_fromCalcPeriodDescription";
    }
    
//    @Transient
//    @Translation(originalField = "fromCalcPeriodDescription")
//    private String fromCalcPeriodDescriptionTranslated;
//
//    public String getFromCalcPeriodDescriptionTranslated() {
//        return fromCalcPeriodDescriptionTranslated;
//    }
//
//    public void setFromCalcPeriodDescriptionTranslated(String fromCalcPeriodDescriptionTranslated) {
//        this.fromCalcPeriodDescriptionTranslated = fromCalcPeriodDescriptionTranslated;
//    }
//    
//    public String getFromCalcPeriodDescriptionTranslatedDD() {
//        return "RepEmployeeSalaryUpgrade_fromCalcPeriodDescription";
//    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="toCalcPeriodDescription">
    @Column
    //@Translatable(translationField = "toCalcPeriodDescriptionTranslated")    
    private String toCalcPeriodDescription;

    public void setToCalcPeriodDescription(String toCalcPeriodDescription) {
        this.toCalcPeriodDescription = toCalcPeriodDescription;
    }

    public String getToCalcPeriodDescription() {
        return toCalcPeriodDescription;
    }

    public String getToCalcPeriodDescriptionDD() {
        return "RepEmployeeSalaryUpgrade_toCalcPeriodDescription";
    }
    
//    @Transient
//    @Translation(originalField = "toCalcPeriodDescription")
//    private String toCalcPeriodDescriptionTranslated;
//
//    public String getToCalcPeriodDescriptionTranslated() {
//        return toCalcPeriodDescriptionTranslated;
//    }
//
//    public void setToCalcPeriodDescriptionTranslated(String toCalcPeriodDescriptionTranslated) {
//        this.toCalcPeriodDescriptionTranslated = toCalcPeriodDescriptionTranslated;
//    }
//    
//    public String getToCalcPeriodDescriptionTranslatedDD() {
//        return "RepEmployeeSalaryUpgrade_toCalcPeriodDescription";
//    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fromCalcPeriodDbid">
    @Column
    //@Translatable(translationField = "fromCalcPeriodDbidTranslated")
    private String fromCalcPeriodDbid;

    public void setFromCalcPeriodDbid(String fromCalcPeriodDbid) {
        this.fromCalcPeriodDbid = fromCalcPeriodDbid;
    }

    public String getFromCalcPeriodDbid() {
        return fromCalcPeriodDbid;
    }

    public String getFromCalcPeriodDbidDD() {
        return "RepEmployeeSalaryUpgrade_fromCalcPeriodDbid";
    }
    
//    @Transient
//    @Translation(originalField = "fromCalcPeriodDbid")
//    private String fromCalcPeriodDbidTranslated;
//
//    public String getFromCalcPeriodDbidTranslated() {
//        return fromCalcPeriodDbidTranslated;
//    }
//
//    public void setFromCalcPeriodDbidTranslated(String fromCalcPeriodDbidTranslated) {
//        this.fromCalcPeriodDbidTranslated = fromCalcPeriodDbidTranslated;
//    }
//    
//    public String getFromCalcPeriodDbidTranslatedDD() {
//        return "RepEmployeeSalaryUpgrade_fromCalcPeriodDbid";
//    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private Long dsDbid;

    public void setDsDbid(Long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public Long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeSalaryUpgrade_dsDbid";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="toCalcPeriodDbid">
    @Column
    //@Translatable(translationField = "toCalcPeriodDbidDescription")
    private String toCalcPeriodDbid;

    public void setToCalcPeriodDbid(String toCalcPeriodDbid) {
        this.toCalcPeriodDbid = toCalcPeriodDbid;
    }

    public String getToCalcPeriodDbid() {
        return toCalcPeriodDbid;
    }

    public String getToCalcPeriodDbidDD() {
        return "RepEmployeeSalaryUpgrade_toCalcPeriodDbid";
    }
    
//    @Transient
//    @Translation(originalField = "toCalcPeriodDbid")
//    private String toCalcPeriodDbidDescription;
//
//    public String getToCalcPeriodDbidDescription() {
//        return toCalcPeriodDbidDescription;
//    }
//
//    public void setToCalcPeriodDbidDescription(String toCalcPeriodDbidDescription) {
//        this.toCalcPeriodDbidDescription = toCalcPeriodDbidDescription;
//    }
//    
//    public String getToCalcPeriodDbidDescriptionDD() {
//        return "RepEmployeeSalaryUpgrade_toCalcPeriodDbid";
//    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="applyDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date applyDate;

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public String getApplyDateDD() {
        return "RepEmployeeSalaryUpgrade_applyDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="givenDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date givenDate;

    public void setGivenDate(Date givenDate) {
        this.givenDate = givenDate;
    }

    public Date getGivenDate() {
        return givenDate;
    }

    public String getGivenDateDD() {
        return "RepEmployeeSalaryUpgrade_givenDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="cancelDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date cancelDate;

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public String getCancelDateDD() {
        return "RepEmployeeSalaryUpgrade_cancelDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="cancelReason">
    @Column
    @Translatable(translationField = "cancelReasonTranslated")
    private String cancelReason;

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public String getCancelReasonDD() {
        return "RepEmployeeSalaryUpgrade_cancelReason";
    }
    
    @Transient
    @Translation(originalField = "cancelReason")
    private String cancelReasonTranslated;

    public String getCancelReasonTranslated() {
        return cancelReasonTranslated;
    }

    public void setCancelReasonTranslated(String cancelReasonTranslated) {
        this.cancelReasonTranslated = cancelReasonTranslated;
    }
    
    public String getCancelReasonTranslatedDD() {
        return "RepEmployeeSalaryUpgrade_cancelReason";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="upgradeReason">
    @Column
    @Translatable(translationField = "upgradeReasonTranslated")
    private String upgradeReason;

    public void setUpgradeReason(String upgradeReason) {
        this.upgradeReason = upgradeReason;
    }

    public String getUpgradeReason() {
        return upgradeReason;
    }

    public String getUpgradeReasonDD() {
        return "RepEmployeeSalaryUpgrade_upgradeReason";
    }
    
    @Transient
    @Translation(originalField = "upgradeReason")
    private String upgradeReasonTranslated;

    public String getUpgradeReasonTranslated() {
        return upgradeReasonTranslated;
    }

    public void setUpgradeReasonTranslated(String upgradeReasonTranslated) {
        this.upgradeReasonTranslated = upgradeReasonTranslated;
    }
    
    public String getUpgradeReasonTranslatedDD() {
        return "RepEmployeeSalaryUpgrade_upgradeReason";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="givenBy">
    @Column
    @Translatable(translationField = "givenByTranslated")
    private String givenBy;

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }

    public String getGivenBy() {
        return givenBy;
    }

    public String getGivenByDD() {
        return "RepEmployeeSalaryUpgrade_givenBy";
    }
    
    @Transient
    @Translation(originalField = "givenBy")
    private String givenByTranslated;

    public String getGivenByTranslated() {
        return givenByTranslated;
    }

    public void setGivenByTranslated(String givenByTranslated) {
        this.givenByTranslated = givenByTranslated;
    }
    
    public String getGivenByTranslatedDD() {
        return "RepEmployeeSalaryUpgrade_givenBy";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="cancelledBy">
    @Column
    @Translatable(translationField = "cancelledByTranslated")
    private String cancelledBy;

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public String getCancelledByDD() {
        return "RepEmployeeSalaryUpgrade_cancelledBy";
    }
    
    @Transient
    @Translation(originalField = "cancelledBy")
    private String cancelledByTranslated;

    public String getCancelledByTranslated() {
        return cancelledByTranslated;
    }

    public void setCancelledByTranslated(String cancelledByTranslated) {
        this.cancelledByTranslated = cancelledByTranslated;
    }
    
    public String getCancelledByTranslatedDD() {
        return "RepEmployeeSalaryUpgrade_cancelledBy";
    }
    // </editor-fold>
}