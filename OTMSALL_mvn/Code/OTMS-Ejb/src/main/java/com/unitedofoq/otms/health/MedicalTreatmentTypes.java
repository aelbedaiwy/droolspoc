/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mostafa
 */
@Entity
@ParentEntity(fields = {"company"})
public class MedicalTreatmentTypes extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="code">
    private String code; // to be used as desc

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "MedicalTreatmentTypes_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="medicalStatus">
    //@Translatable(translationField = "medicalStatusTranslated")
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC medicalStatus; // to be used as desc

    public UDC getMedicalStatus() {
        return medicalStatus;
    }

    public void setMedicalStatus(UDC medicalStatus) {
        this.medicalStatus = medicalStatus;
    }

    public String getMedicalStatusDD() {
        return "MedicalTreatmentTypes_medicalStatus";
    }
    
    
//    @Transient
//    @Translation(originalField = "medicalStatus")
//    private String medicalStatusTranslated;
//
//    public String getMedicalStatusTranslated() {
//        return medicalStatusTranslated;
//    }
//
//    public void setMedicalStatusTranslated(String medicalStatusTranslated) {
//        this.medicalStatusTranslated = medicalStatusTranslated;
//    }
//
//    public String getMedicalStatusTranslatedDD() {
//        return "MedicalTreatmentTypes_medicalStatus";
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="financialOperations">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC financialOperations;

    public UDC getFinancialOperations() {
        return financialOperations;
    }

    public String getFinancialOperationsDD() {
        return "MedicalTreatmentTypes_financialOperations";
    }

    public void setFinancialOperations(UDC financialOperations) {
        this.financialOperations = financialOperations;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Amount">
    @Column(precision = 18, scale = 3)
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public String getAmountDD() {
        return "MedicalTreatmentTypes_amount";
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    @Transient
    private BigDecimal amountMask;

    public BigDecimal getAmountMask() {
        amountMask = amount;
        return amountMask;
    }

    public String getAmountMaskDD() {
        return "MedicalTreatmentTypes_amountMask";
    }

    public void setAmountMask(BigDecimal amountMask) {
        updateDecimalValue("amount", amountMask);
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Conditions">
    @Column
    @Translatable(translationField = "conditionsTranslated")
    private String conditions;

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getConditionsDD() {
        return "MedicalCategories_conditions";
    }
    @Transient
    @Translation(originalField = "conditions")
    private String conditionsTranslated;

    public String getConditionsTranslated() {
        return conditionsTranslated;
    }

    public void setConditionsTranslated(String conditionsTranslated) {
        this.conditionsTranslated = conditionsTranslated;
    }

    public String getConditionsTranslatedDD() {
        return "MedicalCategories_conditions";
    }
// </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "MedicalTreatmentTypes_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="category">
    @ManyToOne(fetch = FetchType.LAZY)
    private MedicalCategories category;

    public MedicalCategories getCategory() {
        return category;
    }

    public void setCategory(MedicalCategories category) {
        this.category = category;
    }

    public String getCategoryDD() {
        return "MedicalTreatmentTypes_category";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="operation">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC operation;

    public UDC getOperation() {
        return operation;
    }

    public void setOperation(UDC operation) {
        this.operation = operation;
    }

    public String getOperationDD() {
        return "MedicalTreatmentTypes_operation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="empMaxCoverage">
    @Column(precision = 18, scale = 3)
    private BigDecimal empMaxCoverage; // precentage

    public BigDecimal getEmpMaxCoverage() {
        return empMaxCoverage;
    }

    public void setEmpMaxCoverage(BigDecimal empMaxCoverage) {
        this.empMaxCoverage = empMaxCoverage;
    }

    public String getEmpMaxCoverageDD() {
        return "MedicalTreatmentTypes_empMaxCoverage";
    }
    @Transient
    private BigDecimal empMaxCoverageMask;

    public BigDecimal getEmpMaxCoverageMask() {
        empMaxCoverageMask = empMaxCoverage;
        return empMaxCoverageMask;
    }

    public String getEmpMaxCoverageMaskDD() {
        return "MedicalTreatmentTypes_empMaxCoverageMask";
    }

    public void setEmpMaxCoverageMask(BigDecimal empMaxCoverageMask) {
        updateDecimalValue("empMaxCoverage", empMaxCoverageMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="familyMaxCoverage">
    @Column(precision = 18, scale = 3)
    private BigDecimal familyMaxCoverage; // precentage

    public BigDecimal getFamilyMaxCoverage() {
        return familyMaxCoverage;
    }

    public void setFamilyMaxCoverage(BigDecimal familyMaxCoverage) {
        this.familyMaxCoverage = familyMaxCoverage;
    }

    public String getFamilyMaxCoverageDD() {
        return "MedicalTreatmentTypes_familyMaxCoverage";
    }
    @Transient
    private BigDecimal familyMaxCoverageMask;

    public BigDecimal getFamilyMaxCoverageMask() {
        familyMaxCoverageMask = familyMaxCoverage;
        return familyMaxCoverageMask;
    }

    public String getFamilyMaxCoverageMaskDD() {
        return "MedicalClaim_familyMaxCoverageMask";
    }

    public void setFamilyMaxCoverageMask(BigDecimal familyMaxCoverageMask) {
        updateDecimalValue("familyMaxCoverage", familyMaxCoverageMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maxCoverage">
    private BigDecimal maxCoverage;

    public BigDecimal getMaxCoverage() {
        return maxCoverage;
    }

    public void setMaxCoverage(BigDecimal maxCoverage) {
        this.maxCoverage = maxCoverage;
    }

    public String getMaxCoverageDD() {
        return "MedicalTreatmentTypes_maxCoverage";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="empAmount">
    private BigDecimal empAmount;

    public BigDecimal getEmpAmount() {
        return empAmount;
    }

    public void setEmpAmount(BigDecimal empAmount) {
        this.empAmount = empAmount;
    }

    public String getEmpAmountDD() {
        return "MedicalTreatmentTypes_empAmount";
    }
    @Transient
    private BigDecimal empAmountMask;

    public BigDecimal getEmpAmountMask() {
        empAmountMask = empAmount;
        return empAmountMask;
    }

    public String getEmpAmountMaskDD() {
        return "MedicalTreatmentTypes_empAmountMask";
    }

    public void setEmpAmountMask(BigDecimal empAmountMask) {
        updateDecimalValue("empAmount", empAmountMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="familyAmount">
    private BigDecimal familyAmount;

    public BigDecimal getFamilyAmount() {
        return familyAmount;
    }

    public void setFamilyAmount(BigDecimal familyAmount) {
        this.familyAmount = familyAmount;
    }

    public String getFamilyAmountDD() {
        return "MedicalTreatmentTypes_familyAmount";
    }
    @Transient
    private BigDecimal familyAmountMask;

    public BigDecimal getFamilyAmountMask() {
        familyAmountMask = familyAmount;
        return familyAmountMask;
    }

    public String getFamilyAmountMaskDD() {
        return "MedicalTreatmentTypes_familyAmountMask";
    }

    public void setFamilyAmountMask(BigDecimal familyAmountMask) {
        updateDecimalValue("familyAmount", familyAmountMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="amountType">
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC amountType;

    public UDC getAmountType() {
        return amountType;
    }

    public void setAmountType(UDC amountType) {
        this.amountType = amountType;
    }

    public String getAmountTypeDD() {
        return "MedicalTreatmentTypes_amountType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="glassesExpiryYrs">
    @Column(precision = 18, scale = 3)
    private BigDecimal glassesExpiryYrs;

    public BigDecimal getGlassesExpiryYrs() {
        return glassesExpiryYrs;
    }

    public void setGlassesExpiryYrs(BigDecimal glassesExpiryYrs) {
        this.glassesExpiryYrs = glassesExpiryYrs;
    }

    public String getGlassesExpiryYrsDD() {
        return "MedicalTreatmentTypes_glassesExpiryYrs";
    }
    @Transient
    private BigDecimal glassesExpiryYrsMask;

    public BigDecimal getGlassesExpiryYrsMask() {
        glassesExpiryYrsMask = glassesExpiryYrs;
        return glassesExpiryYrsMask;
    }

    public String getGlassesExpiryYrsMaskDD() {
        return "MedicalTreatmentTypes_glassesExpiryYrsMask";
    }

    public void setGlassesExpiryYrsMask(BigDecimal glassesExpiryYrsMask) {
        updateDecimalValue("glassesExpiryYrs", glassesExpiryYrsMask);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="glasses">
    private boolean glasses;

    public boolean isGlasses() {
        return glasses;
    }

    public String getGlassesDD() {
        return "MedicalTreatmentTypes_glasses";
    }

    public void setGlasses(boolean glasses) {
        this.glasses = glasses;
    }
    // </editor-fold >
}
