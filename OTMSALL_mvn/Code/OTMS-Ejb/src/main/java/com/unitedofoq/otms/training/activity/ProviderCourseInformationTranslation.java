/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author lap2
 */
@Entity
@Table(name= "provcouinfotranslation")
public class ProviderCourseInformationTranslation extends BaseEntityTranslation{
    //<editor-fold defaultstate="collapsed" desc="instructorName">
    private String instructorName;

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getInstructorName() {
        return instructorName;
    }
    //</editor-fold>
}