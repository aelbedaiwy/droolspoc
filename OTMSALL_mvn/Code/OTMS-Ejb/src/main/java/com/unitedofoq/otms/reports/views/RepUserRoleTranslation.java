
package com.unitedofoq.otms.reports.views;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class RepUserRoleTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="userFullName">
    @Column
    private String userFullName;

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserFullName() {
        return userFullName;
    }
    // </editor-fold>

}
