/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author mmohamed
 */
@Entity
@DiscriminatorValue("APP")
@ParentEntity(fields={"applicant"})
public class ApplicantUser extends OUser {
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)    
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public String getApplicantDD() {
        return "ApplicantUser_applicant";
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
    


}
