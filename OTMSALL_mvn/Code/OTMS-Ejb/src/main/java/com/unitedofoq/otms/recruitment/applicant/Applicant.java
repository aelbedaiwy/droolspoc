package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.RecruitMatchBase;
import com.unitedofoq.otms.recruitment.vacancy.Vacancy;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = "company")
@ChildEntity(fields = {"educationHistorys", "competencys", "dependences",
    "experiences", "trainings", "criminalOffencess", "applicantUser", "certificates",
    "additionalInfo", "languages", "references", "iqamas", "passports"})
@FABSEntitySpecs(customCascadeFields = {"applicantUser"})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ATYPE")
@DiscriminatorValue("MASTER")
public class Applicant extends RecruitMatchBase {

    // <editor-fold defaultstate="collapsed" desc="criminalOffencess">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantCriminalOffences> criminalOffencess;

    public List<ApplicantCriminalOffences> getCriminalOffencess() {
        return criminalOffencess;
    }

    public void setCriminalOffencess(List<ApplicantCriminalOffences> criminalOffencess) {
        this.criminalOffencess = criminalOffencess;
    }

    public String getCriminalOffencessDD() {
        return "Applicant_criminalOffencess";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dependences">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantDependence> dependences;

    public List<ApplicantDependence> getDependences() {
        return dependences;
    }

    public void setDependences(List<ApplicantDependence> dependences) {
        this.dependences = dependences;
    }

    public String getDependencesDD() {
        return "Applicant_dependences";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competencys">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantCompetency> competencys;

    public List<ApplicantCompetency> getCompetencys() {
        return competencys;
    }

    public void setCompetencys(List<ApplicantCompetency> competencys) {
        this.competencys = competencys;
    }

    public String getCompetencysDD() {
        return "Applicant_competencys";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="type">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC type;

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public String getTypeDD() {
        return "Applicant_type";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applicationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date applicationDate;

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getApplicationDateDD() {
        return "Applicant_applicationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="graduationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date graduationDate;

    public Date getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(Date graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getGraduationDateDD() {
        return "Applicant_graduationDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "Applicant_code";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applicantStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC applicantStatus;

    public UDC getApplicantStatus() {
        return applicantStatus;
    }

    public void setApplicantStatus(UDC applicantStatus) {
        this.applicantStatus = applicantStatus;
    }

    public String getApplicantStatusDD() {
        return "Applicant_applicantStatus";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attachDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date attachDate;

    public Date getAttachDate() {
        return attachDate;
    }

    public void setAttachDate(Date attachDate) {
        this.attachDate = attachDate;
    }

    public String getAttachDateDD() {
        return "Applicant_attachDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attachBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee attachBy;//(name = "cancelled_by")

    public Employee getAttachBy() {
        return attachBy;
    }

    public void setAttachBy(Employee attachBy) {
        this.attachBy = attachBy;
    }

    public String getAttachByDD() {
        return "Applicant_attachBy";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="rejectionDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date rejectionDate;

    public Date getRejectionDate() {
        return rejectionDate;
    }

    public void setRejectionDate(Date rejectionDate) {
        this.rejectionDate = rejectionDate;
    }

    public String getRejectionDateDD() {
        return "Applicant_rejectionDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="rejectedBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee rejectedBy;//(name = "cancelled_by")

    public Employee getRejectedBy() {
        return rejectedBy;
    }

    public void setRejectedBy(Employee rejectedBy) {
        this.rejectedBy = rejectedBy;
    }

    public String getRejectedByDD() {
        return "Applicant_rejectedBy";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="rejectionReason">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC rejectionReason;

    public UDC getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(UDC rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getRejectionReasonDD() {
        return "Applicant_rejectionReason";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="companyRejection">
    @Column
    private boolean companyRejection;

    public boolean isCompanyRejection() {
        return companyRejection;
    }

    public void setCompanyRejection(boolean companyRejection) {
        this.companyRejection = companyRejection;
    }

    public String getCompanyRejectionDD() {
        return "Requisition_companyRejection";
    }
    // </editor-fold>//True-Company,False-Vacacy
    // <editor-fold defaultstate="collapsed" desc="hiringDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date hiringDate;

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getHiringDateDD() {
        return "Applicant_hiringDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiredBy">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee hiredBy;

    public Employee getHiredBy() {
        return hiredBy;
    }

    public void setHiredBy(Employee hiredBy) {
        this.hiredBy = hiredBy;
    }

    public String getHiredByDD() {
        return "Applicant_hiredBy";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "Applicant_startDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startSalary">
    @Column
    private BigDecimal startSalary;

    public BigDecimal getStartSalary() {
        return startSalary;
    }

    public void setStartSalary(BigDecimal startSalary) {
        this.startSalary = startSalary;
    }

    public String getStartSalaryDD() {
        return "Applicant_startSalary";
    }
    @Transient
    private BigDecimal startSalaryMask;

    public BigDecimal getStartSalaryMask() {
        startSalaryMask = startSalary;
        return startSalaryMask;
    }

    public void setStartSalaryMask(BigDecimal startSalaryMask) {
        updateDecimalValue("startSalary", startSalaryMask);
    }

    public String getStartSalaryMaskDD() {
        return "Applicant_startSalaryMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="notes">
    @Column(nullable = true)
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotesDD() {
        return "Applicant_notes";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Applicant_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="previousExperienceYears">
    @Column
    private Short previousExperienceYears;

    public Short getPreviousExperienceYears() {
        return previousExperienceYears;
    }

    public void setPreviousExperienceYears(Short previousExperienceYears) {
        this.previousExperienceYears = previousExperienceYears;
    }

    public String getPreviousExperienceYearsDD() {
        return "Applicant_previousExperienceYears";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="previousExperienceMonths">
    @Column
    private Short previousExperienceMonths;

    public Short getPreviousExperienceMonths() {
        return previousExperienceMonths;
    }

    public void setPreviousExperienceMonths(Short previousExperienceMonths) {
        this.previousExperienceMonths = previousExperienceMonths;
    }

    public String getPreviousExperienceMonthsDD() {
        return "Applicant_previousExperienceMonths";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="agency">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC agency;

    public UDC getAgency() {
        return agency;
    }

    public void setAgency(UDC agency) {
        this.agency = agency;
    }

    public String getAgencyDD() {
        return "Applicant_agency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="faxNo">
    @Column
    private String faxNo;

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getFaxNoDD() {
        return "Applicant_faxNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="password">
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordDD() {
        return "Applicant_password";
    }
    @Transient
    private String passwordConfirmation;

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getPasswordConfirmationDD() {
        return "Applicant_passwordConfirmation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="passportNo">
    @Column
    private String passportNo;

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportNoDD() {
        return "Applicant_passportNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nominatedRecommended">
    @Column
    private String nominatedRecommended;

    public String getNominatedRecommended() {
        return nominatedRecommended;
    }

    public void setNominatedRecommended(String nominatedRecommended) {
        this.nominatedRecommended = nominatedRecommended;
    }

    public String getNominatedRecommendedDD() {
        return "Applicant_nominatedRecommended";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nominatedByWhom">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee nominatedByWhom;

    public Employee getNominatedByWhom() {
        return nominatedByWhom;
    }

    public void setNominatedByWhom(Employee nominatedByWhom) {
        this.nominatedByWhom = nominatedByWhom;
    }

    public String getNominatedByWhomDD() {
        return "Applicant_nominatedByWhom";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nominatedDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date nominatedDate;

    public Date getNominatedDate() {
        return nominatedDate;
    }

    public void setNominatedDate(Date nominatedDate) {
        this.nominatedDate = nominatedDate;
    }

    public String getNominatedDateDD() {
        return "Applicant_nominatedDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="source">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC source;

    public String getSourceDD() {
        return "Applicant_source";
    }

    public UDC getSource() {
        return source;
    }

    public void setSource(UDC source) {
        this.source = source;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hired">
    @Column
    private boolean hired;

    public boolean isHired() {
        return hired;
    }

    public void setHired(boolean hired) {
        this.hired = hired;
    }

    public String getHiredDD() {
        return "Applicant_hireFlag";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="birthNationality">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC birthNationality;

    public String getBirthNationalityDD() {
        return "Applicant_birthNationality";
    }

    public UDC getBirthNationality() {
        return birthNationality;
    }

    public void setBirthNationality(UDC birthNationality) {
        this.birthNationality = birthNationality;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="leisureTime">
    @Column
    private String leisureTime;

    public String getLeisureTime() {
        return leisureTime;
    }

    public void setLeisureTime(String leisureTime) {
        this.leisureTime = leisureTime;
    }

    public String getLeisureTimeDD() {
        return "Applicant_leisureTime";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "Applicant_comments";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applicantUser">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "applicant")
    @JoinColumn
    private ApplicantUser applicantUser;

    public ApplicantUser getApplicantUser() {
        return applicantUser;
    }

    public String getApplicantUserDD() {
        return "Applicant_applicantUser";
    }

    public void setApplicantUser(ApplicantUser applicantUser) {
        this.applicantUser = applicantUser;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="residence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC residence;

    public UDC getResidence() {
        return residence;
    }

    public String getResidenceDD() {
        return "Applicant_residence";
    }

    public void setResidence(UDC residence) {
        this.residence = residence;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="educationCategory">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC educationCategory;

    public UDC getEducationCategory() {
        return educationCategory;
    }

    public String getEducationCategoryDD() {
        return "Applicant_educationCategory";
    }

    public void setEducationCategory(UDC educationCategory) {
        this.educationCategory = educationCategory;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nationalID">
    @Column(unique = true)
    private String nationalID;

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    public String getNationalIDDD() {
        return "Applicant_nationalID";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="additionalInfo">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "applicant")
    private ApplicantAdditionalInfo additionalInfo;

    public ApplicantAdditionalInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public String getAdditionalInfoDD() {
        return "Applicant_additionalInfo";
    }

    public void setAdditionalInfo(ApplicantAdditionalInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postalCode">
    private String postalCode;

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCodeDD() {
        return "Applicant_postalCode";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="street">
    private String street;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetDD() {
        return "Applicant_street";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="userName">
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNameDD() {
        return "Applicant_userName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cv">
    @Column(columnDefinition = "BLOB(8388608)")
    private byte[] cv;

    public byte[] getCv() {
        return cv;
    }

    public void setCv(byte[] cv) {
        this.cv = cv;
    }

    public String getCvDD() {
        return "Applicant_cv";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Applicant Personal Info Fields">
    @Translatable(translationField = "addressTranslated")
    private String address;
    @Transient
    @Translation(originalField = "address")
    private String addressTranslated;
    @Translatable(translationField = "address1Translated")
    private String address1;
    @Transient
    @Translation(originalField = "address1")
    private String address1Translated;
    private String phone;
    private String phone1;
    private String mobile;
    private String mobile1;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String email1;
    @Temporal(value = TemporalType.DATE)
    private Date birthdate;
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC birthPlace;
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC nationality;
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC maritalStatus;
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC militaryStatus;
    @ManyToOne(fetch = FetchType.LAZY)
    private UDC salute;
    @Translatable(translationField = "additionalInformationTranslated")
    private String additionalInformation;

    // <editor-fold defaultstate="collapsed" desc="religion">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC religion;

    public String getReligionDD() {
        return "Applicant_religion";
    }

    public UDC getReligion() {
        return religion;
    }

    public void setReligion(UDC religion) {
        this.religion = religion;
    }
    // </editor-fold>

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public String getAdditionalInformationDD() {
        return "Applicant_additionalInformation";
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public UDC getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(UDC maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatusDD() {
        return "Applicant_maritalStatus";
    }

    public UDC getMilitaryStatus() {
        return militaryStatus;
    }

    public String getMilitaryStatusDD() {
        return "Applicant_militaryStatus";
    }

    public void setMilitaryStatus(UDC militaryStatus) {
        this.militaryStatus = militaryStatus;
    }

    public UDC getSalute() {
        return salute;
    }

    public String getSaluteDD() {
        return "Applicant_salute";
    }

    public void setSalute(UDC salute) {
        this.salute = salute;
    }

    public String getAddress() {
        return address;
    }

    public String getAddressDD() {
        return "Applicant_address";
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthdateDD() {
        return "Applicant_birthdate";
    }

    public UDC getBirthPlace() {
        return birthPlace;
    }

    public String getBirthPlaceDD() {
        return "Applicant_birthPlace";
    }

    public void setBirthPlace(UDC birthPlace) {
        this.birthPlace = birthPlace;
    }

    public UDC getNationality() {
        return nationality;
    }

    public String getNationalityDD() {
        return "Applicant_nationality";
    }

    public void setNationality(UDC nationality) {
        this.nationality = nationality;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressTranslated() {
        return addressTranslated;
    }

    public void setAddressTranslated(String addressTranslated) {
        this.addressTranslated = addressTranslated;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1DD() {
        return "Applicant_address1";
    }

    public String getAddress1Translated() {
        return address1Translated;
    }

    public void setAddress1Translated(String address1Translated) {
        this.address1Translated = address1Translated;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhoneDD() {
        return "Applicant_phone";
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone1() {
        return phone1;
    }

    public String getPhone1DD() {
        return "Applicant_phone1";
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileDD() {
        return "Applicant_mobile";
    }

    public String getMobile1() {
        return mobile1;
    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
    }

    public String getMobile1DD() {
        return "Applicant_mobile1";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailDD() {
        return "Applicant_email";
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail1DD() {
        return "Applicant_email1";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public String getNameDD() {
        return "Applicant_name";
    }

    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "Applicant_nameTranslated";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="languages">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantLanguage> languages;

    public List<ApplicantLanguage> getLanguages() {
        return languages;
    }

    public void setLanguages(List<ApplicantLanguage> languages) {
        this.languages = languages;
    }

    public String getLanguagesDD() {
        return "Applicant_languages";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="references">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantReference> references;

    public List<ApplicantReference> getReferences() {
        return references;
    }

    public void setReferences(List<ApplicantReference> references) {
        this.references = references;
    }

    public String getReferencesDD() {
        return "Applicant_references";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="skills">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantSkill> skills;

    public List<ApplicantSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<ApplicantSkill> skills) {
        this.skills = skills;
    }

    public String getSkillsDD() {
        return "Applicant_skills";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="certificates">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantCertificate> certificates;

    public List<ApplicantCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<ApplicantCertificate> certificates) {
        this.certificates = certificates;
    }

    public String getCertificatesDD() {
        return "Applicant_certificates";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="experiences">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantExperience> experiences;

    public List<ApplicantExperience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<ApplicantExperience> experiences) {
        this.experiences = experiences;
    }

    public String getExperiencesDD() {
        return "Applicant_experiences";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainings">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantTraining> trainings;

    public List<ApplicantTraining> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<ApplicantTraining> trainings) {
        this.trainings = trainings;
    }

    public String getAppicantTrainingsDD() {
        return "Applicant_trainings";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="educationHistorys">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantEducationHistory> educationHistorys;

    public List<ApplicantEducationHistory> getEducationHistorys() {
        return educationHistorys;
    }

    public void setEducationHistorys(List<ApplicantEducationHistory> educationHistorys) {
        this.educationHistorys = educationHistorys;
    }

    public String getEducationHistorysDD() {
        return "Applicant_educationHistorys";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applicantReferals">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantReferal> applicantReferals;

    public List<ApplicantReferal> getApplicantReferals() {
        return applicantReferals;
    }

    public void setApplicantReferals(List<ApplicantReferal> applicantReferals) {
        this.applicantReferals = applicantReferals;
    }

    public String getApplicantReferalsDD() {
        return "Applicant_applicantReferals";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="five vacancies">
    @ManyToOne(fetch = FetchType.LAZY)
    private Vacancy vacancy1;

    public Vacancy getVacancy1() {
        return vacancy1;
    }

    public void setVacancy1(Vacancy vacancy1) {
        this.vacancy1 = vacancy1;
    }

    public String getVacancy1DD() {
        return "Applicant_vacancy1";
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Vacancy vacancy2;

    public Vacancy getVacancy2() {
        return vacancy2;
    }

    public void setVacancy2(Vacancy vacancy2) {
        this.vacancy2 = vacancy2;
    }

    public String getVacancy2DD() {
        return "Applicant_vacancy2";
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Vacancy vacancy3;

    public Vacancy getVacancy3() {
        return vacancy3;
    }

    public void setVacancy3(Vacancy vacancy3) {
        this.vacancy3 = vacancy3;
    }

    public String getVacancy3DD() {
        return "Applicant_vacancy3";
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Vacancy vacancy4;

    public Vacancy getVacancy4() {
        return vacancy4;
    }

    public void setVacancy4(Vacancy vacancy4) {
        this.vacancy4 = vacancy4;
    }

    public String getVacancy4DD() {
        return "Applicant_vacancy4";
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Vacancy vacancy5;

    public Vacancy getVacancy5() {
        return vacancy5;
    }

    public void setVacancy5(Vacancy vacancy5) {
        this.vacancy5 = vacancy5;
    }

    public String getVacancy5DD() {
        return "Applicant_vacancy5";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="iqamas">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantIqama> iqamas;

    public List<ApplicantIqama> getIqamas() {
        return iqamas;
    }

    public void setIqamas(List<ApplicantIqama> iqamas) {
        this.iqamas = iqamas;
    }

    public String getIqamasDD() {
        return "Applicant_iqamas";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="passports">
    @OneToMany(mappedBy = "applicant")
    private List<ApplicantPassport> passports;

    public List<ApplicantPassport> getPassports() {
        return passports;
    }

    public void setPassports(List<ApplicantPassport> passports) {
        this.passports = passports;
    }

    public String getPassportsDD() {
        return "Applicant_passports";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="previousCompanyFirst">
    private String otherCertificate;

    public String getOtherCertificate() {
        return otherCertificate;
    }

    public void setOtherCertificate(String otherCertificate) {
        this.otherCertificate = otherCertificate;
    }

    public String getOtherCertificateDD() {
        return "Applicant_otherCertificate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="previousCompanyFirst">
    private String previousCompanyFirst;

    public String getPreviousCompanyFirst() {
        return previousCompanyFirst;
    }

    public void setPreviousCompanyFirst(String previousCompanyFirst) {
        this.previousCompanyFirst = previousCompanyFirst;
    }

    public String getPreviousCompanyFirstDD() {
        return "Applicant_previousCompanyFirst";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="arabicName">
    private String arabicName;

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getArabicNameDD() {
        return "Applicant_arabicName";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="englishName">
    private String englishName;

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getEnglishNameDD() {
        return "Applicant_englishName";
    }
    //</editor-fold>
}
