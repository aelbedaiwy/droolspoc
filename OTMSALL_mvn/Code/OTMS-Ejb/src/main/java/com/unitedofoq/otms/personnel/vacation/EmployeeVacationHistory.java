/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author MIR
 */
@Entity
@ParentEntity(fields={"employee"})
public class EmployeeVacationHistory extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeVacationHistory_employee";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacation">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }
    public String getVacationDD() {
        return "EmployeeVacationHistory_vacation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;//(name = "vacation_start_date")

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public String getStartDateDD() {
        return "EmployeeVacationHistory_startDate";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;//(name = "vacation_end_date")

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public String getEndDateDD() {
        return "EmployeeVacationHistory_endDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="period">
     @Column(precision=18, scale=3)
     private BigDecimal period;//(name = "vacation_period")

     public void setPeriod(BigDecimal period) {
            this.period = period;
     }
     public BigDecimal getPeriod() {
        return period;
     }

    public String getPeriodDD() {
        return "EmployeeVacationHistory_period";
    }
    @Transient
    private BigDecimal periodMask;

    public void setPeriodMask(BigDecimal periodMask) {
        updateDecimalValue("period",periodMask);
    }
    public BigDecimal getPeriodMask() {
        periodMask = period;
        return periodMask;
    }

    public String getPeriodMaskDD() {
        return "EmployeeVacationHistory_periodMask";
    }
    // </editor-fold>
}