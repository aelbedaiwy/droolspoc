
package com.unitedofoq.otms.foundation.company;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class CompanyTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="costCenterDescription">
    @Column
    private String costCenterDescription;

    public void setCostCenterDescription(String costCenterDescription) {
        this.costCenterDescription = costCenterDescription;
    }

    public String getCostCenterDescription() {
        return costCenterDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="positionDescription">
    @Column
    private String positionDescription;

    public void setPositionDescription(String positionDescription) {
        this.positionDescription = positionDescription;
    }

    public String getPositionDescription() {
        return positionDescription;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ownerName">
    @Column
    private String ownerName;

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerName() {
        return ownerName;
    }
    // </editor-fold>

}
