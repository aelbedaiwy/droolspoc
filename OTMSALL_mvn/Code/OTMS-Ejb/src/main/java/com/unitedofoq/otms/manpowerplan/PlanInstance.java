
package com.unitedofoq.otms.manpowerplan;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.*;

@Entity
public class PlanInstance extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "PlanInstance_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public void setCompany(Company company) {
        this.company = company;
    }

    public Company getCompany() {
        return company;
    }

    public String getCompanyDD() {
        return "PlanInstance_company";
    }
    // </editor-fold>

}
