package com.unitedofoq.otms.utilities.audit;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.util.Date;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Aboelnour
 */
@MappedSuperclass
public class BusinessAudit extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="loggedUser">
    String loggedUser;

    public String getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(String loggedUser) {
        this.loggedUser = loggedUser;
    }

    public String getLoggedUserDD() {
        return "BusinessAudit_loggedUser";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="methodName">
    String methodName;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodNameDD() {
        return "BusinessAudit_methodName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="message">
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageDD() {
        return "BusinessAudit_message";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="logDate">
    @Temporal(TemporalType.TIMESTAMP)
    Date logDate;

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public String getLogDateDD() {
        return "BusinessAudit_logDate";
    }
    // </editor-fold>
}
