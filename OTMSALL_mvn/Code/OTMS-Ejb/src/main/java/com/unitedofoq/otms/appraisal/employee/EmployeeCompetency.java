package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.otms.appraisal.employee.EmployeeKPA;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import com.unitedofoq.otms.appraisal.succession.SuccessionPlan;
import com.unitedofoq.otms.foundation.competency.Competency;
import com.unitedofoq.otms.foundation.competency.CompetencyLevel;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.JobCompetency;
import com.unitedofoq.otms.foundation.position.PositionCompetency;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeCompetency extends EmployeeKPA {
    // <editor-fold defaultstate="collapsed" desc="competencyTrainingCourses">
//    @OneToMany
//    private List<CompetencyTrainingCourse> competencyTrainingCourses;
//
//    public List<CompetencyTrainingCourse> getCompetencyTrainingCourses() {
//        return competencyTrainingCourses;
//    }
//
//    public void setCompetencyTrainingCourses(List<CompetencyTrainingCourse> competencyTrainingCourses) {
//        this.competencyTrainingCourses = competencyTrainingCourses;
//    }
//
//    public String getCompetencyTrainingCoursesDD() {
//        return "Competency_competencyTrainingCourses";
//    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable = false)
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public String getNameTranslatedDD() {
        return "EmployeeCompetency_nameTranslated";
    }

    public void setName(String name) {
        this.name = name;
    }
    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable = false)
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public String getDescription() {
        return description;
    }

    public String getDescriptionTranslatedDD() {
        return "EmployeeCompetency_description";
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="levels">
//
//    @OneToMany(mappedBy="competency")
//    private List<CompetencyLevel> levels;
//     public List<CompetencyLevel> getLevels() {
//        return levels;
//    }
//    public String getLevelsDD() {
//        return "EmployeeCompetency_levels";
//    }
//    public void setLevels(List<CompetencyLevel> levels) {
//        this.levels = levels;
//    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn(nullable = false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeCompetency_employee";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }

    public void setCompetency(Competency competency) {
        this.competency = competency;
    }

    public String getCompetencyDD() {
        return "EmployeeCompetency_competency";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jobCompetency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private JobCompetency jobCompetency;

    public JobCompetency getJobCompetency() {
        return jobCompetency;
    }

    public void setJobCompetency(JobCompetency jobCompetency) {
        this.jobCompetency = jobCompetency;
    }

    public String getJobCompetencyDD() {
        return "EmployeeCompetency_jobCompetency";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="taregetAsRating">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule taregetAsRating;

    public ScaleTypeRule getTaregetAsRating() {
        return taregetAsRating;
    }

    public String getTaregetAsRatingDD() {
        return "EmployeeCompetency_taregetAsRating";
    }

    public void setTaregetAsRating(ScaleTypeRule taregetAsRating) {
        this.taregetAsRating = taregetAsRating;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="positionCompetency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PositionCompetency positionCompetency;

    public PositionCompetency getPositionCompetency() {
        return positionCompetency;
    }

    public void setPositionCompetency(PositionCompetency positionCompetency) {
        this.positionCompetency = positionCompetency;
    }

    public String getPositionCompetencyDD() {
        return "EmployeeCompetency_positionCompetency";
    }
    //</editor-fold>
    // for recruitment:
    // =================
    private boolean gap;

    public String getGapDD() {
        return "EmployeeCompetency_gap";
    }

    public boolean getGap() {
        return gap;
    }

    public void setGap(boolean gap) {
        this.gap = gap;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private CompetencyLevel competencyLevel;

    public CompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }

    public String getCompetencyLevelDD() {
        return "EmployeeCompetency_competencyLevel";
    }

    public void setCompetencyLevel(CompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }

    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "EmployeeCompetency_applicant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="successionPlan">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SuccessionPlan successionPlan;

    public SuccessionPlan getSuccessionPlan() {
        return successionPlan;
    }

    public SuccessionPlan getSuccessionPlanDD() {
        return successionPlan;
    }

    public void setSuccessionPlan(SuccessionPlan successionPlan) {
        this.successionPlan = successionPlan;
    }
    //</editor-fold >
}
