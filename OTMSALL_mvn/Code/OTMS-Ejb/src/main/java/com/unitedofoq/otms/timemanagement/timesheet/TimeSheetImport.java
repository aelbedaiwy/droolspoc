package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class TimeSheetImport extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="index">
     private String recIndex;

     public String getRecIndex() {
     return recIndex;
     }

     public void setRecIndex(String recIndex) {
     this.recIndex = recIndex;
     }

     public String getRecIndexDD() {
     return "TimeSheetImport_recIndex";
     }
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="employee">
     @ManyToOne(fetch= FetchType.LAZY)
     private Employee employee;

     public Employee getEmployee() {
     return employee;
     }

     public void setEmployee(Employee employee) {
     this.employee = employee;
     }

     public String getEmployeeDD() {
     return "TimeSheetImport_employee";
     }
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="dailyDate">
     @Temporal(TemporalType.DATE)
     private Date dailyDate;

     public Date getDailyDate() {
     return dailyDate;
     }

     public void setDailyDate(Date dailyDate) {
     this.dailyDate = dailyDate;
     }
    
     public String getDailyDateDD() {
     return "TimeSheetImport_dailyDate";
     }
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="dayNature">
     private String dayNature;

     public String getDayNature() {
     return dayNature;
     }

     public void setDayNature(String dayNature) {
     this.dayNature = dayNature;
     }
    
     public String getDayNatureDD() {
     return "TimeSheetImport_dayNature";
     }
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="numHrs">
     private String numHrs;

     public String getNumHrs() {
     return numHrs;
     }

     public void setNumHrs(String numHrs) {
     this.numHrs = numHrs;
     }
    
     public String getNumHrsDD() {
     return "TimeSheetImport_numHrs";
     }
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="posted">
     private boolean posted;

     public boolean isPosted() {
     return posted;
     }

     public void setPosted(boolean posted) {
     this.posted = posted;
     }
    
     public String getPostedDD() {
     return "TimeSheetImport_posted";
     }
     // </editor-fold>
}
