package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.otms.recruitment.onlineexam.EssayQuestion;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class ApplicantEssayQuestAnswer extends ApplicantAnswer {

    // <editor-fold defaultstate="collapsed" desc="question">

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private EssayQuestion question;

    public EssayQuestion getQuestion() {
        return question;
    }

    public void setQuestion(EssayQuestion question) {
        this.question = question;
    }

    public String getQuestionDD() {
        return "ApplicantAnswer_question";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="answer">
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerDD() {
        return "ApplicantAnswer_answer";
    }
    // </editor-fold>
}
