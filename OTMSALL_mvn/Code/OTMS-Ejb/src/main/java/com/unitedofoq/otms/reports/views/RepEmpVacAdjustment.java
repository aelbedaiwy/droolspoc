package com.unitedofoq.otms.reports.views;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class RepEmpVacAdjustment extends RepEmployeeBase{
    // <editor-fold defaultstate="collapsed" desc="creationDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDateDD() {
        return "RepEmpVacAdjustment_creationDate";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="AdjustmentValue">
    private BigDecimal adjustmentValue;

    public BigDecimal getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(BigDecimal adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    public String getAdjustmentValueDD() {
        return "RepEmpVacAdjustment_adjustmentValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="comments">
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCommentsDD() {
        return "RepEmpVacAdjustment_comments";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="vacation">
    private String vacation;

    public String getVacation() {
        return vacation;
    }

    public void setVacation(String vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "RepEmpVacAdjustment_vacation";
    }
    // </editor-fold>
}