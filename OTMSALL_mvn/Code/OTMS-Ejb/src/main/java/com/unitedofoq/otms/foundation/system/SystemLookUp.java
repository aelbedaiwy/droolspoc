package com.unitedofoq.otms.foundation.system;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import java.util.List;
import javax.persistence.*;

@Entity
@ChildEntity(fields = {"details"})
public class SystemLookUp extends SystemLookUpBase {

    // <editor-fold defaultstate="collapsed" desc="details">
    @OneToMany(mappedBy = "systemLookUp")
    private List<SystemLookUpDetail> details;

    public void setDetails(List<SystemLookUpDetail> details) {
        this.details = details;
    }

    public List<SystemLookUpDetail> getDetails() {
        return details;
    }

    public String getDetailsDD() {
        return "SystemLookUp_details";
    }
    // </editor-fold>
}
