/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author mmohamed
 */
@Entity
@ParentEntity(fields={"company"})
public class VacationBlocking extends BaseEntity {
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fromDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date toDate;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }
    
    public String getCompanyDD() {
        return "VacationBlocking_company";
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Date getFromDate() {
        return fromDate;
    }
    
    public String getFromDateDD() {
        return "VacationBlocking_fromDate";
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }
    
    public String getToDateDD() {
        return "VacationBlocking_toDate";
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
