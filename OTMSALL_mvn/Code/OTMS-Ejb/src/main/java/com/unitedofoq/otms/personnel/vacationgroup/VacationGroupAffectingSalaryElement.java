/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.personnel.dayoff.DayOffAffectingSalaryElement;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
public class VacationGroupAffectingSalaryElement extends DayOffAffectingSalaryElement {
    // <editor-fold defaultstate="collapsed" desc="vacationGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn (nullable= false)
    private VacationGroup vacationGroup;

    public VacationGroup getVacationGroup() {
        return vacationGroup;
    }

    public void setVacationGroup(VacationGroup vacationGroup) {
        this.vacationGroup = vacationGroup;
    }

    public String getVacationGroupDD() {
        return "VacationGroupAffectingSalaryElement_vacationGroup";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @Transient
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
     public String getSalaryElementDD() {
        return "VacationGroupAffectingSalaryElement_salaryElement";
    }
 // </editor-fold>
}
