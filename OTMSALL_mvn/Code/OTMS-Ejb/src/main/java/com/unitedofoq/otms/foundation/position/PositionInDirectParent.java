/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.position;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"position"})
@DiscriminatorValue("INDIRECT")
public class PositionInDirectParent extends PositionParentBase{

    // <editor-fold defaultstate="collapsed" desc="parentPositionType">
 @JoinColumn(name="EPTYPE",nullable=false)
@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
 private UDC parentPositionType;
 public UDC getParentPositionType() {
        return parentPositionType;
    }
public String getParentPositionTypeDD() {
        return "PositionInDirectParent_parentPositionType";
    }
 public void setParentPositionType(UDC parentPositionType) {
        this.parentPositionType = parentPositionType;
 }
 
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="position">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Position position ;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
    public String getPositionDD() {
        return "PositionInDirectParent_position";
    }


    // </editor-fold>
}
