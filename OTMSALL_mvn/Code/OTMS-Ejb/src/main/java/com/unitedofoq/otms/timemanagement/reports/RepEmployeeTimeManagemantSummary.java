
package com.unitedofoq.otms.timemanagement.reports;

import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import javax.persistence.*;
import java.math.BigDecimal;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
@Table(name= "repemptmsummary")
public class RepEmployeeTimeManagemantSummary extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmployeeTimeManagemantSummary_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="delay">
    @Column
    private BigDecimal delay;

    public void setDelay(BigDecimal delay) {
        this.delay = delay;
    }

    public BigDecimal getDelay() {
        return delay;
    }

    public String getDelayDD() {
        return "RepEmployeeTimeManagemantSummary_delay";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="absenceDays">
    private BigDecimal absenceDays;

    public BigDecimal getAbsenceDays() {
        return absenceDays;
    }

    public void setAbsenceDays(BigDecimal absenceDays) {
        this.absenceDays = absenceDays;
    }
    
    public String getAbsenceDaysDD() {
        return "RepEmployeeTimeManagemantSummary_absenceDays";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="earlyLeave">
    @Column
    private BigDecimal earlyLeave;

    public void setEarlyLeave(BigDecimal earlyLeave) {
        this.earlyLeave = earlyLeave;
    }

    public BigDecimal getEarlyLeave() {
        return earlyLeave;
    }

    public String getEarlyLeaveDD() {
        return "RepEmployeeTimeManagemantSummary_earlyLeave";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOvertime">
    @Column
    private BigDecimal dayOvertime;

    public void setDayOvertime(BigDecimal dayOvertime) {
        this.dayOvertime = dayOvertime;
    }

    public BigDecimal getDayOvertime() {
        return dayOvertime;
    }

    public String getDayOvertimeDD() {
        return "RepEmployeeTimeManagemantSummary_dayOvertime";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nightOvertime">
    @Column
    private BigDecimal nightOvertime;

    public void setNightOvertime(BigDecimal nightOvertime) {
        this.nightOvertime = nightOvertime;
    }

    public BigDecimal getNightOvertime() {
        return nightOvertime;
    }

    public String getNightOvertimeDD() {
        return "RepEmployeeTimeManagemantSummary_nightOvertime";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="holiday">
    @Column
    private BigDecimal holiday;

    public void setHoliday(BigDecimal holiday) {
        this.holiday = holiday;
    }

    public BigDecimal getHoliday() {
        return holiday;
    }

    public String getHolidayDD() {
        return "RepEmployeeTimeManagemantSummary_holiday";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="weekend">
    @Column
    private BigDecimal weekend;

    public void setWeekend(BigDecimal weekend) {
        this.weekend = weekend;
    }

    public BigDecimal getWeekend() {
        return weekend;
    }

    public String getWeekendDD() {
        return "RepEmployeeTimeManagemantSummary_weekend";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="workingDays">
    @Column
    private BigDecimal workingDays;

    public void setWorkingDays(BigDecimal workingDays) {
        this.workingDays = workingDays;
    }

    public BigDecimal getWorkingDays() {
        return workingDays;
    }

    public String getWorkingDaysDD() {
        return "RepEmployeeTimeManagemantSummary_workingDays";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="missions">
    @Column
    private BigDecimal missions;

    public void setMissions(BigDecimal missions) {
        this.missions = missions;
    }

    public BigDecimal getMissions() {
        return missions;
    }

    public String getMissionsDD() {
        return "RepEmployeeTimeManagemantSummary_missions";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="vacations">
    @Column
    private BigDecimal vacations;

    public void setVacations(BigDecimal vacations) {
        this.vacations = vacations;
    }

    public BigDecimal getVacations() {
        return vacations;
    }

    public String getVacationsDD() {
        return "RepEmployeeTimeManagemantSummary_vacations";
    }
    // </editor-fold>    
}