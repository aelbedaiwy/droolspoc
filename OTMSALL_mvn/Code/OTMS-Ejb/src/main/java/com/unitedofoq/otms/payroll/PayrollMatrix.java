package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PayrollMatrix extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="position">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPositionDD() {
        return "PayrollMatrix_position";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payGrade">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private PayGrade payGrade;

    public PayGrade getPayGrade() {
        return payGrade;
    }

    public void setPayGrade(PayGrade payGrade) {
        this.payGrade = payGrade;
    }

    public String getPayGradeDD() {
        return "PayrollMatrix_payGrade";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="unit">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitDD() {
        return "PayrollMatrix_unit";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="amount">
    @Column(precision = 25, scale = 13)
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAmountDD() {
        return "PayrollMatrix_amount";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="job">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job job;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getJobDD() {
        return "PayrollMatrix_job";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "PayrollMatrix_location";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="amountEnc">
    private String amountEnc;

    public String getAmountEnc() {
        return amountEnc;
    }

    public void setAmountEnc(String amountEnc) {
        this.amountEnc = amountEnc;
    }

    public String getAmountEncDD() {
        return "PayrollMatrix_amountEnc";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "PayrollMatrix_salaryElement";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="hiringType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC hiringType;

    public UDC getHiringType() {
        return hiringType;
    }

    public void setHiringType(UDC hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringTypeDD() {
        return "PayrollMatrix_hiringType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="includeSubPositions">
    private boolean includeSubPositions;

    public boolean isIncludeSubPositions() {
        return includeSubPositions;
    }

    public void setIncludeSubPositions(boolean includeSubPositions) {
        this.includeSubPositions = includeSubPositions;
    }

    public String getIncludeSubPositionsDD() {
        return "PayrollMatrix_includeSubPositions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="includeSubUnits">
    private boolean includeSubUnits;

    public boolean isIncludeSubUnits() {
        return includeSubUnits;
    }

    public void setIncludeSubUnits(boolean includeSubUnits) {
        this.includeSubUnits = includeSubUnits;
    }

    public String getIncludeSubUnitsDD() {
        return "PayrollMatrix_includeSubUnits";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sequences">
    private Integer sequences;

    public Integer getSequences() {
        return sequences;
    }

    public void setSequences(Integer sequences) {
        this.sequences = sequences;
    }

    public String getSequencesDD() {
        return "PayrollMatrix_sequences";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="applyDate">
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date applyDate;

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public String getApplyDateDD() {
        return "PayrollMatrix_applyDate";
    }
    // </editor-fold>
}
