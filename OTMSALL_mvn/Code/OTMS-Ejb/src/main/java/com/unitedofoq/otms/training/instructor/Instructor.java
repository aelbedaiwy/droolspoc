package com.unitedofoq.otms.training.instructor;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.training.activity.Provider;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields = {"personalInfo"})
@ChildEntity(fields = {"exceptions", "skills"})
public class Instructor extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "Instructor_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="internal">
    @Column
    private boolean internal;

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public String getInternalDD() {
        return "Instructor_internal";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "Instructor_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cost">
    @Column(precision = 25, scale = 13)
    private BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCostDD() {
        return "Instructor_cost";
    }
    @Transient
    private BigDecimal costMask;

    public BigDecimal getCostMask() {
        costMask = cost;
        return costMask;
    }

    public void setCostMask(BigDecimal costMask) {
        updateDecimalValue("cost", costMask);
    }

    public String getCostMaskDD() {
        return "Instructor_costMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "Instructor_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costPeriod">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC costPeriod;

    public UDC getCostPeriod() {
        return costPeriod;
    }

    public void setCostPeriod(UDC costPeriod) {
        this.costPeriod = costPeriod;
    }

    public String getCostPeriodDD() {
        return "Instructor_costPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "Instructor_provider";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exceptions">
    @OneToMany(mappedBy = "instructor")
    private List<InstructorException> exceptions;

    public List<InstructorException> getExceptions() {
        return exceptions;
    }

    public void setExceptions(List<InstructorException> exceptions) {
        this.exceptions = exceptions;
    }

    public String getExceptionsDD() {
        return "Instructor_exceptions";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="skills">
    @OneToMany(mappedBy = "instructor")
    private List<InstructorSkill> skills;

    public List<InstructorSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<InstructorSkill> skills) {
        this.skills = skills;
    }

    public String getSkillsDD() {
        return "Instructor_skills";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="personalInfo">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade = {CascadeType.ALL}, optional = false, mappedBy = "instructor")
    private InstructorPerson personalInfo;

    public InstructorPerson getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(InstructorPerson personalInfo) {
        this.personalInfo = personalInfo;
    }

    public String getPersonalInfoDD() {
        return "Instructor_personalInfo";
    }
    //</editor-fold >
}
