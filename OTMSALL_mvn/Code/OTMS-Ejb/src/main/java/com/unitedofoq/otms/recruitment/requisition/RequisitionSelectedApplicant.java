package com.unitedofoq.otms.recruitment.requisition;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import com.unitedofoq.otms.recruitment.applicant.AppliedApplicant;
import com.unitedofoq.otms.recruitment.onlineexam.Exam;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity
@ParentEntity(fields = {"requisition"})
@ChildEntity(fields = {"employmentRequests", "itRequests"})
public class RequisitionSelectedApplicant extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public String getApplicantDD() {
        return "RequisitionSelectedApplicant_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="requisition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private Requisition requisition;

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }

    public Requisition getRequisition() {
        return requisition;
    }

    public String getRequisitionDD() {
        return "RequisitionSelectedApplicant_requisition";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="count">
    @Column
    private BigDecimal count;

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    public BigDecimal getCount() {
        return count;
    }

    public String getCountDD() {
        return "RequisitionSelectedApplicant_count";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="appliedApplicant">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppliedApplicant appliedApplicant;

    public AppliedApplicant getAppliedApplicant() {
        return appliedApplicant;
    }

    public void setAppliedApplicant(AppliedApplicant appliedApplicant) {
        this.appliedApplicant = appliedApplicant;
    }

    public String getAppliedApplicantDD() {
        return "RequisitionSelectedApplicant_appliedApplicant";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentStatus">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC currentStatus;

    public UDC getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(UDC currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCurrentStatusDD() {
        return "RequisitionSelectedApplicant_currentStatus";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nextAction">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nextAction;

    public UDC getNextAction() {
        return nextAction;
    }

    public void setNextAction(UDC nextAction) {
        this.nextAction = nextAction;
    }

    public String getNextActionDD() {
        return "RequisitionSelectedApplicant_nextAction";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentStatusWhen">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date currentStatusWhen;

    public Date getCurrentStatusWhen() {
        return currentStatusWhen;
    }

    public void setCurrentStatusWhen(Date currentStatusWhen) {
        this.currentStatusWhen = currentStatusWhen;
    }

    public String getCurrentStatusWhenDD() {
        return "RequisitionSelectedApplicant_currentStatusWhen";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentStatusWhat">
    private String currentStatusWhat;

    public String getCurrentStatusWhat() {
        return currentStatusWhat;
    }

    public void setCurrentStatusWhat(String currentStatusWhat) {
        this.currentStatusWhat = currentStatusWhat;
    }

    public String getCurrentStatusWhatDD() {
        return "RequisitionSelectedApplicant_currentStatusWhat";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentStatusWho">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee currentStatusWho;

    public Employee getCurrentStatusWho() {
        return currentStatusWho;
    }

    public void setCurrentStatusWho(Employee currentStatusWho) {
        this.currentStatusWho = currentStatusWho;
    }

    public String getCurrentStatusWhoDD() {
        return "RequisitionSelectedApplicant_currentStatusWho";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentStatusResult">
    private String currentStatusResult;

    public String getCurrentStatusResult() {
        return currentStatusResult;
    }

    public void setCurrentStatusResult(String currentStatusResult) {
        this.currentStatusResult = currentStatusResult;
    }

    public String getCurrentStatusResultDD() {
        return "RequisitionSelectedApplicant_currentStatusResult";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currentStatusComment">
    private String currentStatusComment;

    public String getCurrentStatusComment() {
        return currentStatusComment;
    }

    public void setCurrentStatusComment(String currentStatusComment) {
        this.currentStatusComment = currentStatusComment;
    }

    public String getCurrentStatusCommentDD() {
        return "RequisitionSelectedApplicant_currentStatusComment";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nextActionWhen">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date nextActionWhen;

    public Date getNextActionWhen() {
        return nextActionWhen;
    }

    public void setNextActionWhen(Date nextActionWhen) {
        this.nextActionWhen = nextActionWhen;
    }

    public String getNextActionWhenDD() {
        return "RequisitionSelectedApplicant_nextActionWhen";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nextActionWhat">
    private String nextActionWhat;

    public String getNextActionWhat() {
        return nextActionWhat;
    }

    public void setNextActionWhat(String nextActionWhat) {
        this.nextActionWhat = nextActionWhat;
    }

    public String getNextActionWhatDD() {
        return "RequisitionSelectedApplicant_nextActionWhat";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nextActionWho">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee nextActionWho;

    public Employee getNextActionWho() {
        return nextActionWho;
    }

    public void setNextActionWho(Employee nextActionWho) {
        this.nextActionWho = nextActionWho;
    }

    public String getNextActionWhoDD() {
        return "RequisitionSelectedApplicant_nextActionWho";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nextActionComment">
    private String nextActionComment;

    public String getNextActionComment() {
        return nextActionComment;
    }

    public void setNextActionComment(String nextActionComment) {
        this.nextActionComment = nextActionComment;
    }

    public String getNextActionCommentDD() {
        return "RequisitionSelectedApplicant_nextActionComment";
    }
    //</editor-fold>
    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    @Override
    public void PostLoad() {
        super.PostLoad();
        setM2mCheck(true);
    }
    @OneToMany(mappedBy = "requisitionSelectedApplicant")
    private List<RequisitionSelectedApplicantHistory> requisitionSelectedApplicantHistory;

    public List<RequisitionSelectedApplicantHistory> getRequisitionSelectedApplicantHistory() {
        return requisitionSelectedApplicantHistory;
    }

    public void setRequisitionSelectedApplicantHistory(List<RequisitionSelectedApplicantHistory> requisitionSelectedApplicantHistory) {
        this.requisitionSelectedApplicantHistory = requisitionSelectedApplicantHistory;
    }
    //<editor-fold defaultstate="collapsed" desc="exam">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Exam exam;

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public String getExamDD() {
        return "RequisitionSelectedApplicant_exam";
    }
    //</editor-fold>
    //    //<editor-fold defaultstate="collapsed" desc="currentExam">
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private Exam currentExam;
//
//    public Exam getCurrentExam() {
//        return currentExam;
//    }
//
//    public void setCurrentExam(Exam currentExam) {
//        this.currentExam = currentExam;
//    }
//
//    public String getCurrentExamDD() {
//        return "RequisitionSelectedApplicant_currentExam";
//    }
//    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD() {
        return "RequisitionSelectedApplicant_done";
    }
    //</editor-fold>
}
