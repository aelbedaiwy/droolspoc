package com.unitedofoq.otms.timemanagement.workingcalendar;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import java.util.Date;
import javax.persistence.*;

@Entity
@FABSEntitySpecs(customCascadeFields = {"hijriCalendar"})
public class HijriCalendarReflection extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="hijriCalendar">

    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private HijriCalendar hijriCalendar;

    public HijriCalendar getHijriCalendar() {
        return hijriCalendar;
    }

    public void setHijriCalendar(HijriCalendar reflection) {
        this.hijriCalendar = reflection;
    }

    public String getHijriCalendarDD() {
        return "HijriCalendar_hijriCalendar";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gregorianFrom">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date gregorianFrom;

    public void setGregorianFrom(Date gregorianFrom) {
        this.gregorianFrom = gregorianFrom;
    }

    public Date getGregorianFrom() {
        return gregorianFrom;
    }

    public String getGregorianFromDD() {
        return "HijriCalendarReflection_gregorianFrom";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="gregorianTo">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date gregorianTo;

    public void setGregorianTo(Date gregorianTo) {
        this.gregorianTo = gregorianTo;
    }

    public Date getGregorianTo() {
        return gregorianTo;
    }

    public String getGregorianToDD() {
        return "HijriCalendarReflection_gregorianTo";
    }
    // </editor-fold>
}
