/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.training;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields="company")
@ChildEntity(fields="facultys")
public class University extends BaseEntity {
     // <editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDD() {
        return "University_code";
    }
// </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="name">
    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;
     public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
     public String getNameTranslatedDD() {
         return "University_nameTranslated";
     }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="state">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC state;

        public UDC getState() {
            return state;
        }

        public void setState(UDC state) {
            this.state = state;
        }

        public String getStateDD() {
            return "University_state";
        }
        // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="city">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC city;
        public String getCityDD(){
            return "University_city";
        }

        public UDC getCity() {
            return city;
        }

        public void setCity(UDC city) {
            this.city = city;
        }

    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="country">
        @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
        private UDC country;
        public String getCountryDD(){
            return "University_country";
        }

        public UDC getCountry() {
            return country;
        }

        public void setCountry(UDC country) {
            this.country = country;
        }

    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="company">
     @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
     private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    public String getCompanyDD() {
        return "University_company";
    }
  
    // </editor-fold>
     // <editor-fold defaultstate="collapsed" desc="facultys">
    @OneToMany(mappedBy = "university")
    private List<Faculty> facultys;

    public List<Faculty> getFacultys() {
        return facultys;
    }

    public void setFacultys(List<Faculty> facultys) {
        this.facultys = facultys;
    }
      public String getFacultysDD() {
        return "University_facultys";
    }
      // </editor-fold>
}
