package com.unitedofoq.otms.recruitment.jobrequisition;


import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.occupation.OccupationElementRatingBase;
import com.unitedofoq.otms.foundation.occupation.Skill;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@ParentEntity(fields={"jobRequisition"})
public class JobReqSkill extends OccupationElementRatingBase {

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="skill_DBID", nullable=false)
    private Skill skill;

    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }   
    
    public String getSkillDD(){
        return "JobReqSkill_skill";
    }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn(name="JobRequisition_DBID", nullable=false)
    private JobRequisition jobRequisition;

    public JobRequisition getJobRequisition() {
        return jobRequisition;
    }

    public void setJobRequisition(JobRequisition jobRequisition) {
        this.jobRequisition = jobRequisition;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    @Override
    protected void PostLoad(){
        super.PostLoad();
        setM2mCheck(true);
    }
    
}