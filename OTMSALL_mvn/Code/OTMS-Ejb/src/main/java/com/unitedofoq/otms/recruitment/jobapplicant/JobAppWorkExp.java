/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.jobapplicant;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author mmohamed
 */
@Entity
public class JobAppWorkExp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long jobapplicant_dbid;

    public Long getJobapplicant_dbid() {
        return jobapplicant_dbid;
    }

    public void setJobapplicant_dbid(Long jobapplicant_dbid) {
        this.jobapplicant_dbid = jobapplicant_dbid;
    }

    int duration;
    public String getDurationDD() {
        return "JobAppWorkExp_duration";
    }
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jobapplicant_dbid != null ? jobapplicant_dbid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobAppWorkExp)) {
            return false;
        }
        JobAppWorkExp other = (JobAppWorkExp) object;
        if ((this.jobapplicant_dbid == null && other.jobapplicant_dbid != null) || (this.jobapplicant_dbid != null && !this.jobapplicant_dbid.equals(other.jobapplicant_dbid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.unitedofoq.fabs.recruitment.jobapplicant.JobAppWorkExp[id=" + jobapplicant_dbid + "]";
    }

}
