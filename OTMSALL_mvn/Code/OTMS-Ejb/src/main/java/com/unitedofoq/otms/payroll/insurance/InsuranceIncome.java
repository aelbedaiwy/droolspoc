package com.unitedofoq.otms.payroll.insurance;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import java.math.BigDecimal;

/**
 * 
 */
@Entity
@ParentEntity(fields={"insurance"})
public class InsuranceIncome extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="insurance">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Insurance insurance;
    public Insurance getInsurance() {
        return insurance;
    }
    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }
    public String getInsuranceDD() {
        return "InsuranceIncome_insurance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="basicOrVariable">
	@Column(length=1)
	private String basicOrVariable;
    public String getBasicOrVariable() {
        return basicOrVariable;
    }
    public void setBasicOrVariable(String basicOrVariable) {
        this.basicOrVariable = basicOrVariable;
    }

    
    public String getBasicOrVariableDD() {
        return "InsuranceIncome_basicOrVariable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="income">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;
    public Income getIncome() {
        return income;
    }
    public void setIncome(Income income) {
        this.income = income;
    }
    public String getIncomeDD() {
        return "InsuranceIncome_income";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="maximumValue">
    @Column(precision=25, scale=13, nullable=false)
    private BigDecimal maximumValue;

    public BigDecimal getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(BigDecimal maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getMaximumValueDD() {
        return "InsuranceIncome_maximumValue";
    }
    @Transient
    private BigDecimal maximumValueMask;

    public BigDecimal getMaximumValueMask() {
        maximumValueMask = maximumValue ;
        return maximumValueMask;
    }

    public void setMaximumValueMask(BigDecimal maximumValueMask) {
        updateDecimalValue("maximumValue",maximumValueMask);
    }

    public String getMaximumValueMaskDD() {
        return "InsuranceIncome_maximumValueMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="percent">
    @Column(precision=25, scale=13,nullable=false,name="insurancePercent")
    private BigDecimal percent;
    public BigDecimal getPercent() {
        return percent;
    }
    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }
    public String getPercentDD() {
        return "InsuranceIncome_percent";
    }
    @Transient
    private BigDecimal percentMask;
    public BigDecimal getPercentMask() {
        percentMask = percent ;
        return percentMask;
    }
    public void setPercentMask(BigDecimal percentMask) {
        updateDecimalValue("percent",percentMask);
    }
    public String getPercentMaskDD() {
        return "InsuranceIncome_percentMask";
    }
    // </editor-fold>
}