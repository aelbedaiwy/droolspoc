/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import com.unitedofoq.otms.foundation.competency.CompetencyLevel;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author aibrahim
 */
@Entity
public class PersonnelCompetencyBase extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="competency">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Competency competency;
    public Competency getCompetency() {
        return competency;
    }
    public String getCompetencyDD() {
        return "PersonnelCompetencyBase_competency";
    }
    public void setCompetency(Competency competency) {
        this.competency = competency;
    }    
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competencyLevel">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CompetencyLevel competencyLevel;
    public CompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }
    public void setCompetencyLevel(CompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }
     public String getCompetencyLevelDD() {
        return "PersonnelCompetencyBase_competencyLevel";
    }
    // </editor-fold>
  }
