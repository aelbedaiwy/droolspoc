/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.unit;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.*;


/**
 *
 * @author abayomy
 */
@Entity
@ParentEntity(fields="company")
@DiscriminatorValue("MASTER")
public class UnitLevel extends UnitLevelBase  {
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "UnitLevel_company";
    }
    // </editor-fold>
 /// Added By Ayman
    // <editor-fold defaultstate="collapsed" desc="levelId">
    @Column
    private String levelId;

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }
     public String getLevelIdDD() {
        return "UnitLevel_levelId";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="units">
    @OneToMany(mappedBy="unitLevel")
	private List<Unit> units;
    public List<Unit> getUnits() {
        return units;
    }
    public void setUnits(List<Unit> units) {
        this.units = units;
    }
     public String getUnitsDD() {
        return "UnitLevel_units";
    }
    // </editor-fold>
   
}
