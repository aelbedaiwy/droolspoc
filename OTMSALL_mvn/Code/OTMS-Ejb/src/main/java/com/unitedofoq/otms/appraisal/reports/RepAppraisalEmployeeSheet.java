/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.appraisal.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.ReadOnly;

/**
 *
 * @author lap2
 */
@Entity
@ReadOnly
public class RepAppraisalEmployeeSheet extends RepEmployeeBase
{
    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepAppraisalEmployeeSheet_dsDbid";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="targetWeight">
    @Column(scale=25,precision=13)
    private BigDecimal targetWeight;

    public BigDecimal getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(BigDecimal targetWeight) {
        this.targetWeight = targetWeight;
    }
    
    public String getTargetWeightDD() {
        return "RepAppraisalEmployeeSheet_targetWeight";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="targetDescription">
    private String targetDescription;

    public String getTargetDescription() {
        return targetDescription;
    }

    public void setTargetDescription(String targetDescription) {
        this.targetDescription = targetDescription;
    }
    
    public String getTargetDescriptionDD() {
        return "RepAppraisalEmployeeSheet_targetDescription";
    }    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescriptionDD() {
        return "RepAppraisalEmployeeSheet_description";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="type">
    @Translatable(translationField = "typeTranslated")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getTypeDD() {
        return "RepAppraisalEmployeeSheet_type";
    }
    
    @Transient
    @Translation(originalField = "type")
    private String typeTranslated;

    public String getTypeTranslated() {
        return typeTranslated;
    }

    public void setTypeTranslated(String typeTranslated) {
        this.typeTranslated = typeTranslated;
    }
    
    public String getTypeTranslatedDD() {
        return "RepAppraisalEmployeeSheet_type";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="template">
    @Column
    @Translatable(translationField = "templateTranslated")
    private String template;

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public String getTemplateDD() {
        return "RepAppraisalEmployeeSheet_template";
    }
    
    @Transient
    @Translation(originalField = "template")
    private String templateTranslated;

    public String getTemplateTranslated() {
        return templateTranslated;
    }

    public void setTemplateTranslated(String templateTranslated) {
        this.templateTranslated = templateTranslated;
    }
    
    public String getTemplateTranslatedDD() {
        return "RepAppraisalEmployeeSheet_template";
    }
    // </editor-fold>
       
    // <editor-fold defaultstate="collapsed" desc="year">
    @Column
    private String year;

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public String getYearDD() {
        return "RepAppraisalEmployeeSheet_year";
    }
    
    // </editor-fold>
}