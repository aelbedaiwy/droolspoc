
package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.training.company.CompanyCourse;
import javax.persistence.*;

@Entity
public class ProviderCourse extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="code">
    @Column(unique=true,nullable=false)
    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getCodeDD() {
        return "ProviderCourse_code";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField = "descriptionTranslated")
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionDD() {
        return "ProviderCourse_description";
    }
    
    @Transient
    @Translation(originalField = "description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD() {
        return "ProviderCourse_description";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="learningMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC learningMethod;

    public void setLearningMethod(UDC learningMethod) {
        this.learningMethod = learningMethod;
    }

    public UDC getLearningMethod() {
        return learningMethod;
    }

    public String getLearningMethodDD() {
        return "ProviderCourse_learningMethod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="preRequisites">
    @Column
    @Translatable(translationField = "preRequisitesTranslated")
    private String preRequisites;

    public void setPreRequisites(String preRequisites) {
        this.preRequisites = preRequisites;
    }

    public String getPreRequisites() {
        return preRequisites;
    }

    public String getPreRequisitesDD() {
        return "ProviderCourse_preRequisites";
    }
    @Transient
    @Translation(originalField = "preRequisites")
    private String preRequisitesTranslated;

    public String getPreRequisitesTranslated() {
        return preRequisitesTranslated;
    }

    public void setPreRequisitesTranslated(String preRequisitesTranslated) {
        this.preRequisitesTranslated = preRequisitesTranslated;
    }
    
    public String getPreRequisitesTranslatedDD() {
        return "ProviderCourse_preRequisites";
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="content">
    @Column
    @Translatable(translationField = "contentTranslated")
    private String content;

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getContentDD() {
        return "ProviderCourse_content";
    }
    
    @Transient
    @Translation(originalField = "content")
    private String contentTranslated;

    public String getContentTranslated() {
        return contentTranslated;
    }

    public void setContentTranslated(String contentTranslated) {
        this.contentTranslated = contentTranslated;
    }
    
    public String getContentTranslatedDD() {
        return "ProviderCourse_content";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="forWhom">
    @Column
    private String forWhom;

    public void setForWhom(String forWhom) {
        this.forWhom = forWhom;
    }

    public String getForWhom() {
        return forWhom;
    }

    public String getForWhomDD() {
        return "ProviderCourse_forWhom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseClass courseClass;

    public void setCourseClass(ProviderCourseClass courseClass) {
        this.courseClass = courseClass;
    }

    public ProviderCourseClass getCourseClass() {
        return courseClass;
    }

    public String getCourseClassDD() {
        return "ProviderCourse_courseClass";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companyCourse">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CompanyCourse companyCourse;

    public void setCompanyCourse(CompanyCourse companyCourse) {
        this.companyCourse = companyCourse;
    }

    public CompanyCourse getCompanyCourse() {
        return companyCourse;
    }

    public String getCompanyCourseDD() {
        return "ProviderCourse_companyCourse";
    }
    // </editor-fold>

}
