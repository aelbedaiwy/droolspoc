/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.jobGroup;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.competency.Competency;
import com.unitedofoq.otms.foundation.competency.CompetencyLevel;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author ashienawy
 */
@Entity
@ParentEntity(fields={"jobGroup"})
//@ChildEntity(fields={"levels"})
//@ParentEntity(fields="competencyGroup")
public class JobGroupCompetency extends BusinessObjectBaseEntity  {
   // <editor-fold defaultstate="collapsed" desc="jobGroup">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private JobGroup jobGroup;

    public JobGroup getJobGroup() {
        return jobGroup;
    }
    public String getJobGroupDD() {
        return "JobGroupCompetency_jobGroup";
    }
    public void setJob(JobGroup jobGroup) {
        this.jobGroup = jobGroup;
    }
    // </editor-fold>
  // <editor-fold defaultstate="collapsed" desc="competencyLevel">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private CompetencyLevel competencyLevel;

    public CompetencyLevel getCompetencyLevel() {
        return competencyLevel;
    }
    public String getCompetencyLevelDD() {
        return "JobGroupCompetency_competencyLevel";
    }
    public void setCompetencyLevel(CompetencyLevel competencyLevel) {
        this.competencyLevel = competencyLevel;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="competency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Competency competency;

    public Competency getCompetency() {
        return competency;
    }
    public String getCompetencyDD() {
        return "JobGroupCompetency_competency";
    }
    public void setCompetency(Competency competency) {
        this.competency = competency;
    }
    // </editor-fold>
}
