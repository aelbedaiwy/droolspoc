/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.instructor;

import javax.persistence.Entity;
import com.unitedofoq.fabs.core.data.Person;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 *
 * @author mragab
 */
@Entity
@DiscriminatorValue("INSSTRUCTOR")
@ChildEntity(fields={"instructor"})
public class InstructorPerson extends Person {
    // <editor-fold defaultstate="collapsed" desc="instructor">
	@JoinColumn
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private Instructor instructor;

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public String getInstructorDD() {
        return "InstructorPerson_instructor";
    }
    // </editor-fold>
}