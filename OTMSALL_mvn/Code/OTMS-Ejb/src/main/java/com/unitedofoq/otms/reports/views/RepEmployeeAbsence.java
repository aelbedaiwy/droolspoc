package com.unitedofoq.otms.reports.views;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.ReadOnly;


@Entity
@ReadOnly
@Table(name="repemployeeabsence")
public class RepEmployeeAbsence extends RepEmpVacationAbsenceBase {
 
}
