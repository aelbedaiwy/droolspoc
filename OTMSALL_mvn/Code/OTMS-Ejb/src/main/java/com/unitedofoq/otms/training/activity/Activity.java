/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author mragab
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ACTIVITYTYPE")
@ChildEntity(fields={"preSkills", "preActivities","upgradedSkills","costIems"})
public class Activity extends ActivityBase {
    // <editor-fold defaultstate="collapsed" desc="activityClass">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
	private ActivityClass activityClass;

    public ActivityClass getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(ActivityClass activityClass) {
        this.activityClass = activityClass;
    }

    public String getActivityClassDD() {
        return "Activity_activityClass";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="minimumAttendPercent">
    @Column
    private Short minimumAttendPercent;

    public Short getMinimumAttendPercent() {
        return minimumAttendPercent;
    }

    public void setMinimumAttendPercent(Short minimumAttendPercent) {
        this.minimumAttendPercent = minimumAttendPercent;
    }

    public String getMinimumAttendPercentDD() {
        return "Activity_minimumAttendPercent";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="attendanceEvaluation">
    @Column
    private boolean attendanceEvaluation;

    public boolean isAttendanceEvaluation() {
        return attendanceEvaluation;
    }

    public void setAttendanceEvaluation(boolean attendanceEvaluation) {
        this.attendanceEvaluation = attendanceEvaluation;
    }

    public String getAttendanceEvaluationDD() {
        return "Activity_attendanceEvaluation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="performanceEvaluation">
    @Column
    private boolean performanceEvaluation;

    public boolean isPerformanceEvaluation() {
        return performanceEvaluation;
    }

    public void setPerformanceEvaluation(boolean performanceEvaluation) {
        this.performanceEvaluation = performanceEvaluation;
    }

    public String getPerformanceEvaluationDD() {
        return "Activity_performanceEvaluation";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="topics">
	private String topics;

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getTopicsDD() {
        return "Activity_topics";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="preSkills">
    @OneToMany(mappedBy="activity")
	private List<ActivityPreSkill> preSkills;

    public List<ActivityPreSkill> getPreSkills() {
        return preSkills;
    }

    public void setPreSkills(List<ActivityPreSkill> preSkills) {
        this.preSkills = preSkills;
    }

    public String getPreSkillsDD() {
        return "Activity_preSkills";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="preActivities">
    @OneToMany(mappedBy="activity")
	private List<ActivityPreActivity> preActivities;

    public List<ActivityPreActivity> getPreActivities() {
        return preActivities;
    }

    public void setPreActivities(List<ActivityPreActivity> preActivities) {
        this.preActivities = preActivities;
    }

    public String getPreActivitiesDD() {
        return "Activity_preActivities";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="upgradedSkills">
    @OneToMany(mappedBy="activity")
	private List<ActivityUpgradeSkill> upgradedSkills;

    public List<ActivityUpgradeSkill> getUpgradedSkills() {
        return upgradedSkills;
    }

    public void setUpgradedSkills(List<ActivityUpgradeSkill> upgradedSkills) {
        this.upgradedSkills = upgradedSkills;
    }

    public String getUpgradedSkillsDD() {
        return "Activity_upgradedSkills";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="costIems">
    @OneToMany(mappedBy="activity")
	private List<ActivityCostItem> costIems;

    public List<ActivityCostItem> getCostIems() {
        return costIems;
    }

    public void setCostIems(List<ActivityCostItem> costIems) {
        this.costIems = costIems;
    }

    public String getCostIemsDD() {
        return "Activity_costIems";
    }
    // </editor-fold>
}