package com.unitedofoq.otms.recruitment.jobapplicant;



import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.jobrequisitionapplicant.JobRequisitionApplicant;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@ChildEntity(fields={"jobAppAbilities", "jobAppEducation", "jobAppInterests" ,
        "jobAppKnowledges" , "jobAppLanguages", "jobAppSkills", "jobAppRejection",
        "personalInfo", "jobAppNominations", "cv", "jobAppWorkHistory"})
public class JobApplicant extends BaseEntity {

    private String recommendation;

    public String getRecommendationDD() {
        return "JobApplicant_recommendation";
    }
    private String customKnowlege;

    public String getCustomKnowlegeDD() {
        return "JobApplicant_customKnowlege";
    }
    private String customInterest;

    public String getCustomInterestDD() {
        return "JobApplicant_customInterest";
    }
    private String customExperience;

    public String getCustomExperienceDD() {
        return "JobApplicant_customExperience";
    }
    private String hearAboutUsCustom;

    public String getHearAboutUsCustomDD() {
        return "JobApplicant_hearAboutUsCustom";
    }
    private String customAbility;

    public String getCustomAbilityDD() {
        return "JobApplicant_customAbility";
    }
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobRequisitionApplicant> jobRequisitionApplicants;

    //<editor-fold defaultstate="collapsed" desc="personalInfo">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, cascade={CascadeType.ALL},optional = false, mappedBy="jobApplicant")
    private JobAppPerson personalInfo;
    public JobAppPerson getPersonalInfo() {
        return personalInfo;
    }
    public void setPersonalInfo(JobAppPerson thePersonalInfo) {
        personalInfo = thePersonalInfo;
    }

    @Override
    public void PrePersist() {
        super.PrePersist();
        if (personalInfo != null)
            if (personalInfo.getJobApplicant() == null)
                personalInfo.setJobApplicant(this);
    }

    public String getPersonalInfoDD(){
        return "JobApplicant_personalInfo";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="jobApplicantUser">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "jobApplicant")
    private JobAppUser jobApplicantUser;
    public JobAppUser getJobApplicantUser() {
        return jobApplicantUser;
    }
    public void setJobApplicantUser(JobAppUser theJobApplicantUser) {
        jobApplicantUser = theJobApplicantUser;
    }
    //</editor-fold>

    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppAbility> jobAppAbilities = new ArrayList<JobAppAbility>();
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppSkill> jobAppSkills = new ArrayList<JobAppSkill>();
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppInterest> jobAppInterests = new ArrayList<JobAppInterest>();
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppKnowledge> jobAppKnowledges = new ArrayList<JobAppKnowledge>();
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppLanguage> jobAppLanguages = new ArrayList<JobAppLanguage>();
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppNomination> jobAppNominations = new ArrayList<JobAppNomination>();
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    private JobApplicantCV cv;
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppEducation> jobAppEducation = new ArrayList<JobAppEducation>();
    @ManyToOne
    private Employee hiredAsEmployee;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC status;
    @ManyToOne
    private UDC hearAboutUs;
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    private JobAppSearchIndex jobAppSearchIndex;
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppWorkHistory> jobAppWorkHistory = new ArrayList<JobAppWorkHistory>();
    @OneToMany(mappedBy = "jobApplicant")
    private List<JobAppRejection> jobAppRejection = new ArrayList<JobAppRejection>();

    public List<JobRequisitionApplicant> getJobRequisitionApplicants() {
        return jobRequisitionApplicants;
    }

    public void setJobRequisitionApplicants(List<JobRequisitionApplicant> jobRequisitionApplicants) {
        this.jobRequisitionApplicants = jobRequisitionApplicants;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String theRecommendation) {
        recommendation = theRecommendation;
    }

    public String getCustomKnowlege() {
        return customKnowlege;
    }

    public void setCustomKnowlege(String theCustomKnowlege) {
        customKnowlege = theCustomKnowlege;
    }

    public String getCustomInterest() {
        return customInterest;
    }

    public void setCustomInterest(String theCustomInterest) {
        customInterest = theCustomInterest;
    }

    public String getCustomExperience() {

        return customExperience;
    }

    public void setCustomExperience(String theCustomExperience) {
        customExperience = theCustomExperience;
    }

    public String getCustomAbility() {
        return customAbility;
    }

    public void setCustomAbility(String theCustomAbility) {
        customAbility = theCustomAbility;
    }

    public JobApplicantCV getCv() {
        return cv;
    }

    public void setCv(JobApplicantCV theCv) {
        cv = theCv;
    }

    public Employee getHiredAsEmployee() {
        return hiredAsEmployee;
    }

    public void setHiredAsEmployee(Employee theHiredAsEmployee) {
        hiredAsEmployee = theHiredAsEmployee;
    }

    public UDC getStatus() {
        return status;
    }

    public void setStatus(UDC theStatus) {
        status = theStatus;
    }

    public UDC getHearAboutUs() {
        return hearAboutUs;
    }

    public void setHearAboutUs(UDC theHearAboutUs) {
        hearAboutUs = theHearAboutUs;
    }

    public String getHearAboutUsCustom() {
        return hearAboutUsCustom;
    }

    public void setHearAboutUsCustom(String theHearAboutUsCustom) {
        hearAboutUsCustom = theHearAboutUsCustom;
    }

    public JobAppSearchIndex getJobAppSearchIndex() {
        return jobAppSearchIndex;
    }

    public void setJobAppSearchIndex(JobAppSearchIndex theJobAppSearchIndex) {
        jobAppSearchIndex = theJobAppSearchIndex;
    }

    public List<JobAppAbility> getJobAppAbilities() {
        return jobAppAbilities;
    }

    public void setJobAppAbilities(List<JobAppAbility> jobAppAbilities) {
        this.jobAppAbilities = jobAppAbilities;
    }

    public List<JobAppEducation> getJobAppEducation() {
        return jobAppEducation;
    }

    public void setJobAppEducation(List<JobAppEducation> jobAppEducation) {
        this.jobAppEducation = jobAppEducation;
    }

    public List<JobAppWorkHistory> getJobAppWorkHistory() {
        return jobAppWorkHistory;
    }

    public void setJobAppWorkHistory(List<JobAppWorkHistory> jobAppWorkHistory) {
        this.jobAppWorkHistory = jobAppWorkHistory;
    }

    public List<JobAppInterest> getJobAppInterests() {
        return jobAppInterests;
    }

    public void setJobAppInterests(List<JobAppInterest> jobAppInterests) {
        this.jobAppInterests = jobAppInterests;
    }

    public List<JobAppKnowledge> getJobAppKnowledges() {
        return jobAppKnowledges;
    }

    public void setJobAppKnowledges(List<JobAppKnowledge> jobAppKnowledges) {
        this.jobAppKnowledges = jobAppKnowledges;
    }

    public List<JobAppLanguage> getJobAppLanguages() {
        return jobAppLanguages;
    }

    public void setJobAppLanguages(List<JobAppLanguage> jobAppLanguages) {
        this.jobAppLanguages = jobAppLanguages;
    }

    public List<JobAppNomination> getJobAppNominations() {
        return jobAppNominations;
    }

    public void setJobAppNominations(List<JobAppNomination> jobAppNominations) {
        this.jobAppNominations = jobAppNominations;
    }

    public List<JobAppRejection> getJobAppRejection() {
        return jobAppRejection;
    }

    public void setJobAppRejection(List<JobAppRejection> jobAppRejection) {
        this.jobAppRejection = jobAppRejection;
    }

    public List<JobAppSkill> getJobAppSkills() {
        return jobAppSkills;
    }

    public void setJobAppSkills(List<JobAppSkill> jobAppSkills) {
        this.jobAppSkills = jobAppSkills;
    }
}
