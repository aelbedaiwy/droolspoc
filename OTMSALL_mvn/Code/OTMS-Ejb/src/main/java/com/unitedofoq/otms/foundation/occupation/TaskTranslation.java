
package com.unitedofoq.otms.foundation.occupation;

import javax.persistence.*;
import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;

@Entity
public class TaskTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="name">
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>

}
