/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.budget;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields="company")
public class BudgetElement extends BusinessObjectBaseEntity {
    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD(){
        return "BudgetElement_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    
    public String getDescriptionTranslatedDD(){
        return "BudgetElement_description";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    public Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    
    public String getCurrencyDD(){
        return "BudgetElement_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private UDC budgetType;

    public UDC getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(UDC budgetType) {
        this.budgetType = budgetType;
    }
    
    public String getBudgetTypeDD(){
        return "BudgetElement_budgetType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD(){
        return "BudgetElement_company";
    }
    // </editor-fold>
    
//    //<editor-fold defaultstate="collapsed" desc="internalType">
//    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
//    private SystemLookUpDetail internalType;
//
//    public SystemLookUpDetail getInternalType() {
//        return internalType;
//    }
//
//    public void setInternalType(SystemLookUpDetail internalType) {
//        this.internalType = internalType;
//    }
//    
//    public String getInternalTypeDD() {
//        return "BudgetElement_internalType";
//    }
//    //</editor-fold>
}
