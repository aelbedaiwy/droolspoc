package com.unitedofoq.otms.payroll.formula;

import javax.persistence.*;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;



/**
 * The persistent class for the main_query_table database table.
 * 
 */
@Entity
public class FormulaQuery extends BaseEntity
{
  
    // <editor-fold defaultstate="collapsed" desc="arithmaticOperator">
	@Column
	private String arithmaticOperator;
    public String getArithmaticOperator() {
        return arithmaticOperator;
    }
    public void setArithmaticOperator(String arithmaticOperator) {
        this.arithmaticOperator = arithmaticOperator;
    }
    
    public String getArithmaticOperatorDD(){
        return "FormulaQuery_arithmaticOperator";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fullToken">
	@Column
	private String fullToken;
    public String getFullToken() {
        return fullToken;
    }
    public void setFullToken(String fullToken) {
        this.fullToken = fullToken;
    }
    
    public String getFullTokenDD(){
        return "FormulaQuery_fullToken";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="columnName">
	@Column
	private String columnName;
    public String getColumnName() {
        return columnName;
    }
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnNameDD(){
        return "FormulaQuery_columnName";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="columnType">
	@Column //FIXME: to be UDC
	private String columnType;
    public String getColumnType() {
        return columnType;
    }
    
    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnTypeDD(){
        return "FormulaQuery_columnType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="startTerminator">
	@Column
	private String startTerminator;
    public String getStartTerminator() {
        return startTerminator;
    }
    public void setStartTerminator(String startTerminator) {
        this.startTerminator = startTerminator;
    }

    public String getStartTerminatorDD(){
        return "FormulaQuery_startTerminator";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="endTerminator">
	@Column
	private String endTerminator;
    public String getEndTerminator() {
        return endTerminator;
    }
    public void setEndTerminator(String endTerminator) {
        this.endTerminator = endTerminator;
    }

    public String getEndTerminatorDD(){
        return "FormulaQuery_endTerminator";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="logicalOperator">
	@Column
	private String logicalOperator;
    public String getLogicalOperator() {
        return logicalOperator;
    }

    public void setLogicalOperator(String logicalOperator) {
        this.logicalOperator = logicalOperator;
    }
    
    public String getLogicalOperatorDD(){
        return "FormulaQuery_logicalOperator";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="multi">
	@Column
	private String multi;
    public String getMulti() {
        return multi;
    }
    public void setMulti(String multi) {
        this.multi = multi;
    }

    public String getMultiDD(){
        return "FormulaQuery_multi";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="queryName">
	@Column(nullable=false)
	private String queryName;
    public String getQueryName() {
        return queryName;
    }
    
    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public String getQueryNameDD(){
        return "FormulaQuery_queryName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="result">
	@Column
	private String result;
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public String getResultDD(){
        return "FormulaQuery_result";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="querySequence">
	//Serial per query need to be reviewed
	@Column
	private int querySequence;
    public int getQuerySequence() {
        return querySequence;
    }
    public void setQuerySequence(int querySequence) {
        this.querySequence = querySequence;
    }

    public String getQuerySequenceDD(){
        return "FormulaQuery_querySequence";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    //Serial per query need to be reviewed
	@Column(nullable=false)
	private String sortIndex;
    public String getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(String sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD(){
        return "FormulaQuery_sortIndex";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="systemUse">
	@Column(length=1)
	private String systemUse;
    public String getSystemUse() {
        return systemUse;
    }
    public void setSystemUse(String systemUse) {
        this.systemUse = systemUse;
    }

    public String getSystemUseDD(){
        return "FormulaQuery_systemUse";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tableName">
	@Column
	private String tableName;
    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableNameDD(){
        return "FormulaQuery_tableName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="firstValue">
	@Column
	private String firstValue;
    public String getFirstValue() {
        return firstValue;
    }
    public void setFirstValue(String firstValue) {
        this.firstValue = firstValue;
    }

    public String getFirstValueDD(){
        return "FormulaQuery_firstValue";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="secondValue">
	@Column
	private String secondValue;
    public String getSecondValue() {
        return secondValue;
    }
    
    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue;
    }

    public String getSecondValueDD(){
        return "FormulaQuery_secondValue";
    }
    // </editor-fold>

}