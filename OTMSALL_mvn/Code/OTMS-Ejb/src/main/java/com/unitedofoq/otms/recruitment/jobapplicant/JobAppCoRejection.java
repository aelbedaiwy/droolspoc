
package com.unitedofoq.otms.recruitment.jobapplicant;

import com.unitedofoq.otms.foundation.company.Company;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class JobAppCoRejection extends JobAppRejection {

	@ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private Company company;
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company theCompany) {
		company = theCompany;
	}
}
