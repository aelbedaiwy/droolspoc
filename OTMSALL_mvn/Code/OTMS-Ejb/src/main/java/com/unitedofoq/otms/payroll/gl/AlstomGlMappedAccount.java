package com.unitedofoq.otms.payroll.gl;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
@ParentEntity (fields="company")
public class AlstomGlMappedAccount extends BaseEntity {
    
    //<editor-fold defaultstate="collapse" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getCompanyDD() {
        return "AlstomMappedAccount_company";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="salaryElement">
    @ManyToOne (fetch= javax.persistence.FetchType.LAZY)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }
    
    public String getSalaryElementDD() {
        return "AlstomGlMappedAccount_salaryElement";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="location">
    @ManyToOne (fetch= javax.persistence.FetchType.LAZY)
    private UDC location;

    public UDC getLocation() {
        return location;
    }

    public void setLocation(UDC location) {
        this.location = location;
    }
    
    public String getLocationDD() {
        return "AlstomGlMappedAccount_location";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="creditAccount">
    private String creditAccount;

    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }
    
    
    
    public String getCreditAccountDD() {
        return "AlstomGlMappedAccount_creditAccount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="debitAccount">
    private String debitAccount;

    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }
    
    public String getDebitAccountDD() {
        return "AlstomGlMappedAccount_debitAccount";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapse" desc="glMappedAccount">
    @ManyToOne
    private GLMappedAccount glMappedAccount;

    public GLMappedAccount getGlMappedAccount() {
        return glMappedAccount;
    }

    public void setGlMappedAccount(GLMappedAccount glMappedAccount) {
        this.glMappedAccount = glMappedAccount;
    }
    
    public String getGlMappedAccountDD() {
        return "AlstomGlMappedAccount_glMappedAccount";
    }

    //<editor-fold defaultstate="collapse" desc="accruedAcct">
    private String accruedAcct;

    public String getAccruedAcct() {
        return accruedAcct;
    }
    public void setAccruedAcct(String accruedAcct) {
        this.accruedAcct = accruedAcct;
    }
    
    public String getAccruedAcctDD() {
        return "AlstomGlMappedAccount_accruedAcct";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapse" desc="SalaryElementType">
    private String salaryElementType;

    public String getSalaryElementType() {
        return salaryElementType;
    }

    public void setSalaryElementType(String salaryElementType) {
        this.salaryElementType = salaryElementType;
    }
    
    public String getSalaryElementTypeDD() {
        return "AlstomGlMappedAccount_SalaryElementType;";
    }
    //</editor-fold>


}
