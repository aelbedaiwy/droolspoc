
package com.unitedofoq.otms.training.reports;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.reports.views.RepEmployeeBase;
import java.util.Date;
import javax.persistence.*;

@Entity
public class RepEmpCourseRequest extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column
    private long dsDbid;

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    public long getDsDbid() {
        return dsDbid;
    }

    public String getDsDbidDD() {
        return "RepEmpCourseRequest_dsDbid";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="courseClass">
    @Column
    @Translatable(translationField = "courseClassTranslated")
    private String courseClass;

    public String getCourseClass() {
        return courseClass;
    }

    public void setCourseClass(String courseClass) {
        this.courseClass = courseClass;
    }
 
    public String getCourseClassDD() {
        return "RepEmpCourseRequest_courseClass";
    }
    
    @Transient
    @Translation(originalField = "courseClass")
    private String courseClassTranslated;

    public String getCourseClassTranslated() {
        return courseClassTranslated;
    }

    public void setCourseClassTranslated(String courseClassTranslated) {
        this.courseClassTranslated = courseClassTranslated;
    }

    public String getCourseClassTranslatedDD() {
        return "RepEmpCourseRequest_courseClasss";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="course">
    @Column
    @Translatable(translationField = "courseTranslated")
    private String course;

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourse() {
        return course;
    }

    public String getCourseDD() {
        return "RepEmpCourseRequest_course";
    }
    
    @Transient
    @Translation(originalField = "course")
    private String courseTranslated;

    public String getCourseTranslated() {
        return courseTranslated;
    }

    public void setCourseTranslated(String courseTranslated) {
        this.courseTranslated = courseTranslated;
    }

    public String getCourseTranslatedDD() {
        return "RepEmpCourseRequest_course";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="requestStatus">
    @Column
    @Translatable(translationField = "requestStatusTranslated")
    private String requestStatus;

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }
    
    public String getRequestStatusDD() {
        return "RepEmpCourseRequest_requestStatus";
    }
    
    @Transient
    @Translation(originalField = "requestStatus")
    private String requestStatusTranslated;

    public String getRequestStatusTranslated() {
        return requestStatusTranslated;
    }

    public void setRequestStatusTranslated(String requestStatusTranslated) {
        this.requestStatusTranslated = requestStatusTranslated;
    }

    public String getRequestStatusTranslatedDD() {
        return "RepEmpCourseRequest_requestStatus";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public String getRequestDateDD() {
        return "RepEmpCourseRequest_requestDate";
    }
    // </editor-fold>
}