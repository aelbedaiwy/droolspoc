/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;


import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"trainingRequest"})
public class TrainingRequestCheckList extends BusinessObjectBaseEntity {
    
    // <editor-fold defaultstate="collapsed" desc="trainingRequest">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TrainingRequest trainingRequest;

    public TrainingRequest getTrainingRequest() {
        return trainingRequest;
    }

    public void setTrainingRequest(TrainingRequest trainingRequest) {
        this.trainingRequest = trainingRequest;
    }

    public String getTrainingRequestDD() {
        return "TrainingRequestCheckList_trainingRequest";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="trainingEnrollementCheckList">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name= "trainenrlchk_dbid")
    private TrainingEnrollementCheckList trainingEnrollementCheckList;

    public TrainingEnrollementCheckList getTrainingEnrollementCheckList() {
        return trainingEnrollementCheckList;
    }

    public void setTrainingEnrollementCheckList(TrainingEnrollementCheckList trainingEnrollementCheckList) {
        this.trainingEnrollementCheckList = trainingEnrollementCheckList;
    }
    
    public String getTrainingEnrollementCheckListDD() {
        return "TrainingPlanCheckList_trainingEnrollementCheckList";
    }
    // </editor-fold>
}