package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Transient;


@MappedSuperclass
public class RepEmpVacationAbsenceBase extends RepEmployeeBase {
    //<editor-fold defaultstate="collapsed" desc="dsDbid">
    @Column

    private long dsDbid;

    public long getDsDbid() {
        return dsDbid;
    }

    public void setDsDbid(long dsDbid) {
        this.dsDbid = dsDbid;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="genderDescription">
    @Column
    @Translatable(translationField = "genderDescriptionTranslated")
    private String genderDescription;

    public String getGenderDescription() {
        return genderDescription;
    }

    public void setGenderDescription(String genderDescription) {
        this.genderDescription = genderDescription;
    }

    public String getGenderDescriptionDD() {
        return "RepEmpVacationAbsenceBase_genderDescription";
    }

    @Transient
    @Translation(originalField = "genderDescription")
    private String genderDescriptionTranslated;

    public String getGenderDescriptionTranslated() {
        return genderDescriptionTranslated;
    }

    public void setGenderDescriptionTranslated(String genderDescriptionTranslated) {
        this.genderDescriptionTranslated = genderDescriptionTranslated;
    }

    public String getGenderDescriptionTranslatedDD() {
        return "RepEmpVacationAbsenceBase_genderDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employeeActive">
    @Column//(length = 1)
    @Translatable(translationField = "employeeActiveTranslated")
    private String employeeActive;

    public String isEmployeeActive() {
        return employeeActive;
    }

    public void setEmployeeActive(String employeeActive) {
        this.employeeActive = employeeActive;
    }

    public String isEmployeeActiveDD() {
        return "RepEmpVacationAbsenceBase_employeeActive";
    }

    @Transient
    @Translation(originalField = "employeeActive")
    private String employeeActiveTranslated;

    public String getEmployeeActiveTranslated() {
        return employeeActiveTranslated;
    }

    public void setEmployeeActiveTranslated(String employeeActiveTranslated) {
        this.employeeActiveTranslated = employeeActiveTranslated;
    }

    public String getEmployeeActiveTranslatedDD() {
        return "RepEmpVacationAbsenceBase_employeeActive";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="name">
    @Column
    @Translatable(translationField = "nameTranslated")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDD() {
        return "RepEmpVacationAbsenceBase_name";
    }

    @Transient
    @Translation(originalField = "name")
    private String nameTranslated;

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }

    public String getNameTranslatedDD() {
        return "RepEmpVacationAbsenceBase_name";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartDateDD() {
        return "RepEmpVacationAbsenceBase_startDate";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndDateDD() {
        return "RepEmpVacationAbsenceBase_endDate";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="requestDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDateDD() {
        return "RepEmpVacationAbsenceBase_requestDate";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="override">
    @Column(length=1)
    //Y-N
    private String override;

    public String getOverride() {
        return override;
    }

    public void setOverride(String override) {
        this.override = override;
    }
    public String getOverrideDD() {
        return "RepEmpVacationAbsenceBase_override";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacationPeriod">
     @Column(precision=18, scale=3)
     private BigDecimal vacationPeriod;

     public void setVacationPeriod(BigDecimal vacationPeriod) {
            this.vacationPeriod = vacationPeriod;
     }
     public BigDecimal getVacationPeriod() {
        return vacationPeriod;
     }

    public String getVacationPeriodDD() {
        return "RepEmpVacationAbsenceBase_vacationPeriod";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="exitCharge">
       @Column(length=1)
     private String exitCharge;

    public String getExitCharge() {
        return exitCharge;
    }

    public void setExitCharge(String exitCharge) {
        this.exitCharge = exitCharge;
    }
     public String getExitChargeDD() {
        return "RepEmpVacationAbsenceBase_exitCharge";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issueTicket">
     @Column(length=1)
     private String issueTicket;

    public String getIssueTicket() {
        return issueTicket;
    }

    public void setIssueTicket(String issueTicket) {
        this.issueTicket = issueTicket;
    }
     public String getIssueTicketDD() {
        return "RepEmpVacationAbsenceBase_issueTicket";
    }
 // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cancelDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date cancelDate;

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelDateDD() {
        return "RepEmpVacationAbsenceBase_cancelDate";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cancelReason">
    @Column
    @Translatable(translationField = "cancelReasonTranslated")
    private String cancelReason;

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelReasonDD() {
        return "RepEmpVacationAbsenceBase_cancelReason";
    }

    @Transient
    @Translation(originalField = "cancelReason")
    private String cancelReasonTranslated;

    public String getCancelReasonTranslated() {
        return cancelReasonTranslated;
    }

    public void setCancelReasonTranslated(String cancelReasonTranslated) {
        this.cancelReasonTranslated = cancelReasonTranslated;
    }

    public String getCancelReasonTranslatedDD() {
        return "RepEmpVacationAbsenceBase_cancelReason";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="canceledBy">
    @Column
    @Translatable(translationField = "canceledByTranslated")
    private String canceledBy;

    public String getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(String canceledBy) {
        this.canceledBy = canceledBy;
    }

    public String getCanceledByDD() {
        return "RepEmpVacationAbsenceBase_canceledBy";
    }

    @Transient
    @Translation(originalField = "canceledBy")
    private String canceledByTranslated;

    public String getCanceledByTranslated() {
        return canceledByTranslated;
    }

    public void setCanceledByTranslated(String canceledByTranslated) {
        this.canceledByTranslated = canceledByTranslated;
    }

    public String getCanceledByTranslatedDD() {
        return "RepEmpVacationAbsenceBase_canceledBy";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="approvedBy">
    @Column
    @Translatable(translationField = "approvedByTranslated")
    private String approvedBy;

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedByDD() {
        return "RepEmpVacationAbsenceBase_approvedBy";
    }

    @Transient
    @Translation(originalField = "approvedBy")
    private String approvedByTranslated;

    public String getApprovedByTranslated() {
        return approvedByTranslated;
    }

    public void setApprovedByTranslated(String approvedByTranslated) {
        this.approvedByTranslated = approvedByTranslated;
    }

    public String getApprovedByTranslatedDD() {
        return "RepEmpVacationAbsenceBase_approvedBy";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="decisionApprovedBy">
    @Column
    @Translatable(translationField = "decisionApprovedByTranslated")
    private String decisionApprovedBy;

    public String getDecisionApprovedBy() {
        return decisionApprovedBy;
    }

    public void setDecisionApprovedBy(String decisionApprovedBy) {
        this.decisionApprovedBy = decisionApprovedBy;
    }

    public String getDecisionApprovedByDD() {
        return "RepEmpVacationAbsenceBase_decisionApprovedBy";
    }

    @Transient
    @Translation(originalField = "decisionApprovedBy")
    private String decisionApprovedByTranslated;

    public String getDecisionApprovedByTranslated() {
        return decisionApprovedByTranslated;
    }

    public void setDecisionApprovedByTranslated(String decisionApprovedByTranslated) {
        this.decisionApprovedByTranslated = decisionApprovedByTranslated;
    }

    public String getDecisionApprovedByTranslatedDD() {
        return "RepEmpVacationAbsenceBase_decisionApprovedBy";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="penaltyDescription">
    @Column
    @Translatable(translationField = "penaltyDescriptionTranslated")
    private String penaltyDescription;

    public String getPenaltyDescription() {
        return penaltyDescription;
    }

    public void setPenaltyDescription(String penaltyDescription) {
        this.penaltyDescription = penaltyDescription;
    }

    public String getPenaltyDescriptionDD() {
        return "RepEmpVacationAbsenceBase_penaltyDescription";
    }

    @Transient
    @Translation(originalField = "penaltyDescription")
    private String penaltyDescriptionTranslated;

    public String getPenaltyDescriptionTranslated() {
        return penaltyDescriptionTranslated;
    }

    public void setPenaltyDescriptionTranslated(String penaltyDescriptionTranslated) {
        this.penaltyDescriptionTranslated = penaltyDescriptionTranslated;
    }

    public String getPenaltyDescriptionTranslatedDD() {
        return "RepEmpVacationAbsenceBase_penaltyDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="halfDay">
    @Column(length=1)
    private String halfDay;//(name = "half_day")

    public String getHalfDay() {
        return halfDay;
    }

    public void setHalfDay(String halfDay) {
        this.halfDay = halfDay;
    }
     public String getHalfDayDD() {
        return "RepEmpVacationAbsenceBase_halfDay";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="givenNotes">
    @Column
    private String givenNotes;

    public String getGivenNotes() {
        return givenNotes;
    }

    public void setGivenNotes(String givenNotes) {
        this.givenNotes = givenNotes;
    }

    public String getGivenNotesDD() {
        return "RepEmpVacationAbsenceBase_givenNotes";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="visaRequest">
     @Column(length=1)
     private String visaRequest;

    public String getVisaRequest() {
        return visaRequest;
    }

    public void setVisaRequest(String visaRequest) {
        this.visaRequest = visaRequest;
    }

     public String getVisaRequestDD() {
        return "RepEmpVacationAbsenceBase_visaRequest";
    }
 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decisionDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date decisionDate;

    public Date getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }
    public String getDecisionDateDD() {
        return "RepEmpVacationAbsenceBase_decisionDate";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="decisionNumber">
    @Column
    private Long decisionNumber;

    public Long getDecisionNumber() {
        return decisionNumber;
    }

    public void setDecisionNumber(Long decisionNumber) {
        this.decisionNumber = decisionNumber;
    }
     public String getDecisionNumberDD() {
        return "RepEmpVacationAbsenceBase_decisionNumber";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entryDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date entryDate;

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }
    public String getEntryDateDD() {
        return "RepEmpVacationAbsenceBase_entryDate";
    }
      // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="requestNote">
    @Column
    private String requestNote;

    public String getRequestNote() {
        return requestNote;
    }

    public void setRequestNote(String requestNote) {
        this.requestNote = requestNote;
    }

    public String getRequestNoteDD() {
        return "RepEmpVacationAbsenceBase_requestNote";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cancelled">
    @Column(length=1)
    //Y-N
    private String cancelled;//(name = "cancelled_flag")

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }
    public String getCancelledDD() {
        return "RepEmpVacationAbsenceBase_cancelled";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="needPayment">
    @Column(length=1)
    private String needPayment;//(name = "NEED_PAYMENT")

    public String getNeedPayment() {
        return needPayment;
    }

    public void setNeedPayment(String needPayment) {
        this.needPayment = needPayment;
    }
    public String getNeedPaymentDD() {
        return "RepEmpVacationAbsenceBase_needPayment";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="initialBalance">
    @Column(precision=18, scale=3)
    private BigDecimal initialBalance;

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }
     public String getInitialBalanceDD() {
        return "RepEmpVacationAbsenceBase_initialBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currentBalance">
    @Column(precision=18, scale=3)
    private BigDecimal currentBalance;

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }
     public String getCurrentBalanceDD() {
        return "RepEmpVacationAbsenceBase_currentBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="takenBalance">
    @Column(precision=18, scale=3)
    private BigDecimal takenBalance;

    public BigDecimal getTakenBalance() {
        return takenBalance;
    }

    public void setTakenBalance(BigDecimal takenBalance) {
        this.takenBalance = takenBalance;
    }
     public String getTakenBalanceDD() {
        return "RepEmpVacationAbsenceBase_takenBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="postedBalance">
    @Column(precision=18, scale=3)
    private BigDecimal postedBalance;

    public BigDecimal getPostedBalance() {
        return postedBalance;
    }

    public void setPostedBalance(BigDecimal postedBalance) {
        this.postedBalance = postedBalance;
    }
     public String getPostedBalanceDD() {
        return "RepEmpVacationAbsenceBase_postedBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="lastPostingDate">
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date lastPostingDate;

    public Date getLastPostingDate() {
        return lastPostingDate;
    }

    public void setLastPostingDate(Date lastPostingDate) {
        this.lastPostingDate = lastPostingDate;
    }
    public String getLastPostingDateDD() {
        return "RepEmpVacationAbsenceBase_lastPostingDate";
    }
      // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="transactionInitialBalance">
    @Column(precision=18, scale=3)
    private BigDecimal transactionInitialBalance;

    public BigDecimal getTransactionInitialBalance() {
        return transactionInitialBalance;
    }

    public void setTransactionInitialBalance(BigDecimal transactionInitialBalance) {
        this.transactionInitialBalance = transactionInitialBalance;
    }
     public String getTransactionInitialBalanceDD() {
        return "RepEmpVacationAbsenceBase_transactionInitialBalance";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="transactionCurrentBalance">
    @Column(precision=18, scale=3)
    private BigDecimal transactionCurrentBalance;

    public BigDecimal getTransactionCurrentBalance() {
        return transactionCurrentBalance;
    }

    public void setTransactionCurrentBalance(BigDecimal transactionCurrentBalance) {
        this.transactionCurrentBalance = transactionCurrentBalance;
    }
     public String getTransactionCurrentBalanceDD() {
        return "RepEmpVacationAbsenceBase_transactionCurrentBalance";
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="takenBefore">
    @Column(precision=18, scale=3)
    private BigDecimal takenBefore;

    public BigDecimal getTakenBefore() {
        return takenBefore;
    }

    public void setTakenBefore(BigDecimal takenBefore) {
        this.takenBefore = takenBefore;
    }

    public String getTakenBeforeDD() {
        return "RepEmpVacationAbsenceBase_takenBefore";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="secondPostingDate, secondPostedBalance, cancelComments *** datatypes r to be changed ***">
    //<editor-fold defaultstate="collapsed" desc="secondPostingDate">
    @Column
    private String secondPostingDate;

    public String getSecondPostingDate() {
        return secondPostingDate;
    }

    public void setSecondPostingDate(String secondPostingDate) {
        this.secondPostingDate = secondPostingDate;
    }

    public String getSecondPostingDateDD() {
        return "RepEmpVacationAbsenceBase_secondPostingDate";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="secondPostedBalance">
    @Column
    private String secondPostedBalance;

    public String getSecondPostedBalance() {
        return secondPostedBalance;
    }

    public void setSecondPostedBalance(String secondPostedBalance) {
        this.secondPostedBalance = secondPostedBalance;
    }

    public String getSecondPostedBalanceDD() {
        return "RepEmpVacationAbsenceBase_secondPostedBalance";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cancelComments">
    @Column
    private String cancelComments;

    public String getCancelComments() {
        return cancelComments;
    }

    public void setCancelComments(String cancelComments) {
        this.cancelComments = cancelComments;
    }

    public String getCancelCommentsDD() {
        return "RepEmpVacationAbsenceBase_cancelComments";
    }
    //</editor-fold>
    //</editor-fold>
    }
