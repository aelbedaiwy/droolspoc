package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@ParentEntity(fields="businessOutcome")
public class EDSIDPBusinessOutcomeAcc extends BaseEntity{
    @OneToMany(mappedBy="outcomeAcc")
    private List<EDSIDPBusinessOutcomeForOutLrnIntr> outLrnIntrs;
    public String getOutLrnIntrsDD(){   return "EDSIDPBusinessOutcomeAcc_outLrnIntrs";  }
    public List<EDSIDPBusinessOutcomeForOutLrnIntr> getOutLrnIntrs() {
        return outLrnIntrs;
    }

    public void setOutLrnIntrs(List<EDSIDPBusinessOutcomeForOutLrnIntr> outLrnIntrs) {
        this.outLrnIntrs = outLrnIntrs;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSIDPBusinessOutcome businessOutcome;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSIDPAccountability accountability;

    public String getBusinessOutcomeDD(){   return "EDSIDPBusinessOutcomeAcc_businessOutcome";  }
    public String getAccountabilityDD() {   return "EDSIDPBusinessOutcomeAcc_accountability";  }

    public EDSIDPBusinessOutcome getBusinessOutcome() {
        return businessOutcome;
    }

    public void setBusinessOutcome(EDSIDPBusinessOutcome BusinessOutcome) {
        this.businessOutcome = BusinessOutcome;
    }

    public EDSIDPAccountability getAccountability() {
        return accountability;
    }

    public void setAccountability(EDSIDPAccountability accountability) {
        this.accountability = accountability;
    }

}
