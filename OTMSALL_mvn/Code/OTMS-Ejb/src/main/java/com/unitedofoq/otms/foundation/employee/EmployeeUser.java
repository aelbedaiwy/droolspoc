package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
//import com.unitedofoq.fabs.core.entitybase.VersionControlSpecs;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("EMPLOYEE")
@ParentEntity(fields = {"employee"})
//@VersionControlSpecs(omoduleFieldExpression = "omodule")
public class EmployeeUser extends OUser {

    // <editor-fold defaultstate="collapsed" desc="OModule">
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
//    private OModule omodule;
//
//    @Override
//    public String getOmoduleDD() {
//        return "EmployeeUser_omodule";
//    }
//
//    @Override
//    public OModule getOmodule() {
//        return omodule;
//    }
//
//    @Override
//    public void setOmodule(OModule omodule) {
//        this.omodule = omodule;
//    }
    // </editor-fold>
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, optional = false)
    @JoinColumn(name = "Employee_DBID")
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeUser_employee";
    }
    boolean selfRoleEnabled = true;

    public boolean isSelfRoleEnabled() {
        return selfRoleEnabled;
    }

    public void setSelfRoleEnabled(boolean selfRoleEnabled) {
        this.selfRoleEnabled = selfRoleEnabled;
    }

    public String getSelfRoleEnabledDD() {
        return "EmployeeUser_selfRoleEnabled";
    }

    @Override
    public void PrePersist() {
        if (getUserPrefrence1() == null || getUserPrefrence1() == 0L) {
            if (getEmployee() != null) {
                if (getEmployee().getPositionSimpleMode() != null) {
                    if (getEmployee().getPositionSimpleMode().getUnit() != null) {
                        if (getEmployee().getPositionSimpleMode().getUnit().getCompany() != null) {
                            setUserPrefrence1(getEmployee().getPositionSimpleMode()
                                    .getUnit().getCompany().getDbid());
                        } else {
                            OLog.logError("NULL Company", new OUser(), "EmployeeUser loginname", getLoginName());
                        }
                    } else {
                        OLog.logError("NULL Unit", new OUser(), "EmployeeUser loginname", getLoginName());
                    }
                } else {
                    OLog.logError("NULL PositionSimpleMode", new OUser(), "EmployeeUser loginname", getLoginName());
                }
            } else {
                OLog.logError("NULL Employee", new OUser(), "EmployeeUser loginname", getLoginName());
            }
        }
    }
}