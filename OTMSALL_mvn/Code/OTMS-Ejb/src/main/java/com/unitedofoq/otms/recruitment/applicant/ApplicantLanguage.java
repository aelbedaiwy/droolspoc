package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields={"applicant"})
public class ApplicantLanguage extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="applicant">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch= FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "ApplicantLanguage_applicant";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="langLevel">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC langLevel;

    public String getLangLevelDD() {
        return "ApplicantLanguage_langLevel";
    }

    public UDC getLangLevel() {
        return langLevel;
    }

    public void setLangLevel(UDC langLevel) {
        this.langLevel = langLevel;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="language">
    @ManyToOne(fetch= FetchType.LAZY)
    private UDC language;

    public UDC getLanguage() {
        return language;
    }

    public void setLanguage(UDC language) {
        this.language = language;
    }

    public String getLanguageDD() {
        return "ApplicantLanguage_language";
    }
    // </editor-fold>
}