/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training.activity;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.budget.BudgetElement;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.training.instructor.Instructor;
import com.unitedofoq.otms.training.location.Location;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@MappedSuperclass
public class ActivityBase extends BusinessObjectBaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="company">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
	private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "ActivityBase_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "Activity_description";
    }
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activityType">
    //==>Internal , External , ....
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC activityType;

    public UDC getActivityType() {
        return activityType;
    }

    public void setActivityType(UDC activityType) {
        this.activityType = activityType;
    }

    public String getActivityTypeDD() {
        return "ActivityBase_activityType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activityMethod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC activityMethod;

    public UDC getActivityMethod() {
        return activityMethod;
    }

    public void setActivityMethod(UDC activityMethod) {
        this.activityMethod = activityMethod;
    }

    public String getActivityMethodDD() {
        return "ActivityBase_activityMethod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="budgetElement">
    //______________Budget Part
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private BudgetElement budgetElement;

    public BudgetElement getBudgetElement() {
        return budgetElement;
    }

    public void setBudgetElement(BudgetElement budgetElement) {
        this.budgetElement = budgetElement;
    }

    public String getBudgetElementDD() {
        return "ActivityBase_budgetElement";
    }
   // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "ActivityBase_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="instructor">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Instructor instructor;
    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public String getInstructorDD() {
        return "ActivityBase_instructor";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="price">
    @Column(precision=25, scale=13)
    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPriceDD() {
        return "ActivityBase_price";
    }
    @Transient
    private BigDecimal priceMask;

    public BigDecimal getPriceMask() {
        priceMask = price ;
        return priceMask;
    }

    public void setPriceMask(BigDecimal priceMask) {
        updateDecimalValue("price", priceMask);
    }

    public String getPriceMaskDD() {
        return "ActivityBase_priceMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getCurrencyDD() {
        return "ActivityBase_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshYears">
    @Column
    private BigDecimal refreshYears;

    public BigDecimal getRefreshYears() {
        return refreshYears;
    }

    public void setRefreshYears(BigDecimal refreshYears) {
        this.refreshYears = refreshYears;
    }
    
    public String getRefreshYearsDD() {
        return "ActivityBase_refreshYears";
    }
    @Transient
    private BigDecimal refreshYearsMask;

    public BigDecimal getRefreshYearsMask() {
        refreshYearsMask = refreshYears ;
        return refreshYearsMask;
    }

    public void setRefreshYearsMask(BigDecimal refreshYearsMask) {
        updateDecimalValue("refreshYears", refreshYearsMask);
        this.refreshYears = refreshYearsMask;
    }

    public String getRefreshYearsMaskDD() {
        return "ActivityBase_refreshYearsMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="refreshMonths">
    @Column
    private BigDecimal refreshMonths;

    public BigDecimal getRefreshMonths() {
        return refreshMonths;
    }

    public void setRefreshMonths(BigDecimal refreshMonths) {
        this.refreshMonths = refreshMonths;
    }
    
    public String getRefreshMonthsDD() {
        return "ActivityBase_refreshMonths";
    }
    @Transient
    private BigDecimal refreshMonthsMask;

    public BigDecimal getRefreshMonthsMask() {
        refreshMonthsMask = refreshMonths ;
        return refreshMonthsMask;
    }

    public void setRefreshMonthsMask(BigDecimal refreshMonthsMask) {
        updateDecimalValue("refreshMonths", refreshMonthsMask);
    }

    public String getRefreshMonthsMaskDD() {
        return "ActivityBase_refreshMonthsMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deactiveYears">
    @Column
    private BigDecimal deactiveYears;

    public BigDecimal getDeactiveYears() {
        return deactiveYears;
    }

    public void setDeactiveYears(BigDecimal deactiveYears) {
        this.deactiveYears = deactiveYears;
    }

    public String getDeactiveYearsDD() {
        return "ActivityBase_deactiveYears";
    }
    @Transient
    private BigDecimal deactiveYearsMask;

    public BigDecimal getDeactiveYearsMask() {
        deactiveYearsMask = deactiveYears ;
        return deactiveYearsMask;
    }

    public void setDeactiveYearsMask(BigDecimal deactiveYearsMask) {
        updateDecimalValue("deactiveYears", deactiveYearsMask);
    }

    public String getDeactiveYearsMaskDD() {
        return "ActivityBase_deactiveYearsMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="deactiveMonths">
    @Column
    private BigDecimal deactiveMonths;

    public BigDecimal getDeactiveMonths() {
        return deactiveMonths;
    }

    public void setDeactiveMonths(BigDecimal deactiveMonths) {
        this.deactiveMonths = deactiveMonths;
    }

    public String getDeactiveMonthsDD() {
        return "ActivityBase_deactiveMonths";
    }
    @Transient
    private BigDecimal deactiveMonthsMask;

    public BigDecimal getDeactiveMonthsMask() {
        deactiveMonthsMask = deactiveMonths ;
        return deactiveMonthsMask;
    }

    public void setDeactiveMonthsMask(BigDecimal deactiveMonthsMask) {
        updateDecimalValue("deactiveMonths", deactiveMonthsMask);
    }

    public String getDeactiveMonthsMaskDD() {
        return "ActivityBase_deactiveMonthsMask";
    }
    // </editor-fold>
}