/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author abayomy
 */
@Entity
public class EmployeeVacationDeducedDays extends BaseEntity{
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeVacationDeducedDays_employee";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="vacation">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "EmployeeVacationDeducedDays_vacation";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="calculatedPeriod">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private CalculatedPeriod calculatedPeriod;

    public CalculatedPeriod getCalculatedPeriod() {
        return calculatedPeriod;
    }

    public void setCalculatedPeriod(CalculatedPeriod calculatedPeriod) {
        this.calculatedPeriod = calculatedPeriod;
    }
    public String getCalculatedPeriodDD() {
        return "EmployeeVacationDeducedDays_calculatedPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="affectedDays">
    @Column(precision=18,scale=3)
    private BigDecimal affectedDays;//name = "vacation_affect_days"

    public BigDecimal getAffectedDays() {
        return affectedDays;
    }

    public void setAffectedDays(BigDecimal affectedDays) {
        this.affectedDays = affectedDays;
    }

    public String getAffectedDaysDD() {
        return "EmployeeVacationDeducedDays_affectedDays";
    }
    @Transient
    private BigDecimal affectedDaysMask;

    public BigDecimal getAffectedDaysMask() {
        affectedDaysMask = affectedDays ;
        return affectedDaysMask;
    }

    public void setAffectedDaysMask(BigDecimal affectedDaysMask) {
        updateDecimalValue("affectedDaysMask",affectedDaysMask);
    }

    public String getAffectedDaysMaskDD() {
        return "EmployeeVacationDeducedDays_affectedDaysMask";
    }
    // </editor-fold>
}