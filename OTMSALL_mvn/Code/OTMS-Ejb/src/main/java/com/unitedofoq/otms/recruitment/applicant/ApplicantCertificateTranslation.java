package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ApplicantCertTranslation")
public class ApplicantCertificateTranslation extends BaseEntityTranslation {

    // <editor-fold defaultstate="collapsed" desc="name">
    // ApplicantCertificateNewTranslation
    @Column
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // </editor-fold>

}
