package com.unitedofoq.otms.core.report;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.dd.DDServiceRemote;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.core.report.template.ORepTemplDSField;
import com.unitedofoq.otms.core.report.template.OTemplRep;
import com.unitedofoq.otms.core.report.template.OTemplRep1R1CFieldBase;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.core.report.RepTemplValidationServiceRemote",
        beanInterface = RepTemplValidationServiceRemote.class)
public class RepTemplValidationServiceBean implements RepTemplValidationServiceRemote {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    DDServiceRemote ddService;
    @EJB
    OFunctionServiceRemote functionService;

    @Override
    public OFunctionResult validateGrpBy(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        List<OTemplRep1R1CFieldBase> templRepField = (List<OTemplRep1R1CFieldBase>) odm.getData().get(1);
        OFunctionResult functionResult = new OFunctionResult();
//        Data1R1C
//        SingleCalc1R1C
        OTemplRep templRep = (OTemplRep) odm.getData().get(0);
        if (!templRep.getOrepTempl().getCode().endsWith("NG")
                && !templRep.getOrepTempl().getCode().startsWith("Payslip")) {
            List<String> groubByCodeList = new ArrayList<String>();
            for (OTemplRep1R1CFieldBase oTemplRep1R1CFieldBase : templRepField) {
                if (oTemplRep1R1CFieldBase.getGroupedBy() != null
                        && oTemplRep1R1CFieldBase.getGroupedBy().getCode() != null) {
                    groubByCodeList.add(oTemplRep1R1CFieldBase.getGroupedBy().getCode());
                }
            }
            if (groubByCodeList.size() > 2) {
                functionResult.addError(userMessageServiceRemote.getUserMessage(
                        "ExtraGrpByFieldsInGrpByTemplRep", loggedUser));
            }
            if (groubByCodeList.size() == 2) {
                if (groubByCodeList.get(0).equals(groubByCodeList.get(1))) {
                    functionResult.addError(userMessageServiceRemote.getUserMessage(
                            "IdenticalGrpByFieldsInGrpByTemplRep", loggedUser));
                }
            }
            if (groubByCodeList.isEmpty()) {
                functionResult.addError(userMessageServiceRemote.getUserMessage(
                        "NoGrpByFieldsInGrpByTemplRep", loggedUser));
            }
        }
        return functionResult;
    }

    @Override
    public OFunctionResult validateDSFilterFields(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        ORepTemplDSField dataSetFilterField = (ORepTemplDSField) odm.getData().get(1);
        OFunctionResult functionResult = new OFunctionResult();
        List<String> msgParams = null;
        if (dataSetFilterField.isUsedAsFilter()) {
            msgParams = new ArrayList<String>();
            msgParams.add(dataSetFilterField.getColumnName());
            if (dataSetFilterField.getOactOnEntity() == null
                    && dataSetFilterField.getUdcType() == null) {
                functionResult.addError(userMessageServiceRemote.getUserMessage(
                        "InvalidFilterValueTemplRep", msgParams, loggedUser));
            }
            if (dataSetFilterField.getOactOnEntity() != null
                    && dataSetFilterField.getUdcType() == null) {
                if (dataSetFilterField.getFieldExpression() == null
                        || dataSetFilterField.getFieldExpression().trim().equals("")) {
                    functionResult.addError(userMessageServiceRemote.getUserMessage(
                            "InvalidFilterFldExpValueTemplRep", msgParams, loggedUser));
                }
                if (dataSetFilterField.getCompanyExpression() == null
                        || dataSetFilterField.getCompanyExpression().trim().equals("")) {
                    functionResult.addError(userMessageServiceRemote.getUserMessage(
                            "InvalidFilterFldCompExpValueTemplRep", msgParams, loggedUser));
                }
            }
        }
        return functionResult;
    }

    @Override
    public OFunctionResult validatePaySlipNumberOfElements(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        OTemplRep1R1CFieldBase fieldBase = (OTemplRep1R1CFieldBase) odm.getData().get(0);

        int numIncomesSingle = 15;
        int numDeductionsSingle = 15;
        int numIncomesDual = 7;
        int numDeductionsDual = 7;
        int incomesCounter = 0;
        int deductionsCounter = 0;

        if (fieldBase.getOrepTemplDSField().getColumnName().startsWith("salI")) {
            incomesCounter++;
        } else if (fieldBase.getOrepTemplDSField().getColumnName().startsWith("salD")) {
            deductionsCounter++;
        }

        long reportDbid = fieldBase.getOreport().getDbid();
        List<String> templRepConditions = new ArrayList<String>();
        templRepConditions.add("dbid=" + reportDbid);
        OTemplRep templRep = null;
        try {
            templRep = (OTemplRep) oem.loadEntity(OTemplRep.class.getSimpleName(), templRepConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(RepTemplValidationServiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (templRep != null) {
            //load the fields of the report
            templRepConditions = new ArrayList<String>();
            templRepConditions.add("oreport.dbid=" + templRep.getDbid());
            if (templRep.getOrepTempl().getCode().startsWith("Payslip")) {
                List<OTemplRep1R1CFieldBase> fieldBases = null;
                try {
                    fieldBases = oem.loadEntityList(OTemplRep1R1CFieldBase.class.getSimpleName(), templRepConditions, null, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(RepTemplValidationServiceBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (fieldBases != null) {
                    for (int i = 0; i < fieldBases.size(); i++) {
                        if (fieldBases.get(i).getOrepTemplDSField().getColumnName().startsWith("salI")) {
                            incomesCounter++;
                        } else if (fieldBases.get(i).getOrepTemplDSField().getColumnName().startsWith("salD")) {
                            deductionsCounter++;
                        }
                    }

                    if (templRep.getOrepTempl().getCode().startsWith("PayslipRep")) {
                        if (incomesCounter > numIncomesSingle || deductionsCounter > numDeductionsSingle) {
                            ofr.addError(userMessageServiceRemote.getUserMessage("numberOfSelectedElementsExceedsSpecified", loggedUser));
                        }
                    } else if (templRep.getOrepTempl().getCode().startsWith("PayslipDualRep")) {
                        if (incomesCounter > numIncomesDual || deductionsCounter > numDeductionsDual) {
                            ofr.addError(userMessageServiceRemote.getUserMessage("numberOfSelectedElementsExceedsSpecified", loggedUser));
                        }
                    }
                }
            }
        }

        ofr.setReturnedDataMessage(odm);
        return ofr;
    }

    @Override
    public OFunctionResult validateFilterByDateFieldType(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        OTemplRep templRep = (OTemplRep) odm.getData().get(1);
        if (templRep.getFilterDateExp() != null
                && !templRep.getFilterDateExp().isEmpty()) {
            try {
                //load entity field with name == templRep.getFilterDateExp()
                Class actOnentity = Class.forName(templRep.getOrepTemplDS().getDsEntity().getEntityClassPath());
                Field filterByDateFieldExp =
                        BaseEntity.getClassField(actOnentity,
                        templRep.getFilterDateExp());
                //if type != date return error
                if (filterByDateFieldExp.getType() != Date.class) {
                    List<String> msgParams = new ArrayList<String>();
                    msgParams.add(templRep.getFilterDateExp());
                    ofr.addError(userMessageServiceRemote.getUserMessage("NonDateTypeFieldExp", msgParams, loggedUser));
                    return ofr;
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(RepTemplValidationServiceBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ofr;
    }
}
