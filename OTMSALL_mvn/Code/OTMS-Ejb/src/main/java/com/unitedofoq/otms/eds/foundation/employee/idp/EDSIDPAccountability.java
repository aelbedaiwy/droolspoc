package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@ParentEntity(fields="idp")
public class EDSIDPAccountability extends BaseEntity{
    @OneToMany(mappedBy = "accountability")
    private List<EDSIDPActionForJobGap> eDSIDPActionForJobGaps;

    public List<EDSIDPActionForJobGap> getEDSIDPActionForJobGaps() {
        return eDSIDPActionForJobGaps;
    }

    public void setEDSIDPActionForJobGaps(List<EDSIDPActionForJobGap> eDSIDPActionForJobGaps) {
        this.eDSIDPActionForJobGaps = eDSIDPActionForJobGaps;
    }

    @OneToMany(mappedBy = "accountability")
    private List<EDSConsultant> edsConsultants;

    public List<EDSConsultant> getEdsConsultants() {
        return edsConsultants;
    }

    public void setEdsConsultants(List<EDSConsultant> edsConsultants) {
        this.edsConsultants = edsConsultants;
    }

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSIDP idp;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private Employee employee;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    private UDC type;

    public String getIdpDD()        {   return "EDSIDPAccountability_idp";  }
    public String getEdsConsultantsDD()        {   return "EDSIDPAccountability_edsConsultants";  }
    public String getEmployeeDD()   {   return "EDSIDPAccountability_employee";  }
    public String getTypeDD()       {   return "EDSIDPAccountability_type";  }

    public UDC getType() {
        return type;
    }

    public void setType(UDC type) {
        this.type = type;
    }

    public EDSIDP getIdp() {
        return idp;
    }

    public void setIdp(EDSIDP idp) {
        this.idp = idp;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
