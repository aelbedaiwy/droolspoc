/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author MIR
 */
@Entity
public class ProjectNatureTranslation extends BaseEntityTranslation {
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    // </editor-fold>
}