/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.job.Job;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields={"passport"})
public class EmployeeResidencyGov extends BusinessObjectBaseEntity  {
    // <editor-fold defaultstate="collapsed" desc="passport">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeePassportGov passport;

    public EmployeePassportGov getPassport() {
        return passport;
    }

    public void setPassport(EmployeePassportGov passport) {
        this.passport = passport;
    }

    public String getPassportDD() {
        return "EmployeeResidencyGov_passport";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="number">
    @Column(nullable=false, name="residencyGovNum")
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumberDD() {
        return "EmployeeResidencyGov_number";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDate;

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewalDateDD() {
        return "EmployeeResidencyGov_renewalDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="renewalDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDateHijri;

    public Date getRenewalDateHijri() {
        return renewalDateHijri;
    }

    public void setRenewalDateHijri(Date renewalDateHijri) {
        this.renewalDateHijri = renewalDateHijri;
    }

    public String getRenewalDateHijriDD() {
        return "EmployeeResidencyGov_renewalDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cardDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardDate;

    public Date getCardDate() {
        return cardDate;
    }

    public void setCardDate(Date cardDate) {
        this.cardDate = cardDate;
    }

    public String getCardDateDD() {
        return "EmployeeResidencyGov_cardDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cardDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardDateHijri;

    public Date getCardDateHijri() {
        return cardDateHijri;
    }

    public void setCardDateHijri(Date cardDateHijri) {
        this.cardDateHijri = cardDateHijri;
    }

    public String getCardDateHijriDD() {
        return "EmployeeResidencyGov_cardDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entranceDate;

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public String getEntranceDateDD() {
        return "EmployeeResidencyGov_entranceDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entranceDateHijri">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date entranceDateHijri;

    public Date getEntranceDateHijri() {
        return entranceDateHijri;
    }

    public void setEntranceDateHijri(Date entranceDateHijri) {
        this.entranceDateHijri = entranceDateHijri;
    }

    public String getEntranceDateHijriDD() {
        return "EmployeeResidencyGov_entranceDateHijri";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="entrancePort">
    @Column
    private String entrancePort;

    public String getEntrancePort() {
        return entrancePort;
    }

    public void setEntrancePort(String entrancePort) {
        this.entrancePort = entrancePort;
    }

    public String getEntrancePortDD() {
        return "EmployeeResidencyGov_entrancePort";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="issuePlace">
    @Column
    private String issuePlace;

    public String getIssuePlace() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }

    public String getIssuePlaceDD() {
        return "EmployeeResidencyGov_issuePlace";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="job">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Job job;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getJobDD() {
        return "EmployeeResidencyGov_job";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="jobNo">
    private String jobNo;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getJobNoDD() {
        return "EmployeeResidencyGov_jobNo";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="labourOfficeNo">
    private String labourOfficeNo;

    public String getLabourOfficeNo() {
        return labourOfficeNo;
    }

    public void setLabourOfficeNo(String labourOfficeNo) {
        this.labourOfficeNo = labourOfficeNo;
    }

    public String getLabourOfficeNoDD() {
        return "EmployeeResidencyGov_labourOfficeNo";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="guarantorNo">
    @Column
    private String guarantorNo;

    public String getGuarantorNo() {
        return guarantorNo;
    }

    public void setGuarantorNo(String guarantorNo) {
        this.guarantorNo = guarantorNo;
    }

    public String getGuarantorNoDD() {
        return "EmployeeResidencyGov_guarantorNo";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorName">
    @Column
    private String guarantorName;

    public String getGuarantorName() {
        return guarantorName;
    }

    public void setGuarantorName(String guarantorName) {
        this.guarantorName = guarantorName;
    }

    public String getGuarantorNameDD() {
        return "EmployeeResidencyGov_guarantorName";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="guarantorAddress">
    @Column
    private String guarantorAddress;

    public String getGuarantorAddress() {
        return guarantorAddress;
    }

    public void setGuarantorAddress(String guarantorAddress) {
        this.guarantorAddress = guarantorAddress;
    }

    public String getGuarantorAddressDD() {
        return "EmployeeResidencyGov_guarantorAddress";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="guarantorTelephoneNo">
    @Column
    private String guarantorTelephoneNo;

    public String getGuarantorTelephoneNo() {
        return guarantorTelephoneNo;
    }

    public void setGuarantorTelephoneNo(String guarantorTelephoneNo) {
        this.guarantorTelephoneNo = guarantorTelephoneNo;
    }

    public String getGuarantorTelephoneNoDD() {
        return "EmployeeResidencyGov_guarantorTelephoneNo";
    }
    // </editor-fold >
}
