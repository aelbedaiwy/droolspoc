package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.recruitment.requisition.Requisition;
import java.util.List;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class SearchCriteria extends BaseEntity {

// <editor-fold defaultstate="collapsed" desc="country">
    @Transient
    private List<String> country;

    public List<String> getCountry() {
        return country;
    }

    public void setCountry(List<String> country) {
        this.country = country;
    }

    public String getCountryDD() {
        return "SearchCriteria_country";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="city">
    @Transient
    private List<String> city;

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public String getCityDD() {
        return "SearchCriteria_city";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="area">
    @Transient
    private List<String> area;

    public List<String> getArea() {
        return area;
    }

    public void setArea(List<String> area) {
        this.area = area;
    }

    public String getAreaDD() {
        return "SearchCriteria_area";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="level">
    @Transient
    private List<String> level;

    public List<String> getLevel() {
        return level;
    }

    public void setLevel(List<String> level) {
        this.level = level;
    }

    public String getLevelDD() {
        return "SearchCriteria_level";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="category">
    // educational category - example: IT - Engineering - Business etc.
    @Transient
    private List<String> eduCategory;

    public List<String> getEduCategory() {
        return eduCategory;
    }

    public void setEduCategory(List<String> eduCategory) {
        this.eduCategory = eduCategory;
    }

    public String getEduCategoryDD() {
        return "SearchCriteria_eduCategory";
    }
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="university">
    @Transient
    private List<String> university;

    public List<String> getUniversity() {
        return university;
    }

    public void setUniversity(List<String> university) {
        this.university = university;
    }

    public String getUniversityDD() {
        return "SearchCriteria_university";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="careerLevel">
    @Transient
    private List<String> careerLevel;

    public List<String> getCareerLevel() {
        return careerLevel;
    }

    public void setCareerLevel(List<String> careerLevel) {
        this.careerLevel = careerLevel;
    }

    public String getCareerLevelDD() {
        return "SearchCriteria_careerLevel";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="industry">
    @Transient
    private List<String> industry;

    public List<String> getIndustry() {
        return industry;
    }

    public void setIndustry(List<String> industry) {
        this.industry = industry;
    }

    public String getIndustryDD() {
        return "SearchCriteria_industry";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="role">
    //
    @Transient
    private List<String> role;

    public List<String> getRole() {
        return role;
    }

    public void setRole(List<String> role) {
        this.role = role;
    }

    public String getRoleDD() {
        return "SearchCriteria_role";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="nationality">
    @Transient
    private List<String> nationality;

    public List<String> getNationality() {
        return nationality;
    }

    public void setNationality(List<String> nationality) {
        this.nationality = nationality;
    }

    public String getNationalityDD() {
        return "SearchCriteria_nationality";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="gender">
    @Transient
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGenderDD() {
        return "SearchCriteria_gender";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="salaryFrom">
    @Transient
    private Double salaryFrom;

    public Double getSalaryFrom() {
        return salaryFrom;
    }

    public void setSalaryFrom(Double salaryFrom) {
        this.salaryFrom = salaryFrom;
    }

    public String getSalaryFromDD() {
        return "SearchCriteria_salaryFrom";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="salaryTo">
    @Transient
    private Double salaryTo;

    public Double getSalaryTo() {
        return salaryTo;
    }

    public void setSalaryTo(Double salaryTo) {
        this.salaryTo = salaryTo;
    }

    public String getSalaryToDD() {
        return "SearchCriteria_salaryTo";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="language">
    @Transient
    private List<String> language;

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public String getLanguageDD() {
        return "SearchCriteria_language";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="currency">
    // to be used later
    @Transient
    private List<String> currency;

    public List<String> getCurrency() {
        return currency;
    }

    public void setCurrency(List<String> currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "SearchCriteria_currency";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="yearsOfExperience">
    @Transient
    private Double yearsOfExperience;

    public Double getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(Double yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getYearsOfExperienceDD() {
        return "SearchCriteria_yearsOfExperience";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="requisition">
    @Transient
    private Requisition requisition;

    public Requisition getRequisition() {
        return requisition;
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }

    public String getRequisitionDD() {
        return "SearchCriteria_requisition";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="criteriaString">
    private String criteriaString;

    public String getCriteriaString() {
        return criteriaString;
    }

    public void setCriteriaString(String criteriaString) {
        this.criteriaString = criteriaString;
    }

    public String getCriteriaStringDD() {
        return "SearchCriteria_criteriaString";
    }
// </editor-fold>
}
