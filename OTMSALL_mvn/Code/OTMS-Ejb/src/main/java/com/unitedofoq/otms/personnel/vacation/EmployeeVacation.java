package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("VACATION")
@ParentEntity(fields = {"employee"})
public class EmployeeVacation extends EmployeeVacationBase {
    // <editor-fold defaultstate="collapsed" desc="vacation">

    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "EmployeeVacation_vacation";
    }
    // </editor-fold>
}
