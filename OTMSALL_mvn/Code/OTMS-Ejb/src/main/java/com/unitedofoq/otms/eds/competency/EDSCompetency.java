/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.otms.eds.foundation.company.EDSCompany;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author arezk
 */

@Entity
@ChildEntity(fields={"levels", "competencyTrainingCourses", "competencyLearningResources"
, "positiveIndicators", "nigativeIndicators"})
@Table(name="COMPETENCY")
public class EDSCompetency extends BusinessObjectBaseEntity{
    // <editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private EDSCompany company;
    public EDSCompany getEDSCompany() {
        return company;
    }
    public void setEDSCompany(EDSCompany company) {
        this.company = company;
    }
    public String getEDSCompetencyDD() {
        return "EDSCompetency_company";
    }
    // </editor-fold>
    
    public String getCustNigativeIndicatorsDD()               {    return "EDSCompetency_custNigativeIndicators";    }
    public String getCustPositiveIndicatorsDD()               {    return "EDSCompetency_custPositiveIndicators";    }

    @Column(length=1000)
    private String custNigativeIndicators;

    @Column(length=1000)
    private String custPositiveIndicators;

    public String getCustNigativeIndicators() {
        return custNigativeIndicators;
    }

    public void setCustNigativeIndicators(String custNigativeIndicators) {
        this.custNigativeIndicators = custNigativeIndicators;
    }

    public String getCustPositiveIndicators() {
        return custPositiveIndicators;
    }

    public void setCustPositiveIndicators(String custPositiveIndicators) {
        this.custPositiveIndicators = custPositiveIndicators;
    }

    @Translatable(translationField="nameTranslated")
    @Column(nullable=false)
    private String name;

    @Translation(originalField="name")
    @Transient
    private String nameTranslated;

    @Translatable(translationField="descriptionTranslated")
    @Column(nullable=false)
    private String description;
    
    @Translation(originalField="description")
    @Transient
    private String descriptionTranslated;

    @OneToMany(mappedBy="competency")
    private List<EDSCompetencyLevel> levels;
    @Transient
    private EDSCompetencyLevel level1;
    @Transient
    private EDSCompetencyLevel level2;
    @Transient
    private EDSCompetencyLevel level3;
    @Transient
    private EDSCompetencyLevel level4;
    @Transient
    private EDSCompetencyLevel level5;
    @OneToMany(mappedBy="competency")
    private List<EDSCompetencyTrainingCourse> competencyTrainingCourses;
    @OneToMany(mappedBy="competency")
    private List<EDSCompetencyLearningResource> competencyLearningResources;
    @OneToMany(mappedBy="competency")
    private List<EDSCompetencyPosIndicator> positiveIndicators;
    @OneToMany(mappedBy="competency")
    private List<EDSCompetencyNegIndicator> nigativeIndicators;
    
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name="competencyGroup_dbid")
    private EDSCompetencyGroup group;

    public EDSCompetencyGroup getGroup() {
        return group;
    }

    public void setGroup(EDSCompetencyGroup group) {
        this.group = group;
    }

    public String getGroupDD(){
        return "EDSCompetency_group";  
    }

    @Override
    protected void PostLoad() {
        super.PostLoad();
        setLevelIndex();
    }
    
    private void setLevelIndex(){
        for (EDSCompetencyLevel level : levels) {
            switch(level.getSortIndex()){
                    case 1:
                        level1 = level;
                    case 2:
                        level2 = level;
                    case 3:
                        level3 = level;
                    case 4:
                        level4 = level;
                    case 5:
                        level5 = level;
            }
        }
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }

    public String getNameTranslated() {
        return nameTranslated;
    }

    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    
    public String getNameTranslatedDD()               {    return "EDSCompetency_name";    }
    public String getDescriptionTranslatedDD()        {    return "EDSCompetency_description";    }

    public List<EDSCompetencyLearningResource> getCompetencyLearningResources() {
        return competencyLearningResources;
    }

    public void setCompetencyLearningResources(List<EDSCompetencyLearningResource> competencyLearningResources) {
        this.competencyLearningResources = competencyLearningResources;
    }

    public List<EDSCompetencyTrainingCourse> getCompetencyTrainingCourses() {
        return competencyTrainingCourses;
    }

    public void setCompetencyTrainingCourses(List<EDSCompetencyTrainingCourse> competencyTrainingCourses) {
        this.competencyTrainingCourses = competencyTrainingCourses;
    }

    public EDSCompetencyLevel getLevel1() {
        return level1;
    }
    public String getLevel1DD() {
        return "EDSCompetency_level1";
    }

    public void setLevel1(EDSCompetencyLevel level1) {
        this.level1 = level1;
    }

    public EDSCompetencyLevel getLevel2() {
        return level2;
    }
    public String getLevel2DD() {
        return "EDSCompetency_level2";
    }

    public void setLevel2(EDSCompetencyLevel level2) {
        this.level2 = level2;
    }

    public EDSCompetencyLevel getLevel3() {
        return level3;
    }
    public String getLevel3DD() {
        return "EDSCompetency_level3";
    }

    public void setLevel3(EDSCompetencyLevel level3) {
        this.level3 = level3;
    }

    public EDSCompetencyLevel getLevel4() {
        return level4;
    }
    public String getLevel4DD() {
        return "EDSCompetency_level4";
    }

    public void setLevel4(EDSCompetencyLevel level4) {
        this.level4 = level4;
    }

    public EDSCompetencyLevel getLevel5() {
        return level5;
    }
    public String getLevel5DD() {
        return "EDSCompetency_level5";
    }

    public void setLevel5(EDSCompetencyLevel level5) {
        this.level5 = level5;
    }

    public List<EDSCompetencyLevel> getLevels() {
        return levels;
    }

    public void setLevels(List<EDSCompetencyLevel> levels) {
        this.levels = levels;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EDSCompetencyNegIndicator> getNigativeIndicators() {
        return nigativeIndicators;
    }

    public void setNigativeIndicators(List<EDSCompetencyNegIndicator> nigativeIndicators) {
        this.nigativeIndicators = nigativeIndicators;
    }

    public List<EDSCompetencyPosIndicator> getPositiveIndicators() {
        return positiveIndicators;
    }

    public void setPositiveIndicators(List<EDSCompetencyPosIndicator> positiveIndicators) {
        this.positiveIndicators = positiveIndicators;
    }

    public int getLevelOrder(EDSCompetencyLevel edsLevel){
        return edsLevel.getSortIndex();
    }
}
