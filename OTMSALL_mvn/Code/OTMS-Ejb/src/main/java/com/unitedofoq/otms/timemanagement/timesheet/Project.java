package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.company.Company;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


@Entity
@ParentEntity(fields="company")
@ChildEntity(fields = {"natures","projectItem"})
public class Project extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="company">
    @JoinColumn(nullable=false)
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Company company;
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public String getCompanyDD() {
        return "Project_company";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="id">
    @Column
    private String id;

    public String getId() {
        return id;
    }
    public String getIdDD() {
        return "Project_id";
    }

    public void setId(String id) {
        this.id = id;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslatedDD() {
        return "Project_description";
    }

    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="defaultProj">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProjectNature defaultNature;

    public ProjectNature getDefaultNature() {
        return defaultNature;
    }
    public String getDefaultNatureDD() {
        return "Project_defaultNature";
    }

    public void setDefaultNature(ProjectNature defaultNature) {
        this.defaultNature = defaultNature;
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="method">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC method;

    public UDC getMethod() {
        return method;
    }
    public String getMethodDD() {
        return "Project_method";
    }

    public void setMethod(UDC method) {
        this.method = method;
    }
    // </editor-fold >
    //<editor-fold defaultstate="collapsed" desc="natures">
    @OneToMany(mappedBy="project")
    private List<ProjectNature> natures;

    public List<ProjectNature> getNatures() {
        return natures;
    }
    public String getNaturesDD() {
        return "Project_natures";
    }

    public void setNatures(List<ProjectNature> natures) {
        this.natures = natures;
    }
    //</editor-fold >
    
    //<editor-fold defaultstate="collapsed" desc="employeeProject">
    /*
    @OneToMany
    private EmployeeProject employeeProject;

    public EmployeeProject getEmployeeProject() {
        return employeeProject;
    }

    public void setEmployeeProject(EmployeeProject employeeProject) {
        this.employeeProject = employeeProject;
    }
    public String getEmployeeProjectDD(){
    return "Employee_employeeProject";
    }*/
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="projectItem">
    @OneToMany
    private List<ProjectItem> projectItem; 

    public List<ProjectItem> getProjectItem() {
        return projectItem;
    }

    public void setProjectItem(List<ProjectItem> projectItem) {
        this.projectItem = projectItem;
    }
    
    public String getProjectItemDD(){
        return "Project_projectItem";
    }
    //</editor-fold>
}
