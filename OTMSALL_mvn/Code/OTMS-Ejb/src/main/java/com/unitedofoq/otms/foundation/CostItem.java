/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.foundation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import com.unitedofoq.fabs.core.module.OModule;
import com.unitedofoq.otms.budget.BudgetElement;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.currency.Currency;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author mragab
 */
@Entity
@ParentEntity(fields="company")
public class CostItem extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="company">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyDD() {
        return "CostItem_company";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeDD() {
        return "CostItem_code";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable=false)
    @Translatable(translationField="descriptionTranslated")
    private String description;
    @Transient
    @Translation(originalField="description")
    private String descriptionTranslated;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionTranslatedDD() {
        return "CostItem_description";
    }
    public String getDescriptionTranslated() {
        return descriptionTranslated;
    }

    public void setDescriptionTranslated(String descriptionTranslated) {
        this.descriptionTranslated = descriptionTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="plannedMappedItem">
    private String plannedMappedItem;

    public String getPlannedMappedItem() {
        return plannedMappedItem;
    }

    public void setPlannedMappedItem(String plannedMappedItem) {
        this.plannedMappedItem = plannedMappedItem;
    }

    public String getPlannedMappedItemDD() {
        return "CostItem_plannedMappedItem";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="actualMappedItem">
    private String actualMappedItem;

    public String getActualMappedItem() {
        return actualMappedItem;
    }

    public void setActualMappedItem(String actualMappedItem) {
        this.actualMappedItem = actualMappedItem;
    }
    
    public String getActualMappedItemDD() {
        return "CostItem_actualMappedItem";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    
    public String getCurrencyDD() {
        return "CostItem_currency";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="budgetElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private BudgetElement budgetElement;

    public BudgetElement getBudgetElement() {
        return budgetElement;
    }

    public void setBudgetElement(BudgetElement budgetElement) {
        this.budgetElement = budgetElement;
    }

    public String getBudgetElementDD() {
        return "CostItem_budgetElement";
    }
    // </editor-fold >
    // <editor-fold defaultstate="collapsed" desc="glAccount">
    private String glAccount;

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }
    
    public String getGlAccountDD() {
        return "CostItem_glAccount";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="source">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private OModule source;

    public OModule getSource() {
        return source;
    }

    public void setSource(OModule source) {
        this.source = source;
    }
    
    public String getSourceDD() {
        return "CostItem_source";
    }
    // </editor-fold >
}