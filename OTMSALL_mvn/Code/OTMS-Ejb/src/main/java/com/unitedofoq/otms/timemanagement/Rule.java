/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.personnel.absence.Absence;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.personnel.vacation.Vacation;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author lap2
 */
@Entity
@Table(name = "TMRule")
//@ParentEntity(fields={"company"})
//@ParentEntity(fields={"workingCalendar"})
@ParentEntity(fields = {"tmCalendar"})
public class Rule extends BusinessObjectBaseEntity {

    //weekEnd
    // <editor-fold defaultstate="collapsed" desc="WeekEndCompensation">
    private boolean vacationCompensationWEOT;
    private BigDecimal maxCompWEOT;
    @Transient
    private BigDecimal maxCompWEOTMask;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation effectOnVacationCompWEOT;
    private BigDecimal maxPayrollWEOT;
    @Transient
    private BigDecimal maxPayrollWEOTMask;
    private BigDecimal maxPayrollDayWEOT;
    @Transient
    private BigDecimal maxPayrollDayWEOTMask;
    private BigDecimal maxPayrollNightWEOT;
    @Transient
    private BigDecimal maxPayrollNightWEOTMask;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "EFONPAYROLLWEOTCOMP_dbid")
    private SalaryElement effectOnPayrollWEOTComp;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "EFONPAYROLLDAYWEOTCOMP_dbid")
    private SalaryElement effectOnPayrollDayWEOTComp;
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "EFONPAYROLLNIGHTWEOTCOMP_DBID")
    private SalaryElement effectOnPayrollNightWEOTComp;

    public boolean isVacationCompensationWEOT() {
        return vacationCompensationWEOT;
    }

    public void setVacationCompensationWEOT(boolean vacationCompensationWEOT) {
        this.vacationCompensationWEOT = vacationCompensationWEOT;
    }

    public String getVacationCompensationWEOTDD() {
        return "Rule_vacationCompensationWEOT";
    }

    public BigDecimal getMaxCompWEOT() {
        return maxCompWEOT;
    }

    public void setMaxCompWEOT(BigDecimal maxCompWEOT) {
        this.maxCompWEOT = maxCompWEOT;
    }

    public String getMaxCompWEOTDD() {
        return "Rule_maxCompWEOT";
    }

    public BigDecimal getMaxCompWEOTMask() {
        maxCompWEOTMask = maxCompWEOT;
        return maxCompWEOTMask;
    }

    public void setMaxCompWEOTMask(BigDecimal maxCompWEOTMask) {
        updateDecimalValue("maxCompWEOT", maxCompWEOTMask);
    }

    public String getMaxCompWEOTMaskDD() {
        return "Rule_maxCompWEOTMask";
    }

    public Vacation getEffectOnVacationCompWEOT() {
        return effectOnVacationCompWEOT;
    }

    public void setEffectOnVacationCompWEOT(Vacation effectOnVacationCompWEOT) {
        this.effectOnVacationCompWEOT = effectOnVacationCompWEOT;
    }

    public String getEffectOnVacationCompWEOTDD() {
        return "Rule_effectOnVacationCompWEOT";
    }

    public BigDecimal getMaxPayrollWEOT() {
        return maxPayrollWEOT;
    }

    public void setMaxPayrollWEOT(BigDecimal maxPayrollWEOT) {
        this.maxPayrollWEOT = maxPayrollWEOT;
    }

    public String getMaxPayrollWEOTDD() {
        return "Rule_maxPayrollWEOT";
    }

    public BigDecimal getMaxPayrollWEOTMask() {
        maxPayrollWEOTMask = maxPayrollWEOT;
        return maxPayrollWEOTMask;
    }

    public void setMaxPayrollWEOTMask(BigDecimal maxPayrollWEOTMask) {
        updateDecimalValue("maxPayrollWEOT", maxPayrollWEOTMask);
    }

    public String getMaxPayrollWEOTMaskDD() {
        return "Rule_maxPayrollWEOTMask";
    }

    public BigDecimal getMaxPayrollDayWEOT() {
        return maxPayrollDayWEOT;
    }

    public void setMaxPayrollDayWEOT(BigDecimal maxPayrollDayWEOT) {
        this.maxPayrollDayWEOT = maxPayrollDayWEOT;
    }

    public String getMaxPayrollDayWEOTDD() {
        return "Rule_maxPayrollDayWEOT";
    }

    public BigDecimal getMaxPayrollDayWEOTMask() {
        maxPayrollDayWEOTMask = maxPayrollDayWEOT;
        return maxPayrollDayWEOTMask;
    }

    public void setMaxPayrollDayWEOTMask(BigDecimal maxPayrollDayWEOTMask) {
        updateDecimalValue("maxPayrollDayWEOT", maxPayrollDayWEOTMask);
    }

    public String getMaxPayrollDayWEOTMaskDD() {
        return "Rule_maxPayrollDayWEOTMask";
    }

    public BigDecimal getMaxPayrollNightWEOT() {
        return maxPayrollNightWEOT;
    }

    public void setMaxPayrollNightWEOT(BigDecimal maxPayrollNightWEOT) {
        this.maxPayrollNightWEOT = maxPayrollNightWEOT;
    }

    public String getMaxPayrollNightWEOTDD() {
        return "Rule_maxPayrollNightWEOT";
    }

    public BigDecimal getMaxPayrollNightWEOTMask() {
        maxPayrollNightWEOTMask = maxPayrollNightWEOT;
        return maxPayrollNightWEOTMask;
    }

    public void setMaxPayrollNightWEOTMask(BigDecimal maxPayrollNightWEOTMask) {
        updateDecimalValue("maxPayrollNightWEOT", maxPayrollNightWEOTMask);
    }

    public String getMaxPayrollNightWEOTMaskDD() {
        return "Rule_maxPayrollNightWEOTMask";
    }

    public SalaryElement getEffectOnPayrollWEOTComp() {
        return effectOnPayrollWEOTComp;
    }

    public void setEffectOnPayrollWEOTComp(SalaryElement effectOnPayrollWEOTComp) {
        this.effectOnPayrollWEOTComp = effectOnPayrollWEOTComp;
    }

    public String getEffectOnPayrollWEOTCompDD() {
        return "Rule_effectOnPayrollWEOTComp";
    }

    public SalaryElement getEffectOnPayrollDayWEOTComp() {
        return effectOnPayrollDayWEOTComp;
    }

    public void setEffectOnPayrollDayWEOTComp(SalaryElement effectOnPayrollDayWEOTComp) {
        this.effectOnPayrollDayWEOTComp = effectOnPayrollDayWEOTComp;
    }

    public String getEffectOnPayrollDayWEOTCompDD() {
        return "Rule_effectOnPayrollDayWEOTComp";
    }

    public SalaryElement getEffectOnPayrollNightWEOTComp() {
        return effectOnPayrollNightWEOTComp;
    }

    public void setEffectOnPayrollNightWEOTComp(SalaryElement effectOnPayrollNightWEOTComp) {
        this.effectOnPayrollNightWEOTComp = effectOnPayrollNightWEOTComp;
    }

    public String getEffectOnPayrollNightWEOTCompDD() {
        return "Rule_effectOnPayrollNightWEOTComp";
    }
        // </editor-fold>

    //    //<editor-fold defaultstate="collapsed" desc="company">
//    @JoinColumn(nullable=false)
//    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
//    private Company company;
//
//    public Company getCompany() {
//        return company;
//    }
//
//    public void setCompany(Company company) {
//        this.company = company;
//    }
//
//    public String getCompanyDD() {
//        return "Rule_company";
//    }
//    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="workingCalendar">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private NormalWorkingCalendar workingCalendar;

    public NormalWorkingCalendar getWorkingCalendar() {
        return workingCalendar;
    }

    public void setWorkingCalendar(NormalWorkingCalendar workingCalendar) {
        this.workingCalendar = workingCalendar;
    }

    public String getWorkingCalendarDD() {
        return "Rule_workingCalendar";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="absenceFactor">
    @Column
    private BigDecimal absenceFactor;

    public void setAbsenceFactor(BigDecimal absenceFactor) {
        this.absenceFactor = absenceFactor;
    }

    public BigDecimal getAbsenceFactor() {
        return absenceFactor;
    }

    public String getAbsenceFactorDD() {
        return "Rule_absenceFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="delayFactor">
    @Column
    private BigDecimal delayFactor;

    public void setDelayFactor(BigDecimal delayFactor) {
        this.delayFactor = delayFactor;
    }

    public BigDecimal getDelayFactor() {
        return delayFactor;
    }

    public String getDelayFactorDD() {
        return "Rule_delayFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="earlyLeaveFactor">
    @Column
    private BigDecimal earlyLeaveFactor;

    public void setEarlyLeaveFactor(BigDecimal earlyLeaveFactor) {
        this.earlyLeaveFactor = earlyLeaveFactor;
    }

    public BigDecimal getEarlyLeaveFactor() {
        return earlyLeaveFactor;
    }

    public String getEarlyLeaveFactorDD() {
        return "Rule_earlyLeaveFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nightOverTimeFactor">
    @Column
    private BigDecimal nightOverTimeFactor;

    public void setNightOverTimeFactor(BigDecimal nightOverTimeFactor) {
        this.nightOverTimeFactor = nightOverTimeFactor;
    }

    public BigDecimal getNightOverTimeFactor() {
        return nightOverTimeFactor;
    }

    public String getNightOverTimeFactorDD() {
        return "Rule_nightOverTimeFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOverTimeFactor">
    @Column
    private BigDecimal dayOverTimeFactor;

    public void setDayOverTimeFactor(BigDecimal dayOverTimeFactor) {
        this.dayOverTimeFactor = dayOverTimeFactor;
    }

    public BigDecimal getDayOverTimeFactor() {
        return dayOverTimeFactor;
    }

    public String getDayOverTimeFactorDD() {
        return "Rule_dayOverTimeFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="holidayFactor">
    @Column
    private BigDecimal holidayFactor;

    public void setHolidayFactor(BigDecimal holidayFactor) {
        this.holidayFactor = holidayFactor;
    }

    public BigDecimal getHolidayFactor() {
        return holidayFactor;
    }

    public String getHolidayFactorDD() {
        return "Rule_holidayFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="weekEndFactor">
    @Column
    private BigDecimal weekEndFactor;

    public void setWeekEndFactor(BigDecimal weekEndFactor) {
        this.weekEndFactor = weekEndFactor;
    }

    public BigDecimal getWeekEndFactor() {
        return weekEndFactor;
    }

    public String getWeekEndFactorDD() {
        return "Rule_weekEndFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="offDayFactor">
    @Column
    private BigDecimal offDayFactor;

    public void setOffDayFactor(BigDecimal offDayFactor) {
        this.offDayFactor = offDayFactor;
    }

    public BigDecimal getOffDayFactor() {
        return offDayFactor;
    }

    public String getOffDayFactorDD() {
        return "Rule_offDayFactor";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="absenceComment">
    @Column
    private String absenceComment;

    public void setAbsenceComment(String absenceComment) {
        this.absenceComment = absenceComment;
    }

    public String getAbsenceComment() {
        return absenceComment;
    }

    public String getAbsenceCommentDD() {
        return "Rule_absenceComment";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="delayComment">
    private String delayComment;

    public String getDelayComment() {
        return delayComment;
    }

    public void setDelayComment(String delayComment) {
        this.delayComment = delayComment;
    }

    public String getDelayCommentDD() {
        return "Rule_delayComment";
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="earlyLeaveComment">
    @Column
    private String earlyLeaveComment;

    public void setEarlyLeaveComment(String earlyLeaveComment) {
        this.earlyLeaveComment = earlyLeaveComment;
    }

    public String getEarlyLeaveComment() {
        return earlyLeaveComment;
    }

    public String getEarlyLeaveCommentDD() {
        return "Rule_earlyLeaveComment";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nightOverTimeComment">
    @Column
    private String nightOverTimeComment;

    public void setNightOverTimeComment(String nightOverTimeComment) {
        this.nightOverTimeComment = nightOverTimeComment;
    }

    public String getNightOverTimeComment() {
        return nightOverTimeComment;
    }

    public String getNightOverTimeCommentDD() {
        return "Rule_nightOverTimeComment";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOverTimeComment">
    @Column
    private String dayOverTimeComment;

    public void setDayOverTimeComment(String dayOverTimeComment) {
        this.dayOverTimeComment = dayOverTimeComment;
    }

    public String getDayOverTimeComment() {
        return dayOverTimeComment;
    }

    public String getDayOverTimeCommentDD() {
        return "Rule_dayOverTimeComment";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="holidayComment">
    @Column
    private String holidayComment;

    public void setHolidayComment(String holidayComment) {
        this.holidayComment = holidayComment;
    }

    public String getHolidayComment() {
        return holidayComment;
    }

    public String getHolidayCommentDD() {
        return "Rule_holidayComment";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="offDayComment">
    @Column
    private String offDayComment;

    public void setOffDayComment(String offDayComment) {
        this.offDayComment = offDayComment;
    }

    public String getOffDayComment() {
        return offDayComment;
    }

    public String getOffDayCommentDD() {
        return "Rule_offDayComment";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="weekEndComment">
    @Column
    private String weekEndComment;

    public void setWeekEndComment(String weekEndComment) {
        this.weekEndComment = weekEndComment;
    }

    public String getWeekEndComment() {
        return weekEndComment;
    }

    public String getWeekEndCommentDD() {
        return "Rule_weekEndComment";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="absenceEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement absenceEffectOnPayroll;

    public SalaryElement getAbsenceEffectOnPayroll() {
        return absenceEffectOnPayroll;
    }

    public void setAbsenceEffectOnPayroll(SalaryElement absenceEffectOnPayroll) {
        this.absenceEffectOnPayroll = absenceEffectOnPayroll;
    }

    public String getAbsenceEffectOnPayrollDD() {
        return "Rule_absenceEffectOnPayroll";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="delayEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement delayEffectOnPayroll;

    public SalaryElement getDelayEffectOnPayroll() {
        return delayEffectOnPayroll;
    }

    public void setDelayEffectOnPayroll(SalaryElement delayEffectOnPayroll) {
        this.delayEffectOnPayroll = delayEffectOnPayroll;
    }

    public String getDelayEffectOnPayrollDD() {
        return "Rule_delayEffectOnPayroll";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="earlyLeaveEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement earlyLeaveEffectOnPayroll;

    public SalaryElement getEarlyLeaveEffectOnPayroll() {
        return earlyLeaveEffectOnPayroll;
    }

    public void setEarlyLeaveEffectOnPayroll(SalaryElement earlyLeaveEffectOnPayroll) {
        this.earlyLeaveEffectOnPayroll = earlyLeaveEffectOnPayroll;
    }

    public String getEarlyLeaveEffectOnPayrollDD() {
        return "Rule_earlyLeaveEffectOnPayroll";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="dayOverTimeEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "dayovertimeeffonpay_dbid")
    private SalaryElement dayOverTimeEffectOnPayroll;

    public SalaryElement getDayOverTimeEffectOnPayroll() {
        return dayOverTimeEffectOnPayroll;
    }

    public void setDayOverTimeEffectOnPayroll(SalaryElement dayOverTimeEffectOnPayroll) {
        this.dayOverTimeEffectOnPayroll = dayOverTimeEffectOnPayroll;
    }

    public String getDayOverTimeEffectOnPayrollDD() {
        return "Rule_dayOverTimeEffectOnPayroll";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="nightOverTimeEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "nitovertimeeffonpay_dbid")
    private SalaryElement nightOverTimeEffectOnPayroll;

    public SalaryElement getNightOverTimeEffectOnPayroll() {
        return nightOverTimeEffectOnPayroll;
    }

    public void setNightOverTimeEffectOnPayroll(SalaryElement nightOverTimeEffectOnPayroll) {
        this.nightOverTimeEffectOnPayroll = nightOverTimeEffectOnPayroll;
    }

    public String getNightOverTimeEffectOnPayrollDD() {
        return "Rule_nightOverTimeEffectOnPayroll";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="offDayEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement offDayEffectOnPayroll;

    public SalaryElement getOffDayEffectOnPayroll() {
        return offDayEffectOnPayroll;
    }

    public void setOffDayEffectOnPayroll(SalaryElement offDayEffectOnPayroll) {
        this.offDayEffectOnPayroll = offDayEffectOnPayroll;
    }

    public String getOffDayEffectOnPayrollDD() {
        return "Rule_offDayEffectOnPayroll";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="holidayEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement holidayEffectOnPayroll;

    public SalaryElement getHolidayEffectOnPayroll() {
        return holidayEffectOnPayroll;
    }

    public void setHolidayEffectOnPayroll(SalaryElement holidayEffectOnPayroll) {
        this.holidayEffectOnPayroll = holidayEffectOnPayroll;
    }

    public String getHolidayEffectOnPayrollDD() {
        return "Rule_holidayEffectOnPayroll";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="weekEndEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement weekEndEffectOnPayroll;

    public SalaryElement getWeekEndEffectOnPayroll() {
        return weekEndEffectOnPayroll;
    }

    public void setWeekEndEffectOnPayroll(SalaryElement weekEndEffectOnPayroll) {
        this.weekEndEffectOnPayroll = weekEndEffectOnPayroll;
    }

    public String getWeekEndEffectOnPayrollDD() {
        return "Rule_weekEndEffectOnPayroll";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="delayEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence delayEffectOnAbsence;

    public void setDelayEffectOnAbsence(Absence delayEffectOnAbsence) {
        this.delayEffectOnAbsence = delayEffectOnAbsence;
    }

    public Absence getDelayEffectOnAbsence() {
        return delayEffectOnAbsence;
    }

    public String getDelayEffectOnAbsenceDD() {
        return "Rule_delayEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="delayEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation delayEffectOnVacation;

    public void setDelayEffectOnVacation(Vacation delayEffectOnVacation) {
        this.delayEffectOnVacation = delayEffectOnVacation;
    }

    public Vacation getDelayEffectOnVacation() {
        return delayEffectOnVacation;
    }

    public String getDelayEffectOnVacationDD() {
        return "Rule_delayEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="delayEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty delayEffectOnPenalty;

    public void setDelayEffectOnPenalty(Penalty delayEffectOnPenalty) {
        this.delayEffectOnPenalty = delayEffectOnPenalty;
    }

    public Penalty getDelayEffectOnPenalty() {
        return delayEffectOnPenalty;
    }

    public String getDelayEffectOnPenaltyDD() {
        return "Rule_delayEffectOnPenalty";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="lessWorkComment">
    private String lessWorkComment;

    public String getLessWorkComment() {
        return lessWorkComment;
    }

    public void setLessWorkComment(String lessWorkComment) {
        this.lessWorkComment = lessWorkComment;
    }

    public String getLessWorkCommentDD() {
        return "Rule_lessWorkComment";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lessWorkFactor">
    @Column
    private BigDecimal lessWorkFactor;

    public void setLessWorkFactor(BigDecimal lessWorkFactor) {
        this.lessWorkFactor = lessWorkFactor;
    }

    public BigDecimal getLessWorkFactor() {
        return lessWorkFactor;
    }

    public String getLessWorkFactorDD() {
        return "Rule_lessWorkFactor";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="lessWorkEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement lessWorkEffectOnPayroll;

    public SalaryElement getLessWorkEffectOnPayroll() {
        return lessWorkEffectOnPayroll;
    }

    public void setLessWorkEffectOnPayroll(SalaryElement lessWorkEffectOnPayroll) {
        this.lessWorkEffectOnPayroll = lessWorkEffectOnPayroll;
    }

    public String getLessWorkEffectOnPayrollDD() {
        return "Rule_lessWorkEffectOnPayroll";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lessWorkEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence lessWorkEffectOnAbsence;

    public void setLessWorkEffectOnAbsence(Absence lessWorkEffectOnAbsence) {
        this.lessWorkEffectOnAbsence = lessWorkEffectOnAbsence;
    }

    public Absence getLessWorkEffectOnAbsence() {
        return lessWorkEffectOnAbsence;
    }

    public String getLessWorkEffectOnAbsenceDD() {
        return "Rule_lessWorkEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lessWorkEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation lessWorkEffectOnVacation;

    public void setLessWorkEffectOnVacation(Vacation lessWorkEffectOnVacation) {
        this.lessWorkEffectOnVacation = lessWorkEffectOnVacation;
    }

    public Vacation getLessWorkEffectOnVacation() {
        return lessWorkEffectOnVacation;
    }

    public String getLessWorkEffectOnVacationDD() {
        return "Rule_lessWorkEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="lessWorkEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty lessWorkEffectOnPenalty;

    public void setLessWorkEffectOnPenalty(Penalty lessWorkEffectOnPenalty) {
        this.lessWorkEffectOnPenalty = lessWorkEffectOnPenalty;
    }

    public Penalty getLessWorkEffectOnPenalty() {
        return lessWorkEffectOnPenalty;
    }

    public String getLessWorkEffectOnPenaltyDD() {
        return "Rule_lessWorkEffectOnPenalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="earlyLeaveEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence earlyLeaveEffectOnAbsence;

    public void setEarlyLeaveEffectOnAbsence(Absence earlyLeaveEffectOnAbsence) {
        this.earlyLeaveEffectOnAbsence = earlyLeaveEffectOnAbsence;
    }

    public Absence getEarlyLeaveEffectOnAbsence() {
        return earlyLeaveEffectOnAbsence;
    }

    public String getEarlyLeaveEffectOnAbsenceDD() {
        return "Rule_earlyLeaveEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="earlyLeaveEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "earlyleaveeffonvac_dbid")
    private Vacation earlyLeaveEffectOnVacation;

    public void setEarlyLeaveEffectOnVacation(Vacation earlyLeaveEffectOnVacation) {
        this.earlyLeaveEffectOnVacation = earlyLeaveEffectOnVacation;
    }

    public Vacation getEarlyLeaveEffectOnVacation() {
        return earlyLeaveEffectOnVacation;
    }

    public String getEarlyLeaveEffectOnVacationDD() {
        return "Rule_earlyLeaveEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="earlyLeaveEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty earlyLeaveEffectOnPenalty;

    public void setEarlyLeaveEffectOnPenalty(Penalty earlyLeaveEffectOnPenalty) {
        this.earlyLeaveEffectOnPenalty = earlyLeaveEffectOnPenalty;
    }

    public Penalty getEarlyLeaveEffectOnPenalty() {
        return earlyLeaveEffectOnPenalty;
    }

    public String getEarlyLeaveEffectOnPenaltyDD() {
        return "Rule_earlyLeaveEffectOnPenalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOvertimeEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "dayovertimeeffonvac_dbid")
    private Vacation dayOvertimeEffectOnVacation;

    public void setDayOvertimeEffectOnVacation(Vacation dayOvertimeEffectOnVacation) {
        this.dayOvertimeEffectOnVacation = dayOvertimeEffectOnVacation;
    }

    public Vacation getDayOvertimeEffectOnVacation() {
        return dayOvertimeEffectOnVacation;
    }

    public String getDayOvertimeEffectOnVacationDD() {
        return "Rule_dayOvertimeEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOvertimeEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "dayovertimeeffonabs_dbid")
    private Absence dayOvertimeEffectOnAbsence;

    public void setDayOvertimeEffectOnAbsence(Absence dayOvertimeEffectOnAbsence) {
        this.dayOvertimeEffectOnAbsence = dayOvertimeEffectOnAbsence;
    }

    public Absence getDayOvertimeEffectOnAbsence() {
        return dayOvertimeEffectOnAbsence;
    }

    public String getDayOvertimeEffectOnAbsenceDD() {
        return "Rule_dayOvertimeEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="dayOvertimeEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "dayovertimeeffonpen_dbid")
    private Penalty dayOvertimeEffectOnPenalty;

    public void setDayOvertimeEffectOnPenalty(Penalty dayOvertimeEffectOnPenalty) {
        this.dayOvertimeEffectOnPenalty = dayOvertimeEffectOnPenalty;
    }

    public Penalty getDayOvertimeEffectOnPenalty() {
        return dayOvertimeEffectOnPenalty;
    }

    public String getDayOvertimeEffectOnPenaltyDD() {
        return "Rule_dayOvertimeEffectOnPenalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nightOvertimeEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "nitovertimeeffonvac_dbid")
    private Vacation nightOvertimeEffectOnVacation;

    public void setNightOvertimeEffectOnVacation(Vacation nightOvertimeEffectOnVacation) {
        this.nightOvertimeEffectOnVacation = nightOvertimeEffectOnVacation;
    }

    public Vacation getNightOvertimeEffectOnVacation() {
        return nightOvertimeEffectOnVacation;
    }

    public String getNightOvertimeEffectOnVacationDD() {
        return "Rule_nightOvertimeEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nightOvertimeEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "nitovertimeeffonabs_dbid")
    private Absence nightOvertimeEffectOnAbsence;

    public void setNightOvertimeEffectOnAbsence(Absence nightOvertimeEffectOnAbsence) {
        this.nightOvertimeEffectOnAbsence = nightOvertimeEffectOnAbsence;
    }

    public Absence getNightOvertimeEffectOnAbsence() {
        return nightOvertimeEffectOnAbsence;
    }

    public String getNightOvertimeEffectOnAbsenceDD() {
        return "Rule_nightOvertimeEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="nightOvertimeEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(name = "nitovertimeeffonpen_dbid")
    private Penalty nightOvertimeEffectOnPenalty;

    public void setNightOvertimeEffectOnPenalty(Penalty nightOvertimeEffectOnPenalty) {
        this.nightOvertimeEffectOnPenalty = nightOvertimeEffectOnPenalty;
    }

    public Penalty getNightOvertimeEffectOnPenalty() {
        return nightOvertimeEffectOnPenalty;
    }

    public String getNightOvertimeEffectOnPenaltyDD() {
        return "Rule_nightOvertimeEffectOnPenalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="offDayEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation offDayEffectOnVacation;

    public void setOffDayEffectOnVacation(Vacation offDayEffectOnVacation) {
        this.offDayEffectOnVacation = offDayEffectOnVacation;
    }

    public Vacation getOffDayEffectOnVacation() {
        return offDayEffectOnVacation;
    }

    public String getOffDayEffectOnVacationDD() {
        return "Rule_offDayEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="offDayEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence offDayEffectOnAbsence;

    public void setOffDayEffectOnAbsence(Absence offDayEffectOnAbsence) {
        this.offDayEffectOnAbsence = offDayEffectOnAbsence;
    }

    public Absence getOffDayEffectOnAbsence() {
        return offDayEffectOnAbsence;
    }

    public String getOffDayEffectOnAbsenceDD() {
        return "Rule_offDayEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="offDayEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty offDayEffectOnPenalty;

    public void setOffDayEffectOnPenalty(Penalty offDayEffectOnPenalty) {
        this.offDayEffectOnPenalty = offDayEffectOnPenalty;
    }

    public Penalty getOffDayEffectOnPenalty() {
        return offDayEffectOnPenalty;
    }

    public String getOffDayEffectOnPenaltyDD() {
        return "Rule_offDayEffectOnPenalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="holidayEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence holidayEffectOnAbsence;

    public void setHolidayEffectOnAbsence(Absence holidayEffectOnAbsence) {
        this.holidayEffectOnAbsence = holidayEffectOnAbsence;
    }

    public Absence getHolidayEffectOnAbsence() {
        return holidayEffectOnAbsence;
    }

    public String getHolidayEffectOnAbsenceDD() {
        return "Rule_holidayEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="holidayEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation holidayEffectOnVacation;

    public void setHolidayEffectOnVacation(Vacation holidayEffectOnVacation) {
        this.holidayEffectOnVacation = holidayEffectOnVacation;
    }

    public Vacation getHolidayEffectOnVacation() {
        return holidayEffectOnVacation;
    }

    public String getHolidayEffectOnVacationDD() {
        return "Rule_holidayEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="holidayEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty holidayEffectOnPenalty;

    public void setHolidayEffectOnPenalty(Penalty holidayEffectOnPenalty) {
        this.holidayEffectOnPenalty = holidayEffectOnPenalty;
    }

    public Penalty getHolidayEffectOnPenalty() {
        return holidayEffectOnPenalty;
    }

    public String getHolidayEffectOnPenaltyDD() {
        return "Rule_holidayEffectOnPenalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="absenceEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence absenceEffectOnAbsence;

    public void setAbsenceEffectOnAbsence(Absence absenceEffectOnAbsence) {
        this.absenceEffectOnAbsence = absenceEffectOnAbsence;
    }

    public Absence getAbsenceEffectOnAbsence() {
        return absenceEffectOnAbsence;
    }

    public String getAbsenceEffectOnAbsenceDD() {
        return "Rule_absenceEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="absenceEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation absenceEffectOnVacation;

    public void setAbsenceEffectOnVacation(Vacation absenceEffectOnVacation) {
        this.absenceEffectOnVacation = absenceEffectOnVacation;
    }

    public Vacation getAbsenceEffectOnVacation() {
        return absenceEffectOnVacation;
    }

    public String getAbsenceEffectOnVacationDD() {
        return "Rule_absenceEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="absenceEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty absenceEffectOnPenalty;

    public void setAbsenceEffectOnPenalty(Penalty absenceEffectOnPenalty) {
        this.absenceEffectOnPenalty = absenceEffectOnPenalty;
    }

    public Penalty getAbsenceEffectOnPenalty() {
        return absenceEffectOnPenalty;
    }

    public String getAbsenceEffectOnPenaltyDD() {
        return "Rule_absenceEffectOnPenalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="weekEndEffectOnAbsence">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence weekEndEffectOnAbsence;

    public void setWeekEndEffectOnAbsence(Absence weekEndEffectOnAbsence) {
        this.weekEndEffectOnAbsence = weekEndEffectOnAbsence;
    }

    public Absence getWeekEndEffectOnAbsence() {
        return weekEndEffectOnAbsence;
    }

    public String getWeekEndEffectOnAbsenceDD() {
        return "Rule_weekEndEffectOnAbsence";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="weekEndEffectOnVacation">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation weekEndEffectOnVacation;

    public void setWeekEndEffectOnVacation(Vacation weekEndEffectOnVacation) {
        this.weekEndEffectOnVacation = weekEndEffectOnVacation;
    }

    public Vacation getWeekEndEffectOnVacation() {
        return weekEndEffectOnVacation;
    }

    public String getWeekEndEffectOnVacationDD() {
        return "Rule_weekEndEffectOnVacation";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="weekEndEffectOnPenalty">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Penalty weekEndEffectOnPenalty;

    public void setWeekEndEffectOnPenalty(Penalty weekEndEffectOnPenalty) {
        this.weekEndEffectOnPenalty = weekEndEffectOnPenalty;
    }

    public Penalty getWeekEndEffectOnPenalty() {
        return weekEndEffectOnPenalty;
    }

    public String getWeekEndEffectOnPenaltyDD() {
        return "Rule_weekEndEffectOnPenalty";
    }
    // </editor-fold>

    //Loubna - 08/09/2013 - for rotation & shift cases (monthly consolidation), changing the Rule to be on TMCalendar
    //<editor-fold defaultstate="collapsed" desc="tmCalendar">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private TMCalendar tmCalendar;

    public TMCalendar getTmCalendar() {
        return tmCalendar;
    }

    public void setTmCalendar(TMCalendar tmCalendar) {
        this.tmCalendar = tmCalendar;
    }

    public String getTmCalendarDD() {
        return "Rule_tmCalendar";
    }
    //</editor-fold>

    //For Cement
    // <editor-fold defaultstate="collapsed" desc="normalOTFactor">
    @Column
    private BigDecimal normalOTFactor;

    public BigDecimal getNormalOTFactor() {
        return normalOTFactor;
    }

    public String getNormalOTFactorDD() {
        return "Rule_normalOTFactor";
    }

    public void setNormalOTFactor(BigDecimal normalOTFactor) {
        this.normalOTFactor = normalOTFactor;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="normalOTOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement normalOTOnPayroll;

    public SalaryElement getNormalOTOnPayroll() {
        return normalOTOnPayroll;
    }

    public String getNormalOTOnPayrollDD() {
        return "Rule_normalOTOnPayroll";
    }

    public void setNormalOTOnPayroll(SalaryElement normalOTOnPayroll) {
        this.normalOTOnPayroll = normalOTOnPayroll;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dayLessWorkAbsenceEffect">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Absence dayLessWorkAbsenceEffect;

    public void setDayLessWorkAbsenceEffect(Absence dayLessWorkAbsenceEffect) {
        this.dayLessWorkAbsenceEffect = dayLessWorkAbsenceEffect;
    }

    public Absence getDayLessWorkAbsenceEffect() {
        return dayLessWorkAbsenceEffect;
    }

    public String getDayLessWorkAbsenceEffectDD() {
        return "Rule_dayLessWorkAbsenceEffect";
    }
    // </editor-fold>
    @Column
    private BigDecimal dayLessWorkFactor;

    public void setDayLessWorkFactor(BigDecimal dayLessWorkFactor) {
        this.dayLessWorkFactor = dayLessWorkFactor;
    }

    public BigDecimal getDayLessWorkFactor() {
        return dayLessWorkFactor;
    }

    public String getDayLessWorkFactorDD() {
        return "Rule_dayLessWorkFactor";
    }

    private boolean vacationCompensation;

    public boolean isVacationCompensation() {
        return vacationCompensation;
    }

    public void setVacationCompensation(boolean vacationCompensation) {
        this.vacationCompensation = vacationCompensation;
    }

    public String getVacationCompensationDD() {
        return "NormalWorkingCalendar_vacationCompensation";
    }

    // <editor-fold defaultstate="collapsed" desc="vacation">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    public String getVacationDD() {
        return "EmployeeVacationRequest_vacation";
    }
    // </editor-fold>

    private double otTolerance;

    public double getOtTolerance() {
        return otTolerance;
    }

    public String getOtToleranceDD() {
        return "NormalWorkingCalendar_otTolerance";
    }

    public void setOtTolerance(double otTolerance) {
        this.otTolerance = otTolerance;
    }

    private boolean holidayOTCompendsation;

    public boolean isHolidayOTCompendsation() {
        return holidayOTCompendsation;
    }

    public void setHolidayOTCompendsation(boolean holidayOTCompendsation) {
        this.holidayOTCompendsation = holidayOTCompendsation;
    }

    public String getHolidayOTCompendsationDD() {
        return "Rule_holidayOTCompendsation";
    }

    @JoinColumn(name = "holidayVacCompensation_dbid")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation holidayVacationCompensation;

    public Vacation getHolidayVacationCompensation() {
        return holidayVacationCompensation;
    }

    public void setHolidayVacationCompensation(Vacation holidayVacationCompensation) {
        this.holidayVacationCompensation = holidayVacationCompensation;
    }

    public String getHolidayVacationCompensationDD() {
        return "Rule_holidayVacationCompensation";
    }

    private double holidayCompendsationValue;

    public double getHolidayCompendsationValue() {
        return holidayCompendsationValue;
    }

    public void setHolidayCompendsationValue(double holidayCompendsationValue) {
        this.holidayCompendsationValue = holidayCompendsationValue;
    }

    public String getHolidayCompendsationValueDD() {
        return "Rule_holidayCompendsationValue";
    }
    //<editor-fold defaultstate="collapsed" desc="holidayOTEffectOnPayroll">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private SalaryElement holidayOTEffectOnPayroll;

    public SalaryElement getHolidayOTEffectOnPayroll() {
        return holidayOTEffectOnPayroll;
    }

    public void setHolidayOTEffectOnPayroll(SalaryElement holidayOTEffectOnPayroll) {
        this.holidayOTEffectOnPayroll = holidayOTEffectOnPayroll;
    }

    public String getHolidayOTEffectOnPayrollDD() {
        return "Rule_holidayOTEffectOnPayroll";
    }
    //</editor-fold>
    @Column
    private BigDecimal holidayOTFactor;

    public void setHolidayOTFactor(BigDecimal holidayOTFactor) {
        this.holidayOTFactor = holidayOTFactor;
    }

    public BigDecimal getHolidayOTFactor() {
        return holidayOTFactor;
    }

    public String getHolidayOTFactorDD() {
        return "Rule_holidayOTFactor";
    }
}
