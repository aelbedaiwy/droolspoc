package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import com.unitedofoq.otms.appraisal.scaletype.ScaleType;
import com.unitedofoq.otms.appraisal.scaletype.ScaleTypeRule;
import com.unitedofoq.otms.appraisal.template.AppraisalTemplateSequence;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.training.activity.Provider;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class EmployeeKPA extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="targetAsValue">
    @Column
    private BigDecimal targetAsValue;

    public BigDecimal getTargetAsValue() {
        return targetAsValue;
    }

    public void setTargetAsValue(BigDecimal targetAsValue) {
        this.targetAsValue = targetAsValue;
    }

    public String getTargetAsValueDD() {
        return "EmployeeKPA_targetAsValue";
    }

    @Transient
    private BigDecimal targetAsValueMask;

    public BigDecimal getTargetAsValueMask() {
        targetAsValueMask = targetAsValue;
        return targetAsValueMask;
    }

    public void setTargetAsValueMask(BigDecimal targetAsValueMask) {
        updateDecimalValue("targetAsValue", targetAsValueMask);
    }

    public String getTargetAsValueMaskDD() {
        return "EmployeeKPA_targetAsValueMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="targetAsDescription">
    @Column
    private String targetAsDescription;

    public String getTargetAsDescription() {
        return targetAsDescription;
    }

    public void setTargetAsDescription(String targetAsDescription) {
        this.targetAsDescription = targetAsDescription;
    }

    public String getTargetAsDescriptionDD() {
        return "EmployeeKPA_targetAsDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="targetWeight">
    @Column(precision = 25, scale = 13)
    private BigDecimal targetWeight;

    public BigDecimal getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(BigDecimal targetWeight) {
        this.targetWeight = targetWeight;
    }

    public String getTargetWeightDD() {
        return "EmployeeKPA_targetWeight";
    }

    @Transient
    private BigDecimal targetWeightMask;

    public BigDecimal getTargetWeightMask() {
        targetWeightMask = targetWeight;
        return targetWeightMask;
    }

    public void setTargetWeightMask(BigDecimal targetWeightMask) {
        updateDecimalValue("targetWeight", targetWeightMask);
    }

    public String getTargetWeightMaskDD() {
        return "EmployeeKPA_targetWeightMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="targetAsGrading">
    @Column
    private String targetAsGrading;

    public String getTargetAsGrading() {
        return targetAsGrading;
    }

    public void setTargetAsGrading(String targetAsGrading) {
        this.targetAsGrading = targetAsGrading;
    }

    public String getTargetAsGradingDD() {
        return "EmployeeKPA_targetAsGrading";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="comment">
    @Column(name = "comments")
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentDD() {
        return "EmployeeKPA_comment";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="scaleType">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleType scaleType;

    public ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    public String getScaleTypeDD() {
        return "EmployeeKPA_scaleType";
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="evaluationComment">
    @Column
    private String evaluationComment;

    public String getEvaluationComment() {
        return evaluationComment;
    }

    public void setEvaluationComment(String evaluationComment) {
        this.evaluationComment = evaluationComment;
    }

    public String getEvaluationCommentDD() {
        return "EmployeeKPA_evaluationComment";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="evaluationAsDescription">
    @Column
    private String evaluationAsDescription;

    public String getEvaluationAsDescription() {
        return evaluationAsDescription;
    }

    public void setEvaluationAsDescription(String evaluationAsDescription) {
        this.evaluationAsDescription = evaluationAsDescription;
    }

    public String getEvaluationAsDescriptionDD() {
        return "EmployeeKPA_evaluationAsDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="evaluationAsValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal evaluationAsValue;

    public BigDecimal getEvaluationAsValue() {
        return evaluationAsValue;
    }

    public void setEvaluationAsValue(BigDecimal evaluationAsValue) {
        this.evaluationAsValue = evaluationAsValue;
    }

    public String getEvaluationAsValueDD() {
        return "EmployeeKPA_evaluationAsValue";
    }

    @Transient
    private BigDecimal evaluationAsValueMask;

    public BigDecimal getEvaluationAsValueMask() {
        evaluationAsValueMask = evaluationAsValue;
        return evaluationAsValueMask;
    }

    public void setEvaluationAsValueMask(BigDecimal evaluationAsValueMask) {
        updateDecimalValue("evaluationAsValue", evaluationAsValueMask);
    }

    public String getEvaluationAsValueMaskDD() {
        return "EmployeeKPA_evaluationAsValueMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="weightRating">
    @Column(precision = 25, scale = 13)
    private BigDecimal weightRating;

    public BigDecimal getWeightRating() {
        return weightRating;
    }

    public void setWeightRating(BigDecimal weightRating) {
        this.weightRating = weightRating;
    }

    public String getWeightRatingDD() {
        return "EmployeeKPA_weightRating";
    }

    @Transient
    private BigDecimal weightRatingMask;

    public BigDecimal getWeightRatingMask() {
        weightRatingMask = weightRating;
        return weightRatingMask;
    }

    public void setWeightRatingMask(BigDecimal weightRatingMask) {
        updateDecimalValue("weightRating", weightRatingMask);
    }

    public String getWeightRatingMaskDD() {
        return "EmployeeKPA_weightRatingMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="evaluationAsRating">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule evaluationAsRating;

    public ScaleTypeRule getEvaluationAsRating() {
        return evaluationAsRating;
    }

    public void setEvaluationAsRating(ScaleTypeRule evaluationAsRating) {
        this.evaluationAsRating = evaluationAsRating;
    }

    public String getEvaluationAsRatingDD() {
        return "EmployeeKPA_evaluationAsRating";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="selfEvaluationComment">
    @Column
    private String selfEvaluationComment;

    public String getSelfEvaluationComment() {
        return selfEvaluationComment;
    }

    public String getSelfEvaluationCommentDD() {
        return "EmployeeKPA_selfEvaluationComment";
    }

    public void setSelfEvaluationComment(String selfEvaluationComment) {
        this.selfEvaluationComment = selfEvaluationComment;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="selfEvaluationAsDescription">
    @Column
    private String selfEvaluationAsDescription;

    public String getSelfEvaluationAsDescription() {
        return selfEvaluationAsDescription;
    }

    public void setSelfEvaluationAsDescription(String selfEvaluationAsDescription) {
        this.selfEvaluationAsDescription = selfEvaluationAsDescription;
    }

    public String getSelfEvaluationAsDescriptionDD() {
        return "EmployeeKPA_selfEvaluationAsDescription";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="selfEvaluationAsValue">
    @Column(precision = 25, scale = 13)
    private BigDecimal selfEvaluationAsValue;

    public BigDecimal getSelfEvaluationAsValue() {
        return selfEvaluationAsValue;
    }

    public void setSelfEvaluationAsValue(BigDecimal selfEvaluationAsValue) {
        this.selfEvaluationAsValue = selfEvaluationAsValue;
    }

    public String getSelfEvaluationAsValueDD() {
        return "EmployeeKPA_selfEvaluationAsValue";
    }

    @Transient
    private BigDecimal selfEvaluationAsValueMask;

    public BigDecimal getSelfEvaluationAsValueMask() {
        selfEvaluationAsValueMask = selfEvaluationAsValue;
        return selfEvaluationAsValueMask;
    }

    public void setSelfEvaluationAsValueMask(BigDecimal selfEvaluationAsValueMask) {
        updateDecimalValue("selfEvaluationAsValue", selfEvaluationAsValueMask);
    }

    public String getSelfEvaluationAsValueMaskDD() {
        return "EmployeeKPA_selfEvaluationAsValueMask";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="selfEvaluationAsRating">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScaleTypeRule selfEvaluationAsRating;

    public ScaleTypeRule getSelfEvaluationAsRating() {
        return selfEvaluationAsRating;
    }

    public void setSelfEvaluationAsRating(ScaleTypeRule selfEvaluationAsRating) {
        this.selfEvaluationAsRating = selfEvaluationAsRating;
    }

    public String getSelfEvaluationAsRatingDD() {
        return "EmployeeKPA_selfEvaluationAsRating";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="appraiser">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee appraiser;

    public Employee getAppraiser() {
        return appraiser;
    }

    public void setAppraiser(Employee appraiser) {
        this.appraiser = appraiser;
    }

    public String getAppraiserDD() {
        return "EmployeeKPA_appraiser";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="sequence">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private AppraisalTemplateSequence sequence;

    public AppraisalTemplateSequence getSequence() {
        return sequence;
    }

    public void setSequence(AppraisalTemplateSequence sequence) {
        this.sequence = sequence;
    }

    public String getSequenceDD() {
        return "EmployeeKPA_sequence";
    }
    //</editor-fold>

    @Transient
    boolean m2mCheck;

    public boolean isM2mCheck() {
        return m2mCheck;
    }

    public void setM2mCheck(boolean m2mCheck) {
        this.m2mCheck = m2mCheck;
    }

    public String getM2mCheckDD() {
        return "EmployeeKPA_m2mCheck";
    }

    @Override
    public void PostLoad() {
        super.PostLoad();
        setM2mCheck(true);
    }

    public Employee getEmployee() {
        return null;
    }

    // for training:
    // =================
    //<editor-fold defaultstate="collapsed" desc="course">
    @ManyToOne
    private ProviderCourseInformation course;

    public ProviderCourseInformation getCourse() {
        return course;
    }

    public void setCourse(ProviderCourseInformation course) {
        this.course = course;
    }

    public String getCourseDD() {
        return "EmployeeKPA_course";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "EmployeeKPA_provider";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="managerConfirmation">
    private boolean managerConfirmation;

    public boolean isManagerConfirmation() {
        return managerConfirmation;
    }

    public String getManagerConfirmationDD() {
        return "EmployeeKPA_managerConfirmation";
    }

    public void setManagerConfirmation(boolean managerConfirmation) {
        this.managerConfirmation = managerConfirmation;
    }
    //</editor-fold>
}
