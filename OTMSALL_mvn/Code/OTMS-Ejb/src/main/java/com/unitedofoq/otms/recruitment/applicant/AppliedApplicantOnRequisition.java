package com.unitedofoq.otms.recruitment.applicant;

import com.unitedofoq.fabs.core.entitybase.FABSEntitySpecs;
import com.unitedofoq.otms.recruitment.requisition.Requisition;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "REQUISITION")
@FABSEntitySpecs(customCascadeFields = {"applicant"})
public class AppliedApplicantOnRequisition extends AppliedApplicant {

    // <editor-fold defaultstate="collapsed" desc="requisition">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Requisition requisition;

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }

    public Requisition getRequisition() {
        return requisition;
    }

    public String getRequisitionDD() {
        return "AppliedApplicantOnRequisition_requisition";
    }
    // </editor-fold>
}
