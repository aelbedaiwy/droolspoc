/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.absence;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author abayomy
 */
@Entity
@DiscriminatorValue("ABSENCEINC")
@ParentEntity(fields={"absence"})
//@FABSEntitySpecs(hideInheritedFields={"salaryElement"})
public class AbsenceAffectingIncome extends AbsenceAffectingSalaryElement {
    
    // <editor-fold defaultstate="collapsed" desc="income">
    @JoinColumn//(name="SALARYELEMENT_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Income income;

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }
public String getIncomeDD() {
        return "AbsenceAffectingIncome_income";
    }
// </editor-fold>
}
