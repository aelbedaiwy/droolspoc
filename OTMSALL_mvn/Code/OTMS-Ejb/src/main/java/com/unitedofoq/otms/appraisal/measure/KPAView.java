package com.unitedofoq.otms.appraisal.measure;

import com.unitedofoq.fabs.core.entitybase.BusinessObjectBaseEntity;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class KPAView extends BusinessObjectBaseEntity {

    //<editor-fold defaultstate="collapsed" desc="description">
    @Column
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "KPAView_description";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="companyDbid">
    @Column
    private int companyDbid;

    public int getCompanyDbid() {
        return companyDbid;
    }

    public void setCompanyDbid(int companyDbid) {
        this.companyDbid = companyDbid;
    }

    public String getCompanyDbidDD() {
        return "KPAView_companyDbid";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="companyCode">
    @Column
    private String companyCode;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyCodeDD() {
        return "KPAView_companyCode";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="parentKPAView">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private KPAView parentKPAView;

    public KPAView getParentKPAView() {
        return parentKPAView;
    }

    public void setParentKPAView(KPAView parentKPAView) {
        this.parentKPAView = parentKPAView;
    }

    public String getParentKPAViewDD() {
        return "KPAView_parentKPAView";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="childUnits">
    @OneToMany(mappedBy = "parentKPAView")
    private List<KPAView> childKPAViews;

    public List<KPAView> getChildKPAViews() {
        return childKPAViews;
    }

    public void setChildKPAViews(List<KPAView> childKPAViews) {
        this.childKPAViews = childKPAViews;
    }

    public String getChildKPAViewsDD() {
        return "KPAView_childKPAViews";
    }
// </editor-fold>
}
