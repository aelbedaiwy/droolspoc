package com.unitedofoq.otms.eds.competency;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.eds.training.EDSTrainingCourse;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields="competency")
public class EDSCompetencyTrainingCourse extends BaseEntity{
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSCompetency competency;
    public String getCompetencyDD()      {    return "EDSCompetencyTrainingCourse_competency";  }
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY, optional=false)
    @JoinColumn
    private EDSTrainingCourse trainingCourse;
    public String getTrainingCourseDD()      {    return "EDSCompetencyTrainingCourse_trainingCourse";  }
    public EDSCompetency getCompetency() {
        return competency;
    }

    public void setCompetency(EDSCompetency competency) {
        this.competency = competency;
    }

    public EDSTrainingCourse getTrainingCourse() {
        return trainingCourse;
    }

    public void setTrainingCourse(EDSTrainingCourse trainingCourse) {
        this.trainingCourse = trainingCourse;
    }
}
