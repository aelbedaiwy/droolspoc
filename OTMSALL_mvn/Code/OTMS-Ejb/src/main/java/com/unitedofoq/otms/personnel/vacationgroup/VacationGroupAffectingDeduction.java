/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacationgroup;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author mragab
 */
@Entity
@DiscriminatorValue("VACATIONGROUPDED")
@ParentEntity(fields={"vacationGroup"})
//@FABSEntitySpecs(hideInheritedFields={"salaryElement"})
public class VacationGroupAffectingDeduction extends VacationGroupAffectingSalaryElement{
    // <editor-fold defaultstate="collapsed" desc="deduction">
    @JoinColumn//(name="SALARYELEMENT_DBID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Deduction deduction;

    public Deduction getDeduction() {
        return deduction;
    }

    public void setDeduction(Deduction deduction) {
        this.deduction = deduction;
    }
    public String getDeductionDD() {
        return "VacationGroupAffectingDeduction_deduction";
    }
// </editor-fold>
}