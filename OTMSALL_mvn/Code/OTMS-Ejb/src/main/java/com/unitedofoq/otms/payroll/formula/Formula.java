package com.unitedofoq.otms.payroll.formula;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import javax.persistence.*;

/**
 * The persistent class for the formula_w database table.
 * 
 */
@Entity
@ChildEntity(fields={"fullExpression"})
public class Formula extends BaseEntity
{
    // <editor-fold defaultstate="collapsed" desc="formulaExpression">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY, mappedBy = "formula")
    private FormulaExpression fullExpression;

    public FormulaExpression getFullExpression() {
        return fullExpression;
    }

    public void setFullExpression(FormulaExpression fullExpression) {
        this.fullExpression = fullExpression;
    }

     public String getFullExpressionDD() {
        return "Formula_fullExpression";
    }
  
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="expression">
    @Lob()
	@Column
	private String expression;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

        public String getExpressionDD() {
        return "Formula_expression";
    }
    
   

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="queryName">
    @Lob()
	@Column
	private String queryName;

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

     public String getQueryNameDD() {
        return "Formula_queryName";
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="queryStatement">
     @Lob()
	@Column
	private String queryStatement;

    public String getQueryStatement() {
        return queryStatement;
    }

    public void setQueryStatement(String queryStatement) {
        this.queryStatement = queryStatement;
    }

  public String getQueryStatementDD() {
        return "Formula_queryStatement";
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="name">
    @Column(nullable=false)
    @Translatable(translationField="nameTranslated")
    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getNameTranslatedDD(){

      return "Formula_nameTranslated";
    }
    @Transient
    @Translation(originalField="name")
    private String nameTranslated;
    public String getNameTranslated() {
        return nameTranslated;
    }
    public void setNameTranslated(String nameTranslated) {
        this.nameTranslated = nameTranslated;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="sortIndex">
	//sort order is numbers from 1 to 7 for each formula
	@Column(nullable=false)
	private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSortIndexDD() {
        return "Formula_sortIndex";
    }

 // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="statement">

	@Column
	private String statement;

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }
     public String getStatementDD() {
        return "Formula_statement";

    }


    // </editor-fold>

}