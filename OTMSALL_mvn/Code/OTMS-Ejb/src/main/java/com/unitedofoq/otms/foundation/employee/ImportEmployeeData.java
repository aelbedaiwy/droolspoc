package com.unitedofoq.otms.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.Entity;

@Entity
public class ImportEmployeeData extends BaseEntity{

    //<editor-fold defaultstate="collapsed" desc="code">
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD(){
        return "ImportEmployeeData_code";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="name">
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String date) {
        this.name = date;
    }

    public String getNameDD(){
        return "ImportEmployeeData_name";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="arabicName">
    private String arabicName;

    public String getArabicName() {
        return arabicName;
    }

    public String getArabicNameDD() {
        return "Employee_arabicName";
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }



    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="englishName">
    private String englishName;

    public String getEnglishName() {
        return englishName;
    }

    public String getEnglishNameDD() {
        return "Employee_englishName";
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="unitDescription">
    private String unitDescription;

    public String getUnitDescription() {
        return unitDescription;
    }

    public void setUnitDescription(String inTime) {
        this.unitDescription = inTime;
    }

    public String getUnitDescriptionDD(){
        return "ImportEmployeeData_unitDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="positionDescription">
    private String positionDescription;

    public String getPositionDescription() {
        return positionDescription;
    }

    public void setPositionDescription(String outTime) {
        this.positionDescription = outTime;
    }

    public String getPositionDescriptionDD(){
        return "ImportEmployeeData_positionDescription";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="hiringType">
    private String hiringType;

    public String getHiringType() {
        return hiringType;
    }

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }

    public String getHiringTypeDD(){
        return "ImportEmployeeData_hiringType";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="nationalID">
    private String nationalID;

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String done) {
        this.nationalID = done;
    }

    public String getNationalIDDD(){
        return "ImportEmployeeData_nationalID";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="currency">
    private String currency;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "ImportEmployeeData_currency";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="done">
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDoneDD(){
        return "ImportEmployeeData_done";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="isManager">
    private boolean manager;

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }

    public String getManagerDD(){
        return "ImportEmployeeData_manager";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="calendar">
    private String calendar;

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getCalendarDD() {
        return "ImportEmployeeData_calendar";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="birthDate">
    private String birthDate;

    public String getBirthDate() {
        return birthDate;
    }

    public String getBirthDateDD() {
        return "ImportEmployeeData_birthDate";
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="email">
    private String email;

    public String getEmail() {
        return email;
    }

    public String getEmailDD() {
        return "ImportEmployeeData_email";
    }

    public void setEmail(String email) {
        this.email = email;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="startDate">
    private String startDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getStartDateDD() {
        return "ImportEmployeeData_startDate";
    }
    //</editor-fold>
    private String mobile;
    private String address;
    private String address1;
    private String accountNo;
    private String bank;
    private String branch; //?
    private String cashOrBank;
    private String insuranceNumber;
    private String overTimeEntitled;
    private String phone;
    private String jobDescription; //?
    private String startSalary;
    private String prevExpMonths;
    private String prevExpYears;

    private String title;
    private String gender;
    private String maritalStatus;
    private String militaryStatus;
    private String nationality;
    private String religion;
    private String country;
    private String city;
    private String insuranceGroup;
    private String basicBase;
    private String variableBase;
    private String hiringDate;
    private String location;
    private String costCenter;
    private String taxGroup;
    private String payGrade;
    private String insuranceOffice;


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileDD() {
        return "ImportEmployeeData_mobile";
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDD() {
        return "ImportEmployeeData_address";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleDD() {
        return "ImportEmployeeData_title";
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGenderDD() {
        return "ImportEmployeeData_gender";
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatusDD() {
        return "ImportEmployeeData_maritalStatus";
    }

    public String getMilitaryStatus() {
        return militaryStatus;
    }

    public void setMilitaryStatus(String militaryStatus) {
        this.militaryStatus = militaryStatus;
    }

    public String getMilitaryStatusDD() {
        return "ImportEmployeeData_militaryStatus";
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNationalityDD() {
        return "ImportEmployeeData_nationality";
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getReligionDD() {
        return "ImportEmployeeData_religion";
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryDD() {
        return "ImportEmployeeData_country";
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityDD() {
        return "ImportEmployeeData_city";
    }

    private int sortIndex;

    public int getSortIndex() {
        return sortIndex;
    }

    public String getSortIndexDD() {
        return "ImportEmployeeData_sortIndex";
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getInsuranceOffice() {
        return insuranceOffice;
    }

    public String getInsuranceOfficeDD() {
        return "ImportEmployeeData_insuranceOffice";
    }

    public void setInsuranceOffice(String insuranceOffice) {
        this.insuranceOffice = insuranceOffice;
    }

    public String getPayGrade() {
        return payGrade;
    }

    public String getPayGradeDD() {
        return "ImportEmployeeData_payGrade";
    }

    public void setPayGrade(String payGrade) {
        this.payGrade = payGrade;
    }

    public String getBasicBase() {
        return basicBase;
    }

    public String getBasicBaseDD() {
        return "ImportEmployeeData_basicBase";
    }

    public void setBasicBase(String basicBase) {
        this.basicBase = basicBase;
    }


    public String getCostCenter() {
        return costCenter;
    }

    public String getCostCenterDD() {
        return "ImportEmployeeData_costCenter";
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }


    public String getInsuranceGroup() {
        return insuranceGroup;
    }

    public String getInsuranceGroupDD() {
        return "ImportEmployeeData_insuranceGroup";
    }

    public void setInsuranceGroup(String insuranceGroup) {
        this.insuranceGroup = insuranceGroup;
    }

    public String getLocation() {
        return location;
    }

    public String getLocationDD() {
        return "ImportEmployeeData_location";
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHiringDate() {
        return hiringDate;
    }

    public String getHiringDateDD() {
        return "ImportEmployeeData_hiringDate";
    }

    public void setHiringDate(String hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getTaxGroup() {
        return taxGroup;
    }

    public String getTaxGroupDD() {
        return "ImportEmployeeData_taxGroup";
    }

    public void setTaxGroup(String taxGroup) {
        this.taxGroup = taxGroup;
    }

    public String getVariableBase() {
        return variableBase;
    }

    public String getVariableBaseDD() {
        return "ImportEmployeeData_variableBase";
    }

    public void setVariableBase(String variableBase) {
        this.variableBase = variableBase;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1DD() {
        return "ImportEmployeeData_address1";
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountNoDD() {
        return "ImportEmployeeData_ACCOUNTNO";
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankDD() {
        return "ImportEmployeeData_bank";
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranchDD() {
        return "ImportEmployeeData_branch";
    }

    public String getCashOrBank() {
        return cashOrBank;
    }

    public void setCashOrBank(String cashOrBank) {
        this.cashOrBank = cashOrBank;
    }

    public String getCashOrBankDD() {
        return "ImportEmployeeData_cashOrBank";
    }
    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getInsuranceNumberDD() {
        return "ImportEmployeeData_insuranceNumber";
    }

    public String getOverTimeEntitled() {
        return overTimeEntitled;
    }

    public void setOverTimeEntitled(String overTimeEntitled) {
        this.overTimeEntitled = overTimeEntitled;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneDD() {
        return "ImportEmployeeData_phone";
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobDescriptionDD() {
        return "ImportEmployeeData_jobDescription";
    }

    public String getPrevExpMonths() {
        return prevExpMonths;
    }

    public void setPrevExpMonths(String prevExpMonths) {
        this.prevExpMonths = prevExpMonths;
    }

    public String getPrevExpMonthsDD() {
        return "ImportEmployeeData_prevExpMonths";
    }

    public String getPrevExpYears() {
        return prevExpYears;
    }

    public void setPrevExpYears(String prevExpYears) {
        this.prevExpYears = prevExpYears;
    }

    public String getPrevExpYearsDD() {
        return "ImportEmployeeData_prevExpYears";
    }

    public String getStartSalary() {
        return startSalary;
    }

    public void setStartSalary(String startSalary) {
        this.startSalary = startSalary;
    }

    public String getStartSalaryDD() {
        return "ImportEmployeeData_startSalary";
    }

    // <editor-fold defaultstate="collapsed" desc="">
    private String loginName;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginNameDD() {
        return "ImportEmployeeData_loginName";
    }
    // </editor-fold>
}
