package com.unitedofoq.otms.payroll;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("RETRO")
public class RetroPayrollCalculation extends PayrollCalculation {

    // <editor-fold defaultstate="collapsed" desc="toCalculatedPeriod">

    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    CalculatedPeriod toCalculatedPeriod;

    public CalculatedPeriod getToCalculatedPeriod() {
        return toCalculatedPeriod;
    }

    public void setToCalculatedPeriod(CalculatedPeriod toCalculatedPeriod) {
        this.toCalculatedPeriod = toCalculatedPeriod;
    }

    public String getToCalculatedPeriodDD() {
        return "RetroPayrollCalculation_toCalculatedPeriod";
    }
    // </editor-fold>
}
