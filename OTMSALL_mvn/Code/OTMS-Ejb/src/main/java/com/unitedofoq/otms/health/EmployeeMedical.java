/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.health;

import com.unitedofoq.fabs.core.entitybase.ChildEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author mostafa
 */
@Entity
@ChildEntity(fields = {"medicalClaim"})
@ParentEntity(fields = {"employee"})
public class EmployeeMedical extends EmployeeMedicalBase{
    
    // <editor-fold defaultstate="collapsed" desc="employee">
    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable=false)
    private Employee employee;
    public Employee getEmployee() {
        return employee;
    }
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeMedical_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="medicalClaim">
    @OneToMany(mappedBy = "employee")
    private List<MedicalClaim> medicalClaim;
    
  
    public List<MedicalClaim> getMedicalClaim() {
        return medicalClaim;
    }

    public void setMedicalClaim(List<MedicalClaim> medicalClaim) {
        this.medicalClaim = medicalClaim;
    }
    public String getMedicalClaimDD() {
        return "EmployeeMedical_medicalClaim";
    }
    
     // </editor-fold>
}
