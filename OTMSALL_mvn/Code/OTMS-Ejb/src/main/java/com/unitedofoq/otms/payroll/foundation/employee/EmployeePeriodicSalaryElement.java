package com.unitedofoq.otms.payroll.foundation.employee;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.otms.foundation.currency.Currency;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElementType;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 */
@Entity
@ParentEntity(fields = "employee")
public class EmployeePeriodicSalaryElement extends BaseEntity {

    // <editor-fold defaultstate="collapsed" desc="basicAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal basicAmount;

    public BigDecimal getBasicAmount() {
        return basicAmount;
    }

    public void setBasicAmount(BigDecimal basicAmount) {
        this.basicAmount = basicAmount;
    }

    public String getBasicAmountDD() {
        return "EmployeePeriodicSalaryElement_basicAmount";
    }
    @Transient
    private BigDecimal basicAmountMask;

    public BigDecimal getBasicAmountMask() {
        basicAmountMask = basicAmount;
        return basicAmountMask;
    }

    public void setBasicAmountMask(BigDecimal basicAmountMask) {
        updateDecimalValue("basicAmount", basicAmountMask);
    }

    public String getBasicAmountMaskDD() {
        return "EmployeePeriodicSalaryElement_basicAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="basicAmountEnc">
    @Column
    private String basicAmountEnc;

    public String getBasicAmountEnc() {
        return basicAmountEnc;
    }

    public void setBasicAmountEnc(String basicAmountEnc) {
        this.basicAmountEnc = basicAmountEnc;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="payCalculatedPeriod">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private CalculatedPeriod payCalculatedPeriod;

    public CalculatedPeriod getPayCalculatedPeriod() {
        return payCalculatedPeriod;
    }

    public void setPayCalculatedPeriod(CalculatedPeriod payCalculatedPeriod) {
        this.payCalculatedPeriod = payCalculatedPeriod;
    }

    public String getPayCalculatedPeriodDD() {
        return "EmployeePeriodicSalaryElement_payCalculatedPeriod";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="forCompany">
    @Column(length = 1)
    private String forCompany;

    public String getForCompany() {
        return forCompany;
    }

    public void setForCompany(String forCompany) {
        this.forCompany = forCompany;
    }

    public String getForCompanyDD() {
        return "EmployeePeriodicSalaryElement_forCompany";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency currency;  //Copy of the salary element currency

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCurrencyDD() {
        return "EmployeePeriodicSalaryElement_currency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="nature">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private UDC nature;

    public UDC getNature() {
        return nature;
    }

    public void setNature(UDC nature) {
        this.nature = nature;
    }

    public String getNatureDD() {
        return "EmployeePeriodicSalaryElement_nature";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="elementType">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private SalaryElementType elementType;

    public SalaryElementType getElementType() {
        return elementType;
    }

    public void setElementType(SalaryElementType elementType) {
        this.elementType = elementType;
    }

    public String getElementTypeDD() {
        return "EmployeePeriodicSalaryElement_elementType";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeePeriodicSalaryElement_employee";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalEntityCurrency">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Currency legalEntityCurrency; // Of Employee Legal Entity

    public Currency getLegalEntityCurrency() {
        return legalEntityCurrency;
    }

    public void setLegalEntityCurrency(Currency legalEntityCurrency) {
        this.legalEntityCurrency = legalEntityCurrency;
    }

    public String getLegalEntityCurrencyDD() {
        return "EmployeePeriodicSalaryElement_legalEntityCurrency";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="legalEntityCurNetAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal legalEntityCurNetAmount; // Legal Entity of Employee

    public BigDecimal getLegalEntityCurNetAmount() {
        return legalEntityCurNetAmount;
    }

    public void setLegalEntityCurNetAmount(BigDecimal legalEntityCurNetAmount) {
        this.legalEntityCurNetAmount = legalEntityCurNetAmount;
    }

    public String getLegalEntityCurNetAmountDD() {
        return "EmployeePeriodicSalaryElement_legalEntityCurNetAmount";
    }
    @Transient
    private BigDecimal legalEntityCurNetAmountMask;

    public BigDecimal getLegalEntityCurNetAmountMask() {
        legalEntityCurNetAmountMask = legalEntityCurNetAmount;
        return legalEntityCurNetAmountMask;
    }

    public void setLegalEntityCurNetAmountMask(BigDecimal legalEntityCurNetAmountMask) {
        updateDecimalValue("legalEntityCurNetAmount", legalEntityCurNetAmountMask);
    }

    public String getLegalEntityCurNetAmountMaskDD() {
        return "EmployeePeriodicSalaryElement_legalEntityCurNetAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="monthly">
    @Column(length = 1)
    private String monthly;

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getMonthlyDD() {
        return "EmployeePeriodicSalaryElement_monthly";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="currencyNetAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal currencyNetAmount; // Salary Element Currency Net Amount

    public BigDecimal getCurrencyNetAmount() {
        return currencyNetAmount;
    }

    public void setCurrencyNetAmount(BigDecimal currencyNetAmount) {
        this.currencyNetAmount = currencyNetAmount;
    }

    public String getCurrencyNetAmountDD() {
        return "EmployeePeriodicSalaryElement_currencyNetAmount";
    }
    @Transient
    private BigDecimal currencyNetAmountMask;

    public BigDecimal getCurrencyNetAmountMask() {
        currencyNetAmountMask = currencyNetAmount;
        return currencyNetAmountMask;
    }

    public void setCurrencyNetAmountMask(BigDecimal currencyNetAmountMask) {
        updateDecimalValue("currencyNetAmount", currencyNetAmountMask);
    }

    public String getCurrencyNetAmountMaskDD() {
        return "EmployeePeriodicSalaryElement_currencyNetAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="localCurrencyNetAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal localCurrencyNetAmount; //Organization Currency

    public BigDecimal getLocalCurrencyNetAmount() {
        return localCurrencyNetAmount;
    }

    public void setLocalCurrencyNetAmount(BigDecimal localCurrencyNetAmount) {
        this.localCurrencyNetAmount = localCurrencyNetAmount;
    }

    public String getLocalCurrencyNetAmountDD() {
        return "EmployeePeriodicSalaryElement_localCurrencyNetAmount";
    }
    @Transient
    private BigDecimal localCurrencyNetAmountMask;

    public BigDecimal getLocalCurrencyNetAmountMask() {
        localCurrencyNetAmountMask = localCurrencyNetAmount;
        return localCurrencyNetAmountMask;
    }

    public void setLocalCurrencyNetAmountMask(BigDecimal localCurrencyNetAmountMask) {
        updateDecimalValue("localCurrencyNetAmount", localCurrencyNetAmountMask);
    }

    public String getLocalCurrencyNetAmountMaskDD() {
        return "EmployeePeriodicSalaryElement_localCurrencyNetAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn(nullable = false)
    private SalaryElement salaryElement;

    public SalaryElement getSalaryElement() {
        return salaryElement;
    }

    public void setSalaryElement(SalaryElement salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElementDD() {
        return "EmployeePeriodicSalaryElement_salaryElement";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="taxable">
    @Column(length = 1)
    private String taxable;

    public String getTaxable() {
        return taxable;
    }

    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }

    public String getTaxableDD() {
        return "EmployeePeriodicSalaryElement_taxable";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="basisAmount">
    @Column(precision = 25, scale = 13)
    private BigDecimal basisAmount;

    public BigDecimal getBasisAmount() {
        return basisAmount;
    }

    public void setBasisAmount(BigDecimal basisAmount) {
        this.basisAmount = basisAmount;
    }

    public String getBasisAmountDD() {
        return "EmployeePeriodicSalaryElement_basisAmount";
    }
    @Transient
    private BigDecimal basisAmountMask;

    public BigDecimal getBasisAmountMask() {
        basisAmountMask = basisAmount;
        return basisAmountMask;
    }

    public void setBasisAmountMask(BigDecimal basisAmountMask) {
        updateDecimalValue("basisAmount", basisAmountMask);
    }

    public String getBasisAmountMaskDD() {
        return "EmployeePeriodicSalaryElement_basisAmountMask";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="basisAmountEnc">
    @Column
    private String basisAmountEnc;

    public String getBasisAmountEnc() {
        return basisAmountEnc;
    }

    public void setBasisAmountEnc(String basisAmountEnc) {
        this.basisAmountEnc = basisAmountEnc;
    }
     // </editor-fold>
}
