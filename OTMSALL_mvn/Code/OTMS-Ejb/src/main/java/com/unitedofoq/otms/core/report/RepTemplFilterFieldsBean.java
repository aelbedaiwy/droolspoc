package com.unitedofoq.otms.core.report;

import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class RepTemplFilterFieldsBean implements RepTemplFilterFieldsBeanLocal {

    @EJB
    private OEntityManagerRemote oem;

    @Override
    public List<RepFilterField> getDataSetFilterFields(long dsDBID, OUser loggedUser) {
        List<RepFilterField> fields = new ArrayList<RepFilterField>();
        try {
            fields = oem.
                    createListNamedQuery("getFilterFields", loggedUser,
                    "langDBID", loggedUser.getFirstLanguage().getDbid(), "dsDBID", dsDBID);
//            fields = (List<Object>) em.createNativeQuery("select fieldDBID, fieldExpression, distinctFilterList, companyExpression"
//                    + ", columnName, udcDBID, oentityDBID, oentityClassPath, ddLabel "
//                    + " from RepFilterFields where dsDBID = " + dsDBID
//                    + "  and langid = " + loggedUser.getFirstLanguage().getDbid()).getResultList();

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser, "OUser", loggedUser);
        } finally {
            return fields;
        }
    }

    @Override
    public List<Object> getReportFilterScreenDDs(OUser loggedUser) {
        List<Object> dds = new ArrayList<Object>();
        String query = null;
        try {
            if (loggedUser.getFirstLanguage().getDbid() == 23) {//English
                query = "select name,label from dd where name in ("
                        + "'unitLevel1',"
                        + "'unitLevel2',"
                        + "'unitLevel3',"
                        + "'unitLevel4',"
                        + "'unitLevel5',"
                        + "'unitLevel6',"
                        + "'unitLevel7',"
                        + "'unitLevel8',"
                        + "'unitLevel9',"
                        + "'repFilterStartDateLabel',"
                        + "'repFilterEndDateLabel',"
                        + "'repFilterDateSectionTitle',"
                        + "'repFilterLevelSectionTitle',"
                        + "'repFilterOrdinarySectionTitle',"
                        + "'singleCalcPeriodValidationMsg',"
                        + "'noSelectedCalcPeriodValidationMsg',"
                        + "'buttonSave',"
                        + "'repConfigDeselectAll',"
                        + "'noneLabel4Report',"
                        + "'buttonRun'"
                        + ")";
            } else {
                query = "select t1.name, t0.label as labelTranslated,t1.label"
                        + " from ddtranslation t0 LEFT OUTER JOIN dd t1 ON (t0.entitydbid = t1.dbid) where t1.name in ("
                        + "'unitLevel1',"
                        + "'unitLevel2',"
                        + "'unitLevel3',"
                        + "'unitLevel4',"
                        + "'unitLevel5',"
                        + "'unitLevel6',"
                        + "'unitLevel7',"
                        + "'unitLevel8',"
                        + "'unitLevel9',"
                        + "'repFilterStartDateLabel',"
                        + "'repFilterEndDateLabel',"
                        + "'repFilterDateSectionTitle',"
                        + "'repFilterLevelSectionTitle',"
                        + "'repFilterOrdinarySectionTitle',"
                        + "'singleCalcPeriodValidationMsg',"
                        + "'noSelectedCalcPeriodValidationMsg',"
                        + "'buttonSave',"
                        + "'repConfigDeselectAll',"
                        + "'buttonRun'"
                        + ") and t0.language_dbid = " + loggedUser.getFirstLanguage().getDbid();
            }
            dds = oem.executeEntityListNativeQuery(query, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser, "OUser", loggedUser);
        } finally {
            return dds;
        }
    }
}
