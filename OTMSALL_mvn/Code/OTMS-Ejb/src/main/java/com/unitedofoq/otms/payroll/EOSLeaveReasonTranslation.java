/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.payroll;

import com.unitedofoq.fabs.core.entitybase.BaseEntityTranslation;
import javax.persistence.Entity;

/**
 *
 * @author MIR
 */
@Entity
public class EOSLeaveReasonTranslation extends BaseEntityTranslation {
    // <editor-fold defaultstate="collapsed" desc="description">
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // </editor-fold >
}
