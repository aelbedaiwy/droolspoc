/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.otms.foundation.employee.Employee;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mostafa
 */
@Entity
public class EmployeeVacationCategory extends BaseEntity {
    //<editor-fold defaultstate="collapsed" desc="period">
    private Integer vacPeriod;

    public Integer getVacPeriod() {
        return vacPeriod;
    }

    public void setVacPeriod(Integer vacPeriod) {
        this.vacPeriod = vacPeriod;
    }
    
    public String getVacPeriodDD() {
        return "EmployeeVacationCategory_vacPeriod";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="startDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getStartDateDD() {
        return "EmployeeVacationCategory_startDate";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="category">
    private Integer category;

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
    
    public String getCategoryDD() {
        return "EmployeeVacationCategory_category";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="endDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public String getEndDateDD() {
        return "EmployeeVacationCategory_endDate";
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="vacationRequest">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private EmployeeVacationRequest vacationRequest;
    
    public EmployeeVacationRequest getVacationRequest() {
        return vacationRequest;
    }

    public void setVacationRequest(EmployeeVacationRequest vacationRequest) {
        this.vacationRequest = vacationRequest;
    }
    
    public String getVacationRequestDD() {
        return "EmployeeVacationCategory_vacationRequest";
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="vacation">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Vacation vacation;

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }
     public String getVacationDD() {
        return "EmployeeVacationCategory_vacation";
    }
     // </editor-fold>
     
    // <editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getEmployeeDD() {
        return "EmployeeVacationCategory_employee";
    }
// </editor-fold>
}
