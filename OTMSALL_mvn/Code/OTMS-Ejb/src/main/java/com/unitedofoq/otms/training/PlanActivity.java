/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.training;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.training.activity.Activity;
import com.unitedofoq.otms.training.activity.Provider;
import com.unitedofoq.otms.training.activity.ProviderActivity;
import com.unitedofoq.otms.training.instructor.Instructor;
import com.unitedofoq.otms.training.location.Location;
import com.unitedofoq.otms.training.scheduleevent.ScheduleEvent;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mragab
 */

@Entity
@ParentEntity(fields={"trainingPlan"})
public class PlanActivity extends BaseEntity {
    // <editor-fold defaultstate="collapsed" desc="trainingPlan">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private TrainingPlan trainingPlan;

    public TrainingPlan getTrainingPlan() {
        return trainingPlan;
    }

    public void setTrainingPlan(TrainingPlan trainingPlan) {
        this.trainingPlan = trainingPlan;
    }

    public String getTrainingPlanDD() {
        return "PlanActivity_trainingPlan";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="activity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getActivityDD() {
        return "PlanActivity_activity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="providerActivity">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderActivity providerActivity;

    public ProviderActivity getProviderActivity() {
        return providerActivity;
    }

    public void setProviderActivity(ProviderActivity providerActivity) {
        this.providerActivity = providerActivity;
    }

    public String getProviderActivityDD() {
        return "PlanActivity_providerActivity";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="scheduleEvent">
	@ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ScheduleEvent scheduleEvent;

    public ScheduleEvent getScheduleEvent() {
        return scheduleEvent;
    }

    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
        this.scheduleEvent = scheduleEvent;
    }

    public String getScheduleEventDD() {
        return "PlanActivity_scheduleEvent";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDateDD() {
        return "PlanActivity_fromDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toDate">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getToDateDD() {
        return "PlanActivity_toDate";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fromTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromTime;

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public String getFromTimeDD() {
        return "PlanActivity_fromTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="toTime">
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date toTime;

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public String getToTimeDD() {
        return "PlanActivity_toTime";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "PlanActivity_provider";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="location">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLocationDD() {
        return "PlanActivity_location";
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="instructor">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Instructor instructor;
    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public String getInstructorDD() {
        return "PlanActivity_instructor";
    }
    // </editor-fold>
}