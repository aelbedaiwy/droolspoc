package com.unitedofoq.otms.appraisal.employee;

import com.unitedofoq.fabs.core.entitybase.ParentEntity;
import com.unitedofoq.otms.appraisal.measure.QualitativeMeasure;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.recruitment.applicant.Applicant;
import com.unitedofoq.otms.training.activity.Provider;
import com.unitedofoq.otms.training.activity.ProviderCourseInformation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@ParentEntity(fields = {"employee"})
public class EmployeeQualitativeMeasure extends EmployeeKPA {

    //<editor-fold defaultstate="collapsed" desc="code">
    @Column
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDD() {
        return "EmployeeQualitativeMeasures_code";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="description">
    @Column(nullable = false)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionDD() {
        return "EmployeeQualitativeMeasures_description";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employee">
    @JoinColumn
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeDD() {
        return "EmployeeQualitativeMeasures_employee";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="qualitativeMeasure">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @JoinColumn
    private QualitativeMeasure qualitativeMeasure;

    public QualitativeMeasure getQualitativeMeasure() {
        return qualitativeMeasure;
    }

    public void setQualitativeMeasure(QualitativeMeasure qualitativeMeasure) {
        this.qualitativeMeasure = qualitativeMeasure;
    }

    public String getQualitativeMeasureDD() {
        return "EmployeeQualitativeMeasuresqualitativeMeasure";
    }
    //</editor-fold>

    // for recruitment:
    // =================
    //<editor-fold defaultstate="collapsed" desc="applicant">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Applicant applicant;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getApplicantDD() {
        return "EmployeeQualitativeMeasures_applicant";
    }
    //</editor-fold>

    // for training:
    // =================
    //<editor-fold defaultstate="collapsed" desc="course">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private ProviderCourseInformation course;

    public ProviderCourseInformation getCourse() {
        return course;
    }

    public void setCourse(ProviderCourseInformation course) {
        this.course = course;
    }

    public String getCourseDD() {
        return "EmployeeQualitativeMeasures_course";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="provider">
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderDD() {
        return "EmployeeQualitativeMeasures_provider";
    }
    //</editor-fold>
}
