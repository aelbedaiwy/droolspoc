/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.foundation.employee;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author ashienawy
 */

@Entity
//Salema[Employee New Design]
//@ParentEntity(fields={"employee"})
@Table(name="EmployeePosition")
@DiscriminatorColumn(name="EPTYPE")
@DiscriminatorValue(value="2001")
public class EmployeeMainPosition extends EmployeePositionBase {
    // <editor-fold defaultstate="collapsed" desc="employee">
    //Salema[Employee New Design]
//    @OneToOne(fetch = javax.persistence.FetchType.LAZY)
//    @JoinColumn(nullable=false)
//    private Employee employee ;
//
//    public Employee getEmployee() {
//        return employee;
//    }
//
//    public void setEmployee(Employee employee) {
//        this.employee = employee;
//    }
//    public String getEmployeeDD() {
//        return "EmployeeMainPosition_employee";
//    }
    // </editor-fold>
}
