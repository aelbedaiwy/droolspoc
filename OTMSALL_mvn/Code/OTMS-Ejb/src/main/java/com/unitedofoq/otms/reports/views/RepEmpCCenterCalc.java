/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.reports.views;

import java.math.BigDecimal;
import javax.persistence.*;

/**
 *
 * @author lahmed
 */
@Entity
public class RepEmpCCenterCalc extends RepEmployeeBase  {

    // <editor-fold defaultstate="collapsed" desc="costCenterDistCode">
    @Column
    private String costCenterDistCode;

    public void setCostCenterDistCode(String costCenterDistCode) {
        this.costCenterDistCode = costCenterDistCode;
    }

    public String getCostCenterDistCode() {
        return costCenterDistCode;
    }

    public String getCostCenterDistCodeDD() {
        return "RepEmpCCenterCalc_costCenterDistCode";
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="costCenterDist">
    private String costCenterDist;

    public String getCostCenterDist() {
        return costCenterDist;
    }

    public void setCostCenterDist(String costCenterDist) {
        this.costCenterDist = costCenterDist;
    }
    
    public String getCostCenterDistDD() {
        return "RepEmpCCenterCalc_costCenterDist";
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="calcPeriod">
    @Column
    private String calcPeriod;

    public void setCalcPeriod(String calcPeriod) {
        this.calcPeriod = calcPeriod;
    }

    public String getCalcPeriod() {
        return calcPeriod;
    }

    public String getCalcPeriodDD() {
        return "RepEmpCCenterCalc_calcPeriod";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="distributedVal">
    @Column
    private BigDecimal distributedVal;

    public void setDistributedVal(BigDecimal distributedVal) {
        this.distributedVal = distributedVal;
    }

    public BigDecimal getDistributedVal() {
        return distributedVal;
    }

    public String getDistributedValDD() {
        return "RepEmpCCenterCalc_distributedVal";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="salaryElement">
    @Column
    private String salaryElement;

    public void setSalaryElement(String salaryElement) {
        this.salaryElement = salaryElement;
    }

    public String getSalaryElement() {
        return salaryElement;
    }

    public String getSalaryElementDD() {
        return "RepEmpCCenterCalc_salaryElement";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="salaryElementType">
    @Column
    private String salaryElementType;

    public void setSalaryElementType(String salaryElementType) {
        this.salaryElementType = salaryElementType;
    }

    public String getSalaryElementType() {
        return salaryElementType;
    }

    public String getSalaryElementTypeDD() {
        return "RepEmpCCenterCalc_salaryElementType";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="segment1">
    @Column
    private String segment1;

    public String getSegment1() {
        return segment1;
    }

    public void setSegment1(String segment1) {
        this.segment1 = segment1;
    }
    
    public String getSegment1DD() {
        return "RepEmpCCenterCalc_segment1";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment2">
    @Column
    private String segment2;

    public String getSegment2() {
        return segment2;
    }

    public void setSegment2(String segment2) {
        this.segment2 = segment2;
    }
    
    public String getSegment2DD() {
        return "RepEmpCCenterCalc_segment2";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment3">
    @Column
    private String segment3;

    public String getSegment3() {
        return segment3;
    }

    public void setSegment3(String segment3) {
        this.segment3 = segment3;
    }
    
    public String getSegment3DD() {
        return "RepEmpCCenterCalc_segment3";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment4">
    @Column
    private String segment4;

    public String getSegment4() {
        return segment4;
    }

    public void setSegment4(String segment4) {
        this.segment4 = segment4;
    }
    
    public String getSegment4DD() {
        return "RepEmpCCenterCalc_segment4";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment5">
    @Column
    private String segment5;

    public String getSegment5() {
        return segment5;
    }

    public void setSegment5(String segment5) {
        this.segment5 = segment5;
    }
    
    public String getSegment5DD() {
        return "RepEmpCCenterCalc_segment5";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment6">
    @Column
    private String segment6;

    public String getSegment6() {
        return segment6;
    }

    public void setSegment6(String segment6) {
        this.segment6 = segment6;
    }
    
    public String getSegment6DD() {
        return "RepEmpCCenterCalc_segment6";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment7">
    @Column
    private String segment7;

    public String getSegment7() {
        return segment7;
    }

    public void setSegment7(String segment7) {
        this.segment7 = segment7;
    }
    
    public String getSegment7DD() {
        return "RepEmpCCenterCalc_segment7";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment8">
    @Column
    private String segment8;

    public String getSegment8() {
        return segment8;
    }

    public void setSegment8(String segment8) {
        this.segment8 = segment8;
    }
    
    public String getSegment8DD() {
        return "RepEmpCCenterCalc_segment8";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment9">
    @Column
    private String segment9;

    public String getSegment9() {
        return segment9;
    }

    public void setSegment9(String segment9) {
        this.segment9 = segment9;
    }
    
    public String getSegment9DD() {
        return "RepEmpCCenterCalc_segment9";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="segment10">
    @Column
    private String segment10;

    public String getSegment10() {
        return segment10;
    }

    public void setSegment10(String segment10) {
        this.segment10 = segment10;
    }
    
    public String getSegment10DD() {
        return "RepEmpCCenterCalc_segment10";
    }
    // </editor-fold>
}
