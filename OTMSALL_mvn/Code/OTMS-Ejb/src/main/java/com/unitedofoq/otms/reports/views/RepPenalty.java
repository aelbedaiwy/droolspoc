
package com.unitedofoq.otms.reports.views;

import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.i18n.Translatable;
import com.unitedofoq.fabs.core.i18n.Translation;
import java.math.BigDecimal;
import javax.persistence.*;
import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@ReadOnly
public class RepPenalty extends BaseEntity  {

    // <editor-fold defaultstate="collapsed" desc="dsDBID">
    @Column
    private long dsDBID;

    public void setDsDBID(long dsDBID) {
        this.dsDBID = dsDBID;
    }

    public long getDsDBID() {
        return dsDBID;
    }

    public String getDsDBIDDD() {
        return "RepPenalty_dsDBID";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="penalty">
    @Column
    @Translatable(translationField = "penaltyTranslated")
    private String penalty;

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getPenalty() {
        return penalty;
    }

    public String getPenaltyDD() {
        return "RepPenalty_penalty";
    }
    
    @Transient
    @Translation(originalField = "penalty")
    private String penaltyTranslated;

    public String getPenaltyTranslated() {
        return penaltyTranslated;
    }

    public void setPenaltyTranslated(String penaltyTranslated) {
        this.penaltyTranslated = penaltyTranslated;
    }
    
    public String getPenaltyTranslatedDD() {
        return "RepPenalty_penalty";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="occuranceOrValue">
    @Column
    @Translatable(translationField = "occuranceOrValueTranslated")
    private String occuranceOrValue;

    public void setOccuranceOrValue(String occuranceOrValue) {
        this.occuranceOrValue = occuranceOrValue;
    }

    public String getOccuranceOrValue() {
        return occuranceOrValue;
    }

    public String getOccuranceOrValueDD() {
        return "RepPenalty_occuranceOrValue";
    }
    
    @Transient
    @Translation(originalField = "occuranceOrValue")
    private String occuranceOrValueTranslated;

    public String getOccuranceOrValueTranslated() {
        return occuranceOrValueTranslated;
    }

    public void setOccuranceOrValueTranslated(String occuranceOrValueTranslated) {
        this.occuranceOrValueTranslated = occuranceOrValueTranslated;
    }
    
    public String getOccuranceOrValueTranslatedDD() {
        return "RepPenalty_occuranceOrValue";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="sortIndex">
    @Column
    private int sortIndex;

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public String getSortIndexDD() {
        return "RepPenalty_sortIndex";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="occuranceFrom">
    @Column
    private BigDecimal occuranceFrom;

    public void setOccuranceFrom(BigDecimal occuranceFrom) {
        this.occuranceFrom = occuranceFrom;
    }

    public BigDecimal getOccuranceFrom() {
        return occuranceFrom;
    }

    public String getOccuranceFromDD() {
        return "RepPenalty_occuranceFrom";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="occuranceTo">
    @Column
    private BigDecimal occuranceTo;

    public void setOccuranceTo(BigDecimal occuranceTo) {
        this.occuranceTo = occuranceTo;
    }

    public BigDecimal getOccuranceTo() {
        return occuranceTo;
    }

    public String getOccuranceToDD() {
        return "RepPenalty_occuranceTo";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="factorOrAmount">
    @Column
    @Translatable(translationField = "factorOrAmountTranslated")
    private String factorOrAmount;

    public void setFactorOrAmount(String factorOrAmount) {
        this.factorOrAmount = factorOrAmount;
    }

    public String getFactorOrAmount() {
        return factorOrAmount;
    }

    public String getFactorOrAmountTranslatedDD() {
        return "RepPenalty_factorOrAmount";
    }
    
    @Transient
    @Translation(originalField = "factorOrAmount")
    private String factorOrAmountTranslated;

    public String getFactorOrAmountTranslated() {
        return factorOrAmountTranslated;
    }

    public void setFactorOrAmountTranslated(String factorOrAmountTranslated) {
        this.factorOrAmountTranslated = factorOrAmountTranslated;
    }    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="decreasePercent">
    @Column
    private BigDecimal decreasePercent;

    public void setDecreasePercent(BigDecimal decreasePercent) {
        this.decreasePercent = decreasePercent;
    }

    public BigDecimal getDecreasePercent() {
        return decreasePercent;
    }

    public String getDecreasePercentDD() {
        return "RepPenalty_decreasePercent";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="companyId">
    @Column
    private String companyID;

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String getCompanyIDDD() {
        return "RepPenalty_companyID";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="refreshEvery">
    @Column
    private BigDecimal refreshEvery;

    public BigDecimal getRefreshEvery() {
        return refreshEvery;
    }

    public void setRefreshEvery(BigDecimal refreshEvery) {
        this.refreshEvery = refreshEvery;
    }

     public String getRefreshEveryDD() {
        return "RepPenalty_refreshEvery";
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="refreshUnitMeasure">
    @Column
    @Translatable(translationField = "refreshUnitMeasureTranslated")
    private String refreshUnitMeasure;//(name = "refresh_unit_measure")

    public String getRefreshUnitMeasure() {
        return refreshUnitMeasure;
    }

    public void setRefreshUnitMeasure(String refreshUnitMeasure) {
        this.refreshUnitMeasure = refreshUnitMeasure;
    }

     public String getRefreshUnitMeasureTranslatedDD() {
        return "RepPenalty_refreshUnitMeasure";
    }
    
    @Transient
    @Translation(originalField = "refreshUnitMeasure")
    private String refreshUnitMeasureTranslated;

    public String getRefreshUnitMeasureTranslated() {
        return refreshUnitMeasureTranslated;
    }

    public void setRefreshUnitMeasureTranslated(String refreshUnitMeasureTranslated) {
        this.refreshUnitMeasureTranslated = refreshUnitMeasureTranslated;
    }    
    // </editor-fold>
     
     // <editor-fold defaultstate="collapsed" desc="unitMeasure">
    //Hour H - Day D - Value V
    @Column
    @Translatable(translationField = "unitMeasureTranslated")
    private String unitMeasure;//(name = "unit_measure")

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getUnitMeasureTranslatedDD() {
        return "RepPenalty_unitMeasure";
    }
    
    @Transient
    @Translation(originalField = "unitMeasure")
    private String unitMeasureTranslated;

    public String getUnitMeasureTranslated() {
        return unitMeasureTranslated;
    }

    public void setUnitMeasureTranslated(String unitMeasureTranslated) {
        this.unitMeasureTranslated = unitMeasureTranslated;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="refreshMethod">
    @Column
    //Hiring Date H - First Penalty Date P
    @Translatable(translationField = "refreshMethodTranslated")
    private String refreshMethod;//(name = "refresh_method")

    public String getRefreshMethod() {
        return refreshMethod;
    }

    public void setRefreshMethod(String refreshMethod) {
        this.refreshMethod = refreshMethod;
    }
    public String getRefreshMethodTranslatedDD() {
        return "RepPenalty_refreshMethod";
    }
    
    @Transient
    @Translation(originalField = "refreshMethod")
    private String refreshMethodTranslated;
    
    public String getRefreshMethodTranslated() {
        return refreshMethodTranslated;
    }

    public void setRefreshMethodTranslated(String refreshMethodTranslated) {
        this.refreshMethodTranslated = refreshMethodTranslated;
    }
    // </editor-fold>
}
