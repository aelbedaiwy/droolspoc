package com.unitedofoq.otms.personnel.investigation;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface InvestigationServiceLocal {
    public OFunctionResult postSaveEmpInvestigation(ODataMessage odm,OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateInvestigationSuspension(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
