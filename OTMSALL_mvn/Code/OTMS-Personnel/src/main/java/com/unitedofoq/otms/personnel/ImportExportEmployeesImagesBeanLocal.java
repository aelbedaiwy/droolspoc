package com.unitedofoq.otms.personnel;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.ejb.Local;

/**
 *
 * @author Mohamed Aboelnour
 */
@Local
public interface ImportExportEmployeesImagesBeanLocal {
    public OFunctionResult Import(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
    public OFunctionResult Export(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) throws FileNotFoundException, IOException;
    
    public OFunctionResult initializePayrollParameter(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
