package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.personnel.penalty.PenaltyValidationLocal", beanInterface = PenaltyValidationLocal.class)
public class PenaltyValidation implements PenaltyValidationLocal {

	@EJB
	private UserMessageServiceRemote userMessageServiceRemote;

	@Override
	public OFunctionResult validatePenaltyDate(ODataMessage odm,
			OFunctionParms parms, OUser loggedUser) {
		OFunctionResult functionResult = new OFunctionResult();
		Date penaltyDate;
		Date todayDate;

		try {
			EmployeePenaltyRequest employeePenaltyRequest = (EmployeePenaltyRequest) odm
					.getData().get(1);
			penaltyDate = employeePenaltyRequest.getPenaltyDate();
			todayDate = new Date();
			if (penaltyDate != null) {
				if (todayDate.before(penaltyDate)) {
					functionResult.addError(userMessageServiceRemote
							.getUserMessage("PenaltyDateError", loggedUser));
				}

			} else {
				functionResult.addError(userMessageServiceRemote
						.getUserMessage("InvalidPenaltyDate", loggedUser));
			}
		} catch (Exception ee) {
			functionResult.addError(userMessageServiceRemote.getUserMessage(
					"InvalidPenaltyDate", loggedUser), ee);
		}
		return functionResult;
	}
}
