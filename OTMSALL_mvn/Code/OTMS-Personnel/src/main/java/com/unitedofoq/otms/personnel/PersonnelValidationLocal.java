package com.unitedofoq.otms.personnel;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface PersonnelValidationLocal {
    public OFunctionResult AbsenceCancellation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult AbsenceEntry(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult PenaltyEntry(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult GetPenaltyValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult PenaltyCancellation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

}