package com.unitedofoq.otms.personnel.absence;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name="java:global/ofoq/com.unitedofoq.otms.personnel.absence.AbsenceProcessInputLocal",
    beanInterface=AbsenceProcessInputLocal.class)
public class AbsenceProcessInput implements AbsenceProcessInputLocal {

    @Override
    public OFunctionResult prepareAbsenceProcessInput(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult result = new OFunctionResult();
        List<Object> data = new ArrayList<Object>();
        if(odm != null && odm.getData() != null && odm.getData().size() == 2)
        {
            String processInput = "";
            EmployeeAbsenceRequest absenceRequest = (EmployeeAbsenceRequest) odm.getData().get(0);
            processInput = fillXML(absenceRequest, loggedUser);
            data.add(processInput);
        }
        result.setReturnedDataMessage(odm);
        result.setReturnValues(data);
        return result;
    }
    private String fillXML(EmployeeAbsenceRequest absenceRequest,OUser loggedUser)
    {
        String output = "";
        String desc = null, managerName = null, employeeName = null, department = null, userName = null;
        try
        {
         desc = absenceRequest.getAbsence().getDescription();
         managerName = absenceRequest.getEmployee().getDirectManager().getName();
         employeeName = absenceRequest.getEmployee().getName();
         department = absenceRequest.getEmployee().getUnit().getName();
         userName = absenceRequest.getEmployee().getDirectManager().getEmployeeUser().getLoginName();
        }
         catch(Exception e)
        {
             if(desc == null) desc = "";
             if(managerName == null) managerName = "test Manager Name";
             if(employeeName == null) employeeName = "test";
             if(department == null) department = "";
             if(userName == null) userName = "test";
         }
        output = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/s"
                + "oap/envelope/\" xmlns:abs=\"http://example.com/Process/Absenc"
                + "eProcess/AbsenceProcess\" xmlns:otms=\"http://unitedofoq.com/"
                + "otms\"><soapenv:Header/>"
                + "   <soapenv:Body>"
                + "      <abs:startProcessRequest>"
                + "         <otms:dbid>"+absenceRequest.getDbid()+"</otms:dbid>"
                + "         <otms:startDate>"+absenceRequest.getStartDate()+"</otms:startDate>"
                + "         <otms:endDate>"+absenceRequest.getEndDate()+"</otms:endDate>"
                + "         <otms:period></otms:period>"
                + "         <otms:requestedDate></otms:requestedDate>"
                + "         <otms:notes>"+absenceRequest.getNotes()+"</otms:notes>"
                + "         <otms:cancelled></otms:cancelled>"
                + "         <otms:cancelDate></otms:cancelDate>"
                + "         <otms:posted></otms:posted>"
                + "         <otms:override></otms:override>"
                + "         <otms:replaceWorkingDay></otms:replaceWorkingDay>"
                + "         <otms:penaltyIndex></otms:penaltyIndex>"
                + "         <otms:halfDay></otms:halfDay>"
                + "         <otms:type>"+absenceRequest.getAbsence().getDescription()+"</otms:type>"
                + "         <otms:decisionNumber></otms:decisionNumber>"
                + "         <otms:decisionDate></otms:decisionDate>"
                + "         <otms:entryDate></otms:entryDate>"
                + "         <otms:needPayment></otms:needPayment>"
                + "         <otms:balance></otms:balance>"
                + "         <otms:annualBalance></otms:annualBalance>"
                + "         <otms:postedDays></otms:postedDays>"
                + "         <otms:dtype></otms:dtype>"
                + "         <otms:initialBalance></otms:initialBalance>"
                + "         <otms:currentBalance></otms:currentBalance>"
                + "         <otms:managerName>"+managerName+"</otms:managerName>"
                + "         <otms:employeeName>"+employeeName+"</otms:employeeName>"
                + "         <otms:department>"+department+"</otms:department>"
                + "         <otms:reason>"+desc+"</otms:reason>"
                + "         <otms:userName>"+userName+"</otms:userName>"
                + "      </abs:startProcessRequest>"
                + "   </soapenv:Body>"
                + "</soapenv:Envelope>";
        return output;
    }

}
