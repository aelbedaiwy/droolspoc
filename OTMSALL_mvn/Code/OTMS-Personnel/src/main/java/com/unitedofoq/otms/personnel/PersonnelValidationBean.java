package com.unitedofoq.otms.personnel;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.payroll.PayrollParameter;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.personnel.penalty.EmployeePenalty;
import com.unitedofoq.otms.personnel.penalty.EmployeePenaltyRequest;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.personnel.PersonnelValidationLocal",
beanInterface = PersonnelValidationLocal.class)
public class PersonnelValidationBean implements PersonnelValidationLocal {
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private OEntityManagerRemote oem;
    @Override
    public OFunctionResult AbsenceCancellation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EmployeeAbsenceRequest employeeAbsenceRequest = (EmployeeAbsenceRequest) odm.getData().get(0);

            if (employeeAbsenceRequest.getCancelled() == null || employeeAbsenceRequest.getCancelled().equals("N")) {
                Calendar cal = Calendar.getInstance();
                employeeAbsenceRequest.setCancelled("Y");
                employeeAbsenceRequest.setCancelDate(cal.getTime());
            }
            oem.saveEntity(employeeAbsenceRequest, loggedUser);
            return oFR;

        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;
        }
    }
    @Override
    public OFunctionResult AbsenceEntry(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            String fldExp = odm.getData().get(1).toString();
            Date newVal = (Date) odm.getData().get(2);
            long val = 0;
            EmployeeAbsenceRequest employeeAbsenceRequest = (EmployeeAbsenceRequest) odm.getData().get(0);
            if (employeeAbsenceRequest.getCancelled() == null || employeeAbsenceRequest.getCancelled().equals("N")) {
                if (fldExp.compareTo("startDate") == 0) {
                    val = (employeeAbsenceRequest.getEndDate().getTime() - newVal.getTime()) / (1000 * 60 * 60 * 24);
                } else if (fldExp.compareTo("endDate") == 0) {
                    val = (newVal.getTime() - employeeAbsenceRequest.getStartDate().getTime()) / (1000 * 60 * 60 * 24);
                }
                employeeAbsenceRequest.setPeriod(BigDecimal.valueOf(val + 1));
            } else {
                return oFR;
            }
            return oFR;
        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;
        }
    }

    @Override
    public OFunctionResult PenaltyEntry(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        List<String> conditions = new ArrayList<String>();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EmployeePenaltyRequest employeePenaltyRequest = (EmployeePenaltyRequest) odm.getData().get(0);
            Date previousPenaltyDate = null;
            long employeeDbid = employeePenaltyRequest.getEmployee().getDbid();
            long penaltyDbid = employeePenaltyRequest.getPenalty().getDbid();//employeePenaltyRequest.getPenalty().getDbid();
            List<String> penaltyConditions = new ArrayList<String>();
            penaltyConditions.add("employee.dbid = " + employeeDbid);
            penaltyConditions.add("penalty.dbid = " + penaltyDbid);
            List<EmployeePenalty> employeePenaltys = oem.loadEntityList(
                    EmployeePenalty.class.getSimpleName(),
                    penaltyConditions, null, null, loggedUser);

            if (employeePenaltys.size() >= 1) {
                //Having Header
                EmployeePenalty employeePenalty = employeePenaltys.get(0);
                employeePenalty.setCountNumber(Short.valueOf((short) (employeePenalty.getCountNumber().shortValue() + 1)));
                employeePenalty.setTotal(employeePenalty.getTotal().add(employeePenaltyRequest.getPenaltyValue()));
                employeePenalty.setCurrentMonthOccurance(employeePenalty.getCurrentMonthOccurance().add(BigDecimal.ONE));
                previousPenaltyDate = employeePenalty.getLastPenaltyDate();
                employeePenalty.setLastPenaltyDate(employeePenaltyRequest.getPenaltyDate());
                if (employeePenalty.getPenalty().getRefreshMethod().equalsIgnoreCase("P")
                        && employeePenalty.getRefreshStartFrom() == null) {
                    employeePenalty.setRefreshStartFrom(employeePenaltyRequest.getPenaltyDate());
                }
                oem.saveEntity(employeePenalty, loggedUser);
            } else {
                //Creating Header
                EmployeePenalty employeePenalty = new EmployeePenalty();
                employeePenalty.setEmployee(employeePenaltyRequest.getEmployee());
                employeePenalty.setPenalty(employeePenaltyRequest.getPenalty());
                short penCount;
                if (employeePenalty.getCountNumber() == null) {
                    penCount = 0;
                } else {
                    penCount = employeePenalty.getCountNumber().shortValue();
                }

                employeePenalty.setCountNumber(Short.valueOf((short) (penCount + 1)));

                employeePenalty.setTotal(employeePenaltyRequest.getPenaltyValue());
                employeePenalty.setCurrentMonthOccurance(BigDecimal.ONE);
                employeePenalty.setLastPenaltyDate(employeePenaltyRequest.getPenaltyDate());

                if (employeePenalty.getPenalty().getRefreshMethod().equalsIgnoreCase("H")) // Hiring Date
                {
                    employeePenalty.setRefreshStartFrom(employeePenalty.getEmployee().getHiringDate());
                } else if (employeePenalty.getPenalty().getRefreshMethod().equalsIgnoreCase("P")) // First Penalty Date
                {
                    employeePenalty.setRefreshStartFrom(employeePenaltyRequest.getPenaltyDate());
                } else if (employeePenalty.getPenalty().getRefreshMethod().equalsIgnoreCase("L")) // Last Penalty Date
                {
                    employeePenalty.setRefreshStartFrom(employeePenaltyRequest.getPenaltyDate());
                } else if (employeePenalty.getPenalty().getRefreshMethod().equalsIgnoreCase("C")) // Cut Off
                {
                    String calDay, nextMonthDay;
                    String tempDate;
                    String tempMonth;
                    String tempYear;
                    String tempDay;
                    Date date1, date2, penDate;
                    Integer day, month, year;
                    DateFormat dateFormat;
                    penDate = employeePenaltyRequest.getPenaltyDate();
                    conditions.add(" description = 'day_of_calculation' ");
                    conditions.add(" company.dbid = " + employeePenalty.getPenalty().getCompany().getDbid());
                    List<PayrollParameter> payrollParameters = oem.loadEntityList(PayrollParameter.class.getSimpleName(), conditions, null, null, loggedUser);
                    if (payrollParameters.size() > 0) {
                        calDay = payrollParameters.get(0).getValueData();
                        nextMonthDay = payrollParameters.get(0).getAuxiliary();
                    } else {
                        oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
                        return oFR;
                    }
                    if (calDay == null || new Integer(calDay) <= 0) {
                        calDay = "01";
                    }
                    dateFormat = new SimpleDateFormat("MM-yyyy");
                    tempDate = dateFormat.format(employeePenaltyRequest.getPenaltyDate());
                    day = new Integer(calDay) + 1;
                    tempDay = day.toString();
                    tempDate = tempDay.concat("-").concat(tempDate);
                    dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    date1 = dateFormat.parse(tempDate);
                    dateFormat = new SimpleDateFormat("MM");
                    tempMonth = dateFormat.format(penDate);
                    dateFormat = new SimpleDateFormat("yyyy");
                    tempYear = dateFormat.format(penDate);
                    if ((new Integer(tempMonth) + 1) > 12) {
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        day = new Integer(nextMonthDay) + 1;
                        tempDay = day.toString();
                        year = new Integer(tempYear) + 1;
                        tempDate = tempDay.concat("-01").concat(year.toString());
                        date2 = dateFormat.parse(tempDate);
                    } else {
                        day = new Integer(calDay) + 1;
                        month = new Integer(tempMonth) + 1;
                        year = new Integer(tempYear);
                        tempDate = day.toString().concat("-").concat(month.toString()).concat("-").concat(year.toString());
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        date2 = dateFormat.parse(tempDate);
                    }
                    if ((penDate.equals(date1)) || (penDate.equals(date2))) {
                        employeePenalty.setRefreshStartFrom(date1);
                    } else if ((penDate.after(date1)) && (penDate.before(date2))) {
                        employeePenalty.setRefreshStartFrom(date1);
                    } else {
                        dateFormat = new SimpleDateFormat("MM");
                        tempMonth = dateFormat.format(penDate);
                        month = new Integer(tempMonth);
                        day = new Integer(calDay) + 1;
                        dateFormat = new SimpleDateFormat("yyyy");
                        tempYear = dateFormat.format(penDate);
                        if (month == 1) {
                            month = 12;
                            year = new Integer(tempYear) - 1;
                            tempDate = day.toString().concat("-").concat(month.toString()).concat("-").concat(year.toString());
                            dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            date1 = dateFormat.parse(tempDate);
                        } else {
                            month = month - 1;
                            year = new Integer(tempYear);
                            tempDate = day.toString().concat("-").concat(month.toString()).concat("-").concat(year.toString());
                            dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            date1 = dateFormat.parse(tempDate);
                        }
                    }
                    employeePenalty.setRefreshStartFrom(date1);
                }
                oem.saveEntity(employeePenalty, loggedUser);
            }
            employeePenaltyRequest.setPreviousPenaltyDate(previousPenaltyDate);
            oem.saveEntity(employeePenaltyRequest, loggedUser);

            oFR.setReturnedDataMessage(odm);
            return oFR;

        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;

        }
    }
    @Override
    public OFunctionResult PenaltyCancellation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EmployeePenaltyRequest employeePenaltyRequest = (EmployeePenaltyRequest) odm.getData().get(0);


            if (employeePenaltyRequest.getCancelled() == null || employeePenaltyRequest.getCancelled().equals("N")) {
                Calendar cal = Calendar.getInstance();
                employeePenaltyRequest.setCancelled("Y");
                employeePenaltyRequest.setCancelDate(cal.getTime());
                oem.saveEntity(employeePenaltyRequest, loggedUser);
                long employeeDbid = employeePenaltyRequest.getEmployee().getDbid();
                long penaltyDbid = employeePenaltyRequest.getPenalty().getDbid();

                List<String> penaltyConditions = new ArrayList<String>();
                penaltyConditions.add("employee.dbid = " + employeeDbid);
                penaltyConditions.add("penalty.dbid = " + penaltyDbid);


                List<EmployeePenalty> employeePenaltys = oem.loadEntityList(
                        EmployeePenalty.class.getSimpleName(),
                        penaltyConditions, null, null, loggedUser);

                if (employeePenaltys.size() >= 1) {

                    EmployeePenalty employeePenalty = employeePenaltys.get(0);
                    employeePenalty.setCountNumber(Short.valueOf((short) (employeePenalty.getCountNumber().shortValue() - 1)));
                    employeePenalty.setTotal(employeePenalty.getTotal().subtract(employeePenaltyRequest.getPenaltyValue()));
                    employeePenalty.setCurrentMonthOccurance(employeePenalty.getCurrentMonthOccurance().subtract(BigDecimal.ONE));
                    employeePenalty.setLastPenaltyDate(employeePenaltyRequest.getPenaltyDate());
                    oem.saveEntity(employeePenalty, loggedUser);
                }
                oFR.addSuccess(userMessageServiceRemote.getUserMessage("PenaltyCancellation001", loggedUser));
            } else {
                oFR.addError(userMessageServiceRemote.getUserMessage("PenaltyCancellation002", loggedUser));
            }

            return oFR;

        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;

        }
    }

    @Override
    public OFunctionResult GetPenaltyValue(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EmployeePenaltyRequest employeePenaltyRequest = (EmployeePenaltyRequest) odm.getData().get(0);

            Object newVal = odm.getData().get(2);
            Penalty penalty = (Penalty) newVal;
            employeePenaltyRequest.setPenaltyValue(penalty.getDefaultValue());
            oFR.setReturnedDataMessage(odm);
            return oFR;

        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", systemUser));
            return oFR;

        }
    }
}