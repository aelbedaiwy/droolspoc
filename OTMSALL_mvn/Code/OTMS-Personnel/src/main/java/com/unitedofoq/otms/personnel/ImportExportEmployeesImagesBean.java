package com.unitedofoq.otms.personnel;

import com.unitedofoq.fabs.core.datatype.DataTypeServiceRemote;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.encryption.DataEncryptorServiceLocal;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.payroll.PayrollParameter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Mohamed Aboelnour
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.personnel.ImportExportEmployeesImagesBeanLocal",
        beanInterface = ImportExportEmployeesImagesBeanLocal.class)
public class ImportExportEmployeesImagesBean implements ImportExportEmployeesImagesBeanLocal {

    @EJB
    DataEncryptorServiceLocal dataEncryptorService;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private DataTypeServiceRemote dataTypeService;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    
    private static final String OUTPUT_FOLDER = "EmployeeImages";
    
    @Override
    public OFunctionResult Import(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
        // 1- Connecting to Database and select data from DBATTACHMENT
        String sqlStatement = "SELECT NAME,ATTACHMENT FROM DBATTACHMENT WHERE NAME = 'EmployeesImages.zip'";
        List<Object[]> data = (List<Object[]>)oem.executeEntityListNativeQuery(sqlStatement, loggedUser);
        
        File ZIPFile = null;
        InputStream is = null;
        OutputStream os = null;
        byte[] content = new byte[1024];
        int size = 0;
        for(int i=0; i<data.size();i++) {
            is = new ByteArrayInputStream((byte[]) data.get(i)[1]);
            ZIPFile = new File(data.get(i)[0].toString());
            ZIPFile.createNewFile();
            os = new FileOutputStream(ZIPFile);
            while((size = is.read(content)) != -1){
                os.write(content, 0, size);
            }
        }
        
//        ImportExportEmployeesImagesBean unZip = new ImportExportEmployeesImagesBean();
//    	unZip.unZipIt(ZIPFile.getAbsolutePath(),OUTPUT_FOLDER, loggedUser);
        
        byte[] buffer = new byte[1024];
        String zipFile = ZIPFile.getAbsolutePath();
        
        //create output directory is not exists
    	File folder = new File(OUTPUT_FOLDER);
    	if(!folder.exists()){
            folder.mkdir();
    	}

    	//get the zip file content
    	ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
    	//get the zipped file list entry
    	ZipEntry ze = zis.getNextEntry();
    	while(ze!=null){
            
    	   String fileName = ze.getName();
           File newFile = new File(OUTPUT_FOLDER + File.separator + fileName);        

           System.out.println("file unzip : "+ newFile.getAbsoluteFile());

            //create all non exists folders
            //else you will hit FileNotFoundException for compressed folder
            new File(newFile.getParent()).mkdirs();

            FileOutputStream fos = new FileOutputStream(newFile);

            int len;
            while ((len = zis.read(buffer)) > 0) {
       		fos.write(buffer, 0, len);
            }

           FileInputStream fin = new FileInputStream(newFile);  
           String[] employeeDBID = fileName.split(".jpg");
           String empID = employeeDBID[0];
           String sql = "SELECT DBID FROM OEMPLOYEE WHERE CODE = '"+empID+"'";
           Object obj = oem.executeEntityNativeQuery(sql, loggedUser);
           if(obj.toString() != null) {
                String sqlStat = "SELECT DBID FROM EMPLOYEEPHOTO WHERE EMPLOYEE_DBID = '"+obj.toString()+"'";
                Object obj2 = oem.executeEntityNativeQuery(sqlStat, loggedUser);
                String MaxDBID = "SELECT MAX(DBID)+1 FROM EMPLOYEEPHOTO";
                Object objMaxDBID = oem.executeEntityNativeQuery(MaxDBID, loggedUser);
                Long DBID = null;
                if (objMaxDBID != null) {
                    DBID = Long.parseLong(objMaxDBID.toString());
                }
               DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));            
               Connection con = datasource.getConnection();
                if (obj2 != null) {
                    PreparedStatement pre = con.prepareStatement("UPDATE EMPLOYEEPHOTO SET PHOTODATA = ? WHERE EMPLOYEE_DBID = ?");
                    pre.setBinaryStream(1,(InputStream)fin,(int)newFile.length());
                    pre.setString(2, obj.toString());
                    pre.executeUpdate();
                } else {
                    PreparedStatement pre = con.prepareStatement("INSERT INTO EMPLOYEEPHOTO (DBID, NAME,LASTMAINTUSER_DBID, EMPLOYEE_DBID, PHOTODATA) VALUES (?,?,?,?,?)");
                    pre.setLong(1, DBID);
                    pre.setString(2, newFile.getName());
                    pre.setLong(3, loggedUser.getDbid());
                    pre.setString(4, obj.toString());
                    pre.setBinaryStream(5,(InputStream)fin,(int)newFile.length());
                    pre.executeUpdate();
                }
                
            }
            
            fos.close();
            ze = zis.getNextEntry();
            
    	}

        zis.closeEntry();
    	zis.close();
        //Deleteing ZIP file and folder used.
        new File("EmployeesImages.zip").delete();
        deleteDir(new File("EmployeeImages"));
        String sql1 = "DELETE FROM ENTITYATTACHMENTS WHERE ATTACHMENTID = (SELECT DBID FROM DBATTACHMENT WHERE NAME = 'EmployeesImages.zip')";
        oem.executeEntityUpdateNativeQuery(sql1, loggedUser);
        String sql2 = "DELETE FROM DBATTACHMENT WHERE NAME = 'EmployeesImages.zip'";
        oem.executeEntityUpdateNativeQuery(sql2, loggedUser);
    	System.out.println("Done");
        } catch (UserNotAuthorizedException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EntityExistsException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EntityNotFoundException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NonUniqueResultException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoResultException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OptimisticLockException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RollbackException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransactionRequiredException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NamingException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        ofr.addSuccess(usrMsgService.getUserMessage("Successfully imported all employees images.", loggedUser));
        return ofr;
    }
    
    @Override
    public OFunctionResult Export(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) throws FileNotFoundException, IOException {
        OFunctionResult ofr = new OFunctionResult();
        try {
            // 1- Delete old Zip file if exists
            File oldZIP = new File("EmployeesImages.zip");
            if(oldZIP.exists()) {
                new File("EmployeesImages.zip").delete();
            }
            
            // 2- Connecting to Database and select data from EMPLOYEEPHOTO
            String sqlStatement = "SELECT CODE,PHOTODATA FROM EMPLOYEEPHOTO JOIN OEMPLOYEE ON EMPLOYEEPHOTO.EMPLOYEE_DBID = OEMPLOYEE.DBID";
            List<Object[]> data = (List<Object[]>)oem.executeEntityListNativeQuery(sqlStatement, loggedUser);
            
            // 3- Creating folder and add images to it.
            File f;
            new File("EmployeesImages").mkdir();
            InputStream is = null;
            OutputStream os = null;
            byte[] content = new byte[1024];
            int size = 0;  
            for(int i=0; i<data.size();i++) {
                is = new ByteArrayInputStream((byte[]) data.get(i)[1]);
                f = new File("EmployeesImages/"+data.get(i)[0].toString()+".jpg");
                f.createNewFile();
                os = new FileOutputStream(f);
                while((size = is.read(content)) != -1){
                    os.write(content, 0, size);
                }
            }
            
            // 4- Creating ZIP file.
            File srcFile = new File("EmployeesImages");
            File[] files = srcFile.listFiles();
            FileOutputStream fos = new FileOutputStream("EmployeesImages.zip");
            ZipOutputStream zos = new ZipOutputStream(fos);
            
            for (int i = 0; i < files.length; i++) {
                byte[] buffer = new byte[1024];
                FileInputStream fis = new FileInputStream(files[i]);
                zos.putNextEntry(new ZipEntry(files[i].getName()));
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            
            // 5- Delete images folder.
            deleteDir(new File("EmployeesImages"));
            
        } catch (UserNotAuthorizedException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EntityExistsException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EntityNotFoundException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NonUniqueResultException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoResultException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OptimisticLockException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RollbackException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransactionRequiredException ex) {
            Logger.getLogger(ImportExportEmployeesImagesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        ofr.addReturnValue("EmployeesImages.zip");
        return ofr;
    }
    
    @Override
    public OFunctionResult initializePayrollParameter(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            PayrollParameter payrollParameter;
            String sql = "select dbid from payrollparameter where description ='ImportEmployeesImages'";
            Object obj = oem.executeEntityNativeQuery(sql, loggedUser);
            payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), Collections.singletonList("dbid = " + obj), null, loggedUser);
            ODataMessage dm = new ODataMessage(dataTypeService.loadODataType("SingleObjectValue", loggedUser), Collections.singletonList((Object) payrollParameter));
            ofr.setReturnedDataMessage(dm);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }
    
    private static boolean deleteDir(File dir) {
      if (dir.isDirectory()) {
         String[] children = dir.list();
         for (int i = 0; i < children.length; i++) {
            deleteDir (new File(dir, children[i])); 
         }
      }
      return dir.delete();
   }
}
