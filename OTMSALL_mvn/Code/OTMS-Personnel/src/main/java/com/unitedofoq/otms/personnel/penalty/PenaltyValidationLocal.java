package com.unitedofoq.otms.personnel.penalty;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface PenaltyValidationLocal {
    public OFunctionResult validatePenaltyDate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    
}
