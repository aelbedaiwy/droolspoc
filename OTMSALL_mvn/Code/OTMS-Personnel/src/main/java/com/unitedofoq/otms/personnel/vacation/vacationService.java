package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.job.Job;
import com.unitedofoq.otms.foundation.position.Position;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.costcenter.CostCenter;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryElement;
import com.unitedofoq.otms.payroll.paygrade.PayGrade;
import com.unitedofoq.otms.personnel.absence.Absence;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.personnel.dayoff.DayOff;
import com.unitedofoq.otms.personnel.dayoff.EmployeeDayOffRequest;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.timemanagement.employee.EmployeeAnnualPlanner;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendance;
import com.unitedofoq.otms.timemanagement.employee.EmployeeShift;
import com.unitedofoq.otms.timemanagement.employee.EmployeeTimeSheet;
import com.unitedofoq.otms.timemanagement.employee.EmployeeWorkingCalHistory;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.ejb.EntityUtilities;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import com.unitedofoq.otms.utilities.filter.FilterUtility;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.joda.time.DateTime;
import org.joda.time.Days;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.personnel.vacation.vacationServiceLocal", beanInterface = vacationServiceLocal.class)
public class vacationService implements vacationServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote userMessageService;
    @EJB
    private vacationServiceLocal vacationService;
    @EJB
    private WSUtilityLocal webServiceUtility;
    @EJB
    private DataSecurityServiceLocal dataSecurityServiceLocal;

    private DateTimeUtility dateTimeUtility = new DateTimeUtility();

    @Override
    public OFunctionResult validateEmployeeVacationRequest(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest vac = (EmployeeVacationRequest) odm
                    .getData().get(0);
            vac.setActualPeriod(vac.getPeriod());

            if (vac.getStartDate() == null || vac.getEndDate() == null) {
                ofr.setReturnedDataMessage(odm);
                return ofr;
            }
            // Validate on Period Null Or Not
            if (vac.getPeriod() == null) {
                ofr.addError(userMessageService.getUserMessage(
                        "VacationEntry001", loggedUser));
                return ofr;
            }
            if (vac.getPeriod().doubleValue() <= 0) {
                ofr.addError(userMessageService.getUserMessage(
                        "VacationEntry001", loggedUser));
                return ofr;
            }
            // Validate on Balance Null Or Not
            if (vac.getBalance() == null) {
                ofr.addError(userMessageService.getUserMessage(
                        "VacationEntry002", loggedUser));
                return ofr;
            }
            if ((vac.getBalance().doubleValue() <= 0)
                    && vac.getOverride().equals("N")) {
                ofr.addError(userMessageService.getUserMessage(
                        "VacationEntry002", loggedUser));
                return ofr;
            }
            if ((vac.getBalance().doubleValue() < vac.getPeriod().doubleValue())
                    && vac.getOverride().equals("N")) {
                ofr.addError(userMessageService.getUserMessage(
                        "VacationEntry003", loggedUser));
                return ofr;
            }
            // CALL VACATION Validation OPERATION
            List vacValidationdata = new ArrayList();
            DateFormat dateFormat;
            String endDate;
            if (vac.getVacation().getUnitOfMeasure().equalsIgnoreCase("h")) {
                dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                endDate = dateFormat.format(vac.getEndDate());
            } else {
                dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                vac.setStartDate(dateFormat.parse(dateFormat.format(vac
                        .getStartDate())));
                endDate = dateFormat.format(vac.getEndDate()).concat(
                        " 23:59:59");
                dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                vac.setEndDate(dateFormat.parse(endDate));
                dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            }
            vacValidationdata.add(vac.getEmployee().getDbid());
            vacValidationdata.add(vac.getVacation().getDbid());
            vacValidationdata.add(dateFormat.format(vac.getStartDate()));
            vacValidationdata.add(endDate);
            vacValidationdata.add(vac.getPeriod());

            String webServiceCode = "VV_f";
            String result = webServiceUtility.callingWebService(webServiceCode,
                    vacValidationdata, loggedUser);
            if (result == null || result.compareTo(" ") == 0) {
            } else {
                UserMessage msg = new UserMessage();
                msg.setMessageText(result);
                msg.setMessageTitle("Validation Error");
                msg.setName("Validation Error");
                msg.setMessageTextTranslated(result);
                msg.setMessageType(UserMessage.TYPE_ERROR);
                ofr.addError(msg);
            }
        } catch (Exception ee) {
            OLog.logException(ee, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult UpdateEmployeeVacationHeader(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDayOffRequest employeeDayOffRequest = (EmployeeDayOffRequest) odm
                    .getData().get(0);

            if (employeeDayOffRequest.getStartDate() == null
                    || employeeDayOffRequest.getEndDate() == null) {
                ofr.setReturnedDataMessage(odm);
                return ofr;
            }
            if (employeeDayOffRequest.getEntryDate() == null) {
                employeeDayOffRequest.setEntryDate(employeeDayOffRequest
                        .getRequestedDate());
            }

            // CALL VACATION Header Creation OPERATION
            List empVacHeaderdata = new ArrayList();
            DateFormat dateFormat;
            DayOff dayOff = employeeDayOffRequest.getDayOff();
            if (dayOff.getUnitOfMeasure().equalsIgnoreCase("h")) {
                dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            } else {
                dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            }

            empVacHeaderdata.add(employeeDayOffRequest.getEmployee().getDbid());
            empVacHeaderdata.add(employeeDayOffRequest.getDayOff().getDbid());

            empVacHeaderdata.add(dateFormat.format(employeeDayOffRequest
                    .getStartDate()));
            empVacHeaderdata.add(dateFormat.format(employeeDayOffRequest
                    .getEndDate()));

            String webServiceCode = "UEVH";
            webServiceUtility.callingWebService(webServiceCode,
                    empVacHeaderdata, loggedUser);
            // TODO: Log WS Result & return Error
        } catch (Exception ee) {
            OLog.logException(ee, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult cancelVacation(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest employeeVacationRequest = (EmployeeVacationRequest) odm
                    .getData().get(0);
            if (employeeVacationRequest.getCancelDate() == null) {
                employeeVacationRequest.setCancelDate(new Date());
            }
            // _________________________________________________________
            if (employeeVacationRequest.getCancelled().equals("Y")) {
                ofr.addError(userMessageService.getUserMessage(
                        "cancelVacation001", loggedUser));
                return ofr;
            }
            // CALL VACATION Cancel OPERATION
            List CancelVacdata = new ArrayList();
            CancelVacdata.add(employeeVacationRequest.getDbid());
            String webServiceCode = "CV_1";
            webServiceUtility.callingWebService(webServiceCode, CancelVacdata,
                    loggedUser);

            employeeVacationRequest.setCancelled("Y");
            employeeVacationRequest.setStatus("C");
            employeeVacationRequest.setDecisionDate(employeeVacationRequest
                    .getCancelDate());
            employeeVacationRequest
                    .setDecisionApprovedBy(employeeVacationRequest
                            .getCancelledBy());
            oem.saveEntity(employeeVacationRequest, loggedUser);

            // <desc="Effect on Time Management">
            ofr.append(cancelVacationEffectOnTM(employeeVacationRequest, loggedUser));
            // </editor-fold>

            ofr.addSuccess(userMessageService.getUserMessage(
                    "cancelVacation002", loggedUser));
        } catch (Exception ee) {
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return ofr;
    }

    /**
     * This method first cancels the old vacation request and then inserts a new
     * one with the modified dates
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult modifyVacation(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        // Cancel Current Vacation & Generate New One
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest employeeVacationRequest = (EmployeeVacationRequest) odm.getData().get(0);
            ofr.append(vacationService.cancelRequest(odm, functionParms, loggedUser));
            if (ofr.getErrors().size() > 0) {
                return ofr;
            }
            EmployeeVacationRequest newVacationRequest = setEmployeeVacationRequestData(employeeVacationRequest);
            //_______________________________________________
            ofr.append(vacationService.addVacationRequest(newVacationRequest, loggedUser));
            //Fetch Error Message If Exist
            if (ofr.getErrors().size() > 0) {
                //Should Make RollBack To The Cancellation
            } else {
                employeeVacationRequest.setDbid(newVacationRequest.getDbid());
                ofr.addSuccess(userMessageService.getUserMessage("modifyVacation002", loggedUser));
            }
        } catch (Exception ee) {
            OLog.logException(ee, loggedUser);
            ofr.addError(userMessageService.getUserMessage("SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult UpdateVacationDetail(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest employeeVacationRequest = (EmployeeVacationRequest) odm
                    .getData().get(0);
            // Parameters
            List vacDetaildata = new ArrayList();
            vacDetaildata.add(employeeVacationRequest.getDbid());
            String webServiceCode = "UVD_001";
            webServiceUtility.callingWebService(webServiceCode, vacDetaildata,
                    loggedUser);
        } catch (Exception ee) {
            OLog.logException(ee, loggedUser);
        }
        return ofr;
    }

    @Override
    public OFunctionResult validateHajMaternityLeaveVacation(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        EmployeeVacationRequest employeeVacation = (EmployeeVacationRequest) odm
                .getData().get(0);
        try {
            Vacation vacation = employeeVacation.getVacation();
            String internalCode = vacation.getInternalCode();
            Employee emp = (Employee) oem.loadEntity(Employee.class
                    .getSimpleName(), employeeVacation.getEmployee().getDbid(),
                    null, null, loggedUser);
            // HAJJ
            if (internalCode != null && internalCode.equals("HAJJ")) {
                UDC religion = emp.getReligion();
                if (religion == null) {
                    oFR.addError(userMessageService.getUserMessage(
                            "MissingReligion", loggedUser));
                    return oFR;
                }
                if (!religion.getCode().equals("MUS")) {
                    oFR.addError(userMessageService.getUserMessage(
                            "OnlyMuslimsHaj", loggedUser));
                    return oFR;
                }

                // MATERNITY
            } else if (internalCode != null && internalCode.equals("MAT")) {
                UDC gender = emp.getGender();
                if (gender == null) {
                    oFR.addError(userMessageService.getUserMessage(
                            "MissingGender", loggedUser));
                    return oFR;
                }
                if (!gender.getCode().equals("Female")) {
                    oFR.addError(userMessageService.getUserMessage(
                            "OnlyFemalesMat", loggedUser));
                    return oFR;
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        } finally {
            return oFR;
        }
    }

    @Override
    public OFunctionResult calcActualBalance(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        EmployeeDayOffRequest employeeDayOff = (EmployeeDayOffRequest) odm
                .getData().get(0);

        try {
            if (employeeDayOff.getBalance() != null
                    && employeeDayOff.getActualPeriod() != null) {
                employeeDayOff.setActualBalance(employeeDayOff.getBalance()
                        .subtract(employeeDayOff.getActualPeriod()));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        }
        oFR.setReturnedDataMessage(odm);
        return oFR;

    }

    /**
     * This method is the one called in case of modification. It is a new
     * transaction so that the new vacation entered in modification can be
     * added.
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult cancelRequest(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest employeeVacationRequest = (EmployeeVacationRequest) odm
                    .getData().get(0);
            // Load The Vacation Request & Make It As Cancel.
            EmployeeVacationRequest cancelledRequest = (EmployeeVacationRequest) oem
                    .loadEntity(EmployeeVacationRequest.class.getSimpleName(),
                            employeeVacationRequest.getDbid(), null, null,
                            loggedUser);
            if (cancelledRequest == null) {
                ofr.addError(userMessageService.getUserMessage(
                        "modifyVacation001", loggedUser));
                return ofr;
            }
            if (!(cancelledRequest.getCancelled().equals("Y"))) {
                // Cancel Vacation
                // CALL VACATION Cancel OPERATION
                List cancelVacdata = new ArrayList();
                cancelVacdata.add(employeeVacationRequest.getDbid());
                String webServiceCode = "CV_1";
                webServiceUtility.callingWebService(webServiceCode,
                        cancelVacdata, loggedUser);

                cancelledRequest.setCancelled("Y");
                cancelledRequest.setCancelDate(new Date());
                cancelledRequest.setStatus("C");
                cancelledRequest.setDecisionDate(employeeVacationRequest
                        .getCancelDate());
                cancelledRequest.setDecisionApprovedBy(employeeVacationRequest
                        .getCancelledBy());
                oem.saveEntity(cancelledRequest, loggedUser);
                ofr.append(cancelVacationEffectOnTM(cancelledRequest, loggedUser));
                // </editor-fold>
                ofr.addReturnValue(cancelledRequest);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult importEmpVacReq(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String sqlStatement;
            sqlStatement = "insert into employeevacationhistory(employee_dbid,vacation_dbid,startdate,enddate,active,deleted,period) "
                    + " SELECT  oemployee.dbid,dayoff.dbid,importemployeevacation.startdate,importemployeevacation.enddate,1,0 "
                    + " ,importemployeevacation.vacPeriod"
                    + " FROM importemployeevacation ,dayoff,oemployee "
                    + " where importemployeevacation.empcode = Oemployee.code AND "
                    + " importemployeevacation.vacCode = dayoff.code ";
            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            if (!employeeDbidList.isEmpty()) {
                String employeeDbids = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                String securityCond = " AND oemployee.dbid in " + employeeDbids;
                sqlStatement += securityCond;
            }
            boolean saveflag = oem.executeEntityUpdateNativeQuery(sqlStatement,
                    loggedUser);
            if (saveflag) {
                ofr.addSuccess(userMessageService.getUserMessage(
                        "Date Impoted Successfully", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    private EmployeeVacationRequest setEmployeeVacationRequestData(
            EmployeeVacationRequest employeeVacationRequest) {
        EmployeeVacationRequest newVacationRequest = new EmployeeVacationRequest();
        newVacationRequest.setEmployee(employeeVacationRequest.getEmployee());
        newVacationRequest.setVacation(employeeVacationRequest.getVacation());
        newVacationRequest.setStartDate(employeeVacationRequest.getStartDate());
        newVacationRequest.setEndDate(employeeVacationRequest.getEndDate());
        newVacationRequest.setRequestedDate(employeeVacationRequest
                .getRequestedDate());
        newVacationRequest.setBalance(employeeVacationRequest.getBalance());
        newVacationRequest.setPeriod(employeeVacationRequest.getPeriod());
        newVacationRequest.setPosted(employeeVacationRequest.getPosted());
        newVacationRequest.setPostedDays(employeeVacationRequest
                .getPostedDays());
        newVacationRequest.setOverride(employeeVacationRequest.getOverride());
        newVacationRequest.setDecisionDate(employeeVacationRequest
                .getDecisionDate());
        newVacationRequest.setDecisionNumber(employeeVacationRequest
                .getDecisionNumber());
        newVacationRequest.setDecisionApprovedBy(employeeVacationRequest
                .getDecisionApprovedBy());
        newVacationRequest.setNotes(employeeVacationRequest.getNotes());
        newVacationRequest.setCancelled("N");
        newVacationRequest.setTicket(employeeVacationRequest.getTicket());
        newVacationRequest.setVisaRequest(employeeVacationRequest
                .getVisaRequest());
        newVacationRequest.setExits(employeeVacationRequest.getExits());
        return newVacationRequest;
    }

    /**
     * This method makes the effect of canceling a vacation on the TM; daily
     * attendance, annual planner & TM sheet (if exists)
     *
     * @param cancelledRequest
     * @param loggedUser
     * @return
     */
    private OFunctionResult cancelVacationEffectOnTM(EmployeeVacationRequest cancelledRequest, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            DateTimeUtility dtUtility = new DateTimeUtility();
            String startYear = dtUtility.YEAR_FORMAT.format(cancelledRequest.getStartDate());
            String endYear = dtUtility.YEAR_FORMAT.format(cancelledRequest.getEndDate());

            UDC absenceDayNature = null, holidayDayNature = null, weekendDayNature = null, workingDayNature = null, missionDayNature = null,
                    workOnWeekendDayNature = null, workOnHolidayDayNature;
            EmployeeShift employeeShift;

            List<String> udcConditions = new ArrayList<String>();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Absence'");
            absenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(),
                    udcConditions, null, loggedUser);

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Holiday'");
            try {
                holidayDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception e) {
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='WeekEnd'");
            try {
                weekendDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception e) {
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Working'");
            try {
                workingDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception e) {
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='WorkingOnWeekEnd'");
            try {
                workOnWeekendDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception e) {
            }

            if (endYear.equals(startYear)) {
                List<String> conditions = new ArrayList<String>();
                conditions.add("employee.dbid = " + cancelledRequest.getEmployee().getDbid());
                conditions.add("plannerYear = " + startYear);
                EmployeeAnnualPlanner annualPlanner = (EmployeeAnnualPlanner) oem
                        .loadEntity(
                                EmployeeAnnualPlanner.class.getSimpleName(),
                                conditions, null, loggedUser);
                EmployeeTimeSheet timeSheet = (EmployeeTimeSheet) oem
                        .loadEntity(EmployeeTimeSheet.class.getSimpleName(),
                                conditions, null, loggedUser);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(cancelledRequest.getStartDate());

                EmployeeDailyAttendance dailyAttendance;
                List<String> empDAConditions;

                employeeShift = (EmployeeShift) oem.loadEntity(
                        EmployeeShift.class.getSimpleName(), conditions, null, loggedUser);

                if (employeeShift == null) {
                    employeeShift = new EmployeeShift();
                    employeeShift.setEmployee(cancelledRequest.getEmployee());
                    employeeShift.setPlannerYear(startYear.toString());
                    oem.saveEntity(employeeShift, loggedUser);
                }

                // in case of cancelling a vacation of type hours, then just remove the exception and recalc
                if (cancelledRequest.getVacation().getUnitOfMeasure()
                        .equals("H")) {

                    Date missionDay = calendar.getTime();
                    TMCalendar tmCalendar = getEmpCalendar(cancelledRequest.getStartDate(), cancelledRequest.getEmployee(), loggedUser);
                    NormalWorkingCalendar workingCal = null;
                    if (tmCalendar.getType().getCode().equals("Rotation")) {
                        Object[] returned = getShift(tmCalendar, cancelledRequest.getStartDate(), loggedUser);
                        if (returned[1] != null) {
                            workingCal = (NormalWorkingCalendar) returned[1];
                        }
                    } else if (tmCalendar.getType().getCode().equals("Flexible")
                            || tmCalendar.getType().getCode().equals("Regular")) {
                        conditions.clear();
                        conditions.add("calendar.dbid = " + tmCalendar.getDbid());
                        workingCal = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), conditions, null, loggedUser);
                    }
                    if (workingCal != null) {
                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
                        if (dateTimeUtility.compareTime(timeFormat.format(cancelledRequest.getStartDate()), workingCal.getStartDayTime()) < 0) {
                            missionDay = dateTimeUtility.addValueToDate(cancelledRequest.getStartDate(), -1, DateTimeUtility.DateFieldType.DAY);
                        }
                    }

                    empDAConditions = new ArrayList<String>();
                    empDAConditions.add("employee.dbid = "
                            + cancelledRequest.getEmployee().getDbid());
                    empDAConditions.add("dailyDate = '"
                            + dtUtility.DATE_FORMAT.format(missionDay) + "'");
                    empDAConditions.add("header = true");

                    dailyAttendance = (EmployeeDailyAttendance) oem.loadEntity(
                            EmployeeDailyAttendance.class.getSimpleName(),
                            empDAConditions, null, loggedUser);

                    if (dailyAttendance != null) {
                        dailyAttendance.setExceptionFrom("");
                        dailyAttendance.setExceptionTo("");
                        dailyAttendance.setExceptionHours(new BigDecimal(0));
                        entitySetupService.callEntityUpdateAction(
                                dailyAttendance, loggedUser);
                    }
                } // in case of cancelling a vacation of type days, then the day should return back to its origin
                else {
                    boolean isOffday = false;
                    do {
                        isOffday = false;
                        String month = dtUtility.MONTH_FORMAT.format(calendar.getTime());
                        String day = dtUtility.DAY_FORMAT.format(calendar.getTime());
                        month = Integer.toString(Integer.parseInt(month));
                        day = Integer.toString(Integer.parseInt(day));
                        // daily attendance part

                        empDAConditions = new ArrayList<String>();
                        empDAConditions.add("employee.dbid = "
                                + cancelledRequest.getEmployee()
                                .getDbid());
                        empDAConditions.add("dailyDate = '"
                                + dtUtility.DATE_FORMAT.format(calendar.getTime()) + "'");
                        empDAConditions.add("header = true");

                        dailyAttendance = (EmployeeDailyAttendance) oem
                                .loadEntity(EmployeeDailyAttendance.class
                                        .getSimpleName(), empDAConditions,
                                        null, loggedUser);

                        if (dailyAttendance != null) {

                            Method tempDay = null;
                            try {
                                final String methodName = "getM" + month + "D" + day;
                                tempDay = EmployeeAnnualPlanner.class.getMethod(methodName);
                            } catch (Exception ex) {
                                Logger.getLogger(vacationService.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            Object dayValue = null;
                            try {
                                try {
                                    if (annualPlanner != null) {
                                        dayValue = tempDay.invoke(annualPlanner, null);
                                    }
                                } catch (IllegalAccessException ex) {
                                    Logger.getLogger(vacationService.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } catch (Exception ex) {
                                Logger.getLogger(vacationService.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            String dayVal = (String) dayValue;
                            String selectStat, updateStat;
                            if (dayVal != null && !dayVal.equals("O")
                                    && !dayVal.equals("H")) {
                                // 1 - check it there exists a holiday on the same day
                                String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
                                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                    selectStat = "select count(*) from holidaycalendar "
                                            + " where (datefrom <= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calendar.getTime()))
                                            + " and dateto>= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calendar.getTime())) + ")";
                                } else {
                                    selectStat = "select count(*) from holidaycalendar "
                                            + " where (datefrom <= '" + dateTimeUtility.DATE_FORMAT.format(calendar.getTime())
                                            + "' and dateto>= '" + dateTimeUtility.DATE_FORMAT.format(calendar.getTime()) + "')";
                                }
                                Object result = oem.executeEntityNativeQuery(selectStat, loggedUser);
                                if (result != null && !result.toString().equals("0")) {
                                    dailyAttendance.setDayNature(holidayDayNature);
                                    entitySetupService.callEntityUpdateAction(dailyAttendance, loggedUser);
                                    // planner
                                    updateStat = "update employeeannualplanner set m" + month + "D" + Integer.parseInt(day) + " = 'H'"
                                            + " where  employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                            + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                                    // shift
                                    updateStat = "update employeeshift set m" + month + "D" + Integer.parseInt(day) + " = 'H'"
                                            + " where  employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                            + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                                } // 2 - check if there exists an offday on the same day
                                else {
                                    TMCalendar empCal = getEmpCalendar(calendar.getTime(), cancelledRequest.getEmployee(), loggedUser);
                                    Object[] returned = null;
                                    boolean isFlexible = false;
                                    if (empCal.getType().getCode().equals("Rotation")) {
                                        returned = getShift(empCal, calendar.getTime(), loggedUser);
                                        if (returned[1] != null && ((NormalWorkingCalendar) returned[1]).getNumberOfHours().compareTo(BigDecimal.ZERO) == 0) {
                                            isOffday = true;
                                        }
                                    } else if (empCal.getType().getCode().equals("Flexible")) {
                                        int dayIndex = calendar.getTime().getDay() + 1;
                                        int dayOff1 = 0;
                                        if (empCal.getCalendars().get(0).getDayOff1() != null) {
                                            if (empCal.getCalendars().get(0).getDayOff1().getCode() != null) {
                                                dayOff1 = Integer.parseInt(empCal.getCalendars().get(0).getDayOff1().getCode());
                                            }
                                        }
                                        int dayOff2 = 0;
                                        if (empCal.getCalendars().get(0).getDayOff2() != null) {
                                            if (empCal.getCalendars().get(0).getDayOff2().getCode() != null) {
                                                dayOff2 = Integer.parseInt(empCal.getCalendars().get(0).getDayOff2().getCode());
                                            }
                                        }
                                        if (dayIndex == dayOff1
                                                || dayIndex == dayOff2) {
                                            isOffday = true;
                                            isFlexible = true;
                                        }
                                    }
                                    if (isOffday) {
                                        if (dailyAttendance.getTimeIn() != null && !dailyAttendance.getTimeIn().isEmpty()) {
                                            dailyAttendance.setDayNature(workOnWeekendDayNature);
                                            entitySetupService.callEntityUpdateAction(dailyAttendance, loggedUser);
                                            // planner
                                            updateStat = "update employeeannualplanner set m" + month + "D" + Integer.parseInt(day) + " = 'E'"
                                                    + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                    + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                                            // shift
                                            if (!isFlexible) {
                                                updateStat = "update employeeshift "
                                                        + " set m" + month + "D" + Integer.parseInt(day) + " = '" + ((NormalWorkingCalendar) returned[1]).getCode() + "'"
                                                        + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                        + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                                            }
                                        } else {
                                            dailyAttendance.setDayNature(weekendDayNature);
                                            entitySetupService.callEntityUpdateAction(dailyAttendance, loggedUser);
                                            // planner
                                            updateStat = "update employeeannualplanner set m" + month + "D" + Integer.parseInt(day) + " = 'O'"
                                                    + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                    + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                                            // shift
                                            if (!isFlexible) {
                                                updateStat = "update employeeshift "
                                                        + " set m" + month + "D" + Integer.parseInt(day) + " = '" + ((NormalWorkingCalendar) returned[1]).getCode() + "'"
                                                        + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                        + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                                            }
                                        }
                                    } // 3- case that this day has timeIn, so it will return working
                                    else if (dailyAttendance.getTimeIn() != null && !dailyAttendance.getTimeIn().isEmpty()) {
                                        dailyAttendance.setDayNature(workingDayNature);
                                        entitySetupService.callEntityUpdateAction(dailyAttendance, loggedUser);
                                        // planner
                                        updateStat = "update employeeannualplanner set m" + month + "D" + Integer.parseInt(day) + " = 'W'"
                                                + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                        oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                                        // shift
                                        updateStat = "update employeeshift "
                                                + " set m" + month + "D" + Integer.parseInt(day) + " = '" + ((NormalWorkingCalendar) returned[1]).getCode() + "'"
                                                + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                        oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                                    } // 3 - the final case that it must return to absence
                                    else {
                                        dailyAttendance.setDayNature(absenceDayNature);
                                        entitySetupService.callEntityUpdateAction(dailyAttendance, loggedUser);
                                        TMCalendar empCalendar;
                                        empCalendar = getEmpCalendar(calendar.getTime(), cancelledRequest.getEmployee(), loggedUser);

                                        String dayValShift = "";

                                        NormalWorkingCalendar wc;
                                        if (empCalendar.getType().getCode().equals("Rotation")) {
                                            wc = (NormalWorkingCalendar) getShift(empCalendar, calendar.getTime(), loggedUser)[1];
                                            dayValShift = wc != null ? wc.getCode() : "";
                                            // shift
                                            updateStat = "update employeeshift "
                                                    + " set m" + month + "D" + Integer.parseInt(day) + " = '" + dayValShift + "'"
                                                    + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                    + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                                        }

                                        // planner
                                        updateStat = "update employeeannualplanner set m" + month + "D" + Integer.parseInt(day) + " = 'A'"
                                                + " where employee_dbid = " + cancelledRequest.getEmployee().getDbid()
                                                + " and planneryear = " + dtUtility.YEAR_FORMAT.format(calendar.getTime());
                                        oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                                    }
                                }
                                //oem.saveEntity(dailyAttendance, loggedUser);
                            }

                        }
                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                    } while (calendar.getTime().compareTo(
                            cancelledRequest.getEndDate()) <= 0);
//                    if (annualPlanner != null) {
//                        entitySetupService.callEntityUpdateAction(
//                                annualPlanner, loggedUser);
//                    }
//                    if (employeeShift != null) {
//                        entitySetupService.callEntityUpdateAction(
//                                employeeShift, loggedUser);
//                    }
//                    if (timeSheet != null) {
//                        entitySetupService.callEntityUpdateAction(timeSheet,
//                                loggedUser);
//                    }
                }
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }

    class VacationComparable implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            Date date1, date2;
            SimpleDateFormat dateFormatDays = new SimpleDateFormat("dd-MM-yyyy");
            try {
                date1 = dateFormatDays.parse(((ImportEmployeeVacation) o1)
                        .getStartDate());
                date2 = dateFormatDays.parse(((ImportEmployeeVacation) o2)
                        .getStartDate());
                if (date1.compareTo(date2) == 1) {
                    return 1;
                }
            } catch (ParseException ex) {
                Logger.getLogger(vacationService.class.getName()).log(
                        Level.SEVERE, null, ex);
            }
            return 0;
        }
    };

    @Override
    public OFunctionResult importEmployeeVacation(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult oFR = new OFunctionResult();
        String errors;
        Date fromDate;
        Date toDate;
        Date currentDate;
        String vacationCode;
        String employeeCode;
        List<String> conds;
        DateFormat dateFormat;
        BigDecimal vacationBalance, postedDays, vacPeriod;
        List vacBdata = new ArrayList();
        List vacPOdata = new ArrayList();
        List vacPEdata = new ArrayList();
        SimpleDateFormat dateFormatDays = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat dateFormatHours = new SimpleDateFormat(
                "dd-MM-yyyy HH:mm");
        conds = new ArrayList<String>();

        currentDate = new Date();

        try {
            conds.add("readyToPost = 'R'");
            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            if (!employeeDbidList.isEmpty()) {
                String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                String securityCond = "empCode in ( SELECT CODE FROM oemployee where dbid in " + employeeDbidStr + ")";
                conds.add(securityCond);
            }
            List<ImportEmployeeVacation> impEmpVac = oem.loadEntityList(
                    ImportEmployeeVacation.class.getSimpleName(), conds, null,
                    null, loggedUser);
            Collections.sort(impEmpVac, new VacationComparable());
            conds.clear();
            for (int i = 0; i < impEmpVac.size(); i++) {
                try {
                    ImportEmployeeVacation importEmployeeVacation = impEmpVac
                            .get(i);

                    vacationCode = importEmployeeVacation.getVacCode();
                    employeeCode = importEmployeeVacation.getEmpCode();
                    conds.clear();
                    conds.add("code = '" + employeeCode + "'");
                    List<Employee> employee = oem.loadEntityList("Employee",
                            conds, null, null, loggedUser);

                    conds.clear();

                    conds.add("code ='" + vacationCode + "'");
                    List<DayOff> dayoff = oem.loadEntityList("DayOff", conds,
                            null, null, loggedUser);

                    if (dayoff.get(0).getUnitOfMeasure().equalsIgnoreCase("h")) {
                        fromDate = dateFormatHours.parse(importEmployeeVacation
                                .getStartDate());
                        toDate = dateFormatHours.parse(importEmployeeVacation
                                .getEndDate());
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    } else {
                        fromDate = dateFormatDays.parse(importEmployeeVacation
                                .getStartDate());
                        toDate = dateFormatDays.parse(importEmployeeVacation
                                .getEndDate());
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    }

                    // CALL VACATION BALANCE OPERATION
                    vacBdata.clear();

                    vacBdata.add(employee.get(0).getDbid());
                    vacBdata.add(dayoff.get(0).getDbid());
                    vacBdata.add(dateFormat.format(fromDate));
                    vacBdata.add(dateFormat.format(toDate));

                    String webServiceCode = "CVB_01";
                    String result = webServiceUtility.callingWebService(
                            webServiceCode, vacBdata, loggedUser);

                    vacationBalance = new BigDecimal(result);

                    // CALL VACATION POSTED OPERATION
                    vacPOdata.clear();
                    vacPOdata.add(employee.get(0).getDbid());
                    vacPOdata.add(dayoff.get(0).getDbid());
                    vacPOdata.add(dateFormat.format(fromDate));
                    vacPOdata.add(dateFormat.format(toDate));

                    webServiceCode = "Vposted_01";
                    result = webServiceUtility.callingWebService(
                            webServiceCode, vacPOdata, loggedUser);
                    postedDays = new BigDecimal(result);

                    // CALL VACATION PERIOD OPERATION
                    vacPEdata.clear();
                    vacPEdata.add(employee.get(0).getDbid());
                    vacPEdata.add(dayoff.get(0).getDbid());
                    vacPEdata.add(dateFormat.format(fromDate));
                    vacPEdata.add(dateFormat.format(toDate));
                    vacPEdata
                            .add(dayoff.get(0).getUnitOfMeasure() != null ? dayoff
                                            .get(0).getUnitOfMeasure() : "");

                    webServiceCode = "Vperiod_01";
                    result = webServiceUtility.callingWebService(
                            webServiceCode, vacPEdata, loggedUser);
                    // ___________________________________________
                    vacPeriod = new BigDecimal(result);
                    if ("Vacation".equals(dayoff.get(0).getClassName())) {
                        conds.clear();
                        conds.add("code ='" + vacationCode + "'");
                        List<Vacation> vacation = oem.loadEntityList(
                                "Vacation", conds, null, null, loggedUser);
                        EmployeeVacationRequest empVacReq = new EmployeeVacationRequest();
                        empVacReq.setEmployee(employee.get(0));
                        empVacReq.setDayOff(dayoff.get(0));
                        empVacReq.setVacation(vacation.get(0));
                        empVacReq.setPeriod(vacPeriod);
                        empVacReq.setBalance(vacationBalance);
                        empVacReq.setPostedDays(postedDays);
                        empVacReq.setStartDate(fromDate);
                        empVacReq.setEndDate(toDate);
                        empVacReq.setCancelled("N");
                        empVacReq.setOverride("N");
                        empVacReq.setPosted("N");
                        empVacReq.setTicket("N");
                        empVacReq.setVisaRequest("N");
                        empVacReq.setExits("N");
                        empVacReq.setEntryDate(currentDate);
                        empVacReq.setDecisionDate(currentDate);
                        empVacReq.setRequestedDate(currentDate);
                        errors = vacationService.enterImportedVacations(
                                empVacReq, loggedUser);

                        if (errors.length() > 0) {
                            importEmployeeVacation.setReadyToPost("N");
                            importEmployeeVacation.setComments(errors);
                        } else {
                            importEmployeeVacation.setReadyToPost("P");
                        }
                    } else {
                        conds.clear();
                        conds.add("code ='" + vacationCode + "'");
                        List<Absence> absence = oem.loadEntityList("Absence",
                                conds, null, null, loggedUser);

                        EmployeeAbsenceRequest empAbsReq = new EmployeeAbsenceRequest();
                        empAbsReq.setEmployee(employee.get(0));
                        empAbsReq.setDayOff(dayoff.get(0));
                        empAbsReq.setAbsence(absence.get(0));
                        empAbsReq.setPeriod(vacPeriod);
                        empAbsReq.setBalance(vacationBalance);
                        empAbsReq.setPostedDays(postedDays);
                        empAbsReq.setStartDate(fromDate);
                        empAbsReq.setEndDate(toDate);
                        empAbsReq.setCancelled("N");
                        empAbsReq.setOverride("N");
                        empAbsReq.setPosted("N");
                        empAbsReq.setEntryDate(currentDate);
                        empAbsReq.setDecisionDate(currentDate);
                        empAbsReq.setRequestedDate(currentDate);
                        errors = vacationService.enterImportedVacations(
                                empAbsReq, loggedUser);

                        if (errors.length() > 0) {

                            importEmployeeVacation.setReadyToPost("N");
                            importEmployeeVacation.setComments(errors);
                        } else {
                            importEmployeeVacation.setReadyToPost("P");
                        }
                    }
                    oem.saveEntity(importEmployeeVacation, loggedUser);
                } catch (Exception ex) {
                    OLog.logException(ex, loggedUser);
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return oFR;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public String enterImportedVacations(BaseEntity empVacReq, OUser loggedUser
    ) {
        String errors;
        errors = "";
        try {
            OFunctionResult vacRequestOFR = entitySetupService
                    .callEntityCreateAction(empVacReq, loggedUser);
            if (!vacRequestOFR.getErrors().isEmpty()) {
                errors = vacRequestOFR.getErrors().get(0).getMessageText();
            }
            vacRequestOFR.addReturnValue(empVacReq.getDbid());
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return errors;
    }

    @Override
    public OFunctionResult validateVacReqOnSetup(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest vacRequest = (EmployeeVacationRequest) odm
                    .getData().get(0);
            if (vacRequest == null) {
                return ofr;
            }

            Employee employee = null;
            UDC religion, gender;
            Vacation vacation = null;
            VacationEntitlement vacationEntitlement = null;

            List<String> conds = new ArrayList<String>();

            employee = (Employee) oem.loadEntity("Employee", vacRequest
                    .getEmployee().getDbid(), null, null, loggedUser);
            religion = employee.getReligion();
            gender = employee.getGender();
            vacation = vacRequest.getVacation();

            conds.clear();
            conds.add("vacation.dbid = " + vacation.getDbid());

            vacationEntitlement = (VacationEntitlement) oem.loadEntity(
                    VacationEntitlement.class.getSimpleName(), conds, null,
                    loggedUser);

            if (vacationEntitlement != null) {
                if (vacationEntitlement.getGender() != null) {
                    if (gender == null
                            || (gender != null && !gender.getCode().equals(
                                    vacationEntitlement.getGender().getCode()))) {
                        ofr.addError(userMessageService.getUserMessage(
                                "VacEntGender", loggedUser));
                        return ofr;
                    }
                }
                if (vacationEntitlement.getReligion() != null) {
                    if (religion == null
                            || (religion != null && !religion.getCode()
                            .equals(vacationEntitlement.getReligion()
                                    .getCode()))) {
                        ofr.addError(userMessageService.getUserMessage(
                                "VacEntReligion", loggedUser));
                        return ofr;
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return ofr;
    }

    /*
     * validate if employee has balance in his
     * "vacation.affectedVacationBalance" vacation, then he can't get the
     * vacation till he finishes his balance.
     */
    @Override
    public OFunctionResult validateAffectingLeaveBalance(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        BigDecimal newBalance;
        try {
            EmployeeVacationRequest vac = (EmployeeVacationRequest) odm
                    .getData().get(0);
            Vacation affectedVacation;

            if (vac.getVacation() != null) {
                affectedVacation = vac.getVacation()
                        .getAffectedVacationBalance();

                if (affectedVacation != null) {
                    List vacBalData = new ArrayList();
                    DateFormat dateFormat = null;
                    if (affectedVacation.getUnitOfMeasure().equalsIgnoreCase(
                            "h")) {
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    } else {
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    }
                    vacBalData.add(vac.getEmployee().getDbid());
                    vacBalData.add(affectedVacation.getDbid());
                    vacBalData.add(dateFormat.format(vac.getStartDate()));
                    vacBalData.add(dateFormat.format(vac.getEndDate()));

                    String webServiceCode = "CVB_01";
                    String result = webServiceUtility.callingWebService(
                            webServiceCode, vacBalData, loggedUser);

                    newBalance = new BigDecimal(result);
                    if (newBalance.doubleValue() > 0) {
                        ofr.addError(userMessageService.getUserMessage(
                                "vacUnpaidLeave", loggedUser));
                        return ofr;
                    }

                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return ofr;
    }

    /**
     * This Method is used when you change a vacation to be settled, the
     * function fires and it returns the balance of the vacation which will be
     * settled for specific employee and returns the income and deduction salary
     * element that are defined in the vacation setup
     *
     * TOFIX: if the employee has a vacation in the date which be settled in,
     * the vacation enters normally which is not correct
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return vacationBalance, incomeSalaryElement, deductionSalaryElementd
     */
    // TOFIX: if the employee has a vacation in the date which be settled in,
    // the vacation enters normally which is not correct
    @Override
    public OFunctionResult vacationSettlementChangeFunction(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        BigDecimal balance;
        try {
            VacationSettlement vacSettlement = (VacationSettlement) odm
                    .getData().get(0);
            String fldExp = odm.getData().get(1).toString();
            Object newVal = odm.getData().get(2);
            Date requestDate = fldExp.equalsIgnoreCase("requestDate") ? (Date) newVal
                    : vacSettlement.getRequestDate();
            Vacation vacation = fldExp.toLowerCase().contains(
                    "originalVacation")
                    && newVal instanceof Vacation ? (Vacation) newVal
                            : vacSettlement.getOriginalVacation();

            if (fldExp
                    .equalsIgnoreCase("originalVacation.descriptionTranslated")
                    || fldExp.equalsIgnoreCase("requestDate")) {
                if (vacation != null) {
                    if (vacation.getSettlementIncome() != null) {
                        vacSettlement.setIncome(vacation.getSettlementIncome());
                    }
                    if (vacation.getSettlementDeduction() != null) {
                        vacSettlement.setDeduction(vacation
                                .getSettlementDeduction());
                    }
                    if (requestDate != null) {
                        List vacBalanceList = new ArrayList();
                        DateFormat dateFormat = null;
                        if (vacation.getUnitOfMeasure().equalsIgnoreCase("h")) {
                            dateFormat = new SimpleDateFormat(
                                    "dd-MM-yyyy HH:mm:ss");
                        } else {
                            dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        }

                        vacBalanceList.add(vacSettlement.getEmployee()
                                .getDbid());
                        vacBalanceList.add(vacation.getDbid());
                        vacBalanceList.add(dateFormat.format(requestDate));
                        vacBalanceList.add(dateFormat.format(requestDate));

                        String webServiceCode = "CVB_01";
                        String result = webServiceUtility.callingWebService(
                                webServiceCode, vacBalanceList, loggedUser);

                        balance = new BigDecimal(result);
                        vacSettlement.setOriginalBalance(balance);
                        // SET VALUES IN ENTITY
                        ofr.setReturnedDataMessage(odm);
                        ofr.append(ofr);
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return ofr;
    }

    /**
     * PostAction will settle el chosen vacation according to the target days,
     * if greater than zero, will settle on income, if lower than zero, will
     * settle on deduction salary element then it enters a new vacation to the
     * employee which will be deduct from employee vacation balance.
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult vacationSettlementPostAction(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            VacationSettlement vacSettlement = (VacationSettlement) odm
                    .getData().get(0);
            if (vacSettlement != null) {
                List<String> conditions;
                boolean settlementUpdate = false;
                if (vacSettlement.getTargetDays().doubleValue() > 0) {
                    if (vacSettlement.getIncome() != null) {
                        conditions = new ArrayList<String>();
                        conditions.add("employee.dbid = "
                                + vacSettlement.getEmployee().getDbid());
                        conditions.add("salaryElement.dbid = "
                                + vacSettlement.getIncome().getDbid());
                        EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) oem
                                .loadEntity(EmployeeSalaryElement.class
                                        .getSimpleName(), conditions, null,
                                        loggedUser);
                        if (employeeSalaryElement != null) {
                            employeeSalaryElement.setValue(vacSettlement
                                    .getTargetDays());
                            oem.saveEntity(employeeSalaryElement, loggedUser);
                            settlementUpdate = true;
                        }
                    }
                } else if (vacSettlement.getDeduction() != null) {
                    conditions = new ArrayList<String>();
                    conditions.add("employee.dbid = "
                            + vacSettlement.getEmployee().getDbid());
                    conditions.add("salaryElement.dbid = "
                            + vacSettlement.getDeduction().getDbid());
                    EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) oem
                            .loadEntity(EmployeeSalaryElement.class
                                    .getSimpleName(), conditions, null,
                                    loggedUser);
                    if (employeeSalaryElement != null) {
                        employeeSalaryElement.setValue(vacSettlement
                                .getTargetDays());
                        oem.saveEntity(employeeSalaryElement, loggedUser);
                        settlementUpdate = true;
                    }
                }
                if (settlementUpdate) {
                    // Enter New Vac
                    EmployeeVacationRequest newVacationRequest = new EmployeeVacationRequest();
                    newVacationRequest.setEmployee(vacSettlement.getEmployee());
                    newVacationRequest.setVacation(vacSettlement
                            .getOriginalVacation());
                    newVacationRequest.setStartDate(vacSettlement
                            .getRequestDate());
                    newVacationRequest.setEndDate(vacSettlement
                            .getRequestDate());
                    newVacationRequest.setRequestedDate(vacSettlement
                            .getRequestDate());
                    newVacationRequest.setBalance(vacSettlement
                            .getOriginalBalance());
                    newVacationRequest.setPeriod(vacSettlement.getTargetDays());
                    newVacationRequest.setNotes("Vacation Settlement");
                    newVacationRequest.setCancelled("N");
                    newVacationRequest.setType("A");
                    ofr.append(entitySetupService.callEntityCreateAction(
                            newVacationRequest, loggedUser));
                    if (ofr.getErrors().isEmpty()) {
                        vacSettlement.setDone(settlementUpdate);
                        oem.saveEntity(vacSettlement, loggedUser);
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult addVacationRequest(EmployeeVacationRequest request, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            ofr = entitySetupService.callEntityCreateAction(request, loggedUser);
            ofr.addReturnValue(request.getDbid());
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult manualPostingValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            ManualVacationPosting vacationPosting = (ManualVacationPosting) odm.getData().get(0);
            if (vacationPosting == null) {
                ofr.addError(userMessageService.getUserMessage("VacManualPosting001", loggedUser));
            }
            Vacation vacation = vacationPosting.getVacation();
            if (vacation == null || (vacation != null && vacation.getDbid() == 0)) {
                ofr.addError(userMessageService.getUserMessage("VacManualPosting002", loggedUser));
            }
            String year = vacationPosting.getPostedYear();
            if (year == null || (year != null && year.length() < 4)) {
                ofr.addError(userMessageService.getUserMessage("VacManualPosting003", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("VacManualPosting004", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult manualPostingProcess(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            ofr = manualPostingValidation(odm, functionParms, loggedUser);
            if (ofr.getErrors().isEmpty()) {
                ManualVacationPosting vacationPosting = (ManualVacationPosting) odm.getData().get(0);
                List wsData = new ArrayList();
                Map<String, Long> filterMap = new HashMap<String, Long>();
                //______________________________________
                Employee employee = vacationPosting.getEmployee();
                CostCenter costCenter = vacationPosting.getCostCenter();
                UDC location = vacationPosting.getLocation();
                UDC hiringType = vacationPosting.getHiringType();
                Position position = vacationPosting.getPosition();
                Unit unit = vacationPosting.getUnit();
                Job job = vacationPosting.getJob();
                PayGrade grade = vacationPosting.getPayGrade();
                //______________________________________
                Long employeeDbid = (employee == null) ? 0 : employee.getDbid();
                Long ccDbid = (costCenter == null) ? 0 : costCenter.getDbid();
                Long locationDbid = (location == null) ? 0 : location.getDbid();
                Long hiringTypeDbid = (hiringType == null) ? 0 : hiringType.getDbid();
                Long positionDbid = (position == null) ? 0 : position.getDbid();
                Long unitDbid = (unit == null) ? 0 : unit.getDbid();
                Long jobDbid = (job == null) ? 0 : job.getDbid();
                Long gradeDbid = (grade == null) ? 0 : grade.getDbid();
                filterMap.put("employee", employeeDbid);
                filterMap.put("costCenter", ccDbid);
                filterMap.put("location", locationDbid);
                filterMap.put("hiringType", hiringTypeDbid);
                filterMap.put("position", positionDbid);
                filterMap.put("unit", unitDbid);
                filterMap.put("job", jobDbid);
                filterMap.put("grade", gradeDbid);
                String whereSQL = FilterUtility.getFilterOHR(filterMap);

                wsData.add(vacationPosting.getVacation().getDbid());
                if (vacationPosting.isSecondPosting()) {
                    wsData.add("S");
                } else {
                    wsData.add("M");
                }
                wsData.add(vacationPosting.getPostedYear());
                wsData.add(whereSQL);
                String webServiceCode = "WSF-MVP0001";
                String result = webServiceUtility.callingWebService(webServiceCode, wsData, loggedUser);
                ofr.addSuccess(userMessageService.getUserMessage("VacManualPostingS001", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("VacManualPosting004", loggedUser));
        }
        return ofr;
    }

    private TMCalendar getEmpCalendar(Date currDate, Employee employee, OUser loggedUser) {
        TMCalendar calendar = null;
        try {
            List<String> conds = new ArrayList<String>();
            conds.add("currentEffectiveDate<='" + dateTimeUtility.DATE_FORMAT.format(currDate) + "'");
            conds.add("currentEffectiveEndDate>='" + dateTimeUtility.DATE_FORMAT.format(currDate) + "'");
            conds.add("employee.dbid=" + employee.getDbid());
            conds.add("cancelled = 'false'");
            EmployeeWorkingCalHistory calHistory = (EmployeeWorkingCalHistory) oem.loadEntity(EmployeeWorkingCalHistory.class.getSimpleName(),
                    conds, null, loggedUser);
            if (calHistory != null) {
                calendar = calHistory.getCurrentWorkingCal();
            } else {
                calendar = employee.getCalendar();
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return calendar;
    }

    private Object[] getShift(TMCalendar rotationCal, Date startDate, OUser loggedUser) {

        Object returned[] = new Object[2];

        NormalWorkingCalendar shift = null;

        List<String> shiftConds = new ArrayList<String>();
        shiftConds.add("calendar.dbid = " + rotationCal.getDbid());
        List<NormalWorkingCalendar> shifts = null;

        List<String> sort = new ArrayList<String>();
        sort.add("shiftSequence");

        try {
            shifts = (List<NormalWorkingCalendar>) oem.loadEntityList(
                    NormalWorkingCalendar.class.getSimpleName(), shiftConds, null, sort, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(vacationService.class.getName()).log(Level.SEVERE, null, ex);
        }

        Date referenceDate = rotationCal.getReferenceDate();

        int result = referenceDate.compareTo(startDate);
        int totalNumDays = 0;

        DateTime fromDT = new DateTime(referenceDate, dateTimeUtility.dateTimeZone);
        DateTime toDT = new DateTime(startDate, dateTimeUtility.dateTimeZone);

        if (result <= 0) {
            totalNumDays = Days.daysBetween(fromDT, toDT).getDays();
        }
        if (result > 0) {
            totalNumDays = Days.daysBetween(toDT, fromDT).getDays();
        }

        totalNumDays++;

        if (shifts != null && !shifts.isEmpty()) {
            int numShifts = shifts.size();
            int roundDuration = 0; // number of days in each round
            for (int j = 0; j < numShifts; j++) {
                roundDuration += shifts.get(j).getNumberOfDays();
            }

            int remainder = totalNumDays % roundDuration;

            if (remainder == 0) {
                remainder = roundDuration;
            }
            returned[0] = remainder;

            int temp = 0;
            for (int j = 0; j < shifts.size(); j++) {

                temp += shifts.get(j).getNumberOfDays();
                if (remainder <= temp) {
                    shift = shifts.get(j);
                    returned[1] = shift;
                    break;
                }
            }
        }
        return returned;
    }

}
