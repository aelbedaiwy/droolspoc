package com.unitedofoq.otms.personnel.absence;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface AbsenceServiceLocal {
    public OFunctionResult validateEmployeeAbsenceRequest(ODataMessage odm,OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult cancelAbsent(ODataMessage odm,OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult modifyAbsent(ODataMessage odm,OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult cancelAbsRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
}
