package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.filter.FilterUtility;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.sql.DataSource;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.personnel.vacation.EmployeeVacationAdjustmentServiceLocal",
        beanInterface = EmployeeVacationAdjustmentServiceLocal.class)
public class EmployeeVacationAdjustmentServiceBean implements EmployeeVacationAdjustmentServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote userMessageService;
    @EJB
    private DataSecurityServiceLocal dataSecurityServiceLocal;

    @Override
    public OFunctionResult executeVacationAdjustmentProcess(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            DateTimeUtility dateTimeUtility = new DateTimeUtility();
            EmployeeVacationAdjustmentAssign vacAdjustmentFilter = (EmployeeVacationAdjustmentAssign) odm.getData().get(0);
            Date creationDate = null;
            Vacation vac = null;
            BigDecimal adjValue = null;
            String creationDateStr = null;

            String comments = "";
            //construct filter query
            if (vacAdjustmentFilter != null) {

                adjValue = vacAdjustmentFilter.getAdjustmentValue();

                vac = vacAdjustmentFilter.getVacation();

                creationDate = vacAdjustmentFilter.getCreationDate();

                comments = vacAdjustmentFilter.getComments();
                DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                Connection connection = datasource.getConnection();
                if (connection.getMetaData().getDatabaseProductName().toLowerCase().contains("oracle")) {
                    creationDateStr = "to_date('" + dateTimeUtility.DATE_FORMAT.format(creationDate) + "','YYYY-MM-DD')";
                } else {
                    creationDateStr = "'" + dateTimeUtility.DATE_FORMAT.format(creationDate) + "'";
                }
            }

            //if vacation date, type, or adjustment value is null then show an error message
            if ((adjValue == null) || (vac == null) || (creationDate == null)) {
                //You must enter adjustment value and vacation type and date
                ofr.addError(userMessageService.getUserMessage("VACADJERR000001", loggedUser));
                return ofr;
            }

            Map<String, Long> filterMap = new HashMap<String, Long>();
            filterMap.put("employee", vacAdjustmentFilter.getEmployee() == null ? 0 : vacAdjustmentFilter.getEmployee().getDbid());
            filterMap.put("costCenter", vacAdjustmentFilter.getCostCenter() == null ? 0 : vacAdjustmentFilter.getCostCenter().getDbid());
            filterMap.put("location", vacAdjustmentFilter.getLocation() == null ? 0 : vacAdjustmentFilter.getLocation().getDbid());
            filterMap.put("hiringType", vacAdjustmentFilter.getHiringType() == null ? 0 : vacAdjustmentFilter.getHiringType().getDbid());
            filterMap.put("position", vacAdjustmentFilter.getPosition() == null ? 0 : vacAdjustmentFilter.getPosition().getDbid());
            filterMap.put("unit", vacAdjustmentFilter.getUnit() == null ? 0 : vacAdjustmentFilter.getUnit().getDbid());
            filterMap.put("job", vacAdjustmentFilter.getJob() == null ? 0 : vacAdjustmentFilter.getJob().getDbid());
            filterMap.put("grade", vacAdjustmentFilter.getPayGrade() == null ? 0 : vacAdjustmentFilter.getPayGrade().getDbid());

            String whereSQL = FilterUtility.getFilter(filterMap, "dbid");
            if (whereSQL == null) {
                whereSQL = "";
            }
            String securityCond = "";
            if (vacAdjustmentFilter.getEmployee() == null || vacAdjustmentFilter.getEmployee().getDbid() == 0) {
                List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
                String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                securityCond = " AND employee_dbid in " + employeeDbidStr;
            }
            String deleteSQL = "Delete from  employeevacationadjustment where creationdate = " + creationDateStr + " " + whereSQL + securityCond;
            oem.executeEntityUpdateNativeQuery(deleteSQL, loggedUser);
            String selectEmployees = "SELECT * FROM OEMPLOYEE WHERE 1=1 " + whereSQL + securityCond.replace("employee_dbid", "dbid");
            List<Object[]> employees = (List<Object[]>) oem.executeEntityListNativeQuery(selectEmployees, loggedUser);
            if (employees.isEmpty()) {
                ofr.addError(userMessageService.getUserMessage("VACADJERR000003", loggedUser));
                return ofr;
            }

            //insert employeevacationadjustment requests
            String insertStmt = "INSERT INTO employeevacationadjustment(employee_dbid, deleted , ACTIVE,adjustmentvalue, vacation_dbid, creationdate,comments) VALUES(";
            String insertValues;
            for (Object[] object : employees) {
                String empDBID = object[0].toString();

                insertValues = empDBID + ", 0, 1," + adjValue + ", " + vac.getDbid() + "," + creationDateStr + ",'" + comments + "')";
                String insertStmtComplete = insertStmt + insertValues;

                oem.executeEntityUpdateNativeQuery(insertStmtComplete, loggedUser);

                updateEmployeeDayoff(Long.parseLong(empDBID), vac.getDbid(), adjValue.toString(), creationDateStr, loggedUser);
            }

            ofr.addSuccess(userMessageService.getUserMessage("VACADJSUC000001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("VACADJERR000002", loggedUser));
        } finally {

            return ofr;
        }
    }

    @Override
    public OFunctionResult importEmployeeVacationAdjustment(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            if (odm != null) {
                String sql;
                String securityCond = "";
                List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
                if (!employeeDbidList.isEmpty()) {
                    String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                    securityCond = " AND BB.dbid in " + employeeDbidStr;
                }
                sql = "select AA.empCode,AA.vacCode,AA.applyDate,AA.adjustValue,AA.comments,"
                        + " BB.dbid, CC.dbid, AA.dbid"
                        + " From EmpVacAdjImport AA ,Oemployee BB, DayOff CC"
                        + " where AA.done = 0  "
                        + " AND AA.empCode = BB.code"
                        + " AND AA.vacCOde = CC.code"
                        + securityCond;
                List<Object[]> objs = oem.executeEntityListNativeQuery(sql, loggedUser);
                String empCode;
                String vacCode;
                String applyDate;
                String adjustValue;
                String comments;
                String insertStmt;
                Long empDbid;
                Long vacDbid;
                Long dbid;
                DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                Connection connection = datasource.getConnection();
                String driverType = connection.getMetaData().getDatabaseProductName().toLowerCase();
                for (Object[] obj : objs) {
                    empCode = obj[0].toString();
                    vacCode = obj[1].toString();
                    applyDate = obj[2].toString();
                    adjustValue = obj[3].toString();
                    comments = obj[4].toString();
                    empDbid = Long.parseLong(obj[5].toString());
                    vacDbid = Long.parseLong(obj[6].toString());
                    dbid = Long.parseLong(obj[7].toString());
                    if ((applyDate != null) && (!applyDate.equals("1900-01-01"))
                            && (adjustValue != null)
                            && (!new BigDecimal(adjustValue).equals(BigDecimal.ZERO))) {
                        insertStmt = "INSERT INTO employeevacationadjustment(employee_dbid, adjustmentvalue, "
                                + "vacation_dbid, creationdate,comments,active,deleted) VALUES("
                                + empDbid + ", " + adjustValue + "," + vacDbid + ",";
                        if (driverType.contains("oracle")) {
                            insertStmt = insertStmt + "To_Date('" + applyDate + "','YYYY-MM-DD'),'" + comments + "',1,0)";
                            updateEmployeeDayoff(empDbid, vacDbid, adjustValue, "To_Date('" + applyDate + "','YYYY-MM-DD')", loggedUser);
                        } else {
                            insertStmt = insertStmt + "'" + applyDate + "','" + comments + "',1,0)";
                            updateEmployeeDayoff(empDbid, vacDbid, adjustValue, applyDate, loggedUser);
                        }
                        if (oem.executeEntityUpdateNativeQuery(insertStmt, loggedUser)) {
                            sql = "Update EmpVacAdjImport set done = 1 where dbid = " + dbid;
                            oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                        }

                    }
                    ofr.addSuccess(userMessageService.getUserMessage("VACADJERR000003", loggedUser));
                }

            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageService.getUserMessage("VACADJERR000004", loggedUser));
        } finally {

            return ofr;
        }
    }

    private void updateEmployeeDayoff(long empDbid, long vacDbid, String adjustmentVal, String adjustmentDate,
            OUser loggedUser) {
        try {
            String updateStat = "update employeedayoff set adjustmentvalue = "
                    + adjustmentVal + ", adjustmentDate = " + adjustmentDate
                    + " where employee_dbid = " + empDbid + " and vacation_dbid = " + vacDbid;
            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }
}
