package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface vacationServiceLocal {

    public OFunctionResult UpdateEmployeeVacationHeader(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateEmployeeVacationRequest(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult cancelVacation(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult modifyVacation(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult UpdateVacationDetail(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult validateHajMaternityLeaveVacation(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public com.unitedofoq.fabs.core.function.OFunctionResult calcActualBalance(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public OFunctionResult cancelRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult importEmpVacReq(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult importEmployeeVacation(com.unitedofoq.fabs.core.datatype.ODataMessage odm, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);

    public String enterImportedVacations(BaseEntity empVacReq, OUser loggedUser);

    public OFunctionResult validateVacReqOnSetup(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateAffectingLeaveBalance(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult vacationSettlementPostAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult vacationSettlementChangeFunction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult addVacationRequest(EmployeeVacationRequest request, OUser loggedUser);

    public OFunctionResult manualPostingValidation(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult manualPostingProcess(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);
}
