package com.unitedofoq.otms.personnel.vacation;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

@Local
public interface EmployeeVacationAdjustmentServiceLocal {

    public OFunctionResult executeVacationAdjustmentProcess(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult importEmployeeVacationAdjustment(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
}
