package com.unitedofoq.otms.personnel.absence;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.personnel.absence.AbsenceServiceLocal",
beanInterface = AbsenceServiceLocal.class)
public class AbsenceService implements AbsenceServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private AbsenceServiceLocal absService;
    @EJB
    private WSUtilityLocal webServiceUtility;

    @Override
    public OFunctionResult validateEmployeeAbsenceRequest(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        try {
            EmployeeAbsenceRequest absence = (EmployeeAbsenceRequest) odm.getData().get(0);
            if (absence.getStartDate() == null || absence.getEndDate() == null) {
                ofr.setReturnedDataMessage(odm);
                return ofr;
            }
            List absenceValidationData = new ArrayList();
            DateFormat dateFormat ;
            String endDate;
            if (absence.getAbsence().getUnitOfMeasure().equalsIgnoreCase("h")) {
                dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                endDate = dateFormat.format(absence.getEndDate());
            } else {
                dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                absence.setStartDate(dateFormat.parse(dateFormat.format(absence.getStartDate())));
                endDate = dateFormat.format(absence.getEndDate()).concat(" 23:59:59");
                dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                absence.setEndDate(dateFormat.parse(endDate));
                dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            }

            absenceValidationData.add(absence.getEmployee().getDbid());
            absenceValidationData.add(absence.getAbsence().getDbid());
            absenceValidationData.add(dateFormat.format(absence.getStartDate()));
            absenceValidationData.add(endDate);

            String webServiceCode = "V_A_F";
            String result = webServiceUtility.callingWebService(webServiceCode, absenceValidationData, loggedUser);           

            if (result == null || result.compareTo(" ") == 0 ) {
            } else {
                UserMessage msg = new UserMessage();
                msg.setMessageText(result);
                msg.setMessageTitle("Validation Error");
                msg.setName("Validation Error");
                msg.setMessageTextTranslated(result);
                msg.setMessageType(UserMessage.TYPE_ERROR);
                ofr.addError(msg);
            }
            return ofr;

        } catch (Exception ee) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(ee, loggedUser);
            // </editor-fold>
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ee);
            return ofr;
        }
    }

    @Override
    public OFunctionResult cancelAbsent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeAbsenceRequest absRequest = (EmployeeAbsenceRequest) odm.getData().get(0);
            if (absRequest.getCancelDate() == null) {
                absRequest.setCancelDate(new Date());
            }
            if (absRequest.getCancelled().equals("Y")) {
                ofr.addError(usrMsgService.getUserMessage("cancelAbsent001", loggedUser));
                return ofr;
            }
            // CALL Absent Cancel OPERATION
            List CancelVacdata = new ArrayList();
            CancelVacdata.add(absRequest.getDbid());
            String webServiceCode = "WSF_CA";
            webServiceUtility.callingWebService(webServiceCode, CancelVacdata, loggedUser);           

            absRequest.setCancelled("Y");
            absRequest.setStatus("C");
            absRequest.setDecisionDate(absRequest.getCancelDate());
            absRequest.setDecisionApprovedBy(absRequest.getCancelledBy());

            oem.saveEntity(absRequest, loggedUser);
            ofr.addSuccess(usrMsgService.getUserMessage("cancelAbsent002", loggedUser));
        } catch (Exception ee) {
            OLog.logException(ee, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult modifyAbsent(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        {   //Cancel Current Vacation & Generate New One
            OFunctionResult ofr = new OFunctionResult();
            try {
                EmployeeAbsenceRequest absRequest = (EmployeeAbsenceRequest) odm.getData().get(0);
                ofr.append(absService.cancelAbsRequest(odm, functionParms, loggedUser));
                if (ofr.getErrors().size() > 0) {
                    return ofr;
                }
                //Generate New Tran.
                EmployeeAbsenceRequest newVacationRequest = new EmployeeAbsenceRequest();
                newVacationRequest.setEmployee(absRequest.getEmployee());
                newVacationRequest.setAbsence(absRequest.getAbsence());
                newVacationRequest.setStartDate(absRequest.getStartDate());
                newVacationRequest.setEndDate(absRequest.getEndDate());
                newVacationRequest.setRequestedDate(absRequest.getRequestedDate());
                newVacationRequest.setBalance(absRequest.getBalance());
                newVacationRequest.setPeriod(absRequest.getPeriod());
                newVacationRequest.setPosted(absRequest.getPosted());
                newVacationRequest.setPostedDays(absRequest.getPostedDays());
                newVacationRequest.setOverride(absRequest.getOverride());
                newVacationRequest.setDecisionDate(absRequest.getDecisionDate());
                newVacationRequest.setDecisionNumber(absRequest.getDecisionNumber());
                newVacationRequest.setDecisionApprovedBy(absRequest.getDecisionApprovedBy());
                newVacationRequest.setNotes(absRequest.getNotes());
                newVacationRequest.setCancelled("N");


                OFunctionResult returnResult = entitySetupService.callEntityCreateAction(newVacationRequest, loggedUser);
                //Fetch Error Message If Exist
                if (returnResult.getErrors().size() > 0) {
                    for (int index = 0; index < returnResult.getErrors().size(); index++) {
                        ofr.addError(returnResult.getErrors().get(index));
                    }
                } else {
                    absRequest.setDbid(newVacationRequest.getDbid());
                    ofr.addSuccess(usrMsgService.getUserMessage("modifyAbsent002", loggedUser));
                }
            } catch (Exception ee) {
                OLog.logException(ee, loggedUser);
                ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
            }
            return ofr;
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult cancelAbsRequest(
            ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        EmployeeAbsenceRequest absRequest = (EmployeeAbsenceRequest) odm.getData().get(0);
        //Load The Vacation Request & Make It As Cancel.
        EmployeeAbsenceRequest cancelledRequest = null;
        try {
            cancelledRequest = (EmployeeAbsenceRequest) oem.loadEntity(EmployeeAbsenceRequest.class.getSimpleName(), absRequest.getDbid(), null, null, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        if (cancelledRequest == null) {
            ofr.addError(usrMsgService.getUserMessage("modifyAbsent001", loggedUser));
            return ofr;
        }

        if (!(cancelledRequest.getCancelled().equals("Y"))) {
            //Cancel Vacation
            // CALL VACATION Cancel OPERATION
            List CancelVacdata = new ArrayList();
            CancelVacdata.add(absRequest.getDbid());
            
            
            String webServiceCode = "WSF_CA";
            webServiceUtility.callingWebService(webServiceCode, CancelVacdata, loggedUser);           

            cancelledRequest.setCancelled("Y");
            cancelledRequest.setCancelDate(new Date());
            cancelledRequest.setStatus("C");
            cancelledRequest.setDecisionDate(absRequest.getCancelDate());
            cancelledRequest.setDecisionApprovedBy(absRequest.getCancelledBy());
            try {
                oem.saveEntity(cancelledRequest, loggedUser);
            } catch (Exception ex) {
                OLog.logException(ex, loggedUser);
            }
        }
        return ofr;
    }
}
