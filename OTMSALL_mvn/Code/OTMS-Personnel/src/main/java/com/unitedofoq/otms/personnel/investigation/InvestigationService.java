package com.unitedofoq.otms.personnel.investigation;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.function.OFunctionServiceRemote;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.PayrollParameter;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeePayroll;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryElement;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryUpgrade;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.personnel.penalty.EmployeePenalty;
import com.unitedofoq.otms.personnel.penalty.EmployeePenaltyRequest;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.personnel.penalty.PenaltyAffectingDeduction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.personnel.investigation.InvestigationServiceLocal", beanInterface = InvestigationServiceLocal.class)
public class InvestigationService implements InvestigationServiceLocal {

	@EJB
	private OEntityManagerRemote oem;
	@EJB
	protected EntityServiceLocal entityServiceLocal;
	@EJB
	OFunctionServiceRemote functionService;
	@EJB
	private UserMessageServiceRemote usrMsgService;
	@EJB
	private EntitySetupServiceRemote entitySetupService;

	@Override
	public OFunctionResult postSaveEmpInvestigation(ODataMessage odm,
			OFunctionParms functionParms, OUser loggedUser) {
		OFunctionResult oFR = new OFunctionResult();
		try {
			EmpInvestigation request = (EmpInvestigation) odm.getData().get(0);
			EmpInvestigation empInvestigation;
			InvestigationStatus status;
			EmployeePayroll employeePayroll;
			Employee employee;
			Penalty penalty;
			EmployeeSalaryElement employeeSalaryElement = null;
			List<String> conditions = new ArrayList<String>();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			EmpInvestigation susInvestigation;

			if (request != null) {
				empInvestigation = request.getEmpInvestigation();
				if (empInvestigation == null) {
					return oFR;
				}
				employee = request.getEmployee();
				if (employee == null) {
					employee = empInvestigation.getEmployee();
				}
				status = request.getStatus();
				if (status != null) {
					if (status.getCode().equalsIgnoreCase("suspend")) // Suspend
																		// Employee
					{
						employeePayroll = employee.getPayroll();
						if (employeePayroll != null
								&& employeePayroll.getDbid() != 0) {
							employeePayroll.setSuspended(true);
							oem.saveEntity(employeePayroll, loggedUser);
						}
					} else if (status.getCode().equalsIgnoreCase("csuspend")) // Cancel
																				// Suspend
																				// Employee
					{
						// __________________________________
						employeePayroll = employee.getPayroll();
						if (employeePayroll != null
								&& employeePayroll.getDbid() != 0) {
							employeePayroll = employee.getPayroll();
							employeePayroll.setSuspended(false);
							oem.saveEntity(employeePayroll, loggedUser);
						}
						Double totalIncomes = 0.0;
						Double totalDeductions = 0.0;
						Double netSalary;

						Date startDate;
						Date endDate;
						String expression;
						Object tmp;
						// Update Emp Sal Elem.
						empInvestigation = request.getEmpInvestigation();
						conditions.clear();
						conditions.add("empInvestigation.dbid ="
								+ empInvestigation.getDbid());
						conditions.add("status.code = 'suspend'");
						conditions.add("suspensionCancelled = 'N'");
						susInvestigation = null;
						try {
							susInvestigation = (EmpInvestigation) oem
									.loadEntity("EmpInvestigation", conditions,
											null, loggedUser);
						} catch (Exception ex) {
							OLog.logException(ex, loggedUser);
							oFR.addError(usrMsgService.getUserMessage(
									"Suspend001", loggedUser));
						}
						if (susInvestigation != null) {
							startDate = susInvestigation.getInvDate();
							endDate = request.getInvDate();
							expression = "select sum(lec_net_amount) from periodic_salary_elements where employee = '"
									+ request.getEmpInvestigation()
											.getEmployee().getDbid()
									+ "' and company_flag = 'N' and income_or_deduction = 'I' and "
									+ " calculated_period_id in (select calculated_period_id from calculated_period where "
									+ " code like 'M' and (start_date between '"
									+ dateFormat.format(startDate)
									+ "' and '"
									+ dateFormat.format(endDate) + "')  )";
							tmp = oem.executeEntityNativeQuery(expression,
									loggedUser);
							if (tmp != null) {
								totalIncomes = Double.parseDouble(tmp
										.toString());
							}

							expression = "select sum(lec_net_amount) from periodic_salary_elements where employee = '"
									+ request.getEmpInvestigation()
											.getEmployee().getDbid()
									+ "' and company_flag = 'N' and income_or_deduction = 'D' and "
									+ " calculated_period_id in (select calculated_period_id from calculated_period where "
									+ " code like 'M' and (start_date between '"
									+ dateFormat.format(startDate)
									+ "' and '"
									+ dateFormat.format(endDate) + "')  )";
							tmp = oem.executeEntityNativeQuery(expression,
									loggedUser);
							if (tmp != null) {
								totalDeductions = Double.parseDouble(tmp
										.toString());
							}
							netSalary = totalIncomes - totalDeductions;
							conditions.clear();
							conditions.add("employee.dbid = "
									+ request.getEmpInvestigation()
											.getEmployee().getDbid());
							conditions
									.add("salaryElement.internalCode = 'reversesuspend'");
							employeeSalaryElement = (EmployeeSalaryElement) oem
									.loadEntity("EmployeeSalaryElement",
											conditions, null, loggedUser);
							if (employeeSalaryElement != null) {
								employeeSalaryElement.setValue(new BigDecimal(
										netSalary));
								oem.saveEntity(employeeSalaryElement,
										loggedUser);
							}
							susInvestigation.setSuspensionCancelled("Y");
							oem.saveEntity(susInvestigation, loggedUser);
						}
					} else if (status.getCode().equalsIgnoreCase("peneffect")) // Adding
																				// Penalty
																				// Request
					{
						penalty = request.getStatus().getPenalty();
						if (penalty != null) {
							oFR.append(penaltyRequest(employee, penalty,
									empInvestigation, loggedUser));
							if (oFR.getErrors().isEmpty()) {
								oFR.addSuccess(usrMsgService.getUserMessage(
										"InvPenaltyEntry", loggedUser));
							}
						}
						return oFR;
					} else if (status.getCode().equalsIgnoreCase("cpeneffect")) // Cancel
																				// Penalty
																				// Request
					{
						empInvestigation = request.getEmpInvestigation();
						if (empInvestigation != null) {
							List<EmployeePenaltyRequest> penaltyRequests;
							EmployeePenaltyRequest penaltyRequest;
							conditions.clear();
							conditions.add("cancelled = 'N'");
							conditions.add("investigation.dbid = "
									+ empInvestigation.getDbid());
							penaltyRequests = oem.loadEntityList(
									"EmployeePenaltyRequest", conditions, null,
									null, loggedUser);
							for (int i = 0; i < penaltyRequests.size(); i++) {
								penaltyRequest = penaltyRequests.get(i);
								if (penaltyRequest != null) {
									if (penaltyRequest.getPenalty().getDbid() == (status
											.getPenalty().getDbid())) {
										if (cancelPenaltyRequest(
												penaltyRequest, loggedUser)) {
											oFR.addSuccess(usrMsgService
													.getUserMessage(
															"CInvPenaltyEntry",
															loggedUser));
										}
									}
								}
							}
						}
					} else if (status.getCode().equalsIgnoreCase("saleffect")) // Salary
																				// Upgrade
																				// Request
					{
						if (status.getSalaryEffect() == null) {
							return oFR;
						}
						if (status.getSalaryEffect().doubleValue() > 0) {
							salaryUpgradeRequest(employee, status.getIncome(),
									empInvestigation, request, loggedUser);
						}
					} else if (status.getCode().equalsIgnoreCase("csaleffect")) // Cancel
																				// Salary
																				// Upgrade
																				// Request
					{
						empInvestigation = request.getEmpInvestigation();
						conditions.clear();
						List<EmployeeSalaryUpgrade> salaryUpgrades;
						EmployeeSalaryUpgrade salaryUpgradeRequest;
						conditions.add("investigation.dbid = "
								+ empInvestigation.getDbid());
						salaryUpgrades = oem.loadEntityList(
								"EmployeeSalaryUpgrade", conditions, null,
								null, loggedUser);
						for (int i = 0; i < salaryUpgrades.size(); i++) {
							salaryUpgradeRequest = salaryUpgrades.get(i);
							cancelSalaryUpgradeRequest(salaryUpgradeRequest,
									loggedUser);
						}
					}
				}
			}
		} catch (Exception ex) {
			oFR.addError(usrMsgService.getUserMessage("SystemInternalError",
					loggedUser));
			OLog.logException(ex, loggedUser);
		} finally {
			return oFR;
		}
	}

	public boolean cancelPenaltyRequest(
			EmployeePenaltyRequest employeePenaltyRequest, OUser loggedUser) {
		try {
			if (employeePenaltyRequest.getCancelled() == null
					|| employeePenaltyRequest.getCancelled().equals("N")) {
				Calendar cal = Calendar.getInstance();
				employeePenaltyRequest.setCancelled("Y");
				employeePenaltyRequest.setCancelDate(cal.getTime());
				oem.saveEntity(employeePenaltyRequest, loggedUser);
				long employeeDbid = employeePenaltyRequest.getEmployee()
						.getDbid();
				long penaltyDbid = employeePenaltyRequest.getPenalty()
						.getDbid();

				List<String> penaltyConditions = new ArrayList<String>();
				penaltyConditions.add("employee.dbid = " + employeeDbid);
				penaltyConditions.add("penalty.dbid = " + penaltyDbid);
				List<EmployeePenalty> employeePenaltys = oem.loadEntityList(
						EmployeePenalty.class.getSimpleName(),
						penaltyConditions, null, null, loggedUser);

				if (employeePenaltys.size() >= 1) {
					EmployeePenalty employeePenalty = employeePenaltys.get(0);
					employeePenalty.setCountNumber(Short
							.valueOf((short) (employeePenalty.getCountNumber()
									.shortValue() - 1)));
					employeePenalty
							.setTotal(employeePenalty.getTotal().subtract(
									employeePenaltyRequest.getPenaltyValue()));
					employeePenalty.setCurrentMonthOccurance(employeePenalty
							.getCurrentMonthOccurance()
							.subtract(BigDecimal.ONE));
					employeePenalty.setLastPenaltyDate(employeePenaltyRequest
							.getPenaltyDate());
					oem.saveEntity(employeePenalty, loggedUser);
				}
				// ____________________Apply ESE
				// Update________________________________
				Deduction penaltyDeduction = null;
				PenaltyAffectingDeduction penaltyAffectingDeduction;
				Date penaltyDate;
				PayrollParameter payrollParameter;
				int year, month;
				String parameterValue = "0";
				penaltyConditions.clear();
				penaltyConditions.add("penalty.dbid = "
						+ employeePenaltyRequest.getPenalty().getDbid());
				penaltyAffectingDeduction = (PenaltyAffectingDeduction) oem
						.loadEntity("PenaltyAffectingDeduction",
								penaltyConditions, null, loggedUser);
				if (penaltyAffectingDeduction != null
						&& penaltyAffectingDeduction.getDbid() != 0) {
					penaltyDeduction = penaltyAffectingDeduction.getDeduction();
					penaltyDate = employeePenaltyRequest.getPenaltyDate();
					penaltyConditions.clear();
					penaltyConditions.add("valueData = 'day_of_calculation'");
					payrollParameter = (PayrollParameter) oem.loadEntity(
							"PayrollParameter", penaltyConditions, null,
							loggedUser);
					if (payrollParameter != null
							&& payrollParameter.getDbid() != 0) {
						parameterValue = payrollParameter.getValueData();
					}
					// Current Month
					month = penaltyDate.getMonth() + 1;
					year = penaltyDate.getYear() + 1900;
					if (penaltyDate.getDate() > Integer
							.parseInt(parameterValue)) {
						// Next Month
						month += 1;
						if (month > 12) {
							month = 1;
							year += 1;
						}
					}
					// Get The Value From PSE
					String expression = "select sum(lec_net_amount) from periodic_salary_elements where employee = '"
							+ employeePenaltyRequest.getEmployee().getDbid()
							+ "' and company_flag = 'N' and income_or_deduction = 'D' and "
							+ " salary_element_id = '"
							+ penaltyDeduction.getDbid()
							+ "'"
							+ " and calculated_period_id in (select calculated_period_id from calculated_period where "
							+ " code like 'M%' and (curr_month ="
							+ month
							+ " ) and " + " (curr_year = " + year + " ))";
					Object tmp = oem.executeEntityNativeQuery(expression,
							loggedUser);
					Double penaltyValue = 0.0;
					if (tmp != null) {
						penaltyValue = Double.parseDouble(tmp.toString());
					}
					penaltyConditions.clear();
					penaltyConditions.add("employee.dbid = " + employeeDbid);
					penaltyConditions
							.add("salaryElement.internalCode = 'PenEffect'");
					EmployeeSalaryElement employeeSalaryElement = (EmployeeSalaryElement) oem
							.loadEntity("EmployeeSalaryElement",
									penaltyConditions, null, loggedUser);
					if (employeeSalaryElement != null) {
						employeeSalaryElement.setValue(new BigDecimal(
								penaltyValue));
						oem.saveEntity(employeeSalaryElement, loggedUser);
					}
				}

			}
		} catch (Exception ex) {
			OLog.logError(ex.toString(), loggedUser);
			return false;
		}

		return true;
	}

	public OFunctionResult penaltyRequest(Employee employee, Penalty penalty,
			EmpInvestigation request, OUser loggedUser) {
		OFunctionResult result = new OFunctionResult();
		try {
			EmployeePenaltyRequest penaltyRequest = new EmployeePenaltyRequest();
			penaltyRequest.setEmployee(employee);
			penaltyRequest.setPenalty(penalty);
			penaltyRequest.setPenaltyDate(new Date());
			penaltyRequest.setPenaltyValue(penalty.getDefaultValue());
			penaltyRequest.setInvestigation(request);
			penaltyRequest.setGivenBy(employee);
			penaltyRequest.setCancelled("N");
			result = entitySetupService.callEntityCreateAction(penaltyRequest,
					loggedUser);
			return result;

		} catch (Exception ex) {
			OLog.logError(ex.toString(), loggedUser);
		}
		return result;
	}

	public boolean salaryUpgradeRequest(Employee employee, Income income,
			EmpInvestigation header, EmpInvestigation detail, OUser loggedUser) {
		try {
			// ________________Get Old Value & Update ESE With New
			// Value_________________________________
			EmployeeSalaryElement ese;
			BigDecimal oldValue = new BigDecimal(BigInteger.ZERO);
			BigDecimal newValue = new BigDecimal(BigInteger.ZERO);
			BigDecimal difference = new BigDecimal(BigInteger.ZERO);
			List<String> conditions = new ArrayList<String>();
			conditions.add("employee.dbid =" + employee.getDbid());
			conditions.add("salaryElement.dbid =" + income.getDbid());
			ese = (EmployeeSalaryElement) oem.loadEntity(
					"EmployeeSalaryElement", conditions, null, loggedUser);
			if (ese != null) {
				oldValue = ese.getValue();
				newValue = oldValue.subtract(oldValue.multiply(
						detail.getStatus().getSalaryEffect()).divide(
						new BigDecimal(100)));
				difference = newValue.subtract(oldValue);
				ese.setValue(newValue);
				oem.saveEntity(ese, loggedUser);
			}
			// ________________Get Old Value & Update ESE With New
			// Value_________________________________
			EmployeeSalaryUpgrade employeeSalaryUpgrade = new EmployeeSalaryUpgrade();
			employeeSalaryUpgrade.setEmployee(employee);
			employeeSalaryUpgrade.setIncome(income);
			employeeSalaryUpgrade.setApplyDate(header.getInvDate()); // the date
																		// of
																		// the
																		// header
																		// ??
			employeeSalaryUpgrade.setGivenDate(header.getInvDate());
			employeeSalaryUpgrade.setInvestigation(header);
			employeeSalaryUpgrade.setValueBefore(oldValue);
			employeeSalaryUpgrade.setValueAfter(newValue);
			employeeSalaryUpgrade.setDifference(difference);
			employeeSalaryUpgrade.setTransactionNumber(BigDecimal.ONE);
			employeeSalaryUpgrade.setApplied("Y");
			employeeSalaryUpgrade.setCancelled("N");

			oem.saveEntity(employeeSalaryUpgrade, loggedUser);

		} catch (Exception ex) {
			OLog.logError(ex.toString(), loggedUser);
			return false;
		}

		return true;
	}

	public boolean cancelSalaryUpgradeRequest(
			EmployeeSalaryUpgrade salaryUpgrade, OUser loggedUser) {
		try {
			if (salaryUpgrade == null) {
				return false;
			}
			if (salaryUpgrade.getCancelled().equals("Y")) {
				return false;
			}
			if (salaryUpgrade.getApplied() != null
					&& salaryUpgrade.getApplied().equals("Y")) {
				List conditions = new ArrayList();
				conditions.add("employee.dbid = "
						+ salaryUpgrade.getEmployee().getDbid());
				conditions.add("salaryElement.dbid = "
						+ salaryUpgrade.getIncome().getDbid());
				List<EmployeeSalaryElement> ese = oem.loadEntityList(
						"EmployeeSalaryElement", conditions, null, null,
						loggedUser);
				if (ese.size() > 0) {
					ese.get(0).setValue(salaryUpgrade.getValueBefore());
					oem.saveEntity(ese.get(0), loggedUser);
				}
			}
			salaryUpgrade.setCancelled("Y");
			oem.saveEntity(salaryUpgrade, loggedUser);
		} catch (Exception ex) {
			OLog.logError(ex.toString(), loggedUser);
			return false;
		}

		return true;
	}

	@Override
	public OFunctionResult validateInvestigationSuspension(ODataMessage odm,
			OFunctionParms functionParms, OUser loggedUser) {
		OFunctionResult oFR = new OFunctionResult();
		try {
			// Update Emp Sal Elem.
			EmpInvestigation request = (EmpInvestigation) odm.getData().get(0);
			EmpInvestigation empInvestigation = request.getEmpInvestigation(), susInvestigation;
			List<String> conditions = new ArrayList<String>();
			if (request.getStatus().getCode().equalsIgnoreCase("Suspend")) {
				request.setSuspensionCancelled("N");

				conditions.add("empInvestigation.dbid ="
						+ empInvestigation.getDbid());
				conditions.add("status.code = 'suspend'");
				conditions.add("suspensionCancelled != 'Y'");

				susInvestigation = (EmpInvestigation) oem.loadEntity(
						"EmpInvestigation", conditions, null, loggedUser);

				if (susInvestigation != null) {
					oFR.addError(usrMsgService.getUserMessage("Suspend001",
							loggedUser));
				}
			}
		} catch (Exception ex) {
			OLog.logError(ex.toString(), loggedUser);

		}
		return oFR;
	}
}