/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author nkhalil
 */
@Local
public interface IDPValidationLocal {
    public OFunctionResult validateEDSIDPBusinessOutcomeDate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateLearningInterventionDate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validatePlannedActionsDate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult setPersonalEmployeeCompetencies(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult setEmployeeProfilerAssessorConsultantType(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult postCreateIDP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateHRManagerInEDSSetup (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult postCreateEmployeeProfiler(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult jobGapPostAction (ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);
    public OFunctionResult validateBusinessOutcome(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);    
    public com.unitedofoq.fabs.core.function.OFunctionResult gapUserExit(com.unitedofoq.fabs.core.datatype.ODataMessage oDM, com.unitedofoq.fabs.core.function.OFunctionParms functionParms, com.unitedofoq.fabs.core.security.user.OUser loggedUser);
}
