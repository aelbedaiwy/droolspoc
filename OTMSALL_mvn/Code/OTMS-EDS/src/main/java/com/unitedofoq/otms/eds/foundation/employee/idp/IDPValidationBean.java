package com.unitedofoq.otms.eds.foundation.employee.idp;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntityServiceLocal;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.entitysetup.OEntity;
import com.unitedofoq.fabs.core.entitysetup.OEntityAction;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.setup.FABSSetup;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.uiframework.UIFrameworkServiceRemote;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.eds.EDSJobSetup;
import com.unitedofoq.otms.eds.foundation.employee.EDSEmployee;
import com.unitedofoq.otms.eds.foundation.employee.EDSEmployeeCompetency;
import com.unitedofoq.otms.eds.foundation.employee.EmployeeProfiler;
import com.unitedofoq.otms.eds.foundation.employee.EmployeeProfilerAssessor;
import com.unitedofoq.otms.eds.foundation.employee.EmployeeProfilerAssessorConsultant;
import com.unitedofoq.otms.eds.foundation.employee.EmployeeProfilerAssessorEmployee;
import com.unitedofoq.otms.eds.foundation.job.EDSJob;
import com.unitedofoq.otms.eds.foundation.job.EDSJobCompetency;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.eds.foundation.employee.idp.IDPValidationLocal",
beanInterface = IDPValidationLocal.class)
public class IDPValidationBean implements IDPValidationLocal {

    @EJB
    private UIFrameworkServiceRemote uIFrameworkService;
    @EJB
    private EntityServiceLocal entityServiceLocal;
    @EJB
    private UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;

    @Override
    public OFunctionResult validateEDSIDPBusinessOutcomeDate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EDSIDPBusinessOutcome businessOutcome = (EDSIDPBusinessOutcome) odm.getData().get(0);
            EDSIDP edsidp = businessOutcome.getIdp();
            if (businessOutcome.getWhen() != null && businessOutcome.getWhen().before(edsidp.getEffectiveDate())) {
                OLog.logError("When date cannot be before IDP effective date.", loggedUser,
                        "Business Outcome", businessOutcome,
                        "EDSIDP", edsidp);
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "BOEndDateError_1", systemUser));
                return oFR;
            }
            if (edsidp.getEndDate() != null && businessOutcome.getWhen() != null && businessOutcome.getWhen().after(edsidp.getEndDate())) {
                OLog.logError("When date cannot be After IDP end date.", loggedUser,
                        "Business Outcome", businessOutcome,
                        "EDSIDP", edsidp);
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "BOEndDateError_2", systemUser));
                return oFR;
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
        } 
        return oFR;
    }

    public OFunctionResult validateLearningInterventionDate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EDSIDPBusinessOutcomeForOutLrnIntr intr = (EDSIDPBusinessOutcomeForOutLrnIntr) odm.getData().get(0);
            EDSIDPBusinessOutcome businessOutcome = intr.getBusinessOutcome();
            EDSIDP edsidp = businessOutcome.getIdp();
            if (intr.getWhenIntr() != null
                    && businessOutcome.getWhen() != null
                    && intr.getWhenIntr().after(businessOutcome.getWhen())) {
                OLog.logError("(When) to be not after (When) in (Bus.Outcome) Entity.", loggedUser,
                        "Learning Intervention", intr,
                        "Business Outcome", businessOutcome);
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "LearningInvetionValidation", systemUser));
                return oFR;
            }
            if (edsidp.getEndDate() != null
                    && intr.getWhenIntr() != null
                    && intr.getWhenIntr().after(edsidp.getEndDate())) {
                OLog.logError("(When) to be not after (Plan End Date) in (EDSIDP) Entity.", loggedUser,
                        "Learning Intervention", intr,
                        "EDIDP", edsidp);
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "LearningInvetionValidation", systemUser));
                return oFR;
            }

        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
        } 
        return oFR;
    }

    @Override
    public OFunctionResult validatePlannedActionsDate(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EDSIDPActionForBusinessOutcome businessOutcomeAction = (EDSIDPActionForBusinessOutcome) odm.getData().get(0);
            EDSIDPBusinessOutcomeForOutLrnIntr intr = businessOutcomeAction.getChannelLrnIntr();
            EDSIDPBusinessOutcome businessOutcome = intr.getBusinessOutcome();
            EDSIDP edsidp = businessOutcome.getIdp();
            if (businessOutcomeAction.getPlannedStartDate().after(intr.getWhenIntr())) {
                OLog.logError("(Start Date to be not after (When) in (Learning Intervention) Entity.", loggedUser,
                        "Learning Intervention", intr,
                        "EDSIDPActionForBusinessOutcome", businessOutcomeAction);
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "The Learning Action Date must be after the Action Start Date.", systemUser));
                return oFR;
            }

            if (edsidp.getEndDate() != null && businessOutcomeAction.getPlannedEndDate().after(edsidp.getEndDate())) {
                OLog.logError("End Date Cannot be after (Plan End Date) in (EDSIDP) Entity.", loggedUser,
                        "EDSIDPActionForBusinessOutcome", businessOutcomeAction,
                        "EDIDP", edsidp);
                oFR.addError(userMessageServiceRemote.getUserMessage(
                        "ActionValidation_1", systemUser));
                return oFR;
            }

        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult setPersonalEmployeeCompetencies(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            EmployeeProfiler employeeProfiler = (EmployeeProfiler) odm.getData().get(0);
            EDSEmployee employee = employeeProfiler.getEmployee();
            EDSJob job = employee.getJob();
            List<EDSJobCompetency> jobCompetencies = job.getJobCompetencies();
            List<BaseEntity> employeeCompetencies = new ArrayList<BaseEntity>();
            for (int compIdx = 0; compIdx < jobCompetencies.size(); compIdx++) {
                EDSEmployeeCompetency employeeCompetency = new EDSEmployeeCompetency();
                employeeCompetency.setEmployeeProfiler(employeeProfiler);
                employeeCompetency.setCompetency(jobCompetencies.get(compIdx).getCompetency());
                employeeCompetencies.add(employeeCompetency);
            }
            oem.saveEntityList(employeeCompetencies, loggedUser);
            return oFR;
        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult setEmployeeProfilerAssessorConsultantType(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        OUser systemUser = oem.getSystemUser(loggedUser);
        try {
            List<String> conditions = new ArrayList<String>();
            conditions.add("type.dbid=36051");
            conditions.add("dbid=159652");
            UDC type = (UDC) oem.loadEntity("UDC", conditions, null, systemUser);
            EmployeeProfilerAssessorConsultant assessorConsultant = (EmployeeProfilerAssessorConsultant) odm.getData().get(0);
            assessorConsultant.setType(type);
            oem.saveEntity(assessorConsultant, loggedUser);
            return oFR;
        } catch (Exception e) {
            OLog.logException(e, loggedUser,
                    "ODataMessage", odm);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", systemUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult postCreateIDP(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();

        long selfdbid = 1000002849;
        long firstMdbid = 1000002846;
        long secondMdbid = 1000002848;
        long hrMdbid = 1000002847;


        //Setup parameter Key
        String hrManagerParameterKey = "HRMANAGER";
        EDSIDP idp = (EDSIDP) odm.getData().get(0);
        try {
            OEntity edsIDPAccOEntity = entitySetupService.loadOEntity(
                    EDSIDPAccountability.class.getName(), oem.getSystemUser(loggedUser));
            OEntityAction edsIDPAccCreateAction = edsIDPAccOEntity.getCreateAction();

            ODataMessage dataTypeDM;
            UDC accType;

            if (idp.isSelf()) {
                EDSIDPAccountability selfAccount = new EDSIDPAccountability();
                selfAccount.setEmployee(idp.getEmployee());
                selfAccount.setIdp(idp);
                accType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), selfdbid, null, null, oem.getSystemUser(loggedUser));
                selfAccount.setType(accType);
                dataTypeDM = entityServiceLocal.constructEntityODataMessage(selfAccount, oem.getSystemUser(loggedUser));
                oFR.append(entitySetupService.executeAction(edsIDPAccCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
            }

            if (idp.isFirstManager()) {
                EDSIDPAccountability firstMAcc = new EDSIDPAccountability();
                //TODO EDS-Update
                firstMAcc.setIdp(idp);
                accType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), firstMdbid, null, null, oem.getSystemUser(loggedUser));
                firstMAcc.setType(accType);
                dataTypeDM = entityServiceLocal.constructEntityODataMessage(firstMAcc, oem.getSystemUser(loggedUser));
                oFR.append(entitySetupService.executeAction(edsIDPAccCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
            }

            if (idp.isSecondManager()) {
                EDSIDPAccountability secondMAcc = new EDSIDPAccountability();
                //TODO EDS-Update
                secondMAcc.setIdp(idp);
                accType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), secondMdbid, null, null, oem.getSystemUser(loggedUser));
                secondMAcc.setType(accType);
                dataTypeDM = entityServiceLocal.constructEntityODataMessage(secondMAcc, oem.getSystemUser(loggedUser));
                oFR.append(entitySetupService.executeAction(edsIDPAccCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
            }

            if (idp.isHrManager()) {
                EDSIDPAccountability hrMAcc = new EDSIDPAccountability();
                List<String> condtions = new ArrayList<String>();
                condtions.add("setupKey = '" + hrManagerParameterKey + "'");
                // FIXME: possibility to get NonUniqueResultException(); manage that
                FABSSetup fabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        condtions, null, oem.getSystemUser(loggedUser));


                EDSJob job = null;
                if (fabsSetup != null && fabsSetup.getSvalue() != null && !fabsSetup.getSvalue().equals("")) {
                    try {
                        long edsJobID = Long.parseLong(fabsSetup.getSvalue().toString());
                        job = (EDSJob) oem.loadEntity(EDSJob.class.getSimpleName(),
                                edsJobID, null, null, oem.getSystemUser(loggedUser));

                    } catch (NumberFormatException ex) {
                        OLog.logError("EDSJob Setup value error", loggedUser);
                    }
                }

                if (job != null) {
                    if (job.getEmployees() != null) {
                        EDSEmployee hrMgr = null;
                        for (EDSEmployee emp : job.getEmployees()) {
                            if (!emp.isInActive()) {
                                hrMgr = (EDSEmployee) emp;
                                break;
                            }
                        }
                        if (hrMgr != null) {
                            //TODO EDS-Update
                        }
                    }
                }

                hrMAcc.setIdp(idp);
                accType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), hrMdbid, null, null, oem.getSystemUser(loggedUser));
                hrMAcc.setType(accType);
                dataTypeDM = entityServiceLocal.constructEntityODataMessage(hrMAcc, oem.getSystemUser(loggedUser));
                oFR.append(entitySetupService.executeAction(edsIDPAccCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
            }

            oFR.addReturnValue(idp);
            return oFR;
        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "UserNotAuthorized", loggedUser));
            return oFR;
        }
    }

    /**
     * Validate Only One HR Manager in the EDS Job Setup (Job Family) <br>If
     * 'count EDSJobSetup.jobFamily=hrMFamilydbid' if count > 1 display Error
     * User Message for Duplicate HR manager has {@link #DELENTITY_DELTREEKEY},
     *
     * @param odm
     * @param functionParms
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult validateHRManagerInEDSSetup(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        long hrMFamilydbid = 38302;
        OFunctionResult oFR = new OFunctionResult();
        List<EDSJobSetup> edsJob = (List<EDSJobSetup>) odm.getData().get(1);
        int count = 0;
        for (int i = 0; i < edsJob.size(); i++) {
            if (edsJob.get(i).getJobFamily().getDbid() == hrMFamilydbid) {
                count++;
            }
        }

        if (count > 1) {
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "DuplicateValue", loggedUser));
            return oFR;
        }
        return oFR;
    }

    @Override
    public OFunctionResult postCreateEmployeeProfiler(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult oFR = new OFunctionResult();

        long selfdbid = 1000002849;
        long firstMdbid = 1000002846;
        long secondMdbid = 1000002848;
        long hrMdbid = 1000002847;
        //Setup parameter Key
        String hrManagerParameterKey = "HRMANAGER";
        EmployeeProfiler employeeProfiler = (EmployeeProfiler) odm.getData().get(0);
        try {
            OEntity employeAssessorOEntity = entitySetupService.loadOEntity(
                    EmployeeProfilerAssessor.class.getName(), oem.getSystemUser(loggedUser));
            OEntityAction assessorCreateAction = employeAssessorOEntity.getCreateAction();

            ODataMessage dataTypeDM;
            UDC assessType;

            if (employeeProfiler.isEmpAssossorSelf()) {
                EmployeeProfilerAssessorEmployee selfAssessor = new EmployeeProfilerAssessorEmployee();
                //TODO EDS-Update
                selfAssessor.setEmployeeProfiler(employeeProfiler);
                assessType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), selfdbid, null, null, oem.getSystemUser(loggedUser));
                selfAssessor.setType(assessType);
                dataTypeDM = entityServiceLocal.constructEntityODataMessage(selfAssessor, oem.getSystemUser(loggedUser));
                oFR.append(entitySetupService.executeAction(assessorCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
            }

            if (employeeProfiler.isEmpAssossorLineManger()) {
                EmployeeProfilerAssessorEmployee lineAssessor = new EmployeeProfilerAssessorEmployee();
                if (employeeProfiler.getEmployee().getFirstLevelManager() != null) {
                    //TODO EDS-Update
                    lineAssessor.setEmployeeProfiler(employeeProfiler);
                    assessType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), firstMdbid, null, null, oem.getSystemUser(loggedUser));
                    lineAssessor.setType(assessType);
                    dataTypeDM = entityServiceLocal.constructEntityODataMessage(lineAssessor, oem.getSystemUser(loggedUser));
                    oFR.append(entitySetupService.executeAction(assessorCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
                } else {
                    UserMessage errMsg = userMessageServiceRemote.getUserMessage("FManagerAssessorError", loggedUser);
                    oFR.addError(errMsg);
                }
            }

            if (employeeProfiler.isEmpAssossorSecondManger()) {
                EmployeeProfilerAssessorEmployee secondAssessor = new EmployeeProfilerAssessorEmployee();
                if (employeeProfiler.getEmployee().getSecondLevelManager() != null) {
                    //TODO EDS-Update
                    secondAssessor.setEmployeeProfiler(employeeProfiler);
                    assessType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), secondMdbid, null, null, oem.getSystemUser(loggedUser));
                    secondAssessor.setType(assessType);
                    dataTypeDM = entityServiceLocal.constructEntityODataMessage(secondAssessor, oem.getSystemUser(loggedUser));
                    oFR.append(entitySetupService.executeAction(assessorCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
                } else {
                    UserMessage errMsg = userMessageServiceRemote.getUserMessage("SManagerAssessorError", loggedUser);
                    oFR.addError(errMsg);
                }
            }

            if (employeeProfiler.isEmpAssossorHrManger()) {
                EmployeeProfilerAssessorEmployee hrAssessor = new EmployeeProfilerAssessorEmployee();
                List<String> condtions = new ArrayList<String>();
                condtions.add("setupKey = '" + hrManagerParameterKey + "'");
                // FIXME: possibility to get NonUniqueResultException(); manage that
                FABSSetup fabsSetup = (FABSSetup) oem.loadEntity(FABSSetup.class.getSimpleName(),
                        condtions, null, oem.getSystemUser(loggedUser));


                EDSJob job = null;
                if (fabsSetup != null && fabsSetup.getSvalue() != null && !fabsSetup.getSvalue().equals("")) {
                    try {
                        long edsJobID = Long.parseLong(fabsSetup.getSvalue().toString());
                        job = (EDSJob) oem.loadEntity(EDSJob.class.getSimpleName(),
                                edsJobID, null, null, oem.getSystemUser(loggedUser));

                    } catch (NumberFormatException ex) {
                        OLog.logError("EDSJob Setup value error", loggedUser);
                    }
                }

                if (job != null) {
                    if (job.getEmployees() != null) {
                        EDSEmployee hrMgr = null;
                        for (EDSEmployee emp : job.getEmployees()) {
                            if (!emp.isInActive()) {
                                hrMgr = (EDSEmployee) emp;
                                break;
                            }
                        }
                        if (hrMgr != null) {
                            //TODO EDS-Update
                        }
                    }
                }

                hrAssessor.setEmployeeProfiler(employeeProfiler);
                assessType = (UDC) oem.loadEntity(UDC.class.getSimpleName(), hrMdbid, null, null, oem.getSystemUser(loggedUser));
                hrAssessor.setType(assessType);
                dataTypeDM = entityServiceLocal.constructEntityODataMessage(hrAssessor, oem.getSystemUser(loggedUser));
                oFR.append(entitySetupService.executeAction(assessorCreateAction, dataTypeDM, new OFunctionParms(), loggedUser));
            }

            oFR.addReturnValue(employeeProfiler);
            return oFR;
        } catch (Exception e) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(e, loggedUser);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "UserNotAuthorized", loggedUser));
            return oFR;
        }
    }

    @Override
    public OFunctionResult jobGapPostAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            //
            EDSIDPForJobGap iDPForJobGap = (EDSIDPForJobGap) odm.getData().get(0);

            List<EDSEmployeeCompetency> competencies = iDPForJobGap.getEmployeeProfiler().getEdsEmployeeCompetencies();


            OEntity edsCompGapOentity = entitySetupService.loadOEntity(EmployeeCompetencyGap.class.getName(), loggedUser);
            OEntityAction createAction = edsCompGapOentity.getCreateAction();


            for (int i = 0; i < competencies.size(); i++) {

                EDSEmployeeCompetency competency = competencies.get(i);
                if (competency.isInActive()) {
                    continue;
                }

                if (competency.getRankGap() > 0) {
                    EmployeeCompetencyGap competencyGap = new EmployeeCompetencyGap();
                    competencyGap.setJobGap(iDPForJobGap);
                    competencyGap.setGapCompetency(competency);

                    competencyGap.setBehaviorNeeded(competency.getJobCompetency().getCompetencyLevel().getBehaviorIndicators());

                    ODataMessage gapDM = entityServiceLocal.constructEntityODataMessage(competencyGap, loggedUser);
                    oFR.append(entitySetupService.executeAction(createAction, gapDM, functionParms, loggedUser));
                }
            }

        } catch (Exception ex) {
            OLog.logError("Error In Job Gap Post Action", loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult validateBusinessOutcome(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser) {
        if (odm.getData().size() < 2) {
            return getCorruptedDataErrorMessage(loggedUser);
        }

        OFunctionResult oFR = new OFunctionResult();
        List<EDSIDPBusinessOutcome> businessOutcomes = (List<EDSIDPBusinessOutcome>) odm.getData().get(1);
        double overallPercentage = 0;
        for (int i = 0; i < businessOutcomes.size(); i++) {
            overallPercentage += businessOutcomes.get(i).getContributionPercentage();
        }

        if (!(overallPercentage == 0 || overallPercentage == 100)) {
            oFR.addError(userMessageServiceRemote.getUserMessage(
                    "BOutComePercentError", loggedUser));
        }
        return oFR;
    }

    private OFunctionResult getCorruptedDataErrorMessage(OUser loggedUser) {
        UserMessage errorMsg = userMessageServiceRemote.getUserMessage("CorruptedData", loggedUser);
        OFunctionResult functionResult = new OFunctionResult();
        if (errorMsg != null) {
            functionResult.addError(errorMsg);
        }
        return functionResult;
    }

    @Override
    public OFunctionResult gapUserExit(
            ODataMessage oDM, OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult oFR = new OFunctionResult();
        try {
            List<String> conditions = new ArrayList<String>();
            OFunctionResult constructFR = uIFrameworkService.constructScreenDataLoadingUEConditionsOFR(conditions, loggedUser);
            oFR.append(constructFR);
            oFR.setReturnedDataMessage(constructFR.getReturnedDataMessage());

            return oFR;

        } catch (Exception ex) {
            // <editor-fold defaultstate="collapsed" desc="Log">
            OLog.logException(ex, loggedUser,
                    "odm", oDM);
            // </editor-fold>
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser), ex);

        } finally {
            return oFR;
        }
    }
}