package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.timemanagement.employee.EmployeeWorkingCalHistory;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

@Local
public interface CalendarServiceLocal {

    public OFunctionResult updateWorkingCalendarAfterModifyOrCancel(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult insertIntoReflection(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult attendanceChangeAction(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult assignAnnualPlanner(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult updateAnnualPlanner(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult postImportedDAs(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult importDAValidation(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postEmployeeOTRequest(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateEmployeeOTRequest(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult workingCalendarUpdate(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateWorkingCalendarUpdate(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateNormalWorkingCalendar(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateTMCalendar(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateNumOfRegularCal(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult setDAForNonExist(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult setPlannerForNonExist(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateWorkingCalHistory(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateShift(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult cancelEmployeeOTRequest(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateOTReqUpdate(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult setTM(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult setPlannerTransaction(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult postSdk(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postImported(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateExcuseVacation(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public List<String> getEmployeeVacations(Date start, Date end, int currentYear);

    public List<Date> getDateFromDay(Date startDate, Date endDate, int dayOffIndex);

    public List<Date> getHolidayDates(Date from, Date to);

    public Object[] getShift(TMCalendar rotationCal, Date startDate, OUser loggedUser);

    public void setCalendarsMap(Map<Integer, NormalWorkingCalendar> rotCalsMap, List<NormalWorkingCalendar> cals, int roundDuration);

    public OFunctionResult postManualEntry(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult postImportedDAsME(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult createEmpCalHistoryOnHire(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public NormalWorkingCalendar getShftFromSequence(TMCalendar calendar, int rotSeq, OUser user);

    public OFunctionResult postSdkSingleEmp(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult workingCalendarUpdateAll(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateCalendarHistoryCases(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult postSdkScheduledServerJob(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postConsolidationForSingleEmployee(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult postConsolidation(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult reSetEmployeeTMImportFromMachineEmp(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult reSetEmployeeTMImportEmp(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult reSetEmployeeTMImportFromMachine(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult fixEmployeeTMEntry(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult reSetEmployeeTMImport(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult getPlannedInAndPlannedOut(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public TMCalendar getEmpCalendar(Date currDate, Employee employee, OUser loggedUser);

    public NormalWorkingCalendar getEmployeeNormalCalendar(Employee employee, TMCalendar tmCalendar,
            Date currDate, OUser loggedUser);

    public String[] getFromPayrollParameter(String description, OUser loggedUser);

    public OFunctionResult postDeductedAnnualAndPenaltyDaysSingleEmp(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult postDeductedAnnualAndPenaltyDaysAllEmp(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult validateOTRequest(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser);

    public OFunctionResult importOTRequest(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateEmployeeWorkingCalHistory(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateTMCalendarFormat(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateNormalWorkingCalendarFormat(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateOTSegmentFormat(ODataMessage odm, OFunctionParms params, OUser loggedUser);

    public OFunctionResult importMultiEntryAttendance(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult calendarUpdate(OFunctionResult ofr,
            EmployeeWorkingCalHistory calHistory, Boolean check, OUser loggedUser, Boolean deleted);
}
