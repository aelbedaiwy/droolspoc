package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.PayrollParameter;
import com.unitedofoq.otms.payroll.foundation.employee.EmployeeSalaryElement;
import com.unitedofoq.otms.payroll.salaryelement.SalaryElement;
import com.unitedofoq.otms.personnel.absence.Absence;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendance;
import com.unitedofoq.otms.timemanagement.employee.EmployeeMonthlyConsolidation;
import com.unitedofoq.otms.timemanagement.employee.EmployeeOTImport;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.timemanagement.TimeManagementServiceLocal",
        beanInterface = TimeManagementServiceLocal.class)
public class TimeManagementService implements TimeManagementServiceLocal {

    @EJB
    UserMessageServiceRemote userMessageServiceRemote;
    @EJB
    OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private DataSecurityServiceLocal dataSecurityServiceLocal;

    DateTimeUtility dateTimeUtility = new DateTimeUtility();

    @Override
    public OFunctionResult monthlyConsolidationSinglePost(ODataMessage odm,
            OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeMonthlyConsolidation consolidation = (EmployeeMonthlyConsolidation) odm.getData().get(0);
            Employee employee;
            employee = consolidation.getEmployee();
            CalculatedPeriod calculatedPeriod;
            List<String> cutOffConditions = new ArrayList<String>();
            cutOffConditions.add("company.dbid = " + employee.getPositionSimpleMode().getUnit().getCompany().getDbid());
            cutOffConditions.add("description = 'day_of_calculation'");
            PayrollParameter cutOffParameter = null;
            try {
                cutOffParameter = (PayrollParameter) oem.loadEntity(
                        PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            Integer cutOffStart = null,
                    cutOffEnd = null;

            String calcCurrentMonth = null,
                    calcFullMonth = null;
            //calcCurrentMonth
            cutOffConditions.clear();
            cutOffConditions.add("company.dbid = " + employee.getPositionSimpleMode().getUnit().getCompany().getDbid());
            cutOffConditions.add("description = 'calculation_currentMonth'");
            PayrollParameter calcCurrentMonthParam = null;
            try {
                calcCurrentMonthParam = (PayrollParameter) oem.loadEntity(
                        PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (calcCurrentMonthParam != null) {
                calcCurrentMonth = calcCurrentMonthParam.getValueData();
            }

            //calcFullMonth
            cutOffConditions.clear();
            cutOffConditions.add("company.dbid = " + employee.getPositionSimpleMode().getUnit().getCompany().getDbid());
            cutOffConditions.add("description = 'calculation_fullMonth'");
            PayrollParameter calcFullMonthParam = null;
            try {
                calcFullMonthParam = (PayrollParameter) oem.loadEntity(
                        PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (calcFullMonthParam != null) {
                calcFullMonth = calcFullMonthParam.getValueData();
            }
            calculatedPeriod = consolidation.getCalculatedPeriod();
            Integer perviousMonth = calculatedPeriod.getMonth();
            Integer perviousYear = calculatedPeriod.getYear();

            String calcYear = String.valueOf(calculatedPeriod.getYear());
            String calcMonth = String.valueOf(calculatedPeriod.getMonth());
            if (calcMonth.length() == 1) {
                calcMonth = "0".concat(calcMonth);
            }

            int calcYearVal, calcMonthVal;
            calcYearVal = Integer.parseInt(calcYear);
            calcMonthVal = Integer.parseInt(calcMonth);

            if (calcFullMonth != null && calcFullMonth.equals("Y")) { // from 1->30
                cutOffStart = 1;
                cutOffEnd = dateTimeUtility.getLastDayInMonth(calculatedPeriod.getYear(), calculatedPeriod.getMonth());
                if ((calcCurrentMonth != null && calcCurrentMonth.equals("N")) || // the prev month
                        (calcFullMonth != null && calcFullMonth.equals("N"))) {
                    perviousMonth = calculatedPeriod.getMonth() - 1;
                    if (calculatedPeriod.getMonth() == 1) {
                        perviousMonth = 12;
                        perviousYear = calculatedPeriod.getYear() - 1;
                    }
                }
            } else {
                cutOffStart = Integer.parseInt(cutOffParameter.getValueData()) + 1;
                cutOffEnd = Integer.parseInt(cutOffParameter.getAuxiliary()) + 1;

                perviousMonth = calculatedPeriod.getMonth() - 1;
                if (calculatedPeriod.getMonth() == 1) {
                    perviousMonth = 12;
                    perviousYear = calculatedPeriod.getYear() - 1;
                }
            }

            String endDateStr = calcYearVal + "-" + calcMonthVal + "-" + cutOffEnd;
            String startDateStr = perviousYear.toString() + "-" + perviousMonth.toString() + "-" + cutOffStart;

            oFR.append(postMonthlyConsolidationForEmployee(consolidation, startDateStr, endDateStr, loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult monthlyConsolidationPost(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {

            CalculatedPeriod calculatedPeriod = ((EmployeeMonthlyConsolidation) odm.getData().get(0)).getCalculatedPeriod();
            Company company = ((EmployeeMonthlyConsolidation) odm.getData().get(0)).getEmployee().getPositionSimpleMode().getUnit().getCompany();

            if (calculatedPeriod == null || company == null) {
                return oFR;
            }

            List<String> conds = new ArrayList<String>();
            conds.add("posted = 'N'");
            conds.add("employee.positionSimpleMode.unit.company.dbid = "
                    + company.getDbid());
            conds.add("calculatedPeriod.dbid = " + calculatedPeriod.getDbid());
            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            if (!employeeDbidList.isEmpty()) {
                String employeeDbids = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                String securityCond = "employee.dbid in " + employeeDbids;
                conds.add(securityCond);
            }
            List<EmployeeMonthlyConsolidation> consolidations = oem.loadEntityList(
                    EmployeeMonthlyConsolidation.class.getSimpleName(), conds, null,
                    null, loggedUser);

            List<String> cutOffConditions = new ArrayList<String>();
            cutOffConditions.add("company.dbid = " + company.getDbid());
            cutOffConditions.add("description = 'day_of_calculation'");
            PayrollParameter cutOffParameter = null;
            try {
                cutOffParameter = (PayrollParameter) oem.loadEntity(
                        PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            Integer cutOffStart = null,
                    cutOffEnd = null;

            String calcCurrentMonth = null,
                    calcFullMonth = null;

            //calcCurrentMonth
            cutOffConditions.clear();
            cutOffConditions.add("company.dbid = " + company.getDbid());
            cutOffConditions.add("description = 'calculation_currentMonth'");
            PayrollParameter calcCurrentMonthParam = null;
            try {
                calcCurrentMonthParam = (PayrollParameter) oem.loadEntity(
                        PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (calcCurrentMonthParam != null) {
                calcCurrentMonth = calcCurrentMonthParam.getValueData();
            }

            //calcFullMonth
            cutOffConditions.clear();
            cutOffConditions.add("company.dbid = " + company.getDbid());
            cutOffConditions.add("description = 'calculation_fullMonth'");
            PayrollParameter calcFullMonthParam = null;
            try {
                calcFullMonthParam = (PayrollParameter) oem.loadEntity(
                        PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (calcFullMonthParam != null) {
                calcFullMonth = calcFullMonthParam.getValueData();
            }

            Integer perviousMonth = calculatedPeriod.getMonth();
            Integer perviousYear = calculatedPeriod.getYear();

            String calcYear = String.valueOf(calculatedPeriod.getYear());
            String calcMonth = String.valueOf(calculatedPeriod.getMonth());

            int calcYearVal, calcMonthVal;
            calcYearVal = Integer.parseInt(calcYear);
            calcMonthVal = Integer.parseInt(calcMonth);

            if (calcFullMonth != null && calcFullMonth.equals("Y")) { // from 1->30
                cutOffStart = 1;
                cutOffEnd = dateTimeUtility.getLastDayInMonth(calculatedPeriod.getYear(), calculatedPeriod.getMonth());
                if ((calcCurrentMonth != null && calcCurrentMonth.equals("N")) || // the prev month
                        (calcFullMonth != null && calcFullMonth.equals("N"))) {
                    perviousMonth = calculatedPeriod.getMonth() - 1;
                    if (calculatedPeriod.getMonth() == 1) {
                        perviousMonth = 12;
                        perviousYear = calculatedPeriod.getYear() - 1;
                    }
                }
            } else if (calcFullMonth != null && calcFullMonth.equals("N")) {
                cutOffStart = Integer.parseInt(cutOffParameter.getValueData()) + 1;
                cutOffEnd = Integer.parseInt(cutOffParameter.getAuxiliary()) + 1;

                perviousMonth = calculatedPeriod.getMonth() - 1;
                if (calculatedPeriod.getMonth() == 1) {
                    perviousMonth = 12;
                    perviousYear = calculatedPeriod.getYear() - 1;
                }
            }

            String endDateStr = calcYearVal + "-" + calcMonthVal + "-" + cutOffEnd;
            String startDateStr = perviousYear.toString() + "-" + perviousMonth.toString() + "-" + cutOffStart;

            consolidations.addAll(createMonthlyConsForNonTMEnt(calculatedPeriod, startDateStr, endDateStr, loggedUser));

            for (int i = 0; i < consolidations.size(); i++) {
                EmployeeMonthlyConsolidation consolidation = consolidations.get(i);
                oFR.append(postMonthlyConsolidationForEmployee(consolidation, startDateStr, endDateStr, loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    private List<EmployeeMonthlyConsolidation> createMonthlyConsForNonTMEnt(CalculatedPeriod calculatedPeriod, String startDay, String endDay, OUser loggeddUser) {
        List<EmployeeMonthlyConsolidation> consolidations = new ArrayList<EmployeeMonthlyConsolidation>();

        String select = "select oemployee.dbid from oemployee "
                + " join employeepayroll on oemployee.dbid = employeepayroll.employee_dbid and employeepayroll.tmEntitled = 'N' "
                + " where oemployee.deleted  = 0 and oemployee.dbid not in "
                + " (select employee_dbid from employeemonthlyconsolidation where calculatedPeriod_dbid = " + calculatedPeriod.getDbid() + ")";

        String selectWD = "select count(dbid) from employeedailyattendance where dailydate <= '" + endDay + "' and dailydate >= " + "'" + startDay + "' "
                + "and daynature_dbid in (select dbid from daynatures where daynatures.type = 'W') and deleted = 0 and employee_dbid = ";;
        String selectAD = "select count(dbid) from employeedailyattendance where dailydate <= '" + endDay + "' and dailydate >= " + "'" + startDay + "' "
                + "and daynature_dbid in (select dbid from daynatures where daynatures.type = 'A') and deleted = 0 and employee_dbid = ";;
        String selectVD = "select count(dbid) from employeedailyattendance where dailydate <= '" + endDay + "' and dailydate >= " + "'" + startDay + "' "
                + "and daynature_dbid in (select dbid from daynatures where daynatures.type = 'V') and deleted = 0 and employee_dbid = ";;

        try {
            List empDbids = (List) oem.executeEntityListNativeQuery(select, loggeddUser);
            Employee emp;
            Long empDbid;
            EmployeeMonthlyConsolidation empMonth = null;
            Object tempRet;

            if (empDbids != null && empDbids.size() > 0) {
                for (int i = 0; i < empDbids.size(); i++) {
                    empDbid = Long.parseLong(empDbids.get(i).toString());
                    emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggeddUser);
                    if (emp != null) {

                        empMonth = new EmployeeMonthlyConsolidation();
                        empMonth.setEmployee(emp);
                        empMonth.setCalculatedPeriod(calculatedPeriod);

                        tempRet = oem.executeEntityNativeQuery(selectWD + empDbid, loggeddUser);
                        if (tempRet != null) {
                            empMonth.setWorkingDays(new BigDecimal(tempRet.toString()));
                        }

                        tempRet = oem.executeEntityNativeQuery(selectAD + empDbid, loggeddUser);
                        if (tempRet != null) {
                            empMonth.setAbsenceDays(new BigDecimal(tempRet.toString()));
                        }

                        tempRet = oem.executeEntityNativeQuery(selectVD + empDbid, loggeddUser);
                        if (tempRet != null) {
                            empMonth.setVacations(new BigDecimal(tempRet.toString()));
                        }

                        oem.saveEntity(empMonth, loggeddUser);
                        consolidations.add(empMonth);
                    }
                }
            }
            return consolidations;
        } catch (Exception exception) {
            OLog.logException(exception, loggeddUser);
        } finally {
            return consolidations;
        }
    }

    private OFunctionResult postMonthlyConsolidationForEmployee(EmployeeMonthlyConsolidation consolidation, String startDateStr, String endDateStr, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            if (consolidation.getPosted().equals("Y")) {
                oFR.addError(userMessageServiceRemote.getUserMessage("PostedConsolidation", Collections.singletonList(consolidation.getEmployee().getCode()), loggedUser));
            }
            DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Employee employee = consolidation.getEmployee();
            CalculatedPeriod calculatedPeriod = consolidation.getCalculatedPeriod();
            if (calculatedPeriod != null) {
                //Loubna - 08/09/2013 - for rotation & shift cases (monthly consolidation), changing the Rule to be on TMCalendar
                TMCalendar tmCalendar = employee.getCalendar();
                List<Rule> rules = tmCalendar.getRules();

                if (rules.isEmpty()) {
                    List<String> params = new ArrayList<String>();
                    params.add(consolidation.getEmployee().getCode());
                    params.add(consolidation.getEmployee().getCalendar().getName());
                    oFR.addError(userMessageServiceRemote.getUserMessage("TMConsolidation_001", params, loggedUser));
                    return oFR;
                }

                List<String> monthlyConditions = new ArrayList<String>();

                monthlyConditions.add("employee.dbid = " + employee.getDbid());
                monthlyConditions.add("dailyDate >= '" + startDateStr + "'");
                monthlyConditions.add("dailyDate <= '" + endDateStr + "'");
                monthlyConditions.add("header = true");
                ArrayList<EmployeeDailyAttendance> dailyAttendances = (ArrayList<EmployeeDailyAttendance>) oem.loadEntityList("EmployeeDailyAttendance", monthlyConditions, null, null, loggedUser);

                for (Rule rule : rules) {
                    Absence delayEffectOnAbsence = rule.getDelayEffectOnAbsence();
                    Absence earlyLeaveEffectOnAbsence = rule.getEarlyLeaveEffectOnAbsence();

                    SalaryElement dayOverTimeEffectOnPayroll = rule.getDayOverTimeEffectOnPayroll();
                    SalaryElement nightOverTimeEffectOnPayroll = rule.getNightOverTimeEffectOnPayroll();
                    SalaryElement weekendOverTimeEffectOnPayroll = rule.getWeekEndEffectOnPayroll();
                    SalaryElement holidayOverTimeEffectOnPayroll = rule.getHolidayEffectOnPayroll();

                    Absence absenceEffectOnAbsence = rule.getAbsenceEffectOnAbsence();

                    EmployeeSalaryElement empSalElement4DayOT = null;
                    EmployeeSalaryElement empSalElement4NightOT = null;
                    EmployeeSalaryElement empSalElement4WeekendOT = null;
                    EmployeeSalaryElement empSalElement4HolidayOT = null;

                    if (dayOverTimeEffectOnPayroll != null) {
                        ArrayList conditions = new ArrayList();
                        conditions.add("employee.dbid = " + employee.getDbid());
                        conditions.add("salaryElement.dbid = " + dayOverTimeEffectOnPayroll.getDbid());

                        empSalElement4DayOT = (EmployeeSalaryElement) oem.loadEntity(
                                "EmployeeSalaryElement", conditions, null, loggedUser);

                        if (empSalElement4DayOT != null) {
                            empSalElement4DayOT.setValue(new BigDecimal(0));
                        }
                    }

                    if (nightOverTimeEffectOnPayroll != null) {
                        ArrayList conditions = new ArrayList();
                        conditions.add("employee.dbid = " + employee.getDbid());
                        conditions.add("salaryElement.dbid = " + nightOverTimeEffectOnPayroll.getDbid());

                        empSalElement4NightOT = (EmployeeSalaryElement) oem.loadEntity(
                                "EmployeeSalaryElement", conditions, null, loggedUser);

                        if (empSalElement4NightOT != null) {
                            empSalElement4NightOT.setValue(new BigDecimal(0));
                        }
                    }

                    if (weekendOverTimeEffectOnPayroll != null) {
                        ArrayList conditions = new ArrayList();
                        conditions.add("employee.dbid = " + employee.getDbid());
                        conditions.add("salaryElement.dbid = " + weekendOverTimeEffectOnPayroll.getDbid());

                        empSalElement4WeekendOT = (EmployeeSalaryElement) oem.loadEntity(
                                "EmployeeSalaryElement", conditions, null, loggedUser);

                        if (empSalElement4WeekendOT != null) {
                            empSalElement4WeekendOT.setValue(new BigDecimal(0));
                        }
                    }

                    if (holidayOverTimeEffectOnPayroll != null) {
                        ArrayList conditions = new ArrayList();
                        conditions.add("employee.dbid = " + employee.getDbid());
                        conditions.add("salaryElement.dbid = " + holidayOverTimeEffectOnPayroll.getDbid());

                        empSalElement4HolidayOT = (EmployeeSalaryElement) oem.loadEntity(
                                "EmployeeSalaryElement", conditions, null, loggedUser);

                        if (empSalElement4HolidayOT != null) {
                            empSalElement4HolidayOT.setValue(new BigDecimal(0));
                        }
                    }

                    // Overtime
                    if (dayOverTimeEffectOnPayroll != null) {
                        if (consolidation.getPostedDayOvertime() != null) {
                            ArrayList conditions = new ArrayList();
                            conditions.add("employee.dbid = " + employee.getDbid());
                            conditions.add("salaryElement.dbid = " + dayOverTimeEffectOnPayroll.getDbid());

                            if (empSalElement4DayOT != null && !consolidation.getPostedDayOvertime().equals(new BigDecimal(0))) {
                                empSalElement4DayOT.setValue(empSalElement4DayOT.getValue().add(consolidation.getPostedDayOvertime()));
                                oFR.append(entitySetupService.callEntityUpdateAction(empSalElement4DayOT, loggedUser));
                            }
                        }
                    }

                    if (nightOverTimeEffectOnPayroll != null) {
                        if (consolidation.getPostedNightOvertime() != null) {
                            ArrayList conditions = new ArrayList();
                            conditions.add("employee.dbid = " + employee.getDbid());
                            conditions.add("salaryElement.dbid = " + nightOverTimeEffectOnPayroll.getDbid());

                            if (empSalElement4NightOT != null) {
                                empSalElement4NightOT.setValue(empSalElement4NightOT.getValue().add(consolidation.getPostedNightOvertime()));
                                oFR.append(entitySetupService.callEntityUpdateAction(empSalElement4NightOT, loggedUser));
                            }
                        }
                    }

                    if (weekendOverTimeEffectOnPayroll != null) {
                        if (consolidation.getPostedWeekend() != null) {
                            ArrayList conditions = new ArrayList();
                            conditions.add("employee.dbid = " + employee.getDbid());
                            conditions.add("salaryElement.dbid = " + weekendOverTimeEffectOnPayroll.getDbid());

                            if (empSalElement4WeekendOT != null) {
                                empSalElement4WeekendOT.setValue(empSalElement4WeekendOT.getValue().add(consolidation.getPostedWeekend()));
                                oFR.append(entitySetupService.callEntityUpdateAction(empSalElement4WeekendOT, loggedUser));
                            }
                        }
                    }

                    if (holidayOverTimeEffectOnPayroll != null) {
                        if (consolidation.getPostedHoliday() != null) {
                            ArrayList conditions = new ArrayList();
                            conditions.add("employee.dbid = " + employee.getDbid());
                            conditions.add("salaryElement.dbid = " + holidayOverTimeEffectOnPayroll.getDbid());

                            if (empSalElement4HolidayOT != null) {
                                empSalElement4HolidayOT.setValue(empSalElement4HolidayOT.getValue().add(consolidation.getPostedHoliday()));
                                oFR.append(entitySetupService.callEntityUpdateAction(empSalElement4HolidayOT, loggedUser));
                            }
                        }
                    }

                    //day absences
                    if (absenceEffectOnAbsence != null) {
                        if (consolidation.getAbsenceDays() != null && !consolidation.getAbsenceDays().equals(new BigDecimal(0))
                                && !consolidation.getAbsenceDays().equals("0E-13")) {
                            double totalAbsenceDays = consolidation.getAbsenceDays().doubleValue();
                            for (EmployeeDailyAttendance dailyAttendance : dailyAttendances) {
                                if (totalAbsenceDays <= 0) {
                                    break;
                                }
                                UDC dayNature = dailyAttendance.getDayNature();
                                if (dayNature != null) {
                                    if (dayNature.getValue().equals("Absence")) {
                                        EmployeeAbsenceRequest absenceRequest = new EmployeeAbsenceRequest();

                                        absenceRequest.setEmployee(employee);
                                        absenceRequest.setAbsence(absenceEffectOnAbsence);
                                        absenceRequest.setStartDate(dailyAttendance.getDailyDate());
                                        absenceRequest.setEndDate(dailyAttendance.getDailyDate());
                                        absenceRequest.setRequestedDate(dailyAttendance.getDailyDate());
                                        absenceRequest.setBalance(new BigDecimal(1));
                                        absenceRequest.setPeriod(new BigDecimal(1));
                                        absenceRequest.setPosted("N");
                                        absenceRequest.setCancelled("N");
                                        absenceRequest.setPostedDays(new BigDecimal(0));

                                        oFR.append(entitySetupService.callEntityCreateAction(absenceRequest, loggedUser));

                                        totalAbsenceDays -= 1;
                                    }
                                }

                            }
                        }
                    }
                    //delay
                    if (delayEffectOnAbsence != null) {
                        if (consolidation.getPostedDelayHours() != null && !consolidation.getPostedDelayHours().equals(new BigDecimal(0))
                                && !consolidation.getPostedDelayHours().equals("0E-13")) {
                            BigDecimal totalDelayHrs = consolidation.getPostedDelayHours();
                            for (EmployeeDailyAttendance dailyAttendance : dailyAttendances) {
                                if (totalDelayHrs.compareTo(new BigDecimal(0)) <= 0) {
                                    break;
                                }
                                BigDecimal delayHoursPeriod = dailyAttendance.getDelayHoursPeriod();
                                if (delayHoursPeriod != null && !delayHoursPeriod.equals(new BigDecimal(0))
                                        && !delayHoursPeriod.toString().equals("0E-13")) {

                                    Date startDate = dailyAttendance.getDailyDate(), endDate = dailyAttendance.getDailyDate();

                                    DateTimeZone dateTimeZone = DateTimeZone.forID("EET");

                                    DateTime startDT = new DateTime(startDate, dateTimeZone);
                                    DateTime endDT = new DateTime(endDate, dateTimeZone);
                                    if (dailyAttendance.getTimeIn() != null) {
                                        String[] timeParts = new String[3];

                                        timeParts = dailyAttendance.getPlannedIn().split(":");
                                        String tempStart = Integer.toString(startDT.getYear()) + "-" + Integer.toString(startDT.getMonthOfYear())
                                                + "-" + Integer.toString(startDT.getDayOfMonth()) + " " + timeParts[0] + ":" + timeParts[1];
                                        startDate = dateTimeFormat.parse(tempStart);

                                        timeParts = dailyAttendance.getTimeIn().split(":");
                                        String tempEnd = Integer.toString(endDT.getYear()) + "-" + Integer.toString(endDT.getMonthOfYear())
                                                + "-" + Integer.toString(endDT.getDayOfMonth()) + " " + timeParts[0] + ":" + timeParts[1];
                                        endDate = dateTimeFormat.parse(tempEnd);

                                        EmployeeAbsenceRequest absenceRequest = new EmployeeAbsenceRequest();
                                        absenceRequest.setEmployee(employee);
                                        absenceRequest.setAbsence(delayEffectOnAbsence);
                                        absenceRequest.setStartDate(startDate);
                                        absenceRequest.setEndDate(endDate);
                                        absenceRequest.setRequestedDate(dailyAttendance.getDailyDate());
                                        absenceRequest.setBalance(delayHoursPeriod);
                                        absenceRequest.setPeriod(delayHoursPeriod);
                                        absenceRequest.setPosted("N");
                                        absenceRequest.setCancelled("N");
                                        absenceRequest.setPostedDays(new BigDecimal(0));

                                        oFR.append(entitySetupService.callEntityCreateAction(absenceRequest, loggedUser));

                                        totalDelayHrs = totalDelayHrs.subtract(delayHoursPeriod);
                                    }
                                }
                            }
                        }
                    }

                    //early leave
                    if (earlyLeaveEffectOnAbsence != null) {
                        if (consolidation.getEarlyLeave() != null && !consolidation.getEarlyLeave().equals(new BigDecimal(0))
                                && !consolidation.getEarlyLeave().equals("0E-13")) {
                            BigDecimal totalEarlyLeaveHrs = consolidation.getEarlyLeave();
                            for (EmployeeDailyAttendance dailyAttendance : dailyAttendances) {
                                if (totalEarlyLeaveHrs.compareTo(new BigDecimal(0)) <= 0) {
                                    break;
                                }
                                BigDecimal earlyLeaveHoursPeriod = dailyAttendance.getEarlyLeavePeriod();
                                if (earlyLeaveHoursPeriod != null && !earlyLeaveHoursPeriod.equals(new BigDecimal(0))
                                        && !earlyLeaveHoursPeriod.toString().equals("0E-13")) {

                                    Date startDate = dailyAttendance.getDailyDate(), endDate = dailyAttendance.getDailyDate();

                                    DateTimeZone dateTimeZone = DateTimeZone.forID("EET");

                                    DateTime startDT = new DateTime(startDate, dateTimeZone);
                                    DateTime endDT = new DateTime(endDate, dateTimeZone);

                                    if (dailyAttendance.getTimeOut() != null && dailyAttendance.getPlannedOut() != null) {

                                        String[] timeParts = null;
                                        if (!dailyAttendance.getTimeOut().isEmpty()) {

                                            timeParts = dailyAttendance.getTimeOut().split(":");
                                            String tempStart = Integer.toString(startDT.getYear()) + "-" + Integer.toString(startDT.getMonthOfYear())
                                                    + "-" + Integer.toString(startDT.getDayOfMonth()) + " " + timeParts[0] + ":" + timeParts[1];
                                            startDate = dateTimeFormat.parse(tempStart);

                                            if (!dailyAttendance.getPlannedOut().isEmpty()) {
                                                timeParts = dailyAttendance.getPlannedOut().split(":");
                                                String tempEnd = Integer.toString(endDT.getYear()) + "-" + Integer.toString(endDT.getMonthOfYear())
                                                        + "-" + Integer.toString(endDT.getDayOfMonth()) + " " + timeParts[0] + ":" + timeParts[1];
                                                endDate = dateTimeFormat.parse(tempEnd);
                                            }

                                            EmployeeAbsenceRequest absenceRequest = new EmployeeAbsenceRequest();
                                            absenceRequest.setEmployee(employee);
                                            absenceRequest.setAbsence(earlyLeaveEffectOnAbsence);
                                            absenceRequest.setStartDate(startDate);
                                            absenceRequest.setEndDate(endDate);
                                            absenceRequest.setRequestedDate(dailyAttendance.getDailyDate());
                                            absenceRequest.setBalance(earlyLeaveHoursPeriod);
                                            absenceRequest.setPeriod(earlyLeaveHoursPeriod);
                                            absenceRequest.setPosted("N");
                                            absenceRequest.setCancelled("N");
                                            absenceRequest.setPostedDays(new BigDecimal(0));

                                            oFR.append(entitySetupService.callEntityCreateAction(absenceRequest, loggedUser));

                                            totalEarlyLeaveHrs = totalEarlyLeaveHrs.subtract(earlyLeaveHoursPeriod);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (consolidation.getWorkingDays() != null && !consolidation.getWorkingDays().equals(new BigDecimal(0))) {
                        ArrayList conditions = new ArrayList();
                        conditions.add("employee.dbid = " + employee.getDbid());
                        conditions.add("salaryElement.internalCode = 'WD'");

                        List<EmployeeSalaryElement> empSalElement4WorkingDays = (List<EmployeeSalaryElement>) oem.loadEntityList(
                                "EmployeeSalaryElement", conditions, null, null, loggedUser);
                        if (empSalElement4WorkingDays != null && !empSalElement4WorkingDays.isEmpty()) {
                            for (int j = 0; j < empSalElement4WorkingDays.size(); j++) {
                                empSalElement4WorkingDays.get(j).setValue(consolidation.getWorkingDays());
                                oFR.append(entitySetupService.callEntityUpdateAction(empSalElement4WorkingDays.get(j), loggedUser));
                            }
                        }
                    }
                }
                consolidation.setPosted("Y");
                oem.saveEntity(consolidation, loggedUser);

                List<String> params = new ArrayList<String>();
                params.add(consolidation.getEmployee().getCode());
                oFR.addSuccess(userMessageServiceRemote.getUserMessage("TMConsolidation_002", params, loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult applySummar(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
            Connection connection = datasource.getConnection();
            String procedure = "{call proc_summar()}";
            CallableStatement callableStatement = connection.prepareCall(procedure);
            callableStatement.executeUpdate();
            connection.close();
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("applySummarProc001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult applyWinter(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
            Connection connection = datasource.getConnection();
            String procedure = "{call proc_winter()}";
            CallableStatement callableStatement = connection.prepareCall(procedure);
            callableStatement.executeUpdate();
            connection.close();
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("applyWinterProc001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult applySummarhrrmv(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
            Connection connection = datasource.getConnection();
            String procedure = "{call proc_summar_hrrmv()}";
            CallableStatement callableStatement = connection.prepareCall(procedure);
            callableStatement.executeUpdate();
            connection.close();
            oFR.addSuccess(userMessageServiceRemote.getUserMessage("applySummarHrrmvProc001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult applyOTRequestForSingleEmployee(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeOTImport request = (EmployeeOTImport) odm.getData().get(0);
            if (request != null) {
                Date requestedDate = request.getRequestedDate();
                Calendar calendar;
                calendar = Calendar.getInstance();
                calendar.setTime(requestedDate);

                int year;
                year = calendar.get(Calendar.YEAR);
                int month;
                month = calendar.get(Calendar.MONTH) + 1;
                int day;
                day = calendar.get(Calendar.DAY_OF_MONTH);

                String paymentMethodDbid = getDefaultPaymentMethod(loggedUser);
                String paymentPeriodDbid = getDefaultPaymentPeriod(loggedUser);

                Long calculatedPeriodDbid;
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(day, month, year, paymentMethodDbid, paymentPeriodDbid, loggedUser);

                Long employeeDbid;
                employeeDbid = request.getEmployee().getDbid();

                String sqlWhere;
                sqlWhere = "employee_dbid = " + employeeDbid;
                ofr.append(updateCalculatedPeriodForOTImport(sqlWhere, paymentMethodDbid, paymentPeriodDbid, loggedUser));

                if (ofr.getErrors().isEmpty()) {
                    ofr.append(updateNormalOTImportForEmployee(calculatedPeriodDbid.toString(), employeeDbid.toString(), loggedUser));
                }
            }
            ofr.addSuccess(userMessageServiceRemote.getUserMessage("OTRequestImport001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult applyOTRequestForAll(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeOTImport request = (EmployeeOTImport) odm.getData().get(0);
            if (request != null) {
                Date requestedDate = request.getRequestedDate();
                Calendar calendar;
                calendar = Calendar.getInstance();
                calendar.setTime(requestedDate);

                int year;
                year = calendar.get(Calendar.YEAR);
                int month;
                month = calendar.get(Calendar.MONTH) + 1;
                int day;
                day = calendar.get(Calendar.DAY_OF_MONTH);

                String paymentMethodDbid = getDefaultPaymentMethod(loggedUser);
                String paymentPeriodDbid = getDefaultPaymentPeriod(loggedUser);

                Long calculatedPeriodDbid;
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(day, month, year, paymentMethodDbid, paymentPeriodDbid, loggedUser);

                Long employeeDbid;
                employeeDbid = request.getEmployee().getDbid();

                ofr.append(updateCalculatedPeriodForOTImport("", paymentMethodDbid, paymentPeriodDbid, loggedUser));

                if (ofr.getErrors().isEmpty()) {

                    ofr.append(updateNormalOTImportForAll(calculatedPeriodDbid.toString(), loggedUser));
                }
            }
            ofr.addSuccess(userMessageServiceRemote.getUserMessage("OTRequestImport001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult applyAddingOTRequest(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeOTImport request = (EmployeeOTImport) odm.getData().get(0);
            if (request != null) {
                Date requestedDate = request.getRequestedDate();
                Calendar calendar;
                calendar = Calendar.getInstance();
                calendar.setTime(requestedDate);

                int year;
                year = calendar.get(Calendar.YEAR);
                int month;
                month = calendar.get(Calendar.MONTH) + 1;
                int day;
                day = calendar.get(Calendar.DAY_OF_MONTH);

                String paymentMethodDbid = getDefaultPaymentMethod(loggedUser);
                String paymentPeriodDbid = getDefaultPaymentPeriod(loggedUser);
                Long calculatedPeriodDbid;
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(day, month, year, paymentMethodDbid, paymentPeriodDbid, loggedUser);

                if (!calculatedPeriodDbid.equals("0")) {
                    String employeeDbid;
                    employeeDbid = String.valueOf(request.getEmployee().getDbid());
                    Long monthlyConsolidationDbid;
                    monthlyConsolidationDbid = addingMonthlyConsolidationIfNotExist(employeeDbid, calculatedPeriodDbid.toString(), loggedUser);
                    if (monthlyConsolidationDbid == -1) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("EOTI-E0001", loggedUser));
                        return oFR;
                    }

                    if (!monthlyConsolidationDbid.equals(0)) {
                        String sql;
                        sql = "Update EmployeeMonthlyConsolidation "
                                + " Set normalOT = "
                                + " Case When (normalOT + " + request.getOtValue().toString()
                                + " > 53) then 53 Else "
                                + " (normalOT + " + request.getOtValue().toString()
                                + " ) End"
                                + " Where dbid = " + monthlyConsolidationDbid;
                        try {
                            oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                        } catch (Exception ex) {
                            OLog.logException(ex, loggedUser);
                        }
                    }
                    oFR.addSuccess(userMessageServiceRemote.getUserMessage("EOTI-S0001", loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult applyUpdatingOTRequest(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {
            EmployeeOTImport request = (EmployeeOTImport) odm.getData().get(0);
            if (request != null) {
                Date requestedDate = request.getRequestedDate();
                Calendar calendar;
                calendar = Calendar.getInstance();
                calendar.setTime(requestedDate);

                int year;
                year = calendar.get(Calendar.YEAR);
                int month;
                month = calendar.get(Calendar.MONTH) + 1;
                int day;
                day = calendar.get(Calendar.DAY_OF_MONTH);

                String paymentMethodDbid = getDefaultPaymentMethod(loggedUser);
                String paymentPeriodDbid = getDefaultPaymentPeriod(loggedUser);

                Long calculatedPeriodDbid;
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(day, month, year, paymentMethodDbid, paymentPeriodDbid, loggedUser);
                if (!calculatedPeriodDbid.equals(0)) {
                    //__________Get Old OT Value______________________
                    EmployeeOTImport employeeOTImport;
                    employeeOTImport = (EmployeeOTImport) oem.loadEntity("EmployeeOTImport", request.getDbid(), null, null, loggedUser);

                    BigDecimal otValue;
                    BigDecimal oldOTValue = BigDecimal.ZERO;
                    Long oldEmployeeDbid = null;
                    Long newEmployeeDbid;

                    if (employeeOTImport != null) {
                        oldOTValue = employeeOTImport.getOtValue();
                        oldEmployeeDbid = employeeOTImport.getEmployee().getDbid();
                    }
                    newEmployeeDbid = request.getEmployee().getDbid();
                    otValue = request.getOtValue();
                    String sql;

                    Long monthlyConsolidationDbid;
                    monthlyConsolidationDbid = addingMonthlyConsolidationIfNotExist(newEmployeeDbid.toString(), calculatedPeriodDbid.toString(), loggedUser);
                    if (monthlyConsolidationDbid == -1) {
                        oFR.addError(userMessageServiceRemote.getUserMessage("EOTI-E0001", loggedUser));
                        return oFR;
                    }
                    if (newEmployeeDbid.equals(oldEmployeeDbid)
                            && !monthlyConsolidationDbid.equals(0)) {
                        otValue = otValue.subtract(oldOTValue);
                        if (otValue.doubleValue() != 0.0) {
                            sql = "Update EmployeeMonthlyConsolidation "
                                    + " Set normalOT = "
                                    + " Case When (normalOT + " + otValue.toString()
                                    + " > 53) then 53 Else "
                                    + " (normalOT + " + otValue.toString()
                                    + " ) End"
                                    + " Where dbid = " + monthlyConsolidationDbid;
                            try {
                                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                            } catch (Exception ex) {
                                OLog.logException(ex, loggedUser);
                            }
                        }

                    } else {
                        if (otValue.doubleValue() != 0.0) {
                            sql = "Update EmployeeMonthlyConsolidation "
                                    + " Set normalOT = "
                                    + " Case When (normalOT + " + otValue.toString()
                                    + " > 53) then 53 Else "
                                    + " (normalOT + " + otValue.toString()
                                    + " ) End"
                                    + " Where dbid = " + monthlyConsolidationDbid;
                            try {
                                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                            } catch (Exception ex) {
                                OLog.logException(ex, loggedUser);
                            }
                        }
                        //________________________________
                        if (oldOTValue.doubleValue() != 0.0) {
                            monthlyConsolidationDbid = addingMonthlyConsolidationIfNotExist(oldEmployeeDbid.toString(), calculatedPeriodDbid.toString(), loggedUser);
                            if (monthlyConsolidationDbid == -1) {
                                oFR.addError(userMessageServiceRemote.getUserMessage("EOTI-S0001", loggedUser));
                                return oFR;
                            }
                            if (monthlyConsolidationDbid != 0) {
                                sql = "Update EmployeeMonthlyConsolidation "
                                        + " Set normalOT = normalOT - " + oldOTValue.toString()
                                        + " Where dbid = " + monthlyConsolidationDbid.toString();
                                try {
                                    oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                                } catch (Exception ex) {
                                    OLog.logException(ex, loggedUser);
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    @Override
    public OFunctionResult applyDeletingOTRequest(ODataMessage odm, OFunctionParms parms, OUser loggedUser) {
        OFunctionResult oFR = new OFunctionResult();
        try {

            EmployeeOTImport request = (EmployeeOTImport) odm.getData().get(0);
            if (request != null) {
                Date requestedDate = request.getRequestedDate();
                Calendar calendar;
                calendar = Calendar.getInstance();
                calendar.setTime(requestedDate);

                int year;
                year = calendar.get(Calendar.YEAR);
                int month;
                month = calendar.get(Calendar.MONTH) + 1;
                int day;
                day = calendar.get(Calendar.DAY_OF_MONTH);

                String paymentMethodDbid = getDefaultPaymentMethod(loggedUser);
                String paymentPeriodDbid = getDefaultPaymentPeriod(loggedUser);

                Long calculatedPeriodDbid;
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(day, month, year, paymentMethodDbid, paymentPeriodDbid, loggedUser);

                if (!calculatedPeriodDbid.equals(0)) {
                    String sql;
                    sql = "Update EmployeeMonthlyConsolidation "
                            + " Set normalOT = normalOT - " + request.getOtValue().toString()
                            + " Where employee_dbid = " + request.getEmployee().getDbid()
                            + " AND calculatedPeriod_dbid = " + calculatedPeriodDbid.toString();
                    try {
                        oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                    } catch (Exception ex) {
                        OLog.logException(ex, loggedUser);
                    }
                }

            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            oFR.addError(userMessageServiceRemote.getUserMessage("SystemInternalError", loggedUser));
        }
        return oFR;
    }

    //TO DO : Should Make commom TM functions in one service
    @Override
    public long getCalculatedPeriodOfThisDay(int day, int month, int year, String paymentMethodDbid, String paymentPeriodDbid, OUser loggedUser) {
        String payrollParameterDsc;
        String[] payrollParameterResult;

        // loading the cut off from payrollparameter
        payrollParameterDsc = "day_of_calculation";
        String cuttOffDay1 = "0";
        payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
        if (payrollParameterResult != null) {
            cuttOffDay1 = payrollParameterResult[0];
        }
        //calcCurrentMonth
        String calcCurrentMonth = "";;
        payrollParameterDsc = "calculation_currentMonth";
        payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
        if (payrollParameterResult != null) {
            calcCurrentMonth = payrollParameterResult[0];
        }

        //calcFullMonth
        String calcFullMonth = "N";
        payrollParameterDsc = "calculation_fullMonth";
        payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
        if (payrollParameterResult != null) {
            calcFullMonth = payrollParameterResult[0];
        }
        int cutOffStart;
        if (calcFullMonth != null && calcFullMonth.equals("Y")) { // from 1->30
            cutOffStart = 1;

            if (calcCurrentMonth != null && calcCurrentMonth.equals("N")) { // the prev month
                if (month == 12) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }
            }
        } else if ((calcFullMonth != null && calcFullMonth.equals("N"))
                || (calcFullMonth == null)) {
            cutOffStart = Integer.parseInt(cuttOffDay1) + 1;
            if (day >= cutOffStart) {
                month = (month + 1) % 12;
                if (month == 0) {
                    month = 12;
                }
                if (month == 1) {
                    year++;
                }
            }
        }

        String selectCalcPeriod = "select dbid from calculatedPeriod where year = " + year + " and month = " + month + " and paymentMethod_Dbid = "
                + paymentMethodDbid + " and payPeriod_dbid = " + paymentPeriodDbid;
        long periodDbid = 0;
        try {
            Object result = oem.executeEntityNativeQuery(selectCalcPeriod, loggedUser);
            if (result != null) {
                periodDbid = Long.parseLong(result.toString());
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return periodDbid;
    }

    private String[] getFromPayrollParameter(String description, OUser loggedUser) {
        String[] result = null;
        try {
            String SQL;
            SQL = "Select valueData,auxiliary From PayrollParameter where company_dbid = "
                    + loggedUser.getUserPrefrence1() + " AND description = '" + description + "'";
            Object[] obj = (Object[]) oem.executeEntityNativeQuery(SQL, loggedUser);

            if (obj != null) {
                result = new String[2];
                result[0] = obj[0].toString();
                result[1] = obj[1].toString();
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return result;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private OFunctionResult updateCalculatedPeriodForOTImport(String sqlWhere, String paymentMethodDbid, String paymentPeriodDbid, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String sql;
            sql = "select dbid,requestedDate "
                    + " from EmployeeOTImport "
                    + " Where (calculatedPeriod_dbid is null OR calculatedPeriod_dbid = 0)";

            if (sqlWhere != null && !sqlWhere.equals("")) {
                sql += " AND " + sqlWhere;
            }
            List<Object[]> objs;
            Long recordCPDbid;
            Long recordDbid;
            int day, month, year;
            Object obj = null;
            Calendar calendar;
            calendar = Calendar.getInstance();
            try {
                objs = oem.executeEntityListNativeQuery(sql, loggedUser);
            } catch (Exception ex) {
                OLog.logException(ex, loggedUser);
                ofr.addError(userMessageServiceRemote.getUserMessage("OTRequestImportE003", loggedUser));
                return ofr;
            }
            for (Object[] object : objs) {
                calendar.setTime((Date) object[1]);

                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH) + 1;
                day = calendar.get(Calendar.DAY_OF_MONTH);

                recordDbid = Long.valueOf(object[0].toString());
                recordCPDbid = getCalculatedPeriodOfThisDay(day, month, year, paymentMethodDbid, paymentPeriodDbid, loggedUser);
                sql = "Update EmployeeOTImport "
                        + " Set calculatedPeriod_dbid = " + recordCPDbid.toString()
                        + " WHERE dbid = " + recordDbid.toString();
                try {
                    oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                } catch (Exception ex) {
                    OLog.logException(ex, loggedUser);
                    ofr.addError(userMessageServiceRemote.getUserMessage("OTRequestImportE004", loggedUser));
                    return ofr;
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private OFunctionResult updateNormalOTImportForEmployee(String calculatedPeriodDbid, String employeeDbid, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String sql;
            Object obj = null;

            if (!calculatedPeriodDbid.equals("0")) {
                sql = "Select sum(otValue) "
                        + " From EmployeeOTImport "
                        + " Where employee_dbid =" + employeeDbid
                        + " AND calculatedPeriod_dbid = " + calculatedPeriodDbid;

                try {
                    obj = oem.executeEntityNativeQuery(sql, loggedUser);
                } catch (Exception ex) {
                    OLog.logException(ex, loggedUser);
                    ofr.addError(userMessageServiceRemote.getUserMessage("OTRequestImportE001", loggedUser));
                    return ofr;
                }

                String otSummation = "0";
                if (obj != null) {
                    otSummation = obj.toString();
                }
                if (!otSummation.equals("0")) {
                    Long monthlyConsolidationDbid = addingMonthlyConsolidationIfNotExist(employeeDbid, calculatedPeriodDbid, loggedUser);
                    if (monthlyConsolidationDbid == -1) {
                        ofr.addError(userMessageServiceRemote.getUserMessage("OTRequestImportE002", loggedUser));
                    }
                    if (monthlyConsolidationDbid != 0) {
                        //_________________________________________
                        if (Double.parseDouble(otSummation) > 53.0) {
                            otSummation = "53";
                        }
                        //_________________________________________
                        sql = "Update EmployeeMonthlyConsolidation "
                                + " Set normalOT = " + otSummation
                                + " Where dbid = " + monthlyConsolidationDbid;
                        try {
                            oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                        } catch (Exception ex) {
                            OLog.logException(ex, loggedUser);
                            ofr.addError(userMessageServiceRemote.getUserMessage("OTRequestImportE003", loggedUser));
                            return ofr;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(userMessageServiceRemote.getUserMessage("OTRequestImportE004", loggedUser));
        }
        return ofr;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private OFunctionResult updateNormalOTImportForAll(String calculatedPeriodDbid, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String sql;
            Object obj = null;
            if (!calculatedPeriodDbid.equals(0)) {

                sql = "Select Distinct employee_dbid "
                        + " From EmployeeOTImport "
                        + " Where calculatedPeriod_dbid = " + calculatedPeriodDbid;
                List<Object> objects;
                objects = oem.executeEntityListNativeQuery(sql, loggedUser);
                for (Object object : objects) {
                    ofr.append(updateNormalOTImportForEmployee(calculatedPeriodDbid, object.toString(), loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    @Override
    public String getDefaultPaymentMethod(OUser loggedUser) {
        String paymentMethodDbid = "";
        try {
            String[] payrollParameterResult;
            String payrollParameterDsc;
            payrollParameterDsc = "default_pay_method";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                paymentMethodDbid = payrollParameterResult[0];
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return paymentMethodDbid;
    }

    @Override
    public String getDefaultPaymentPeriod(OUser loggedUser) {
        String paymentPeriodDbid = "";
        try {
            String[] payrollParameterResult;
            String payrollParameterDsc;
            payrollParameterDsc = "default_pay_period";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                paymentPeriodDbid = payrollParameterResult[0];
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return paymentPeriodDbid;
    }

    @Override
    public Long checkConsolidationExisting(String employeeDbid, String calculatedPeriodDbid, OUser loggedUser) {
        Long monthlyConsolidationDbid;
        monthlyConsolidationDbid = Long.valueOf(0);
        try {
            String sql;
            sql = "Select dbid "
                    + " From EmployeeMonthlyConsolidation"
                    + " Where employee_dbid = " + employeeDbid
                    + " AND calculatedPeriod_dbid = " + calculatedPeriodDbid;

            Object obj;
            obj = oem.executeEntityNativeQuery(sql, loggedUser);
            if (obj != null && Long.valueOf(obj.toString()) > 0) {
                monthlyConsolidationDbid = Long.valueOf(obj.toString());
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            monthlyConsolidationDbid = Long.valueOf(-1);
        }
        return monthlyConsolidationDbid;
    }

    @Override
    public Long addingMonthlyConsolidation(String employeeDbid, String calculatedPeriodDbid, OUser loggedUser) {
        Long monthlyConsolidationDbid = Long.valueOf(0);
        try {

            Employee employee;
            employee = (Employee) oem.loadEntity("Employee", Long.valueOf(employeeDbid), null, null, loggedUser);

            CalculatedPeriod period;
            period = (CalculatedPeriod) oem.loadEntity("CalculatedPeriod", Long.valueOf(calculatedPeriodDbid), null, null, loggedUser);
            if (employee != null & period != null) {
                EmployeeMonthlyConsolidation consolidation;
                consolidation = new EmployeeMonthlyConsolidation();
                consolidation.setEmployee(employee);
                consolidation.setCalculatedPeriod(period);
                consolidation.setPosted("N");
                consolidation.setNormalOT(BigDecimal.ZERO);
                oem.saveEntity(consolidation, loggedUser);
                monthlyConsolidationDbid = consolidation.getDbid();
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            monthlyConsolidationDbid = (long) -1;
        }
        return monthlyConsolidationDbid;
    }

    @Override
    public Long addingMonthlyConsolidationIfNotExist(String employeeDbid, String calculatedPeriodDbid, OUser loggedUser) {
        Long monthlyConsolidationDbid;
        try {
            monthlyConsolidationDbid = checkConsolidationExisting(employeeDbid, calculatedPeriodDbid, loggedUser);
            if (monthlyConsolidationDbid == 0) {
                monthlyConsolidationDbid = addingMonthlyConsolidation(employeeDbid, calculatedPeriodDbid, loggedUser);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            monthlyConsolidationDbid = (long) -1;
        }
        return monthlyConsolidationDbid;
    }

    @Override
    public String[] getCutOffDatesFromCalculatedPeriod(Integer currentMonth, Integer currentYear, OUser loggedUser) {
        String[] cutOffDates = new String[2];
        try {
            String payrollParameterDsc;
            String[] payrollParameterResult;
            Integer cutOffEnd = 0, cutOffStart = 0;
            // loading the cut off from payrollparameter
            payrollParameterDsc = "day_of_calculation";
            String cuttOffDay1 = "0", cuttOffDay2 = "0";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                cuttOffDay1 = payrollParameterResult[0];
                cuttOffDay2 = payrollParameterResult[1];
            }
            Integer previousMonth, previousYear;

            previousMonth = currentMonth - 1;
            previousYear = currentYear;
            if (previousMonth == 0) {
                previousMonth = 12;
                previousYear = previousYear - 1;
            }

            String startDateStr, endDateStr;
            if (cuttOffDay1.equals("0")) {
                startDateStr = previousYear + "-" + previousMonth + "-01";
                endDateStr = previousYear + "-" + currentMonth + "-" + dateTimeUtility.getLastDayInMonth(previousYear, previousMonth);
            } else {
                startDateStr = previousYear + "-" + previousMonth + "-" + String.valueOf((Integer.parseInt(cuttOffDay1) + 1));
                endDateStr = currentYear + "-" + currentMonth + "-" + String.valueOf((Integer.parseInt(cuttOffDay2) + 1));
            }

            cutOffDates[0] = startDateStr;
            cutOffDates[1] = endDateStr;

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            cutOffDates = null;
        }
        return cutOffDates;
    }

    @Override
    public String[] getCutOffDatesFromCalculatedPeriod(Integer currentMonth, Integer currentYear, Integer currentDay, OUser loggedUser) {
        String[] cutOffDates = new String[2];
        try {
            String payrollParameterDsc;
            String[] payrollParameterResult;
            Integer cutOffEnd = 0, cutOffStart = 0;
            // loading the cut off from payrollparameter
            payrollParameterDsc = "day_of_calculation";
            String cuttOffDay1 = "0", cuttOffDay2 = "0";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                cuttOffDay1 = payrollParameterResult[0];
                cuttOffDay2 = payrollParameterResult[1];
            }

            Integer previousMonth, previousYear;

            previousMonth = currentMonth - 1;
            previousYear = currentYear;
            if (previousMonth == 0) {
                previousMonth = 12;
                previousYear = previousYear - 1;
            }
            if (currentDay > Integer.parseInt(cuttOffDay2) + 1) {
                previousMonth += 1;
                currentMonth += 1;
            }
            String startDateStr, endDateStr;
            if (cuttOffDay1.equals("0")) {
                startDateStr = previousYear + "-" + previousMonth + "-01";
                endDateStr = previousYear + "-" + currentMonth + "-" + dateTimeUtility.getLastDayInMonth(previousYear, previousMonth);
            } else {
                startDateStr = previousYear + "-" + previousMonth + "-" + String.valueOf((Integer.parseInt(cuttOffDay1) + 1));
                endDateStr = currentYear + "-" + currentMonth + "-" + String.valueOf((Integer.parseInt(cuttOffDay2) + 1));
            }

            cutOffDates[0] = startDateStr;
            cutOffDates[1] = endDateStr;

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            cutOffDates = null;
        }
        return cutOffDates;
    }
}
