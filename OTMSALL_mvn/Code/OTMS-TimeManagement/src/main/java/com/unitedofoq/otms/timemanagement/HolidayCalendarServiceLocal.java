package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.personnel.holiday.HolidayAssign;
import javax.ejb.Local;

@Local
public interface HolidayCalendarServiceLocal {

    public void assignHolidayToEmployee(HolidayAssign ha, OUser loggedUser);

    public OFunctionResult deleteHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult editHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult addHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult assignHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult deleteEmployeeHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult deleteAllHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateReligionApplied(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateHolidayDates(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateDuplicateDates(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult validateInactiveHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
}
