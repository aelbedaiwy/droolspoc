/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;

/**
 *
 * @author lap2
 */
public class EmpDailyAttendanceBatch {

    public static final DateFormat YEAR_FORMAT = new SimpleDateFormat("yyyy");
    OUser loggedUser = null;
    private Long dbid;
    int empRecordsNum = 370;
    int holidayDays = 0;
    InitialContext context = null;
    DataSource dataSource = null;
    
    public EmpDailyAttendanceBatch(OUser loggedUser, Long dbid) {
        this.loggedUser = loggedUser;
        this.dbid = dbid;
        try {
            this.context = new InitialContext();
        } catch(NamingException ex){
            OLog.logException(ex, loggedUser);
            OLog.logError("Exception creating Initial Context in EmpDailyAttendanceBatch", loggedUser);
            return ;
        }
        try {
            this.dataSource = (DataSource)context.lookup("jdbc/" + 
                    loggedUser.getTenant().getPersistenceUnitName());
        } catch(NamingException ex){
            OLog.logException(ex, loggedUser);
            OLog.logError("Exception Looking up DataSource in EmpDailyAttendanceBatch", loggedUser);
        }
    }

    public EmpDailyAttendanceBatch(OUser loggedUser) {
        this.loggedUser = loggedUser;
        try {
            this.context = new InitialContext();
        } catch(NamingException ex){
            OLog.logException(ex, loggedUser);
            OLog.logError("Exception creating Initial Context in EmpDailyAttendanceBatch", loggedUser);
            return ;
        }
        try {
            this.dataSource = (DataSource)context.lookup("jdbc/" + 
                    loggedUser.getTenant().getPersistenceUnitName());
        } catch(NamingException ex){
            OLog.logException(ex, loggedUser);
            OLog.logError("Exception Looking up DataSource in EmpDailyAttendanceBatch", loggedUser);
        }
    }

//    public void setTMData(List<Long> employees) {
//        Connection connection = null;
//        try {
//            connection = dataSource.getConnection();
//        // set autocommit by false to avoid inserting employees without postactions.
//            connection.setAutoCommit(false);
//
//            insertEmpPlanner(employees, connection);
//            insertEmpDA(employees, connection);
//
//            // commit all  transactions after postactions.
////           if (savedSuccessfully =  true)
////           {
//            connection.commit();
//            //}
////           else
////           {
//            // OLog.logInformation("Error encountered, data not saved", loggedUser);
//            //}
//            // date one field ***
//        } catch (Exception ex) {
//            Logger.getLogger(EmpDailyAttendanceBatch.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                connection.close();
//            } catch (SQLException sex){
//                OLog.logException(sex, loggedUser);
//            }
//        }
//        
//    }
    
    public void setTMDAData(List<Integer> employees, String plannerYear) {
        List<Long> emp = new ArrayList<Long>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            generateDbid(employees.size(), connection);
            for (int i=0;i<employees.size();i++)
            {
                emp.add( Long.parseLong(employees.get(i).toString()));
            }
            insertEmpDA(emp, plannerYear, connection);
            connection.commit();
        } catch (Exception ex) {
            Logger.getLogger(EmpDailyAttendanceBatch.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException sex){
                OLog.logException(sex, loggedUser);
            }
        }
    }

    public void insertEmpPlanner(List<Long> employee, Connection connection) {
        PreparedStatement insertPreStatement = null;
        StringBuilder insertStatement = new StringBuilder("INSERT INTO employeeannualplanner (dbid, deleted, employee_dbid, "
                + "planneryear, ");
        String plannerYear = "2013";

        java.util.Date currentDate = Calendar.getInstance().getTime();
        plannerYear = YEAR_FORMAT.format(currentDate);

        Date startDate = java.sql.Date.valueOf(plannerYear + "-01-01");
        try {
            for (Integer i = 1; i <= 12; i++) {
                if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) {
                    for (Integer j = 1; j <= 31; j++) {
                        insertStatement.append("m").append(i.toString()).append("d").append(j.toString()).append(",");
                    }
                } else if (i == 4 || i == 6 || i == 8 || i == 11 || i == 9) {
                    for (Integer j = 1; j <= 30; j++) {
                        insertStatement.append("m").append(i.toString()).append("d").append(j.toString()).append(",");
                    }
                } else {
                    for (Integer j = 1; j <= 28; j++) {
                        insertStatement.append("m").append(i.toString()).append("d").append(j.toString()).append(",");
                    }
                }
                if (i == 12) {
                    insertStatement.replace(insertStatement.length() - 1, insertStatement.length(), ") values (");
                }
            }
            for (int i = 1; i <= 369; i++) {
                insertStatement.append("?,");
            }
            insertStatement.replace(insertStatement.length() - 1, insertStatement.length(), ")");

            //absence
            insertPreStatement = connection.prepareStatement(insertStatement.toString());
            for (int i = 0; i < employee.size(); i++) {

                insertPreStatement.setLong(1, dbid);
                insertPreStatement.setInt(2, 0);
                insertPreStatement.setLong(3, employee.get(i));
                insertPreStatement.setString(4, plannerYear);
                int counter = 5;

                Statement selectStatement = connection.createStatement();
                ResultSet hiringDateRS = selectStatement.executeQuery("select hiringdate from oemployee "
                        + "where dbid = " + employee.get(i));
                int daysCounter = 0;
                if (hiringDateRS.next()) {
                    Date hiringDate = hiringDateRS.getDate("hiringdate");
                    if (hiringDate.after(startDate)) {
                        Date tempDate = startDate;
                        DateTimeZone zone = DateTimeZone.forID("EET");
                        DateTime tempDT = new DateTime(tempDate, zone);
                        DateTime hiringDT = new DateTime(hiringDate, zone);
                        while (tempDT.isBefore(hiringDT)) {
                            insertPreStatement.setString(counter, null);
                            counter++;
                            tempDT = tempDT.plusDays(1);
                            daysCounter++;
                        }
                    }
                }
                for (int j = daysCounter; j < 365; j++) {
                    insertPreStatement.setString(counter, "A");
                    counter++;
                }
                insertPreStatement.addBatch();
                dbid++;
            }
            insertPreStatement.executeBatch();

            //holidays
            holidayDays = 0;
            List<String> holidayDates = new ArrayList<String>();
            StringBuilder updateStatement = new StringBuilder("update employeeannualplanner set ");
            
            Statement statement = connection.createStatement();
            ResultSet holidayResult = statement.executeQuery("select * from holidaycalendar");
            while (holidayResult.next()) {
                java.util.Date from = holidayResult.getDate("dateFrom");
                java.util.Date to = holidayResult.getDate("dateTo");

                List<String> tempHolidays = getHolidayDates(from, to);
                holidayDates.addAll(tempHolidays);
                updateStatement.append(generateUpdateStat(tempHolidays));
            }
            ResultSet chrisHolidayResult = statement.executeQuery("select * from ChristianCalendar");
            while (chrisHolidayResult.next()) {
                java.util.Date from = chrisHolidayResult.getDate("dateFrom");
                java.util.Date to = chrisHolidayResult.getDate("dateTo");

                List<String> tempHolidays = getHolidayDates(from, to);
                holidayDates.addAll(tempHolidays);
                updateStatement.append(generateUpdateStat(tempHolidays));
            }
            updateStatement.replace(updateStatement.length() - 1, updateStatement.length(),
                    " where employee_dbid = ?");

            PreparedStatement updatePlanner = connection.prepareStatement(updateStatement.toString());
            for (int i = 0; i < employee.size(); i++) {
                Statement selectStatement = connection.createStatement();
                ResultSet hiringDateRS = selectStatement.executeQuery("select hiringdate from oemployee "
                        + "where dbid = " + employee.get(i));

                if (hiringDateRS.next()) {
                    Date hiringDate = hiringDateRS.getDate("hiringdate");
                    for (int j = 0; j < holidayDates.size(); j++) {
                        String[] temp = holidayDates.get(j).split("d");
                        String[] temp2 = temp[0].split("m");
                        String month = temp2[1];
                        String day = temp[1];
                        String monthStr, dayStr;
                        if ((Integer.parseInt(month)) / 10 == 0) {
                            monthStr = "0" + month.toString();
                        } else {
                            monthStr = month.toString();
                        }
                        if ((Integer.parseInt(day)) / 10 == 0) {
                            dayStr = "0" + day.toString();
                        } else {
                            dayStr = day.toString();
                        }
                        Date tempDate = Date.valueOf(plannerYear + "-" + monthStr + "-" + dayStr);
                        if (hiringDate.before(tempDate)
                                || hiringDate.equals(tempDate)) {
                            updatePlanner.setString(j+1, "H");
                        } else {
                            updatePlanner.setString(j+1, null);
                        }
                    }
                    updatePlanner.setLong(holidayDays + 1, employee.get(i));
                    updatePlanner.addBatch();
                    dbid++;
                }
            }
            updatePlanner.executeBatch();
            
            // vacation
            holidayDays = 0;
            updateStatement = new StringBuilder("update employeeannualplanner set ");
            for (int i = 0; i < employee.size(); i++) {                
                ResultSet vacationResult = statement.executeQuery("select * from employeedayoffrequest where employee_dbid = " + employee.get(i)
                        + " and year(startdate) = " + plannerYear + " and `DAYOFFTYPE` = 'VACATION'" );
                
                boolean hasVacation = false;
                List<String> vacations = new ArrayList<String>();
                    while (vacationResult.next()) { 
                        java.util.Date from = vacationResult.getDate("startDate");
                        java.util.Date to = vacationResult.getDate("endDate");

                        List<String> tempVac = getHolidayDates(from, to);
                        vacations.addAll(tempVac);
                        updateStatement.append(generateUpdateStat(tempVac));
                        hasVacation = true;
                    }
                    if(hasVacation){
                    updateStatement.replace(updateStatement.length() - 1, updateStatement.length(),
                            " where employee_dbid = ?");

                    updatePlanner  = connection.prepareStatement(updateStatement.toString());
                    for (int j = 1; j <= holidayDays; j++) {
                        updatePlanner.setString(j, "V");
                    }
                    updatePlanner.setLong(holidayDays+1, employee.get(i));
                    updatePlanner.addBatch();
                    dbid++;
                }
            }
            updatePlanner.executeBatch();

        } catch (Exception exception) {
            Logger.getLogger(EmpDailyAttendanceBatch.class.getName()).log(Level.SEVERE, null, exception);
        }
    }

    public PreparedStatement insertEmpDA(List<Long> employee, String plannerYear, Connection connection) {
        PreparedStatement insertPreStatement = null;
        String insertStatement = "INSERT INTO employeedailyattendance (dbid, deleted, employee_dbid, daynature_dbid, dailydate, header)"
                + "values (?,?,?,?,?,?)";
        try {
            insertPreStatement = connection.prepareStatement(insertStatement);
            for (int i = 0; i < employee.size(); i++) {
                Statement selectEmp = connection.createStatement();
                ResultSet empRes = selectEmp.executeQuery("select * from employeeannualplanner where employee_dbid = "
                        + employee.get(i) + " and plannerYear = '" + plannerYear + "'");

                Integer day = 1, month = 1;
                String monthStr = "", dayStr = "";
                if (empRes.next()) {
                    while (true) {
                        long dayNature_dbid = 0;
                        Date dailyDate;
                        String year = empRes.getString("planneryear");

                        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                            if (day == 32) {
                                if (month == 12) {
                                    break;
                                }
                                day = 1;
                                month++;
                            }
                        } else if (month == 2) {
                            if (day == 29) {
                                day = 1;
                                month++;
                            }
                        } else {
                            if (day == 31) {
                                day = 1;
                                month++;
                            }
                        }

                        if (month / 10 == 0) {
                            monthStr = "0" + month.toString();
                        } else {
                            monthStr = month.toString();
                        }
                        if (day / 10 == 0) {
                            dayStr = "0" + day.toString();
                        } else {
                            dayStr = day.toString();
                        }

                        //System.out.println(year + "-" + monthStr + "-" + dayStr);
                        dailyDate = java.sql.Date.valueOf(year + "-" + monthStr + "-" + dayStr);

                        String fieldName = "m" + month.toString() + "d" + day.toString();
                        Statement selectDayNature = connection.createStatement();
                        ResultSet dayNatureRes = selectDayNature.executeQuery("select dbid from daynatures where type = '"
                                + empRes.getString(fieldName) + "'");
                        if (dayNatureRes.next()) {
                            dayNature_dbid = dayNatureRes.getLong("dbid");
                        }
                        
                        if(dayNature_dbid!=0){
                            insertPreStatement.setLong(1, dbid);
                            insertPreStatement.setInt(2, 0);
                            insertPreStatement.setLong(3, employee.get(i));
                            insertPreStatement.setLong(4, dayNature_dbid);
                            insertPreStatement.setDate(5, dailyDate);
                            insertPreStatement.setBoolean(6, true); // as this record is the header

                            insertPreStatement.addBatch();
                            dbid++;
                        }
                        day++;
                    }
                }
            }
            insertPreStatement.executeBatch();
        } catch (Exception exception) {
            Logger.getLogger(EmpDailyAttendanceBatch.class.getName()).log(Level.SEVERE, null, exception);
        }
        return insertPreStatement;
    }
    
    
    private void generateDbid(int size, Connection connection) {
        try {
            Statement selectEmp = connection.createStatement();
            ResultSet empRes = selectEmp.executeQuery("select seq_count from sequence where seq_name = 'SEQ_GEN'");
            if (empRes.next()) {
                dbid = empRes.getLong("seq_count");
                dbid += 50;                    
            }
            long tempDbid;
            tempDbid = dbid + (size*366);

            Statement updateSeq = connection.createStatement();
            updateSeq.executeUpdate("update sequence set seq_count = " + tempDbid + " where seq_name = 'SEQ_GEN'");
            connection.commit();
        } catch (Exception ex) {
            Logger.getLogger(EmpDailyAttendanceBatch.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            try {
//                connection.close();
//            } catch (SQLException sex){
//                OLog.logException(sex, loggedUser);
//            }
        }
        
    }
    
    private String generateUpdateStat(List<String> holidayDates) {
        String retHolidayCalendars = "";
        for (int i = 0; i < holidayDates.size(); i++) {
            retHolidayCalendars += (holidayDates.get(i)+" =?,");
        }
        return retHolidayCalendars;
    }

    private List<String> getHolidayDates(java.util.Date from, java.util.Date to) {
        DateTimeZone dateTimeZone = DateTimeZone.forID("EET");

        List<String> holidayDates = new ArrayList<String>();

        DateTime fromDT = new DateTime(from, dateTimeZone);
        DateTime toDT = new DateTime(to, dateTimeZone);

        int numberOfDays = Days.daysBetween(fromDT, toDT).getDays();

        for (int k = 0; k <= numberOfDays; k++) {
            holidayDates.add("m" + fromDT.getMonthOfYear() + "d" + fromDT.getDayOfMonth());
            fromDT = fromDT.plusDays(1);
            holidayDays++;
        }
        return holidayDates;
    }
}