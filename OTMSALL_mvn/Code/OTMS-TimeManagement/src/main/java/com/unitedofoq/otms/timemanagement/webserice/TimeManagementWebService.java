/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.webserice;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.LegalEntity;
import com.unitedofoq.otms.timemanagement.CalendarServiceLocal;
import com.unitedofoq.otms.timemanagement.MultiEntryImport;
import com.unitedofoq.otms.timemanagement.TmWebServiceSetup;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author dev
 */
@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.timemanagement.TimeManagementWebServiceLocal",
        beanInterface = TimeManagementWebServiceLocal.class)
public class TimeManagementWebService implements TimeManagementWebServiceLocal {

    @EJB
    private OEntityManagerRemote oem;

    @EJB
    private CalendarServiceLocal calendarSer;

    @Override
    public OFunctionResult getDataFromMachine(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        callAllWS(loggedUser);
        insertDataInTable(loggedUser);
        return calendarSer.importMultiEntryAttendance(odm, params, loggedUser);
    }

    private SOAPMessage createSOAPRequest(int maxSerial, String userName, String pass) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        String soapBody = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">\n"
                + "   <soap:Header/>\n"
                + "   <soap:Body>\n"
                + "      <tem:GetAtt>\n"
                + "         <tem:MaxSerial>" + maxSerial + "</tem:MaxSerial>\n"
                + "         <!--Optional:-->\n"
                + "         <tem:UserName>" + userName + "</tem:UserName>\n"
                + "         <!--Optional:-->\n"
                + "         <tem:Pass>" + pass + "</tem:Pass>\n"
                + "      </tem:GetAtt>\n"
                + "   </soap:Body>\n"
                + "</soap:Envelope>";

        SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soapBody.getBytes()));
        String x = soapMessage.getContentDescription();
        //soapMessage.setProperty("maxReceivedMessageSize", 80000000);
        return soapMessage;
    }

    private void callWebService(String url, String userName, String pass, int maxSerial) {

        SOAPConnection soapConnection = null;
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            // Send SOAP Message to SOAP Server
//            int maxSerial = 605000;
//            String userName = "SWDHR";
//            String pass = "SWD@9759";
//            String url = "http://217.139.39.152:8899/OTMSATTService.asmx";
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(maxSerial, userName, pass), url);
            try {
                FileOutputStream fos = new FileOutputStream("entries.xml");
                soapResponse.writeTo(fos);
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TimeManagementWebService.class.getName()).log(Level.SEVERE, null, ex);

            }

        } catch (Exception ex) {

            Logger.getLogger(TimeManagementWebService.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            if (soapConnection != null) {
                try {

                    //  soapConnection.close();
                } catch (Exception ex) {
                    Logger.getLogger(TimeManagementWebService.class.getName()).log(Level.SEVERE, null, ex);

                }
            }
        }

    }

    private void insertDataInTable(OUser loggedUser) {
        try {
            File inputFile = new File("entries.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            NodeList nList = doc.getElementsByTagName("Table");

            for (int i = 0; i < nList.getLength(); i++) {
                MultiEntryImport entry = new MultiEntryImport();
                Node nNode = nList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String empCode = eElement.getElementsByTagName("EmployeeID").item(0).getTextContent();
                    List<String> conds = new ArrayList<String>();
                    conds = new ArrayList<String>();
                    conds.add("code=" + empCode);
                    Employee emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), conds, null, loggedUser);
                    String legalEntityCode = eElement.getElementsByTagName("GroupCompanyID").item(0).getTextContent();
                    conds.clear();
                    conds.add("code=" + legalEntityCode);
                    LegalEntity legalEntity = (LegalEntity) oem.loadEntity(LegalEntity.class.getSimpleName(), conds, null, loggedUser);
                    if (emp != null && legalEntity != null) {
                        String transactionType = eElement.getElementsByTagName("TransInOrOut").item(0).getTextContent();
                        String transactionTime = eElement.getElementsByTagName("TransactionTime").item(0).getTextContent();
                        String transactionDate = eElement.getElementsByTagName("TransactionDate").item(0).getTextContent();
                        String serial = eElement.getElementsByTagName("OFOQTranseferedSerial").item(0).getTextContent();

                        entry.setEntryDate(df.parse(transactionDate));
                        entry.setEmployee(emp);
                        entry.setType(transactionType);
                        entry.setEntryTime(getTimeFromString(transactionTime));
                        entry.setMaxSerial(Integer.parseInt(serial));
                        entry.setLegalEntity(legalEntity);
                        oem.saveEntity(entry, loggedUser);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TimeManagementWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getTimeFromString(String machineTime) {
        String hours = "";
        String minutes = "";
        try {
            hours = machineTime.substring(machineTime.indexOf("T") + 1, machineTime.indexOf("H"));
            if (hours.length() < 2) {
                hours = "0" + hours;
            }
            if (!machineTime.contains("M")) {
                minutes = "00";
            } else {
                minutes = machineTime.substring(machineTime.indexOf("H") + 1, machineTime.indexOf("M"));
                if (minutes.length() < 2) {
                    minutes = "0" + minutes;
                }
            }
            return hours + ":" + minutes;
        } catch (Exception ex) {
            Logger.getLogger(TimeManagementWebService.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("************" + machineTime + "--------------------------");
            return "00:00";
        }
    }

    private void callAllWS(OUser loggedUser) {
        try {

            List<TmWebServiceSetup> setups = oem.loadEntityList(TmWebServiceSetup.class.getSimpleName(),
                    null, null, null, loggedUser);
            List<String> urls = new ArrayList<>();
            if (!setups.isEmpty()) {
                for (int i = 0; i < setups.size(); i++) {
                    if (!urls.contains(setups.get(i).getUrl())) {
                        urls.add(setups.get(i).getUrl());
                        // String sql = "select Max(MAXSERIAL) from TM_MULTIENTRY_IMPORT where legalentity_dbid=" + setups.get(i).getLegalEntity().getDbid();
                        String sql = "select Max(MAXSERIAL) from TM_MULTIENTRY_IMPORT where legalentity_dbid in((select legalentity_dbid from TmWebServiceSetup"
                                + " where url ='" + setups.get(i).getUrl() + "'))";
                        BigDecimal maxSerial = (BigDecimal) oem.executeEntityNativeQuery(sql, loggedUser);
                        if (maxSerial != null) {
                            callWebService(setups.get(i).getUrl(), setups.get(i).getUserName(), setups.get(i).getPassword(), maxSerial.intValue());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TimeManagementWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public OFunctionResult fetchManual(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        TmWebServiceSetup serviceSetup = (TmWebServiceSetup) odm.getData().get(0);
        try {
            //String sql = "select Max(MAXSERIAL) from TM_MULTIENTRY_IMPORT where legalentity_dbid=" + serviceSetup.getLegalEntity().getDbid();
            String sql = "select Max(MAXSERIAL) from TM_MULTIENTRY_IMPORT where legalentity_dbid in((select legalentity_dbid from TmWebServiceSetup"
                    + " where url ='" + serviceSetup.getUrl() + "'))";
            BigDecimal maxSerial = (BigDecimal) oem.executeEntityNativeQuery(sql, loggedUser);
            if (maxSerial != null) {
                callWebService(serviceSetup.getUrl(), serviceSetup.getUserName(), serviceSetup.getPassword(), maxSerial.intValue());
                insertDataInTable(loggedUser);
                return calendarSer.importMultiEntryAttendance(odm, params, loggedUser);
            }

        } catch (Exception ex) {
            Logger.getLogger(TimeManagementWebService.class.getName()).log(Level.SEVERE, null, ex);

        }
        return ofr;
    }
}
