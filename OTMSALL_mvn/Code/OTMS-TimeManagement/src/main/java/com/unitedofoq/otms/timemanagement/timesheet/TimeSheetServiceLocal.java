package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author lap2
 */
@Local
public interface TimeSheetServiceLocal {
    public OFunctionResult updateTimeSheet(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult setTimeSheetData(ODataMessage odm, 
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult setTimeSheet(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult importTimeSheet(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult timeSheetConsolidation(ODataMessage odm, 
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult timeSheetConsolidationPosting(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult fetchTMSheetToConsolidation(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult singleTimeSheetConsolidationPosting(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);    
    public OFunctionResult updateDayOffs(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
    public OFunctionResult importMonthlyTimeSheet(ODataMessage odm, 
            OFunctionParms params, OUser loggedUser);
}
