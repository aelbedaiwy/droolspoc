package com.unitedofoq.otms.timemanagement.timesheet;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.PaymentMethod;
import com.unitedofoq.otms.payroll.PaymentPeriod;
import com.unitedofoq.otms.payroll.PayrollParameter;
import com.unitedofoq.otms.payroll.salaryelement.Deduction;
import com.unitedofoq.otms.payroll.salaryelement.Income;
import com.unitedofoq.otms.personnel.absence.Absence;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.personnel.dayoff.DayOff;
import com.unitedofoq.otms.personnel.penalty.EmployeePenaltyRequest;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationRequest;
import com.unitedofoq.otms.personnel.vacation.Vacation;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.timemanagement.CalendarServiceLocal;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendance;
import com.unitedofoq.otms.timemanagement.employee.EmployeeShift;
import com.unitedofoq.otms.timemanagement.employee.EmployeeTimeSheet;
import com.unitedofoq.otms.timemanagement.workingcalendar.ChristianCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.HijriCalendarReflection;
import com.unitedofoq.otms.timemanagement.workingcalendar.HolidayCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.FacesContext;
import org.joda.time.DateTime;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.timemanagement.timesheet.TimeSheetServiceLocal",
        beanInterface = TimeSheetServiceLocal.class)
public class TimeSheetService implements TimeSheetServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private TimeSheetServiceLocal timeSheetService;
    @EJB
    private CalendarServiceLocal calendarServiceLocal;
    @EJB
    private WSUtilityLocal webServiceUtility;
    @EJB
    private DataSecurityServiceLocal dataSecurityServiceLocal;

    Map<Long, Map<String, TMSheetRule>> projNatureRule = new HashMap<Long, Map<String, TMSheetRule>>();

    DateTimeUtility dtUtility = new DateTimeUtility();

    private OFunctionResult consolidationPosting(List<TimeSheetConsolidation> consolidations, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            CalculatedPeriod calculatedPeriod = consolidations.get(0).getCalculatedPeriod();
            TMCalendar tmCalendar;
            NormalWorkingCalendar workingCalendar;
            Absence absence = null;
            Penalty penalty = null;
            Income income = null;
            Deduction deduction = null;
            TimeSheetConsolidation consolidation;
            List<TimeSheetRule> timeSheetRules = null;
            TimeSheetRule timeSheetRule = null;
            Employee employee;
            EmployeeTimeSheet empTimeSheet;

            String updateSE = "";
            String attName, nature, select;
            Long incomeDbid, dedDbid, projDbid;

            Object temp, attValueObj;
            BigDecimal attValue;

            Date endDate = null, startDate = null, currentDate = null;
            List<Date> startEndDate;
            Map<String, Date> daysFields = new HashMap<String, Date>();

            List<String> conds = new ArrayList<String>();

            // get cutoffs
            startEndDate = getCutOffs(calculatedPeriod, loggedUser);
            if (startEndDate != null && !startEndDate.isEmpty()) {
                startDate = startEndDate.get(0);
                endDate = startEndDate.get(1);
            } else {
                ofr.addError(usrMsgService.getUserMessage("TMSheet_CutoffDatesProblem", loggedUser));
                return ofr;
            }

            // iterate on all consolidations within the calcPeriod & unit chosen
            for (int i = 0; i < consolidations.size(); i++) {
                consolidation = consolidations.get(i);
                employee = consolidation.getEmployee();
                tmCalendar = employee.getCalendar();
                workingCalendar = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(),
                        Collections.singletonList("calendar.dbid = " + tmCalendar.getDbid()), null, loggedUser);

                conds.clear();
                conds.add("employee.dbid = " + employee.getDbid());
                conds.add("plannerYear = '" + dtUtility.YEAR_FORMAT.format(endDate) + "'");
                conds.add("recordNum = 1");
                empTimeSheet = (EmployeeTimeSheet) oem.loadEntity(EmployeeTimeSheet.class.getSimpleName(), conds, null, loggedUser);

                if (calculatedPeriod != null) {

                    // get the proj of the employee, to get the rules on each nature
                    select = "select project_dbid from employeeproject where employee_Dbid = " + employee.getDbid()
                            + " and deleted = 0";
                    temp = oem.executeEntityNativeQuery(select, loggedUser);
                    if (temp == null) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_EmpProjMissing", Collections.singletonList(employee.getCode()), loggedUser));
                        continue;
                    }
                    projDbid = Long.parseLong(temp.toString());
                    //natureRule = projNatureRule.get(projDbid);

                    conds.clear();
                    conds.add("calendar.dbid = " + workingCalendar.getDbid());
                    conds.add("projectNature.project.dbid = " + projDbid);
                    timeSheetRules = (List<TimeSheetRule>) oem.loadEntityList(TimeSheetRule.class.getSimpleName(), conds, null, null, loggedUser);

                    for (int j = 0; j < timeSheetRules.size(); j++) {
                        timeSheetRule = timeSheetRules.get(j);

                        attName = "attribute" + timeSheetRule.getAttributeNo();
                        attValueObj = BaseEntity.getValueFromEntity(consolidation, attName);
                        if (attValueObj == null) {
                            continue;
                        }
                        attValue = new BigDecimal(attValueObj.toString());

                        // update salary elements values
                        income = timeSheetRule.getIncome();
                        deduction = timeSheetRule.getDeduction();
                        incomeDbid = income != null ? income.getDbid() : 0;
                        dedDbid = deduction != null ? deduction.getDbid() : 0;

                        updateSE = "update employeesalaryelement set value = " + attValue + " where deleted = 0 "
                                + " and employee_dbid = " + employee.getDbid() + " and (salaryelement_dbid = " + incomeDbid
                                + " or salaryelement_dbid = " + dedDbid + ")";
                        oem.executeEntityUpdateNativeQuery(updateSE, loggedUser);

                        // add vacations, absences & penalties
                        // ** Vacations will be excluded, as they are already inserted on DA or TM Sheet
                        // ** Absences will be calculated
                        absence = timeSheetRule.getAbsence();
                        penalty = timeSheetRule.getPenalty();

                        daysFields = generateTMSheetConsFields(startDate, endDate);

                        // insert absence
                        if (absence != null && absence.getDbid() != 0) {
                            for (String fieldName : daysFields.keySet()) {
                                nature = BaseEntity.getValueFromEntity(empTimeSheet, fieldName).toString();
                                currentDate = daysFields.get(fieldName);

                                if (timeSheetRule.getProjectNature().getSymbol().equals(nature)) {
                                    ofr.append(dayOffRequest(employee, absence, true, currentDate, loggedUser));
                                }
                            }
                        } else if (penalty != null && penalty.getDbid() != 0) {
                            ofr.append(penaltyRequest(employee, penalty, attValue, loggedUser));
                        }
                    }
                }
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }

    @Override
    public OFunctionResult updateDayOffs(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            NormalWorkingCalendar normalWorkingCalendar;
            normalWorkingCalendar = (NormalWorkingCalendar) odm.getData().get(0);

            long calendarDBID = normalWorkingCalendar.getDbid();

            String sql = "select dayOff1_dbid, dayOff2_dbid "
                    + "from normalworkingcalendar where dbid = " + calendarDBID;

            Object[] object = (Object[]) oem.executeEntityNativeQuery(sql, loggedUser);

            Long dayOff1_newDBID, dayOff2_newDBID = null;

            List<String> conds = new ArrayList<String>();
            conds.add("employee.calendar.dbid = " + normalWorkingCalendar.getCalendar().getDbid());
            conds.add("recordNum = 1");

            List<EmployeeTimeSheet> timeSheets = (List<EmployeeTimeSheet>) oem.loadEntityList(EmployeeTimeSheet.class.getSimpleName(),
                    conds, null, null, loggedUser);

            if (object == null) {

                dayOff1_newDBID = normalWorkingCalendar.getDayOff1().getDbid();
                dayOff2_newDBID = normalWorkingCalendar.getDayOff2().getDbid();

                dayOffCaseOldNull(timeSheets, dayOff1_newDBID, dayOff2_newDBID, loggedUser);
            }//end
            else {

                Long dayOff1_oldDBID = Long.parseLong(object[0].toString());
                Long dayOff2_oldDBID = null;

                dayOff1_newDBID = normalWorkingCalendar.getDayOff1().getDbid();

                // dayoff2 has a new value
                if (object[1] != null) {
                    dayOff2_oldDBID = Long.parseLong(object[1].toString());

                    if (normalWorkingCalendar.getDayOff2() != null) {
                        dayOff2_newDBID = normalWorkingCalendar.getDayOff2().getDbid();
                    }
                } else { // dayoff2 became null
                    if (normalWorkingCalendar.getDayOff2() != null) {
                        dayOff2_newDBID = normalWorkingCalendar.getDayOff2().getDbid();
                    }
                }
                // reset the old value to either holiday, vacation or absence
                dayOffCaseNewValue(timeSheets,
                        dayOff1_oldDBID, dayOff2_oldDBID, loggedUser);
                // set the new value of the new day off
                dayOffCaseOldNull(timeSheets,
                        dayOff1_newDBID, dayOff2_newDBID, loggedUser);

            }//end
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }

    private void dayOffCaseOldNull(List<EmployeeTimeSheet> timeSheets, Long dayOff1_dbid,
            Long dayOff2_dbid, OUser loggedUser) {
        try {
            String sql;
            //loop on employeeTimeSheet
            for (int i = 0; i < timeSheets.size(); i++) {
                //<editor-fold defaultstate="collapsed" desc="Case A: null--> value">

                //<editor-fold defaultstate="collapsed" desc="check if dayOff1 have a new value">
                if (dayOff1_dbid != null) {
                    int plannerYear = Integer.parseInt(timeSheets.get(i).getPlannerYear());

                    Date hiringDate = timeSheets.get(i).getEmployee().getHiringDate();

                    int year = plannerYear - 1900;

                    Date startDate = new Date(year, 0, 1);
                    Date endDate = new Date(year, 11, 31);

                    if (hiringDate.compareTo(startDate) > 0) {
                        startDate = hiringDate;
                    }

                    sql = "select code from udc where dbid = " + dayOff1_dbid;
                    Object udcCode = oem.executeEntityNativeQuery(sql, loggedUser);

                    List<Date> dates = calendarServiceLocal.getDateFromDay(startDate, endDate,
                            Integer.parseInt(udcCode.toString()));

                    //loop on dates
                    int month, day;
                    for (int j = 0; j < dates.size(); j++) {
                        month = (dates.get(j).getMonth()) + 1;
                        day = dates.get(j).getDate();
                        BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day, "O");
                    }//end of loop on dates
                    //oem.saveEntity(timeSheets.get(i), loggedUser);

                }//end chech if dayOff1 was null and have a new value
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="check if dayOff2 have a new value">
                if (dayOff2_dbid != null) {
                    int plannerYear = Integer.parseInt(timeSheets.get(i).getPlannerYear());

                    Date hiringDate = timeSheets.get(i).getEmployee().getHiringDate();

                    int year = plannerYear - 1900;

                    Date startDate = new Date(year, 0, 1);
                    Date endDate = new Date(year, 11, 31);

                    if (hiringDate.compareTo(startDate) > 0) {
                        startDate = hiringDate;
                    }

                    sql = "select code from udc where dbid = " + dayOff2_dbid;
                    Object udcCode = oem.executeEntityNativeQuery(sql, loggedUser);

                    List<Date> dates = calendarServiceLocal.getDateFromDay(startDate, endDate,
                            Integer.parseInt(udcCode.toString()));

                    //loop on dates
                    int month, day;
                    for (int j = 0; j < dates.size(); j++) {
                        month = (dates.get(j).getMonth()) + 1;
                        day = dates.get(j).getDate();
                        BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day, "O");
                    }//end of loop on dates
                    //  oem.saveEntity(timeSheets.get(i), loggedUser);

                }//end chech if dayOff1 was null and have a new value
                //</editor-fold>

                oem.saveEntity(timeSheets.get(i), loggedUser);
                //</editor-fold>
            }//end loop on employeeTimeSheet
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private void dayOffCaseNewValue(List<EmployeeTimeSheet> timeSheets,
            Long dayOff1_newDBID, Long dayOff2_newDbid,
            OUser loggedUser) throws Exception {

        String sql;
        List<String> conds = new ArrayList<String>();
        conds.clear();

        conds.add("company.dbid = " + loggedUser.getUserPrefrence1());
        List<HolidayCalendar> holidayCalendars = oem.loadEntityList(HolidayCalendar.class.getSimpleName(), conds,
                null, null, loggedUser);

        Object udcCode;
        int plannerYear, year;
        Date hiringDate, startDate, endDate;
        Date vacStartDate, vacEndDate;
        List<Date> vactionDates = new ArrayList<Date>();
        List<Date> dayOffDates = new ArrayList<Date>();
        List<Date> holidayDates = new ArrayList<Date>();

        int month, day;
        boolean flag = false;

        Date dateFrom;
        Date dateTo;
        for (int i = 0; i < holidayCalendars.size(); i++) {
            dateFrom = holidayCalendars.get(i).getDateFrom();
            dateTo = holidayCalendars.get(i).getDateTo();
            holidayDates.addAll(calendarServiceLocal.getHolidayDates(dateFrom, dateTo));
        }

        //loop on employeeTimeSheet
        for (int i = 0; i < timeSheets.size(); i++) {

            plannerYear = Integer.parseInt(timeSheets.get(i).getPlannerYear());
            hiringDate = timeSheets.get(i).getEmployee().getHiringDate();

            year = plannerYear - 1900;

            startDate = new Date(year, 0, 1);
            endDate = new Date(year, 11, 31);

            if (hiringDate.compareTo(startDate) > 0) {
                startDate = hiringDate;
            }

            conds.clear();
            conds.add("employee.dbid = " + timeSheets.get(i).getDbid());
            List<EmployeeVacationRequest> employeeVacationRequests = (List<EmployeeVacationRequest>) oem.loadEntityList(
                    EmployeeVacationRequest.class.getSimpleName(), conds, null, null, loggedUser);

            for (int j = 0; j < employeeVacationRequests.size(); j++) {
                vacStartDate = employeeVacationRequests.get(j).getStartDate();
                vacEndDate = employeeVacationRequests.get(j).getEndDate();
                vactionDates.addAll(calendarServiceLocal.getHolidayDates(vacStartDate, vacEndDate));
            }

            //<editor-fold defaultstate="collpased" desc="dayOff1">
            sql = "select code from udc where dbid = " + dayOff1_newDBID;

            udcCode = oem.executeEntityNativeQuery(sql, loggedUser);

            dayOffDates = calendarServiceLocal.getDateFromDay(startDate, endDate,
                    Integer.parseInt(udcCode.toString()));
            //loop on dates
            for (int j = 0; j < dayOffDates.size(); j++) {

                month = (dayOffDates.get(j).getMonth()) + 1;
                day = dayOffDates.get(j).getDate();

                for (int k = 0; k < holidayCalendars.size(); k++) {

                    if (dayOffDates.get(j).compareTo(holidayDates.get(k)) == 0) {
                        flag = true;
                        BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day, "H");
                    }
                }

                if (flag == false) {
                    for (int k = 0; k < vactionDates.size(); k++) {

                        if (dayOffDates.get(j).compareTo(vactionDates.get(k)) == 0) {
                            flag = true;
                            BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day,
                                    employeeVacationRequests.get(k).getVacation().getShortDescription());
                        }
                    }
                }
                if (flag == false) {
                    BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day, "A");
                }
            }
            //</editor-fold>

            if (dayOff2_newDbid != null) {
                //<editor-fold defaultstate="collpased" desc="dayOff2">

                sql = "select code from udc where dbid = " + dayOff2_newDbid;

                udcCode = oem.executeEntityNativeQuery(sql, loggedUser);

                plannerYear = Integer.parseInt(timeSheets.get(i).getPlannerYear());
                hiringDate = timeSheets.get(i).getEmployee().getHiringDate();

                year = plannerYear - 1900;

                dayOffDates = calendarServiceLocal.getDateFromDay(startDate, endDate,
                        Integer.parseInt(udcCode.toString()));

                //loop on dates
                for (int a = 0; a < dayOffDates.size(); a++) {

                    month = (dayOffDates.get(a).getMonth()) + 1;
                    day = dayOffDates.get(a).getDate();

                    for (int k = 0; k < holidayCalendars.size(); k++) {

                        if (dayOffDates.get(a).compareTo(holidayDates.get(k)) == 0) {
                            flag = true;
                            BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day, "H");
                        }
                    }

                    if (flag == false) {
                        for (int k = 0; k < vactionDates.size(); k++) {

                            if (dayOffDates.get(a).compareTo(vactionDates.get(k)) == 0) {
                                flag = true;
                                BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day,
                                        employeeVacationRequests.get(k).getVacation().getShortDescription());
                            }
                        }
                    }
                    if (flag == false) {
                        BaseEntity.setValueInEntity(timeSheets.get(i), "m" + month + "D" + day, "A");
                    }
                }

                //</editor-fold>
            }
            //}//end loop on employeeTimeSheet
            oem.saveEntity(timeSheets.get(i), loggedUser);
        }
    }

    class TMSheetRule {

        String attNum;
        BigDecimal factor;

        public String getAttNum() {
            return attNum;
        }

        public void setAttNum(String attNum) {
            this.attNum = attNum;
        }

        public BigDecimal getFactor() {
            return factor;
        }

        public void setFactor(BigDecimal factor) {
            this.factor = factor;
        }
    };

    @Override
    public OFunctionResult updateTimeSheet(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        List<String> plannerConditions = new ArrayList<String>();
        Object arglist[] = new Object[1];

        Employee employee = null;
        Vacation vacation = null;
        NormalWorkingCalendar calendar = null;
        TMCalendar tmcalendar = null;

        if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeVacationRequest")) {
            EmployeeVacationRequest vacationRequest = (EmployeeVacationRequest) odm.getData().get(0);
            employee = vacationRequest.getEmployee();
            vacation = vacationRequest.getVacation();
            tmcalendar = employee.getCalendar();
            if (tmcalendar.getType().getCode().equals("Regular")) {
                List<String> conds = new ArrayList<String>();
                conds.add("calendar.dbid = " + tmcalendar.getDbid());
                try {
                    calendar = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            DateTime startDT = new DateTime(vacationRequest.getStartDate(), dtUtility.dateTimeZone);

            Integer currentYear = startDT.getYear();

            List<String> vacationMethods = calendarServiceLocal.getEmployeeVacations(vacationRequest.getStartDate(), vacationRequest.getEndDate(), currentYear);
            plannerConditions.add("plannerYear='" + currentYear.toString() + "'");
            plannerConditions.add("employee.dbid=" + employee.getDbid());
            plannerConditions.add("recordNum = 1");
            EmployeeTimeSheet timeSheet = null;
            try {
                timeSheet = (EmployeeTimeSheet) oem.loadEntity(EmployeeTimeSheet.class.getSimpleName(), plannerConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
            }

            List<String> udcConditions = new ArrayList<String>();
            UDC dayNature = null;
            udcConditions.add("type.code='900'");
            if (!vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                udcConditions.add("code='Vacation'");
            } else {
                udcConditions.add("code='Absence'");
            }

            try {
                dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (timeSheet != null) {
                Class types[] = new Class[1];
                types[0] = Void.class;
                for (int j = 0; j < vacationMethods.size(); j++) {
                    String[] temp = vacationMethods.get(j).split("D");
                    String[] temp2 = temp[0].split("m");
                    String month = temp2[1];
                    String day = temp[1];

                    String dayName = "";

                    String currentDateStr = currentYear + "-" + month + "-" + day;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date currentDate = null;
                    try {
                        currentDate = dateFormat.parse(currentDateStr);
                    } catch (ParseException ex) {
                        Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dayName = dtUtility.DAY_NAME_FORMAT.format(currentDate);

                    Object dayValue = null,
                            holiday = null;
                    try {
                        dayValue = BaseEntity.getValueFromEntity(timeSheet, vacationMethods.get(j));
                    } catch (Exception ex) {
                        Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (dayValue != null) {
                        String dayVal = (String) dayValue;
                        // get the holiday day
                        try {
                            holiday = oem.executeEntityNativeQuery("select dbid from holidaycalendar where datefrom <= '"
                                    + dateFormat.format(currentDate) + "' and dateto >= '" + dateFormat.format(currentDate) + "'"
                                    + " and deleted = 0 ", loggedUser);
                        } catch (Exception exception) {
                            OLog.logException(exception, loggedUser, arglist);
                        }
                        if (vacation.getRemoveHoliday().equals("Y") && holiday != null) {
                            arglist = new Object[1];
                            if (vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                                arglist[0] = "A";
                            } else {
                                arglist[0] = "H";
                            }
                            try {
                                BaseEntity.setValueInEntity(timeSheet, vacationMethods.get(j), arglist[0]);
                            } catch (Exception e) {
                                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, e);
                            }
                        } else if ((vacation.getRemoveHoliday().equals("N") && dayVal.equals("H"))
                                || (vacation.getRemoveHoliday().equals("Y") && !dayVal.equals("H"))) {
                            arglist = new Object[1];
                            if (vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                                arglist[0] = "A";
                            } else {
                                arglist[0] = vacation.getShortDescription();
                            }
                            try {
                                BaseEntity.setValueInEntity(timeSheet, vacationMethods.get(j), arglist[0]);
                            } catch (Exception e) {
                                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, e);
                            }
                        } else if ((vacation.getPayWorkingCalender().equals("Y") && calendar.getDayOff1() != null && dayName.equals(calendar.getDayOff1().getValue()))
                                || (vacation.getPayWorkingCalender().equals("Y") && calendar.getDayOff2() != null && dayName.equals(calendar.getDayOff2().getValue()))) {
                            arglist = new Object[1];
                            if (vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                                arglist[0] = "A";
                            } else {
                                arglist[0] = "O";
                            }
                            try {
                                BaseEntity.setValueInEntity(timeSheet, vacationMethods.get(j), arglist[0]);
                            } catch (Exception e) {
                                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, e);
                            }
                        } else if ((vacation.getPayWorkingCalender().equals("N") && dayVal.equals("O"))
                                || (vacation.getPayWorkingCalender().equals("Y") && !dayVal.equals("O"))) {
                            arglist = new Object[1];
                            if (vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                                arglist[0] = "A";
                            } else {
                                arglist[0] = vacation.getShortDescription();
                            }
                            try {
                                BaseEntity.setValueInEntity(timeSheet, vacationMethods.get(j), arglist[0]);
                            } catch (Exception e) {
                                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, e);
                            }
                        }
                    }

                    //<editor-fold defaultstate="collapsed" desc="insert or update records in daily attendance with vacation nature">
                    String exceptionFrom = null, exceptionTo = null;
                    if (vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                        DateFormat df = new SimpleDateFormat("HH:mm");
                        exceptionFrom = df.format(vacationRequest.getStartDate());
                        exceptionTo = df.format(vacationRequest.getEndDate());
                    }

                    List<String> empDAConditions = new ArrayList<String>();
                    EmployeeDailyAttendance loadedEmpDailyAttendance = null;
                    empDAConditions.add("employee.dbid = " + employee.getDbid());
                    empDAConditions.add("dailyDate = '" + dtUtility.DATE_FORMAT.format(currentDate) + "'");
                    try {
                        loadedEmpDailyAttendance = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), empDAConditions, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (loadedEmpDailyAttendance == null) {
                    } else {
                        loadedEmpDailyAttendance.setDayNature(dayNature);
                        loadedEmpDailyAttendance.setExceptionFrom(exceptionFrom);
                        loadedEmpDailyAttendance.setExceptionTo(exceptionTo);
                        entitySetupService.callEntityUpdateAction(loadedEmpDailyAttendance, loggedUser);
                    }
                    //</editor-fold>
                }
                entitySetupService.callEntityUpdateAction(timeSheet, loggedUser);
            }
        }
        return ofr;
    }

    /**
     *
     * @param odm
     * @param params
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult setTimeSheetData(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            ofr = timeSheetService.setTimeSheet(odm, params, loggedUser);

            List<Employee> returnedEmps = ofr.getReturnValues();

            HashMap emps = new HashMap();
            emps.put(1, returnedEmps);
            params = new OFunctionParms();
            params.setParams(emps);
            //setDAForNonExist(odm, params, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    /**
     *
     * @param odm
     * @param params
     * @param loggedUser
     * @return
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult setTimeSheet(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Calendar toDayCal = Calendar.getInstance();

            Date toDayDate = toDayCal.getTime();
            Date startDate = null;
            Date endYearDate;

            Integer currentYear = Integer.parseInt(dtUtility.YEAR_FORMAT.format(toDayDate));
            endYearDate = dtUtility.DATE_FORMAT.parse(currentYear + "-12-31");

            //<editor-fold defaultstate="collapsed" desc="EmployeeStartDate class">
            class EmployeeStartDate {

                Employee employee;

                public Employee getEmployee() {
                    return employee;
                }

                public void setEmployee(Employee employee) {
                    this.employee = employee;
                }
                Date startDate;

                public Date getStartDate() {
                    return startDate;
                }

                public void setStartDate(Date startDate) {
                    this.startDate = startDate;
                }
                int day;

                public int getDay() {
                    return day;
                }

                public void setDay(int day) {
                    this.day = day;
                }
                int month;

                public int getMonth() {
                    return month;
                }

                public void setMonth(int month) {
                    this.month = month;
                }
                int year;

                public int getYear() {
                    return year;
                }

                public void setYear(int year) {
                    this.year = year;
                }
            }
            //</editor-fold>

            List<Employee> employees = new ArrayList<Employee>();
            List<EmployeeStartDate> employeeStartDates = new ArrayList<EmployeeStartDate>();
            List<EmployeeTimeSheet> timeSheets = new ArrayList<EmployeeTimeSheet>();
            Map<Long, EmployeeShift> employeeShifts = new HashMap<Long, EmployeeShift>();

            Map<TMCalendar, List<Date>> regCalendarsMap = new HashMap<TMCalendar, List<Date>>();

            Company company = null;
            company = (Company) oem.loadEntity(Company.class.getSimpleName(), loggedUser.getUserPrefrence1(), null, null, loggedUser);

            String[] startDateParts = new String[3];
            int startMonth = 1, startDay = 1, startYear = 2012;
            int calculatedPeriodMonths = 12;

            if (company != null /*&& DAYMONTH_FORMAT.format(toDayDate).equals("12-31")*/) { // runs every year on 31-12-yyyy
                //<editor-fold defaultstate="collapsed" desc="insert calculated periods">
                List<String> payrollParameterconditions = new ArrayList<String>();
                payrollParameterconditions.add("description = 'default_pay_method'");
                payrollParameterconditions.add("company.dbid = " + company.getDbid());
                PayrollParameter parameter = null;
                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), payrollParameterconditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                }
                String paymentMethodDbid = "";
                if (parameter != null) {
                    paymentMethodDbid = parameter.getValueData();
                }

                payrollParameterconditions = new ArrayList<String>();
                payrollParameterconditions.add("description = 'default_pay_period'");
                payrollParameterconditions.add("company.dbid = " + company.getDbid());
                parameter = null;
                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), payrollParameterconditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                }
                String paymentPeriodDbid = "";
                if (parameter != null) {
                    paymentPeriodDbid = parameter.getValueData();
                }

                PaymentMethod paymentMethod = null;
                PaymentPeriod paymentPeriod = null;
                try {
                    paymentMethod = (PaymentMethod) oem.loadEntity(PaymentMethod.class.getSimpleName(), Collections.singletonList("dbid = " + paymentMethodDbid), null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    paymentPeriod = (PaymentPeriod) oem.loadEntity(PaymentPeriod.class.getSimpleName(), Collections.singletonList("dbid = " + paymentPeriodDbid), null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                }

                for (int i = 1; i <= calculatedPeriodMonths; i++) {
                    CalculatedPeriod calculatedPeriod = null;

                    List<String> conditions = new ArrayList<String>();
                    conditions.add("month = " + i);
                    conditions.add("year = " + currentYear);
                    conditions.add("paymentMethod.dbid = " + paymentMethodDbid);
                    conditions.add("payPeriod.dbid = " + paymentPeriodDbid);
                    try {
                        calculatedPeriod = (CalculatedPeriod) oem.loadEntity(CalculatedPeriod.class.getSimpleName(), conditions, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (calculatedPeriod == null) {
                        calculatedPeriod = new CalculatedPeriod();

                        calculatedPeriod.setMonth(i);
                        calculatedPeriod.setYear(currentYear);
                        calculatedPeriod.setDescription("Monthly " + i + "-" + currentYear);

                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        String endDay;

                        String startDateStr = currentYear + "-" + i + "-01";
                        String endDateStr;

                        Date calculatedPeriodStartDate = null;
                        Date endDate = null;

                        if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) {
                            endDay = "31";
                        } else if (i == 2) {
                            if (currentYear % 4 == 0) {
                                endDay = "29";
                            } else {
                                endDay = "28";
                            }
                        } else {
                            endDay = "30";
                        }

                        endDateStr = currentYear + "-" + i + "-" + endDay;

                        try {
                            calculatedPeriodStartDate = format.parse(startDateStr);
                            endDate = format.parse(endDateStr);
                        } catch (ParseException ex) {
                            OLog.logException(ex, loggedUser);
                        }
                        calculatedPeriod.setStartDate(calculatedPeriodStartDate);
                        calculatedPeriod.setEndDate(endDate);

                        calculatedPeriod.setPayPeriod(paymentPeriod);
                        calculatedPeriod.setPaymentMethod(paymentMethod);

                        entitySetupService.callEntityCreateAction(calculatedPeriod, loggedUser);
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="load employees">
                if (odm.getData().get(0).getClass().getSimpleName().equals("Unit")) {
                    Unit unit = (Unit) odm.getData().get(0);
                    List<String> unitConds = new ArrayList<String>();
                    unitConds.add("positionSimpleMode.unit.dbid = " + unit.getDbid());
                    employees = oem.loadEntityList(Employee.class.getSimpleName(),
                            unitConds, null, null, loggedUser);
                } else if (odm.getData().get(0).getClass().getSimpleName().equals("Employee")) {
                    Employee employee = (Employee) odm.getData().get(0);
                    employees.add(employee);
                } else {
                    List<String> conds = new ArrayList<String>();

                    List<Long> employeeDbids = (List<Long>) oem.executeEntityListNativeQuery("Select dbid From oemployee "
                            + "where oemployee.deleted = 0 and oemployee.dbid not in "
                            + "(select employee_dbid from EmployeeTimeSheet where "
                            + "planneryear = " + currentYear + ")"
                            + " and year(oemployee.hiringdate) <= " + currentYear, loggedUser);

                    employees = new ArrayList<Employee>();
                    Employee tempEmployee;
                    for (int i = 0; i < employeeDbids.size(); i++) {
                        conds.clear();
                        conds.add("positionSimpleMode.unit.company.dbid = " + company.getDbid());
                        conds.add("dbid = " + employeeDbids.get(i).toString());
                        tempEmployee = (Employee) oem.loadEntity(Employee.class.getSimpleName(),
                                conds, null, loggedUser);
                        if (tempEmployee != null) {
                            employees.add(tempEmployee);
                        }
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="get calendars">
                List<Long> calendarDbids = (List<Long>) oem.executeEntityListNativeQuery("Select Distinct calendar_dbid"
                        + " From oemployee where deleted = 0 ", loggedUser);//and dbid not in (select employee_dbid from EmployeeAnnualPlanner where "
                //+ "planneryear = " + currentYear + ")", loggedUser);

                List<TMCalendar> regCalendars = new ArrayList<TMCalendar>();
                if (calendarDbids != null) {
                    for (int i = 0; i < calendarDbids.size(); i++) {
                        if (calendarDbids.get(i) == null) {
                            continue;
                        }
                        regCalendars.add((TMCalendar) oem.loadEntity(TMCalendar.class.getSimpleName(), calendarDbids.get(i), null, null, loggedUser));
                    }
                    UDC dayOff1;
                    UDC dayOff2;
                    int offdaysNumber = 0;
                    for (int i = 0; i < regCalendars.size(); i++) {
                        if (regCalendars.get(i) == null) {
                            continue;
                        }
                        List<String> nwcCond = new ArrayList<String>();
                        nwcCond.add("calendar.dbid = " + regCalendars.get(i).getDbid());
                        offdaysNumber = 0;
                        //NormalWorkingCalendar nwc = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), nwcCond, null, loggedUser);
                        List<NormalWorkingCalendar> nwc = oem.loadEntityList(NormalWorkingCalendar.class.getSimpleName(), nwcCond, null, null, loggedUser);
                        for (int k = 0; k < nwc.size(); k++) {
                            //offdaysNumber = 0;
                            dayOff1 = nwc.get(k).getDayOff1();//employeeStartDates.get(i).getEmployee()
                            dayOff2 = nwc.get(k).getDayOff2();//employeeStartDates.get(i).getEmployee()

                            if (dayOff1 != null) {
                                offdaysNumber++;
                            }
                            if (dayOff2 != null) {
                                offdaysNumber++;
                            }
                            for (int j = 0; j < offdaysNumber; j++) {
                                if (j == 0) {
                                    regCalendarsMap.put(regCalendars.get(i), calendarServiceLocal.getDateFromDay(dtUtility.DATE_FORMAT.parse(currentYear + "-01-01"),
                                            dtUtility.DATE_FORMAT.parse(currentYear + "-12-31"), Integer.parseInt(dayOff1.getCode())));
                                } else {
                                    List<Date> dates = regCalendarsMap.get(regCalendars.get(i));
                                    dates.addAll(calendarServiceLocal.getDateFromDay(dtUtility.DATE_FORMAT.parse(currentYear + "-01-01"),
                                            dtUtility.DATE_FORMAT.parse(currentYear + "-12-31"), Integer.parseInt(dayOff2.getCode())));
                                    regCalendarsMap.put(regCalendars.get(i), dates);
                                }
                                Collections.sort(regCalendarsMap.get(regCalendars.get(i)));
                            }
                        }
                    }
                }
                //</editor-fold>

                class DateComparable implements Comparator<Object> {

                    @Override
                    public int compare(Object o1, Object o2) {
                        if ((((Date) o1).compareTo((Date) o2)) >= 0) {
                            return 0;
                        }
                        return 1;
                    }
                }

                List<List<Date>> holidayDates = new ArrayList<List<Date>>();
                EmployeeTimeSheet timeSheet = null;

                //<editor-fold defaultstate="collapsed" desc="load holidays">
                List<String> holidayConditions = new ArrayList<String>();
                holidayConditions.add("company.dbid=" + company.getDbid());
                holidayConditions.add("dateFrom >= '" + currentYear + "-01-01" + "'");
                holidayConditions.add("dateFrom <= '" + currentYear + "-12-31" + "'");
                List<HolidayCalendar> calendars;
                List<HijriCalendarReflection> hijriCalendarsRef = new ArrayList<HijriCalendarReflection>();
                List<ChristianCalendar> christianCalendars;

                try {
                    calendars = (List<HolidayCalendar>) oem.loadEntityList(HolidayCalendar.class.getSimpleName(), holidayConditions, null, null, loggedUser);
                    christianCalendars = (List<ChristianCalendar>) oem.loadEntityList(ChristianCalendar.class.getSimpleName(), holidayConditions, null, null, loggedUser);

                    List<Date> regHolidayDates = new ArrayList<Date>();
                    List<Date> hijriHolidayDates = new ArrayList<Date>();
                    List<Date> christianHolidayDates = new ArrayList<Date>();

                    for (int j = 0; j < calendars.size() && calendars != null; j++) {
                        Date from = calendars.get(j).getDateFrom();
                        Date to = calendars.get(j).getDateTo();

                        //holidays.addAll(getHolidayMethods(from, to));
                        regHolidayDates.addAll(calendarServiceLocal.getHolidayDates(from, to));
                    }
                    holidayDates.add(regHolidayDates);
                    //retHolidayCalendars.add(holidays);
                    for (int j = 0; j < christianCalendars.size() && christianCalendars != null; j++) {
                        Date from = christianCalendars.get(j).getDateFrom();
                        Date to = christianCalendars.get(j).getDateTo();

                        //christianHolidays.addAll(getHolidayMethods(from, to));
                        christianHolidayDates.addAll(calendarServiceLocal.getHolidayDates(from, to));
                    }
                    //retHolidayCalendars.add(christianHolidays);
                    holidayDates.add(christianHolidayDates);
                } catch (Exception ex) {
                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                }
                //</editor-fold>

                String selectStat = "select project_dbid from employeeproject where deleted = 0 and employee_Dbid = ";
                Object projDdid = null;
                String defNature = "A";

                //<editor-fold defaultstate="collapsed" desc="set absence by default">
                for (int i = 0; i < employees.size(); i++) {

                    if (employees.get(i).getPayroll() == null) {
                        ofr.addError(usrMsgService.getUserMessage("TM_PayrollMissing",
                                Collections.singletonList(employees.get(i).getCode()), loggedUser));
                        continue;
                    }

                    List<String> plannerConditions = new ArrayList<String>();
                    plannerConditions.add("plannerYear='" + currentYear.toString() + "'");
                    plannerConditions.add("employee.dbid=" + employees.get(i).getDbid());

                    try {
                        timeSheet = (EmployeeTimeSheet) oem.loadEntity(
                                EmployeeTimeSheet.class.getSimpleName(), plannerConditions, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (timeSheet == null) {
                        //the shift record
                        if (employees.get(i).getCalendar() != null && (employees.get(i).getCalendar().getType().getCode().equals("VariableShift")
                                || employees.get(i).getCalendar().getType().getCode().equals("Rotation"))) {
                            EmployeeShift employeeShift = null;
                            List<String> shiftConds = new ArrayList<String>();
                            shiftConds.add("employee.dbid = " + employees.get(i).getDbid());
                            shiftConds.add("plannerYear='" + currentYear.toString() + "'");
                            employeeShift = (EmployeeShift) oem.loadEntity(EmployeeShift.class.getSimpleName(), shiftConds, null, loggedUser);

                            if (employeeShift == null) {
                                employeeShift = new EmployeeShift();
                                employeeShift.setEmployee(employees.get(i));
                                employeeShift.setPlannerYear(currentYear.toString());
                            }
                            employeeShifts.put(employees.get(i).getDbid(), employeeShift);
                        }

                        // annual planner record
                        timeSheet = new EmployeeTimeSheet();
                        timeSheet.setEmployee(employees.get(i));
                        timeSheet.setPlannerYear(currentYear.toString());
                        timeSheet.setRecordNum(1);
                        try {
                            startDate = dtUtility.DATE_FORMAT.parse(currentYear.toString() + "-01-01");
                        } catch (ParseException ex) {
                            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (startDate.compareTo(employees.get(i).getHiringDate()) < 0) {
                            startDate = employees.get(i).getHiringDate();
                        }

                        startDateParts = dtUtility.DATE_FORMAT.format(startDate).split("-");
                        startMonth = Integer.parseInt(startDateParts[1]);
                        startDay = Integer.parseInt(startDateParts[2]);
                        startYear = Integer.parseInt(startDateParts[0]);

                        EmployeeStartDate tempEmployeeStartDate = new EmployeeStartDate();
                        tempEmployeeStartDate.setEmployee(employees.get(i));
                        tempEmployeeStartDate.setYear(startYear);
                        tempEmployeeStartDate.setMonth(startMonth);
                        tempEmployeeStartDate.setDay(startDay);
                        tempEmployeeStartDate.setStartDate(startDate);

                        employeeStartDates.add(tempEmployeeStartDate);

                        Project project = null;
                        projDdid = oem.executeEntityNativeQuery(selectStat + employees.get(i).getDbid(), loggedUser);
                        if (projDdid != null) {
                            project = (Project) oem.loadEntity(Project.class.getSimpleName(), Long.valueOf(projDdid.toString()), null, null, loggedUser);
                        }
                        defNature = getTimeSheetNature(project, employees.get(i).getPayroll().getTmEntitled());

                        for (int j = startMonth; j <= 12; j++) {
                            for (int k = startDay; k <= 31; k++) {
                                if (j == 2 && k == 30) {
                                    break;
                                } else if ((j == 4 || j == 6 || j == 9 || j == 11) && k == 31) {
                                    break;
                                }

                                BaseEntity.setValueInEntity(timeSheet, "m" + j + "D" + k, defNature);
                            }
                            startDay = 1;
                        }
                        timeSheets.add(timeSheet);
                        createSingleEmpTimeSheet(timeSheet, startDate, endYearDate, loggedUser);
                    }
                }
                //</editor-fold>

                for (int i = 0; i < employeeStartDates.size(); i++) {
                    OLog.logInformation("EmployeeAnnualPlanner", loggedUser, "employee.dbid", employees.get(i).getDbid(), i);

                    if (employeeStartDates.get(i).getEmployee() != null) {

                        Integer tempYear = employeeStartDates.get(i).getYear();
                        timeSheets.get(i).setPlannerYear(tempYear.toString());
                        timeSheets.get(i).setInActive(false);

                        boolean isVarShift = false;
                        if (employeeStartDates.get(i).getEmployee().getCalendar() != null
                                && employeeStartDates.get(i).getEmployee().getCalendar().getType().getCode().equals("VariableShift")) {
                            isVarShift = true;
                            EmployeeShift employeeShift = employeeShifts.get(employeeStartDates.get(i).getEmployee().getDbid());
                            employeeShift.setPlannerYear(tempYear.toString());
                        }

                        //<editor-fold defaultstate="collapsed" desc="load & set vacations">
                        List<String> dayOffConditions = new ArrayList<String>();
                        List<EmployeeVacationRequest> vacations = null;
                        List<String> vacationMethods = new ArrayList<String>();
                        HashMap<String, List<String>> vacationsMap = new HashMap<String, List<String>>();

                        String currentDateStr = "2012-01-01";
                        if (employeeStartDates.get(i).getStartDate() != null) {
                            currentDateStr = dtUtility.DATE_FORMAT.format(employeeStartDates.get(i).getStartDate());
                        }

                        String endDateStr = currentYear.toString() + "-12-31";
                        Date currentDate = null, endDate = null;
                        try {
                            currentDate = dtUtility.DATE_FORMAT.parse(currentDateStr);
                            endDate = dtUtility.DATE_FORMAT.parse(endDateStr);
                        } catch (ParseException ex) {
                            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        dayOffConditions.add("employee.dbid=" + employeeStartDates.get(i).getEmployee().getDbid());
                        dayOffConditions.add("endDate <= '" + dtUtility.DATE_FORMAT.format(endDate) + "'");
                        dayOffConditions.add("startDate >= '" + dtUtility.DATE_FORMAT.format(currentDate) + "'");
                        dayOffConditions.add("vacation.unitOfMeasure = 'D'");
                        dayOffConditions.add("cancelled = 'N'");
                        try {
                            vacations = (List<EmployeeVacationRequest>) oem.loadEntityList(EmployeeVacationRequest.class.getSimpleName(), dayOffConditions, null, null, loggedUser);
                        } catch (Exception ex) {
                            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        dayOffConditions = new ArrayList<String>();

                        if (vacations != null) {
                            for (int j = 0; j < vacations.size(); j++) {
                                vacationMethods.clear();
                                vacationMethods.addAll(calendarServiceLocal.getEmployeeVacations(vacations.get(j).getStartDate(), vacations.get(j).getEndDate(), currentYear));
                                for (int k = 0; k < vacationMethods.size(); k++) {
                                    BaseEntity.setValueInEntity(timeSheets.get(i), vacationMethods.get(k), vacations.get(j).getVacation().getShortDescription());
                                    if (employeeShifts.get(employees.get(i).getDbid()) != null) {
                                        BaseEntity.setValueInEntity(employeeShifts.get(employees.get(i).getDbid()), vacationMethods.get(k), "V");
                                    }
                                }
                            }
                        }
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="set holidays">
                        if (company != null) {

                            if (holidayDates != null) {
                                Collections.sort(holidayDates.get(0));
                                int holidayDateListIndex = 0;
                                for (int j = 0; j < holidayDates.get(0).size(); j++) {
                                    if (holidayDates.get(0).get(j).compareTo(currentDate) >= 0) {
                                        holidayDateListIndex = j;
                                        break;
                                    } else {
                                        holidayDateListIndex = holidayDates.get(0).size();
                                    }
                                }
                                //Collections.binarySearch(holidayDates.get(0), currentDate, null);
                                if (holidayDateListIndex < 0) {
                                    holidayDateListIndex = 0;
                                }
                                for (int j = holidayDateListIndex; j < holidayDates.get(0).size(); j++) {
                                    try {
                                        DateTime dt = new DateTime(holidayDates.get(0).get(j), dtUtility.dateTimeZone);

                                        BaseEntity.setValueInEntity(timeSheets.get(i), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                        if (employeeShifts.get(employees.get(i).getDbid()) != null) {
                                            BaseEntity.setValueInEntity(employeeShifts.get(employees.get(i).getDbid()), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                        }
                                    } catch (Exception ex) {
                                        Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }

                                //holidayDateListIndex = Collections.binarySearch(holidayDates.get(1), currentDate, new DateComparable());
                                UDC employeeReligion = null;
                                UDC chrisitianReligion = null;

                                List<String> udcConditions = new ArrayList<String>();
                                udcConditions.add("type.code='prs_019'");
                                udcConditions.add("code='CHR'");
                                try {
                                    chrisitianReligion = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                                } catch (Exception ex) {
                                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                if (employeeStartDates.get(i).getEmployee() != null && employeeStartDates.get(i).getEmployee() != null) {
                                    employeeReligion = employeeStartDates.get(i).getEmployee().getReligion();
                                }
                                if (employeeReligion != null && employeeReligion.getCode().equals(chrisitianReligion.getCode())) {

                                    for (int j = 0; j < holidayDates.get(1).size(); j++) {
                                        if (holidayDates.get(1).get(j).compareTo(currentDate) >= 0) {
                                            holidayDateListIndex = j;
                                            break;
                                        } else {
                                            holidayDateListIndex = holidayDates.get(1).size();
                                        }
                                    }
                                    if (holidayDateListIndex < 0) {
                                        holidayDateListIndex = 0;
                                    }
                                    for (int j = holidayDateListIndex; j < holidayDates.get(1).size(); j++) {
                                        try {

                                            DateTime dt = new DateTime(holidayDates.get(1).get(j), dtUtility.dateTimeZone);

                                            BaseEntity.setValueInEntity(timeSheets.get(i), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                            if (employeeShifts.get(i) != null) {
                                                BaseEntity.setValueInEntity(employeeShifts.get(i), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                            }
                                        } catch (Exception ex) {
                                            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                }
                                try {
                                    //oem.saveEntity(annualPlanners.get(i), loggedUser);

                                    // create empty record for the employee for inserting the num of hrs for each day.
                                    // e.g. in the 1st record OT, so in the 2nd record the user will insert 3 hrs
                                    timeSheet = new EmployeeTimeSheet();
                                    timeSheet.setEmployee(employees.get(i));
                                    timeSheet.setPlannerYear(currentYear.toString());
                                    timeSheet.setRecordNum(2);
                                    entitySetupService.callEntityCreateAction(timeSheet, loggedUser);

                                    // creating the time sheet original record
                                    entitySetupService.callEntityCreateAction(timeSheets.get(i), loggedUser);
                                    createSingleEmpTimeSheet(timeSheet, startDate, endYearDate, loggedUser);
                                    if (!employeeShifts.isEmpty() && employeeShifts.get(employees.get(i).getDbid()) != null) {
                                        entitySetupService.callEntityCreateAction(employeeShifts.get(employees.get(i).getDbid()), loggedUser);
                                    }
                                } catch (Exception e) {
                                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, e);
                                }
                            }
                            //</editor-fold>

                            //<editor-fold defaultstate="collapsed" desc="Calendar Part">
                            //<editor-fold defaultstate="collapsed" desc="Regular">
                            if (employees.get(i).getCalendar() != null
                                    && employees.get(i).getCalendar().getType() != null
                                    && (employees.get(i).getCalendar().getType().getCode().equals("Regular")
                                    || employees.get(i).getCalendar().getType().getCode().equals("Flexible"))) {
                                if (employeeStartDates.get(i).getEmployee() != null
                                        && employeeStartDates.get(i).getEmployee().getWorkingCalendar() != null) {

                                    List<Date> tempCalDates = regCalendarsMap.get(
                                            employeeStartDates.get(i).getEmployee().getCalendar());
                                    int dateListIndex = 0;
                                    if (tempCalDates != null) {
                                        for (int j = 0; j < tempCalDates.size(); j++) {
                                            if (tempCalDates.get(j).compareTo(currentDate) >= 0) {
                                                dateListIndex = j;
                                                break;
                                            }
                                        }
                                        //int dateListIndex = Collections.binarySearch(tempCalDates, currentDate, new DateComparable());
                                        if (dateListIndex < 0) {
                                            dateListIndex = 0;
                                        }

                                        if (tempCalDates != null) {
                                            for (int k = dateListIndex; k < tempCalDates.size(); k++) {
                                                Object arglist[] = new Object[1];
                                                arglist[0] = "O";
                                                try {

                                                    DateTime dt = new DateTime(tempCalDates.get(k), dtUtility.dateTimeZone);

                                                    BaseEntity.setValueInEntity(timeSheets.get(i), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "O");
                                                } catch (Exception ex) {
                                                    Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                            }
                                            //oem.saveEntity(annualPlanners.get(i), loggedUser);
                                            entitySetupService.callEntityUpdateAction(timeSheets.get(i), loggedUser);
                                        }
                                    }
                                }
                            }
                            //</editor-fold>
                            //<editor-fold defaultstate="collapsed" desc="Rotation">
                            if (employeeStartDates.get(i).getEmployee().getCalendar() != null
                                    && employeeStartDates.get(i).getEmployee().getCalendar().getType() != null
                                    && employeeStartDates.get(i).getEmployee().getCalendar().getType().getCode().equals("Rotation")) {

                                TMCalendar rotationCal = employeeStartDates.get(i).getEmployee().getCalendar();
                                Date empDate = employeeStartDates.get(i).getStartDate();
                                if (rotationCal != null) {
                                    Date referenceDate = rotationCal.getReferenceDate();

                                    int roundDuration = 0,
                                            numRounds = 0,
                                            numRoundsRem = 0;
                                    Map<Integer, NormalWorkingCalendar> rotCalsMap = new HashMap<Integer, NormalWorkingCalendar>();

                                    String select = "select SUM(numberOfDays) from normalworkingcalendar where calendar_dbid=" + rotationCal.getDbid();
                                    Object ret = oem.executeEntityNativeQuery(select, loggedUser);

                                    if (ret != null) {

                                        roundDuration = Integer.parseInt(ret.toString());
                                        numRounds = (dtUtility.getDaysBetween(empDate, endDate)) + 1 / roundDuration;
                                        numRoundsRem = (dtUtility.getDaysBetween(empDate, endDate)) + 1 % roundDuration;

                                        Object[] returned = calendarServiceLocal.getShift(rotationCal, empDate, loggedUser);
                                        if (returned != null && returned.length != 0) {
                                            if (returned[1] == null) {
                                                ofr.addError(usrMsgService.getUserMessage("WrongCalRetrieval", loggedUser));
                                                return ofr;
                                            }

                                            // fill map of calendars for each day
                                            List<String> conds = new ArrayList<String>(),
                                                    sort = new ArrayList<String>();

                                            conds.add("calendar.dbid = " + rotationCal.getDbid());

                                            sort.add("shiftSequence");

                                            List<NormalWorkingCalendar> rotShfts = oem.loadEntityList(NormalWorkingCalendar.class.getSimpleName(), conds, null, sort, loggedUser);
                                            calendarServiceLocal.setCalendarsMap(rotCalsMap, rotShfts, roundDuration);

                                            EmployeeShift empShift = employeeShifts.get(employeeStartDates.get(i).getEmployee().getDbid());

                                            String code = null,
                                                    dayName = null;

                                            int rDur = (Integer) returned[0];
                                            numRoundsRem += (rDur + 1); /* add to the remaining days; the days missed in the first rotation*/

                                            Calendar tempCal = Calendar.getInstance();
                                            tempCal.setTime(empDate);

                                            for (int r = 0; r < numRounds; r++) {
                                                for (; rDur < roundDuration; rDur++) {
                                                    code = rotCalsMap.get(rDur).getCode();
                                                    empDate = tempCal.getTime();
                                                    tempCal.add(Calendar.DAY_OF_MONTH, 1);
                                                    dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();

                                                    Object dayNature = BaseEntity.getValueFromEntity(empShift, dayName);
                                                    String strDayNature = dayNature != null ? dayNature.toString() : null;
                                                    if (strDayNature != null && strDayNature.equals("H")) {
                                                        continue;
                                                    }

                                                    BaseEntity.setValueInEntity(empShift, dayName, code);
                                                    if (rotCalsMap.get(rDur).getNumberOfHours().equals(BigDecimal.ZERO)) {
                                                        BaseEntity.setValueInEntity(timeSheets.get(i), dayName, "O");
                                                    }
                                                }
                                                rDur = 0;
                                            }

                                            // remaining days for the last round
                                            for (int r = 0; r < numRoundsRem; r++) {
                                                int index = r % (roundDuration);
                                                if (rotCalsMap.get(index) == null) {
                                                    continue;
                                                }
                                                code = rotCalsMap.get(index).getCode();
                                                empDate = tempCal.getTime();
                                                dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();
                                                tempCal.add(Calendar.DAY_OF_MONTH, 1);
                                                dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();

                                                Object dayNature = BaseEntity.getValueFromEntity(empShift, dayName);
                                                String strDayNature = dayNature != null ? dayNature.toString() : null;
                                                if (strDayNature != null && strDayNature.equals("H")) {
                                                    continue;
                                                }

                                                BaseEntity.setValueInEntity(empShift, dayName, code);
                                                if (rotCalsMap.get(index).getNumberOfHours().equals(BigDecimal.ZERO)) {
                                                    BaseEntity.setValueInEntity(timeSheets.get(i), dayName, "O");
                                                }
                                            }

                                            entitySetupService.callEntityUpdateAction(timeSheets.get(i), loggedUser);
                                            entitySetupService.callEntityUpdateAction(empShift, loggedUser);
                                        }
                                    }
                                }
                            }
                            //</editor-fold>
                            //<editor-fold defaultstate="collapsed" desc="Shift">
                            //</editor-fold>
                            //</editor-fold>
                        }
                    }
                }
                ofr.setReturnValues(employees);
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }

    @Override
    public OFunctionResult timeSheetConsolidation(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();

        EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) odm.getData().get(0);

        Long empDADbid;
        empDADbid = empDA.getDbid();
        EmployeeDailyAttendance loadedEmpDA = null;
        List<String> empDAConditions = new ArrayList<String>();
        empDAConditions.add("dbid = " + empDADbid);

        try {
            loadedEmpDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), empDAConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }

        BigDecimal loadedValue = new BigDecimal(BigInteger.ZERO);
        BigDecimal value = new BigDecimal(BigInteger.ZERO);
        ProjectNature projectNature = null;
        NormalWorkingCalendar calendar = null;
        TimeSheetRule timeSheetRule = null;
        TimeSheetConsolidation consolidation = null;
        CalculatedPeriod calculatedPeriod = null;
        List<String> conditions = new ArrayList<String>();
        String attributeNo;
        Method method = null;
        Object object = null;
        if (loadedEmpDA == null) {
            loadedEmpDA = empDA;
        }
        String timeOut;
        String timeIn;

        timeIn = loadedEmpDA.getTimeIn();
        timeOut = loadedEmpDA.getTimeOut();
        if (timeIn != null && timeOut != null) {
            loadedValue = new BigDecimal(dtUtility.convertToNumber(dtUtility.subtractTime(timeIn, timeOut))).setScale(5, RoundingMode.HALF_UP);
        }

        projectNature = empDA.getProjectNature();
        if (projectNature == null) {
            return ofr;
        }

        calendar = empDA.getShift();
        conditions.clear();
        if (calendar != null) {
            conditions.add("calendar.dbid = " + calendar.getDbid());
        }
        conditions.add("projectNature.dbid = " + projectNature.getDbid());
        try {
            timeSheetRule = (TimeSheetRule) oem.loadEntity(TimeSheetRule.class.getSimpleName(), conditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        attributeNo = timeSheetRule.getAttributeNo();
        DateTime dt = new DateTime(empDA.getDailyDate(), dtUtility.dateTimeZone);

        Integer month = dt.getMonthOfYear();
        Integer year = dt.getYear();
        Integer day = dt.getDayOfMonth();

        Integer cutOffStart;

        conditions.clear();
        conditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
        conditions.add("description = 'day_of_calculation'");
        PayrollParameter cutOffParameter = null;
        try {
            cutOffParameter = (PayrollParameter) oem.loadEntity(
                    PayrollParameter.class.getSimpleName(), conditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }

        cutOffStart = Integer.parseInt(cutOffParameter.getValueData()) + 1;

        if (day >= cutOffStart) {
            month = (month + 1) % 12;
            if (month == 1) {
                year++;
            }
        }
        conditions.clear();
        conditions.add("description = 'default_pay_method'");
        conditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
        PayrollParameter parameter = null;
        try {
            parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }

        conditions.clear();
        conditions.add("employee.dbid=" + loadedEmpDA.getEmployee().getDbid());
        conditions.add("calculatedPeriod.month = " + month.toString());
        conditions.add("calculatedPeriod.year = " + year.toString());
        conditions.add("calculatedPeriod.paymentMethod.dbid=" + parameter.getValueData());

        try {
            consolidation = (TimeSheetConsolidation) oem.loadEntity(TimeSheetConsolidation.class.getSimpleName(), conditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (consolidation == null) {
            consolidation = new TimeSheetConsolidation();
            consolidation.setEmployee(empDA.getEmployee());
            conditions.clear();
            conditions.add("month = " + month.toString());
            conditions.add("year = " + year.toString());
            conditions.add("paymentMethod.dbid=" + parameter.getValueData());
            try {
                calculatedPeriod = (CalculatedPeriod) oem.loadEntity(CalculatedPeriod.class.getSimpleName(), conditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
            }
            consolidation.setCalculatedPeriod(calculatedPeriod);
        }

        try {
            method = TimeSheetConsolidation.class.getMethod("getAttribute" + attributeNo, null);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (method != null) {
            try {
                object = method.invoke(consolidation, null);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (object != null) {
                value = new BigDecimal(object.toString());
            }
            if (value.compareTo(new BigDecimal(0)) > 0) {
                //subtraction
                if (loadedValue != null && loadedValue.compareTo(new BigDecimal(0)) > 0) {
                    value = value.subtract(loadedValue);
                }
                //addition
                if (loadedValue != null) {
                    value = value.add(loadedValue);
                }
            } else {
                value = loadedValue;
            }

        }
        BaseEntity.setValueInEntity(consolidation, "attribute" + attributeNo, value);
        try {
            oem.saveEntity(consolidation, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }

        ofr.setReturnedDataMessage(odm);
        return ofr;
    }

    @Override
    public OFunctionResult timeSheetConsolidationPosting(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {

            CalculatedPeriod calculatedPeriod = ((TimeSheetConsolidation) odm.getData().get(0)).getCalculatedPeriod();
            Company company = ((TimeSheetConsolidation) odm.getData().get(0)).getEmployee().getPositionSimpleMode().getUnit().getCompany();

            if (calculatedPeriod == null || company == null) {
                return ofr;
            }

            List<String> conds = new ArrayList<String>();
            //conds.add("posted = 'N'");
            conds.add("employee.positionSimpleMode.unit.company.dbid = "
                    + company.getDbid());
            conds.add("calculatedPeriod.dbid = " + calculatedPeriod.getDbid());

            List<TimeSheetConsolidation> consolidations = oem.loadEntityList(
                    TimeSheetConsolidation.class.getSimpleName(), conds, null,
                    null, loggedUser);

            if ((consolidations != null && consolidations.isEmpty()) || (consolidations == null)) {
                ofr.addWarning(usrMsgService.getUserMessage("TMSheet_NoConsToPost", loggedUser));
                return ofr;
            }

            // post the retrived consolidations
            ofr.append(consolidationPosting(consolidations, loggedUser));

        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult singleTimeSheetConsolidationPosting(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {

            CalculatedPeriod calculatedPeriod = ((TimeSheetConsolidation) odm.getData().get(0)).getCalculatedPeriod();
            Company company = ((TimeSheetConsolidation) odm.getData().get(0)).getEmployee().getPositionSimpleMode().getUnit().getCompany();

            if (calculatedPeriod == null || company == null) {
                return ofr;
            }

            List<TimeSheetConsolidation> consolidations = new ArrayList<TimeSheetConsolidation>();
            consolidations.add((TimeSheetConsolidation) odm.getData().get(0));

            if (consolidations.isEmpty()) {
                ofr.addWarning(usrMsgService.getUserMessage("TMSheet_NoConsToPost", loggedUser));
                return ofr;
            }

            // post the retrived consolidation
            ofr.append(consolidationPosting(consolidations, loggedUser));

        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofr;
    }

    /*
     * Loubna - 10/04/2014
     * For Seadrill Time Sheet; the consolidation is filled from the Time Sheet not from Daily Attendance as usual
     * This is an action button on the calc period, to fetch the data from the Time Sheet
     */
    @Override
    public OFunctionResult fetchTMSheetToConsolidation(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            CalculatedPeriod calcPeriod = (CalculatedPeriod) odm.getData().get(0);
            TimeSheetConsolidation consolidation = null;
            Employee employee = null;
            int month, day,
                    attCount = 100;
            Date startDate, endDate, tempDate;
            Calendar calendar = Calendar.getInstance();
            String selectRec1Stat = "",
                    selectRec2Stat = "",
                    select = "";
            String attributeNo;
            long empDbid, projDbid;
            double value;

            BigDecimal factor = BigDecimal.valueOf(1);

            List timeSheets,
                    timeSheetHrs;
            List<String> conds = new ArrayList<String>();
            List<Date> startEndDate;
            Object[] timeSheet,
                    timeSheetHr;
            Object temp;

            Map<String, TMSheetRule> natureRule = null;

            startEndDate = getCutOffs(calcPeriod, loggedUser);
            if (startEndDate != null && !startEndDate.isEmpty()) {
                startDate = startEndDate.get(0);
                endDate = startEndDate.get(1);
            } else {
                ofr.addError(usrMsgService.getUserMessage("TMSheet_CutoffDatesProblem", loggedUser));
                return ofr;
            }

            // create a map for project_dbid: nature_symbol: rule_attNo
            ofr.append(getProjectNatureRules(loggedUser));

            selectRec1Stat = "select employee_dbid, ";
            tempDate = startDate;
            calendar.setTime(tempDate);
            while (tempDate.compareTo(endDate) <= 0) {

                day = tempDate.getDate();
                month = tempDate.getMonth() + 1;

                selectRec1Stat += "m" + month + "D" + day;

                if (tempDate.compareTo(endDate) != 0) {
                    selectRec1Stat += ",";
                }

                calendar.add(Calendar.DAY_OF_MONTH, 1);
                tempDate = calendar.getTime();
            }
            selectRec1Stat += " from employeetimesheet where deleted = 0 "
                    + " and plannerYear = '" + dtUtility.YEAR_FORMAT.format(endDate) + "'";

            selectRec2Stat = selectRec1Stat + " and recordnum = 2 ";

            selectRec1Stat += " and recordNum = 1";
            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            String securityCond = "";
            if (!employeeDbidList.isEmpty()) {
                String employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                securityCond = " AND employee_dbid in " + employeeDbidStr;
            }
            selectRec1Stat += securityCond;
            timeSheets = oem.executeEntityListNativeQuery(selectRec1Stat, loggedUser);

            if (timeSheets != null && !timeSheets.isEmpty()) {
                for (int i = 0; i < timeSheets.size(); i++) {

                    timeSheet = (Object[]) timeSheets.get(i);
                    empDbid = Long.parseLong(timeSheet[0].toString());

                    employee = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggedUser);

                    conds.clear();
                    conds.add("employee.dbid= " + empDbid);
                    conds.add("calculatedPeriod.dbid = " + calcPeriod.getDbid());
                    consolidation = (TimeSheetConsolidation) oem.loadEntity(TimeSheetConsolidation.class.getSimpleName(),
                            conds, null, loggedUser);
                    if (consolidation == null) {
                        consolidation = new TimeSheetConsolidation();
                        consolidation.setCalculatedPeriod(calcPeriod);
                        consolidation.setEmployee(employee);
                        entitySetupService.callEntityCreateAction(consolidation, loggedUser);
                    }

                    timeSheetHrs = oem.executeEntityListNativeQuery(
                            selectRec2Stat + " and employee_Dbid = " + empDbid, loggedUser);
                    timeSheetHr = (Object[]) timeSheetHrs.get(0);

                    select = "select project_dbid from employeeproject where employee_Dbid = " + empDbid
                            + " and deleted = 0";
                    temp = oem.executeEntityNativeQuery(select, loggedUser);
                    if (temp == null) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_EmpProjMissing", Collections.singletonList(employee.getCode()), loggedUser));
                        continue;
                    }
                    projDbid = Long.parseLong(temp.toString());

                    natureRule = projNatureRule.get(projDbid);

                    if (timeSheet.length > 0) {

                        // reset the record; such that all att's = ZERO.
                        for (int j = 1; j <= attCount; j++) {

                            BaseEntity.setValueInEntity(consolidation,
                                    "attribute" + j, new BigDecimal(BigInteger.ZERO));
                        }
                        oem.saveEntity(consolidation, loggedUser);

                        // accumulate the values.
                        for (int j = 1; j < timeSheet.length; j++) {
                            if (timeSheetHr[j] == null || (timeSheetHr[j] != null && timeSheetHr[j].equals(""))
                                    || (timeSheetHr[j] != null && timeSheetHr[j].equals("0"))) {
                                timeSheetHr[j] = 1;
                            }

                            if (timeSheetHr[j] == null
                                    || (timeSheetHr[j] != null && timeSheetHr[j].equals(""))
                                    || natureRule.get(timeSheet[j]) == null) {
                                continue;
                            }

                            attributeNo = natureRule.get(timeSheet[j]).getAttNum();
                            factor = natureRule.get(timeSheet[j]).getFactor();

                            value = Double.parseDouble(timeSheetHr[j].toString());
                            value *= factor.doubleValue();

                            Object x = BaseEntity.getValueFromEntity(consolidation, "attribute" + attributeNo);

                            System.out.println("attribute" + attributeNo);

                            if (x == null) {
                                value += 0;
                            } else {
                                value += (Double.parseDouble(x.toString()));
                            }

                            BaseEntity.setValueInEntity(consolidation,
                                    "attribute" + attributeNo, new BigDecimal(value));
                            oem.saveEntity(consolidation, loggedUser);
                        }
                    }
                }
                entitySetupService.callEntityUpdateAction(consolidation, loggedUser);
            }
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult importTimeSheet(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        String loggingStr = "*** Import Function is called Successfully ***";
        String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        path += "myFile.txt";
        PrintWriter printWriter = null;

        OLog.logInformation("****************************" + path + "****************************");

        try {
            printWriter = new PrintWriter(path, "UTF-8");

            printWriter.write(loggingStr);

            Date dailyDate = null,
                    hiringDate = null;
            String dayName = null,
                    dayNature = null,
                    dayNumHrs = null,
                    oldDayValue = null,
                    index = null,
                    yearName = null;
            String updateTimeSheet = "",
                    selectTimeSheet = "";
            Employee employee = null;
            Vacation vacation = null;
            TMCalendar tmCalendar = null;
            NormalWorkingCalendar calendar = null;

            UDC dayOff1 = null, dayOff2 = null;

            boolean updateSucceded = false;
            List<String> vacationShortDesc = new ArrayList<String>(),
                    conds = new ArrayList<String>();
            List<Date> existingDates = new ArrayList<Date>();
            Map<Employee, Map<String, List<Date>>> empTimeSheetVacs
                    = new HashMap<Employee, Map<String, List<Date>>>();
            Map<String, List<Date>> vacTrackings = new HashMap<String, List<Date>>();
            Map<String, Vacation> vacationsMap;
            List<TimeSheetImport> imports = new ArrayList<TimeSheetImport>();
            Object[] objs;

            conds.clear();
            conds.add("posted = false");
            imports = oem.loadEntityList("TimeSheetImport", conds, null, null, loggedUser);

            if (imports != null) {
                printWriter.write("List size is: " + imports.size());
            } else {
                printWriter.write("List is null!!!");
            }

            List list = oem.executeEntityListNativeQuery("select shortdescription from dayoff where dayofftype = 'VACATION' and deleted = 0 and shortdescription is not null", loggedUser);
            vacationShortDesc.addAll((List<String>) list);

            vacationsMap = new HashMap<String, Vacation>();
            for (int i = 0; i < list.size(); i++) {
                conds.clear();
                conds.add("shortDescription = '" + list.get(i) + "'");
                vacationsMap.put(list.get(i).toString(), (Vacation) oem.loadEntity(Vacation.class.getSimpleName(), conds, null, loggedUser));
            }

            for (int i = 0; i < imports.size(); i++) {

                index = imports.get(i).getRecIndex();
                dailyDate = imports.get(i).getDailyDate();
                dayNature = imports.get(i).getDayNature();
                dayNumHrs = imports.get(i).getNumHrs();
                employee = imports.get(i).getEmployee();

                tmCalendar = employee.getCalendar();
                calendar = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), Collections.singletonList("calendar.dbid = " + tmCalendar.getDbid()), null, loggedUser);

                dayOff1 = calendar.getDayOff1();
                dayOff2 = calendar.getDayOff2();

                dayName = getDayName(dailyDate);
                yearName = dtUtility.YEAR_FORMAT.format(dailyDate);

                // *** Validations ***
                // 1 - validate the inserted date is not before the hiringdate
                hiringDate = imports.get(i).getEmployee().getHiringDate();
                if (dailyDate.compareTo(hiringDate) < 0) {
                    continue;
                }
                // 2- validate that the inserted nature is not O : OffDay as it is not allowed in our scenario;
                // the offdays are stored on the calendar
                if (dayNature.equals("O")) {
                    ofr.addError(usrMsgService.getUserMessage("TMSheet_OffdayInserted", Collections.singletonList(index), loggedUser));
                    continue;
                }
                if (dayNature.equals("H")) {
                    ofr.addError(usrMsgService.getUserMessage("TMSheet_HolidayInserted", Collections.singletonList(index), loggedUser));
                    continue;
                }
                // 3- validate on the setups of removing holiday & offday
                selectTimeSheet = "select " + dayName + " from employeetimesheet "
                        + " where employee_dbid = " + employee.getDbid() + " and deleted = 0"
                        + " and plannerYear = '" + yearName + "' and recordNum = 1";
                if (oem.executeEntityNativeQuery(selectTimeSheet, loggedUser) != null) {
                    oldDayValue = oem.executeEntityNativeQuery(selectTimeSheet, loggedUser).toString();
                }
                vacation = vacationsMap.get(dayNature);
                if (vacation != null) {
                    objs = ((Object[]) getDayNatureOnVacation(dailyDate, oldDayValue,
                            vacation, dayOff1, dayOff2, loggedUser));
                    if (objs != null && objs.length > 0 && objs[0] != null) {
                        dayNature = objs[0].toString();
                    }
                }

                // update the 1st record; holdaing the day nature for each day
                updateTimeSheet = "update employeetimesheet set " + dayName + " = '" + dayNature + "'"
                        + " where employee_dbid = " + employee.getDbid() + " and deleted = 0"
                        + " and plannerYear = '" + yearName + "' and recordNum = 1"; // to be updated for new DBIDGeneration
                try {
                    updateSucceded = oem.executeEntityUpdateNativeQuery(updateTimeSheet, loggedUser);
                    if (!updateSucceded) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_ImportFailure", Collections.singletonList(index), loggedUser));
                        continue;
                    }

                } catch (Exception e) {
                    ofr.addError(usrMsgService.getUserMessage("TMSheet_ImportFailure", loggedUser));
                    OLog.logException(e, loggedUser);
                    continue;
                }

                // update the 2nd record; holdaing the numHrs for each day
                updateTimeSheet = "update employeetimesheet set " + dayName + " = '" + dayNumHrs + "'"
                        + " where employee_dbid = " + employee.getDbid() + " and deleted = 0"
                        + " and plannerYear = '" + yearName + "' and recordNum = 2"; // to be updated for new DBIDGeneration
                try {
                    updateSucceded = oem.executeEntityUpdateNativeQuery(updateTimeSheet, loggedUser);
                    if (!updateSucceded) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_ImportFailure", Collections.singletonList(index), loggedUser));
                        continue;
                    }

                } catch (Exception e) {
                    ofr.addError(usrMsgService.getUserMessage("TMSheet_ImportFailure", loggedUser));
                    OLog.logException(e, loggedUser);
                    continue;
                }

                // check whether it is a vacation & fill the vacations map
                if (vacationShortDesc.contains(dayNature)) {
                    vacTrackings = empTimeSheetVacs.get(employee);
                    if (vacTrackings == null) {
                        vacTrackings = new HashMap<String, List<Date>>();
                    }

                    existingDates = vacTrackings.get(dayNature);
                    if (existingDates == null) {
                        existingDates = new ArrayList<Date>();
                    }
                    existingDates.add(dailyDate);
                    vacTrackings.put(dayNature, existingDates);
                    empTimeSheetVacs.put(employee, vacTrackings);
                }

                // update import record so as to be posted = Y
                imports.get(i).setPosted(true);
                oem.saveEntity(imports.get(i), loggedUser);
            }

            // insert the vacation requests
            for (Employee emp : empTimeSheetVacs.keySet()) {
                vacTrackings = empTimeSheetVacs.get(emp);
                insertVacRequests(emp, vacTrackings, vacationsMap, loggedUser);
            }

        } catch (Exception e) {
            loggingStr = "*** Import Function : inside the catch ***"
                    + e.getMessage();
            printWriter.println(loggingStr);
            OLog.logException(e, loggedUser);
        } finally {
            printWriter.close();
        }
        return ofr;
    }

    private Object[] getDayNatureOnVacation(Date currentDate, String dayVal, Vacation vacation,
            UDC dayOff1, UDC dayOff2, OUser loggedUser) {
        Object[] arglist = new Object[1];
        Object holiday = null;
        String dayName = dtUtility.DAY_NAME_FORMAT.format(currentDate);
        try {
            holiday = oem.executeEntityNativeQuery("select dbid from holidaycalendar where datefrom <= '"
                    + dtUtility.DATE_FORMAT.format(currentDate) + "' and dateto >= '" + dtUtility.DATE_FORMAT.format(currentDate) + "'"
                    + " and deleted = 0 ", loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (vacation.getRemoveHoliday().equals("Y") && holiday != null) {
            arglist = new Object[1];
            if (vacation.getUnitOfMeasure().equals("H")) {
                arglist[0] = "A";
            } else {
                arglist[0] = "H";
            }
        } else if ((vacation.getRemoveHoliday().equals("N") && dayVal.equals("H"))
                || (vacation.getRemoveHoliday().equals("Y") && !dayVal.equals("H") && !dayVal.equals("O"))) {
            arglist = new Object[1];
            if (vacation.getUnitOfMeasure().equals("H")) {
                arglist[0] = "A";
            } else {
                arglist[0] = vacation.getShortDescription();
            }
        } else if ((vacation.getPayWorkingCalender().equals("Y") && dayOff1 != null && dayName.equals(dayOff1.getValue()))
                || (vacation.getPayWorkingCalender().equals("Y") && dayOff2 != null && dayName.equals(dayOff2.getValue()))) {
            arglist = new Object[1];
            if (vacation.getUnitOfMeasure().equals("H")) {
                arglist[0] = "A";
            } else {
                arglist[0] = "O";
            }
        } else if ((vacation.getPayWorkingCalender().equals("N") && dayVal.equals("O"))
                || (vacation.getPayWorkingCalender().equals("Y") && !dayVal.equals("O"))) {
            arglist = new Object[1];
            if (vacation.getUnitOfMeasure().equals("H")) {
                arglist[0] = "A";
            } else {
                arglist[0] = vacation.getShortDescription();
            }
        }
        return arglist;
    }

    private String getDayName(Date dailyDate) {
        String dayName = null;

        int day = dailyDate.getDate(),
                month = (dailyDate.getMonth() + 1);

        dayName = "m" + month + "D" + day;

        return dayName;
    }

    class VacationPeriod {

        Date startDate;
        Date endDate;

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }
    }

    private List<VacationPeriod> collect(List<Date> dates) {
        List<VacationPeriod> periods = new ArrayList<VacationPeriod>();

        Date startDate = null,
                endDate = null;
        int datesDiff = 0,
                counter = 0;
        VacationPeriod tempPeriod = null;
        Calendar tempCal = Calendar.getInstance();

        for (int i = 0; i < dates.size(); i += counter + 1) {
            counter = 0;
            startDate = dates.get(i);
            endDate = dates.get(i);
            tempCal.setTime(endDate);

            for (int j = i + 1; j < dates.size(); j++) {
                datesDiff = dtUtility.getDaysBetween(dates.get(j), dates.get(j - 1)) + 1;
                if (datesDiff > 1) {
                    break;
                }
                tempCal.add(Calendar.DAY_OF_MONTH, 1);
                counter++;
            }
            endDate = tempCal.getTime();

            tempPeriod = new VacationPeriod();
            tempPeriod.setEndDate(endDate);
            tempPeriod.setStartDate(startDate);
            periods.add(tempPeriod);
        }

        return periods;
    }

    private void insertVacRequests(Employee employee, Map<String, List<Date>> empVacs, Map<String, Vacation> vacations,
            OUser loggedUser) {
        List<Date> dates = new ArrayList<Date>();
        List<VacationPeriod> periods = new ArrayList<VacationPeriod>();
        Date startDate, endDate;
        Vacation vacation = null;

        for (String vacName : empVacs.keySet()) {

            vacation = vacations.get(vacName);

            dates = empVacs.get(vacName);

            // sort the dates per vacation key
            Collections.sort(dates);

            // collect continous vacations
            periods = collect(dates);

            for (int i = 0; i < periods.size(); i++) {
                startDate = periods.get(i).getStartDate();
                endDate = periods.get(i).getEndDate();

                EmployeeVacationRequest empReq = new EmployeeVacationRequest();
                empReq.setEmployee(employee);
                empReq.setStartDate(startDate);
                empReq.setEndDate(endDate);
                empReq.setVacation(vacation);
                empReq.setCancelled("N");

                try {
                    // CALL VACATION BALANCE OPERATION
                    List vacBdata = new ArrayList();
                    DateFormat dateFormat;
                    if (vacation.getUnitOfMeasure() != null && vacation.getUnitOfMeasure().equalsIgnoreCase("h")) {
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    } else {
                        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    }
                    vacBdata.add(employee.getDbid());
                    vacBdata.add(vacation.getDbid());
                    vacBdata.add(dateFormat.format(startDate));
                    vacBdata.add(dateFormat.format(endDate));

                    String webServiceCode = "CVB_01";
                    String result = webServiceUtility.callingWebService(webServiceCode, vacBdata, loggedUser);

                    //In Case Of Modify The Period Should Be Added To Balance
                    if (empReq.getDbid() == 0) {
                        empReq.setBalance(new BigDecimal(result));
                    } else {
                        BigDecimal newBalance, oldBalance;
                        EmployeeVacationRequest oldRequest = (EmployeeVacationRequest) oem.loadEntity(
                                EmployeeVacationRequest.class.getSimpleName(), empReq.getDbid(), null, null, loggedUser);

                        if (!(oldRequest.getCancelled().equals("Y"))) {
                            oldBalance = oldRequest.getPeriod();
                            if (oldBalance == null) {
                                oldBalance = new BigDecimal(0);
                            }
                            newBalance = oldBalance.add(new BigDecimal(result));
                            empReq.setBalance(newBalance);
                        } else {
                            empReq.setBalance(new BigDecimal(result));
                        }
                    }
                    // CALL VACATION POSTED OPERATION
                    List vacPOdata = new ArrayList();
                    vacPOdata.add(employee.getDbid());
                    vacPOdata.add(vacation.getDbid());
                    vacPOdata.add(dateFormat.format(startDate));
                    vacPOdata.add(dateFormat.format(endDate));
                    webServiceCode = "Vposted_01";
                    result = webServiceUtility.callingWebService(webServiceCode, vacPOdata, loggedUser);
                    empReq.setPostedDays(new BigDecimal(result));
                    // CALL VACATION PERIOD OPERATION
                    List vacPEdata = new ArrayList();
                    vacPEdata.add(employee.getDbid());
                    vacPEdata.add(vacation.getDbid());
                    vacPEdata.add(dateFormat.format(startDate));
                    vacPEdata.add(dateFormat.format(endDate));
                    vacPEdata.add(vacation.getUnitOfMeasure() != null ? vacation.getUnitOfMeasure() : "");

                    webServiceCode = "Vperiod_01";
                    result = webServiceUtility.callingWebService(webServiceCode, vacPEdata, loggedUser);
                    //___________________________________________
                    BigDecimal vacPeriod;
                    vacPeriod = new BigDecimal(result);
                    empReq.setPeriod(vacPeriod);
                    empReq.setActualPeriodMask(empReq.getPeriod());

                    entitySetupService.callEntityCreateAction(empReq, loggedUser);
                } catch (Exception e) {
                    OLog.logException(e, loggedUser);
                }
            }
        }
    }

    private List<Date> getCutOffs(CalculatedPeriod calculatedPeriod, OUser loggedUser) {
        List<Date> startEndDates = new ArrayList<Date>();

        int cutOffStart = 0, cutOffEnd = 0;
        String calcFullMonth = null,
                calcCurrentMonth = null;
        List<String> cutOffConditions = new ArrayList<String>();
        PayrollParameter cutOffParameter = null,
                calcCurrentMonthParam = null,
                calcFullMonthParam = null;;

        String calcYear = String.valueOf(calculatedPeriod.getYear());
        String calcMonth = String.valueOf(calculatedPeriod.getMonth());
        if (calcMonth.length() == 1) {
            calcMonth = "0".concat(calcMonth);
        }

        cutOffConditions.clear();
        cutOffConditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
        cutOffConditions.add("description = 'day_of_calculation'");
        try {
            cutOffParameter = (PayrollParameter) oem.loadEntity(
                    PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }

        //calcCurrentMonth
        cutOffConditions.clear();
        cutOffConditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
        cutOffConditions.add("description = 'calculation_currentMonth'");
        try {
            calcCurrentMonthParam = (PayrollParameter) oem.loadEntity(
                    PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (calcCurrentMonthParam != null) {
            calcCurrentMonth = calcCurrentMonthParam.getValueData();
        }

        //calcFullMonth
        cutOffConditions.clear();
        cutOffConditions.add("company.dbid = " + loggedUser.getUserPrefrence1());
        cutOffConditions.add("description = 'calculation_fullMonth'");
        try {
            calcFullMonthParam = (PayrollParameter) oem.loadEntity(
                    PayrollParameter.class.getSimpleName(), cutOffConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(TimeSheetService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (calcFullMonthParam != null) {
            calcFullMonth = calcFullMonthParam.getValueData();
        }

        Integer perviousMonth = calculatedPeriod.getMonth();
        Integer perviousYear = calculatedPeriod.getYear();

        if (calcFullMonth != null && calcFullMonth.equals("Y")) { // from 1->30
            cutOffStart = 1;
            cutOffEnd = dtUtility.getLastDayInMonth(calculatedPeriod.getYear(), calculatedPeriod.getMonth());
            if ((calcCurrentMonth != null && calcCurrentMonth.equals("N")) || // the prev month
                    (calcFullMonth.equals("N"))) {
                perviousMonth = calculatedPeriod.getMonth() - 1;
                if (calculatedPeriod.getMonth() == 1) {
                    perviousMonth = 12;
                    perviousYear = calculatedPeriod.getYear() - 1;
                }
            }
        } else if (calcFullMonth != null && calcFullMonth.equals("N")) {
            cutOffStart = Integer.parseInt(cutOffParameter.getValueData()) + 1;
            cutOffEnd = Integer.parseInt(cutOffParameter.getAuxiliary()) + 1;

            perviousMonth = calculatedPeriod.getMonth() - 1;
            if (calculatedPeriod.getMonth() == 1) {
                perviousMonth = 12;
                perviousYear = calculatedPeriod.getYear() - 1;
            }
        }

        String endDateStr = calcYear + "-" + calcMonth + "-" + cutOffEnd;
        String startDateStr = perviousYear.toString() + "-" + perviousMonth.toString() + "-" + cutOffStart;

        Date date = null;
        try {
            date = dtUtility.DATE_FORMAT.parse(startDateStr);
            startEndDates.add(date);
            date = dtUtility.DATE_FORMAT.parse(endDateStr);
            startEndDates.add(date);
        } catch (Exception exception) {
            OLog.logException(exception, loggedUser);
        }
        return startEndDates;
    }

    private OFunctionResult dayOffRequest(Employee emp, DayOff dayOff, boolean isAbsence, Date absenceDate, OUser loggedUser) {
        OFunctionResult result = new OFunctionResult();
        try {
            if (isAbsence) {
                EmployeeAbsenceRequest absenceRequest = new EmployeeAbsenceRequest();
                absenceRequest.setEmployee(emp);
                absenceRequest.setAbsence((Absence) dayOff);
                absenceRequest.setStartDate(absenceDate);
                absenceRequest.setEndDate(absenceDate);
                absenceRequest.setRequestedDate(absenceDate);
                absenceRequest.setBalance(new BigDecimal(1));
                absenceRequest.setPeriod(new BigDecimal(1));
                absenceRequest.setPosted("N");
                absenceRequest.setCancelled("N");
                absenceRequest.setPostedDays(new BigDecimal(0));
                result = entitySetupService.callEntityCreateAction(absenceRequest, loggedUser);
                return result;
            }
        } catch (Exception ex) {
            OLog.logError(ex.toString(), loggedUser);
        }
        return result;
    }

    public OFunctionResult penaltyRequest(Employee employee, Penalty penalty, BigDecimal value, OUser loggedUser) {
        OFunctionResult result = new OFunctionResult();
        try {
            EmployeePenaltyRequest penaltyRequest = new EmployeePenaltyRequest();
            penaltyRequest.setEmployee(employee);
            penaltyRequest.setPenalty(penalty);
            penaltyRequest.setPenaltyDate(new Date());
            penaltyRequest.setPenaltyValue(value);
            penaltyRequest.setGivenBy(employee);
            penaltyRequest.setCancelled("N");
            result = entitySetupService.callEntityCreateAction(penaltyRequest, loggedUser);
            return result;
        } catch (Exception ex) {
            OLog.logError(ex.toString(), loggedUser);
        }
        return result;
    }

    private OFunctionResult getProjectNatureRules(OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        projNatureRule = null;
        Map<String, TMSheetRule> natureRule = null;
        TMSheetRule tmSheetRule = null;
        List projDbids,
                natures,
                rules;
        Object[] nature = null;
        String select, natureSymbol, attributeNo, factor;
        long projDbid, natureDbid;

        try {
            projNatureRule = new Hashtable<Long, Map<String, TMSheetRule>>();
            select = "select distinct project_dbid from employeeproject where deleted = 0";
            projDbids = oem.executeEntityListNativeQuery(select, loggedUser);
            if (projDbids == null || (projDbids != null && projDbids.isEmpty())) {
                return ofr;
            }
            for (int i = 0; i < projDbids.size(); i++) {
                projDbid = Long.parseLong(projDbids.get(i).toString());
                select = "select dbid, symbol from projectnature where project_dbid = " + projDbids.get(i)
                        + " and deleted = 0";
                natures = oem.executeEntityListNativeQuery(select, loggedUser);
                if (natures == null || (natures != null && natures.isEmpty())) {
                    ofr.addError(usrMsgService.getUserMessage("TMSheet_NaturesMissing", loggedUser));
                    return ofr;
                }
                natureRule = new Hashtable<String, TMSheetRule>();
                for (int j = 0; j < natures.size(); j++) {
                    nature = (Object[]) natures.get(j);

                    natureDbid = Long.parseLong(nature[0].toString());
                    natureSymbol = nature[1].toString();

                    select = "select attributeNo, factor from timesheetrule where projectNature_dbid = " + natureDbid
                            + " and deleted = 0";
                    rules = oem.executeEntityListNativeQuery(select, loggedUser);
                    if (rules == null || (rules != null && rules.isEmpty())) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_RulesMissing", loggedUser));
                        return ofr;
                    }
                    for (int k = 0; k < rules.size(); k++) {
                        Object[] tempArr = (Object[]) rules.get(0);
                        attributeNo = tempArr[0].toString();
                        factor = tempArr[1].toString();

                        tmSheetRule = new TMSheetRule();
                        tmSheetRule.setAttNum(attributeNo);
                        tmSheetRule.setFactor(new BigDecimal(factor));
                        natureRule.put(natureSymbol, tmSheetRule);
                        projNatureRule.put(projDbid, natureRule);
                    }
                }
            }
        } catch (Exception e) {
        }
        return ofr;
    }

    public Map<String, Date> generateTMSheetConsFields(Date startDate, Date endDate) {
        Map<String, Date> days = new HashMap<String, Date>();

        Date tempDate = startDate;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tempDate);
        int day, month;

        while (tempDate.compareTo(endDate) <= 0) {

            day = tempDate.getDate();
            month = tempDate.getMonth() + 1;

            days.put("m" + month + "D" + day, tempDate);

            calendar.add(Calendar.DAY_OF_MONTH, 1);
            tempDate = calendar.getTime();
        }
        return days;
    }

    /**
     *
     * @param project the project on which the employee is assigned
     * @param tmEntitlment whether the employee is entitled for time management
     * or not, such that the default nature is W
     * @return
     */
    private String getTimeSheetNature(Project project, String tmEntitlment) {
        String nature = null;
        if (project != null && project.getDefaultNature() != null && project.getDefaultNature().getDbid() != 0) {
            nature = project.getDefaultNature().getSymbol();
        } else if (tmEntitlment != null && tmEntitlment.equals("N")) { // Loubna - TM Porta - 06/02/2014
            nature = "W";
        } else {
            nature = "A";
        }
        return nature;
    }

    private OFunctionResult createSingleEmpTimeSheet(EmployeeTimeSheet empTimeSheet, Date startDate,
            Date endDate, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        StringBuilder insertTimeSheetDet = null;
        String dayName,
                dayValue = "";
        Object valueFromEntity;
        int month, day;
        Date tempDate = startDate;
        Calendar calendar = Calendar.getInstance();
        int recordNum = empTimeSheet.getRecordNum();
        if (recordNum == 1) {
            insertTimeSheetDet = new StringBuilder("insert into EmployeeTimeSheetDetail "
                    + "(employee_dbid, dailydate, value1)");
        }
        calendar.setTime(tempDate);
        try {
            while (tempDate.compareTo(endDate) <= 0) {
                day = tempDate.getDate();
                month = tempDate.getMonth() + 1;
                dayName = "m" + month + "D" + day;
                valueFromEntity = BaseEntity.getValueFromEntity(empTimeSheet, dayName);
                if (valueFromEntity != null) {
                    dayValue = valueFromEntity.toString();
                }
                if (recordNum == 1) {
                    insertTimeSheetDet = insertTimeSheetDet.append("select ").
                            append(empTimeSheet.getEmployee().getDbid()).
                            append(",'").append(dtUtility.DATE_FORMAT.format(tempDate)).append("','").
                            append(dayValue).append("'");
                    if (!tempDate.equals(endDate)) {
                        insertTimeSheetDet = insertTimeSheetDet.append(" union ");
                    }
                } else {
                    insertTimeSheetDet = new StringBuilder("");
                    insertTimeSheetDet = insertTimeSheetDet.append("update EmployeeTimeSheetDetail SET value2 ='").
                            append(dayValue).append("' where employee_dbid = ").
                            append(empTimeSheet.getEmployee().getDbid()).append("  ").
                            append(" AND dailyDate = ").append("'").append(dtUtility.DATE_FORMAT.format(tempDate)).append("'");
                    oem.executeEntityUpdateNativeQuery(insertTimeSheetDet.toString(), loggedUser);
                }
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                tempDate = calendar.getTime();
            }
            if (recordNum == 1) {
                oem.executeEntityUpdateNativeQuery(insertTimeSheetDet.toString(), loggedUser);
            }
        } catch (Exception exception) {
            ofr.addError(usrMsgService.getUserMessage(exception.getMessage(), loggedUser));
            OLog.logException(exception, null);
            OLog.logInformation(empTimeSheet.getEmployee().getCode());
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult importMonthlyTimeSheet(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        String loggingStr = "*** Import Function is called Successfully ***";
        String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        path += "myFile.txt";
        PrintWriter printWriter = null;

        OLog.logInformation("****************************" + path + "****************************");

        try {
            printWriter = new PrintWriter(path, "UTF-8");

            printWriter.write(loggingStr);

            List<String> vacationShortDesc = new ArrayList<String>(),
                    conds = new ArrayList<String>();
            Map<String, Vacation> vacationsMap;
            List<MonthlyTimeSheetImport> imports = new ArrayList<MonthlyTimeSheetImport>();

            conds.clear();
            conds.add("posted = false");
            imports = oem.loadEntityList("MonthlyTimeSheetImport", conds, null, null, loggedUser);

            if (imports != null) {
                printWriter.write("List size is: " + imports.size());
            } else {
                printWriter.write("List is null!!!");
            }

            List list = oem.executeEntityListNativeQuery("select shortdescription from dayoff where dayofftype = 'VACATION' and deleted = 0 and shortdescription is not null", loggedUser);
            vacationShortDesc.addAll((List<String>) list);

            vacationsMap = new HashMap<String, Vacation>();
            for (int i = 0; i < list.size(); i++) {
                conds.clear();
                conds.add("shortDescription = '" + list.get(i) + "'");
                vacationsMap.put(list.get(i).toString(), (Vacation) oem.loadEntity(Vacation.class.getSimpleName(), conds, null, loggedUser));
            }

            String selectCutOffs = "select valuedata from payrollparameter where description = 'day_of_calculation' "
                    + " and deleted = 0 and company_dbid = " + loggedUser.getUserPrefrence1();
            String cutOffStart = (oem.executeEntityNativeQuery(selectCutOffs, loggedUser)).toString();
            Integer cutOffStartVal = Integer.parseInt(cutOffStart) + 1;
            cutOffStart = cutOffStartVal.toString();

            OFunctionResult singleEmpOfr;
            String updateTMSheetImport = "update monthlytimesheetimport set posted = 1 where dbid = ";
            for (int i = 0; i < imports.size(); i++) {
                singleEmpOfr = importMonthlyTMSheetSingEmp(imports.get(i), loggedUser, vacationsMap, vacationShortDesc, cutOffStart);
                if (singleEmpOfr.getErrors() == null || (singleEmpOfr.getErrors() != null && singleEmpOfr.getErrors().isEmpty())) {
                    oem.executeEntityUpdateNativeQuery(updateTMSheetImport + imports.get(i).getDbid(), loggedUser);
                }
                ofr.append(singleEmpOfr);
            }
        } catch (Exception e) {
            loggingStr = "*** Import Function : inside the catch ***"
                    + e.getMessage();
            printWriter.println(loggingStr);
            OLog.logException(e, loggedUser);
        } finally {
            printWriter.close();
        }
        return ofr;
    }

    private OFunctionResult importMonthlyTMSheetSingEmp(MonthlyTimeSheetImport timeSheetImport, OUser loggedUser,
            Map<String, Vacation> vacationsMap, List<String> vacationShortDesc, String cutOffStart) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            String numbersPattern = "^[0-9]*$",
                    lettersPattern = "^[A-z]+$";

            String month = timeSheetImport.getMonth();
            String year = timeSheetImport.getYear();

            int monthVal = Integer.parseInt(month);

            int recNum = timeSheetImport.getRecNum();

            Date currentDate = dtUtility.DATE_FORMAT.parse(year + "-" + (monthVal - 1) + "-" + cutOffStart);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentDate);

            String dayVal, dayName;

            String selectTimeSheet;

            Map<String, List<Date>> vacTrackings = new HashMap<String, List<Date>>();

            Employee employee = timeSheetImport.getEmployee();
            TMCalendar tmCalendar = employee.getCalendar();
            NormalWorkingCalendar workingCal = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), Collections.singletonList("calendar.dbid = " + tmCalendar.getDbid()), null, loggedUser);

            UDC dayOff1 = workingCal.getDayOff1();
            UDC dayOff2 = workingCal.getDayOff2();

            List<Date> existingDates;

            String updateTimeSheetDet = "";
            for (int i = 1; i <= 31; i++) {

                dayVal = BaseEntity.getValueFromEntity(timeSheetImport, "d" + i).toString();

                dayName = "m" + (currentDate.getMonth() + 1) + "D" + (currentDate.getDate());

                if (recNum == 1) {
                    // *** Validations ***

                    // 2- validate that the inserted nature is not O : OffDay as it is not allowed in our scenario;
                    // the offdays are stored on the calendar
                    if (dayVal.equals("O")) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_OffdayInserted", loggedUser));
                        continue;
                    }
                    if (dayVal.equals("H")) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_HolidayInserted", loggedUser));
                        continue;
                    }
                    // 3- validate on the setups of removing holiday & offday
                    selectTimeSheet = "select " + dayName + " from employeetimesheet "
                            + " where employee_dbid = " + employee.getDbid() + " and deleted = 0"
                            + " and plannerYear = '" + year + "' and recordNum = 1";
                    String oldDayValue = null;
                    if (oem.executeEntityNativeQuery(selectTimeSheet, loggedUser) != null) {
                        oldDayValue = oem.executeEntityNativeQuery(selectTimeSheet, loggedUser).toString();
                    }
                    Vacation vacation = vacationsMap.get(dayVal);
                    if (vacation != null) {
                        Object[] objs = ((Object[]) getDayNatureOnVacation(currentDate, oldDayValue,
                                vacation, dayOff1, dayOff2, loggedUser));
                        if (objs != null && objs.length > 0 && objs[0] != null) {
                            dayVal = objs[0].toString();
                        }
                    }
                }

                if (recNum == 1 && !dayVal.matches(lettersPattern)) {
                    ofr.addError(usrMsgService.getUserMessage("TMSheetInvalidValue001", loggedUser));
                    break;
                } else if (recNum == 2 && !dayVal.matches(numbersPattern)) {
                    dayVal = "";
                    ofr.addError(usrMsgService.getUserMessage("TMSheetInvalidValue002", loggedUser));
                    break;
                }
                // update the 1st record; holdaing the day nature for each day
                String updateTimeSheet = "update employeetimesheet set " + dayName + " = '" + dayVal + "'"
                        + " where employee_dbid = " + employee.getDbid() + " and deleted = 0"
                        + " and plannerYear = '" + year + "' and recordNum = " + recNum;
                try {
                    boolean updateSucceded = oem.executeEntityUpdateNativeQuery(updateTimeSheet, loggedUser);
                    if (!updateSucceded) {
                        ofr.addError(usrMsgService.getUserMessage("TMSheet_ImportFailure", loggedUser));
                        continue;
                    }

                    // update time sheet detail
                    if (recNum == 1) {
                        updateTimeSheetDet = "update employeetimesheetdetail set value1 = '" + dayVal
                                + "' where employee_dbid = " + employee.getDbid()
                                + " and dailydate = '" + dtUtility.DATE_FORMAT.format(currentDate) + "'";
                    } else if (recNum == 2) {
                        updateTimeSheetDet = "update employeetimesheetdetail set value2 = '" + dayVal
                                + "' where employee_dbid = " + employee.getDbid()
                                + " and dailydate = '" + dtUtility.DATE_FORMAT.format(currentDate) + "'";
                    }
                    updateSucceded = oem.executeEntityUpdateNativeQuery(updateTimeSheetDet, loggedUser);

                } catch (Exception e) {
                    ofr.addError(usrMsgService.getUserMessage("TMSheet_ImportFailure", loggedUser));
                    OLog.logException(e, loggedUser);
                }
                // check whether it is a vacation & fill the vacations map
                if (vacationShortDesc.contains(dayVal)) {
                    if (vacTrackings == null) {
                        vacTrackings = new HashMap<String, List<Date>>();
                    }

                    existingDates = vacTrackings.get(dayVal);
                    if (existingDates == null) {
                        existingDates = new ArrayList<Date>();
                    }
                    existingDates.add(currentDate);
                    vacTrackings.put(dayVal, existingDates);
                }

                calendar.add(Calendar.DAY_OF_MONTH, 1);
                currentDate = calendar.getTime();
            }
            insertVacRequests(employee, vacTrackings, vacationsMap, loggedUser);

        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }
}
