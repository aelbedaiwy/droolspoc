package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;

/**
 *
 * @author arezk
 */
public interface TimeManagementServiceLocal {

    public OFunctionResult monthlyConsolidationPost(ODataMessage odm, OFunctionParms parms, OUser oUser);

    public OFunctionResult monthlyConsolidationSinglePost(ODataMessage odm, OFunctionParms parms, OUser oUser);

    public OFunctionResult applySummar(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult applyWinter(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult applySummarhrrmv(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult applyOTRequestForSingleEmployee(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult applyOTRequestForAll(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult applyAddingOTRequest(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult applyUpdatingOTRequest(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public OFunctionResult applyDeletingOTRequest(ODataMessage odm, OFunctionParms parms, OUser loggedUser);

    public long getCalculatedPeriodOfThisDay(int day, int month, int year, String paymentMethodDbid, String paymentPeriodDbid, OUser loggedUser);

    public String getDefaultPaymentMethod(OUser loggedUser);

    public String getDefaultPaymentPeriod(OUser loggedUser);

    public Long checkConsolidationExisting(String employeeDbid, String calculatedPeriodDbid, OUser loggedUser);

    public Long addingMonthlyConsolidation(String employeeDbid, String calculatedPeriodDbid, OUser loggedUser);

    public Long addingMonthlyConsolidationIfNotExist(String employeeDbid, String calculatedPeriodDbid, OUser loggedUser);

    public String[] getCutOffDatesFromCalculatedPeriod(Integer currentMonth, Integer currentYear, OUser loggedUser);

    public String[] getCutOffDatesFromCalculatedPeriod(Integer currentMonth, Integer currentYear, Integer currentDay, OUser loggedUser);
}
