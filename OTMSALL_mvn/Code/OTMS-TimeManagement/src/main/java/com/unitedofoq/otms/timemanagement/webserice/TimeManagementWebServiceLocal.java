/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.otms.timemanagement.webserice;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.security.user.OUser;
import javax.ejb.Local;

/**
 *
 * @author dev
 */
@Local
public interface TimeManagementWebServiceLocal {

    public OFunctionResult getDataFromMachine(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);

    public OFunctionResult fetchManual(ODataMessage odm,
            OFunctionParms params, OUser loggedUser);
}
