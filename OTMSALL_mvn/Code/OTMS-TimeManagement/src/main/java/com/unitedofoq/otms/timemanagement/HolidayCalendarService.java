package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.security.user.UserNotAuthorizedException;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.fabs.core.validation.FABSException;
import com.unitedofoq.fabs.core.validation.OEntityDataValidationException;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.payroll.PayrollParameter;
import com.unitedofoq.otms.personnel.holiday.EmployeeHoliday;
import com.unitedofoq.otms.personnel.holiday.Holiday;
import com.unitedofoq.otms.personnel.holiday.HolidayAssign;
import com.unitedofoq.otms.personnel.holiday.HolidayDates;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.timemanagement.employee.EmployeeAnnualPlanner;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendance;
import com.unitedofoq.otms.timemanagement.employee.EmployeeWorkingCalHistory;
import com.unitedofoq.otms.timemanagement.workingcalendar.ChristianCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.HolidayCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.ejb.EntityUtilities;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.timemanagement.HolidayCalendarServiceLocal",
        beanInterface = HolidayCalendarServiceLocal.class)
public class HolidayCalendarService implements HolidayCalendarServiceLocal {

    @EJB
    OEntityManagerRemote oem;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private HolidayCalendarServiceLocal holidayCalendarService;
    @EJB
    private CalendarServiceLocal calendarService;
    DateTimeUtility dtUtility = new DateTimeUtility();
    @EJB
    DataSecurityServiceLocal dataSecurityService; //getEmployeeSecurityDbids

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult addHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Date from = null, to = null;
            Company company = null;
            boolean isChrisCal = false;
            if (odm.getData().get(0).getClass().getSimpleName().equals("HolidayCalendar")) {
                HolidayCalendar holidayCalendar = (HolidayCalendar) odm.getData().get(0);
                if (holidayCalendar != null) {
                    from = holidayCalendar.getDateFrom();
                    to = holidayCalendar.getDateTo();
                    company = holidayCalendar.getCompany();
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("ChristianCalendar")) {
                ChristianCalendar chrisCal = (ChristianCalendar) odm.getData().get(0);
                if (chrisCal != null) {
                    from = chrisCal.getDateFrom();
                    to = chrisCal.getDateTo();
                    company = chrisCal.getCompany();

                    isChrisCal = true;
                }
            }

            if (from == null || to == null) {
                ofr.addError(usrMsgService.getUserMessage("HolidayMissingDates", loggedUser));
                return ofr;
            }

            String yearStr = dtUtility.YEAR_FORMAT.format(from);
            Integer currentYear = Integer.parseInt(yearStr);

            List<String> udcConditions = new ArrayList<String>();
            UDC holidayNature = null, absenceDayNature = null, vacationDayNaure = null;
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Holiday'");
            try {
                holidayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Absence'");
            try {
                absenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Vacation'");
            try {
                vacationDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            List<String> vacationMethods = calendarService.getEmployeeVacations(from, to, currentYear);
            List<Date> vacationDates = calendarService.getHolidayDates(from, to);

            String chrisStat = "";
            String updatePlannerStat = "";
            String updateShiftStat = "";
            String updateTimeSheetStat = "";
            String updateDAStat = "";
            if (isChrisCal) {
                chrisStat = " and religion_dbid in (select dbid from udc where code = 'CHR' and type_dbid in (select dbid from udc where code = 'prs_019')) ";
            }

            // check if there exists vacation requests intersecting with this holiday
            // if yes then either cancel it or modify it
            int holidayPeriod = 0; // +1 ?
            float vacPeriod = 0;
            long vacReqDbid = 0, vacDbid = 0, empDbid = 0;
            Object[] objArr;
            List vacsDbids = getVacRequestsDbidsOnHoliday(from, to, loggedUser);

            Date vacStartDate, vacEndDate;
            // update periods

            String updateBalance, updatePeriod;
            Employee emp;
            for (int i = 0; i < vacsDbids.size(); i++) {
                objArr = (Object[]) vacsDbids.get(i);
                vacReqDbid = Long.parseLong(objArr[0].toString());
                vacPeriod = Float.parseFloat(objArr[1].toString());
                vacDbid = Long.parseLong(objArr[2].toString());
                empDbid = Long.parseLong(objArr[3].toString());
                vacStartDate = dtUtility.DATE_FORMAT.parse(objArr[4].toString());
                vacEndDate = dtUtility.DATE_FORMAT.parse(objArr[5].toString());

                emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggedUser);
                holidayPeriod = getIntersectionDaysExcludingOffDays(from, to, vacStartDate, vacEndDate, emp, loggedUser);

                vacPeriod -= holidayPeriod;

                updatePeriod = "update employeedayoffrequest "
                        + "set period = " + vacPeriod
                        + " , actualPeriod = " + vacPeriod
                        + " , actualBalance = actualBalance + " + holidayPeriod
                        + " , balance = balance + " + holidayPeriod
                        + " , currentBalance = currentBalance - " + holidayPeriod
                        + "where dbid = " + vacReqDbid;
                oem.executeEntityUpdateNativeQuery(updatePeriod, loggedUser);

                // update all the following vacations' balances
                updateBalance = getUpdateStatForVacsFollowingHoliday(to, loggedUser);

                updateBalance = updateBalance.replaceAll("@holidayPeriod", holidayPeriod + "");
                updateBalance = updateBalance.replaceAll("@empDbid", empDbid + "");
                updateBalance = updateBalance.replaceAll("@vacDbid", vacDbid + "");
                oem.executeEntityUpdateNativeQuery(updateBalance, loggedUser);
            }

            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

            long calculatedPeriodDbid;

            List<String> conds = new ArrayList<String>();
            conds.clear();
            conds.add("description = 'default_pay_method'");
            conds.add("company.dbid = " + company.getDbid());

            PayrollParameter parameter = null;
            try {
                parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            String paymentMethodDbid = "", paymentPeriodDbid = "";
            if (parameter != null) {
                paymentMethodDbid = parameter.getValueData();
            }

            conds.clear();
            conds.add("description = 'default_pay_period'");
            conds.add("company.dbid = " + company.getDbid());
            parameter = null;
            try {
                parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (parameter != null) {
                paymentPeriodDbid = parameter.getValueData();
            }

            String vacationCond;
            for (int j = 0; j < vacationMethods.size(); j++) {
                String[] temp = vacationMethods.get(j).split("D");
                String[] temp2 = temp[0].split("m");
                String month = temp2[1];
                String day = temp[1];

                // in case of absence
                updatePlannerStat = "update employeeannualplanner set " + vacationMethods.get(j) + "='H' "
                        + "where " + vacationMethods.get(j) + " <> 'O' "
                        + " and " + vacationMethods.get(j) + " <> 'W'"
                        + " and " + vacationMethods.get(j) + " <> 'V' and employee_dbid in "
                        + "(select dbid from oemployee where position_dbid in "
                        + "(select dbid from position where unit_dbid in "
                        + "(select dbid from unit "
                        + "where company_dbid = " + company.getDbid() + "))" + chrisStat + ")";
                updateTimeSheetStat = "update employeetimesheet set " + vacationMethods.get(j) + "='H' "
                        + "where " + vacationMethods.get(j) + " <> 'O' "
                        + " and " + vacationMethods.get(j) + " <> 'W'"
                        + " and " + vacationMethods.get(j) + " <> 'V' and employee_dbid in "
                        + "(select dbid from oemployee where position_dbid in "
                        + "(select dbid from position where unit_dbid in "
                        + "(select dbid from unit "
                        + " where company_dbid = " + company.getDbid() + "))" + chrisStat + ")";
                try {
                    oem.executeEntityUpdateNativeQuery(updatePlannerStat, loggedUser);
                    oem.executeEntityUpdateNativeQuery(updateTimeSheetStat, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                // in case of vacation
                updatePlannerStat = "update employeeannualplanner set " + vacationMethods.get(j) + "='H' "
                        + " where " + vacationMethods.get(j) + " = 'V' and employee_dbid in "
                        + "(select dbid from oemployee where position_dbid in "
                        + "(select dbid from position where unit_dbid in "
                        + "(select dbid from unit "
                        + "where company_dbid = " + company.getDbid() + "))" + chrisStat + ""
                        + " and dbid in (select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updatePlannerStat += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "' ))";
                } else {
                    updatePlannerStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                            + " ))";
                }
                updateTimeSheetStat = "update employeetimesheet set " + vacationMethods.get(j) + "='H' "
                        + "where " + vacationMethods.get(j) + " = 'V' "
                        + " and employee_dbid in "
                        + "(select dbid from oemployee where position_dbid in "
                        + "(select dbid from position where unit_dbid in "
                        + "(select dbid from unit "
                        + "where company_dbid = " + company.getDbid() + "))" + chrisStat + ""
                        + " and dbid in (select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateTimeSheetStat += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "' ))";
                } else {
                    updateTimeSheetStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                            + " ))";
                }

                try {
                    oem.executeEntityUpdateNativeQuery(updatePlannerStat, loggedUser);
                    oem.executeEntityUpdateNativeQuery(updateTimeSheetStat, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {

                    // from absence to holiday, so in monthly consolidation: increment holidays & decrement absence
                    updateDAStat = "update employeedailyattendance set daynature_dbid = " + holidayNature.getDbid()
                            + " where daynature_dbid in (select dbid from daynatures where type in('A','O')) "
                            + " and dailydate = '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "'"
                            + " and employee_dbid in "
                            + "(select dbid from oemployee where position_dbid in "
                            + "(select dbid from position where unit_dbid in "
                            + "(select dbid from unit "
                            + "where company_dbid = " + company.getDbid() + ")) " + chrisStat + ")";
                    calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                            paymentMethodDbid, paymentPeriodDbid, loggedUser);
                    updateMonthlyConsolidationOnAddingHoliday(calculatedPeriodDbid, loggedUser, dataBaseConnectionString,
                            vacationDates.get(j), absenceDayNature.getDbid(), "absenceDays", isChrisCal, "");
                    try {
                        oem.executeEntityUpdateNativeQuery(updateDAStat, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    // from vacation to holiday, so in monthly consolidation: increment holidays & decrement vacations
                    updateDAStat = "update employeedailyattendance set daynature_dbid = " + holidayNature.getDbid()
                            + " where daynature_dbid in (select dbid from daynatures where type = 'V') "
                            + " and dailydate = '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "'"
                            + " and employee_dbid in "
                            + "(select dbid from oemployee where position_dbid in "
                            + "(select dbid from position where unit_dbid in "
                            + "(select dbid from unit "
                            + " where company_dbid = " + company.getDbid() + ")) " + chrisStat + ")";
                    vacationCond = " and employee_dbid in  (select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        vacationCond += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "'"
                                + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "' )";
                    } else {
                        vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                                + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                                + " )";
                    }
                    updateDAStat += vacationCond;
                    updateMonthlyConsolidationOnAddingHoliday(calculatedPeriodDbid, loggedUser, dataBaseConnectionString, vacationDates.get(j),
                            vacationDayNaure.getDbid(), "vacations", isChrisCal, vacationCond);
                    try {
                        oem.executeEntityUpdateNativeQuery(updateDAStat, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {

                    updateDAStat = "update employeedailyattendance set daynature_dbid = " + holidayNature.getDbid()
                            + " where daynature_dbid in (select dbid from daynatures where type in( 'A','O')) "
                            + " and dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                            + " and employee_dbid in "
                            + "(select dbid from oemployee where position_dbid in "
                            + "(select dbid from position where unit_dbid in "
                            + "(select dbid from unit "
                            + "where company_dbid = " + company.getDbid() + ")) " + chrisStat + ")";
                    calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                            paymentMethodDbid, paymentPeriodDbid, loggedUser);
                    updateMonthlyConsolidationOnAddingHoliday(calculatedPeriodDbid, loggedUser, dataBaseConnectionString, vacationDates.get(j),
                            absenceDayNature.getDbid(), "absenceDays", isChrisCal, "");

                    try {
                        oem.executeEntityUpdateNativeQuery(updateDAStat, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    // from vacation to holiday, so in monthly consolidation: increment holidays & decrement vacations
                    updateDAStat = "update employeedailyattendance set daynature_dbid = " + holidayNature.getDbid()
                            + " where daynature_dbid in (select dbid from daynatures where type = 'V') "
                            + " and dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                            + " and employee_dbid in "
                            + "(select dbid from oemployee where position_dbid in "
                            + "(select dbid from position where unit_dbid in "
                            + "(select dbid from unit "
                            + "where company_dbid = " + company.getDbid() + ")) " + chrisStat + ")";
                    vacationCond = " and employee_dbid in  (select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        vacationCond += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "'"
                                + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(vacationDates.get(j)) + "' )";
                    } else {
                        vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                                + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                                + " )";
                    }
                    updateDAStat += vacationCond;
                    updateMonthlyConsolidationOnAddingHoliday(calculatedPeriodDbid, loggedUser, dataBaseConnectionString, vacationDates.get(j),
                            vacationDayNaure.getDbid(), "vacations", isChrisCal, vacationCond);
                    try {
                        oem.executeEntityUpdateNativeQuery(updateDAStat, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

            if (isChrisCal) {
                chrisStat = " and eshift.employee_dbid in (select dbid from oemployee where religion_dbid in (select dbid from udc where code = 'CHR' and type_dbid in (select dbid from udc where code = 'prs_019')))";
            }
            for (int j = 0; j < vacationMethods.size(); j++) {
                if (dataBaseConnectionString.toLowerCase().contains("mysql")) {
                    updateShiftStat = "update employeeannualplanner,employeeshift eshift"
                            + " set eshift." + vacationMethods.get(j) + " = 'H'"
                            + " where employeeannualplanner.planneryear = eshift.planneryear and "
                            + " employeeannualplanner.employee_dbid = eshift.employee_dbid and"
                            + " employeeannualplanner." + vacationMethods.get(j) + "<> 'O' " + chrisStat;
                } else if (dataBaseConnectionString.toLowerCase().contains("sql server")) {
                    updateShiftStat = "update eshift set eshift." + vacationMethods.get(j) + " = 'H' "
                            + "from  employeeshift eshift join employeeannualplanner "
                            + "on employeeannualplanner.employee_dbid = eshift.employee_dbid "
                            + "where employeeannualplanner.planneryear = eshift.planneryear "
                            + "and employeeannualplanner." + vacationMethods.get(j) + "<> 'O'" + chrisStat;
                } else if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateShiftStat = "update employeeshift eshift set eshift." + vacationMethods.get(j) + " = 'H'"
                            + " where eshift.planneryear in"
                            + " (select employeeannualplanner.planneryear from employeeannualplanner where "
                            + " employeeannualplanner.employee_dbid = eshift.employee_dbid and  employeeannualplanner."
                            + vacationMethods.get(j) + "<> 'O')" + chrisStat;
                }
                try {
                    oem.executeEntityUpdateNativeQuery(updateShiftStat, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            // check if the current day nature on the holiday date was a working day
            ofr.append(updateWorkingDaysForAllEmployeesOnAddingHoliday(from, to, isChrisCal, holidayNature, loggedUser));

        } catch (Exception e) {

            OLog.logException(e, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), e);
        } finally {
            return ofr;
        }
    }

    // get calendar of employee in this date, check if it is an offday or not, it yes then exclude from counter
    private int getIntersectionDaysExcludingOffDays(Date from, Date to, Date vacStartDate, Date vacEndDate, Employee employee, OUser loggedUser) {
        int intersection = 0;
        Date tempDate = from;
        while (tempDate.compareTo(to) <= 0) {
            if (vacStartDate.compareTo(tempDate) <= 0 && vacEndDate.compareTo(tempDate) >= 0) {
                if (!isOffDay(tempDate, employee, loggedUser)) {
                    intersection++;
                }
            }
            tempDate = dtUtility.addValueToDate(tempDate, 1, DateTimeUtility.DateFieldType.DAY);
        }
        return intersection;
    }

    private boolean isOffDay(Date tempDate, Employee employee, OUser loggedUser) {
        boolean result = false;
        TMCalendar empCal = calendarService.getEmpCalendar(tempDate, employee, loggedUser);
        if (empCal == null) {
            OLog.logError("This employee have a null calendar.", loggedUser);
        } else {
            NormalWorkingCalendar workingCalendar = calendarService.getEmployeeNormalCalendar(employee, empCal, tempDate, loggedUser);
            if (!empCal.getType().getCode().equals("Regular")) {
                if (workingCalendar != null && workingCalendar.getNumberOfHours().equals(BigDecimal.ZERO)) {
                    result = true;
                }
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(tempDate);
                if (Integer.parseInt(workingCalendar.getDayOff1().getCode()) == calendar.get(Calendar.DAY_OF_WEEK)
                        || (workingCalendar.getDayOff2() != null && Integer.parseInt(workingCalendar.getDayOff2().getCode()) == calendar.get(Calendar.DAY_OF_WEEK))) {
                    result = true;
                }
            }
        }

        return result;
    }

    @Override
    public OFunctionResult deleteHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        Date from = null, to = null, tempDate = null;
        Company company = null;

        boolean isChrisCal = false;
        try {
            if (odm.getData().get(0).getClass().getSimpleName().equals("HolidayCalendar")) {
                HolidayCalendar holidayCalendar = (HolidayCalendar) odm.getData().get(0);
                if (holidayCalendar != null) {
                    from = holidayCalendar.getDateFrom();
                    to = holidayCalendar.getDateTo();
                    company = holidayCalendar.getCompany();
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("ChristianCalendar")) {
                ChristianCalendar chrisCal = (ChristianCalendar) odm.getData().get(0);
                if (chrisCal != null) {
                    from = chrisCal.getDateFrom();
                    to = chrisCal.getDateTo();
                    company = chrisCal.getCompany();

                    isChrisCal = true;
                }
            }

            if (from == null || to == null) {
                ofr.addError(usrMsgService.getUserMessage("HolidayMissingDates", loggedUser));
                return ofr;
            }

            String yearStr = dtUtility.YEAR_FORMAT.format(from);
            Integer currentYear = Integer.parseInt(yearStr);

            // check if there exists vacation requests intersecting with this holiday
            // if yes then either cancel it or modify it
            int holidayPeriod = 0;
            float vacPeriod = 0;
            long vacReqDbid = 0, vacDbid = 0, empDbid = 0;
            Object[] objArr;
            List vacsDbids = getVacRequestsDbidsOnHoliday(from, to, loggedUser);

            Date vacStartDate, vacEndDate;
            // update periods

            String updateBalance, updatePeriod;
            Employee emp;
            for (int i = 0; i < vacsDbids.size(); i++) {
                objArr = (Object[]) vacsDbids.get(i);
                vacReqDbid = Long.parseLong(objArr[0].toString());
                vacPeriod = Float.parseFloat(objArr[1].toString());
                vacDbid = Long.parseLong(objArr[2].toString());
                empDbid = Long.parseLong(objArr[3].toString());
                vacStartDate = dtUtility.DATE_FORMAT.parse(objArr[4].toString());
                vacEndDate = dtUtility.DATE_FORMAT.parse(objArr[5].toString());

                emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggedUser);
                holidayPeriod = getIntersectionDaysExcludingOffDays(from, to, vacStartDate, vacEndDate, emp, loggedUser);

                vacPeriod += holidayPeriod;
                updatePeriod = "update employeedayoffrequest "
                        + "set period = " + vacPeriod
                        + " , actualBalance = actualBalance - " + holidayPeriod
                        + " , balance = balance - " + holidayPeriod
                        + " , currentBalance = currentBalance - " + holidayPeriod
                        + "where dbid = " + vacReqDbid;
                oem.executeEntityUpdateNativeQuery(updatePeriod, loggedUser);

                // update all the following vacations' balances
                updateBalance = getUpdateStatForVacsFollowingHoliday(to, loggedUser);

                updateBalance = updateBalance.replaceAll("@holidayPeriod", "-" + holidayPeriod);
                updateBalance = updateBalance.replaceAll("@empDbid", empDbid + "");
                updateBalance = updateBalance.replaceAll("@vacDbid", vacDbid + "");
                oem.executeEntityUpdateNativeQuery(updateBalance, loggedUser);
            }

            UDC workingDayNature = null, workingOnHolidayNature = null, absenceDayNature = null, holidayDayNaure = null,
                    vacationDayNaure = null;
            List<String> udcConditions = new ArrayList<String>();
            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Working'");
            try {
                workingDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='WorkingOnHoliday'");
            try {
                workingOnHolidayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Absence'");
            try {
                absenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Holiday'");
            try {
                holidayDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            udcConditions.clear();
            udcConditions.add("type.code='900'");
            udcConditions.add("code='Vacation'");
            try {
                vacationDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (holidayDayNaure == null || workingDayNature == null || workingOnHolidayNature == null || absenceDayNature == null
                    || vacationDayNaure == null) {
                ofr.addError(usrMsgService.getUserMessage("O-00700", loggedUser));
                return ofr;
            }

            //TM effect
            List<String> conds = new ArrayList<String>();
            conds.clear();
            conds.add("description = 'default_pay_method'");
            conds.add("company.dbid = " + company.getDbid());

            PayrollParameter parameter = null;
            try {
                parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            String paymentMethodDbid = "", paymentPeriodDbid = "";
            if (parameter != null) {
                paymentMethodDbid = parameter.getValueData();
            }

            conds.clear();
            conds.add("description = 'default_pay_period'");
            conds.add("company.dbid = " + company.getDbid());
            parameter = null;
            try {
                parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (parameter != null) {
                paymentPeriodDbid = parameter.getValueData();
            }
            long startTimeInSec = System.currentTimeMillis();

            List<EmployeeDailyAttendance> loadedEmpDailyAttendanceLst = null;
            List<String> empDAConditions = new ArrayList<String>();
            String christCondition = "";
            // get all employees within this company
            String selectStat = "select dbid from oemployee where deleted = 0 and position_dbid in "
                    + "(select dbid from position where unit_dbid in (select dbid from unit where company_dbid =" + loggedUser.getUserPrefrence1() + "))";
            if (isChrisCal) {
                christCondition = "and religion_dbid in (select dbid from udc where code = 'CHR' and deleted = 0)";
                selectStat += christCondition;
            }

            String selectTMorTimeSheet = "select valuedata from payrollparameter where description = 'TimeManagOrTimeSheet'";
            Object result = oem.executeEntityNativeQuery(selectTMorTimeSheet, loggedUser);
            boolean isTimeSheet = false;
            if (result != null) {
                if (result.toString().equals("Sheet")) {
                    isTimeSheet = true;
                }
            }

            String updateTableName;
            if (isTimeSheet) {
                updateTableName = "EmployeeTimeSheet";
            } else {
                updateTableName = "EmployeeAnnualPlanner";
            }

            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

            String updateStat, dayField;
            long calculatedPeriodDbid;
            String vacationCond;
            List<Method> vacationMethods = getEmployeeVacations(from, to, currentYear);
            for (int j = 0; j < vacationMethods.size(); j++) {
                String[] temp = vacationMethods.get(j).getName().split("D");
                String[] temp2 = temp[0].split("M");
                String month = temp2[1];
                String day = temp[1];
                dayField = "m" + month + "D" + day;

                // on vacation
                tempDate = dtUtility.DATE_FORMAT.parse(yearStr + "-" + month + "-" + day);
                updateStat = "update " + updateTableName + " set " + dayField + " = 'V' "
                        + " where employee_dbid in "
                        + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' )";
                } else {
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                }

                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                // update annualplanner & timesheet
                // on working
                updateStat = "update " + updateTableName + " set " + dayField;
                updateStat += "= 'W' where " + dayField + " = 'F' and employee_dbid in (" + selectStat + ") and "
                        + " planneryear = " + currentYear.toString();
                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                // on absence
                updateStat = "update " + updateTableName + " set " + dayField;
                updateStat += "= 'A' where " + dayField + " = 'H' and employee_dbid in (" + selectStat + ") and "
                        + " planneryear = " + currentYear.toString()
                        + " and employee_dbid not in "
                        + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' )";
                } else {
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                }
                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                // Shift table - set the calendar that is in the DA record (normal working calendar)
                updateStat = "update employeeshift set " + dayField;
                if (dataBaseConnectionString.toLowerCase().contains("mysql")) {
                    updateStat += " = (select code  from normalworkingcalendar where dbid in "
                            + " (select workingcalendar_dbid from employeedailyattendance where employeeshift.employee_Dbid = employeedailyattendance.employee_dbid"
                            + " and employeedailyattendance.dailydate = ";
                } else {
                    updateStat += " = (select cast(code as varchar(10)) from normalworkingcalendar where dbid in "
                            + " (select workingcalendar_dbid from employeedailyattendance where employeeshift.employee_Dbid = employeedailyattendance.employee_dbid"
                            + " and employeedailyattendance.dailydate = ";
                }
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += "' " + dtUtility.DATE_FORMAT.format(tempDate) + "'))";
                } else {
                    updateStat += dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate)) + "))";
                }
                updateStat += " where employeeshift.employee_dbid not in "
                        + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' )";
                } else {
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                }

                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                // Shift table - in case of vacation
                updateStat = "update employeeshift set " + dayField + " = 'V' ";
                updateStat += " where employee_dbid in "
                        + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' )";
                } else {
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                }
                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                // <editor-fold defaultstate="collapsed" desc="update employeedailyattendance">
                // 1 - from workingonholiday to working
                updateStat = "update employeedailyattendance set daynature_dbid = " + workingDayNature.getDbid()
                        + " where employeedailyattendance.daynature_dbid = " + workingOnHolidayNature.getDbid();
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += " and employeedailyattendance.dailydate = ' " + dtUtility.DATE_FORMAT.format(tempDate) + "'";
                } else {
                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                }
                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                // 2 - from holiday to absence
                updateStat = "update employeedailyattendance set daynature_dbid = " + absenceDayNature.getDbid()
                        + " where employeedailyattendance.daynature_dbid = " + holidayDayNaure.getDbid();
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += " and employeedailyattendance.dailydate = ' " + dtUtility.DATE_FORMAT.format(tempDate) + "'";
                } else {
                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                }
                vacationCond = " and employee_dbid not in "
                        + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    vacationCond += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' )";
                } else {
                    vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                }
                updateStat += vacationCond;
                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                // recalc consolidation to absence++ and holidaydays--
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                        paymentMethodDbid, paymentPeriodDbid, loggedUser);
                updateMonthlyConsolidationOnDeletingHoliday(calculatedPeriodDbid, loggedUser, dataBaseConnectionString,
                        tempDate, absenceDayNature.getDbid(), "absenceDays", isChrisCal, vacationCond);

                // 3 - from holiday to vacation
                updateStat = "update employeedailyattendance set daynature_dbid = " + vacationDayNaure.getDbid()
                        + " where employeedailyattendance.daynature_dbid = " + holidayDayNaure.getDbid();

                vacationCond = " and employee_dbid in "
                        + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    vacationCond += " and aa.STARTDATE<= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'"
                            + " and aa.ENDDATE>= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' )";
                } else {
                    vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                }
                updateStat += vacationCond;
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateStat += " and employeedailyattendance.dailydate = ' " + dtUtility.DATE_FORMAT.format(tempDate) + "'";
                } else {
                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                }
                oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                // recalc consolidation to vacations++ and holidaydays--
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                        paymentMethodDbid, paymentPeriodDbid, loggedUser);
                updateMonthlyConsolidationOnDeletingHoliday(calculatedPeriodDbid, loggedUser, dataBaseConnectionString,
                        tempDate, vacationDayNaure.getDbid(), "vacations", isChrisCal, vacationCond);

                //<editor-fold defaultstate="collapsed" desc="update records in daily attendance for recalc if attendance exists">
                empDAConditions.clear();
                empDAConditions.add("employee.positionSimpleMode.unit.company.dbid = " + loggedUser.getUserPrefrence1());
                empDAConditions.add("dailyDate = '" + dtUtility.DATE_FORMAT.format(tempDate) + "'");
                empDAConditions.add("timeIn is not null");
                try {
                    loadedEmpDailyAttendanceLst = (List<EmployeeDailyAttendance>) oem.loadEntityList(EmployeeDailyAttendance.class.getSimpleName(),
                            empDAConditions, null, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                for (EmployeeDailyAttendance loadedEmpDailyAttendance : loadedEmpDailyAttendanceLst) {
                    loadedEmpDailyAttendance.setDayNature(workingDayNature);
                    entitySetupService.callEntityUpdateAction(loadedEmpDailyAttendance, loggedUser);
                }
                //</editor-fold>
                // </editor-fold>
            }
            long endTimeInSec = System.currentTimeMillis();
            System.out.println("&&&&&&& Time for deleting holiday is " + ((endTimeInSec - startTimeInSec)) + " m. seconds &&&&&&&");
        } catch (Exception ex) {
            ofr.addError(usrMsgService.getUserMessage("O-00701", loggedUser));
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    /**
     * This method should be no longer called as editing will be deletion
     * followed by addition.
     *
     * @param odm
     * @param params
     * @param loggedUser
     * @return
     */
    @Override
    public OFunctionResult editHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Date from = null, to = null,
                    loadedFrom = null, loadedTo = null;
            Company company = null;

            Calendar today = Calendar.getInstance();

            List<String> loadedConds = new ArrayList<String>();
            String entityName = null;

            if (odm.getData().get(0).getClass().getSimpleName().equals("HolidayCalendar")) {
                entityName = HolidayCalendar.class.getSimpleName();
                HolidayCalendar holidayCalendar = (HolidayCalendar) odm.getData().get(0);
                if (holidayCalendar != null) {
                    from = holidayCalendar.getDateFrom();
                    to = holidayCalendar.getDateTo();
                    company = holidayCalendar.getCompany();

                    loadedConds.clear();
                    loadedConds.add("dbid = " + holidayCalendar.getDbid());
                    HolidayCalendar loadedHolidayCalendar = null;
                    try {
                        loadedHolidayCalendar = (HolidayCalendar) oem.loadEntity(
                                HolidayCalendar.class.getSimpleName(), loadedConds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (loadedHolidayCalendar != null) {
                        loadedFrom = loadedHolidayCalendar.getDateFrom();
                        loadedTo = loadedHolidayCalendar.getDateTo();
                    }
                }
            } else if (odm.getData().get(0).getClass().getSimpleName().equals("ChristianCalendar")) {
                entityName = ChristianCalendar.class.getSimpleName();
                ChristianCalendar chrisCal = (ChristianCalendar) odm.getData().get(0);
                if (chrisCal != null) {
                    from = chrisCal.getDateFrom();
                    to = chrisCal.getDateTo();
                    company = chrisCal.getCompany();

                    loadedConds.clear();
                    loadedConds.add("dbid = " + chrisCal.getDbid());
                    ChristianCalendar loadedChrisCalendar = null;
                    try {
                        loadedChrisCalendar = (ChristianCalendar) oem.loadEntity(
                                ChristianCalendar.class.getSimpleName(), loadedConds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (loadedChrisCalendar != null) {
                        loadedFrom = loadedChrisCalendar.getDateFrom();
                        loadedTo = loadedChrisCalendar.getDateTo();
                    }
                }
            }

            if (from == null || to == null) {
                ofr.addError(usrMsgService.getUserMessage("HolidayMissingDates", loggedUser));
                return ofr;
            }
            if (loadedFrom == null || loadedTo == null) {
                ofr.addError(usrMsgService.getUserMessage("HolidayMissingDates002", loggedUser));
                return ofr;
            }

//            if (from.compareTo(today.getTime()) < 0) {
//                ofr.addError(usrMsgService.getUserMessage("pastDate", loggedUser));
//                return ofr;
//            }
//
//            if (to.compareTo(today.getTime()) < 0) {
//                ofr.addError(usrMsgService.getUserMessage("pastDate", loggedUser));
//                return ofr;
//            }
            if (loadedFrom.compareTo(from) != 0) {
                if (loadedFrom.compareTo(from) > 0) {
                    List<String> condition = new ArrayList<String>();
                    condition.add("name='" + entityName + "'");
                    ODataType dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(), condition, null, loggedUser);
                    List holidayData = new ArrayList();

                    Calendar tempCal = Calendar.getInstance();

                    if (entityName.equals("HolidayCalendar")) {
                        HolidayCalendar tempHC = new HolidayCalendar();
                        tempHC.setDateFrom(from);

                        tempCal.setTime(loadedFrom);
                        tempCal.add(tempCal.DAY_OF_MONTH, -1);

                        tempHC.setDateTo(tempCal.getTime());
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    } else if (entityName.equals("ChristianCalendar")) {
                        ChristianCalendar tempHC = new ChristianCalendar();
                        tempHC.setDateFrom(from);

                        tempCal.setTime(loadedFrom);
                        tempCal.add(tempCal.DAY_OF_MONTH, -1);

                        tempHC.setDateTo(tempCal.getTime());
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    }

                    ODataMessage empDataMessage = new ODataMessage(dataType, holidayData);

                    ofr.append(holidayCalendarService.addHoliday(empDataMessage, null, loggedUser));
                } else {
                    List<String> condition = new ArrayList<String>();
                    condition.add("name='" + entityName + "'");
                    ODataType dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(), condition, null, loggedUser);
                    List holidayData = new ArrayList();

                    Calendar tempCal = Calendar.getInstance();

                    if (entityName.equals("HolidayCalendar")) {
                        HolidayCalendar tempHC = new HolidayCalendar();
                        tempHC.setDateFrom(loadedFrom);
                        tempCal.setTime(from);
                        tempCal.add(tempCal.DAY_OF_MONTH, -1);

                        tempHC.setDateTo(tempCal.getTime());
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    } else if (entityName.equals("ChristianCalendar")) {
                        ChristianCalendar tempHC = new ChristianCalendar();
                        tempHC.setDateFrom(loadedFrom);
                        tempCal.setTime(from);
                        tempCal.add(tempCal.DAY_OF_MONTH, -1);

                        tempHC.setDateTo(tempCal.getTime());
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    }

                    ODataMessage empDataMessage = new ODataMessage(dataType, holidayData);

                    ofr.append(holidayCalendarService.deleteHoliday(empDataMessage, null, loggedUser));
                }
            }
            if (loadedTo.compareTo(to) != 0) {
                if (loadedTo.compareTo(to) < 0) {
                    List<String> condition = new ArrayList<String>();
                    condition.add("name='" + entityName + "'");
                    ODataType dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(), condition, null, loggedUser);
                    List holidayData = new ArrayList();

                    Calendar tempCal = Calendar.getInstance();

                    if (entityName.equals("HolidayCalendar")) {
                        HolidayCalendar tempHC = new HolidayCalendar();

                        tempCal.setTime(loadedTo);
                        tempCal.add(tempCal.DAY_OF_MONTH, 1);

                        tempHC.setDateFrom(tempCal.getTime());
                        tempHC.setDateTo(to);
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    } else if (entityName.equals("ChristianCalendar")) {
                        ChristianCalendar tempHC = new ChristianCalendar();

                        tempCal.setTime(loadedTo);
                        tempCal.add(tempCal.DAY_OF_MONTH, 1);

                        tempHC.setDateFrom(tempCal.getTime());
                        tempHC.setDateTo(to);
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    }

                    ODataMessage empDataMessage = new ODataMessage(dataType, holidayData);

                    ofr.append(holidayCalendarService.addHoliday(empDataMessage, null, loggedUser));
                } else {
                    List<String> condition = new ArrayList<String>();
                    condition.add("name='" + entityName + "'");
                    ODataType dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(), condition, null, loggedUser);
                    List holidayData = new ArrayList();

                    Calendar tempCal = Calendar.getInstance();

                    if (entityName.equals("HolidayCalendar")) {
                        HolidayCalendar tempHC = new HolidayCalendar();

                        tempCal.setTime(to);
                        tempCal.add(tempCal.DAY_OF_MONTH, 1);

                        tempHC.setDateFrom(tempCal.getTime());
                        tempHC.setDateTo(loadedTo);
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    } else if (entityName.equals("ChristianCalendar")) {
                        ChristianCalendar tempHC = new ChristianCalendar();

                        tempCal.setTime(to);
                        tempCal.add(tempCal.DAY_OF_MONTH, 1);

                        tempHC.setDateFrom(tempCal.getTime());
                        tempHC.setDateTo(loadedTo);
                        tempHC.setCompany(company);

                        holidayData.add(tempHC);
                    }

                    ODataMessage empDataMessage = new ODataMessage(dataType, holidayData);

                    ofr.append(holidayCalendarService.deleteHoliday(empDataMessage, null, loggedUser));
                }
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), e);
        } finally {
            return ofr;
        }
    }

    private List<Method> getEmployeeVacations(Date start, Date end, int currentYear) {
        List<Method> vacationMethods = new ArrayList<Method>();
        DateTimeZone dateTimeZone = DateTimeZone.forID("EET");
        DateTime startDateTime = new DateTime(start, dateTimeZone);
        DateTime endDateTime = new DateTime(end, dateTimeZone);

        DateTime tempDT = null;
        Class types[] = new Class[1];
        types[0] = String.class;

        if (currentYear == startDateTime.getYear() && currentYear == endDateTime.getYear()) {
            tempDT = startDateTime;
            //endDateTime = endDateTime.plusDays(1);
            do {
                Method vacMethod = null;
                try {
                    vacMethod = EmployeeAnnualPlanner.class.getMethod("setM" + tempDT.getMonthOfYear() + "D" + (tempDT.getDayOfMonth()), types);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                vacationMethods.add(vacMethod);
                tempDT = tempDT.plusDays(1);
            } while (tempDT.compareTo(endDateTime) == -1 || tempDT.compareTo(endDateTime) == 0);
        } else if (currentYear == startDateTime.getYear() && currentYear != endDateTime.getYear()) {
            tempDT = startDateTime;
            do {
                Method vacMethod = null;
                try {
                    vacMethod = EmployeeAnnualPlanner.class.getMethod("setM" + tempDT.getMonthOfYear() + "D" + (tempDT.getDayOfMonth()), types);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                vacationMethods.add(vacMethod);
                tempDT.plusDays(1);
            } while (tempDT.getYear() == currentYear);
        } else if (currentYear != startDateTime.getYear() && currentYear == endDateTime.getYear()) {
            tempDT = endDateTime;
            do {
                Method vacMethod = null;
                try {
                    vacMethod = EmployeeAnnualPlanner.class.getMethod("setM" + tempDT.getMonthOfYear() + "D" + (tempDT.getDayOfMonth()), types);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                vacationMethods.add(vacMethod);
                tempDT.minusDays(1);
            } while (tempDT.getYear() == currentYear);
        }

        return vacationMethods;
    }

    private List getVacRequestsDbidsOnHoliday(Date holidayFrom, Date holidayTo, OUser loggedUser) {
        List vacRequests = new ArrayList();
        try {
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

            String sql = null;
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                sql = "select AA.dbid, AA.period, AA.vacation_dbid, AA.employee_dbid, AA.startDate, AA.endDate from employeedayoffrequest AA, dayoff BB "
                        + " where AA.deleted = 0 and AA.cancelled = 'N' and AA.dayofftype = 'VACATION' and "
                        + " BB.removeHoliday = 'Y' and AA.vacation_dbid = BB.dbid AND BB.dayofftype = 'VACATION'"
                        + " and ("
                        + "     (AA.startDate >= '" + (dtUtility.DATE_FORMAT.format(holidayFrom)) + "' "
                        + "     and AA.startDate <= '" + (dtUtility.DATE_FORMAT.format(holidayTo)) + "')"
                        + "     or (AA.endDate >= '" + (dtUtility.DATE_FORMAT.format(holidayFrom)) + "' "
                        + "     and AA.endDate <= '" + (dtUtility.DATE_FORMAT.format(holidayTo)) + "')"
                        + "     or (AA.startDate< '" + (dtUtility.DATE_FORMAT.format(holidayFrom)) + "' "
                        + "     and AA.endDate > '" + (dtUtility.DATE_FORMAT.format(holidayTo)) + "') "
                        + ")";
            } else {
                sql = "select AA.dbid, AA.period, AA.vacation_dbid, AA.employee_dbid, AA.startDate, AA.endDate from employeedayoffrequest AA, dayoff BB "
                        + " where AA.deleted = 0 and AA.cancelled = 'N' and AA.dayofftype = 'VACATION' and "
                        + " BB.removeHoliday = 'Y' and AA.vacation_dbid = BB.dbid AND BB.dayofftype = 'VACATION'"
                        + " and ("
                        + "     (AA.startDate >= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayFrom)) + " "
                        + "     and AA.startDate <= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayTo)) + ")"
                        + "     or (AA.endDate >= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayFrom)) + " "
                        + "     and AA.endDate <= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayTo)) + ")"
                        + "     or (AA.startDate< " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayFrom)) + " "
                        + "     and AA.endDate > " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayTo)) + ") "
                        + ")";
            }
            vacRequests = oem.executeEntityListNativeQuery(sql, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return vacRequests;
    }

    private String getUpdateStatForVacsFollowingHoliday(Date holidayTo, OUser loggedUser) {
        String sql = null;
        try {
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                sql = "update employeedayoffrequest set currentBalance = currentBalance+@holidayPeriod, "
                        + " actualbalance = actualbalance+@holidayPeriod, "
                        + " balance = balance+@holidayPeriod "
                        + " where employee_Dbid = @empDbid and vacation_dbid = @vacDbid "
                        + " and startDate > '" + dtUtility.DATE_FORMAT.format(holidayTo) + "' and "
                        + " deleted = 0 and cancelled = 'N' and dayofftype = 'VACATION'";
            } else {
                sql = "update employeedayoffrequest set currentBalance = currentBalance+@holidayPeriod, "
                        + " actualbalance = actualbalance+@holidayPeriod, "
                        + " balance = balance+@holidayPeriod "
                        + " where employee_Dbid = @empDbid and vacation_dbid = @vacDbid "
                        + " and startDate > " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayTo)) + "  and "
                        + " deleted = 0 and cancelled = 'N' and dayofftype = 'VACATION'";
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return sql;
    }

    private OFunctionResult updateWorkingDaysForAllEmployeesOnAddingHoliday(Date from, Date to, boolean isChristianHoliday, UDC dayNature, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<String> conds = new ArrayList<String>();
            conds.add("dailyDate >= '" + dtUtility.DATE_FORMAT.format(from) + "'");
            conds.add("dailyDate <= '" + dtUtility.DATE_FORMAT.format(to) + "'");
            conds.add("timeIn is not null");
            if (isChristianHoliday) {
                conds.add("employee.religion.code = 'CHR'");
            }
            List<EmployeeDailyAttendance> empDAs;
            empDAs = (List<EmployeeDailyAttendance>) oem.loadEntityList(EmployeeDailyAttendance.class.getSimpleName(),
                    conds, null, null, loggedUser);

            EmployeeDailyAttendance empDA;
            for (int i = 0; i < empDAs.size(); i++) {
                empDA = empDAs.get(i);
                empDA.setDayNature(dayNature);
                ofr.append(entitySetupService.callEntityUpdateAction(empDA, loggedUser));
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return ofr;
    }

    private long getCalculatedPeriodOfThisDay(int day, int month, int year, String paymentMethodDbid, String paymentPeriodDbid, OUser loggedUser) {

        // loading the cut off from payrollparameter
        String payrollParameterDsc = "day_of_calculation";
        String cuttOffDay1 = "0";
        String[] payrollParameterResult = calendarService.getFromPayrollParameter(payrollParameterDsc, loggedUser);
        if (payrollParameterResult != null) {
            cuttOffDay1 = payrollParameterResult[0];
        }
        //calcCurrentMonth
        String calcCurrentMonth = "";;
        payrollParameterDsc = "calculation_currentMonth";
        payrollParameterResult = calendarService.getFromPayrollParameter(payrollParameterDsc, loggedUser);
        if (payrollParameterResult != null) {
            calcCurrentMonth = payrollParameterResult[0];
        }

        //calcFullMonth
        String calcFullMonth = "N";
        payrollParameterDsc = "calculation_fullMonth";
        payrollParameterResult = calendarService.getFromPayrollParameter(payrollParameterDsc, loggedUser);
        if (payrollParameterResult != null) {
            calcFullMonth = payrollParameterResult[0];
        }
        int cutOffStart;
        if (calcFullMonth != null && calcFullMonth.equals("Y")) { // from 1->30
            cutOffStart = 1;

            if (calcCurrentMonth != null && calcCurrentMonth.equals("N")) { // the prev month
                if (month == 12) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }
            }
        } else if ((calcFullMonth != null && calcFullMonth.equals("N"))
                || (calcFullMonth == null)) {
            cutOffStart = Integer.parseInt(cuttOffDay1) + 1;
            if (day >= cutOffStart) {
                month = (month + 1) % 12;
                if (month == 0) {
                    month = 12;
                }
                if (month == 1) {
                    year++;
                }
            }
        }

        String selectCalcPeriod = "select dbid from calculatedPeriod where year = " + year + " and month = " + month + " and paymentMethod_Dbid = "
                + paymentMethodDbid + " and payPeriod_dbid = " + paymentPeriodDbid;
        long periodDbid = 0;
        try {
            Object result = oem.executeEntityNativeQuery(selectCalcPeriod, loggedUser);
            if (result != null) {
                periodDbid = Long.parseLong(result.toString());
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return periodDbid;
    }

    private void updateMonthlyConsolidationOnDeletingHoliday(long calculatedPeriodDbid, OUser loggedUser,
            String dataBaseConnectionString, Date currentDate, long natureDbid, String incrementedDaysName, boolean isChrist, String vacationCond) {
        String updateStat = "update employeemonthlyconsolidation set " + incrementedDaysName + "= " + incrementedDaysName + "+ 1 , "
                + " holidays = holidays-1";
        if (incrementedDaysName.contains("absence")) {
            if (!dataBaseConnectionString.toLowerCase().contains("oracle") && !dataBaseConnectionString.toLowerCase().contains("mysql")) {
                updateStat += ", postedAbsenceDays = "
                        + " ((select top 1 AbsenceFactor from TMRule where tmCalendar_dbid = (select top 1 currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) )) * (absencedays) )"
                        + ", "
                        + " parametrizedAbsenceDays = "
                        + " ((select top 1 AbsenceFactor from TMRule where tmCalendar_dbid = (select top 1 currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) )) * (absencedays) )";
            } else if (dataBaseConnectionString.toLowerCase().contains("mysql")) {
                updateStat += ", postedAbsenceDays = "
                        + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) LIMIT 1 ) LIMIT 1 ) * (absencedays) )"
                        + ", "
                        + " parametrizedAbsenceDays = "
                        + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) LIMIT 1 ) LIMIT 1 ) * (absencedays) )";
            } else {
                updateStat += ", postedAbsenceDays = "
                        + " ((select  AbsenceFactor from TMRule where tmCalendar_dbid = (select  currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) and rownum=1)and rownum=1) * (absencedays))"
                        + ", "
                        + " parametrizedAbsenceDays = "
                        + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select  currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) and rownum=1 )and rownum=1) * (absencedays) )";
            }
        }
        updateStat += "  where posted = 'N' and calculatedPeriod_dbid = " + calculatedPeriodDbid
                + " and employee_dbid in (select employee_dbid from employeedailyattendance where dayNature_dbid = " + natureDbid;
        if (!dataBaseConnectionString.toLowerCase()
                .contains("oracle")) {
            updateStat += " and employeedailyattendance.dailydate = ' "
                    + dtUtility.DATE_FORMAT.format(currentDate) + "'" + " )";
            updateStat = updateStat.replaceAll("@date", "'" + dtUtility.DATE_FORMAT.format(currentDate) + "'");
        } else {
            updateStat += " and employeedailyattendance.dailydate = "
                    + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)) + ")";
            updateStat = updateStat.replaceAll("@date", dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)));
        }

        if (isChrist) {
            updateStat += " and employee_Dbid in (select dbid from oemployee "
                    + "where religion_dbid in (select dbid from udc where code = 'CHR' and type_dbid in (select dbid from udc where code = 'prs_019')))";
        }
        updateStat += vacationCond;
        try {
            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private void updateMonthlyConsolidationOnAddingHoliday(long calculatedPeriodDbid, OUser loggedUser,
            String dataBaseConnectionString, Date currentDate, long natureDbid, String decrementedDaysName, boolean isChrist, String vacationCond) {
        String updateStat = "update employeemonthlyconsolidation set " + decrementedDaysName + "= " + decrementedDaysName + "- 1 , "
                + " holidays = holidays+1";
        if (decrementedDaysName.contains("absence")) {
            if (!dataBaseConnectionString.toLowerCase().contains("oracle") && !dataBaseConnectionString.toLowerCase().contains("mysql")) {
                updateStat += ", postedAbsenceDays = "
                        + " ((select TOP 1 AbsenceFactor from TMRule where tmCalendar_dbid = (select TOP 1 currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) )) * (absencedays) )"
                        + ", "
                        + " parametrizedAbsenceDays = "
                        + " ((select TOP 1 AbsenceFactor from TMRule where tmCalendar_dbid = (select TOP 1 currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) )) * (absencedays) )";
            } else if (dataBaseConnectionString.toLowerCase().contains("mysql")) {
                updateStat += ", postedAbsenceDays = "
                        + " ((select  AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) LIMIT 1 ) LIMIT 1) * (absencedays) )"
                        + ", "
                        + " parametrizedAbsenceDays = "
                        + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) LIMIT 1 ) LIMIT 1 ) * (absencedays) )";
            } else {
                updateStat += ", postedAbsenceDays = "
                        + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) and rownum=1 ) and rownum=1) * (absencedays) )"
                        + ", "
                        + " parametrizedAbsenceDays = "
                        + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                        + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                        + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                        + " and EmployeeWorkingCalHistory.cancelled = 0"
                        + " and EmployeeWorkingCalHistory.deleted = 0"
                        + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                        + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                        + " or currentEffectiveEndDate is null) and rownum=1 ) and rownum=1) * (absencedays) )";
            }
        }
        updateStat += " where posted = 'N' and calculatedPeriod_dbid = " + calculatedPeriodDbid;
        updateStat += " and employee_dbid in (select employee_dbid from employeedailyattendance where dayNature_dbid = " + natureDbid;
        if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
            updateStat += " and employeedailyattendance.dailydate = ' "
                    + dtUtility.DATE_FORMAT.format(currentDate) + "')";
            updateStat = updateStat.replaceAll("@date", "'" + dtUtility.DATE_FORMAT.format(currentDate) + "'");
        } else {
            updateStat += " and employeedailyattendance.dailydate = "
                    + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)) + " )";
            updateStat = updateStat.replaceAll("@date", dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)));
        }

        if (isChrist) {
            updateStat += " and employee_Dbid in (select dbid from oemployee "
                    + "where religion_dbid in (select dbid from udc where code = 'CHR' and type_dbid in (select dbid from udc where code = 'prs_019')))";
        }
        updateStat += vacationCond;
        try {
            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    @Override
    public void assignHolidayToEmployee(HolidayAssign holiday, OUser loggedUser) {

        Set<Employee> selectedEmployees = new HashSet<>();
        List<Employee> employees = new ArrayList<>();
        List<String> conds = new ArrayList<>();
        List<String> authorizedToEmployees = dataSecurityService.getEmployeeSecurityDbids(loggedUser);
        List<String> selectedEmployeesDbids = new ArrayList<>();

        if (authorizedToEmployees.isEmpty()) {
            //load all employees
            String sql = "select cast (dbid as varchar(100)) from oemployee where deleted=0 ";
            try {
                if (holiday.getHolidayDates().getHoliday().getReligion() != null && holiday.getHolidayDates().getHoliday().getReligionDependent().equalsIgnoreCase("Y")) {
                    sql += " and Religion_dbid =" + holiday.getHolidayDates().getHoliday().getReligion().getDbid();
                }
                authorizedToEmployees = (List<String>) oem.executeEntityListNativeQuery(sql, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);

            }
        }
        if (holiday != null) {
            //add for all employees
            if (holiday.getEmployee() == null && holiday.getUnit() == null && holiday.getLocation() == null && holiday.getPostion() == null) {
                selectedEmployeesDbids.addAll(authorizedToEmployees);
            } else {
                if (holiday.getEmployee() != null) {
                    selectedEmployees.add(holiday.getEmployee());
                }
                try {
                    if (holiday.getPostion() != null) {
                        if (holiday.getHolidayDates().getHoliday().getReligionDependent().equalsIgnoreCase("Y")) {
                            conds.add("religion.dbid=" + holiday.getHolidayDates().getHoliday().getReligion().getDbid());
                        }
                        conds.add("positionSimpleMode.dbid = " + holiday.getPostion().getDbid());
                        employees = (List<Employee>) oem.loadEntityList(Employee.class.getSimpleName(), conds, null, null, loggedUser);
                        selectedEmployees.addAll(employees);
                    }
                    if (holiday.getUnit() != null) {
                        conds.clear();
                        if (holiday.getHolidayDates().getHoliday().getReligionDependent().equalsIgnoreCase("Y")) {
                            conds.add("religion.dbid=" + holiday.getHolidayDates().getHoliday().getReligion().getDbid());
                        }
                        conds.add("positionSimpleMode.unit.dbid = " + holiday.getUnit().getDbid());
                        employees = (List<Employee>) oem.loadEntityList(Employee.class.getSimpleName(), conds, null, null, loggedUser);
                        selectedEmployees.addAll(employees);
                    }
                    if (holiday.getLocation() != null) {
                        conds.clear();
                        if (holiday.getHolidayDates().getHoliday().getReligionDependent().equalsIgnoreCase("Y")) {
                            conds.add("religion.dbid=" + holiday.getHolidayDates().getHoliday().getReligion().getDbid());
                        }
                        conds.add("payroll.location.dbid = " + holiday.getLocation().getDbid());
                        employees = (List<Employee>) oem.loadEntityList(Employee.class.getSimpleName(), conds, null, null, loggedUser);
                        selectedEmployees.addAll(employees);
                    }
                    // employees is the list of selected employees from the screen
                    employees = new ArrayList<>(selectedEmployees);

                    for (int i = 0; i < employees.size(); i++) {
                        selectedEmployeesDbids.add(String.valueOf(employees.get(i).getDbid()));
                    }
                } catch (Exception ex) {
                    Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            String employeeDbids = "";
            String query = "";

            selectedEmployeesDbids.retainAll(authorizedToEmployees);
            if (!selectedEmployeesDbids.isEmpty()) {

                if (holiday.getIncludeOrExclude().equalsIgnoreCase("I")) {
                    query = partitionEmployeeDbids(selectedEmployeesDbids, "");
                    employeeDbids = partitionEmployeeDbidsJpql(selectedEmployeesDbids, "");
                    includeEmployees(query, employeeDbids, selectedEmployeesDbids, holiday.getHolidayDates(), holiday.getIncludeOrExclude(), loggedUser);
                } else {
                    // exclude selected employees
                    authorizedToEmployees.removeAll(selectedEmployeesDbids);
                    if (!authorizedToEmployees.isEmpty()) {
                        query = partitionEmployeeDbids(selectedEmployeesDbids, " not ");
                        employeeDbids = partitionEmployeeDbidsJpql(selectedEmployeesDbids, " not");
                        includeEmployees(query, employeeDbids, authorizedToEmployees, holiday.getHolidayDates(), holiday.getIncludeOrExclude(), loggedUser);

                    }
                }
            }

        }

    }

    @Override
    public OFunctionResult assignHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        HolidayAssign holiday = (HolidayAssign) odm.getData().get(0);
        assignHolidayToEmployee(holiday, loggedUser);
        return ofr;
    }

    private void includeEmployees(String selectedEmployees, String selectedEmployeesJpql, List<String> employeeDbids, HolidayDates holiday, String IorE, OUser loggedUser) {

        Date from = holiday.getStartFrom();
        Date to = holiday.getEndTo();
        String yearStr = dtUtility.YEAR_FORMAT.format(from);
        Integer currentYear = Integer.parseInt(yearStr);

        List<String> udcConditions = new ArrayList<>();
        UDC holidayNature = null, absenceDayNature = null, vacationDayNaure = null;
        udcConditions.add("type.code='900'");
        udcConditions.add("code='Holiday'");
        try {
            holidayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        udcConditions.clear();
        udcConditions.add("type.code='900'");
        udcConditions.add("code='Absence'");
        try {
            absenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }

        udcConditions.clear();
        udcConditions.add("type.code='900'");
        udcConditions.add("code='Vacation'");
        try {
            vacationDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<String> vacationMethods = calendarService.getEmployeeVacations(from, to, currentYear);
        List<Date> vacationDates = calendarService.getHolidayDates(from, to);

        String updatePlannerStat = "";
        String updateShiftStat = "";
        String updateDAStat = "";
        List vacsDbids = getVacRequestsDbidsOnHoliday(from, to, selectedEmployees, loggedUser);

        Date vacStartDate, vacEndDate;
        // update periods
        int holidayPeriod = 0;
        float vacPeriod = 0;
        long vacReqDbid = 0, vacDbid = 0, empDbid = 0;
        Object[] objArr;
        String updateBalance, updatePeriod;
        Employee emp;
        Company company = holiday.getHoliday().getCompany();
        try {
            for (int i = 0; i < vacsDbids.size(); i++) {
                objArr = (Object[]) vacsDbids.get(i);
                vacReqDbid = Long.parseLong(objArr[0].toString());
                vacPeriod = Float.parseFloat(objArr[1].toString());
                vacDbid = Long.parseLong(objArr[2].toString());
                empDbid = Long.parseLong(objArr[3].toString());
                vacStartDate = dtUtility.DATE_FORMAT.parse(objArr[4].toString());
                vacEndDate = dtUtility.DATE_FORMAT.parse(objArr[5].toString());

                emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggedUser);
                holidayPeriod = getIntersectionDaysExcludingOffDays(from, to, vacStartDate, vacEndDate, emp, loggedUser);

                vacPeriod -= holidayPeriod;

                updatePeriod = "update employeedayoffrequest "
                        + "set period = " + vacPeriod
                        + " , actualPeriod = " + vacPeriod
                        + " , actualBalance = actualBalance + " + holidayPeriod
                        + " , balance = balance + " + holidayPeriod
                        + " , currentBalance = currentBalance - " + holidayPeriod
                        + "where dbid = " + vacReqDbid;
                oem.executeEntityUpdateNativeQuery(updatePeriod, loggedUser);

                // update all the following vacations' balances
                updateBalance = getUpdateStatForVacsFollowingHoliday(to, loggedUser);

                updateBalance = updateBalance.replaceAll("@holidayPeriod", holidayPeriod + "");
                updateBalance = updateBalance.replaceAll("@empDbid", empDbid + "");
                updateBalance = updateBalance.replaceAll("@vacDbid", vacDbid + "");
                oem.executeEntityUpdateNativeQuery(updateBalance, loggedUser);
            }
            long calculatedPeriodDbid;

            List<String> conds = new ArrayList<String>();
            conds.clear();
            conds.add("description = 'default_pay_method'");
            if (company != null) {
                conds.add("company.dbid = " + company.getDbid());
            }

            PayrollParameter parameter = null;
            try {
                parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            String paymentMethodDbid = "", paymentPeriodDbid = "";
            if (parameter != null) {
                paymentMethodDbid = parameter.getValueData();
            }

            conds.clear();
            conds.add("description = 'default_pay_period'");
            if (company != null) {
                conds.add("company.dbid = " + company.getDbid());
            }
            parameter = null;
            try {
                parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (parameter != null) {
                paymentPeriodDbid = parameter.getValueData();
            }
            String vacationCond;
            for (int j = 0; j < vacationMethods.size(); j++) {
                String[] temp = vacationMethods.get(j).split("D");
                String[] temp2 = temp[0].split("m");
                String month = temp2[1];
                String day = temp[1];

                // in case of absence
                updatePlannerStat = "update employeeannualplanner set " + vacationMethods.get(j) + "='H' "
                        + "where " + vacationMethods.get(j) + " <> 'O' "
                        + selectedEmployees
                        + " and " + vacationMethods.get(j) + " <> 'W'"
                        + " and " + vacationMethods.get(j) + " <> 'V' ";

                try {
                    oem.executeEntityUpdateNativeQuery(updatePlannerStat, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                // in case of vacation
                updatePlannerStat = "update employeeannualplanner set " + vacationMethods.get(j) + "='H' "
                        + " where " + vacationMethods.get(j) + " = 'V' and employee_dbid in "
                        + "(select dbid from oemployee where position_dbid in "
                        + "(select dbid from position where unit_dbid in "
                        + "(select dbid from unit "
                        + "where company_dbid = " + company.getDbid() + "))"
                        + " and dbid in (select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";

                updatePlannerStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                        + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                        + " ))"
                        + selectedEmployees;

                try {
                    oem.executeEntityUpdateNativeQuery(updatePlannerStat, loggedUser);

                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                updateDAStat = "update employeedailyattendance set daynature_dbid = " + holidayNature.getDbid()
                        + " where daynature_dbid in (select dbid from daynatures where type in( 'A','O')) "
                        + " and dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                        + selectedEmployees;
                calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                        paymentMethodDbid, paymentPeriodDbid, loggedUser);
                updateMonthlyConsolidationOnAddingHoliday(calculatedPeriodDbid, loggedUser, vacationDates.get(j),
                        absenceDayNature.getDbid(), "absenceDays", "", selectedEmployees);

                try {
                    oem.executeEntityUpdateNativeQuery(updateDAStat, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                // from vacation to holiday, so in monthly consolidation: increment holidays & decrement vacations
                updateDAStat = "update employeedailyattendance set daynature_dbid = " + holidayNature.getDbid()
                        + " where daynature_dbid in (select dbid from daynatures where type = 'V') "
                        + " and dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                        + selectedEmployees;
                vacationCond = " and employee_dbid in  (select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                        + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                        + " and aa.deleted = 0 and aa.cancelled = 'N'";

                vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                        + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(vacationDates.get(j)))
                        + " )" + selectedEmployees;

                updateDAStat += vacationCond;
                updateMonthlyConsolidationOnAddingHoliday(calculatedPeriodDbid, loggedUser, vacationDates.get(j),
                        vacationDayNaure.getDbid(), "vacations", vacationCond, selectedEmployees);
                try {
                    oem.executeEntityUpdateNativeQuery(updateDAStat, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            for (int j = 0; j < vacationMethods.size(); j++) {
                updateShiftStat = "update employeeshift eshift set eshift." + vacationMethods.get(j) + " = 'H'"
                        + " where eshift.planneryear in"
                        + " (select employeeannualplanner.planneryear from employeeannualplanner where "
                        + " employeeannualplanner.employee_dbid = eshift.employee_dbid and  employeeannualplanner."
                        + vacationMethods.get(j) + "<> 'O'" + selectedEmployees + ")"
                        + selectedEmployees;
                oem.executeEntityUpdateNativeQuery(updateShiftStat, loggedUser);
            }
            updateWorkingDaysForSelectedEmployeesOnAddingHoliday(from, to, selectedEmployeesJpql, IorE, holidayNature, loggedUser);

            for (int i = 0; i < employeeDbids.size(); i++) {
                addEmployeeHolidayRecords(vacationDates, holiday, String.valueOf(employeeDbids.get(i)), loggedUser);
            }
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private List getVacRequestsDbidsOnHoliday(Date holidayFrom, Date holidayTo, String employeeDbids, OUser loggedUser) {

        List vacRequests = new ArrayList();
        String sql = "select AA.dbid, AA.period, AA.vacation_dbid, AA.employee_dbid, AA.startDate, AA.endDate from employeedayoffrequest AA, dayoff BB "
                + " where AA.deleted = 0 and AA.cancelled = 'N' and AA.dayofftype = 'VACATION' and "
                + " BB.removeHoliday = 'Y' and AA.vacation_dbid = BB.dbid AND BB.dayofftype = 'VACATION'"
                + employeeDbids
                + " and ("
                + "     (AA.startDate >= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayFrom)) + " "
                + "     and AA.startDate <= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayTo)) + ")"
                + "     or (AA.endDate >= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayFrom)) + " "
                + "     and AA.endDate <= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayTo)) + ")"
                + "     or (AA.startDate< " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayFrom)) + " "
                + "     and AA.endDate > " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(holidayTo)) + ") "
                + ")";

        try {
            vacRequests = oem.executeEntityListNativeQuery(sql, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vacRequests;
    }

    private void updateMonthlyConsolidationOnAddingHoliday(long calculatedPeriodDbid, OUser loggedUser,
            Date currentDate, long natureDbid, String decrementedDaysName, String vacationCond, String employeeDbids) {
        String updateStat = "update employeemonthlyconsolidation set " + decrementedDaysName + "= " + decrementedDaysName + "- 1 , "
                + " holidays = holidays+1";
        if (decrementedDaysName.contains("absence")) {

            updateStat += ", postedAbsenceDays = "
                    + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                    + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                    + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                    + " and EmployeeWorkingCalHistory.cancelled = 0"
                    + " and EmployeeWorkingCalHistory.deleted = 0"
                    + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                    + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                    + " or currentEffectiveEndDate is null) and rownum=1 ) and rownum=1) * (absencedays) )"
                    + ", "
                    + " parametrizedAbsenceDays = "
                    + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                    + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                    + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                    + " and EmployeeWorkingCalHistory.cancelled = 0"
                    + " and EmployeeWorkingCalHistory.deleted = 0"
                    + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                    + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                    + " or currentEffectiveEndDate is null) and rownum=1 ) and rownum=1) * (absencedays) )";

        }
        updateStat += " where posted = 'N' and calculatedPeriod_dbid = " + calculatedPeriodDbid;
        updateStat += " and employee_dbid in (select employee_dbid from employeedailyattendance where dayNature_dbid = " + natureDbid;

        updateStat += " and employeedailyattendance.dailydate = "
                + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)) + " )";
        updateStat = updateStat.replaceAll("@date", dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)));
        updateStat += vacationCond;
        updateStat += employeeDbids;
        try {
            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private void addEmployeeHolidayRecords(List<Date> holidayDates, HolidayDates holiday, String employeeDbid, OUser loggedUser) {

        List<String> conds = new ArrayList<>();
        List<String> employeeHolidayConds = new ArrayList<>();
        conds.add("dbid=" + employeeDbid);
        employeeHolidayConds.add("employee.dbid=" + employeeDbid);
        try {

            Employee employee = (Employee) oem.loadEntity(Employee.class.getSimpleName(), conds, null, loggedUser);
            for (int i = 0; i < holidayDates.size(); i++) {

                employeeHolidayConds.add("holidayDate ='" + dtUtility.DATE_FORMAT.format(holidayDates.get(i)) + "'");
                EmployeeHoliday oldEmployeeHoliday = (EmployeeHoliday) oem.loadEntity(EmployeeHoliday.class.getSimpleName(), employeeHolidayConds, null, loggedUser);
                if (oldEmployeeHoliday == null) {
                    EmployeeHoliday employeeHoliday = new EmployeeHoliday();
                    employeeHoliday.setEmployee(employee);
                    employeeHoliday.setHolidayDate(holidayDates.get(i));
                    employeeHoliday.setHolidayDates(holiday);
                    oem.saveEntity(employeeHoliday, loggedUser);
                } else {
                    oldEmployeeHoliday.setHolidayDates(holiday);
                    oem.saveEntity(oldEmployeeHoliday, loggedUser);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateWorkingDaysForSelectedEmployeesOnAddingHoliday(Date from, Date to, String selectedEmployees, String IorE, UDC dayNature, OUser loggedUser) {
        try {

            String jpql = "select eda from EmployeeDailyAttendance eda where "
                    + " eda.dailyDate >= '" + dtUtility.DATE_FORMAT.format(from) + "' and"
                    + " eda.dailyDate <= '" + dtUtility.DATE_FORMAT.format(to) + "' "
                    + "" + selectedEmployees;
            List<EmployeeDailyAttendance> empDAs;
            empDAs = (List<EmployeeDailyAttendance>) oem.executeEntityListQuery(jpql, loggedUser);
            EmployeeDailyAttendance empDA;
            for (int i = 0; i < empDAs.size(); i++) {
                empDA = empDAs.get(i);
                String sql = "select count(*) from employeedayoffrequest edr , dayoff"
                        + " where edr.vacation_dbid = dayoff.dbid and edr.employee_dbid =" + empDA.getEmployee().getDbid()
                        + " and " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(empDA.getDailyDate()))
                        + " between edr.startdate and edr.endDate and edr.deleted = 0"
                        + " and cancelled = 'N' and dayoff.mission = 1 and dayoff.removeholiday = 'N'";
                Object obj = oem.executeEntityNativeQuery(sql, loggedUser);
                if (obj.toString().equals("0")) {
                    empDA.setDayNature(dayNature);
                }
                entitySetupService.callEntityUpdateAction(empDA, loggedUser);
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private String partitionEmployeeDbids(List<String> selectedEmployeesDbids, String IorE) {

        int partationSize = 999;
        List<List<String>> employeeDbidsPartation = new LinkedList<List<String>>();
        for (int i = 0; i < selectedEmployeesDbids.size(); i += partationSize) {
            employeeDbidsPartation.add(selectedEmployeesDbids.subList(i, Math.min(i + partationSize, selectedEmployeesDbids.size())));
        }
        String employeeDbids = " and (Employee_dbid" + IorE + " in(" + String.valueOf(employeeDbidsPartation.get(0).get(0));
        for (int i = 1; i < employeeDbidsPartation.get(0).size(); i++) {
            employeeDbids += ",";
            employeeDbids += String.valueOf(employeeDbidsPartation.get(0).get(i));
        }
        employeeDbids += ")";
        for (int i = 1; i < employeeDbidsPartation.size(); i++) {
            employeeDbids += " or Employee_dbid" + IorE + " in(" + String.valueOf(employeeDbidsPartation.get(i).get(0));
            for (int j = 1; j < employeeDbidsPartation.get(i).size(); j++) {

                employeeDbids += ",";
                employeeDbids += String.valueOf(employeeDbidsPartation.get(i).get(j));
            }
            employeeDbids += ") ";
        }
        employeeDbids += ")";
        return employeeDbids;
    }

    private String partitionEmployeeDbidsJpql(List<String> selectedEmployeesDbids, String IorE) {

        int partationSize = 999;
        List<List<String>> employeeDbidsPartation = new LinkedList<List<String>>();
        for (int i = 0; i < selectedEmployeesDbids.size(); i += partationSize) {
            employeeDbidsPartation.add(selectedEmployeesDbids.subList(i, Math.min(i + partationSize, selectedEmployeesDbids.size())));
        }
        String employeeDbids = " and (eda.employee.dbid" + IorE + " in(" + String.valueOf(employeeDbidsPartation.get(0).get(0));
        for (int i = 1; i < employeeDbidsPartation.get(0).size(); i++) {
            employeeDbids += ",";
            employeeDbids += String.valueOf(employeeDbidsPartation.get(0).get(i));
        }
        employeeDbids += ")";
        for (int i = 1; i < employeeDbidsPartation.size(); i++) {
            employeeDbids += " or eda.employee.dbid" + IorE + " in(" + String.valueOf(employeeDbidsPartation.get(i).get(0));
            for (int j = 1; j < employeeDbidsPartation.get(i).size(); j++) {

                employeeDbids += ",";
                employeeDbids += String.valueOf(employeeDbidsPartation.get(i).get(j));
            }
            employeeDbids += ") ";
        }
        employeeDbids += ") ";
        return employeeDbids;
    }

    @Override
    public OFunctionResult deleteEmployeeHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        Date from = null, to = null, tempDate = null;
        Company company = null;

        try {

            EmployeeHoliday employeeHoliday = (EmployeeHoliday) odm.getData().get(0);
            if (employeeHoliday != null && employeeHoliday.getHolidayDates() != null) {
                from = employeeHoliday.getHolidayDate();
                to = employeeHoliday.getHolidayDate();
                company = employeeHoliday.getHolidayDates().getHoliday().getCompany();
            }

            if (from == null || to == null) {
                ofr.addError(usrMsgService.getUserMessage("HolidayMissingDates", loggedUser));
                return ofr;
            }
//            String sql = "Select active from EmployeeHoliday where dbid=" + employeeHoliday.getDbid();
//            Object isActive = oem.executeEntityNativeQuery(sql, loggedUser);
            if (employeeHoliday.isInActive()) {

                String yearStr = dtUtility.YEAR_FORMAT.format(from);
                Integer currentYear = Integer.parseInt(yearStr);

                // check if there exists vacation requests intersecting with this holiday
                // if yes then either cancel it or modify it
                int holidayPeriod = 0;
                float vacPeriod = 0;
                long vacReqDbid = 0, vacDbid = 0, empDbid = 0;
                Object[] objArr;
                List vacsDbids = getVacRequestsDbidsOnHoliday(from, to, " and Employee_dbid=" + employeeHoliday.getEmployee().getDbid(), loggedUser);

                Date vacStartDate, vacEndDate;
                // update periods

                String updateBalance, updatePeriod;
                Employee emp;
                for (int i = 0; i < vacsDbids.size(); i++) {
                    objArr = (Object[]) vacsDbids.get(i);
                    vacReqDbid = Long.parseLong(objArr[0].toString());
                    vacPeriod = Float.parseFloat(objArr[1].toString());
                    vacDbid = Long.parseLong(objArr[2].toString());
                    empDbid = Long.parseLong(objArr[3].toString());
                    vacStartDate = dtUtility.DATE_FORMAT.parse(objArr[4].toString());
                    vacEndDate = dtUtility.DATE_FORMAT.parse(objArr[5].toString());

                    emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggedUser);
                    holidayPeriod = getIntersectionDaysExcludingOffDays(from, to, vacStartDate, vacEndDate, emp, loggedUser);

                    vacPeriod += holidayPeriod;
                    updatePeriod = "update employeedayoffrequest "
                            + "set period = " + vacPeriod
                            + " , actualBalance = actualBalance - " + holidayPeriod
                            + " , balance = balance - " + holidayPeriod
                            + " , currentBalance = currentBalance - " + holidayPeriod
                            + "where dbid = " + vacReqDbid;
                    oem.executeEntityUpdateNativeQuery(updatePeriod, loggedUser);

                    // update all the following vacations' balances
                    updateBalance = getUpdateStatForVacsFollowingHoliday(to, loggedUser);

                    updateBalance = updateBalance.replaceAll("@holidayPeriod", "-" + holidayPeriod);
                    updateBalance = updateBalance.replaceAll("@empDbid", empDbid + "");
                    updateBalance = updateBalance.replaceAll("@vacDbid", vacDbid + "");
                    oem.executeEntityUpdateNativeQuery(updateBalance, loggedUser);
                }
                UDC workingDayNature = null, workingOnHolidayNature = null, absenceDayNature = null, holidayDayNaure = null,
                        vacationDayNaure = null;
                List<String> udcConditions = new ArrayList<String>();
                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Working'");
                try {
                    workingDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='WorkingOnHoliday'");
                try {
                    workingOnHolidayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Absence'");
                try {
                    absenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Holiday'");
                try {
                    holidayDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Vacation'");
                try {
                    vacationDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (holidayDayNaure == null || workingDayNature == null || workingOnHolidayNature == null || absenceDayNature == null
                        || vacationDayNaure == null) {
                    ofr.addError(usrMsgService.getUserMessage("O-00700", loggedUser));
                    return ofr;
                }

                //TM effect
                List<String> conds = new ArrayList<String>();
                conds.clear();
                conds.add("description = 'default_pay_method'");
                conds.add("company.dbid = " + company.getDbid());

                PayrollParameter parameter = null;
                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                String paymentMethodDbid = "", paymentPeriodDbid = "";
                if (parameter != null) {
                    paymentMethodDbid = parameter.getValueData();
                }

                conds.clear();
                conds.add("description = 'default_pay_period'");
                conds.add("company.dbid = " + company.getDbid());
                parameter = null;
                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (parameter != null) {
                    paymentPeriodDbid = parameter.getValueData();
                }
                long startTimeInSec = System.currentTimeMillis();

                List<EmployeeDailyAttendance> loadedEmpDailyAttendanceLst = null;
                List<String> empDAConditions = new ArrayList<String>();

                String updateStat, dayField;
                long calculatedPeriodDbid;
                String vacationCond;
                List<Method> vacationMethods = getEmployeeVacations(from, to, currentYear);
                for (int j = 0; j < vacationMethods.size(); j++) {
                    String[] temp = vacationMethods.get(j).getName().split("D");
                    String[] temp2 = temp[0].split("M");
                    String month = temp2[1];
                    String day = temp[1];
                    dayField = "m" + month + "D" + day;

                    // on vacation
                    tempDate = dtUtility.DATE_FORMAT.parse(yearStr + "-" + month + "-" + day);
                    updateStat = "update EmployeeAnnualPlanner set " + dayField + " = 'V' "
                            + " where employee_dbid in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // update annualplanner
                    // on working
                    updateStat = "update  EmployeeAnnualPlanner  set " + dayField;
                    updateStat += "= 'W' where " + dayField + " = 'F' and employee_dbid =" + employeeHoliday.getEmployee().getDbid()
                            + " and planneryear = " + currentYear.toString();
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // on absence
                    updateStat = "update EmployeeAnnualPlanner set " + dayField;
                    updateStat += "= 'A' where " + dayField + " = 'H' and employee_dbid =" + employeeHoliday.getEmployee().getDbid()
                            + " and planneryear = " + currentYear.toString()
                            + " and employee_dbid not in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // Shift table - set the calendar that is in the DA record (normal working calendar)
                    updateStat = "update employeeshift set " + dayField;
                    updateStat += " = (select cast(code as varchar(10)) from normalworkingcalendar where dbid in "
                            + " (select workingcalendar_dbid from employeedailyattendance where employeeshift.employee_Dbid = employeedailyattendance.employee_dbid"
                            + " and employeedailyattendance.dailydate = ";
                    updateStat += dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate)) + "))";

                    updateStat += " where employeeshift.employee_dbid not in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";

                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                    updateStat += " and employee_dbid = " + employeeHoliday.getEmployee().getDbid();

                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // Shift table - in case of vacation
                    updateStat = "update employeeshift set " + dayField + " = 'V' ";
                    updateStat += " where employee_dbid in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                    updateStat += " and employee_dbid = " + employeeHoliday.getEmployee().getDbid();
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // <editor-fold defaultstate="collapsed" desc="update employeedailyattendance">
                    // 1 - from workingonholiday to working
                    updateStat = "update employeedailyattendance set daynature_dbid = " + workingDayNature.getDbid()
                            + " where employeedailyattendance.daynature_dbid = " + workingOnHolidayNature.getDbid();
                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                    updateStat += " and employee_dbid = " + employeeHoliday.getEmployee().getDbid();
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // 2 - from holiday to absence
                    updateStat = "update employeedailyattendance set daynature_dbid = " + absenceDayNature.getDbid()
                            + " where employeedailyattendance.daynature_dbid = " + holidayDayNaure.getDbid();
                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                    updateStat += " and employee_dbid = " + employeeHoliday.getEmployee().getDbid();

                    vacationCond = " and employee_dbid not in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";

                    vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    updateStat += vacationCond;
                    updateStat += " and employee_dbid = " + employeeHoliday.getEmployee().getDbid();
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // recalc consolidation to absence++ and holidaydays--
                    calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                            paymentMethodDbid, paymentPeriodDbid, loggedUser);
                    updateMonthlyConsolidationOnDeletingHoliday(calculatedPeriodDbid, loggedUser,
                            tempDate, absenceDayNature.getDbid(), "absenceDays", " and employee_dbid = " + Long.toString(employeeHoliday.getEmployee().getDbid()) + " ", vacationCond);
                    // 3 - from holiday to vacation
                    updateStat = "update employeedailyattendance set daynature_dbid = " + vacationDayNaure.getDbid()
                            + " where employeedailyattendance.daynature_dbid = " + holidayDayNaure.getDbid();

                    vacationCond = " and employee_dbid in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";

                    vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    updateStat += vacationCond;

                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                    updateStat += " and employee_dbid = " + employeeHoliday.getEmployee().getDbid();
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // recalc consolidation to vacations++ and holidaydays--
                    calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                            paymentMethodDbid, paymentPeriodDbid, loggedUser);
                    updateMonthlyConsolidationOnDeletingHoliday(calculatedPeriodDbid, loggedUser,
                            tempDate, vacationDayNaure.getDbid(), "vacations", " and employee_dbid = " + employeeHoliday.getEmployee().getDbid() + " ", vacationCond);
                    //<editor-fold defaultstate="collapsed" desc="update records in daily attendance for recalc if attendance exists">
                    empDAConditions.clear();
                    empDAConditions.add("dailyDate = '" + dtUtility.DATE_FORMAT.format(tempDate) + "'");
                    empDAConditions.add("timeIn is not null");
                    empDAConditions.add("employee.dbid=" + employeeHoliday.getEmployee().getDbid());
                    try {
                        loadedEmpDailyAttendanceLst = (List<EmployeeDailyAttendance>) oem.loadEntityList(EmployeeDailyAttendance.class.getSimpleName(),
                                empDAConditions, null, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    for (EmployeeDailyAttendance loadedEmpDailyAttendance : loadedEmpDailyAttendanceLst) {
                        loadedEmpDailyAttendance.setDayNature(workingDayNature);
                        entitySetupService.callEntityUpdateAction(loadedEmpDailyAttendance, loggedUser);
                    }
                    //</editor-fold>
                    // </editor-fold>

                    conds.clear();
                    conds.add("employee.dbid = " + employeeHoliday.getEmployee().getDbid());
                    conds.add("currentEffectiveDate <= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'");
                    conds.add("currentEffectiveEndDate >= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' OR entity.currentEffectiveEndDate is null");
                    EmployeeWorkingCalHistory empCalHistory = (EmployeeWorkingCalHistory) oem.loadEntity(EmployeeWorkingCalHistory.class.getSimpleName(), conds, null, loggedUser);
                    //empCalHistory.setCurrentEffectiveDate(tempDate);
                    //empCalHistory.setCurrentEffectiveEndDate(tempDate);
                    calendarService.calendarUpdate(ofr, empCalHistory, false, loggedUser, true);
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);

        }
        return ofr;
    }

    private void updateMonthlyConsolidationOnDeletingHoliday(long calculatedPeriodDbid, OUser loggedUser, Date currentDate,
            long natureDbid, String incrementedDaysName, String employeeDbid, String vacationCond) {
        String updateStat = "update employeemonthlyconsolidation set " + incrementedDaysName + "= " + incrementedDaysName + "+ 1 , "
                + " holidays = holidays-1";
        if (incrementedDaysName.contains("absence")) {

            updateStat += ", postedAbsenceDays = "
                    + " ((select  AbsenceFactor from TMRule where tmCalendar_dbid = (select  currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                    + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                    + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                    + " and EmployeeWorkingCalHistory.cancelled = 0"
                    + " and EmployeeWorkingCalHistory.deleted = 0"
                    + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                    + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                    + " or currentEffectiveEndDate is null) and rownum=1)and rownum=1) * (absencedays))"
                    + ", "
                    + " parametrizedAbsenceDays = "
                    + " ((select AbsenceFactor from TMRule where tmCalendar_dbid = (select  currentWorkingCal_dbid from EmployeeWorkingCalHistory"
                    + " where EmployeeWorkingCalHistory.employee_dbid = employeemonthlyconsolidation.employee_dbid "
                    + " and employeemonthlyconsolidation.calculatedPeriod_dbid = " + calculatedPeriodDbid
                    + " and EmployeeWorkingCalHistory.cancelled = 0"
                    + " and EmployeeWorkingCalHistory.deleted = 0"
                    + " and @date>=EmployeeWorkingCalHistory.currentEffectiveDate"
                    + " and (@date<=EmployeeWorkingCalHistory.currentEffectiveEndDate"
                    + " or currentEffectiveEndDate is null) and rownum=1 )and rownum=1) * (absencedays) )";

        }
        updateStat += "  where posted = 'N' and calculatedPeriod_dbid = " + calculatedPeriodDbid
                + " and employee_dbid in (select employee_dbid from employeedailyattendance where dayNature_dbid = " + natureDbid;
        updateStat += " and employeedailyattendance.dailydate = "
                + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)) + ")";
        updateStat = updateStat.replaceAll("@date", dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(currentDate)));
        updateStat += vacationCond;
        updateStat += employeeDbid;
        try {
            oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    @Override
    public OFunctionResult deleteAllHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        List<String> authorizedToEmployees = dataSecurityService.getEmployeeSecurityDbids(loggedUser);
        List<String> selectedEmployees = new ArrayList<String>();
        if (authorizedToEmployees.isEmpty()) {
            //load all employees
            String sql = "select cast (dbid as varchar(100)) from oemployee where active=1 ";
            try {
                authorizedToEmployees = (List<String>) oem.executeEntityListNativeQuery(sql, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);

            }
        }
        HolidayDates employeeHoliday = (HolidayDates) odm.getData().get(0);

        try {
            String sql = "Select cast(employee_dbid as varchar(100)) from employeeholiday where holidayDates_dbid = " + employeeHoliday.getDbid();
            selectedEmployees = oem.executeEntityListNativeQuery(sql, loggedUser);
            selectedEmployees.retainAll(authorizedToEmployees);
            deleteEmployeeHoliday(selectedEmployees, odm, params, loggedUser);

        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofr;
    }

    public OFunctionResult deleteEmployeeHoliday(List<String> employeeDbid, ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        Date from = null, to = null, tempDate = null;
        Company company = null;

        String employeeDbids = partitionEmployeeDbids(employeeDbid, "");

        try {

            HolidayDates holidayDates = (HolidayDates) odm.getData().get(0);
            if (holidayDates != null && holidayDates.getStartFrom() != null) {
                from = holidayDates.getStartFrom();
                to = holidayDates.getEndTo();
                company = holidayDates.getHoliday().getCompany();
            }

            if (from == null || to == null) {
                ofr.addError(usrMsgService.getUserMessage("HolidayMissingDates", loggedUser));
                return ofr;
            }
//            String sql = "Select active from HolidayDates where dbid=" + holidayDates.getDbid();
//            Object isActive = oem.executeEntityNativeQuery(sql, loggedUser);
            if (holidayDates.isInActive()) {

                String yearStr = dtUtility.YEAR_FORMAT.format(from);
                Integer currentYear = Integer.parseInt(yearStr);

                // check if there exists vacation requests intersecting with this holiday
                // if yes then either cancel it or modify it
                int holidayPeriod = 0;
                float vacPeriod = 0;
                long vacReqDbid = 0, vacDbid = 0, empDbid = 0;
                Object[] objArr;
                List vacsDbids = getVacRequestsDbidsOnHoliday(from, to, employeeDbids, loggedUser);

                Date vacStartDate, vacEndDate;
                // update periods

                String updateBalance, updatePeriod;
                Employee emp;
                for (int i = 0; i < vacsDbids.size(); i++) {
                    objArr = (Object[]) vacsDbids.get(i);
                    vacReqDbid = Long.parseLong(objArr[0].toString());
                    vacPeriod = Float.parseFloat(objArr[1].toString());
                    vacDbid = Long.parseLong(objArr[2].toString());
                    empDbid = Long.parseLong(objArr[3].toString());
                    vacStartDate = dtUtility.DATE_FORMAT.parse(objArr[4].toString());
                    vacEndDate = dtUtility.DATE_FORMAT.parse(objArr[5].toString());

                    emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggedUser);
                    holidayPeriod = getIntersectionDaysExcludingOffDays(from, to, vacStartDate, vacEndDate, emp, loggedUser);

                    vacPeriod += holidayPeriod;
                    updatePeriod = "update employeedayoffrequest "
                            + "set period = " + vacPeriod
                            + " , actualBalance = actualBalance - " + holidayPeriod
                            + " , balance = balance - " + holidayPeriod
                            + " , currentBalance = currentBalance - " + holidayPeriod
                            + "where dbid = " + vacReqDbid;
                    oem.executeEntityUpdateNativeQuery(updatePeriod, loggedUser);

                    // update all the following vacations' balances
                    updateBalance = getUpdateStatForVacsFollowingHoliday(to, loggedUser);

                    updateBalance = updateBalance.replaceAll("@holidayPeriod", "-" + holidayPeriod);
                    updateBalance = updateBalance.replaceAll("@empDbid", empDbid + "");
                    updateBalance = updateBalance.replaceAll("@vacDbid", vacDbid + "");
                    oem.executeEntityUpdateNativeQuery(updateBalance, loggedUser);
                }
                UDC workingDayNature = null, workingOnHolidayNature = null, absenceDayNature = null, holidayDayNaure = null,
                        vacationDayNaure = null;
                List<String> udcConditions = new ArrayList<String>();
                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Working'");
                try {
                    workingDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='WorkingOnHoliday'");
                try {
                    workingOnHolidayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Absence'");
                try {
                    absenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Holiday'");
                try {
                    holidayDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                udcConditions.clear();
                udcConditions.add("type.code='900'");
                udcConditions.add("code='Vacation'");
                try {
                    vacationDayNaure = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (holidayDayNaure == null || workingDayNature == null || workingOnHolidayNature == null || absenceDayNature == null
                        || vacationDayNaure == null) {
                    ofr.addError(usrMsgService.getUserMessage("O-00700", loggedUser));
                    return ofr;
                }

                //TM effect
                List<String> conds = new ArrayList<String>();
                conds.clear();
                conds.add("description = 'default_pay_method'");
                conds.add("company.dbid = " + company.getDbid());

                PayrollParameter parameter = null;
                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                String paymentMethodDbid = "", paymentPeriodDbid = "";
                if (parameter != null) {
                    paymentMethodDbid = parameter.getValueData();
                }

                conds.clear();
                conds.add("description = 'default_pay_period'");
                conds.add("company.dbid = " + company.getDbid());
                parameter = null;
                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (parameter != null) {
                    paymentPeriodDbid = parameter.getValueData();
                }
                long startTimeInSec = System.currentTimeMillis();

                List<EmployeeDailyAttendance> loadedEmpDailyAttendanceLst = null;
                List<String> empDAConditions = new ArrayList<String>();

                String updateStat, dayField;
                long calculatedPeriodDbid;
                String vacationCond;
                List<Method> vacationMethods = getEmployeeVacations(from, to, currentYear);
                for (int j = 0; j < vacationMethods.size(); j++) {
                    String[] temp = vacationMethods.get(j).getName().split("D");
                    String[] temp2 = temp[0].split("M");
                    String month = temp2[1];
                    String day = temp[1];
                    dayField = "m" + month + "D" + day;

                    // on vacation
                    tempDate = dtUtility.DATE_FORMAT.parse(yearStr + "-" + month + "-" + day);
                    updateStat = "update EmployeeAnnualPlanner set " + dayField + " = 'V' "
                            + " where employee_dbid in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // update annualplanner
                    // on working
                    updateStat = "update  EmployeeAnnualPlanner  set " + dayField;
                    updateStat += "= 'W' where " + dayField + " = 'F' " + employeeDbids
                            + " and planneryear = " + currentYear.toString();
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // on absence
                    updateStat = "update EmployeeAnnualPlanner set " + dayField;
                    updateStat += "= 'A' where " + dayField + " = 'H' " + employeeDbids
                            + " and planneryear = " + currentYear.toString()
                            + " and employee_dbid not in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // Shift table - set the calendar that is in the DA record (normal working calendar)
                    updateStat = "update employeeshift set " + dayField;
                    updateStat += " = (select cast(code as varchar(10)) from normalworkingcalendar where dbid in "
                            + " (select workingcalendar_dbid from employeedailyattendance where employeeshift.employee_Dbid = employeedailyattendance.employee_dbid"
                            + " and employeedailyattendance.dailydate = ";
                    updateStat += dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate)) + "))";

                    updateStat += " where employeeshift.employee_dbid not in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";

                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                    updateStat += employeeDbids;

                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // Shift table - in case of vacation
                    updateStat = "update employeeshift set " + dayField + " = 'V' ";
                    updateStat += " where employee_dbid in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";
                    updateStat += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";
                    updateStat += employeeDbids;
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // <editor-fold defaultstate="collapsed" desc="update employeedailyattendance">
                    // 1 - from workingonholiday to working
                    updateStat = "update employeedailyattendance set daynature_dbid = " + workingDayNature.getDbid()
                            + " where employeedailyattendance.daynature_dbid = " + workingOnHolidayNature.getDbid();
                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                    updateStat += employeeDbids;
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);

                    // 2 - from holiday to absence
                    updateStat = "update employeedailyattendance set daynature_dbid = " + absenceDayNature.getDbid()
                            + " where employeedailyattendance.daynature_dbid = " + holidayDayNaure.getDbid();
                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                    updateStat += employeeDbids;

                    vacationCond = " and employee_dbid not in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";

                    vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    updateStat += vacationCond;
                    updateStat += employeeDbids;
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // recalc consolidation to absence++ and holidaydays--
                    calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                            paymentMethodDbid, paymentPeriodDbid, loggedUser);
                    updateMonthlyConsolidationOnDeletingHoliday(calculatedPeriodDbid, loggedUser,
                            tempDate, absenceDayNature.getDbid(), "absenceDays", employeeDbids, vacationCond);
                    // 3 - from holiday to vacation
                    updateStat = "update employeedailyattendance set daynature_dbid = " + vacationDayNaure.getDbid()
                            + " where employeedailyattendance.daynature_dbid = " + holidayDayNaure.getDbid();

                    vacationCond = " and employee_dbid in "
                            + "(select employee_dbid from EMPLOYEEDAYOFFREQUEST aa "
                            + " where aa.VACATION_DBID in (select dbid from DAYOFF bb where bb.REMOVEHOLIDAY = 'Y' and bb.DAYOFFTYPE = 'VACATION')"
                            + " and aa.deleted = 0 and aa.cancelled = 'N'";

                    vacationCond += " and aa.STARTDATE<= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " and aa.ENDDATE>= " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate))
                            + " )";

                    updateStat += vacationCond;

                    updateStat += " and employeedailyattendance.dailydate = " + dtUtility.dateTimeFormatForOracle(dtUtility.DATE_FORMAT.format(tempDate));
                    updateStat += employeeDbids;
                    oem.executeEntityUpdateNativeQuery(updateStat, loggedUser);
                    // recalc consolidation to vacations++ and holidaydays--
                    calculatedPeriodDbid = getCalculatedPeriodOfThisDay(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(yearStr),
                            paymentMethodDbid, paymentPeriodDbid, loggedUser);
                    updateMonthlyConsolidationOnDeletingHoliday(calculatedPeriodDbid, loggedUser,
                            tempDate, vacationDayNaure.getDbid(), "vacations", employeeDbids, vacationCond);
                    //<editor-fold defaultstate="collapsed" desc="update records in daily attendance for recalc if attendance exists">
                    empDAConditions.clear();
                    empDAConditions.add("dailyDate = '" + dtUtility.DATE_FORMAT.format(tempDate) + "'");
                    empDAConditions.add("timeIn is not null");
                    empDAConditions.add(partitionEmployeeDbidsJpql(employeeDbid, ""));
                    try {
                        String jpql = "select eda from EmployeeDailyAttendance eda where "
                                + " eda.dailyDate >= '" + dtUtility.DATE_FORMAT.format(from) + "' and"
                                + " eda.dailyDate <= '" + dtUtility.DATE_FORMAT.format(to) + "' and"
                                + " eda.timeIn is not null " + partitionEmployeeDbidsJpql(employeeDbid, "");

                        loadedEmpDailyAttendanceLst = (List<EmployeeDailyAttendance>) oem.executeEntityListQuery(jpql, loggedUser);

                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    for (EmployeeDailyAttendance loadedEmpDailyAttendance : loadedEmpDailyAttendanceLst) {
                        loadedEmpDailyAttendance.setDayNature(workingDayNature);
                        entitySetupService.callEntityUpdateAction(loadedEmpDailyAttendance, loggedUser);
                    }

                    String sql = "Update employeeHoliday set deleted = 1 where holidaydates_dbid = " + holidayDates.getDbid();
                    oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                    //</editor-fold>
                    // </editor-fold>

                    for (int i = 0; i < employeeDbid.size(); i++) {
                        conds.clear();
                        conds.add("employee.dbid = " + employeeDbid.get(i));
                        conds.add("currentEffectiveDate <= '" + dtUtility.DATE_FORMAT.format(tempDate) + "'");
                        conds.add("currentEffectiveEndDate >= '" + dtUtility.DATE_FORMAT.format(tempDate) + "' OR entity.currentEffectiveEndDate is null");
                        EmployeeWorkingCalHistory empCalHistory = (EmployeeWorkingCalHistory) oem.loadEntity(EmployeeWorkingCalHistory.class.getSimpleName(), conds, null, loggedUser);
                        //empCalHistory.setCurrentEffectiveDate(tempDate);
                        //empCalHistory.setCurrentEffectiveEndDate(tempDate);
                        calendarService.calendarUpdate(ofr, empCalHistory, false, loggedUser, true);
                    }
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);

        }
        return ofr;
    }

    @Override
    public OFunctionResult validateReligionApplied(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        Holiday holiday = (Holiday) odm.getData().get(0);
        if (holiday.getReligionDependent().equals("Y") && holiday.getReligion() == null) {
            ofr.addError(usrMsgService.getUserMessage("You Must Select Religion", loggedUser));
            return ofr;
        }
        return ofr;
    }

    @Override
    public OFunctionResult validateHolidayDates(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        HolidayDates holidayDates = (HolidayDates) odm.getData().get(0);
        if (holidayDates.getStartFrom().after(holidayDates.getEndTo())) {
            ofr.addError(usrMsgService.getUserMessage("Invalide Dates", loggedUser));
            return ofr;
        }
        return ofr;
    }

    @Override
    public OFunctionResult validateDuplicateDates(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        HolidayDates holidayDates = (HolidayDates) odm.getData().get(0);

        List<String> conds = new ArrayList<>();
        conds.add("holiday.dbid = " + holidayDates.getHoliday().getDbid());
        conds.add("inActive = false");

        try {
            List<HolidayDates> selectedHolidayDates = (List<HolidayDates>) oem.loadEntityList(HolidayDates.class.getSimpleName(), conds, null, null, loggedUser);
            for (int i = 0; i < selectedHolidayDates.size(); i++) {
                if (selectedHolidayDates.get(i).getStartFrom().equals(holidayDates.getStartFrom())
                        && selectedHolidayDates.get(i).getEndTo().equals(holidayDates.getEndTo())) {
                    ofr.addError(usrMsgService.getUserMessage("Holiday With The Same Dates Already Exist", loggedUser));
                    return ofr;
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult validateInactiveHoliday(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        Holiday holiday = (Holiday) odm.getData().get(0);
        if (holiday.isInActive()) {
            List<String> conds = new ArrayList<>();
            conds.add("holiday.dbid = " + holiday.getDbid());

            List<HolidayDates> selectedHolidayDates;
            try {
                selectedHolidayDates = (List<HolidayDates>) oem.loadEntityList(HolidayDates.class.getSimpleName(), conds, null, null, loggedUser);

                if (!selectedHolidayDates.isEmpty()) {
                    ofr.addError(usrMsgService.getUserMessage("You Must Delete Holiday Dates First", loggedUser));
                    return ofr;
                }
                else
                {
                    holiday.setActive(false);
                    oem.saveEntity(holiday, loggedUser);
                }
            } catch (Exception ex) {
                Logger.getLogger(HolidayCalendarService.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
        return ofr;
    }
}
