package com.unitedofoq.otms.timemanagement;

import com.unitedofoq.droolspoc.drools.webservice.CalendarD;
import com.unitedofoq.droolspoc.drools.webservice.DroolsWebService_Service;
import com.unitedofoq.droolspoc.drools.webservice.EmployeeD;
import com.unitedofoq.droolspoc.drools.webservice.EmployeeDailyAttendanceD;
import com.unitedofoq.droolspoc.drools.webservice.OverTimeSegmentD;
import com.unitedofoq.fabs.core.datatype.ODataMessage;
import com.unitedofoq.fabs.core.datatype.ODataType;
import com.unitedofoq.fabs.core.entitybase.BaseEntity;
import com.unitedofoq.fabs.core.entitybase.OEntityManagerRemote;
import com.unitedofoq.fabs.core.entitysetup.EntitySetupServiceRemote;
import com.unitedofoq.fabs.core.function.OFunctionParms;
import com.unitedofoq.fabs.core.function.OFunctionResult;
import com.unitedofoq.fabs.core.log.OLog;
import com.unitedofoq.fabs.core.security.user.OUser;
import com.unitedofoq.fabs.core.udc.UDC;
import com.unitedofoq.fabs.core.usermessage.UserMessage;
import com.unitedofoq.fabs.core.usermessage.UserMessageServiceRemote;
import com.unitedofoq.otms.foundation.company.Company;
import com.unitedofoq.otms.foundation.employee.Employee;
import com.unitedofoq.otms.foundation.employee.EmployeeProfileHistory;
import com.unitedofoq.otms.foundation.unit.Unit;
import com.unitedofoq.otms.nursery.Nursery;
import com.unitedofoq.otms.payroll.CalculatedPeriod;
import com.unitedofoq.otms.payroll.PaymentMethod;
import com.unitedofoq.otms.payroll.PaymentPeriod;
import com.unitedofoq.otms.payroll.PayrollParameter;
import com.unitedofoq.otms.personnel.absence.EmployeeAbsenceRequest;
import com.unitedofoq.otms.personnel.dayoff.EmployeeDayOffRequest;
import com.unitedofoq.otms.personnel.penalty.EmployeePenaltyRequest;
import com.unitedofoq.otms.personnel.penalty.Penalty;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationAdjustment;
import com.unitedofoq.otms.personnel.vacation.EmployeeVacationRequest;
import com.unitedofoq.otms.personnel.vacation.Vacation;
import com.unitedofoq.otms.security.DataSecurityServiceLocal;
import com.unitedofoq.otms.timemanagement.employee.EmployeeAnnualPlanner;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendance;
import com.unitedofoq.otms.timemanagement.employee.EmployeeDailyAttendanceImport;
import com.unitedofoq.otms.timemanagement.employee.EmployeeMonthlyConsolidation;
import com.unitedofoq.otms.timemanagement.employee.EmployeeOTRImport;
import com.unitedofoq.otms.timemanagement.employee.EmployeeOTRequest;
import com.unitedofoq.otms.timemanagement.employee.EmployeeShift;
import com.unitedofoq.otms.timemanagement.employee.EmployeeWorkingCalHistory;
import com.unitedofoq.otms.timemanagement.employee.ManualDailyAttendance;
import com.unitedofoq.otms.timemanagement.workingcalendar.CalendarExceptionBase;
import com.unitedofoq.otms.timemanagement.workingcalendar.ChristianCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.DateCalendarException;
import com.unitedofoq.otms.timemanagement.workingcalendar.DaysCalendarException;
import com.unitedofoq.otms.timemanagement.workingcalendar.EarlyLeaveMandOTSegment;
import com.unitedofoq.otms.timemanagement.workingcalendar.HijriCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.HijriCalendarReflection;
import com.unitedofoq.otms.timemanagement.workingcalendar.NormalWorkingCalendar;
import com.unitedofoq.otms.timemanagement.workingcalendar.OTDateExc;
import com.unitedofoq.otms.timemanagement.workingcalendar.OTDayExc;
import com.unitedofoq.otms.timemanagement.workingcalendar.OTSegment;
import com.unitedofoq.otms.timemanagement.workingcalendar.OTSegmentBase;
import com.unitedofoq.otms.timemanagement.workingcalendar.TMCalendar;
import com.unitedofoq.otms.training.employee.EmployeeCourse;
import com.unitedofoq.otms.utilities.DateTimeUtility;
import com.unitedofoq.otms.utilities.audit.BusinessAuditBeanLocal;
import com.unitedofoq.otms.utilities.audit.EmployeeDailyAttendanceAudit;
import com.unitedofoq.otms.utilities.ejb.EntityUtilities;
import com.unitedofoq.otms.utilities.ejb.WSUtilityLocal;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.chrono.IslamicChronology;

@Stateless
@EJB(name = "java:global/ofoq/com.unitedofoq.otms.timemanagement.CalendarServiceLocal",
        beanInterface = CalendarServiceLocal.class)
public class CalendarService implements CalendarServiceLocal {

    @EJB
    private OEntityManagerRemote oem;
    @EJB
    private UserMessageServiceRemote usrMsgService;
    @EJB
    private EntitySetupServiceRemote entitySetupService;
    @EJB
    private CalendarServiceLocal calendarSer;
    @EJB
    private TimeManagementServiceLocal timeManagementService;
    @EJB
    private WSUtilityLocal webServiceUtility;
    @EJB
    private BusinessAuditBeanLocal audit;
    @EJB
    private DataSecurityServiceLocal dataSecurityServiceLocal;
    private DateTimeUtility dateTimeUtility = new DateTimeUtility();
    @EJB
    private HolidayCalendarServiceLocal HCSL;

    List approvedDayOTList;
    List approvedDayOTInList;
    List approvedDayOTOutList;
    List notApprovedDayOTList;
    List notApprovedDayOTInList;
    List notApprovedDayOTOutList;
    List approvedNightOTList;
    List approvedNightOTInList;
    List approvedNightOTOutList;
    List notApprovedNightOTList;
    List notApprovedNightOTInList;
    List notApprovedNightOTOutList;

    @Override
    public OFunctionResult insertIntoReflection(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            HijriCalendar hijriCalendar = (HijriCalendar) odm.getData().get(0);

            List<String> conditions = new ArrayList<String>();
            HijriCalendarReflection reflection = null;
            conditions.add("hijriCalendar.dbid=" + hijriCalendar.getDbid());
            reflection = (HijriCalendarReflection) oem.loadEntity(HijriCalendarReflection.class.getSimpleName(), conditions, null, loggedUser);

            boolean newReflection = false;
            if (reflection == null) {
                reflection = new HijriCalendarReflection();
                newReflection = true;
            }

            setHijriCalendarReflection(hijriCalendar, reflection);
            if (newReflection) {
                hijriCalendar.setReflection(reflection);
            } else {
                entitySetupService.callEntityUpdateAction(reflection, loggedUser);
            }

            return ofr;

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public OFunctionResult setPlannerForNonExist(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        try {

            //<editor-fold defaultstate="collapsed" desc="EmployeeStartDate class">
            class EmployeeStartDate {

                Employee employee;

                public Employee getEmployee() {
                    return employee;
                }

                public void setEmployee(Employee employee) {
                    this.employee = employee;
                }
                Date startDate;

                public Date getStartDate() {
                    return startDate;
                }

                public void setStartDate(Date startDate) {
                    this.startDate = startDate;
                }
                int day;

                public int getDay() {
                    return day;
                }

                public void setDay(int day) {
                    this.day = day;
                }
                int month;

                public int getMonth() {
                    return month;
                }

                public void setMonth(int month) {
                    this.month = month;
                }
                int year;

                public int getYear() {
                    return year;
                }

                public void setYear(int year) {
                    this.year = year;
                }
            }
            //</editor-fold>

            long startTimeInSec = System.currentTimeMillis();
            Integer size = 0;

            Calendar toDayCal = Calendar.getInstance();
            //toDayCal.setTime(dateTimeUtility.DATE_FORMAT.parse("2013-12-31"));

            Object tempObj = null;

            Date toDayDate = toDayCal.getTime();
            Date startDate = null;
            Date endDate = null;
            List<List<Date>> holidayDates = new ArrayList<List<Date>>();
            List<Date> regHolidayDates = new ArrayList<Date>();
            List<Date> christianHolidayDates = new ArrayList<Date>();

            //Integer currentYear = Integer.parseInt(dateTimeUtility.YEAR_FORMAT.format(toDayDate)) + 1;
            Integer currentYear = Integer.parseInt(dateTimeUtility.YEAR_FORMAT.format(toDayDate));
            Integer tempYear = currentYear;
            int startMonth = 1, startDay = 1, startYear = 2012;
            int calculatedPeriodMonths = 12;

            String endDateStr;
            String startDateStr;
            String endDay;
            String paymentPeriodDbid = "";
            String paymentMethodDbid = "";
            String[] startDateParts = new String[3];
            List<String> conds = new ArrayList<String>();

            List<Employee> employees = new ArrayList<Employee>();
            List<EmployeeStartDate> employeeStartDates = new ArrayList<EmployeeStartDate>();
            List<EmployeeAnnualPlanner> annualPlanners = new ArrayList<EmployeeAnnualPlanner>();

            List<Object[]> calendars = null;
            List<Object[]> christianCalendars = null;

            Map<Long, EmployeeShift> employeeShifts = new HashMap<Long, EmployeeShift>();
            Map<TMCalendar, List<Date>> regCalendarsMap = new HashMap<TMCalendar, List<Date>>();

            Company company = null;
            CalculatedPeriod calculatedPeriod = null;
            PaymentMethod paymentMethod = null;
            PaymentPeriod paymentPeriod = null;
            PayrollParameter parameter = null;
            EmployeeAnnualPlanner annualPlanner = null;
            EmployeeShift employeeShift = null;
            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            String employeeDbidStr = "";
            if (!employeeDbidList.isEmpty()) {
                employeeDbidStr = employeeDbidList.toString().replace("[", "(").replace("]", ")");
            }
            company = (Company) oem.loadEntity(Company.class.getSimpleName(), loggedUser.getUserPrefrence1(), null, null, loggedUser);

            if (company != null /*&& dateTimeUtility.DAYMONTH_FORMAT.format(toDayDate).equals("12-31")*/) { // runs every year on 31-12-yyyy

                Object tempDbid = null;
                try {
                    tempDbid = oem.executeEntityNativeQuery("select max(dbid) from employeedailyattendance", loggedUser);
                    if (tempDbid == null) {
                        dbid = 1;
                    } else {
                        dbid = Long.parseLong(tempDbid.toString());
                        dbid++;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                //<editor-fold defaultstate="collapsed" desc="insert calculated periods">
                conds.clear();
                conds.add("description = 'default_pay_method'");
                conds.add("company.dbid = " + company.getDbid());

                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (parameter != null) {
                    paymentMethodDbid = parameter.getValueData();
                }

                conds.clear();
                conds.add("description = 'default_pay_period'");
                conds.add("company.dbid = " + company.getDbid());
                parameter = null;
                try {
                    parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (parameter != null) {
                    paymentPeriodDbid = parameter.getValueData();
                }

                try {
                    paymentMethod = (PaymentMethod) oem.loadEntity(PaymentMethod.class.getSimpleName(), Collections.singletonList("dbid = " + paymentMethodDbid), null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    paymentPeriod = (PaymentPeriod) oem.loadEntity(PaymentPeriod.class.getSimpleName(), Collections.singletonList("dbid = " + paymentPeriodDbid), null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                for (int i = 1; i <= calculatedPeriodMonths; i++) {
                    conds.clear();
                    conds.add("month = " + i);
                    conds.add("year = " + currentYear);
                    conds.add("paymentMethod.dbid = " + paymentMethodDbid);
                    conds.add("payPeriod.dbid = " + paymentPeriodDbid);
                    try {
                        calculatedPeriod = (CalculatedPeriod) oem.loadEntity(CalculatedPeriod.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (calculatedPeriod == null) {

                        startDateStr = currentYear + "-" + i + "-01";

                        if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) {
                            endDay = "31";
                        } else if (i == 2) {
                            if (currentYear % 4 == 0) {
                                endDay = "29";
                            } else {
                                endDay = "28";
                            }
                        } else {
                            endDay = "30";
                        }
                        endDateStr = currentYear + "-" + i + "-" + endDay;

                        try {
                            startDate = dateTimeUtility.DATE_FORMAT.parse(startDateStr);
                            endDate = dateTimeUtility.DATE_FORMAT.parse(endDateStr);
                        } catch (ParseException ex) {
                            OLog.logException(ex, loggedUser);
                        }

                        calculatedPeriod = new CalculatedPeriod();
                        calculatedPeriod.setMonth(i);
                        calculatedPeriod.setYear(currentYear);
                        calculatedPeriod.setDescription("Monthly " + i + "-" + currentYear);
                        calculatedPeriod.setStartDate(startDate);
                        calculatedPeriod.setEndDate(endDate);
                        calculatedPeriod.setPayPeriod(paymentPeriod);
                        calculatedPeriod.setPaymentMethod(paymentMethod);

                        entitySetupService.callEntityCreateAction(calculatedPeriod, loggedUser);
                    }
                }
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="load employees">
                if (odm != null
                        && odm.getData() != null
                        && odm.getData().get(0).getClass().getSimpleName().equals("Unit")) {
                    Unit unit = (Unit) odm.getData().get(0);
                    conds.clear();
                    conds.add("positionSimpleMode.unit.dbid = " + unit.getDbid());
                    if (!employeeDbidStr.equals("")) {
                        conds.add("dbid in " + employeeDbidStr);
                    }
                    employees = oem.loadEntityList(Employee.class.getSimpleName(),
                            conds, null, null, loggedUser);
                } else if (odm != null
                        && odm.getData() != null
                        && odm.getData().get(0).getClass().getSimpleName().equals("Employee")) {
                    Employee employee = (Employee) odm.getData().get(0);
                    employees.add(employee);
                } else {
                    String sql = "Select dbid From oemployee "
                            + "where oemployee.deleted = 0 and oemployee.dbid not in "
                            + "(select employee_dbid from EmployeeAnnualPlanner where "
                            + " deleted = 0 and planneryear = " + currentYear + ")"
                            + " and year(oemployee.hiringdate) <= " + currentYear;

                    if (!employeeDbidStr.equals("")) {
                        sql += (" AND dbid in " + employeeDbidStr);
                    }
                    List<Object> employeeDbids = (List<Object>) oem.executeEntityListNativeQuery(sql, loggedUser);

                    employees = new ArrayList<Employee>();
                    Employee tempEmployee;
                    for (int i = 0; i < employeeDbids.size(); i++) {
                        System.out.println(employeeDbids.get(i).toString());
                        conds.clear();
                        conds.add("positionSimpleMode.unit.company.dbid = " + company.getDbid());
                        conds.add("dbid = " + employeeDbids.get(i).toString());
                        tempEmployee = (Employee) oem.loadEntity(Employee.class.getSimpleName(),
                                conds, null, loggedUser);
                        if (tempEmployee != null) {
                            employees.add(tempEmployee);
                        }
                    }
                }
                //</editor-fold>

                try {
                    startDate = dateTimeUtility.DATE_FORMAT.parse(currentYear.toString() + "-01-01");
                } catch (ParseException ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                class DateComparable implements Comparator<Object> {

                    @Override
                    public int compare(Object o1, Object o2) {
                        if ((((Date) o1).compareTo((Date) o2)) >= 0) {
                            return 0;
                        }
                        return 1;
                    }
                }
                String sqlStatement;
                String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);

                //<editor-fold defaultstate="collapsed" desc="set absence by default">
                String selectStat = "select dbid from employeeworkingcalhistory where "
                        + " cancelled = 0 and deleted = 0 and currentWorkingCal_dbid in "
                        + " (select dbid from tmcalendar where type_Dbid in (select dbid from udc where code = 'Rotation'"
                        + " or code = 'VariableShift'))"
                        + " and employee_dbid = ";
                for (int i = 0; i < employees.size(); i++) {

                    if (employees.get(i).getPayroll() == null) {
                        ofr.addError(usrMsgService.getUserMessage("TM_PayrollMissing",
                                Collections.singletonList(employees.get(i).getCode()), loggedUser));
                        continue;
                    }

                    conds.clear();
                    conds.add("plannerYear='" + currentYear.toString() + "'");
                    conds.add("employee.dbid=" + employees.get(i).getDbid());

                    try {
                        tempObj = oem.executeEntityNativeQuery("select dbid from employeeannualplanner where deleted = 0 and "
                                + " planneryear = '" + currentYear.toString() + "' "
                                + " and employee_dbid = " + employees.get(i).getDbid(), loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (tempObj == null) {
                        //the shift record

                        List rotShiftResultSet = oem.executeEntityListNativeQuery(selectStat + employees.get(i).getDbid(), loggedUser);
                        if (rotShiftResultSet != null && !rotShiftResultSet.isEmpty()) {

                            conds.clear();
                            conds.add("employee.dbid = " + employees.get(i).getDbid());
                            conds.add("plannerYear='" + currentYear.toString() + "'");
                            employeeShift = (EmployeeShift) oem.loadEntity(EmployeeShift.class.getSimpleName(), conds, null, loggedUser);

                            if (employeeShift == null) {
                                employeeShift = new EmployeeShift();
                                employeeShift.setEmployee(employees.get(i));
                                employeeShift.setPlannerYear(currentYear.toString());
                            }
                            employeeShifts.put(employees.get(i).getDbid(), employeeShift);
                        }

                        // annual planner record
                        annualPlanner = new EmployeeAnnualPlanner();
                        annualPlanner.setEmployee(employees.get(i));
                        annualPlanner.setPlannerYear(currentYear.toString());
                        try {
                            startDate = dateTimeUtility.DATE_FORMAT.parse(currentYear.toString() + "-01-01");
                        } catch (ParseException ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (startDate.compareTo(employees.get(i).getHiringDate()) < 0) {
                            startDate = employees.get(i).getHiringDate();
                        }

                        startDateParts = dateTimeUtility.DATE_FORMAT.format(startDate).split("-");
                        startMonth = Integer.parseInt(startDateParts[1]);
                        startDay = Integer.parseInt(startDateParts[2]);
                        startYear = Integer.parseInt(startDateParts[0]);

                        EmployeeStartDate tempEmployeeStartDate = new EmployeeStartDate();
                        tempEmployeeStartDate.setEmployee(employees.get(i));
                        tempEmployeeStartDate.setYear(startYear);
                        tempEmployeeStartDate.setMonth(startMonth);
                        tempEmployeeStartDate.setDay(startDay);
                        tempEmployeeStartDate.setStartDate(startDate);

                        employeeStartDates.add(tempEmployeeStartDate);

                        for (int j = startMonth; j <= 12; j++) {
                            for (int k = startDay; k <= 31; k++) {
                                if (j == 2 && k == 30) {
                                    break;
                                } else if ((j == 4 || j == 6 || j == 9 || j == 11) && k == 31) {
                                    break;
                                }

                                // Loubna - TM Porta - 06/02/2014
                                if (employees.get(i).getPayroll() != null && employees.get(i).getPayroll().getTmEntitled() != null
                                        && employees.get(i).getPayroll().getTmEntitled().equals("N")) {
                                    BaseEntity.setValueInEntity(annualPlanner, "m" + j + "D" + k, "W");
                                } else {
                                    BaseEntity.setValueInEntity(annualPlanner, "m" + j + "D" + k, "A");
                                }
                            }
                            startDay = 1;
                        }
                        annualPlanners.add(annualPlanner);
                    }
                }
                //</editor-fold>

                // add in the rotations map
                Map<Date, Long> empRotations = new HashMap<Date, Long>();

                String selectDayNatures = "select dbid, type from daynatures ";
                List dayNaturesLst = oem.executeEntityListNativeQuery(selectDayNatures, loggedUser);
                Map<String, Long> dayNaturesMap = new HashMap<String, Long>();;
                if (dayNaturesLst != null) {
                    Object[] dayNatureObjArr;
                    for (int i = 0; i < dayNaturesLst.size(); i++) {
                        dayNatureObjArr = (Object[]) dayNaturesLst.get(i);
                        dayNaturesMap.put(dayNatureObjArr[1].toString(), Long.parseLong(dayNatureObjArr[0].toString()));
                    }
                }
                List<Object[]> vacations = null;
                List<String> vacationMethods = new ArrayList<String>();
                for (int i = 0; i < employeeStartDates.size(); i++) {
                    OLog.logInformation("EmployeeAnnualPlanner", loggedUser, "employee.dbid", employees.get(i).getDbid(), i);
                    vacations = null;
                    vacationMethods = new ArrayList<String>();
                    if (employeeStartDates.get(i).getEmployee() != null) {

                        tempYear = employeeStartDates.get(i).getYear();
                        annualPlanners.get(i).setPlannerYear(tempYear.toString());
                        annualPlanners.get(i).setInActive(false);

                        //<editor-fold defaultstate="collapsed" desc="Load & Set vacations">
                        startDateStr = "2012-01-01";
                        if (employeeStartDates.get(i).getStartDate() != null) {
                            startDateStr = dateTimeUtility.DATE_FORMAT.format(employeeStartDates.get(i).getStartDate());
                        }

                        endDateStr = currentYear.toString() + "-12-31";
                        try {
                            startDate = dateTimeUtility.DATE_FORMAT.parse(startDateStr);
                            endDate = dateTimeUtility.DATE_FORMAT.parse(endDateStr);
                        } catch (ParseException ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            sqlStatement = "select AA.startDate,AA.endDate from EmployeeDayOffRequest AA ,DayOff BB Where  "
                                    + " AA.vacation_dbid = BB.dbid  and BB.unitofmeasure = 'D' and AA.cancelled = 'N' AND "
                                    + " AA.startDate>= to_Date('" + dateTimeUtility.DATE_FORMAT.format(startDate) + "','YYYY-MM-DD') AND "
                                    + " AA.endDate<= to_Date('" + dateTimeUtility.DATE_FORMAT.format(endDate) + "','YYYY-MM-DD') " + " And EMPLOYEE_DBID = " + employeeStartDates.get(i).getEmployee().getDbid();
                        } else {
                            sqlStatement = "select AA.startDate,AA.endDate from EmployeeDayOffRequest AA ,DayOff BB Where  "
                                    + " AA.vacation_dbid = BB.dbid  and BB.unitofmeasure = 'D' and AA.cancelled = 'N' AND "
                                    + " AA.startDate>= '" + dateTimeUtility.DATE_FORMAT.format(startDate) + "' AND "
                                    + " AA.endDate<= '" + dateTimeUtility.DATE_FORMAT.format(endDate) + "' And AA.EMPLOYEE_DBID = " + employeeStartDates.get(i).getEmployee().getDbid();
                        }

                        try {
                            vacations = oem.executeEntityListNativeQuery(sqlStatement, loggedUser);
                        } catch (Exception ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (vacations != null) {
                            for (int j = 0; j < vacations.size(); j++) {
                                vacationMethods.addAll(getEmployeeVacations(dateTimeUtility.DATE_FORMAT.parse(vacations.get(j)[0].toString()),
                                        dateTimeUtility.DATE_FORMAT.parse(vacations.get(j)[1].toString()), currentYear));
                            }
                        }

                        for (int j = 0; j < vacationMethods.size(); j++) {
                            String tmpDayNature;
                            if (isMissionDay(getDateFromPlanner(vacationMethods.get(j), annualPlanners.get(i).getPlannerYear()), annualPlanners.get(i).getEmployee(), loggedUser)) {
                                tmpDayNature = "M";
                            } else {
                                tmpDayNature = "V";
                            }
                            BaseEntity.setValueInEntity(annualPlanners.get(i), vacationMethods.get(j), tmpDayNature);
                            if (employeeShifts.get(employees.get(i).getDbid()) != null) {
                                BaseEntity.setValueInEntity(employeeShifts.get(employees.get(i).getDbid()), vacationMethods.get(j), tmpDayNature);
                            }
                        }
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Set Calendars">
                        List<EmployeeWorkingCalHistory> calHistories
                                = getEmployeeCalHistoriesWithinDates(employeeStartDates.get(i).getStartDate(), endDate,
                                        employeeStartDates.get(i).getEmployee(), loggedUser);
                        EmployeeWorkingCalHistory calHistory = null;

                        // if for any reason, the employee has no calendar history records,
                        // then create a record for him with his hiring date & hiring calendar
                        if (calHistories.isEmpty()) {
                            calHistory = new EmployeeWorkingCalHistory();
                            calHistory.setEmployee(employeeStartDates.get(i).getEmployee());
                            calHistory.setCurrentWorkingCal(employeeStartDates.get(i).getEmployee().getCalendar());
                            calHistory.setCurrentEffectiveDate(employeeStartDates.get(i).getEmployee().getHiringDate());
                            calHistory.setCurrentEffectiveEndDate(null);
                            calHistory.setCancelled(false);
                            calHistory.setDone(true);

                            Object[] returnedShft = new Object[2];
                            String rotSequence = "1";
                            if (employeeStartDates.get(i).getEmployee().getCalendar().getType().getCode().equals("Rotation")) {
                                returnedShft = getShift(employeeStartDates.get(i).getEmployee().getCalendar(),
                                        employeeStartDates.get(i).getEmployee().getHiringDate(), loggedUser);
                                if (returnedShft != null && returnedShft.length != 0 && returnedShft[0] != null) {
                                    rotSequence = returnedShft[0].toString();
                                }
                            }
                            calHistory.setRotSequence(rotSequence);
                            oem.saveEntity(calHistory, loggedUser);
                            calHistories.add(calHistory);
                        } else {
                            calHistory = calHistories.get(0);
                        }

                        //for (int j = 0; j < calHistories.size(); j++) {
                        int j = 0;
                        do {
                            calHistory = calHistories.get(j);

                            if (calHistory.getCurrentWorkingCal() != null
                                    && (calHistory.getCurrentWorkingCal().getType().getCode().equals("VariableShift")
                                    || calHistory.getCurrentWorkingCal().getType().getCode().equals("Rotation"))) {

                                employeeShift = employeeShifts.get(employeeStartDates.get(i).getEmployee().getDbid());
                                employeeShift.setPlannerYear(tempYear.toString());
                            }
                            startDate = employeeStartDates.get(i).getStartDate();

                            Date currStartDate = calHistory.getCurrentEffectiveDate();
                            Date currEndDate = calHistory.getCurrentEffectiveEndDate();
                            if (currEndDate == null) {
                                currEndDate = endDate;
                            }
                            //<editor-fold defaultstate="collapsed" desc="Regular">
                            if (calHistory.getCurrentWorkingCal() != null
                                    && calHistory.getCurrentWorkingCal().getType() != null
                                    && (calHistory.getCurrentWorkingCal().getType().getCode().equals("Regular")
                                    || calHistory.getCurrentWorkingCal().getType().getCode().equals("Flexible"))) {

                                fillRegularCalMapWithOffdaysDates(loggedUser, regCalendarsMap,
                                        calHistory.getCurrentWorkingCal(), currentYear);
                                List<Date> tempCalDates = regCalendarsMap.get(calHistory.getCurrentWorkingCal());

                                if (currStartDate.compareTo(startDate) >= 0) {
                                    startDate = currStartDate;
                                }
                                int dateListIndex = 0;
                                if (tempCalDates != null) {
                                    for (int k = 0; k < tempCalDates.size(); k++) {
                                        if (tempCalDates.get(k).compareTo(startDate) >= 0) {
                                            dateListIndex = k;
                                            break;
                                        }
                                    }
                                    if (dateListIndex < 0) {
                                        dateListIndex = 0;
                                    }

                                    if (tempCalDates != null) {
                                        for (int index = dateListIndex; index < tempCalDates.size() && !tempCalDates.get(index).after(currEndDate); index++) {
                                            try {

                                                DateTime dt = new DateTime(tempCalDates.get(index), dateTimeUtility.dateTimeZone);

                                                BaseEntity.setValueInEntity(annualPlanners.get(i), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "O");
                                            } catch (Exception ex) {
                                                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                        }
                                    }
                                }
                            }
                            //</editor-fold>
                            //<editor-fold defaultstate="collapsed" desc="Rotation">
                            if (calHistory.getCurrentWorkingCal() != null
                                    && calHistory.getCurrentWorkingCal().getType() != null
                                    && calHistory.getCurrentWorkingCal().getType().getCode().equals("Rotation")) {

                                TMCalendar rotationCal = calHistory.getCurrentWorkingCal();
                                Date empDate = employeeStartDates.get(i).getStartDate();
                                if (currStartDate.compareTo(empDate) >= 0) {
                                    empDate = currStartDate;
                                }
                                Calendar tempCal = Calendar.getInstance();
                                tempCal.setTime(empDate);
                                if (rotationCal != null) {
                                    empRotations.clear();

                                    int roundDuration = 0,
                                            numRounds = 0,
                                            numRoundsRem = 0;
                                    Map<Integer, NormalWorkingCalendar> rotCalsMap = new HashMap<Integer, NormalWorkingCalendar>();

                                    String select = "select SUM(numberOfDays) from normalworkingcalendar where "
                                            + " deleted = 0 and calendar_dbid=" + rotationCal.getDbid();
                                    Object ret = oem.executeEntityNativeQuery(select, loggedUser);

                                    if (ret != null) {

                                        roundDuration = Integer.parseInt(ret.toString());
                                        numRounds = (dateTimeUtility.getDaysBetween(empDate, currEndDate) + 1) / roundDuration;
                                        numRoundsRem = (dateTimeUtility.getDaysBetween(empDate, currEndDate) + 1) % roundDuration;

                                        Object[] returned = getShift(rotationCal, empDate, loggedUser);
                                        if (returned != null && returned.length != 0) {
                                            if (returned[1] == null) {
                                                ofr.addError(usrMsgService.getUserMessage("WrongCalRetrieval", loggedUser));
                                                return ofr;
                                            }

                                            // fill map of calendars for each day
                                            List<String> sort = new ArrayList<String>();
                                            sort.add("shiftSequence");

                                            conds.clear();
                                            conds.add("calendar.dbid = " + rotationCal.getDbid());

                                            List<NormalWorkingCalendar> rotShfts = oem.loadEntityList(NormalWorkingCalendar.class.getSimpleName(), conds, null, sort, loggedUser);
                                            setCalendarsMap(rotCalsMap, rotShfts, roundDuration);

                                            employeeShift = employeeShifts.get(employeeStartDates.get(i).getEmployee().getDbid());

                                            String code = null,
                                                    dayName = null;

                                            int rDur = (Integer) returned[0];
                                            //numRoundsRem += (rDur + 1); /* add to the remaining days; the days missed in the first rotation*/
                                            numRoundsRem += rDur;

                                            tempCal = Calendar.getInstance();
                                            tempCal.setTime(empDate);
                                            int tempDur;

                                            for (int r = 0; r < numRounds; r++) {
                                                for (; rDur < roundDuration; rDur++) {
                                                    if (rDur == 0) {
                                                        tempDur = roundDuration - 1;
                                                    } else {
                                                        tempDur = rDur - 1;
                                                    }

                                                    code = rotCalsMap.get(tempDur).getCode();
                                                    empDate = tempCal.getTime();
                                                    tempCal.add(Calendar.DAY_OF_MONTH, 1);
                                                    dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();

                                                    Object dayNature = BaseEntity.getValueFromEntity(employeeShift, dayName);
                                                    String strDayNature = dayNature != null ? dayNature.toString() : null;
                                                    if (strDayNature != null && strDayNature.equals("H")) {
                                                        continue;
                                                    }

                                                    BaseEntity.setValueInEntity(employeeShift, dayName, code);
                                                    if (rotCalsMap.get(tempDur).getNumberOfHours() == null
                                                            || (rotCalsMap.get(tempDur).getNumberOfHours() != null
                                                            && rotCalsMap.get(tempDur).getNumberOfHours().compareTo(BigDecimal.ZERO) == 0)) {
                                                        BaseEntity.setValueInEntity(annualPlanners.get(i), dayName, "O");
                                                    }
                                                    empRotations.put(empDate, rotCalsMap.get(tempDur).getDbid());
                                                }
                                                rDur = 0;
                                            }

                                            // remaining days for the last round
                                            for (int r = 0; r < numRoundsRem; r++) {
                                                int index = r % (roundDuration);
                                                if (index == 0) {
                                                    index = roundDuration - 1;
                                                } else {
                                                    index = index - 1;
                                                }
                                                if (rotCalsMap.get(index) == null) {
                                                    continue;
                                                }
                                                code = rotCalsMap.get(index).getCode();
                                                empDate = tempCal.getTime();
                                                tempCal.add(Calendar.DAY_OF_MONTH, 1);
                                                dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();

                                                Object dayNature = BaseEntity.getValueFromEntity(employeeShift, dayName);
                                                String strDayNature = dayNature != null ? dayNature.toString() : null;
                                                if (strDayNature != null && strDayNature.equals("H")) {
                                                    continue;
                                                }

                                                BaseEntity.setValueInEntity(employeeShift, dayName, code);
                                                if (rotCalsMap.get(index).getNumberOfHours() == null
                                                        || (rotCalsMap.get(index).getNumberOfHours() != null
                                                        && rotCalsMap.get(index).getNumberOfHours().compareTo(BigDecimal.ZERO) == 0)) {
                                                    BaseEntity.setValueInEntity(annualPlanners.get(i), dayName, "O");
                                                }
                                                empRotations.put(empDate, rotCalsMap.get(rDur).getDbid());
                                            }

                                        }
                                    }
                                    //empRotationCals.put(employeeStartDates.get(i).getEmployee().getDbid(), empRotations);
                                }

                            }
                            //</editor-fold>
//                            entitySetupService.callEntityUpdateAction(calHistory, loggedUser);
                            j++;
                        } while (j < calHistories.size());
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="load holidays">
                        try {
                            sqlStatement = "select dateFrom,dateTo "
                                    + " FROM HolidayCalendar "
                                    + " WHERE company_dbid = " + company.getDbid() + " AND ";
                            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                sqlStatement += " dateFrom >= To_Date('" + currentYear + "-01-01" + "','YYYY-MM-DD') AND "
                                        + " dateFrom <= To_Date('" + currentYear + "-12-31" + "','YYYY-MM-DD')  ";
                            } else {
                                sqlStatement += " dateFrom >= '" + currentYear + "-01-01" + "' AND "
                                        + " dateFrom <= '" + currentYear + "-12-31" + "'  ";
                            }
                            try {
                                calendars = oem.executeEntityListNativeQuery(sqlStatement, loggedUser);
                            } catch (Exception ex) {
                                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            Date from;
                            Date to;
                            for (int jj = 0; calendars != null && jj < calendars.size(); jj++) {
                                from = dateTimeUtility.DATE_FORMAT.parse(calendars.get(jj)[0].toString());
                                to = dateTimeUtility.DATE_FORMAT.parse(calendars.get(jj)[1].toString());
                                regHolidayDates.addAll(getHolidayDates(from, to));
                            }
                            holidayDates.add(regHolidayDates);

//                    hijriCalendars = (List<HijriCalendar>) oem.loadEntityList(HijriCalendar.class.getSimpleName(), conds, null, null, loggedUser);
//                    for (int j = 0; hijriCalendars != null && j < hijriCalendars.size(); j++) {
//                        HijriCalendarReflection tempReflection = new HijriCalendarReflection();
//                        setHijriCalendarReflection(hijriCalendars.get(j), tempReflection);
//                        hijriCalendarsRef.add(tempReflection);
//                        from = hijriCalendarsRef.get(j).getGregorianFrom();
//                        to = hijriCalendarsRef.get(j).getGregorianTo();
//                        hijriHolidayDates.addAll(getHolidayDates(from, to));
//                    }
//                    holidayDates.add(hijriHolidayDates);
                        } catch (Exception ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Set Holidays">
                        if (holidayDates != null) {
                            Collections.sort(holidayDates.get(0));
                            int holidayDateListIndex = 0;
                            for (int jj = 0; jj < holidayDates.get(0).size(); jj++) {
                                if (holidayDates.get(0).get(jj).compareTo(startDate) >= 0) {
                                    holidayDateListIndex = jj;
                                    break;
                                }
                                holidayDateListIndex = holidayDates.get(0).size();
                            }
                            if (holidayDateListIndex < 0) {
                                holidayDateListIndex = 0;
                            }
                            for (int jj = holidayDateListIndex; jj < holidayDates.get(0).size(); jj++) {
                                try {
                                    DateTime dt = new DateTime(holidayDates.get(0).get(jj), dateTimeUtility.dateTimeZone);

                                    BaseEntity.setValueInEntity(annualPlanners.get(i), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                    if (employeeShifts.get(employees.get(i).getDbid()) != null) {
                                        BaseEntity.setValueInEntity(employeeShifts.get(employees.get(i).getDbid()), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                    }
                                } catch (Exception ex) {
                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }

                            UDC employeeReligion = null;
                            UDC chrisitianReligion = null;

                            List<String> udcConditions = new ArrayList<String>();
                            udcConditions.add("type.code='prs_019'");
                            udcConditions.add("code='CHR'");
                            try {
                                chrisitianReligion = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                            } catch (Exception ex) {
                                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            if (employeeStartDates.get(i).getEmployee() != null && employeeStartDates.get(i).getEmployee() != null) {
                                employeeReligion = employeeStartDates.get(i).getEmployee().getReligion();
                            }
                            if (employeeReligion != null && employeeReligion.getCode().equals(chrisitianReligion.getCode())) {

                                for (int jj = 0; jj < holidayDates.get(1).size(); jj++) {
                                    if (holidayDates.get(1).get(jj).compareTo(startDate) >= 0) {
                                        holidayDateListIndex = jj;
                                        break;
                                    }
                                    holidayDateListIndex = holidayDates.get(1).size();
                                }
                                if (holidayDateListIndex < 0) {
                                    holidayDateListIndex = 0;
                                }
                                for (int jj = holidayDateListIndex; jj < holidayDates.get(1).size(); jj++) {
                                    try {

                                        DateTime dt = new DateTime(holidayDates.get(1).get(jj), dateTimeUtility.dateTimeZone);

                                        BaseEntity.setValueInEntity(annualPlanners.get(i), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                        if (employeeShifts.get(employees.get(i).getDbid()) != null) {
                                            BaseEntity.setValueInEntity(employeeShifts.get(employees.get(i).getDbid()), "m" + dt.getMonthOfYear() + "D" + dt.getDayOfMonth(), "H");
                                        }
                                    } catch (Exception ex) {
                                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }
                        //</editor-fold>
                        // call createDAForSingleEmployee with the planner record, calhisory records
                        createSingleEmpDA(annualPlanners.get(i), empRotations, dayNaturesMap,
                                employeeStartDates.get(i).getStartDate(), endDate, calHistories, loggedUser);

                    }

                    try {
                        oem.saveEntity(annualPlanners.get(i), loggedUser);
                        if (!employeeShifts.isEmpty()) {
                            oem.saveEntity(employeeShifts.get(annualPlanners.get(i).getEmployee().getDbid()), loggedUser);
                        }
                    } catch (Exception e) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, e);
                    }
                    updateEmployeeVacationRequest(annualPlanners.get(i).getEmployee(), startDate, endDate, loggedUser);
                }
            }
            size = annualPlanners.size();
            long endTimeInSec = System.currentTimeMillis();

            System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
            System.out.println("&&&&&&& Time for " + size + " employees planners is " + ((endTimeInSec - startTimeInSec)) + " m. seconds &&&&&&&");
            System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
            ofr.addSuccess(usrMsgService.getUserMessage("PlannerInsertedSuccessfully", Collections.singletonList(size.toString()), loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        } finally {
            return ofr;
        }
    }

    private Date getDateFromPlanner(String value, String year) {
        value = value.toLowerCase();
        Calendar res = Calendar.getInstance();
        int month = Integer.parseInt(value.substring(value.indexOf("m") + 1, value.indexOf("d")));
        int day = Integer.parseInt(value.substring(value.indexOf("d") + 1));
        res.set(Integer.parseInt(year), month - 1, day);
        return res.getTime();
    }

    @Override
    public OFunctionResult setDAForNonExist(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Calendar todayCal = Calendar.getInstance();
            Date todayDate = todayCal.getTime();

            Integer currentYear = Integer.parseInt(dateTimeUtility.YEAR_FORMAT.format(todayDate));
            Integer comingMonth = Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(todayDate));

            comingMonth = (comingMonth + 1) % 13;
            //comingMonth ++;

            Company company = null;

            List<Long> employeeDbids = new ArrayList<Long>();

            company = (Company) oem.loadEntity(Company.class.getSimpleName(), loggedUser.getUserPrefrence1(), null, null, loggedUser);
            if (company != null) {
                //<editor-fold defaultstate="collapsed" desc="load employees">
                if (odm.getData().get(0).getClass().getSimpleName().equals("Unit")) {
                    Unit unit = (Unit) odm.getData().get(0);
                    employeeDbids = (List<Long>) oem.executeEntityListNativeQuery("Select oemployee.dbid From oemployee ,position"
                            + "where oemployee.deleted = 0 and oemployee.position_dbid = position.dbid and position.unit_dbid = " + unit.getDbid(), loggedUser);
                } else if (odm.getData().get(0).getClass().getSimpleName().equals("Employee")) {
                    Employee employee = (Employee) odm.getData().get(0);
                    employeeDbids.add(new Long((int) employee.getDbid()));
                } else {
                    /* New part - Reviewed - 22042014
                     ******* Start *******
                     */
                    List<EmployeeAnnualPlanner> planners = (List<EmployeeAnnualPlanner>) params.getParams().get(1);
                    Map<Long, Map<Date, Long>> empRotCals = (Map<Long, Map<Date, Long>>) params.getParams().get(2);
                    createDA(planners, empRotCals, currentYear.toString(), loggedUser);
                    /*
                     ******* End *******
                     */
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult updateAnnualPlanner(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        List<String> plannerConditions = new ArrayList<String>();
        Object arglist[] = new Object[1];

        Employee employee = null;
        EmployeeAnnualPlanner annualPlanner = null;
        EmployeeShift employeeShift = null;
        EmployeeDailyAttendance dailyAttendance = null;

        if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeDailyAttendance")) {
            dailyAttendance = (EmployeeDailyAttendance) odm.getData().get(0);
            employee = dailyAttendance.getEmployee();
            Date dailyDate = dailyAttendance.getDailyDate();
            DateTime dailyDateDT = new DateTime(dailyDate, dateTimeUtility.dateTimeZone);

            try {
                Integer currentYear = dailyDateDT.getYear();
                plannerConditions.add("plannerYear='" + currentYear.toString() + "'");
                plannerConditions.add("employee.dbid=" + employee.getDbid());
                annualPlanner = (EmployeeAnnualPlanner) oem.loadEntity(EmployeeAnnualPlanner.class.getSimpleName(), plannerConditions, null, loggedUser);
                employeeShift = (EmployeeShift) oem.loadEntity(EmployeeShift.class.getSimpleName(), plannerConditions, null, loggedUser);

                if (annualPlanner != null) {
                    if (dailyAttendance.getTimeIn() != null) {
                        if (dailyAttendance.getDayNature().getCode().equals("WorkingOnWeekEnd")) {
                            BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "E");
                        } else if (dailyAttendance.getDayNature().getCode().equals("WorkingOnHoliday")) {
                            BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "F");
                        } else {
                            BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "W");
                        }
                    }
                    if (dailyAttendance.getDayNature().getCode().equals("Absence")) {
                        BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "A");
                    } else if (dailyAttendance.getDayNature().getCode().equals("WeekEnd")) {
                        BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "O");
                    } else if (dailyAttendance.getDayNature().getCode().equals("Holiday")) {
                        BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "H");
                    } else if (dailyAttendance.getDayNature().getCode().equals("Mission")) {
                        BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "M");
                        BaseEntity.setValueInEntity(employeeShift, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "M");
                        oem.saveEntity(employeeShift, loggedUser);
                    } else if (dailyAttendance.getDayNature().getCode().equals("Vacation")) {
                        BaseEntity.setValueInEntity(annualPlanner, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "V");
                        BaseEntity.setValueInEntity(employeeShift, "m" + dailyDateDT.getMonthOfYear() + "D" + dailyDateDT.getDayOfMonth(), "V");
                        oem.saveEntity(employeeShift, loggedUser);
                    }
                    entitySetupService.callEntityUpdateAction(annualPlanner, loggedUser);
                }
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeVacationRequest")) {
            EmployeeVacationRequest vacationRequest = (EmployeeVacationRequest) odm.getData().get(0);
            ofr.append(applyVacationRequestOnTM(vacationRequest, loggedUser));

        } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeAbsenceRequest")) {
            EmployeeAbsenceRequest absenceRequest = (EmployeeAbsenceRequest) odm.getData().get(0);
            employee = absenceRequest.getEmployee();
            if (absenceRequest.getAbsence().getUnitOfMeasure().equals("H")) {
                DateFormat df = new SimpleDateFormat("HH:mm");
                String exceptionFrom = df.format(absenceRequest.getStartDate());
                String exceptionTo = df.format(absenceRequest.getEndDate());

                List<String> empDAConditions = new ArrayList<String>();
                EmployeeDailyAttendance loadedEmpDailyAttendance = null;
                empDAConditions.add("employee.dbid = " + employee.getDbid());
                empDAConditions.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(absenceRequest.getStartDate()) + "'");
                empDAConditions.add("header = true");
                try {
                    loadedEmpDailyAttendance = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), empDAConditions, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (loadedEmpDailyAttendance == null) {
                } else {
                    loadedEmpDailyAttendance.setExceptionFrom(exceptionFrom);
                    loadedEmpDailyAttendance.setExceptionTo(exceptionTo);
                    entitySetupService.callEntityUpdateAction(loadedEmpDailyAttendance, loggedUser);
                }
            }
        } else if (odm.getData().get(0).getClass().getSimpleName().equals("EmployeeCourse")) {
            EmployeeCourse employeeCourse = (EmployeeCourse) odm.getData().get(0);
            employee = employeeCourse.getEmployee();

            DateTime startDT = new DateTime(employeeCourse.getCourseInformation().getStartDate(), dateTimeUtility.dateTimeZone);

            List<String> enrolledConds = new ArrayList<String>();
            enrolledConds.add("type.code = 'EmployeeCourse'");
            enrolledConds.add("code = '4.A'");
            UDC enrolledStatus = null;
            try {
                enrolledStatus = (UDC) oem.loadEntity(UDC.class.getSimpleName(), enrolledConds, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (enrolledStatus != null) {
                if (employeeCourse.getStatus().getCode().equals(enrolledStatus.getCode())) {
                    Integer currentYear = startDT.getYear();
                    Employee emp = null;
                    emp = employeeCourse.getEmployee();
                    if (emp != null) {
                        List<String> trainingMethods = null;

                        trainingMethods = getEmployeeVacations(employeeCourse.getCourseInformation().getStartDate(),
                                employeeCourse.getCourseInformation().getEndDate(), currentYear);

                        plannerConditions.add("plannerYear='" + currentYear.toString() + "'");
                        plannerConditions.add("employee.dbid=" + employee.getDbid());
                        try {
                            annualPlanner = (EmployeeAnnualPlanner) oem.loadEntity(EmployeeAnnualPlanner.class.getSimpleName(), plannerConditions, null, loggedUser);
                        } catch (Exception ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        List<String> udcConditions = new ArrayList<String>();
                        UDC dayNature = null;
                        udcConditions.add("type.code='900'");
                        udcConditions.add("code='Training'");

                        try {
                            dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
                        } catch (Exception ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (annualPlanner != null) {
                            Class types[] = new Class[1];
                            types[0] = Void.class;
                            for (int j = 0; j < trainingMethods.size(); j++) {
                                String[] temp = trainingMethods.get(j).toLowerCase().split("d");
                                String[] temp2 = temp[0].toLowerCase().split("m");
                                String month = temp2[1];
                                String day = temp[1];

                                Object dayValue = null;
                                try {
                                    dayValue = BaseEntity.getValueFromEntity(annualPlanner, trainingMethods.get(j));
                                } catch (Exception ex) {
                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                String dayVal = (String) dayValue;
                                if (!dayVal.equals("O") && !dayVal.equals("H")) {
                                    arglist = new Object[1];
                                    arglist[0] = "T";
                                    try {
                                        BaseEntity.setValueInEntity(annualPlanner, trainingMethods.get(j), "T");
                                    } catch (Exception e) {
                                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, e);
                                    }
                                }

                                //<editor-fold defaultstate="collapsed" desc="insert or update records in daily attendance with vacation nature">
                                String currentDateStr = currentYear + "-" + month + "-" + day;
                                Date currentDate = null;
                                try {
                                    currentDate = dateTimeUtility.DATE_FORMAT.parse(currentDateStr);
                                } catch (ParseException ex) {
                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                List<String> empDAConditions = new ArrayList<String>();
                                EmployeeDailyAttendance loadedEmpDailyAttendance = null;
                                empDAConditions.add("employee.dbid = " + employee.getDbid());
                                empDAConditions.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(currentDate) + "'");
                                empDAConditions.add("header = true");
                                try {
                                    loadedEmpDailyAttendance = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), empDAConditions, null, loggedUser);
                                } catch (Exception ex) {
                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                if (loadedEmpDailyAttendance != null) {
                                    loadedEmpDailyAttendance.setDayNature(dayNature);
                                    entitySetupService.callEntityUpdateAction(loadedEmpDailyAttendance, loggedUser);
                                }
                                //</editor-fold>
                            }
                            entitySetupService.callEntityUpdateAction(annualPlanner, loggedUser);
                        }
                    }
                }
            }
        }
        return ofr;
    }

    @Override
    public OFunctionResult attendanceChangeAction(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            approvedDayOTList = new ArrayList();
            approvedDayOTInList = new ArrayList();
            approvedDayOTOutList = new ArrayList();
            notApprovedDayOTList = new ArrayList();
            notApprovedDayOTInList = new ArrayList();
            notApprovedDayOTOutList = new ArrayList();
            approvedNightOTList = new ArrayList();
            approvedNightOTInList = new ArrayList();
            approvedNightOTOutList = new ArrayList();
            notApprovedNightOTList = new ArrayList();
            notApprovedNightOTInList = new ArrayList();
            notApprovedNightOTOutList = new ArrayList();

            boolean multiEntry = false;
            String payrollParameterDsc;
            String[] payrollParameterResult;
            payrollParameterDsc = "TM_MultiEntryEnabled";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                if (payrollParameterResult[0].equalsIgnoreCase("y")) {
                    multiEntry = true;
                }
            }
            NormalWorkingCalendar normalCal = null;
            TMCalendar tmCalendar = null;
            DaysCalendarException dailyExp = null;
            DateCalendarException dateExp = null;
            Rule timeManagementRule = null;
            Company company = null;
            EmployeeMonthlyConsolidation consolidation = null;
            Employee emp = null;
            EmployeeDailyAttendance empDA = null,
                    loadedEmpDA = null;
            EmployeeShift employeeShift = null;
            Integer cutOffStart = null, cutOffEnd = null;
            CalculatedPeriod calculatedPeriod = null;

            Date entryDate = null, outDate = null;

            String calcCurrentMonth = null;
            String calcFullMonth = null;
            String timeIn = null;
            String timeOut = null;
            String breakIn = null;
            String breakOut = null;
            String otEntit = "N";
            String displayTimeIn = null;
            String displaytimeOut = null;
            String displayBreakOut = null;
            String planIn = "00:00";
            String planOut = "00:00";
            String displayPlanOut = "00:00";
            String planBreakIn = "00:00";
            String planBreakOut = "00:00";
            String tolerence = "00:00";
            String earlyLeaveTolerence = "00:00";
            String delay = "00:00";
            String earlyLeave = "00:00";
            String notApprovedDayOT = "00:00";
            String notApprovedNightOT = "00:00";
            String nonPlanned = "00:00";
            String lessWork = "00:00";
            String actualWork = "00:00";
            String plannedWork = "00:00";
            String exceptionHours = "00:00";
            String approvedDayOT = "00:00";
            String approvedNightOT = "00:00";
            String holidayOrWeekendApprovedOT = "00:00";
            String holidayOrWeekendNotApprovedOT = "00:00";
            String dayCondition = "";
            String startDayTime = null;// An attribute to include start day time in Normal WC Entity
            String minOT = "00:00";
            String expectedOut = "-";
            String dayOTIn = "00:00";
            String dayOTOut = "00:00";
            String nightOTIn = "00:00";
            String nightOTOut = "00:00";
            /* a new attribute to handle the flexible calendar with planned in & out (like Pico case)
             it is initilaized to "-" so as to check later if it is "-" then it will be the old flexible with only # of hrs specified
             */

            EmpCalData calData = null;

            List<String> conds = new ArrayList<String>();
            List<String> sort = new ArrayList<String>();
            List<String> requestedFrom = null;
            List<String> requestedTo = null;
            List<String> exceptionFrom = new ArrayList<String>();
            List<String> exceptionTo = new ArrayList<String>();

            List<String> tempCalData = null;

            List<TMEntry> entries = new ArrayList<TMEntry>();

            List<OTSegment> segments = null;
            List<OTDateExc> dateExcs = null;
            List<OTDayExc> dayExcs = null;
            List<EmployeeOTRequest> employeeOTRequests = null;
            List<EmployeeDailyAttendance> empDADetails = new ArrayList<EmployeeDailyAttendance>();
            List<OTSegment> mandatorySegs = null;
            List<EarlyLeaveMandOTSegment> earlyLeaveMandOTSegs = null;
            //List<EmployeeDailyMission> dailyMissions = null;
            List<EmployeeVacationRequest> empMissions = null;

            boolean isReversed = false;
            boolean outOfScope = false; // flag to state whether the emp came & left out of planned hrs,
            // if yes; then the day is absence & OT is calculated through night or day
            boolean empOTCalc = true;
            boolean workingOnHoliday = false;
            boolean workingOnWeekend = false;

            int dayOfWeek;

            BigDecimal mandatoryOT;

            UDC workingDayNature = null;
            UDC calType = null;

            empDA = (EmployeeDailyAttendance) odm.getData().get(0);
            if ((empDA.getTimeIn() != null || !empDA.getTimeIn().equals("00:00")) && (empDA.getTimeOut() != null || !empDA.getTimeOut().equals("00:00"))) {
                empDA = testDrools(empDA, loggedUser);
                oem.saveEntity(empDA, loggedUser);
                ofr.setReturnedDataMessage(odm);
                return ofr;
            }
            if (empDA.getWorkingCalendar().getCalendar().getMaximumWeekEndOT() == null || empDA.getWorkingCalendar().getCalendar().getMaximumWeekEndOT().equals("0")) {
                empDA.getWorkingCalendar().getCalendar().setMaximumWeekEndOT("24:00");
            }

            if (empDA.getWorkingCalendar().getCalendar().getMaximumHolidayOT() == null || empDA.getWorkingCalendar().getCalendar().getMaximumHolidayOT().equals("0")) {
                empDA.getWorkingCalendar().getCalendar().setMaximumHolidayOT("24:00");
            }

            emp = empDA.getEmployee();
            dayOfWeek = getCurrentDayOfWeek(empDA.getDailyDate());
            tmCalendar = getEmpCalendar(empDA.getDailyDate(), emp, loggedUser);
            calType = tmCalendar.getType();// to be changed **!!**
            company = emp.getPositionSimpleMode().getUnit().getCompany();

            // <editor-fold defaultstate="collapsed" desc="validations">
            if (odm.getODataType().getName().equals("EntityChangeField")) {
                String fldExp = odm.getData().get(1).toString();
                Object newVal = odm.getData().get(2);

                timeIn = fldExp.startsWith("timeIn") ? newVal.toString() : empDA.getTimeIn();
                timeOut = fldExp.startsWith("timeOut") ? newVal.toString() : empDA.getTimeOut();

                breakIn = fldExp.startsWith("breakIn") ? newVal.toString() : empDA.getBreakIn();
                breakOut = fldExp.startsWith("breakOut") ? newVal.toString() : empDA.getBreakOut();

                if (timeIn != null && !timeIn.equals("")) {
                    if (fldExp.startsWith("timeIn") && !dateTimeUtility.validateTimeFormat(timeIn)) {
                        ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Time In"), loggedUser));
                        return ofr;
                    }
                }

                if (timeOut != null && !timeOut.equals("")) {
                    if (fldExp.startsWith("timeOut") && !dateTimeUtility.validateTimeFormat(timeOut)) {
                        ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Time Out"), loggedUser));
                        return ofr;
                    }
                }

                if (breakIn != null && !breakIn.equals("")) {
                    if (fldExp.startsWith("breakIn") && !dateTimeUtility.validateTimeFormat(breakIn)) {
                        if (!breakIn.equals("")) {
                            ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Break In"), loggedUser));
                            return ofr;
                        }
                    }
                }

                if (breakOut != null && !breakOut.equals("")) {
                    if (fldExp.startsWith("breakOut") && !dateTimeUtility.validateTimeFormat(breakOut)) {
                        if (!breakOut.equals("")) {
                            ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Break Out"), loggedUser));
                            return ofr;
                        }
                    }
                }
            } else {
                timeIn = empDA.getTimeIn();
                timeOut = empDA.getTimeOut();

                breakIn = empDA.getBreakIn();
                breakOut = empDA.getBreakOut();

                if (timeIn != null && !timeIn.equals("")) {
                    //Customization for Suez Cement
                    if (!dateTimeUtility.validateTimeFormat(timeIn)) {
                        empDA.setTimeIn(timeIn);
//                        ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Time In"), loggedUser));
                        return ofr;
                    }
                }

                if (timeOut != null && !timeOut.equals("")) {
                    //TOFIX: handle the null pointer exception
                    if (!dateTimeUtility.validateTimeFormat(timeOut)) {
                        empDA.setTimeOut(timeOut);
//                        ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Time Out"), loggedUser));
                        return ofr;
                    }
                }

                if (breakIn != null && !breakIn.equals("")) {
                    if (!dateTimeUtility.validateTimeFormat(breakIn)) {
                        if (!breakIn.equals("")) {
                            ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Break In"), loggedUser));
                            return ofr;
                        }
                    }
                }

                if (breakOut != null && !breakOut.equals("")) {
                    if (!dateTimeUtility.validateTimeFormat(breakOut)) {
                        if (!breakOut.equals("")) {
                            ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Break Out"), loggedUser));
                            return ofr;
                        }
                    }
                }
            }

            if (empDA.getDailyDate() == null) {
                return ofr;
            }

            /**
             * ******************
             */
//            if (empDA.getDailyDate().compareTo(Calendar.getInstance().getTime()) > 0) {
//                ofr.addError(usrMsgService.getUserMessage("FutureDate", loggedUser));
//                return ofr;
//            }
//            if ((timeIn == null || (timeIn != null && timeIn.equals(""))) && (timeOut != null || (timeOut != null && !timeOut.equals("")))) {
//                ofr.addError(usrMsgService.getUserMessage("TimeOutWithNoTimeIn", loggedUser));
//                return ofr;
//            }
//            if (timeOut != null && !timeOut.equals("")) {
//                if (dateTimeUtility.compareTime(timeIn, timeOut) > 0) {
//                    ofr.addError(usrMsgService.getUserMessage("TimeInMustBeLessThanTimeOut", Collections.singletonList("Break Out"), loggedUser));
//                    return ofr;
//                }
//            }
            if ((breakIn != null && !breakIn.equals("")) && (breakOut != null && !breakOut.equals(""))) {
                if (dateTimeUtility.compareTime(breakIn, breakOut) < 0) {
                    boolean breakInTimeInOut = insideInterval(breakIn, breakOut, timeIn, timeOut);
                    if (!breakInTimeInOut) {
                        ofr.addError(usrMsgService.getUserMessage("BreakNotInTheWorkingHours", Collections.singletonList("Break Out"), loggedUser));
                        return ofr;
                    }
                } else {
                    ofr.addError(usrMsgService.getUserMessage("BreakInMustBeLessThanBreakOut", Collections.singletonList("Break Out"), loggedUser));
                    return ofr;
                }
            }
            // </editor-fold>

            timeIn = unifyTimeValue(timeIn);
            timeOut = unifyTimeValue(timeOut);
            breakIn = unifyTimeValue(breakIn);
            breakOut = unifyTimeValue(breakOut);

            displaytimeOut = timeOut;
            displayBreakOut = breakOut;
            displayTimeIn = timeIn;

            entryDate = empDA.getDailyDate();
            outDate = empDA.getOutDate();
            //EPH for eligibility
            EmployeeProfileHistory eph = new EmployeeProfileHistory();
            String ephQuery = "select dbid from employeeprofilehistory where employee_dbid=" + empDA.getEmployee().getDbid()
                    + " and actiondate <=" + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()))
                    + " order by actiondate desc";
            List<Object> ephDBID = oem.executeEntityListNativeQuery(ephQuery, loggedUser);
            List<String> conditions = new ArrayList<>();
            if (!ephDBID.isEmpty()) {
                conditions.add("dbid=" + ephDBID.get(0).toString());
                eph = (EmployeeProfileHistory) oem.loadEntity(EmployeeProfileHistory.class.getSimpleName(), conditions, null, loggedUser);
            }
            // load the daily missions for this day
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
            String s;
            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                s = "select startdate, enddate from employeedayoffrequest "
                        + " where employee_Dbid = " + empDA.getEmployee().getDbid()
                        + " and deleted = 0 and cancelled = 'N' and vacation_dbid in (select dbid from dayoff where unitofmeasure = 'H' and deleted = 0)"
                        + " and DAYOFFTYPE = 'VACATION' and  to_date(startdate) >= to_date('" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "','YYYY-MM-dd')"
                        + " and  to_date(trunc(enddate))  <= to_date('" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "','YYYY-MM-dd')";
            } else if (dataBaseConnectionString.toLowerCase().contains("mysql")) {
                s = "select startdate, enddate from employeedayoffrequest "
                        + " where employee_Dbid = " + empDA.getEmployee().getDbid()
                        + " and deleted = 0 and cancelled = 'N' and vacation_dbid in (select dbid from dayoff where unitofmeasure = 'H' and deleted = 0)"
                        + " and DAYOFFTYPE = 'VACATION' and startdate <= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'"
                        + " and enddate >= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'";
            } else {
                s = "select startdate, enddate from employeedayoffrequest "
                        + " where employee_Dbid = " + empDA.getEmployee().getDbid()
                        + " and deleted = 0 and cancelled = 'N' and vacation_dbid in (select dbid from dayoff where unitofmeasure = 'H' and deleted = 0)"
                        + " and DAYOFFTYPE = 'VACATION' and cast (startdate as date) <= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'"
                        + " and cast (enddate as date) >= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'";
            }
            List<Object[]> results = (List<Object[]>) oem.executeEntityListNativeQuery(s, loggedUser);
            DateFormat df = new SimpleDateFormat("HH:mm");
            for (Object[] result : results) {
                exceptionFrom.add(df.format(result[0]));
                exceptionTo.add(df.format(result[1]));
            }

            //<editor-fold defaultstate="collapsed" desc="not posted month">
            Integer calcPeriodMonth = Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(empDA.getDailyDate()));
            Integer calcPeriodDay = Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(empDA.getDailyDate()));
            Integer calcPeriodYear = Integer.parseInt(dateTimeUtility.YEAR_FORMAT.format(empDA.getDailyDate()));

            // loading the cut off from payrollparameter
            payrollParameterDsc = "day_of_calculation";
            String cuttOffDay1 = "0", cuttOffDay2 = "0";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                cuttOffDay1 = payrollParameterResult[0];
                cuttOffDay2 = payrollParameterResult[1];
            }
            //calcCurrentMonth
            payrollParameterDsc = "calculation_currentMonth";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                calcCurrentMonth = payrollParameterResult[0];
            }

            //calcFullMonth
            payrollParameterDsc = "calculation_fullMonth";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                calcFullMonth = payrollParameterResult[0];
            }
            if (calcFullMonth != null && calcFullMonth.equals("Y")) { // from 1->30
                cutOffStart = 1;
                cutOffEnd = dateTimeUtility.getLastDayInMonth(empDA.getDailyDate());

                if (calcCurrentMonth != null && calcCurrentMonth.equals("N")) { // the prev month
                    cutOffEnd = dateTimeUtility.getLastDayInMonth(calcPeriodYear, calcPeriodMonth);
                    if (calcPeriodMonth == 12) {
                        calcPeriodMonth = 1;
                        calcPeriodYear++;
                    } else {
                        calcPeriodMonth++;
                    }
                }
            } else if ((calcFullMonth != null && calcFullMonth.equals("N"))
                    || (calcFullMonth == null)) {
                cutOffStart = Integer.parseInt(cuttOffDay1) + 1;
                cutOffEnd = Integer.parseInt(cuttOffDay2) + 1;
                if (calcPeriodDay >= cutOffStart) {
                    calcPeriodMonth = (calcPeriodMonth + 1) % 12;
                    if (calcPeriodMonth == 0) {
                        calcPeriodMonth = 12;
                    }
                    if (calcPeriodMonth == 1) {
                        calcPeriodYear++;
                    }
                }
            }

            if (empDA.getMonthlyConsolidation() != null) {
                consolidation = empDA.getMonthlyConsolidation();

                if (empDA.getMonthlyConsolidation().getPosted().equals("Y")) {
                    List<String> usrMsgParams = new ArrayList<String>();
                    usrMsgParams.add(consolidation.getCalculatedPeriod().getDescription());
                    usrMsgParams.add(consolidation.getEmployee().getCode());
                    // ofr.addError(usrMsgService.getUserMessage("AlreadyPosted", usrMsgParams, loggedUser));
                    //return ofr;
                }
            } else {
                conds.clear();
                conds.add("employee.dbid = " + empDA.getEmployee().getDbid());
                conds.add("calculatedPeriod.month = " + calcPeriodMonth);
                conds.add("calculatedPeriod.year = " + calcPeriodYear);

                payrollParameterDsc = "default_pay_method";
                payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
                if (payrollParameterResult != null) {
                    conds.add("calculatedPeriod.paymentMethod.dbid=" + payrollParameterResult[0]);
                }

                List<EmployeeMonthlyConsolidation> consolidations = (List<EmployeeMonthlyConsolidation>) oem.loadEntityList(
                        EmployeeMonthlyConsolidation.class.getSimpleName(), conds, null, null, loggedUser);
                if (consolidations != null && !consolidations.isEmpty()) {
                    consolidation = consolidations.get(0);
                }
                if (consolidation != null && consolidation.getPosted().equals("Y")) {
                    List<String> usrMsgParams = new ArrayList<String>();
                    usrMsgParams.add(consolidation.getCalculatedPeriod().getDescription());
                    usrMsgParams.add(consolidation.getEmployee().getCode());
                    // to be removed!!!!
                    //ofr.addError(usrMsgService.getUserMessage("AlreadyPosted", usrMsgParams, loggedUser));
                    //return ofr;
                }
                empDA.setMonthlyConsolidation(consolidation);
            }
            //</editor-fold>

            String originalDelayValue = null, originalEarlyLeaveValue = null, originalLessWorkValue = null;

            conds.clear();
            conds.add("employee.dbid = " + emp.getDbid());
            conds.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'");
            if (multiEntry) {
                conds.add("header = true");
            }
            try {
                loadedEmpDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);
                originalDelayValue = loadedEmpDA.getDelayHoursDisplay();
                originalEarlyLeaveValue = loadedEmpDA.getEarlyLeaveDisplay();
                originalLessWorkValue = loadedEmpDA.getLessWorkDisplay();
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            payrollParameterDsc = "tm_OTEntitlement";
            payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
            if (payrollParameterResult != null) {
                otEntit = payrollParameterResult[0];
                if (otEntit == null) {
                    otEntit = "N";
                }
                if (otEntit.equals("Y")) {
                    if (!checkOverTimeEntitlement(emp.getDbid(), loggedUser)) {
                        empOTCalc = false;
                    }
                }
            }
            switch (dayOfWeek) {
                case 1:
                    dayCondition = "001";
                    break;
                case 2:
                    dayCondition = "002";
                    break;
                case 3:
                    dayCondition = "003";
                    break;
                case 4:
                    dayCondition = "004";
                    break;
                case 5:
                    dayCondition = "005";
                    break;
                case 6:
                    dayCondition = "006";
                    break;
                case 7:
                    dayCondition = "007";
                    break;
            }

            if (timeIn != null && !timeIn.equals("") && !empDA.getDayNature().getCode().equals("Mission")) {
                //<editor-fold defaultstate="collapsed" desc="Load WorkingDayNature UDC">
                conds.clear();
                conds.add("type.code='900'");
                conds.add("code='Working'");
                if (empDA.getDayNature() != null && empDA.getDayNature().getCode() != null) {
                    if (empDA.getDayNature().getCode().equals("WeekEnd")
                            || empDA.getDayNature().getCode().equals("WorkingOnWeekEnd")) {
                        conds.clear();
                        conds.add("type.code='900'");
                        conds.add("code='WorkingOnWeekEnd'");
                        workingOnWeekend = true;
                    } else if (empDA.getDayNature().getCode().equals("Holiday")
                            || empDA.getDayNature().getCode().equals("WorkingOnHoliday")) {
                        conds.clear();
                        conds.add("type.code='900'");
                        conds.add("code='WorkingOnHoliday'");
                        workingOnHoliday = true;
                    }
                }

                try {
                    workingDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                //</editor-fold>
            } else {
                workingDayNature = empDA.getDayNature();
            }
            boolean needCalenarSegments = true;

            if (timeIn != null && !empDA.getDayNature().getCode().equals("Mission")) {
                if (calType.getCode().equals("Regular") || calType.getCode().equals("Flexible")) {
                    conds.clear();
                    conds.add("calendar.dbid = " + tmCalendar.getDbid());
                    normalCal = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), conds, null, loggedUser);
                }

                if (calType.getCode().equals("VariableShift") || calType.getCode().equals("Rotation")) {
                    String currentYear = dateTimeUtility.YEAR_FORMAT.format(empDA.getDailyDate());
                    String currentMonth = dateTimeUtility.MONTH_FORMAT.format(empDA.getDailyDate());
                    String currentDay = dateTimeUtility.DAY_FORMAT.format(empDA.getDailyDate());

                    conds.clear();
                    conds.add("employee.dbid = " + emp.getDbid());
                    conds.add("plannerYear = '" + currentYear.toString() + "'");

                    List<EmployeeShift> employeeShifts = (List<EmployeeShift>) oem.loadEntityList(
                            EmployeeShift.class.getSimpleName(), conds, null, null, loggedUser);
                    if (employeeShifts != null & !employeeShifts.isEmpty()) {
                        if (employeeShifts.size() > 1) {
                            ofr.addError(usrMsgService.getUserMessage("ShiftsError", loggedUser));
                        }
                        employeeShift = employeeShifts.get(0);
                    }

                    if (employeeShift != null) {
                        Object dayValue = BaseEntity.getValueFromEntity(employeeShift, "m" + Integer.parseInt(currentMonth) + "D" + Integer.parseInt(currentDay));
                        String dayShift = dayValue.toString();

                        conds.clear();
                        conds.add("calendar.dbid = " + tmCalendar.getDbid());
                        conds.add("code = '" + dayShift + "'");
                        normalCal = (NormalWorkingCalendar) oem.loadEntity(
                                NormalWorkingCalendar.class.getSimpleName(), conds, null, loggedUser);

                        if (normalCal == null && dayShift.equals("H")) {

                            conds.clear();
                            conds.add("code = 'HOLIDAY'");
                            normalCal = (NormalWorkingCalendar) oem.loadEntity(
                                    NormalWorkingCalendar.class.getSimpleName(), conds, null, loggedUser);

                            if (normalCal == null) {
                                needCalenarSegments = false;
                                if (empDA.getWorkingCalendar() == null) {
                                    Object obj[] = getShift(tmCalendar, empDA.getDailyDate(), loggedUser);
                                    normalCal = (NormalWorkingCalendar) obj[1];
                                } else {
                                    normalCal = empDA.getWorkingCalendar();
                                }
                            }
                        } else if (normalCal == null && !dayShift.equals("H")) {
                            normalCal = empDA.getWorkingCalendar();
                            if (normalCal == null) {
                                List<String> usrMsgParams = new ArrayList<String>();
                                usrMsgParams.add(employeeShift.getEmployee().getCode());
                                usrMsgParams.add(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()));
                                //if (normalCal == null && !dayShift.equals("H")) {
                                ofr.addError(usrMsgService.getUserMessage("NoShiftExist", usrMsgParams, loggedUser));
                                return ofr;
                            }
                        }
                    }
                }

                if (normalCal != null) {
                    empDA.setWorkingCalendar(normalCal);
                    conds.clear();
                    conds.add("calendar.dbid = " + normalCal.getDbid());
                    conds.add("day.code = '" + dayCondition + "'");
                    conds.add("activeFrom  <= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'");
                    conds.add("activeTo  >= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'");
                    dailyExp = (DaysCalendarException) oem.loadEntity(DaysCalendarException.class.getSimpleName(),
                            conds,
                            null,
                            loggedUser);

                    if (dailyExp == null) {
                        conds.clear();
                        conds.add("calendar.dbid = " + normalCal.getDbid());
                        conds.add("activeFrom  <= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'");
                        conds.add("activeTo  >= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'");

                        dateExp = (DateCalendarException) oem.loadEntity(DateCalendarException.class.getSimpleName(),
                                conds,
                                null,
                                loggedUser);
                    }
                }

                BigDecimal numHrs;
                String numHrsStr = "00:00";

                sort.clear();
                sort.add("segmentSeq");
                if (!workingOnHoliday || !workingOnWeekend) {
                    if (dailyExp != null) {

                        if (dailyExp.getCalendar() != null && dailyExp.getCalendar().getTolerence() != null) {
                            tolerence = dateTimeUtility.convertMinutesToTime(dailyExp.getCalendar().getTolerence().doubleValue());
                        }

                        calData = new EmpCalData(tolerence, dailyExp.getTimeIn(), dailyExp.getTimeOut(),
                                dailyExp.getTimeOut(), dailyExp.getBreakFrom(), dailyExp.getBreakTo(), dailyExp.getCalendar().getStartDayTime());

                        conds.clear();
                        conds.add("dayException.dbid = " + dailyExp.getDbid());
                        conds.add("mandatory = false");
                        dayExcs = oem.loadEntityList(OTDayExc.class.getSimpleName(), conds, null, null, loggedUser);
                    } else if (dateExp != null) {
                        if (dateExp.getCalendar() != null && dateExp.getCalendar().getTolerence() != null) {
                            tolerence = dateTimeUtility.convertMinutesToTime(dateExp.getCalendar().getTolerence().doubleValue());
                        }

                        calData = new EmpCalData(tolerence, dateExp.getTimeIn(), dateExp.getTimeOut(),
                                dateExp.getTimeOut(), dateExp.getBreakFrom(), dateExp.getBreakTo(), dateExp.getCalendar().getStartDayTime());

                        conds.clear();
                        conds.add("dateException.dbid = " + dateExp.getDbid());
                        conds.add("mandatory = false");
                        dateExcs = oem.loadEntityList(OTDateExc.class.getSimpleName(), conds, null, null, loggedUser);
                    } else if (normalCal != null) {
                        if (calType.getCode().equals("Flexible") && timeIn != null && !timeIn.equals("")) {
                            numHrs = normalCal.getNumberOfHours();
                            if (numHrs != null) {
                                numHrsStr = dateTimeUtility.convertToTime(numHrs.doubleValue());
                            }
                            /* this is the case of flexible calendars with planned in/out range with # of hrs,
                             so the expectedOut will be the actual in (timeIn) + numHrs
                             */
                            planIn = normalCal.getTimeIn();
                            planOut = normalCal.getTimeOut();
                            if (planIn != null && !planIn.isEmpty()) {

                                boolean expectedOutCalc = false;
                                // if mission before timeIn
                                for (int i = 0; i < exceptionFrom.size(); i++) {
                                    if (dateTimeUtility.compareTime(exceptionFrom.get(i), timeIn) <= 0
                                            && dateTimeUtility.compareTime(exceptionTo.get(i), planIn) >= 0) {
                                        expectedOutCalc = true;
                                        if (dateTimeUtility.compareTime(exceptionFrom.get(i), planIn) > 0) {
                                            expectedOut = dateTimeUtility.addTime(exceptionFrom.get(i), numHrsStr);
                                        } else {
                                            expectedOut = dateTimeUtility.addTime(planIn, numHrsStr);
                                        }
                                    }
                                }

                                if (!expectedOutCalc) {
                                    if (dateTimeUtility.compareTime(timeIn, planIn) < 0) {
                                        expectedOut = dateTimeUtility.addTime(planIn, numHrsStr);
                                    } else {
                                        expectedOut = dateTimeUtility.addTime(timeIn, numHrsStr);
                                    }
                                }
                                planOut = normalCal.getTimeOut();
                                displayPlanOut = expectedOut;
                                if (dateTimeUtility.compareTime(expectedOut, planOut) > 0) {
                                    displayPlanOut = planOut;
                                }
                            } else { // this is the case of flexible calendar with just only # of hrs
                                planIn = timeIn;
                                planOut = dateTimeUtility.addTime(planIn, numHrsStr);
                                displayPlanOut = planOut;
                            }
                        } else {
                            planIn = normalCal.getTimeIn();
                            planOut = normalCal.getTimeOut();
                            displayPlanOut = planOut;
                        }

                        planBreakIn = normalCal.getBreakFrom();
                        planBreakOut = normalCal.getBreakTo();
                        startDayTime = normalCal.getStartDayTime();
                        if (normalCal.getTolerence() != null) {
                            tolerence = dateTimeUtility.convertMinutesToTime(normalCal.getTolerence().doubleValue());
                        }
                        if (normalCal.getEarlyLeaveTolerence() != null) {
                            earlyLeaveTolerence = dateTimeUtility.convertMinutesToTime(normalCal.getEarlyLeaveTolerence().doubleValue());
                        }
                        minOT = normalCal.getMinimumOT();
                        if (minOT == null || minOT.equals("")) {
                            minOT = "00:00";
                        }
                        if (needCalenarSegments) {
                            conds.clear();
                            conds.add("calendar.dbid = " + normalCal.getDbid());
                            conds.add("mandatory = false");
                            segments = oem.loadEntityList(OTSegment.class.getSimpleName(), conds, null, sort, loggedUser);
                        }
                    }
                }

                CalendarExceptionBase calendarException = null;
                if (dailyExp != null) {
                    calendarException = dailyExp;
                    normalCal = dailyExp.getCalendar();
                }
                if (dateExp != null) {
                    calendarException = dateExp;
                    normalCal = dateExp.getCalendar();
                }
                if (calData != null) {
                    planIn = calData.getPlanIn();
                    planOut = calData.getPlanOut();
                    planBreakIn = calData.getPlanBreakIn();
                    planBreakOut = calData.getPlanBreakOut();
                    startDayTime = calData.getStartDayTime();
                    displayPlanOut = planOut;

                    if (calendarException != null
                            && normalCal.getCalendar().getType().getCode().equals("Flexible")
                            && timeIn != null && !timeIn.equals("")) {
                        numHrs = calendarException.getNumberOfHours();
                        if (numHrs != null) {
                            numHrsStr = dateTimeUtility.convertToTime(numHrs.doubleValue());
                        }
                        /* this is the case of flexible calendars with planned in/out range with # of hrs,
                         so the expectedOut will be the actual in (timeIn) + numHrs
                         */
                        if (planIn != null && !planIn.isEmpty()) {

                            boolean expectedOutCalc = false;
                            // if mission before timeIn
                            for (int i = 0; i < exceptionFrom.size(); i++) {
                                if (dateTimeUtility.compareTime(exceptionFrom.get(i), timeIn) <= 0
                                        && dateTimeUtility.compareTime(exceptionTo.get(i), planIn) >= 0) {
                                    expectedOutCalc = true;
                                    if (dateTimeUtility.compareTime(exceptionFrom.get(i), planIn) > 0) {
                                        expectedOut = dateTimeUtility.addTime(exceptionFrom.get(i), numHrsStr);
                                    } else {
                                        expectedOut = dateTimeUtility.addTime(planIn, numHrsStr);
                                    }
                                }
                            }

                            if (!expectedOutCalc) {
                                if (dateTimeUtility.compareTime(timeIn, planIn) < 0) {
                                    expectedOut = dateTimeUtility.addTime(planIn, numHrsStr);
                                } else {
                                    expectedOut = dateTimeUtility.addTime(timeIn, numHrsStr);
                                }
                            }
                            planOut = calendarException.getTimeOut();
                            displayPlanOut = expectedOut;
                            if (dateTimeUtility.compareTime(expectedOut, planOut) > 0) {
                                displayPlanOut = planOut;
                            }
                        } else { // this is the case of flexible calendar with just only # of hrs
                            planIn = timeIn;
                            planOut = dateTimeUtility.addTime(planIn, numHrsStr);
                            displayPlanOut = planOut;
                        }
                    }

                }

                if ((timeIn != null && !timeIn.equals("")) && (timeOut == null || (timeOut != null && timeOut.equals("")))) {
                    timeOut = displayPlanOut;
                    displaytimeOut = timeOut;
                }

                // check if this employee is a female & has nursery
                boolean isNurseryEligible = false;
                String selectNursery;
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    selectNursery = "select count(*) from EmployeeNursery where employeeDependance_dbid in "
                            + " (select dbid from EmployeeDependance where employee_dbid = " + empDA.getEmployee().getDbid() + " and deleted = 0)"
                            + " and to_date(startDate) <= to_date('" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "','YYYY-MM-dd')"
                            + " and to_date(endDate) >= to_date('" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "','YYYY-MM-dd')"
                            + " and deleted = 0";
                } else {
                    selectNursery = "select count(*) from EmployeeNursery where employeeDependance_dbid in "
                            + " (select dbid from EmployeeDependance where employee_dbid = " + empDA.getEmployee().getDbid() + " and deleted = 0)"
                            + " and startDate <= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'"
                            + " and endDate >= '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'"
                            + " and deleted = 0";
                }
                Object resObj = oem.executeEntityNativeQuery(selectNursery, loggedUser);
                if (resObj != null) {
                    if (Integer.parseInt(resObj.toString()) > 0) {
                        isNurseryEligible = true;
                    }
                }
                String nurseryFrom = "", nurseryTo = "",
                        nurseryFrom2 = "", nurseryTo2 = "";
                String nurseryType = "";
                Nursery nursery = (Nursery) oem.loadEntity(Nursery.class.getSimpleName(), null, null, loggedUser);
                if (nursery != null) {

                    if (nursery.getHourType().getCode().equalsIgnoreCase("start")) {
//                        if (calType.getCode().equals("Flexible")) {
//                            if (dateTimeUtility.compareTime(expectedOut, planOut) > 0) {
//                                nurseryFrom = dateTimeUtility.subtractTime(numHrsStr, planOut);
//                            } else {
//                                nurseryFrom = dateTimeUtility.subtractTime(numHrsStr, expectedOut);
//                            }
//                        } else {
                        nurseryFrom = planIn;
//                        }
                        nurseryTo = dateTimeUtility.addTime(nurseryFrom, "01:00"); // TODO: need to be from setup not fixed as 1 hour
                        nurseryType = "start";
                    } else if (nursery.getHourType().getCode().equalsIgnoreCase("end")) {
                        nurseryTo = planOut;
//                        if (calType.getCode().equals("Flexible")) {
//                            if (dateTimeUtility.compareTime(expectedOut, planOut) < 0) {
//                                nurseryTo = expectedOut;
//                            }
//                        }
                        nurseryFrom = dateTimeUtility.subtractTime("01:00", nurseryTo);
                        nurseryType = "end";
                    } else if (nursery.getHourType().getCode().equalsIgnoreCase("both")) {
//                        if (calType.getCode().equals("Flexible")) {
//                            if (dateTimeUtility.compareTime(expectedOut, planOut) > 0) {
//                                nurseryFrom = dateTimeUtility.subtractTime(numHrsStr, planOut);
//                            } else {
//                                nurseryFrom = dateTimeUtility.subtractTime(numHrsStr, expectedOut);
//                            }
//                        } else {
                        nurseryFrom = planIn;
//                        }
                        nurseryTo = dateTimeUtility.addTime(nurseryFrom, "00:30");
                        nurseryTo2 = planOut;
//                        if (calType.getCode().equals("Flexible")) {
//                            if (dateTimeUtility.compareTime(expectedOut, planOut) < 0) {
//                                nurseryTo2 = expectedOut;
//                            }
//                        }
                        nurseryFrom2 = dateTimeUtility.subtractTime("00:30", nurseryTo2);
                        nurseryType = "both";
                    } else if (nursery.getHourType().getCode().equalsIgnoreCase("floating")) {
                        nurseryType = "floating";
                    }
                }

                if (multiEntry) {
                    conds.clear();
                    conds.add("employee.dbid = " + emp.getDbid());
                    conds.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'");
                    conds.add("header = false");
                    conds.add("dbid <> " + empDA.getDbid());

                    empDADetails = (List<EmployeeDailyAttendance>) oem.loadEntityList(
                            EmployeeDailyAttendance.class.getSimpleName(), conds, null, null, loggedUser);

                    if (outDate == null) {
                        outDate = entryDate;
                        empDA.setOutDate(outDate);
                    }
                    if (entryDate.after(outDate)) {
                        ofr.addError(usrMsgService.getUserMessage("TM001", loggedUser));
                        return ofr;
                    } else if (entryDate.before(outDate)) {
                        if (!odm.getODataType().getName().equals("EntityChangeField")) {
                            distributeOnDays(entryDate, outDate, startDayTime, timeOut, startDayTime, emp, loggedUser);
                            empDA.setTimeIn(timeIn);
                            empDA.setTimeOut(startDayTime);
                            timeOut = startDayTime;
                            displayTimeIn = timeIn;
                            displaytimeOut = timeOut;
                        }
                    }

                    if (!empDADetails.contains(empDA)) {
                        empDADetails.add(empDA);
                    }
                    entries = getTMEntry(empDADetails);
                } else {
                    if (outDate != null) {
                        if (entryDate.before(outDate)) {
                            if (!odm.getODataType().getName().equals("EntityChangeField")) {
                                String returnValue = distributeOnDaysSingleEntry(entryDate, outDate, startDayTime, timeOut,
                                        startDayTime, emp, tmCalendar, loggedUser);
                                empDA.setTimeIn(timeIn);
                                if (!returnValue.equals("")) {
                                    long diffInMillies = outDate.getTime() - entryDate.getTime();
                                    TimeUnit timeUnit = TimeUnit.DAYS;
                                    if (timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS) > 1) {
                                        timeOut = startDayTime;
                                    } else {
                                        //timeOut = planOut;
                                        timeOut = returnValue;
                                    }
                                    empDA.setTimeOut(timeOut);

                                } else {
                                    empDA.setTimeOut(timeOut);
                                }
                            }
                        }
                    }
                    //______________________
//                    if (dateTimeUtility.compareTime(timeIn, planIn) <= 0) {
//                        timeIn = planIn;
//                    }
                    displayTimeIn = timeIn;
                    displaytimeOut = timeOut;
                    //______________________
                    TMEntry entry = new TMEntry();
                    entry.setIn(timeIn);
                    entry.setOut(timeOut);
                    entries.add(entry);
                }

                // loading the OT Requests for the employee
                requestedFrom = new ArrayList<String>();
                requestedTo = new ArrayList<String>();

                conds.clear();
                conds.add("employee.dbid = " + empDA.getEmployee().getDbid());
                conds.add("requestedDate = '" + dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()) + "'");
                conds.add("approved = 'Y'");
                employeeOTRequests = (List<EmployeeOTRequest>) oem.loadEntityList(
                        EmployeeOTRequest.class.getSimpleName(), conds, null, null, loggedUser);
                if (employeeOTRequests != null && !employeeOTRequests.isEmpty()) {
                    for (int i = 0; i < employeeOTRequests.size(); i++) {
                        requestedFrom.add(employeeOTRequests.get(i).getTimeFrom());
                        requestedTo.add(employeeOTRequests.get(i).getTimeTo());
                    }
                }

                //<editor-fold defaultstate="collapsed" desc="if calendar is reversed e.g. 18:00 -> 06:00">
                if (startDayTime != null && !startDayTime.equals("")) {
                    if (segments != null) {
                        for (int i = 0; i < segments.size(); i++) {
                            // **** This part is to be handled by DateTime later..
                            // For special case @ Porta
                            if (dateTimeUtility.compareTime(segments.get(i).getFromTime(), segments.get(i).getToTime()) < 0
                                    && dateTimeUtility.compareTime(segments.get(i).getFromTime(), startDayTime) <= 0
                                    && dateTimeUtility.compareTime(segments.get(i).getToTime(), startDayTime) <= 0) {
                                segments.get(i).setFromTime(dateTimeUtility.addTime(segments.get(i).getFromTime(), "24:00"));
                                segments.get(i).setToTime(dateTimeUtility.addTime(segments.get(i).getToTime(), "24:00"));
                            } else {
                                tempCalData = reverseCalendar(segments.get(i).getFromTime(), segments.get(i).getToTime(), startDayTime, true);
                                segments.get(i).setFromTime(tempCalData.get(0));
                                segments.get(i).setToTime(tempCalData.get(1));
                            }
                            // ****
                        }
                    }

                    if (dateExcs != null) {
                        for (int i = 0; i < dateExcs.size(); i++) {
                            tempCalData = reverseCalendar(dateExcs.get(i).getFromTime(), dateExcs.get(i).getToTime(), startDayTime, true);
                            dateExcs.get(i).setFromTime(tempCalData.get(0));
                            dateExcs.get(i).setToTime(tempCalData.get(1));
                        }
                    }

                    if (dayExcs != null) {
                        for (int i = 0; i < dayExcs.size(); i++) {
                            tempCalData = reverseCalendar(dayExcs.get(i).getFromTime(), dayExcs.get(i).getToTime(), startDayTime, true);
                            dayExcs.get(i).setFromTime(tempCalData.get(0));
                            dayExcs.get(i).setToTime(tempCalData.get(1));
                        }
                    }

                    for (int i = 0; i < entries.size(); i++) {
                        // skip if the entries (in/out) are empty string so as not to crash
                        //      --> in the case of: if deleting the timeIn/Out (empty strings), then  the day is to be absence
                        if (entries.get(i).getIn().equals("") || entries.get(i).getOut().equals("")) {
                            continue;
                        }

                        // BG-452 - Loubna - 22/06/2014
                        // **** This part is to be handled by DateTime later..
                        // For special case @ Porta
                        if (dateTimeUtility.compareTime(entries.get(i).getIn(), entries.get(i).getOut()) < 0
                                && dateTimeUtility.compareTime(entries.get(i).getIn(), startDayTime) <= 0
                                && dateTimeUtility.compareTime(entries.get(i).getOut(), startDayTime) <= 0) {
                            entries.get(i).setIn(dateTimeUtility.addTime(entries.get(i).getIn(), "24:00"));
                            entries.get(i).setOut(dateTimeUtility.addTime(entries.get(i).getOut(), "24:00"));
                        } else {
                            tempCalData = reverseCalendar(entries.get(i).getIn(), entries.get(i).getOut(), startDayTime, true);
                            entries.get(i).setIn(tempCalData.get(0));
                            entries.get(i).setOut(tempCalData.get(1));
                        }
                    }

                    tempCalData = reverseCalendar(planIn, planOut, startDayTime, true);
                    planIn = (tempCalData.get(0));
                    planOut = (tempCalData.get(1));

                    tempCalData = reverseCalendar(planBreakIn, planBreakOut, startDayTime, true);
                    planBreakIn = (tempCalData.get(0));
                    planBreakOut = (tempCalData.get(1));

                    tempCalData = reverseCalendar(timeIn, timeOut, startDayTime, true);
                    timeIn = (tempCalData.get(0));
                    timeOut = (tempCalData.get(1));

                    for (int i = 0; i < exceptionFrom.size(); i++) {
                        tempCalData = reverseCalendar(exceptionFrom.get(i), exceptionTo.get(i), startDayTime, false);
                        exceptionFrom.set(i, tempCalData.get(0));
                        exceptionTo.set(i, tempCalData.get(1));
                    }

                    for (int i = 0; i < employeeOTRequests.size(); i++) {
                        tempCalData = reverseCalendar(employeeOTRequests.get(i).getTimeFrom(), employeeOTRequests.get(i).getTimeTo(), startDayTime, false);
                        employeeOTRequests.get(i).setTimeFrom(tempCalData.get(0));
                        employeeOTRequests.get(i).setTimeTo(tempCalData.get(1));
                    }
                }
                //</editor-fold>

                // check whether the attendance is out of scope to set the outOfScope flag
                if (timeIn != null && !timeIn.equals("") && timeOut != null && !timeOut.equals("")) {
                    if (empDA.getDayNature().getCode() != null
                            && !empDA.getDayNature().getCode().equals("WeekEnd")
                            && !empDA.getDayNature().getCode().equals("Holiday")
                            && !empDA.getDayNature().getCode().equals("WorkingOnWeekEnd")
                            && !empDA.getDayNature().getCode().equals("WorkingOnHoliday")
                            && !empDA.getDayNature().getCode().equals("Mission")) {

                        for (int i = 0; i < entries.size(); i++) {
                            outOfScope = isOutOfScope(entries.get(i).getIn(), entries.get(i).getOut(), planIn, planOut);

                            if (!outOfScope) {
                                break;
                            }
                        }
                    }
                }

                // add flag to check if the action is entityChangeFieldAction
                boolean isEntityChangeField = false;
                if (odm.getODataType().getName().equals("EntityChangeField")) {
                    isEntityChangeField = true;
                } else {
                    isEntityChangeField = false;
                }

                String totalOT = "00:00";
                if (workingOnHoliday || workingOnWeekend) // in case of holiday and weekend working
                {
                    if (empOTCalc) {
                        String holidayOrWeekEndOT = "00:00";
                        if (empOTCalc && !outOfScope
                                && segments != null
                                && segments.size() > 0
                                && (calType.getCode().equals("VariableShift") || calType.getCode().equals("Rotation"))) {
                            //
                            holidayOrWeekendNotApprovedOT = dateTimeUtility.subtractTime(timeIn, timeOut);

                            if (workingOnWeekend) {
                                holidayOrWeekEndOT = tmCalendar.getWeekEndOT();
                            } else {
                                holidayOrWeekEndOT = tmCalendar.getHolidayOT();
                            }
                            if (holidayOrWeekEndOT == null || holidayOrWeekEndOT.equals("")) {
                                holidayOrWeekEndOT = "00:00";
                            }
                            if ((dateTimeUtility.compareTime(holidayOrWeekendNotApprovedOT, holidayOrWeekEndOT) >= 0)
                                    || (holidayOrWeekEndOT.equals("00:00"))) {
                                if (holidayOrWeekEndOT.equals("00:00")) {
                                    holidayOrWeekendNotApprovedOT = "00:00";
                                } else {
                                    holidayOrWeekendNotApprovedOT = holidayOrWeekEndOT;
                                }

                                //timeIn = dateTimeUtility.addTime(timeIn, holidayOrWeekendNotApprovedOT);
                                List<OverTime> overTimes = null;
                                if (entries.size() == 1) {
                                    entries.get(0).setIn(dateTimeUtility.addTime(timeIn, holidayOrWeekendNotApprovedOT));
                                }

                                for (int j = 0; j < entries.size(); j++) {
                                    overTimes = new ArrayList<OverTime>();
                                    if (normalCal != null) {
                                        for (int i = 0; i < segments.size(); i++) {
//                                        if (i > 0) { // 1st segment
//                                            minOT = "00:00";
//                                        }
                                            overTimes.add(getOverTime(entries.get(j).getIn(), entries.get(j).getOut(), "00:00", "00:00", employeeOTRequests,
                                                    segments.get(i),
                                                    minOT, isEntityChangeField));
                                        }
                                    } else if (dateExp != null) {
                                        for (int i = 0; i < dateExcs.size(); i++) {
                                            overTimes.add(getOverTime(entries.get(j).getIn(), entries.get(j).getOut(), "00:00", "00:00", employeeOTRequests,
                                                    dateExcs.get(i), minOT, isEntityChangeField));
                                        }
                                    } else if (dayExcs != null) {
                                        for (int i = 0; i < dayExcs.size(); i++) {
                                            overTimes.add(getOverTime(entries.get(j).getIn(), entries.get(j).getOut(), "00:00", "00:00", employeeOTRequests,
                                                    dayExcs.get(i), minOT, isEntityChangeField));
                                        }
                                    }
                                    for (int i = 0; i < overTimes.size(); i++) {
                                        nonPlanned = dateTimeUtility.addTime(nonPlanned, overTimes.get(i).getNonPlanned());
                                        notApprovedDayOT = dateTimeUtility.addTime(notApprovedDayOT, overTimes.get(i).getNotApprovedDayOT());
                                        notApprovedNightOT = dateTimeUtility.addTime(notApprovedNightOT, overTimes.get(i).getNotApprovedNightOT());
                                        approvedDayOT = dateTimeUtility.addTime(approvedDayOT, overTimes.get(i).getApprovedDayOT());
                                        approvedNightOT = dateTimeUtility.addTime(approvedNightOT, overTimes.get(i).getApprovedNightOT());
                                        dayOTIn = overTimes.get(i).getDayOTIn();
                                        dayOTOut = overTimes.get(i).getDayOTOut();
                                        nightOTIn = overTimes.get(i).getNightOTIn();
                                        nightOTOut = overTimes.get(i).getNightOTOut();
                                    }
                                }
                            }
                        } else if (timeIn.equals(displaytimeOut)) {
                            holidayOrWeekendNotApprovedOT = "24:00";
                        } else {
                            holidayOrWeekendNotApprovedOT = dateTimeUtility.subtractTime(timeIn, timeOut);
                        }
                        //_____________________Need TO Be Checked__________________________________________
                        for (int i = 0; i < employeeOTRequests.size(); i++) {
                            String reqFrom = employeeOTRequests.get(i).getTimeFrom(),
                                    reqTo = employeeOTRequests.get(i).getTimeTo();
                            if (isReversed && dateTimeUtility.compareTime(reqTo, reqFrom) < 0) {
                                reqTo = dateTimeUtility.addTime(reqTo, "24:00");
                            }

                            // TODO: Check whether holiday & weekend will be waiting for approval (request) or no
                            List<String> tempResults = getInterval(timeIn, timeOut,
                                    reqFrom, reqTo);
                            String subWEOT = null;

                            if (!tempResults.isEmpty()) {
                                if (holidayOrWeekendApprovedOT.equals("0")) {
                                    holidayOrWeekendApprovedOT = "00:00";
                                }
                                holidayOrWeekendApprovedOT = dateTimeUtility.addTime(holidayOrWeekendApprovedOT, tempResults.get(0));
                                subWEOT = tempResults.get(0);
                            }
                            if (holidayOrWeekEndOT != null && !holidayOrWeekEndOT.equals("00:00")) {
                                if (dateTimeUtility.compareTime(holidayOrWeekendApprovedOT, holidayOrWeekEndOT) >= 0) {
                                    holidayOrWeekendApprovedOT = holidayOrWeekEndOT;

                                }
                            }
                            holidayOrWeekendNotApprovedOT = dateTimeUtility.subtractTime(subWEOT,
                                    holidayOrWeekendNotApprovedOT);
                            try {
                                String maxWeekEndOT = null;
                                if (tmCalendar.getMaximumWeekEndOT() == null || tmCalendar.getMaximumWeekEndOT().equals("")) {
                                    maxWeekEndOT = "24:00";
                                } else {
                                    maxWeekEndOT = tmCalendar.getMaximumWeekEndOT();
                                }
                                String maxHolidayOT = tmCalendar.getMaximumHolidayOT();
                                if (workingOnWeekend && dateTimeUtility.compareTime(holidayOrWeekendApprovedOT, maxWeekEndOT) > 0) {
                                    String greaterThanMaximum = dateTimeUtility.subtractTime(maxWeekEndOT, holidayOrWeekendApprovedOT);
                                    holidayOrWeekendApprovedOT = maxWeekEndOT;
                                    holidayOrWeekendNotApprovedOT = dateTimeUtility.addTime(holidayOrWeekendNotApprovedOT, greaterThanMaximum);

                                }

                                if (workingOnHoliday && dateTimeUtility.compareTime(holidayOrWeekendApprovedOT, maxHolidayOT) > 0) {
                                    String greaterThanMaximum = dateTimeUtility.subtractTime(maxHolidayOT, holidayOrWeekendApprovedOT);
                                    holidayOrWeekendApprovedOT = maxHolidayOT;
                                    holidayOrWeekendNotApprovedOT = dateTimeUtility.addTime(holidayOrWeekendNotApprovedOT, greaterThanMaximum);
                                }

                            } catch (Exception e) {

                                OLog.logException(e, loggedUser);

                            }
                        }
                    }
                } else {
                    if (!outOfScope) {
                        for (int i = 0; i < exceptionFrom.size(); i++) {
                            if (exceptionFrom.get(i) != null && exceptionTo.get(i) != null) {
                                exceptionHours = dateTimeUtility.addTime(exceptionHours,
                                        dateTimeUtility.subtractTime(exceptionFrom.get(i), exceptionTo.get(i)));
                            }
                        }

                        // delay calculation
                        for (int i = 0; i < entries.size(); i++) {
                            if (!entries.get(i).getIn().equals("")
                                    && dateTimeUtility.compareTime(entries.get(i).getIn(), planIn) == 1
                                    && ((i != 0 && dateTimeUtility.compareTime(entries.get(i - 1).getOut(), planIn) < 0)
                                    || i == 0)) {

                                if (isNurseryEligible && (nurseryType.equals("start") || nurseryType.equals("both"))) {
                                    delay = calculateDelay(entries.get(i).getIn(), planIn, tolerence, normalCal.isIgnoreTolerence(),
                                            nurseryFrom, nurseryTo);
                                    break;
                                }
                                if (exceptionFrom.isEmpty()) {
                                    delay = calculateDelay(entries.get(i).getIn(), planIn, tolerence, normalCal.isIgnoreTolerence(),
                                            "", "");
                                } else {
                                    for (int j = 0; j < exceptionFrom.size(); j++) {
                                        // calculate the delay when there is a mission that is within the day; not before its start
                                        delay = calculateDelay(entries.get(i).getIn(), planIn, tolerence, normalCal.isIgnoreTolerence(),
                                                exceptionFrom.get(j), exceptionTo.get(j));
                                        if (dateTimeUtility.compareTime(exceptionTo.get(j), planIn) > 0) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        // early leave calculation
                        for (int i = entries.size() - 1; i >= 0; i--) {
                            if (i == entries.size() - 1 && !entries.get(i).getOut().equals("")) {
                                if (isNurseryEligible) {
                                    if (nurseryType.equals("end")) {
                                        earlyLeave = calculateEarlyLeave(entries.get(i).getOut(), planOut, planIn, earlyLeaveTolerence,
                                                normalCal.isIgnoreEarlyLeaveTolerence(), nurseryFrom, nurseryTo);
                                        break;
                                    } else if (nurseryType.equals("both")) {
                                        earlyLeave = calculateEarlyLeave(entries.get(i).getOut(), planOut, planIn, earlyLeaveTolerence,
                                                normalCal.isIgnoreEarlyLeaveTolerence(), nurseryFrom2, nurseryTo2);
                                        break;
                                    }
                                }
                                if (exceptionFrom.isEmpty()) {
                                    earlyLeave = calculateEarlyLeave(entries.get(i).getOut(), planOut, planIn, earlyLeaveTolerence,
                                            normalCal.isIgnoreEarlyLeaveTolerence(), "", "");
                                    break;
                                }
                                for (int j = exceptionFrom.size() - 1; j >= 0; j--) {
                                    // calculate the early leave when there is a mission that is within the day; not after its end
                                    earlyLeave = calculateEarlyLeave(entries.get(i).getOut(), planOut, planIn, earlyLeaveTolerence,
                                            normalCal.isIgnoreEarlyLeaveTolerence(), exceptionFrom.get(j), exceptionTo.get(j));
                                    if (dateTimeUtility.compareTime(exceptionFrom.get(j), planOut) < 0) {
                                        break;
                                    }
                                }
                            }
                        }
                        // less work calculation in case of flexible calendar with planIn & # of hrs
                        if (!expectedOut.equals("-")) {
                            delay = "00:00";
                            earlyLeave = "00:00";
                            lessWork = "00:00";
                            // less work resulting from delay = expectedOut - planOut
                            if (dateTimeUtility.compareTime(expectedOut, planOut) > 0) {
                                lessWork = dateTimeUtility.subtractTime(planOut, expectedOut);
                                for (int j = 0; j < exceptionFrom.size(); j++) {
                                    List<String> res = getInterval(planIn, timeIn, exceptionFrom.get(j), exceptionTo.get(j));
                                    if (res.size() > 0 && res.get(0) != null) {
                                        if (dateTimeUtility.compareTime(lessWork, res.get(0)) > 0) {
                                            lessWork = dateTimeUtility.subtractTime(res.get(0), lessWork);
                                        } else {
                                            lessWork = "00:00";
                                        }
                                    }
                                }
                            }
                            // less work resulting from early leave = expectedOut - timeOut
                            if (dateTimeUtility.compareTime(expectedOut, timeOut) > 0) {
                                if (dateTimeUtility.compareTime(expectedOut, planOut) <= 0) {
                                    lessWork = dateTimeUtility.addTime(lessWork, dateTimeUtility.subtractTime(timeOut, expectedOut));
                                    for (int j = 0; j < exceptionFrom.size(); j++) {
                                        List<String> res = getInterval(timeOut, expectedOut, exceptionFrom.get(j), exceptionTo.get(j));
                                        if (res.size() > 0 && res.get(0) != null) {
                                            if (dateTimeUtility.compareTime(lessWork, res.get(0)) > 0) {
                                                lessWork = dateTimeUtility.subtractTime(res.get(0), lessWork);
                                            } else {
                                                lessWork = "00:00";
                                            }
                                        }
                                    }
                                } else {
                                    if (dateTimeUtility.compareTime(planOut, timeOut) > 0) {
                                        lessWork = dateTimeUtility.addTime(lessWork, dateTimeUtility.subtractTime(timeOut, planOut));
                                    }
                                    for (int j = 0; j < exceptionFrom.size(); j++) {
                                        List<String> res = getInterval(timeOut, planOut, exceptionFrom.get(j), exceptionTo.get(j));
                                        if (res.size() > 0 && res.get(0) != null) {
                                            if (dateTimeUtility.compareTime(lessWork, res.get(0)) > 0) {
                                                lessWork = dateTimeUtility.subtractTime(res.get(0), lessWork);
                                            } else {
                                                lessWork = "00:00";
                                            }
                                        }
                                    }
                                }

                            }
                            // check if there are gaps between entry and exceptions; in both cases of beginning & end of day
                            for (int i = 0; i < exceptionFrom.size(); i++) {
                                if (dateTimeUtility.compareTime(exceptionTo.get(i), planIn) > 0) {
                                    if (dateTimeUtility.compareTime(timeIn, exceptionTo.get(i)) > 0) {
                                        lessWork = dateTimeUtility.addTime(lessWork,
                                                dateTimeUtility.subtractTime(exceptionTo.get(i), timeIn));
                                        break;
                                    }
                                }
                            }
                            if (isNurseryEligible) {
                                if (dateTimeUtility.compareTime(lessWork, "00:00") > 0) {
                                    if (dateTimeUtility.compareTime(lessWork, "01:00") >= 0) {
                                        lessWork = dateTimeUtility.subtractTime("01:00", lessWork);
                                    } else {
                                        lessWork = "00:00";
                                    }
                                }
                            }
                        }
                    }

                    // whether it is outofscope so it will be considered all as OT, or within the scope so OT will be calculated also,
                    // based on the condition that the emp is checked as OT entitled
                    if (empOTCalc) {
                        EmployeeVacationAdjustment vacAdj = new EmployeeVacationAdjustment();
                        conditions.clear();
                        if (empDA.getWorkingCalendar().getCalendar().getRules().get(0).getVacation() != null) {
                            String vacAdjQuery = "select dbid from EMPLOYEEVACATIONADJUSTMENT where employee_dbid=" + empDA.getEmployee().getDbid()
                                    + " and CREATIONDATE =" + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()))
                                    + " and vacation_dbid=" + empDA.getWorkingCalendar().getCalendar().getRules().get(0).getVacation().getDbid();

                            Object vacAdjDBID = oem.executeEntityNativeQuery(vacAdjQuery, loggedUser);
                            if (vacAdjDBID != null) {
                                conditions.add("dbid=" + vacAdjDBID.toString());
                                vacAdj = (EmployeeVacationAdjustment) oem.loadEntity(EmployeeVacationAdjustment.class.getSimpleName(), conditions, null, loggedUser);
                            }

                            if (vacAdj != null) {
                                oem.deleteEntity(vacAdj, loggedUser);
                            }
                        }
                        List<OverTime> overTimes = null;
                        for (int j = 0; j < entries.size(); j++) {
                            overTimes = new ArrayList<OverTime>();
                            if (normalCal != null && segments != null) {
                                for (int i = 0; i < segments.size(); i++) {
//                                    if (i > 0) { // 1st segment
//                                        minOT = "00:00";
//                                    }
                                    OverTime otCheck = getOverTime(entries.get(j).getIn(), entries.get(j).getOut(), planIn, planOut, employeeOTRequests,
                                            segments.get(i),
                                            minOT, isEntityChangeField);
                                    if (!segments.get(i).isApprovalNeeded()) {
                                        if (eph != null) {
                                            if (isWorkOTEligible(empDA, eph)) {
                                                if (otCheck.getApprovedDayOT() != null && !otCheck.getApprovedDayOT().equals("00:00")) {

                                                    if (dateTimeUtility.compareTime(otCheck.getDayOTIn(), empDA.getPlannedIn()) < 1) {
                                                        double ot1 = dateTimeUtility.convertToNumber(otCheck.getApprovedDayOT());
                                                        EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                                                        adjustment.setEmployee(emp);
                                                        adjustment.setVacation(empDA.getWorkingCalendar().getCalendar().getRules().get(0).getVacation());
                                                        adjustment.setCreationDate(empDA.getDailyDate());
                                                        adjustment.setAdjustmentValue(new BigDecimal(ot1 / 8));
                                                        adjustment.setComments("this record has been added automatically for approved overtime");
                                                        oem.saveEntity(adjustment, loggedUser);
                                                    }
                                                    if (otCheck.getApprovedNightOT() != null && !otCheck.getApprovedNightOT().equals("00:00")) {
                                                        if (dateTimeUtility.compareTime(otCheck.getNightOTIn(), empDA.getPlannedIn()) < 1) {
                                                            double ot1 = dateTimeUtility.convertToNumber(otCheck.getApprovedNightOT());
                                                            EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                                                            adjustment.setEmployee(emp);
                                                            adjustment.setCreationDate(empDA.getDailyDate());
                                                            adjustment.setVacation(empDA.getWorkingCalendar().getCalendar().getRules().get(0).getVacation());
                                                            adjustment.setAdjustmentValue(new BigDecimal(ot1 / 8));
                                                            adjustment.setComments("this record has been added automatically for approved overtime");
                                                            oem.saveEntity(adjustment, loggedUser);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    overTimes.add(getOverTime(entries.get(j).getIn(), entries.get(j).getOut(), planIn, planOut, employeeOTRequests,
                                            segments.get(i),
                                            minOT, isEntityChangeField));
                                }
                            } else if (dateExp != null) {
                                for (int i = 0; i < dateExcs.size(); i++) {
                                    overTimes.add(getOverTime(entries.get(j).getIn(), entries.get(j).getOut(), planIn, planOut, employeeOTRequests,
                                            dateExcs.get(i), minOT, isEntityChangeField));
                                }
                            } else if (dayExcs != null) {
                                for (int i = 0; i < dayExcs.size(); i++) {
                                    overTimes.add(getOverTime(entries.get(j).getIn(), entries.get(j).getOut(), planIn, planOut, employeeOTRequests,
                                            dayExcs.get(i), minOT, isEntityChangeField));
                                }
                            }

                            for (int i = 0; i < overTimes.size(); i++) {
                                nonPlanned = dateTimeUtility.addTime(nonPlanned, overTimes.get(i).getNonPlanned());
                                notApprovedDayOT = dateTimeUtility.addTime(notApprovedDayOT, overTimes.get(i).getNotApprovedDayOT());
                                notApprovedNightOT = dateTimeUtility.addTime(notApprovedNightOT, overTimes.get(i).getNotApprovedNightOT());
                                approvedDayOT = dateTimeUtility.addTime(approvedDayOT, overTimes.get(i).getApprovedDayOT());
                                approvedNightOT = dateTimeUtility.addTime(approvedNightOT, overTimes.get(i).getApprovedNightOT());
                                if ((overTimes.get(i).getApprovedDayOT() != null && !overTimes.get(i).getApprovedDayOT().equals("00:00")) || (overTimes.get(i).getNotApprovedDayOT() != null && !overTimes.get(i).getNotApprovedDayOT().equals("00:00"))) {
                                    dayOTIn = overTimes.get(i).getDayOTIn();
                                    dayOTOut = overTimes.get(i).getDayOTOut();
                                }
                                if ((overTimes.get(i).getApprovedNightOT() != null && !overTimes.get(i).getApprovedNightOT().equals("00:00")) || (overTimes.get(i).getNotApprovedNightOT() != null && !overTimes.get(i).getNotApprovedNightOT().equals("00:00"))) {
                                    nightOTIn = overTimes.get(i).getNightOTIn();
                                    nightOTOut = overTimes.get(i).getNightOTOut();
                                }

                            }
                        }
                        //sewedy work overtime
                        //check position categories and reset overtime if not eligible
                        if (eph != null) {
                            if (isWorkOTEligible(empDA, eph)) {
                                String otAfter = "00:00";
                                String otBefore = "00:00";
                                for (int i = 0; i < overTimes.size(); i++) {
                                    if (overTimes.get(i).getApprovedDayOT() != null && !overTimes.get(i).getApprovedDayOT().equals("00:00")) {
                                        if (dateTimeUtility.compareTime(overTimes.get(i).getDayOTIn(), empDA.getPlannedIn()) < 1) {
                                            otBefore = dateTimeUtility.addTime(otBefore, overTimes.get(i).getApprovedDayOT());
                                        } else {
                                            otAfter = dateTimeUtility.addTime(otAfter, overTimes.get(i).getApprovedDayOT());
                                        }

                                    }
                                    if (overTimes.get(i).getApprovedNightOT() != null && !overTimes.get(i).getApprovedNightOT().equals("00:00")) {
                                        if (dateTimeUtility.compareTime(overTimes.get(i).getNightOTIn(), empDA.getPlannedIn()) < 1) {
                                            otBefore = dateTimeUtility.addTime(otBefore, overTimes.get(i).getApprovedNightOT());
                                        } else {
                                            otAfter = dateTimeUtility.addTime(otAfter, overTimes.get(i).getApprovedNightOT());
                                        }

                                    }
                                }

                                if (!empDA.getWorkingCalendar().getCalendar().getRules().isEmpty()) {
                                    if (empDA.getWorkingCalendar().getCalendar().getRules().get(0).isVacationCompensation()) {
                                        if (!otAfter.equals("00:00")) {
                                            double ot1 = dateTimeUtility.convertToNumber(otAfter);
                                            if (ot1 >= empDA.getWorkingCalendar().getCalendar().getRules().get(0).getOtTolerance()) {
                                                EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                                                adjustment.setEmployee(emp);
                                                adjustment.setVacation(empDA.getWorkingCalendar().getCalendar().getRules().get(0).getVacation());
                                                adjustment.setCreationDate(empDA.getDailyDate());
                                                adjustment.setAdjustmentValue(new BigDecimal(ot1 / 8));
                                                adjustment.setComments("this record has been added automatically for approved overtime");
                                                oem.saveEntity(adjustment, loggedUser);
                                            }
                                        }
                                        if (!otBefore.equals("00:00")) {
                                            double ot2 = dateTimeUtility.convertToNumber(otBefore);
                                            if (ot2 >= empDA.getWorkingCalendar().getCalendar().getRules().get(0).getOtTolerance()) {
                                                EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                                                adjustment.setEmployee(emp);
                                                adjustment.setVacation(empDA.getWorkingCalendar().getCalendar().getRules().get(0).getVacation());
                                                adjustment.setCreationDate(empDA.getDailyDate());
                                                adjustment.setAdjustmentValue(new BigDecimal(ot2 / 8));
                                                adjustment.setComments("this record has been added automatically for approved overtime");
                                                oem.saveEntity(adjustment, loggedUser);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                totalOT = dateTimeUtility.addTime(approvedNightOT,
                        dateTimeUtility.addTime(approvedDayOT,
                                dateTimeUtility.addTime(notApprovedDayOT, notApprovedNightOT)));
                if (dateTimeUtility.compareTime(totalOT, minOT) < 0) {
                    notApprovedDayOT = "00:00";
                    notApprovedNightOT = "00:00";
                    approvedDayOT = "00:00";
                    approvedNightOT = "00:00";
                }

                // Actual & Planned Hours Calculation
                // in the case of flex calendar with # of hrs & plan in/out, then plannedWork = # of hrs of the calendar
                if (calType.getCode().equals("Flexible") && !expectedOut.equals("-")) {
                    plannedWork = numHrsStr;
                } else {
                    plannedWork = calculatePlannedWork(planIn, planOut, planBreakIn, planBreakOut);
                    actualWork = calculateActualWork(timeIn, timeOut, breakIn, breakOut);

                    // Less Work Calculation
                    if (plannedWork != null && actualWork != null && !plannedWork.equals("") && !actualWork.equals("")) {
                        if (dateTimeUtility.compareTime(plannedWork, actualWork) == 1) {
                            lessWork = dateTimeUtility.subtractTime(actualWork, plannedWork);
                        }
                    }
                }

                // if the nursery hours is floating, then the delay & early leave will be zeros
                // and instead of them the less work will be calculated and the nursery hour will be deducted from the less work
                if (isNurseryEligible && nurseryType.equals("floating")
                        && !calType.getCode().equals("Flexible")) {

                    lessWork = dateTimeUtility.addTime(delay, earlyLeave);

                    if (dateTimeUtility.compareTime(lessWork, "01:00") >= 0) {
                        lessWork = dateTimeUtility.subtractTime("01:00", lessWork);
                    } else {
                        lessWork = "00:00";
                    }
                    delay = "00:00";
                    earlyLeave = "00:00";
                } else if (!(calType.getCode().equals("Flexible") && !expectedOut.equals("-"))) {
                    lessWork = "00:00";
                }
            } else if (empDA.getDayNature().getCode().equals("Mission")) {
                lessWork = "00:00";
                delay = "00:00";
                earlyLeave = "00:00";
                notApprovedDayOT = "00:00";
                notApprovedNightOT = "00:00";
                approvedDayOT = "00:00";
                approvedNightOT = "00:00";
                holidayOrWeekendApprovedOT = "00:00";
                holidayOrWeekendNotApprovedOT = "00:00";
                actualWork = "00:00";
                plannedWork = "00:00";
            }

            // Setting the calculations' values in the daily attendance
            if (holidayOrWeekendApprovedOT.equals("0")) {
                holidayOrWeekendApprovedOT = "00:00";
            }
            if (workingOnHoliday) {
                empDA.setApprovedHolidayOT(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendApprovedOT)).setScale(5, RoundingMode.UP));
                empDA.setApprovedHolidayOTDisplay(holidayOrWeekendApprovedOT);
                empDA.setNotApprovedHolidayOT(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendNotApprovedOT)).setScale(5, RoundingMode.UP));
                empDA.setNotApprovedHolidayDisplay(holidayOrWeekendNotApprovedOT);
            }

            if (workingOnWeekend) {
                empDA.setApprovedWeekendOT(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendApprovedOT)).setScale(5, RoundingMode.UP));
                empDA.setApprovedWeekendOTDisplay(holidayOrWeekendApprovedOT);
                empDA.setNotApprovedWeekendOT(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendNotApprovedOT)).setScale(5, RoundingMode.UP));
                empDA.setNotApprovedWeekendDisplay(holidayOrWeekendNotApprovedOT);

            }
            // To-Do : Check With Loubna Why We Need This Check?.
//            if (normalCal == null) {
//                ofr.addError(usrMsgService.getUserMessage("MissingCalendar", loggedUser));
//                return ofr;
//            }
            empDA.setTimeIn(displayTimeIn);
            empDA.setTimeOut(displaytimeOut);
            empDA.setBreakIn(breakIn);
            empDA.setBreakOut(displayBreakOut);
            if (!empDA.getDayNature().getCode().equals("Vacation")) {
                if (!outOfScope) {
                    empDA.setDayNature(workingDayNature);
                } else {
                    conds.clear();
                    conds.add("type.code='900'");
                    conds.add("code='Absence'");
                    UDC AbsenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    empDA.setDayNature(AbsenceDayNature);
                }
            } else {
                lessWork = "00:00";
                delay = "00:00";
                earlyLeave = "00:00";
                notApprovedDayOT = "00:00";
                notApprovedNightOT = "00:00";
                approvedDayOT = "00:00";
                approvedNightOT = "00:00";
                holidayOrWeekendApprovedOT = "00:00";
                holidayOrWeekendNotApprovedOT = "00:00";
                actualWork = "00:00";
                plannedWork = "00:00";
            }
            // reset the OT in both holiday & weekend
            if (!workingOnHoliday) {
                empDA.setApprovedHolidayOT(BigDecimal.ZERO);
                empDA.setNotApprovedHolidayOTMask(BigDecimal.ZERO);
                empDA.setApprovedHolidayOTDisplay("00:00");
                empDA.setNotApprovedHolidayDisplay("00:00");
            }
            if (!workingOnWeekend) {
                empDA.setApprovedWeekendOT(BigDecimal.ZERO);
                empDA.setNotApprovedWeekendOTMask(BigDecimal.ZERO);
                empDA.setApprovedWeekendOTDisplay("00:00");
                empDA.setNotApprovedWeekendDisplay("00:00");
            }

            empDA.setDelayIn(delay);
            empDA.setDelayHoursPeriod((new BigDecimal(dateTimeUtility.convertToNumber(delay).toString())).setScale(5, RoundingMode.HALF_UP));
            empDA.setDelayHoursDisplay(delay);

            empDA.setEarlyLeaveOut(earlyLeave);
            empDA.setEarlyLeavePeriod((new BigDecimal(dateTimeUtility.convertToNumber(earlyLeave).toString())).setScale(5, RoundingMode.HALF_UP));
            empDA.setEarlyLeaveDisplay(earlyLeave);

            empDA.setNonPlanned(nonPlanned);

            empDA.setNotApprovedDay(notApprovedDayOT);
            empDA.setNotApprovedNight(notApprovedNightOT);

            empDA.setDayOverTimePeriod((new BigDecimal(dateTimeUtility.convertToNumber(approvedDayOT).toString())).setScale(5, RoundingMode.HALF_UP));
            empDA.setDayOverTimeDisplay(approvedDayOT);
            empDA.setNightOverTimePeriod((new BigDecimal(dateTimeUtility.convertToNumber(approvedNightOT).toString())).setScale(5, RoundingMode.HALF_UP));
            empDA.setNightOverTimeDisplay(approvedNightOT);

            if (workingOnHoliday || workingOnWeekend) {
                empDA.setPlannedIn("00:00");
                empDA.setPlannedOut("00:00");
            } else {
                empDA.setPlannedIn(planIn);
                empDA.setPlannedOut(displayPlanOut);
            }

            empDA.setLessWorkIn(lessWork);

            empDA.setLessWorkPeriod((new BigDecimal(dateTimeUtility.convertToNumber(lessWork).toString())).setScale(5, RoundingMode.HALF_UP));
            empDA.setLessWorkDisplay(lessWork);

            empDA.setWorkingHours((new BigDecimal(dateTimeUtility.convertToNumber(actualWork).toString())).setScale(5, RoundingMode.HALF_UP));

            //_____________________________________
            // if (exceptionFrom != null && exceptionTo != null) {
            //   exceptionHours = dateTimeUtility.subtractTime(exceptionFrom, exceptionTo);
            // }
            if (exceptionHours == null || (exceptionHours != null && exceptionHours.equals("00:00"))) {
                for (int i = 0; i < exceptionFrom.size(); i++) {
                    if (exceptionFrom.get(i) != null && exceptionTo.get(i) != null) {
                        exceptionHours = dateTimeUtility.addTime(exceptionHours,
                                dateTimeUtility.subtractTime(exceptionFrom.get(i), exceptionTo.get(i)));
                    }
                }
            }
            //_____________________________________
            if (exceptionHours == null) {
                exceptionHours = "00:00";
            }
            // commented by loubna - based on new req for the product, the mission with type hours does not change the nature of the day
            /*if (!exceptionHours.equals("00:00")
             && (empDA.getDayNature().getCode().equalsIgnoreCase("absence"))) {
             payrollParameterDsc = "MissionVacationsDayNatureEnabled";
             payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
             if (payrollParameterResult[0] != null
             && payrollParameterResult[0].equalsIgnoreCase("y")) {

             conds.clear();
             conds.add("type.code='900'");
             conds.add("code='Mission'");
             UDC MissionDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
             empDA.setDayNature(MissionDayNature);
             }

             }
             if (exceptionFrom != null && exceptionTo != null && exceptionHours.equals("00:00")) {
             exceptionHours = dateTimeUtility.subtractTime(exceptionFrom, exceptionTo);
             }
             */

            empDA.setExceptionHours((new BigDecimal(dateTimeUtility.convertToNumber(exceptionHours).toString())).setScale(5, RoundingMode.HALF_UP));

            if (!odm.getODataType().getName().equals("EntityChangeField")) {
                if ((timeIn == null || timeIn.equals("")) && (timeOut == null || timeOut.equals(""))) {
                    boolean resetDay = false;
                    if (empDA.getDayNature().getCode().equals("Working")) {
                        conds.clear();
                        conds = new ArrayList<String>();
                        conds.add("type.code='900'");
                        conds.add("code='Absence'");
                        UDC AbsenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                        empDA.setDayNature(AbsenceDayNature);
                        resetDay = true;
                    } else if (empDA.getDayNature().getCode().equals("WorkingOnHoliday")) {
                        conds.clear();
                        conds = new ArrayList<String>();
                        conds.add("type.code='900'");
                        conds.add("code='Holiday'");
                        UDC dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                        empDA.setDayNature(dayNature);
                        resetDay = true;

                        empDA.setNotApprovedHolidayOT(BigDecimal.ZERO);
                        empDA.setApprovedHolidayOT(BigDecimal.ZERO);
                    } else if (empDA.getDayNature().getCode().equals("WorkingOnWeekEnd")) {
                        conds.clear();
                        conds = new ArrayList<String>();
                        conds.add("type.code='900'");
                        conds.add("code='WeekEnd'");
                        UDC dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                        empDA.setDayNature(dayNature);
                        resetDay = true;

                        empDA.setNotApprovedWeekendOT(BigDecimal.ZERO);
                        empDA.setApprovedWeekendOT(BigDecimal.ZERO);
                    }
                    if (resetDay) {
                        empDA.setTimeIn(null);
                        empDA.setTimeOut(null);
                        empDA.setPlannedOut(null);
                        empDA.setPlannedIn(null);
                        empDA.setDelayHoursPeriod(null);
                        empDA.setEarlyLeavePeriod(null);
                        empDA.setNightOverTimePeriod(null);
                        empDA.setDayOverTimePeriod(null);
                        empDA.setNotApprovedNight(null);
                        empDA.setNotApprovedDay(null);
                        empDA.setLessWorkPeriod(null);
                    }
                }
            }

            boolean checkEL = true;
            //Porta Special Case For Mandatory OT Hour ==> loubna20140610
            if (normalCal != null) {
                conds.clear();
                conds.add("calendar.dbid = " + normalCal.getDbid());
                conds.add("mandatory = true");
                mandatorySegs = oem.loadEntityList(OTSegment.class.getSimpleName(), conds, null, null, loggedUser);
                String mandatoryHrs;
                BigDecimal newDelay = new BigDecimal(dateTimeUtility.convertToNumber(delay));
                BigDecimal newEarlyLeave = new BigDecimal(dateTimeUtility.convertToNumber(earlyLeave));
                String otAfterEalryLeave;
                BigDecimal otAfterEalryLeaveHrs = null;

                conds.clear();
                conds.add("calendar.dbid = " + normalCal.getDbid());
                earlyLeaveMandOTSegs = oem.loadEntityList(EarlyLeaveMandOTSegment.class.getSimpleName(),
                        conds, null, null, loggedUser);

                if (!mandatorySegs.isEmpty()) {
                    mandatoryOT = BigDecimal.ZERO;
                    //Check Delay With OT
                    mandatoryHrs = dateTimeUtility.subtractTime(mandatorySegs.get(0).getFromTime(), mandatorySegs.get(0).getToTime());
                    mandatoryOT = mandatoryOT.add((new BigDecimal(dateTimeUtility.convertToNumber(mandatoryHrs))).setScale(5, RoundingMode.HALF_UP));

                    if (mandatoryOT.doubleValue() > 0) {
                        if (dateTimeUtility.convertToNumber(delay) > 0) {
                            //mandatoryOT = BigDecimal.ZERO;
                            if ((dateTimeUtility.convertToNumber(delay) + dateTimeUtility.convertToNumber(tolerence)) > mandatoryOT.doubleValue()) {
                                newDelay = new BigDecimal((dateTimeUtility.convertToNumber(delay) + dateTimeUtility.convertToNumber(tolerence)) - mandatoryOT.doubleValue()).setScale(5, RoundingMode.HALF_UP);
                            } else {
                                newDelay = BigDecimal.ZERO;
                            }
                            mandatoryOT = BigDecimal.ZERO;
                            checkEL = false;

                            empDA.setDelayHoursPeriod(newDelay);
                            delay = dateTimeUtility.convertToTime(newDelay.doubleValue());

                            if (mandatorySegs.get(0).getOtType().getCode().equalsIgnoreCase("day")) {
                                empDA.setDayOverTimePeriod(empDA.getDayOverTimePeriod().add(mandatoryOT));
                                approvedDayOT = dateTimeUtility.convertToTime(empDA.getDayOverTimePeriod().doubleValue());
                            } else {
                                empDA.setNightOverTimePeriod(empDA.getNightOverTimePeriod().add(mandatoryOT));
                                approvedNightOT = dateTimeUtility.convertToTime(empDA.getNightOverTimePeriod().doubleValue());
                            }
                        }
                    }

                    //Check Early Leave With OT
                    if (checkEL) {
                        mandatoryOT = (new BigDecimal(dateTimeUtility.convertToNumber(mandatoryHrs))).setScale(5, RoundingMode.HALF_UP);
                        if ((dateTimeUtility.convertToNumber(earlyLeave)) > mandatoryOT.doubleValue()) {
                            newEarlyLeave = new BigDecimal((dateTimeUtility.convertToNumber(earlyLeave)) - mandatoryOT.doubleValue());
                        } else {
                            newEarlyLeave = BigDecimal.ZERO;
                        }
                        empDA.setEarlyLeavePeriod(newEarlyLeave.setScale(5, RoundingMode.HALF_UP));

                        for (int i = 0; i < earlyLeaveMandOTSegs.size(); i++) {
                            if (dateTimeUtility.compareTime(earlyLeave, earlyLeaveMandOTSegs.get(i).getFromTime()) >= 0
                                    && dateTimeUtility.compareTime(earlyLeave, earlyLeaveMandOTSegs.get(i).getToTime()) <= 0) {
                                otAfterEalryLeave = earlyLeaveMandOTSegs.get(i).getOtPeriod();
                                otAfterEalryLeaveHrs = (new BigDecimal((dateTimeUtility.convertToNumber(otAfterEalryLeave)))).setScale(5, RoundingMode.HALF_UP);

                                if (mandatorySegs.get(0).getOtType().getCode().equalsIgnoreCase("day")) {
                                    empDA.setDayOverTimePeriod(empDA.getDayOverTimePeriod().add(otAfterEalryLeaveHrs));
                                    approvedDayOT = dateTimeUtility.convertToTime(empDA.getDayOverTimePeriod().doubleValue());
                                } else {
                                    empDA.setNightOverTimePeriod(empDA.getNightOverTimePeriod().add(otAfterEalryLeaveHrs));
                                    approvedNightOT = dateTimeUtility.convertToTime(empDA.getNightOverTimePeriod().doubleValue());
                                }
                            }
                        }
                        earlyLeave = dateTimeUtility.convertToTime(newEarlyLeave.doubleValue());
                    }
                }
            }

            //Less work during the day
            if (empDA.getDayNature().getValue().equalsIgnoreCase("working") && emp.getPositionSimpleMode().isCalculateDayLessWork()) {

                String dayLessWorkValue = calculateLessWorkDuringTheDay(timeIn, timeOut, emp, entryDate, loggedUser);
                if (!dayLessWorkValue.equals("00:00")) {
                    if (empDA.getWorkingCalendar().getDayLessWorkTolerence() == null) {
                        empDA.getWorkingCalendar().setDayLessWorkTolerence(new BigDecimal(0));
                    }
                    String dayLessWorkTolerence = dateTimeUtility.convertMinutesToTime(empDA.getWorkingCalendar().getDayLessWorkTolerence().doubleValue());
                    if (empDA.getWorkingCalendar().isIgnoreDayLessWorkTolerence()) {
                        if (dateTimeUtility.compareTime(dayLessWorkValue, dayLessWorkTolerence) != 1) {
                            empDA.setDayLessWork("00:00");
                            empDA.setDayLessWorkPeriod(new BigDecimal(0));
                        } else {
                            empDA.setDayLessWork(dayLessWorkValue);
                            empDA.setDayLessWorkPeriod((new BigDecimal(dateTimeUtility.convertToNumber(dayLessWorkValue).toString())).setScale(5, RoundingMode.HALF_UP));
                        }
                    } else {

                        BigDecimal val = (new BigDecimal(dateTimeUtility.convertToNumber(dayLessWorkValue).toString())).setScale(5, RoundingMode.HALF_UP).multiply(new BigDecimal(60)).subtract(empDA.getWorkingCalendar().getDayLessWorkTolerence());
                        empDA.setDayLessWork(dateTimeUtility.subtractTime(dayLessWorkTolerence, dayLessWorkValue));
                        empDA.setDayLessWorkPeriod(val.divide(new BigDecimal(60)));

                    }
                } else {
                    empDA.setDayLessWork("00:00");
                    empDA.setDayLessWorkPeriod(new BigDecimal(0));
                }
            } else {
                empDA.setDayLessWork("00:00");
                empDA.setDayLessWorkPeriod(new BigDecimal(0));
            }
            boolean isWeekendOTEligible = false;
            //weekend Eligibility
            empDA.setWeOT(BigDecimal.ZERO);
            empDA.setDayWEOT(BigDecimal.ZERO);
            empDA.setNightWEOT(BigDecimal.ZERO);
            if (empDA.getDayNature().getCode().contains("WeekEnd")) {
                if (empDA.getDayNature() != null) {
                    if ((eph.getPosition().getCategory() != null
                            && eph.getPosition().getCategory().getCustom1() != null
                            && eph.getPosition().getCategory().getCustom1().contains("M"))
                            || (eph.getType() != null && (eph.getType().getCode().equals("1")
                            || eph.getType().getCode().equals("2")
                            || eph.getType().getCode().equals("3")))) {
                        empDA.setNotApprovedWeekendOT(empDA.getNotApprovedWeekendOT().add(empDA.getApprovedWeekendOT()).setScale(5, RoundingMode.HALF_UP));
                        empDA.setNotApprovedWeekendDisplay(dateTimeUtility.addTime(empDA.getApprovedWeekendOTDisplay(), empDA.getNotApprovedWeekendDisplay()));
                        empDA.setApprovedWeekendOT(new BigDecimal(0));
                        empDA.setApprovedWeekendOTDisplay("00:00");
                    } else {
                        isWeekendOTEligible = true;
                        Rule tmRule = (Rule) oem.loadEntity(Rule.class.getSimpleName(), Collections.singletonList("tmCalendar.dbid = " + empDA.getWorkingCalendar().getCalendar().getDbid()), null, loggedUser);
                        if (tmRule != null) {
                            String sql = "DELETE FROM EmployeeVacationAdjustment"
                                    + " where employee_dbid = " + empDA.getEmployee().getDbid()
                                    + " and CREATIONDATE = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()))
                                    + " and Vacation_dbid = " + tmRule.getEffectOnVacationCompWEOT().getDbid();
                            oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                        }
                        if (tmRule != null && tmRule.isVacationCompensationWEOT()) {
                            double weekEndOverTimeHours = empDA.getApprovedWeekendOT().doubleValue();
                            double maxHoursComp = tmRule.getMaxCompWEOT().doubleValue();
                            if (weekEndOverTimeHours != 0) {
                                EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                                adjustment.setEmployee(emp);
                                adjustment.setVacation(tmRule.getEffectOnVacationCompWEOT());
                                adjustment.setCreationDate(empDA.getDailyDate());
                                adjustment.setComments("this record has been added automatically for approved weekend overtime");
                                if (weekEndOverTimeHours >= 8) {
                                    adjustment.setAdjustmentValue(BigDecimal.ONE);
                                    oem.saveEntity(adjustment, loggedUser);
                                } else {
                                    empDA.setWeOT(new BigDecimal(weekEndOverTimeHours / 8));
                                    adjustment.setAdjustmentValue(new BigDecimal(weekEndOverTimeHours / 8));
                                    oem.saveEntity(adjustment, loggedUser);
                                }
                                if (weekEndOverTimeHours > maxHoursComp) {
                                    empDA.setWeOT(new BigDecimal(maxHoursComp));
                                    double remainWE = weekEndOverTimeHours - maxHoursComp;
                                    if (remainWE <= tmRule.getMaxPayrollDayWEOT().doubleValue()) {
                                        empDA.setDayWEOT(new BigDecimal(remainWE));
                                    } else {
                                        empDA.setDayWEOT(tmRule.getMaxPayrollDayWEOT());
                                        if (remainWE - tmRule.getMaxPayrollDayWEOT().doubleValue() >= tmRule.getMaxPayrollNightWEOT().doubleValue()) {
                                            empDA.setNightWEOT(tmRule.getMaxPayrollNightWEOT());
                                        } else {
                                            empDA.setNightWEOT(new BigDecimal(remainWE - tmRule.getMaxPayrollDayWEOT().doubleValue()));
                                        }
                                    }
                                } else {
                                    empDA.setWeOT(new BigDecimal(weekEndOverTimeHours));
                                }
                            }

                        }

                    }
                }
            }
            if (empDA.getWorkingCalendar().getCalendar().getRules().get(0).getHolidayVacationCompensation() != null && empDA.getDayNature().getCode().contains("Holiday")) {
                String sql = "DELETE FROM EmployeeVacationAdjustment"
                        + " where employee_dbid = " + empDA.getEmployee().getDbid()
                        + " and CREATIONDATE = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()))
                        + " and Vacation_dbid = " + empDA.getWorkingCalendar().getCalendar().getRules().get(0).getHolidayVacationCompensation().getDbid();
                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
            }
            if (eph != null) {
                if (!isWorkOTEligible(empDA, eph)) {
                    empDA.setDayOverTimePeriod(new BigDecimal(0));
                    empDA.setDayOverTimeDisplay("00:00");
                    empDA.setDayOverTimePeriodMask(new BigDecimal(0));
                    empDA.setDayOverTimeIn("00:00");
                    empDA.setDayOverTimeOut("00:00");
                    empDA.setNightOverTimePeriod(new BigDecimal(0));
                    empDA.setNightOverTimeDisplay("00:00");
                    empDA.setNightOverTimePeriodMask(new BigDecimal(0));
                    empDA.setNightOverTimeIn("00:00");
                    empDA.setNightOverTimeOut("00:00");
                    empDA.setNotApprovedDay("00:00");
                    empDA.setNotApprovedNight("00:00");
                }
                if (empDA.getWorkingCalendar().getCalendar().getRules().get(0) != null && empDA.getWorkingCalendar().getCalendar().getRules().get(0).isHolidayOTCompendsation()) {
                    if (!isHolidayOTCompensationEligible(eph) && !isHolidayOTPayrollEligible(eph)) {
                        empDA.setApprovedHolidayOT(BigDecimal.ZERO);
                        empDA.setNotApprovedHolidayOTMask(BigDecimal.ZERO);
                        empDA.setApprovedHolidayOTDisplay("00:00");
                        empDA.setNotApprovedHolidayDisplay("00:00");
                    } else if (isHolidayOTCompensationEligible(eph) && empDA.getApprovedHolidayOT() != null && empDA.getApprovedHolidayOT().compareTo(BigDecimal.ZERO) != 0) {
                        EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                        adjustment.setEmployee(emp);
                        adjustment.setVacation(empDA.getWorkingCalendar().getCalendar().getRules().get(0).getHolidayVacationCompensation());
                        adjustment.setCreationDate(empDA.getDailyDate());
                        adjustment.setAdjustmentValue(BigDecimal.valueOf(empDA.getWorkingCalendar().getCalendar().getRules().get(0).getHolidayCompendsationValue()));
                        adjustment.setComments("this record has been added automatically for approved holiday overtime");
                        oem.saveEntity(adjustment, loggedUser);
                    } else if (isHolidayOTPayrollEligible(eph) && empDA.getApprovedHolidayOT() != null && empDA.getApprovedHolidayOT().compareTo(BigDecimal.ZERO) != 0) {
                        empDA.setHolidayPayrollOT(empDA.getApprovedHolidayOT());
                    } else {
                        empDA.setHolidayPayrollOT(BigDecimal.ZERO);
                    }
                }
            }
            boolean isHolidayOTEligible = false;
            if (isHolidayOTCompensationEligible(eph) || isHolidayOTPayrollEligible(eph)) {
                isHolidayOTEligible = true;
            }
            //Sewedy Weekend Mission Compensation
            weekendMissionVacationCompensation(empDA, isWeekendOTEligible, loggedUser);
            //Sewedy Holiday Mission Compensation
            holidayMissionVacationCompensation(empDA, isHolidayOTEligible, loggedUser);

            //<editor-fold defaultstate="collapsed" desc="recalculation of monthlyConslidation">
            {
                if (!odm.getODataType().getName().equals("EntityChangeField")) {

                    // Loubna - 05/12/2013 - update header record
                    BigDecimal totalDelay = new BigDecimal(0),
                            totalEarly = new BigDecimal(0),
                            totalDOT = new BigDecimal(0),
                            totalNOT = new BigDecimal(0),
                            totalLessWork = new BigDecimal(0),
                            totalWork = new BigDecimal(0);
                    String totalNonAppDOT = "00:00",
                            totalNonAppNOT = "00:00";

                    totalDelay = (new BigDecimal(dateTimeUtility.convertToNumber(delay).toString())).setScale(5, RoundingMode.HALF_UP);
                    totalEarly = (new BigDecimal(dateTimeUtility.convertToNumber(earlyLeave).toString())).setScale(5, RoundingMode.HALF_UP);
                    totalDOT = (new BigDecimal(dateTimeUtility.convertToNumber(approvedDayOT).toString())).setScale(5, RoundingMode.HALF_UP);
                    totalNOT = (new BigDecimal(dateTimeUtility.convertToNumber(approvedNightOT).toString())).setScale(5, RoundingMode.HALF_UP);

                    totalWork = new BigDecimal(dateTimeUtility.convertToNumber(actualWork)).setScale(5, RoundingMode.UP);

                    totalLessWork = (new BigDecimal(dateTimeUtility.convertToNumber(lessWork).toString())).setScale(5, RoundingMode.HALF_UP);
                    totalNonAppDOT = notApprovedDayOT;
                    totalNonAppNOT = notApprovedNightOT;

                    loadedEmpDA.setDelayHoursPeriod(totalDelay);
                    loadedEmpDA.setEarlyLeavePeriod(totalEarly);
                    loadedEmpDA.setLessWorkPeriod(totalLessWork);
                    loadedEmpDA.setDayOverTimePeriod(totalDOT);
                    loadedEmpDA.setNightOverTimePeriod(totalNOT);
                    loadedEmpDA.setPlannedIn(planIn);
                    loadedEmpDA.setPlannedOut(planOut);
                    loadedEmpDA.setNotApprovedNight(totalNonAppNOT);
                    loadedEmpDA.setNotApprovedDay(totalNonAppDOT);
                    loadedEmpDA.setDayNature(empDA.getDayNature());
                    loadedEmpDA.setDiffValue(totalWork);
                    loadedEmpDA.setWorkingHours(totalWork);
                    //For Log
                    conds.clear();
                    conds.add("description = 'BusinessAuditLog'");
                    PayrollParameter logParameter = null;
                    try {
                        logParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (logParameter.getValueData().equalsIgnoreCase("Y")) {
                        oem.executeEntityUpdateNativeQuery("delete from EMPLOYEEDAILYATTENDANCEAUDIT where EMPLOYEEDAILYATTENDANCE_DBID=" + loadedEmpDA.getDbid(), loggedUser);
                    }
                    if (workingOnHoliday) {
                        if (logParameter.getValueData().equalsIgnoreCase("Y")) {
                            if (holidayOrWeekendApprovedOT != null && !holidayOrWeekendApprovedOT.equals("00:00")) {
                                EmployeeDailyAttendanceAudit approvedHolidayOvertimeAudit = new EmployeeDailyAttendanceAudit();
                                approvedHolidayOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                                approvedHolidayOvertimeAudit.setSequence(9);
                                if (tmCalendar.getMaximumHolidayOT() != null && !tmCalendar.getMaximumHolidayOT().equals("00:00")) {
                                    approvedHolidayOvertimeAudit.setStartTime(timeIn);
                                    approvedHolidayOvertimeAudit.setEndTime(dateTimeUtility.addTime(timeIn, tmCalendar.getMaximumHolidayOT()));
                                    approvedHolidayOvertimeAudit.setPeriod(holidayOrWeekendApprovedOT);
                                    approvedHolidayOvertimeAudit.setType("Approved Holiday Overtime");
                                    oem.saveEntity(approvedHolidayOvertimeAudit, loggedUser);

                                } else {
                                    approvedHolidayOvertimeAudit.setStartTime(timeIn);
                                    approvedHolidayOvertimeAudit.setEndTime(timeOut);
                                    approvedHolidayOvertimeAudit.setPeriod(holidayOrWeekendApprovedOT);
                                    approvedHolidayOvertimeAudit.setType("Approved Holiday Overtime");
                                    oem.saveEntity(approvedHolidayOvertimeAudit, loggedUser);

                                }
                            }
                        }
                        if (logParameter.getValueData().equalsIgnoreCase("Y")) {
                            if (holidayOrWeekendNotApprovedOT != null && !holidayOrWeekendNotApprovedOT.equals("00:00")) {
                                EmployeeDailyAttendanceAudit notApprovedHolidayOvertimeAudit = new EmployeeDailyAttendanceAudit();
                                notApprovedHolidayOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                                notApprovedHolidayOvertimeAudit.setSequence(10);
                                if (holidayOrWeekendApprovedOT != null && !holidayOrWeekendApprovedOT.equals("00:00")) {
                                    notApprovedHolidayOvertimeAudit.setStartTime(dateTimeUtility.subtractTime(holidayOrWeekendNotApprovedOT, timeOut));
                                } else {
                                    notApprovedHolidayOvertimeAudit.setStartTime(timeIn);
                                }
                                notApprovedHolidayOvertimeAudit.setEndTime(timeOut);
                                notApprovedHolidayOvertimeAudit.setPeriod(holidayOrWeekendNotApprovedOT);
                                notApprovedHolidayOvertimeAudit.setType("Not Approved Holiday Overtime");
                                oem.saveEntity(notApprovedHolidayOvertimeAudit, loggedUser);

                            }
                        }
                        loadedEmpDA.setApprovedHolidayOT(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendApprovedOT).toString()).setScale(5, RoundingMode.UP));
                        loadedEmpDA.setNotApprovedHolidayOTMask(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendNotApprovedOT).toString()).setScale(5, RoundingMode.UP));
                    } else {

                        loadedEmpDA.setApprovedHolidayOT(BigDecimal.ZERO);
                        loadedEmpDA.setNotApprovedHolidayOTMask(BigDecimal.ZERO);
                    }

                    if (workingOnWeekend) {
                        if (logParameter.getValueData().equalsIgnoreCase("Y")) {
                            if (holidayOrWeekendApprovedOT != null && !holidayOrWeekendApprovedOT.equals("00:00")) {
                                EmployeeDailyAttendanceAudit approvedWeekEndOvertimeAudit = new EmployeeDailyAttendanceAudit();
                                approvedWeekEndOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                                approvedWeekEndOvertimeAudit.setSequence(7);
                                if (tmCalendar.getMaximumWeekEndOT() != null && !tmCalendar.getMaximumWeekEndOT().equals("00:00")) {
                                    approvedWeekEndOvertimeAudit.setStartTime(timeIn);
                                    approvedWeekEndOvertimeAudit.setEndTime(dateTimeUtility.addTime(timeIn, tmCalendar.getMaximumHolidayOT()));
                                    approvedWeekEndOvertimeAudit.setPeriod(holidayOrWeekendApprovedOT);
                                    approvedWeekEndOvertimeAudit.setType("Approved Weekend Overtime");
                                    oem.saveEntity(approvedWeekEndOvertimeAudit, loggedUser);
                                } else {
                                    approvedWeekEndOvertimeAudit.setStartTime(timeIn);
                                    approvedWeekEndOvertimeAudit.setEndTime(timeOut);
                                    approvedWeekEndOvertimeAudit.setPeriod(holidayOrWeekendApprovedOT);
                                    approvedWeekEndOvertimeAudit.setType("Approved Weekend Overtime");
                                    oem.saveEntity(approvedWeekEndOvertimeAudit, loggedUser);

                                }
                            }
                        }
                        if (logParameter.getValueData().equalsIgnoreCase("Y")) {
                            if (holidayOrWeekendNotApprovedOT != null && !holidayOrWeekendNotApprovedOT.equals("00:00")) {
                                EmployeeDailyAttendanceAudit notApprovedWeekEndOvertimeAudit = new EmployeeDailyAttendanceAudit();
                                notApprovedWeekEndOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                                notApprovedWeekEndOvertimeAudit.setSequence(8);
                                if (holidayOrWeekendApprovedOT != null && !holidayOrWeekendApprovedOT.equals("00:00")) {
                                    notApprovedWeekEndOvertimeAudit.setStartTime(dateTimeUtility.subtractTime(holidayOrWeekendNotApprovedOT, timeOut));

                                } else {
                                    notApprovedWeekEndOvertimeAudit.setStartTime(timeIn);
                                }

                                notApprovedWeekEndOvertimeAudit.setEndTime(timeOut);
                                notApprovedWeekEndOvertimeAudit.setPeriod(holidayOrWeekendNotApprovedOT);
                                notApprovedWeekEndOvertimeAudit.setType("Not Approved Weekend Overtime");
                                oem.saveEntity(notApprovedWeekEndOvertimeAudit, loggedUser);
                            }
                        }
                        loadedEmpDA.setApprovedWeekendOT(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendApprovedOT).toString()).setScale(5, RoundingMode.UP));
                        loadedEmpDA.setNotApprovedWeekendOTMask(new BigDecimal(dateTimeUtility.convertToNumber(holidayOrWeekendNotApprovedOT)).setScale(5, RoundingMode.UP));
                    } else {

                        loadedEmpDA.setApprovedWeekendOT(BigDecimal.ZERO);
                        loadedEmpDA.setNotApprovedWeekendOTMask(BigDecimal.ZERO);
                    }
                    String oldDayNatureDbid = oem.executeEntityNativeQuery("select DAYNATURE_DBID from EMPLOYEEDAILYATTENDANCE WHERE DBID = " + loadedEmpDA.getDbid(), loggedUser).toString();

                    conds.clear();
                    conds = new ArrayList<String>();
                    conds.add("dbid=" + oldDayNatureDbid);
                    UDC OlddayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    loadedEmpDA.setOldDayNature(OlddayNature);
                    empDA.setOldDayNature(OlddayNature);
                    //For logging
                    if (logParameter.getValueData().equalsIgnoreCase("Y")) {
                        if (delay != null && !delay.equals("00:00")) {
                            EmployeeDailyAttendanceAudit delayAudit = new EmployeeDailyAttendanceAudit();
                            delayAudit.setType("Delay");
                            delayAudit.setSequence(1);
                            delayAudit.setEmployeeDailyAttendance(loadedEmpDA);
                            delayAudit.setStartTime(planIn);
                            delayAudit.setEndTime(timeIn);
                            delayAudit.setPeriod(delay);
                            oem.saveEntity(delayAudit, loggedUser);
                        }

                        if (earlyLeave != null && !earlyLeave.equals("00:00")) {
                            EmployeeDailyAttendanceAudit earlyLeaveAudit = new EmployeeDailyAttendanceAudit();
                            earlyLeaveAudit.setType("Earlyleave");
                            earlyLeaveAudit.setSequence(2);
                            earlyLeaveAudit.setEmployeeDailyAttendance(loadedEmpDA);
                            earlyLeaveAudit.setStartTime(timeOut);
                            earlyLeaveAudit.setEndTime(planOut);
                            earlyLeaveAudit.setPeriod(earlyLeave);
                            oem.saveEntity(earlyLeaveAudit, loggedUser);
                        }
                        if (approvedDayOTList != null && approvedDayOTList.size() != 0) {
                            for (int i = 0; i < approvedDayOTList.size(); i++) {
                                EmployeeDailyAttendanceAudit approvedDayOvertimeAudit = new EmployeeDailyAttendanceAudit();
                                //approvedDayOvertimeAudit.setType("Approved Day Overtime(" + (i + 1) + ")");
                                approvedDayOvertimeAudit.setType("Approved Day Overtime");
                                approvedDayOvertimeAudit.setSequence(3);
                                approvedDayOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                                approvedDayOvertimeAudit.setStartTime((String) approvedDayOTInList.get(i));
                                approvedDayOvertimeAudit.setEndTime((String) approvedDayOTOutList.get(i));
                                approvedDayOvertimeAudit.setPeriod((String) approvedDayOTList.get(i));
                                oem.saveEntity(approvedDayOvertimeAudit, loggedUser);
                            }
                        } else if (approvedDayOT != null && !approvedDayOT.equals("00:00")) {
                            EmployeeDailyAttendanceAudit approvedDayOvertimeAudit = new EmployeeDailyAttendanceAudit();
                            //approvedDayOvertimeAudit.setType("Approved Day Overtime(" + (i + 1) + ")");
                            approvedDayOvertimeAudit.setType("Approved Day Overtime");
                            approvedDayOvertimeAudit.setSequence(3);
                            approvedDayOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                            approvedDayOvertimeAudit.setStartTime(dayOTIn);
                            approvedDayOvertimeAudit.setEndTime(dayOTOut);
                            approvedDayOvertimeAudit.setPeriod(approvedDayOT);
                            oem.saveEntity(approvedDayOvertimeAudit, loggedUser);
                        }

                        if (notApprovedDayOT != null && !notApprovedDayOT.equals("00:00")) {
                            EmployeeDailyAttendanceAudit notApprovedDayOvertimeAudit = new EmployeeDailyAttendanceAudit();
                            notApprovedDayOvertimeAudit.setType("Not Approved Day Overtime");
                            notApprovedDayOvertimeAudit.setSequence(4);
                            notApprovedDayOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                            notApprovedDayOvertimeAudit.setStartTime("-");
                            notApprovedDayOvertimeAudit.setEndTime("-");
                            notApprovedDayOvertimeAudit.setPeriod(notApprovedDayOT);
                            oem.saveEntity(notApprovedDayOvertimeAudit, loggedUser);
                        }
                        if (approvedNightOTList != null && approvedNightOTList.size() != 0) {
                            for (int i = 0; i < approvedNightOTList.size(); i++) {
                                EmployeeDailyAttendanceAudit approvedNightOvertimeAudit = new EmployeeDailyAttendanceAudit();
                                approvedNightOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                                //approvedDayOvertimeAudit.setType("Approved night Overtime(" + (i + 1) + ")");
                                approvedNightOvertimeAudit.setType("Approved Night Overtime");
                                approvedNightOvertimeAudit.setSequence(5);
                                approvedNightOvertimeAudit.setStartTime((String) approvedNightOTInList.get(i));
                                approvedNightOvertimeAudit.setEndTime((String) approvedNightOTOutList.get(i));
                                approvedNightOvertimeAudit.setPeriod((String) approvedNightOTList.get(i));
                                oem.saveEntity(approvedNightOvertimeAudit, loggedUser);
                            }
                        } else if (approvedNightOT != null && !approvedNightOT.equals("00:00")) {
                            EmployeeDailyAttendanceAudit approvedNightOvertimeAudit = new EmployeeDailyAttendanceAudit();
                            //approvedDayOvertimeAudit.setType("Approved Day Overtime(" + (i + 1) + ")");
                            approvedNightOvertimeAudit.setType("Approved Day Overtime");
                            approvedNightOvertimeAudit.setSequence(3);
                            approvedNightOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                            approvedNightOvertimeAudit.setStartTime(nightOTIn);
                            approvedNightOvertimeAudit.setEndTime(nightOTOut);
                            approvedNightOvertimeAudit.setPeriod(approvedNightOT);
                            oem.saveEntity(approvedNightOvertimeAudit, loggedUser);
                        }

                        if (notApprovedNightOT != null && !notApprovedNightOT.equals("00:00")) {
                            EmployeeDailyAttendanceAudit notApprovedNightOvertimeAudit = new EmployeeDailyAttendanceAudit();
                            notApprovedNightOvertimeAudit.setType("Not Approved Night Overtime");
                            notApprovedNightOvertimeAudit.setSequence(6);
                            notApprovedNightOvertimeAudit.setEmployeeDailyAttendance(loadedEmpDA);
                            notApprovedNightOvertimeAudit.setStartTime("-");
                            notApprovedNightOvertimeAudit.setEndTime("-");
                            notApprovedNightOvertimeAudit.setPeriod(notApprovedNightOT);
                            oem.saveEntity(notApprovedNightOvertimeAudit, loggedUser);
                        }
                    }
                    oem.saveEntity(loadedEmpDA, loggedUser);
                    EmployeeDailyAttendance empDAforConsolidation = (EmployeeDailyAttendance) odm.getData().get(0);

                    if (emp != null) {
                        if (emp.getPositionSimpleMode() != null
                                && emp.getPositionSimpleMode().getUnit() != null
                                && emp.getPositionSimpleMode().getUnit().getCompany() != null) {
                            company = emp.getPositionSimpleMode().getUnit().getCompany();

                            List<String> ruleConditions = new ArrayList<String>();
                            //Loubna - 08/09/2013 - for rotation & shift cases (monthly consolidation), changing the Rule to be on TMCalendar
                            if (emp.getCalendar() != null) {
                                ruleConditions.add("tmCalendar.dbid = " + tmCalendar.getDbid()); // to be changed **!!**
                                try {
                                    timeManagementRule = (Rule) oem.loadEntity(Rule.class.getSimpleName(), ruleConditions, null, loggedUser);
                                } catch (Exception ex) {
                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }

                            Integer month = Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(empDA.getDailyDate()).toString());
                            Integer year = Integer.parseInt(dateTimeUtility.YEAR_FORMAT.format(empDA.getDailyDate()).toString());

                            conds.clear();
                            conds.add("description = 'default_pay_method'");
                            conds.add("company.dbid = " + company.getDbid());
                            PayrollParameter parameter = null;
                            try {
                                parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);
                            } catch (Exception ex) {
                                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            EmployeeMonthlyConsolidation monthlyConsolidation = consolidation;

                            BigDecimal dayOverTimeValue = new BigDecimal(0);
                            BigDecimal nightOverTimeValue = new BigDecimal(0);
                            BigDecimal delayValue = new BigDecimal(0);
                            BigDecimal earlyLeaveValue = new BigDecimal(0);
                            BigDecimal holidayOT = new BigDecimal(0);
                            BigDecimal weekendOT = new BigDecimal(0);
                            BigDecimal workingHours = new BigDecimal(0);
                            BigDecimal missionHours = new BigDecimal(0);
                            BigDecimal lessWorkValue = new BigDecimal(0);
                            BigDecimal dayLessWorkValue = new BigDecimal(0);
                            BigDecimal weot = new BigDecimal(0);
                            BigDecimal dayWEOT = new BigDecimal(0);
                            BigDecimal nightWEOT = new BigDecimal(0);
                            BigDecimal holidayPayrollOT = new BigDecimal(0);
                            // *&* Mission *&*
                            long vacations = 0;
//                        int missions = 0;
                            //cust for Suez Cement
//                        int missionHours = 0;
                            long absenceDays = 0;
                            int missionDays = 0;
                            long workingDays = 0;
                            long workingOnHolidays = 0;
                            long workingOnWeekends = 0;

                            String startDateStr;
                            String endDateStr;

                            Integer tmpCutOfEnd = cutOffEnd;
                            Integer tmpCutOfStart = cutOffStart;

                            if (calcPeriodMonth == 1 || calcPeriodMonth == 3 || calcPeriodMonth == 5 || calcPeriodMonth == 7 || calcPeriodMonth == 8 || calcPeriodMonth == 10 || calcPeriodMonth == 12) {
                                //cutOffEnd = 31;
                            } else if (calcPeriodMonth == 2) {
                                if (calcPeriodYear % 4 == 0) {
                                    if (cutOffEnd > 29) {
                                        tmpCutOfEnd = 29;
                                    }
                                } else if (cutOffEnd > 28) {
                                    tmpCutOfEnd = 28;
                                }
                            } else if (cutOffEnd > 30) {
                                tmpCutOfEnd = 30;
                            }

                            endDateStr = calcPeriodYear.toString() + "-" + (calcPeriodMonth).toString() + "-" + tmpCutOfEnd;
                            Integer perviousMonth = calcPeriodMonth;
                            Integer perviosYear = calcPeriodYear;

                            if (calcFullMonth != null && calcFullMonth.equals("Y")) { // from 1->30
                                if (calcCurrentMonth != null && calcCurrentMonth.equals("N")) { // the prev month
                                    Integer tempYear = calcPeriodYear - 1;
                                    if (calcPeriodMonth == 1) {
                                        tempYear--;
                                    }

                                    tmpCutOfEnd = cutOffEnd;
                                    tmpCutOfStart = cutOffStart;
                                    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                                        //cutOffEnd = 31;
                                        tmpCutOfEnd = 31;
                                    } else if (month == 2) {
                                        if (tempYear % 4 == 0) {
                                            if (cutOffEnd > 29) {
                                                tmpCutOfEnd = 29;
                                            }
                                            if (cutOffStart > 29) {
                                                tmpCutOfStart = 29;
                                            }
                                        } else {
                                            if (cutOffEnd > 28) {
                                                tmpCutOfEnd = 28;
                                            }
                                            if (cutOffStart > 28) {
                                                tmpCutOfStart = 28;
                                            }
                                        }
                                    } else {
                                        if (cutOffEnd > 30) {
                                            tmpCutOfEnd = 30;
                                        }
                                        if (cutOffStart > 30) {
                                            tmpCutOfStart = 30;
                                        }
                                    }

                                    endDateStr = tempYear.toString() + "-" + month.toString() + "-" + tmpCutOfEnd;
                                    startDateStr = tempYear.toString() + "-" + month.toString() + "-" + tmpCutOfStart;
                                }
                            } else {
                                perviousMonth = calcPeriodMonth - 1;
                                if (calcPeriodMonth == 1) {
                                    perviousMonth = 12;
//                                perviosYear = year - 1;
                                    perviosYear = perviosYear - 1;
                                }
                            }

                            tmpCutOfStart = cutOffStart;
                            if (perviousMonth == 1 || perviousMonth == 3 || perviousMonth == 5 || perviousMonth == 7 || perviousMonth == 8 || perviousMonth == 10 || perviousMonth == 12) {
                                //cutOffEnd = 31;
                                tmpCutOfEnd = 31;
                            } else if (perviousMonth == 2) {
                                if (perviosYear % 4 == 0) {
                                    if (cutOffEnd > 29) {
                                        tmpCutOfEnd = 29;
                                    }
                                    if (cutOffStart > 29) {
                                        tmpCutOfStart = 29;
                                    }
                                } else {
                                    if (cutOffEnd > 28) {
                                        tmpCutOfEnd = 28;
                                    }
                                    if (cutOffStart > 28) {
                                        tmpCutOfStart = 28;
                                    }
                                }
                            } else {
                                if (cutOffEnd > 30) {
                                    tmpCutOfEnd = 30;
                                }
                                if (cutOffStart > 30) {
                                    tmpCutOfStart = 30;
                                }
                            }

                            startDateStr = perviosYear.toString() + "-" + (perviousMonth).toString() + "-" + tmpCutOfStart;
                            // endDateStr = perviosYear.toString() + "-" + (perviousMonth).toString() + "-" + tmpCutOfEnd;
                            if (empDAforConsolidation.isConsolidation() == true) {
                                if (monthlyConsolidation == null) {
                                    monthlyConsolidation = new EmployeeMonthlyConsolidation();

                                    monthlyConsolidation.setPosted("N");
                                    monthlyConsolidation.setEmployee(empDA.getEmployee());

                                    conds.clear();
                                    conds.add("month = " + calcPeriodMonth);
                                    conds.add("year = " + calcPeriodYear);
                                    conds.add("paymentMethod.dbid=" + parameter.getValueData());

                                    try {
                                        calculatedPeriod = (CalculatedPeriod) oem.loadEntity(CalculatedPeriod.class.getSimpleName(), conds, null, loggedUser);
                                    } catch (Exception ex) {
                                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    monthlyConsolidation.setCalculatedPeriod(calculatedPeriod);
                                    empDA.setMonthlyConsolidation(monthlyConsolidation);                         //rania
                                }

                                // select the count of the working, absence & vacation days
                                String selectDaysCnt;
                                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                    selectDaysCnt = "select 'V',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'Vacation')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union "
                                            + " select 'A',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'Absence')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'W',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'Working')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'WO',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnWeekEnd')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'WH',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnHoliday')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'H',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'Holiday')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'O',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'WeekEnd')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'M',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and daynature_dbid in (select dbid from udc where code = 'Mission')"
                                            + " and dbid <>" + empDA.getDbid();

                                } else {
                                    selectDaysCnt = "select 'V',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'Vacation')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union "
                                            + " select 'A',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'Absence')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'W',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'Working')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'WO',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnWeekEnd')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'WH',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnHoliday')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'H',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'Holiday')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'O',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'WeekEnd')"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " union"
                                            + " select 'M',count(*) from employeedailyattendance\n"
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and daynature_dbid in (select dbid from udc where code = 'Mission')"
                                            + " and dbid <>" + empDA.getDbid();
                                }

                                boolean calculateBasedOnActualDays = tmCalendar.isActualWorkingDays();
                                long actualWorkingDays = 0, totalWorkingDays = 0,
                                        weekendDays = 0, holidays = 0;

                                List daysCntLst = oem.executeEntityListNativeQuery(selectDaysCnt, loggedUser);
                                if (daysCntLst != null && !daysCntLst.isEmpty()) {
                                    Object[] daysCntArr;
                                    String dayNature;
                                    for (int i = 0; i < daysCntLst.size(); i++) {
                                        daysCntArr = (Object[]) daysCntLst.get(i);
                                        if (daysCntArr != null && daysCntArr.length > 0) {
                                            dayNature = daysCntArr[0].toString();
                                            if (dayNature.equals("V")) {
                                                vacations = Long.parseLong(daysCntArr[1].toString());
                                            } else if (dayNature.equals("W")) {
                                                //workingDays = Long.parseLong(daysCntArr[1].toString());
                                                actualWorkingDays = Long.parseLong(daysCntArr[1].toString());
                                            } else if (dayNature.equals("WH")) {
                                                workingOnHolidays = Long.parseLong(daysCntArr[1].toString());
                                            } else if (dayNature.equals("WO")) {
                                                workingOnWeekends = Long.parseLong(daysCntArr[1].toString());
                                            } else if (dayNature.equals("H")) {
                                                holidays = Long.parseLong(daysCntArr[1].toString());
                                            } else if (dayNature.equals("O")) {
                                                weekendDays = Long.parseLong(daysCntArr[1].toString());
                                            } else if (dayNature.equals("M")) {
                                                missionDays = Integer.parseInt(daysCntArr[1].toString());
                                            } else {
                                                absenceDays = Long.parseLong(daysCntArr[1].toString());
                                            }
                                        }
                                    }
                                }
                                // add the current day nature
                                if (empDA.getDayNature().getCode().equals("Vacation")) {
                                    vacations++;
                                } else if (empDA.getDayNature().getCode().equals("Working")) {
                                    actualWorkingDays++;
                                } else if (empDA.getDayNature().getCode().equals("WorkingOnHoliday")) {
                                    workingOnHolidays++;
                                } else if (empDA.getDayNature().getCode().equals("WorkingOnWeekEnd")) {
                                    workingOnWeekends++;
                                } else if (empDA.getDayNature().getCode().equals("Mission")) {
                                    missionDays++;
                                } else {
                                    absenceDays++;
                                }

                                // if the working days is based on 30 days or actual days
                                if (!calculateBasedOnActualDays) {
                                    totalWorkingDays = 30 - (absenceDays + vacations);
                                } else {
                                    totalWorkingDays = actualWorkingDays
                                            + workingOnHolidays
                                            + workingOnWeekends
                                            + weekendDays
                                            + holidays
                                            + absenceDays
                                            + missionDays;
                                }
                                //select the sum of mission of type hours and convert it to days
                                //cust for Suez Cement
                                // *&* Mission *&*
                                String sumPeriodHours;
                                /* sumPeriodHours = " select sum(exceptionHours) from employeedailyattendance\n"
                                 + " where employee_dbid =" + emp.getDbid()
                                 + " and dailydate <= '" + endDateStr + "'"
                                 + " and dailydate >= '" + startDateStr + "'"
                                 //        + " and daynature_dbid in (select dbid from udc where code = 'Mission')"
                                 + " and dbid <>" + empDA.getDbid();*/
                                /* sumPeriodHours = " select sum(period) from employeedayoffrequest mission "
                                 + " left join employeedailyattendance on (employeedailyattendance.employee_dbid = mission.employee_dbid"
                                 + " and employeedailyattendance.dailydate <= '" + endDateStr + "'"
                                 + " and employeedailyattendance.dailydate >= '" + startDateStr + "' and employeedailyattendance.deleted = 0)"
                                 + " left join dayoff on (mission.vacation_dbid = dayoff.dbid and dayoff.deleted = 0)"
                                 + " where employeedailyattendance.dbid <>" + empDA.getDbid() + " and dayoff.unitOfMeasure = 'H'"
                                 + " and dayoff.mission = 1 and mission.employee_dbid = " + emp.getDbid();*/
                                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                    sumPeriodHours = " select sum(exceptionHours) from employeedailyattendance "
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr) + ""
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr) + ""
                                            + " and deleted = 0 and dbid <>" + empDA.getDbid();
                                } else {
                                    sumPeriodHours = " select sum(exceptionHours) from employeedailyattendance "
                                            + " where employee_dbid =" + emp.getDbid()
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dailydate >= '" + startDateStr + "' "
                                            + " and deleted = 0 and dbid <>" + empDA.getDbid();
                                }
                                Object obj = oem.executeEntityNativeQuery(sumPeriodHours, loggedUser);
                                if (obj != null) {
                                    missionHours = (BigDecimal) obj;
                                }
                                //if (empDA.getDayNature().getCode().equals("Mission")) {
                                missionHours = missionHours.add(empDA.getExceptionHours());
                            //}

                                // select the sum of the calculations; DOT, NOT, ealyLEave, delay, WeenEndOT, HolidayOT
                                String selectOT;

                                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                    selectOT = "select sum(dayovertimeperiod), sum(nightovertimeperiod),"
                                            + "sum(delayHoursPeriod), sum(earlyLeavePeriod), sum(approvedWeekendOT)"
                                            + " , sum(approvedHolidayOT), sum(workinghours), sum(lessworkperiod),sum(daylessworkperiod)"
                                            + " ,sum(weot),sum(dayWEOT) ,sum(nightWEOT)"
                                            + " from employeedailyattendance "
                                            + " where employee_dbid = " + emp.getDbid()
                                            + " and dailydate >= '" + startDateStr + "'"
                                            + " and dailydate <= '" + endDateStr + "'"
                                            + " and dbid <>" + empDA.getDbid()
                                            + " and deleted = 0";
                                } else {
                                    selectOT = "select sum(dayovertimeperiod), sum(nightovertimeperiod),"
                                            + "sum(delayHoursPeriod), sum(earlyLeavePeriod), sum(approvedWeekendOT)"
                                            + " , sum(approvedHolidayOT), sum(workinghours), sum(lessworkperiod),sum(daylessworkperiod) "
                                            + " ,sum(weot),sum(dayWEOT) ,sum(nightWEOT),sum(holidayPayrollOT) "
                                            + "from employeedailyattendance "
                                            + " where employee_dbid = " + emp.getDbid()
                                            + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                                            + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                                            + " and dbid <>" + empDA.getDbid()
                                            + " and deleted = 0";
                                }
                                List calcsLst = oem.executeEntityListNativeQuery(selectOT, loggedUser);
                                Object[] calcsArr = !calcsLst.isEmpty() ? (Object[]) calcsLst.get(0) : null;
                                if (calcsArr != null) {
                                    dayOverTimeValue = calcsArr[0] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[0];
                                    nightOverTimeValue = calcsArr[1] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[1];
                                    delayValue = calcsArr[2] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[2];
                                    earlyLeaveValue = calcsArr[3] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[3];
                                    weekendOT = calcsArr[4] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[4];
                                    holidayOT = calcsArr[5] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[5];
                                    workingHours = calcsArr[6] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[6];
                                    lessWorkValue = calcsArr[7] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[7];
                                    dayLessWorkValue = calcsArr[8] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[8];
                                    weot = calcsArr[9] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[9];
                                    dayWEOT = calcsArr[10] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[10];
                                    nightWEOT = calcsArr[11] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[11];
                                    holidayPayrollOT = calcsArr[12] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[12];
                                }
                                dayOverTimeValue = dayOverTimeValue.add(empDA.getDayOverTimePeriod() == null ? BigDecimal.ZERO : empDA.getDayOverTimePeriod());
                                nightOverTimeValue = nightOverTimeValue.add(empDA.getNightOverTimePeriod() == null ? BigDecimal.ZERO : empDA.getNightOverTimePeriod());
                                earlyLeaveValue = earlyLeaveValue.add(empDA.getEarlyLeavePeriod() == null ? BigDecimal.ZERO : empDA.getEarlyLeavePeriod());
                                delayValue = delayValue.add(empDA.getDelayHoursPeriod() == null ? BigDecimal.ZERO : empDA.getDelayHoursPeriod());
                                weekendOT = weekendOT.add(empDA.getApprovedWeekendOT() == null ? BigDecimal.ZERO : empDA.getApprovedWeekendOT());
                                holidayOT = holidayOT.add(empDA.getApprovedHolidayOT() == null ? BigDecimal.ZERO : empDA.getApprovedHolidayOT());
                                workingHours = workingHours.add(empDA.getWorkingHours() == null ? BigDecimal.ZERO : empDA.getWorkingHours());
                                lessWorkValue = lessWorkValue.add(empDA.getLessWorkPeriod() == null ? BigDecimal.ZERO : empDA.getLessWorkPeriod());
                                dayLessWorkValue = dayLessWorkValue.add(empDA.getDayLessWorkPeriod() == null ? BigDecimal.ZERO : empDA.getDayLessWorkPeriod());
                                weot = weot.add(empDA.getWeOT() == null ? BigDecimal.ZERO : empDA.getWeOT());
                                dayWEOT = dayWEOT.add(empDA.getDayWEOT() == null ? BigDecimal.ZERO : empDA.getDayWEOT());
                                nightWEOT = nightWEOT.add(empDA.getNightWEOT() == null ? BigDecimal.ZERO : empDA.getNightWEOT());
                                holidayPayrollOT = holidayPayrollOT.add(empDA.getHolidayPayrollOT() == null ? BigDecimal.ZERO : empDA.getHolidayPayrollOT());
                                // set the values in the monthly consolidation record
                                monthlyConsolidation.setWeOT(weot);
                                monthlyConsolidation.setDayWEOT(dayWEOT);
                                monthlyConsolidation.setNightWEOT(nightWEOT);
                                monthlyConsolidation.setDayOvertime(dayOverTimeValue);
                                monthlyConsolidation.setNightOvertime(nightOverTimeValue);
                                monthlyConsolidation.setDelayHours(delayValue);
                                monthlyConsolidation.setEarlyLeave(earlyLeaveValue);
                                monthlyConsolidation.setVacations(BigDecimal.valueOf(vacations));
                                // *&* Mission *&*
                                monthlyConsolidation.setMissions(missionDays);
                                monthlyConsolidation.setMissionHours(missionHours);
                                monthlyConsolidation.setAbsenceDays(BigDecimal.valueOf(absenceDays));
                                monthlyConsolidation.setWorkingDays(BigDecimal.valueOf(totalWorkingDays));
                                monthlyConsolidation.setActualWorkingDays((int) (actualWorkingDays));
                                monthlyConsolidation.setWeekend(weekendOT);
                                monthlyConsolidation.setHoliday(holidayOT);
                                monthlyConsolidation.setWorkingHours(workingHours);
                                monthlyConsolidation.setWorkingOnWeekEndDays((int) workingOnWeekends);
                                monthlyConsolidation.setWorkingOnHolidayDays((int) workingOnHolidays);
                                monthlyConsolidation.setHolidays((int) holidays);
                                monthlyConsolidation.setWeekEndDays((int) weekendDays);
                                monthlyConsolidation.setLessWork(lessWorkValue);
                                monthlyConsolidation.setDayLessWork(dayLessWorkValue);
                                monthlyConsolidation.setHolidayPayrollOT(holidayPayrollOT);
                                BigDecimal parametrizedH = BigDecimal.ZERO,
                                        parametrizedDOT = BigDecimal.ZERO,
                                        parametrizedEarlyLeave = BigDecimal.ZERO,
                                        parametrizedNOT = BigDecimal.ZERO,
                                        parametrizedDelayHours = BigDecimal.ZERO,
                                        parametrizedAbsence = BigDecimal.ZERO,
                                        parametrizedW = BigDecimal.ZERO,
                                        parameterizedLessWork = BigDecimal.ZERO;

                                if (timeManagementRule != null && timeManagementRule.getDayOverTimeFactor() != null) {
                                    parametrizedDOT = timeManagementRule.getDayOverTimeFactor().multiply(dayOverTimeValue).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedDayOvertime(parametrizedDOT);
                                }
                                monthlyConsolidation.setPostedDayOvertime(parametrizedDOT);

                                if (timeManagementRule != null && timeManagementRule.getEarlyLeaveFactor() != null) {
                                    parametrizedEarlyLeave = timeManagementRule.getEarlyLeaveFactor().multiply(earlyLeaveValue).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedEarlyLeave(parametrizedEarlyLeave);
                                }
                                monthlyConsolidation.setPostedEarlyLeave(parametrizedEarlyLeave);

                                if (timeManagementRule != null && timeManagementRule.getNightOverTimeFactor() != null) {
                                    parametrizedNOT = timeManagementRule.getNightOverTimeFactor().multiply(nightOverTimeValue).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedNightOvertime(parametrizedNOT);
                                }
                                monthlyConsolidation.setPostedNightOvertime(parametrizedNOT);

                                if (timeManagementRule != null && timeManagementRule.getDelayFactor() != null) {
                                    parametrizedDelayHours = timeManagementRule.getDelayFactor().multiply(delayValue).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedDelayHours(parametrizedDelayHours);
                                }
                                monthlyConsolidation.setPostedDelayHours(parametrizedDelayHours);

                                if (timeManagementRule != null && timeManagementRule.getAbsenceFactor() != null) {
                                    parametrizedAbsence = timeManagementRule.getAbsenceFactor().multiply(BigDecimal.valueOf(absenceDays)).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedAbsenceDays(timeManagementRule.getAbsenceFactor().multiply(BigDecimal.valueOf(absenceDays)).setScale(5, RoundingMode.HALF_UP));
                                }
                                monthlyConsolidation.setPostedAbsenceDays(parametrizedAbsence);

                                if (timeManagementRule != null && timeManagementRule.getHolidayFactor() != null) {
                                    parametrizedH = (timeManagementRule.getHolidayFactor()).multiply(holidayOT).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedHoliday(parametrizedH);
                                }
                                monthlyConsolidation.setPostedHoliday(parametrizedH);

                                if (timeManagementRule != null && timeManagementRule.getWeekEndFactor() != null) {
                                    parametrizedW = (timeManagementRule.getWeekEndFactor()).multiply(weekendOT).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedWeekend(parametrizedW);
                                }
                                monthlyConsolidation.setPostedWeekend(parametrizedW);

                                if (timeManagementRule != null && timeManagementRule.getLessWorkFactor() != null) {
                                    parameterizedLessWork = (timeManagementRule.getLessWorkFactor()).multiply(lessWorkValue).setScale(5, RoundingMode.HALF_UP);
                                    monthlyConsolidation.setParametrizedLessWork(parameterizedLessWork);
                                }
                                monthlyConsolidation.setPostedLessWork(parameterizedLessWork);

                                try {
                                    oem.saveEntity(monthlyConsolidation, loggedUser);
                                } catch (Exception ex) {
                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            } else {
                                empDA.setConsolidation(true);
                            }

                        }
                    }
                }
            }
            //</editor-fold>

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            ofr.setReturnedDataMessage(odm);
            return ofr;
        }
    }

    @Override
    public OFunctionResult postDeductedAnnualAndPenaltyDaysAllEmp(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeMonthlyConsolidation request = (EmployeeMonthlyConsolidation) odm.getData().get(0);

            String sql = "delete from employeevacationadjustment where consolidation_dbid in (select dbid from employeemonthlyconsolidation"
                    + " where calculatedPeriod_dbid = " + request.getCalculatedPeriod().getDbid() + ")";
            oem.executeEntityUpdateNativeQuery(sql, loggedUser);

            sql = "delete from EmployeePenaltyRequest where consolidation_dbid in (select dbid from employeemonthlyconsolidation"
                    + " where calculatedPeriod_dbid = " + request.getCalculatedPeriod().getDbid() + ")";
            oem.executeEntityUpdateNativeQuery(sql, loggedUser);

            sql = "Update EmployeeSalaryElement Set value = 0 "
                    + " Where SalaryElement_Dbid in (select distinct affectedSalaryElement_dbid"
                    + " From TMEffectOnPersPayroll Where affectedSalaryElement_dbid is not null "
                    + " UNION select distinct monthlyAffectedSalElement_dbid "
                    + " From TMEffectOnPersPayroll Where monthlyAffectedSalElement_dbid is not null )";

            oem.executeEntityUpdateNativeQuery(sql, loggedUser);
            CalculatedPeriod calcPeriod = request.getCalculatedPeriod();
            String[] cutOffDates;
            cutOffDates = timeManagementService.getCutOffDatesFromCalculatedPeriod(calcPeriod.getMonth(), calcPeriod.getYear(), loggedUser);
            String endDateStr = cutOffDates[1];
            Date endDate = dateTimeUtility.DATE_FORMAT.parse(endDateStr);

            List conditions = new ArrayList();
            conditions.add("calculatedPeriod.dbid = " + calcPeriod.getDbid());
            List<EmployeeMonthlyConsolidation> consolidations = oem.loadEntityList("EmployeeMonthlyConsolidation", conditions, null, null, loggedUser);
            String empWhereCodition;
            for (EmployeeMonthlyConsolidation consolidation : consolidations) {
                //System.out.println("Employee Code = " + consolidation.getEmployee().getCode());
                empWhereCodition = " and employeemonthlyconsolidation.employee_dbid = " + consolidation.getEmployee().getDbid();
                ofr.append(postDeductedAnnualAndPenaltyDays(consolidation, endDate, empWhereCodition, loggedUser));
            }
            ofr.addSuccess(usrMsgService.getUserMessage("PostingPenalty-S00001", loggedUser));

        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("PostingPenalty-E00001", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult postDeductedAnnualAndPenaltyDaysSingleEmp(ODataMessage odm, OFunctionParms functionParms, OUser loggedUser
    ) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeMonthlyConsolidation request = (EmployeeMonthlyConsolidation) odm.getData().get(0);
            String empWhereCodition = " and employeemonthlyconsolidation.employee_dbid = " + request.getEmployee().getDbid();

            String sql = "delete from employeevacationadjustment where consolidation_dbid=" + request.getDbid();
            oem.executeEntityUpdateNativeQuery(sql, loggedUser);

            sql = "delete from EmployeePenaltyRequest where consolidation_dbid = " + request.getDbid();
            oem.executeEntityUpdateNativeQuery(sql, loggedUser);

            sql = "Update EmployeeSalaryElement Set value = 0 "
                    + " Where employee_dbid = " + request.getEmployee().getDbid()
                    + " AND SalaryElement_Dbid in (select distinct affectedSalaryElement_dbid"
                    + " From TMEffectOnPersPayroll Where affectedSalaryElement_dbid is not null "
                    + " UNION select distinct monthlyAffectedSalElement_dbid "
                    + " From TMEffectOnPersPayroll Where monthlyAffectedSalElement_dbid is not null )";

            oem.executeEntityUpdateNativeQuery(sql, loggedUser);
            CalculatedPeriod calcPeriod = request.getCalculatedPeriod();
            String[] cutOffDates;
            cutOffDates = timeManagementService.getCutOffDatesFromCalculatedPeriod(calcPeriod.getMonth(), calcPeriod.getYear(), loggedUser);
            String endDateStr = cutOffDates[1];
            Date endDate = dateTimeUtility.DATE_FORMAT.parse(endDateStr);

            ofr.append(postDeductedAnnualAndPenaltyDays(request, endDate, empWhereCodition, loggedUser));
            ofr.addSuccess(usrMsgService.getUserMessage("PostingPenalty-S00001", loggedUser));
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("PostingPenalty-E00001", loggedUser));
        }
        return ofr;
    }

    private OFunctionResult postDeductedAnnualAndPenaltyDays(EmployeeMonthlyConsolidation request, Date endDate, String empWhereCodition, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            /*
             TODO: for Loubna 27/4/2016
             ==========================
             let the addition be for the employee not for each range (group by employee only)
             and the vacation type can be gotten from any record (or sum grouped by the vacation)
             */
            String stat = "select employeemonthlyconsolidation.employee_dbid, sum(EmpTMPersonnelEffect.deductedPenaltyDays), "
                    + " sum(EmpTMPersonnelEffect.deductedAnnualDays), sum(EmpTMPersonnelEffect.deductedSalaryValue), EmpTMPersonnelEffect.ruleNature_dbid, "
                    + " payroll.payGrade_dbid"
                    + " from EmpTMPersonnelEffect "
                    + " left join employeemonthlyconsolidation on (EmpTMPersonnelEffect.monthlyConsolidation_dbid = employeemonthlyconsolidation.dbid)"
                    + " left join CalculatedPeriod on (employeemonthlyconsolidation.calculatedPeriod_dbid = CalculatedPeriod.dbid and CalculatedPeriod.deleted = 0) "
                    + " left join employeepayroll payroll on (employeemonthlyconsolidation.employee_dbid = payroll.employee_dbid and payroll.deleted = 0)"
                    + " where EmpTMPersonnelEffect.deleted = 0 and CalculatedPeriod.dbid = " + request.getCalculatedPeriod().getDbid()
                    + empWhereCodition
                    + " group by employeemonthlyconsolidation.employee_dbid, EmpTMPersonnelEffect.ruleNature_dbid,"
                    + " payroll.payGrade_dbid";
            List<Object[]> resultLst = (List<Object[]>) oem.executeEntityListNativeQuery(stat, loggedUser),
                    resultLst2;
            long empDbid, natureDbid, vacDbid = 0, penaltyDbid = 0, salaryElementDbid = 0, monthlyAffectedSalElementDbid = 0, gradeDbid = 0;
            double deductedPenaltyDays = 0, deductedAnnualDays = 0, deductedSalaryValue = 0, vacationBalance = 0;
            List vacBdata = new ArrayList();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            for (Object[] resultArr : resultLst) {
                // get deducted vals
                if (resultArr.length == 0) {
                    continue;
                }
                empDbid = resultArr[0] == null ? 0 : Long.parseLong(resultArr[0].toString());
                deductedPenaltyDays = resultArr[1] == null ? 0 : Double.parseDouble(resultArr[1].toString());
                deductedAnnualDays = resultArr[2] == null ? 0 : Double.parseDouble(resultArr[2].toString());
                deductedSalaryValue = resultArr[3] == null ? 0 : Double.parseDouble(resultArr[3].toString());
                natureDbid = resultArr[4] == null ? 0 : Long.parseLong(resultArr[4].toString());
                //rangeDbid = resultArr[5] == null ? 0 : Long.parseLong(resultArr[5].toString());
                //numOccurances = resultArr[5] == null ? 0 : Long.parseLong(resultArr[5].toString());
                gradeDbid = resultArr[5] == null ? 0 : Long.parseLong(resultArr[5].toString());

                // apply
                // 1- get the vacation & penalty to be affected
                stat = "select vacationAffected_dbid, penaltyAffected_dbid, affectedSalaryElement_dbid,monthlyAffectedSalElement_dbid"
                        + " from TMEffectOnPersPayroll"
                        + " where ruleNature_dbid = " + natureDbid;
                resultLst2 = (List<Object[]>) oem.executeEntityListNativeQuery(stat, loggedUser);
                // handling case of monthly where no nature exists
                if (resultLst2 == null || (resultLst2 != null && resultLst2.isEmpty())) {
                    String alternativeStat = "select vacationAffected_dbid, penaltyAffected_dbid, affectedSalaryElement_dbid,monthlyAffectedSalElement_dbid"
                            + " from TMEffectOnPersPayroll"
                            + " where ruleNature_dbid is null";
                    resultLst2 = (List<Object[]>) oem.executeEntityListNativeQuery(alternativeStat, loggedUser);
                }
                for (Object[] resultArr2 : resultLst2) {
                    vacDbid = resultArr2[0] == null ? 0 : Long.parseLong(resultArr2[0].toString());
                    penaltyDbid = resultArr2[1] == null ? 0 : Long.parseLong(resultArr2[1].toString());
                    salaryElementDbid = resultArr2[2] == null ? 0 : Long.parseLong(resultArr2[2].toString());
                    monthlyAffectedSalElementDbid = resultArr2[3] == null ? 0 : Long.parseLong(resultArr2[3].toString());
                }

                // 2- get the vacation balance and check whether it is sufficient
                // if yes, then apply vacation adjustment only; else apply vacation adjustment then salary element deduction based on setup
                if (deductedAnnualDays != 0 && vacDbid != 0) {
                    vacBdata.clear();
                    vacBdata.add(empDbid);
                    vacBdata.add(vacDbid);
                    vacBdata.add(dateFormat.format(endDate));
                    vacBdata.add(dateFormat.format(endDate));

                    String webServiceCode = "CVB_01";
                    String result = webServiceUtility.callingWebService(
                            webServiceCode, vacBdata, loggedUser);

                    vacationBalance = Double.parseDouble(result);

                    if (vacationBalance < deductedAnnualDays) {
                        if (vacationBalance > 0) {
                            stat = "insert into employeevacationadjustment (ADJUSTMENTVALUE,EMPLOYEE_DBID,VACATION_DBID,"
                                    + "creationDate,comments,DELETED,settled,consolidation_dbid)"
                                    + "values (" + (vacationBalance * -1) + "," + empDbid + "," + vacDbid + ",'" + dateTimeUtility.DATE_FORMAT.format(endDate)
                                    + "','Adjustment from Time Management',0,0," + request.getDbid() + ")";
                            oem.executeEntityUpdateNativeQuery(stat, loggedUser);

                            // throw the rest of the deducted days on the salary element
                            stat = "update employeesalaryelement set value = " + (deductedAnnualDays - vacationBalance)
                                    + " where employee_Dbid = " + empDbid + " and salaryelement_Dbid = " + salaryElementDbid + " and deleted = 0";
                            oem.executeEntityUpdateNativeQuery(stat, loggedUser);
                        } else {
                            // throw the rest of the deducted days on the salary element
                            stat = "update employeesalaryelement set value = " + deductedAnnualDays
                                    + " where employee_Dbid = " + empDbid + " and salaryelement_Dbid = " + salaryElementDbid + " and deleted = 0";
                            oem.executeEntityUpdateNativeQuery(stat, loggedUser);
                        }

                    } else {
                        stat = "insert into employeevacationadjustment (ADJUSTMENTVALUE,EMPLOYEE_DBID,VACATION_DBID,"
                                + "creationDate,comments,DELETED,settled,consolidation_dbid)"
                                + "values (" + (deductedAnnualDays * -1) + "," + empDbid + "," + vacDbid + ",'" + dateTimeUtility.DATE_FORMAT.format(endDate)
                                + "','Adjustment from Time Management',0,0," + request.getDbid() + ")";
                        oem.executeEntityUpdateNativeQuery(stat, loggedUser);
                    }
                }
                // 3- apply penalty days deduction
                if (deductedPenaltyDays != 0 && penaltyDbid != 0) {

                    EmployeePenaltyRequest penaltyRequest = new EmployeePenaltyRequest();
                    Employee emp = (Employee) oem.loadEntity(Employee.class.getSimpleName(), empDbid, null, null, loggedUser);
                    Penalty penalty = (Penalty) oem.loadEntity(Penalty.class.getSimpleName(), penaltyDbid, null, null, loggedUser);
                    penaltyRequest.setEmployee(emp);
                    penaltyRequest.setPenalty(penalty);
                    penaltyRequest.setPenaltyDate(endDate);//ask mai?
                    penaltyRequest.setPenaltyValue(new BigDecimal(deductedPenaltyDays));
                    penaltyRequest.setCancelled("N");
                    penaltyRequest.setConsolidation(request);
                    entitySetupService.callEntityCreateAction(penaltyRequest, loggedUser);
                }
                // 4- apply salary element deduction
                if (deductedSalaryValue != 0) {
                    stat = "update employeesalaryelement set value = " + deductedSalaryValue
                            + " where employee_dbid = " + empDbid + " and salaryelement_dbid = " + monthlyAffectedSalElementDbid
                            + "and deleted = 0";
                    oem.executeEntityUpdateNativeQuery(stat, loggedUser);
                }
            }
        } catch (Exception e) {
            ofr.addError(usrMsgService.getUserMessage("O-00704", loggedUser));
        }
        return ofr;
    }

    @Override
    public OFunctionResult assignAnnualPlanner(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) odm.getData().get(0);
            List<String> conds = new ArrayList<String>();

            if (empDA != null) {
                if (empDA.getEmployee() != null) {
                    conds.add("employee.dbid = " + empDA.getEmployee().getDbid());

                    if (empDA.getDailyDate() != null) {
                        Date date = empDA.getDailyDate();

                        conds.add("plannerYear = '" + dateTimeUtility.YEAR_FORMAT.format(date) + "'");

                        List<EmployeeAnnualPlanner> employeeAnnualPlanners = (List<EmployeeAnnualPlanner>) oem.loadEntityList(EmployeeAnnualPlanner.class.getSimpleName(),
                                conds,
                                null, null,
                                loggedUser);

                        if (employeeAnnualPlanners.size() > 0) {
                            if (empDA.getAnnualPlanner() == null) {
                                empDA.setAnnualPlanner(employeeAnnualPlanners.get(0));
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            ofr.setReturnedDataMessage(odm);
            return ofr;
        }
    }

    @Override
    public OFunctionResult validateEmployeeOTRequest(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        try {
            EmployeeOTRequest employeeOTRequest = (EmployeeOTRequest) odm.getData().get(0);

            String timeFrom = employeeOTRequest.getTimeFrom();
            String timeTo = employeeOTRequest.getTimeTo();

            if (timeFrom != null && !timeFrom.equals("")) {
                if (!dateTimeUtility.validateTimeFormat(timeFrom)) {
                    ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Time From"), loggedUser));
                    return ofr;
                }
            }

            if (timeTo != null && !timeTo.equals("")) {
                if (!dateTimeUtility.validateTimeFormat(timeTo)) {
                    ofr.addError(usrMsgService.getUserMessage("InvalidTimeFormat", Collections.singletonList("Time To"), loggedUser));
                    return ofr;
                }
            }

            String SQL = "Select DAYNATURE_DBID from EmployeeDailyAttendance where employee_dbid = " + employeeOTRequest.getEmployee().getDbid()
                    + " AND dailyDate =";
            SQL += dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(employeeOTRequest.getRequestedDate()));
            BigDecimal dayNatureDbid = (BigDecimal) oem.executeEntityNativeQuery(SQL, loggedUser);
            List<String> conds = new ArrayList<String>();
            conds.add("dbid =" + dayNatureDbid);
            UDC dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
            if (dayNature.getCode().contains("WeekEnd")) {
                if (isWeekendOTExceedLimit(employeeOTRequest, loggedUser)) {
                    ofr.addError(usrMsgService.getUserMessage("Request Exceeds maximum weekend overtime", null, loggedUser));
                    return ofr;
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    /*
     * [Porta] - Loubna - 04/02/2014
     * This action was a post action on saving the otRequest in the stage where there was no approved field.
     * It will be the approval action on the balloon of the request & set approved = 'Y'
     */
    @Override
    public OFunctionResult postEmployeeOTRequest(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
//            oem.getEM(loggedUser);
            EmployeeOTRequest employeeOTRequest = (EmployeeOTRequest) odm.getData().get(0);

            if (employeeOTRequest != null) {
                List<String> payConds = new ArrayList<String>();
                payConds.add("description = 'TM_MultiEntryEnabled'");
                PayrollParameter payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), payConds, null, loggedUser);

                // set approved to Y
                employeeOTRequest.setApproved("Y");
                oem.saveEntity(employeeOTRequest, loggedUser);

                List<String> empDAConditions = new ArrayList<String>();
                if (employeeOTRequest.getEmployee() != null && employeeOTRequest.getRequestedDate() != null) {
                    empDAConditions.add("employee.dbid = " + employeeOTRequest.getEmployee().getDbid());
                    empDAConditions.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(employeeOTRequest.getRequestedDate()) + "'");
                    if (payrollParameter != null
                            && payrollParameter.getValueData() != null
                            && payrollParameter.getValueData().equalsIgnoreCase("y")) {
                        empDAConditions.add("header = false");
                    }

                    EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) oem.loadEntity(
                            EmployeeDailyAttendance.class.getSimpleName(), empDAConditions, null, loggedUser);

                    if (empDA != null) {
                        if (empDA.getTimeIn() != null && !empDA.getTimeIn().equals("")) {
                            ofr.append(entitySetupService.callEntityUpdateAction(empDA, loggedUser));
                        }
                    }
                }
            }
            if (ofr.getErrors() == null
                    || (ofr.getErrors() != null && ofr.getErrors().isEmpty())) {
                ofr.addSuccess(usrMsgService.getUserMessage("OTApproved", loggedUser));
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult cancelEmployeeOTRequest(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeOTRequest employeeOTRequest = (EmployeeOTRequest) odm.getData().get(0);

            if (employeeOTRequest != null) {
                // set approved to N
                employeeOTRequest.setApproved("N");
                oem.saveEntity(employeeOTRequest, loggedUser);

                List<String> empDAConditions = new ArrayList<String>();
                if (employeeOTRequest.getEmployee() != null && employeeOTRequest.getRequestedDate() != null) {

                    employeeOTRequest.setTimeTo(employeeOTRequest.getTimeFrom());
                    oem.saveEntity(employeeOTRequest, loggedUser);

                    empDAConditions.add("employee.dbid = " + employeeOTRequest.getEmployee().getDbid());
                    empDAConditions.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(employeeOTRequest.getRequestedDate()) + "'");
                    empDAConditions.add("header = true");

                    EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) oem.loadEntity(
                            EmployeeDailyAttendance.class.getSimpleName(), empDAConditions, null, loggedUser);

                    if (empDA != null) {
                        entitySetupService.callEntityUpdateAction(empDA, loggedUser);
                    }
                }
            }
            ofr.addSuccess(usrMsgService.getUserMessage("cancelOTReq", loggedUser));
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult postImportedDAs(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        try {
            List<EmployeeDailyAttendanceD> droolsFacts = new ArrayList<>();
            List<EmployeeDailyAttendance> returnedList = new ArrayList<>();
            List<String> payConds = new ArrayList<String>();
            payConds.add("description = 'TM_MultiEntryEnabled'");
            PayrollParameter payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), payConds, null, loggedUser);
            if (payrollParameter != null
                    && payrollParameter.getValueData() != null
                    && payrollParameter.getValueData().equalsIgnoreCase("y")) {
                return postImportedDAsME(odm, functionParms, loggedUser);
            }

            List<String> screenFilter = null;
            if (functionParms.getParams().get("ScreenFilter") != null) {
                screenFilter = (List<String>) functionParms.getParams().get("ScreenFilter");
            }
            List conditions = new ArrayList();

            if (screenFilter != null) {
                conditions.addAll(screenFilter);
            }
            conditions.add("done = false");

//            for (int i = 0; i < employeeDAFix.size(); i++) {
//                EmployeeDailyAttendanceImport employeeDA = employeeDAFix.get(i);
//                fixEmployeeDailyAttendanceImport(employeeDA, loggedUser);
//            }
            List<EmployeeDailyAttendanceImport> employeeDAList = oem.loadEntityList(EmployeeDailyAttendanceImport.class.getSimpleName(),
                    conditions, null, null, loggedUser);

            String dayDate = "";
            for (int i = 0; i < employeeDAList.size(); i++) {
                EmployeeDailyAttendanceImport employeeDA = employeeDAList.get(i);

                if (employeeDA.getCode() != null
                        && !employeeDA.getCode().equals("")
                        && !employeeDA.getCode().equals("NA")
                        && employeeDA.getInTime() != null
                        && !employeeDA.getInTime().equals("")
                        && !employeeDA.getInTime().equals("NA")
                        && employeeDA.getOutTime() != null
                        && !employeeDA.getOutTime().equals("")
                        && !employeeDA.getOutTime().equals("NA")) {

                    dayDate = employeeDA.getDate();

                    List<String> conds = new ArrayList<String>();
                    conds.add("dailyDate = '" + dayDate + "'");
                    conds.add("employee.code = '" + employeeDA.getCode() + "'");

                    EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);

                    if (empDA != null) {
                        if (empDA.getTimeIn() == null || empDA.getTimeIn() == ""
                                && empDA.getTimeOut() == null || empDA.getTimeOut() == "") {
                            empDA.setTimeIn(employeeDA.getInTime());
                            empDA.setTimeOut(employeeDA.getOutTime());
                            empDA.setOutDate(employeeDA.getOutDate());
                            empDA.setExternalOrNot(employeeDA.getExternalOrNot());
                        } else {
                            empDA.setTimeOut(employeeDA.getOutTime());

                            empDA.setExternalOrNot(employeeDA.getExternalOrNot());
                        }
                        returnedList.add(empDA);
                        OverTimeSegmentD otNightSegment = new OverTimeSegmentD();
                        otNightSegment.setTimeFrom(10);
                        otNightSegment.setTimeTo(20);
                        OverTimeSegmentD otDaySegment = new OverTimeSegmentD();
                        otDaySegment.setTimeFrom(7);
                        otDaySegment.setTimeTo(9);

                        CalendarD calendar = new CalendarD();

                        calendar.setName("Calendar One");
                        calendar.setOtNightSegment(otNightSegment);

                        calendar.setOtDaySegment(otDaySegment);

                        calendar.setPlannedInHour(dateTimeUtility.convertToNumber(empDA.getWorkingCalendar().getTimeIn()).intValue());
                        calendar.setPlannedOutHour(dateTimeUtility.convertToNumber(empDA.getWorkingCalendar().getTimeOut()).intValue());
                        // calendar.setDelayTolerance(1);
                        EmployeeD employee = new EmployeeD();

                        employee.setName("Aly");
                        employee.setCalendar(calendar);
                        EmployeeDailyAttendanceD eda = new EmployeeDailyAttendanceD();
                        eda.setEmployee(employee);
                        eda.setTimeInHour(dateTimeUtility.convertToNumber(empDA.getTimeIn()).intValue());
                        eda.setTimeOutHour(dateTimeUtility.convertToNumber(empDA.getTimeOut()).intValue());
                        eda.setDayNature(empDA.getDayNature().getCode());
                        droolsFacts.add(eda);
                        employeeDA.setDone(true);
                        oem.saveEntity(employeeDA, loggedUser);
                    }
                }
            }
            DroolsWebService_Service service = new DroolsWebService_Service();
            droolsFacts = service.getDroolsWebServicePort().fireRules(droolsFacts);
            for (int i = 0; i < droolsFacts.size(); i++) {
                EmployeeDailyAttendance empDA = returnedList.get(i);
                empDA.setDelayHoursDisplay("0" + droolsFacts.get(0).getDelay() + ":00");
                empDA.setDayOverTimeDisplay("0" + droolsFacts.get(0).getOverTimeBeforeDayStart() + ":00");
                empDA.setNightOverTimeDisplay("0" + droolsFacts.get(0).getOverTimeAfterDayEnd() + ":00");
                empDA.setEarlyLeaveDisplay("0" + droolsFacts.get(0).getEarlyLeave() + ":00");
                empDA.setPlannedIn(empDA.getWorkingCalendar().getTimeIn());
                empDA.setPlannedOut(empDA.getWorkingCalendar().getTimeOut());
                List<String> conds = new ArrayList<String>();
                if (droolsFacts.get(0).getDayNature().equalsIgnoreCase("Working on WeekEnd")) {
                    conds.add("code='WorkingOnWeekEnd'");
                } else {
                    conds.add("code='Working'");
                }

                try {
                    UDC dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    empDA.setDayNature(dayNature);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                oem.saveEntity(empDA, loggedUser);
            }
            ofr.addSuccess(usrMsgService.getUserMessage("DAInsertedSuccessfully", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }

        return ofr;
    }

    @Override
    public OFunctionResult postImportedDAsME(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        List conditions = new ArrayList();
        conditions.add("done = false");
        conditions.add("fromTimeMachine = 'N'");

        try {

            //<editor-fold defaultstate="collapsed" desc="System Parameter">
            List<String> conds = new ArrayList<String>();
            conds.add("description = 'TM_FILO'");
            PayrollParameter parameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), conds, null, loggedUser);

            boolean FILO = false;
            if (parameter != null) {
                if (parameter.getValueData().equals("Y")) {
                    FILO = true;
                }
            }
            //</editor-fold>

            Map<String, List<EmployeeDailyAttendanceImport>> empDAsMap = new HashMap<String, List<EmployeeDailyAttendanceImport>>();
            List<EmployeeDailyAttendanceImport> employeeDAList = oem.loadEntityList(EmployeeDailyAttendanceImport.class.getSimpleName(),
                    conditions, null, null, loggedUser);
            List<EmployeeDailyAttendanceImport> tempLst = null;
            for (int i = 0; i < employeeDAList.size(); i++) {
                tempLst = empDAsMap.get(employeeDAList.get(i).getDate());
                if (tempLst == null) {
                    tempLst = new ArrayList<EmployeeDailyAttendanceImport>();
                }
                tempLst.add(employeeDAList.get(i));
                empDAsMap.put(employeeDAList.get(i).getDate(), tempLst);
            }

            deleteDAsForReimport(loggedUser);

            String timeIn = null,
                    timeOut = null;
            Date outDate = null;
            for (String key : empDAsMap.keySet()) {
                if (empDAsMap.get(key).size() > 1 && FILO) {
                    Comparator fieldsComparator2 = new Comparator() {
                        public int compare(Object o1, Object o2) {
                            String timeIn1 = ((EmployeeDailyAttendanceImport) o1).getInTime();
                            String timeIn2 = ((EmployeeDailyAttendanceImport) o2).getOutTime();
                            return dateTimeUtility.compareTime(timeIn1, timeIn2);
                        }
                    };

                    Collections.sort(empDAsMap.get(key), fieldsComparator2);

                    timeIn = empDAsMap.get(key).get(0).getInTime();
                    timeOut = empDAsMap.get(key).get(empDAsMap.get(key).size() - 1).getOutTime();

                    EmployeeDailyAttendanceImport employeeDA = empDAsMap.get(key).get(0);
                    if (employeeDA.getCode() != null
                            && !employeeDA.getCode().equals("")
                            && !employeeDA.getCode().equals("NA")
                            && employeeDA.getInTime() != null
                            && !employeeDA.getInTime().equals("")
                            && !employeeDA.getInTime().equals("NA")
                            && employeeDA.getOutTime() != null
                            && !employeeDA.getOutTime().equals("")
                            && !employeeDA.getOutTime().equals("NA")) {

                        conds.clear();
//                        conds.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(key) + "'");
                        conds.add("dailyDate = '" + (key) + "'");
                        conds.add("employee.code = '" + employeeDA.getCode() + "'");
                        conds.add("header = true");
                        EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);

                        if (empDA != null) {
                            //continue;
                            EmployeeDailyAttendance empDADetail = new EmployeeDailyAttendance();
                            empDADetail.setTimeIn(timeIn);
                            empDADetail.setTimeOut(timeOut);
                            empDADetail.setEmployee(empDA.getEmployee());
                            empDADetail.setDailyDate(empDA.getDailyDate());
                            empDADetail.setDayNature(empDA.getDayNature());
                            empDADetail.setEmpDAHeader(empDA);
                            empDADetail.setHeader(false);

                            if (employeeDA.getMachineCode() != null
                                    && !employeeDA.getMachineCode().equals("")
                                    && !employeeDA.getMachineCode().equals("NA")) {

                                TimeManagementMachine machine = (TimeManagementMachine) oem.loadEntity(TimeManagementMachine.class.getSimpleName(), Collections.singletonList("code = '" + employeeDA.getMachineCode() + "'"), null, loggedUser);

                                if (machine != null) {
                                    empDADetail.setAccessMachine(machine);
                                }
                            }

                            OFunctionResult empDAOfr = entitySetupService.callEntityCreateAction(empDADetail, loggedUser);
                            ofr.append(empDAOfr);
                            if (empDAOfr.getErrors() == null || empDAOfr.getErrors().isEmpty()) {
                                employeeDA.setDone(true);
                                employeeDA.setFromTimeMachine("N");
                                OFunctionResult employeeDataOFR = entitySetupService.callEntityUpdateAction(employeeDA, loggedUser);
                                ofr.append(employeeDataOFR);
                            }
                        }
                    }
                } else if ((empDAsMap.get(key).size() > 1 && !FILO) || empDAsMap.get(key).size() == 1) {
                    for (int i = 0; i < empDAsMap.get(key).size(); i++) {
                        EmployeeDailyAttendanceImport employeeDA = empDAsMap.get(key).get(i);

                        if (employeeDA.getCode() != null
                                && !employeeDA.getCode().equals("")
                                && !employeeDA.getCode().equals("NA")
                                && employeeDA.getInTime() != null
                                && !employeeDA.getInTime().equals("")
                                && !employeeDA.getInTime().equals("NA")
                                && employeeDA.getOutTime() != null
                                && !employeeDA.getOutTime().equals("")
                                && !employeeDA.getOutTime().equals("NA")) {

                            conds.clear();
//                            conds.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(employeeDA.getDailyDate()) + "'");
                            conds.add("dailyDate = '" + employeeDA.getDate() + "'");
                            conds.add("employee.code = '" + employeeDA.getCode() + "'");
                            conds.add("header = true");
                            EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);

                            timeIn = empDAsMap.get(key).get(i).getInTime();
                            timeOut = empDAsMap.get(key).get(i).getOutTime();
                            outDate = empDAsMap.get(key).get(i).getOutDate();
                            if (empDA != null) {
                                EmployeeDailyAttendance empDADetail = new EmployeeDailyAttendance();
                                empDADetail.setTimeIn(timeIn);
                                empDADetail.setTimeOut(timeOut);
                                empDADetail.setEmployee(empDA.getEmployee());
                                empDADetail.setDailyDate(empDA.getDailyDate());
                                empDADetail.setDayNature(empDA.getDayNature());
                                empDADetail.setEmpDAHeader(empDA);
                                empDADetail.setOutDate(outDate);
                                empDADetail.setHeader(false);

                                //continue;
                                empDA.setTimeIn(employeeDA.getInTime());
                                empDA.setTimeOut(employeeDA.getOutTime());

                                if (employeeDA.getMachineCode() != null
                                        && !employeeDA.getMachineCode().equals("")
                                        && !employeeDA.getMachineCode().equals("NA")) {

                                    TimeManagementMachine machine = (TimeManagementMachine) oem.loadEntity(TimeManagementMachine.class.getSimpleName(), Collections.singletonList("code = '" + employeeDA.getMachineCode() + "'"), null, loggedUser);

                                    if (machine != null) {
                                        empDA.setAccessMachine(machine);
                                    }
                                }

                                OFunctionResult empDAOfr = entitySetupService.callEntityCreateAction(empDADetail, loggedUser);
                                ofr.append(empDAOfr);
                                if (empDAOfr.getErrors() == null || (empDAOfr.getErrors() != null && empDAOfr.getErrors().isEmpty())) {
                                    employeeDA.setDone(true);
                                    employeeDA.setFromTimeMachine("N");
                                    OFunctionResult employeeDataOFR = entitySetupService.callEntityUpdateAction(employeeDA, loggedUser);
                                    ofr.append(employeeDataOFR);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }

        return ofr;
    }

    @Override
    public OFunctionResult postImported(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDailyAttendanceImport importDA = (EmployeeDailyAttendanceImport) odm.getData().get(0);
            if (importDA == null) {
                return ofr;
            }
            List where = new ArrayList();
            where.add("done = false");
            where.add("fromTimeMachine = 'N'");
            ofr.append(postEmployeeDailyAttendanceImport(where, loggedUser));
            Calendar cal = Calendar.getInstance();
            DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
            Date date = format.parse(importDA.getDate());
            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            int day = cal.get(Calendar.DAY_OF_MONTH);

            String paymentMethod = timeManagementService.getDefaultPaymentMethod(loggedUser);
            String paymentPeriod = timeManagementService.getDefaultPaymentPeriod(loggedUser);
            Long calculatedPeriod = timeManagementService.getCalculatedPeriodOfThisDay(day, month, year, paymentMethod, paymentPeriod, loggedUser);
            TMOpenedCalcPeriod cp = (TMOpenedCalcPeriod) oem.executeEntityQuery("select Entity from TMOpenedCalcPeriod Entity where Entity.calculatedPeriod.dbid= " + calculatedPeriod, loggedUser);
            Employee emp = (Employee) oem.executeEntityQuery("select Entity from Employee Entity where Entity.code= " + importDA.getCode(), loggedUser);
            importDA.setCalcPeriod(cp);
            importDA.setEmployee(emp);
            oem.saveEntity(importDA, loggedUser);
            String condition = " where calcperiod_dbid=" + calculatedPeriod + " and fromTimeMachine = 'N'";
            calculateConsolidationForPosting(condition, calculatedPeriod, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }

        return ofr;
    }

    @Override
    public OFunctionResult postSdk(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        EmployeeDailyAttendanceImport importDA = (EmployeeDailyAttendanceImport) odm.getData().get(0);
        if (importDA == null) {
            return ofr;
        }

        List where = new ArrayList();
        where.add("done = false");
//        where.add("fromTimeMachine = 'Y'");
        if (importDA.getCalcPeriod() != null) {
            where.add("calcPeriod.dbid = " + importDA.getCalcPeriod().getDbid());
        }

        List<String> screenFilter = null;
        if (functionParms.getParams().get("ScreenFilter") != null) {
            screenFilter = (List<String>) functionParms.getParams().get("ScreenFilter");
        }
//        List conditions = new ArrayList();

        if (screenFilter != null) {
            where.addAll(screenFilter);
        }

        ofr.append(postEmployeeDailyAttendanceImport(where, loggedUser));
        String condition = " where calcperiod_dbid=" + importDA.getCalcPeriod().getDbid();
        calculateConsolidationForPosting(condition, importDA.getCalcPeriod().getDbid(), loggedUser);
        return ofr;
    }

    @Override
    public OFunctionResult postSdkSingleEmp(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        EmployeeDailyAttendanceImport importDA = (EmployeeDailyAttendanceImport) odm.getData().get(0);
        if (importDA == null) {
            return ofr;
        }

        List where = new ArrayList();
        where.add("done = false");
        where.add("fromTimeMachine = 'Y'");
        where.add("employee.dbid = " + importDA.getEmployee().getDbid());
        if (importDA.getCalcPeriod() != null) {
            where.add("calcPeriod.dbid = " + importDA.getCalcPeriod().getDbid());
        }

        ofr.append(postEmployeeDailyAttendanceImport(where, loggedUser));
        String condition = " where  calcperiod_dbid=" + importDA.getCalcPeriod().getDbid() + " and employee_dbid=" + importDA.getEmployee().getDbid();
        calculateConsolidationForPosting(condition, importDA.getCalcPeriod().getDbid(), loggedUser);
        return ofr;
    }

    private OFunctionResult postEmployeeDailyAttendanceImport(List<String> where, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<EmployeeDailyAttendanceImport> employeeDAList = oem.loadEntityList(EmployeeDailyAttendanceImport.class.getSimpleName(),
                    where, null, null, loggedUser);

            String employeeCode = null;
            for (int i = 0; i < employeeDAList.size(); i++) {
                EmployeeDailyAttendanceImport employeeDA = employeeDAList.get(i);

                if (((employeeDA.getCode() != null
                        && !employeeDA.getCode().equals("")
                        && !employeeDA.getCode().equals("NA"))
                        || (employeeDA.getEmployee() != null
                        && employeeDA.getEmployee().getDbid() != 0))
                        && employeeDA.getMachineCode() != null
                        && !employeeDA.getMachineCode().equals("")
                        && !employeeDA.getMachineCode().equals("NA")
                        && employeeDA.getInTime() != null
                        && !employeeDA.getInTime().equals("")
                        && !employeeDA.getInTime().equals("NA")
                        && employeeDA.getOutTime() != null
                        && !employeeDA.getOutTime().equals("")
                        && !employeeDA.getOutTime().equals("NA")) {

                    if (employeeDA.getCode() != null && !employeeDA.getCode().equals("NA")) {
                        employeeCode = employeeDA.getCode();
                    } else if (employeeDA.getEmployee() != null) {
                        employeeCode = employeeDA.getEmployee().getCode();
                    }

                    List<String> conds = new ArrayList<String>();
                    conds.add("dailyDate = '" + employeeDA.getDate().trim() + "'");
                    conds.add("employee.code = '" + employeeCode + "'");

                    EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);

                    if (empDA != null) {
                        empDA.setTimeIn(employeeDA.getInTime().trim());
                        empDA.setTimeOut(employeeDA.getOutTime().trim());
                        empDA.setOutDate(employeeDA.getOutDate());
                        empDA.setExternalOrNot(employeeDA.getExternalOrNot());

                        if (employeeDA.getMachineCode() != null
                                && !employeeDA.getMachineCode().equals("")
                                && !employeeDA.getMachineCode().equals("NA")) {

                            TimeManagementMachine machine = (TimeManagementMachine) oem.loadEntity(TimeManagementMachine.class.getSimpleName(), Collections.singletonList("code = '" + employeeDA.getMachineCode() + "'"), null, loggedUser);

                            if (machine != null) {
                                empDA.setAccessMachine(machine);
                            }
                        }
                        empDA.setConsolidation(false);
                        OFunctionResult empDAOfr = entitySetupService.callEntityUpdateAction(empDA, loggedUser);
                        ofr.append(empDAOfr);
                        if (empDAOfr.getErrors() == null || (empDAOfr.getErrors() != null && empDAOfr.getErrors().isEmpty())) {
                            if (employeeDA.getFromTimeMachine().equals("N")) {
                                employeeDA.setDone(true);
                                oem.saveEntity(employeeDA, loggedUser);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult importDAValidation(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        EmployeeDailyAttendanceImport employeeDA = (EmployeeDailyAttendanceImport) odm.getData().get(0);

        if (employeeDA.getCode() == null || employeeDA.getCode().equals("")) {
            employeeDA.setCode("NA");
        }

        if (employeeDA.getMachineCode() == null || employeeDA.getMachineCode().equals("")) {
            employeeDA.setMachineCode("NA");
        }

        if (employeeDA.getInTime() == null || employeeDA.getInTime().equals("") || !dateTimeUtility.validateTimeFormat(employeeDA.getInTime())) {
            employeeDA.setInTime("NA");
        }

        if (employeeDA.getOutTime() == null || employeeDA.getOutTime().equals("") || !dateTimeUtility.validateTimeFormat(employeeDA.getOutTime())) {
            employeeDA.setOutTime("NA");
        }

        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String datePattern1 = "((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";

        DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
        String datePattern2 = "(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((19|20)\\d\\d)";

        String dafaultDate = "1975-01-01";

        if (employeeDA.getDate() != null && !employeeDA.getDate().equals("")) {
            String s = employeeDA.getDate().replace("/", "-");
            if (s.matches(datePattern1)) {
                try {
                    dateFormat1.parse(s);
                } catch (ParseException ex) {
                    s = dafaultDate;
                }
            } else if (s.matches(datePattern2)) {
                try {
                    s = dateFormat1.format(dateFormat2.parse(s));
                } catch (ParseException ex) {
                    s = dafaultDate;
                }
            } else {
                s = dafaultDate;
            }

            employeeDA.setDate(s);
        } else {
            employeeDA.setDate(dafaultDate);
        }

        ofr.setReturnedDataMessage(odm);

        return ofr;
    }

    @Override
    public OFunctionResult validateTMCalendar(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Employee employee = (Employee) odm.getData().get(0);
            if (employee != null) {

                List<String> conds = new ArrayList<String>();
                conds.add("calendar.dbid = " + employee.getCalendar().getDbid());
                List<NormalWorkingCalendar> calendars = oem.loadEntityList(
                        NormalWorkingCalendar.class.getSimpleName(), conds, null, null, loggedUser);
                if (calendars == null || calendars.isEmpty()) {
                    ofr.addError(usrMsgService.getUserMessage("UpdateNotAllowed", loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    /*
     * Validation - To stop the updating of the employee's calendar from the employee's management screen
     * The update can be only done from the employee calendar history screen
     */
    @Override
    public OFunctionResult validateWorkingCalendarUpdate(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Employee employee = (Employee) odm.getData().get(0);
            if (employee == null) {
                return ofr;
            }
            if ((employee.isInActive() || !employee.isActive())) {
                return ofr;
            }
            List<String> conds = new ArrayList<String>();
            conds.add("dbid = " + employee.getDbid());

            Employee loadedEmployee = (Employee) oem.loadEntity(Employee.class.getSimpleName(), conds, null, loggedUser);

            if (loadedEmployee.getCalendar() != null) {
                if (employee.getCalendar().getDbid() != loadedEmployee.getCalendar().getDbid()) {
                    ofr.addError(usrMsgService.getUserMessage("UpdateNotAllowed", loggedUser));
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult validateWorkingCalHistory(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeWorkingCalHistory calHistory = (EmployeeWorkingCalHistory) odm.getData().get(0);
            int empDACount = 0;
            try {
                String sqlStat;
                String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    sqlStat = "select count(*) from employeedailyattendance "
                            + "where employee_dbid = "
                            + calHistory.getEmployee().getDbid() + " and dailydate >= '"
                            + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "' and timeIn is not null and timeIn <> ''";
                } else {
                    sqlStat = "select count(*) from employeedailyattendance "
                            + "where employee_dbid = "
                            + calHistory.getEmployee().getDbid() + " and dailydate >= '"
                            + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "' and timeIn is not null and timeIn <> ''";

                }
                String count = oem.executeEntityNativeQuery(sqlStat, loggedUser).toString();
                empDACount = Integer.parseInt(count);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (empDACount > 0) {
                ofr.addError(usrMsgService.getUserMessage("CalHistoryError", loggedUser));
            }

            // Loubna - Rotation Sequence in Rotation Calendar Assigning [Porta] - 23/02/2014
            if (calHistory.getCurrentWorkingCal() != null
                    && calHistory.getCurrentWorkingCal().getType() != null
                    && calHistory.getCurrentWorkingCal().getType().getCode().equals("Rotation")) {
                String select = "select SUM(numberOfDays) from normalworkingcalendar where "
                        + "deleted = 0 and calendar_dbid=" + calHistory.getCurrentWorkingCal().getDbid();
                Object ret = oem.executeEntityNativeQuery(select, loggedUser);

                if (ret != null) {
                    if (calHistory.getRotSequence() != null && !calHistory.getRotSequence().equals("")) {
                        if (Integer.parseInt(calHistory.getRotSequence()) > Integer.parseInt(ret.toString())) {
                            ofr.addError(usrMsgService.getUserMessage("CalHistoryError002", Collections.singletonList(ret.toString()), loggedUser));
                        }
                    }
                }

                select = "select dbid from employeeworkingcalhistory where "
                        + "employee_Dbid = " + calHistory.getEmployee().getDbid()
                        + " and cancelled = 0 and deleted = 0 and currenteffectivedate = '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "'";
                ret = oem.executeEntityNativeQuery(select, loggedUser);

                if (ret != null) {
                    ofr.addError(usrMsgService.getUserMessage("CalHistoryError003", loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult workingCalendarUpdate(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {

            EmployeeWorkingCalHistory calHistory = (EmployeeWorkingCalHistory) odm.getData().get(0);

            calendarUpdate(ofr, calHistory, true, loggedUser, false);

        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), e);
        }
        return ofr;
    }

    @Override
    public OFunctionResult calendarUpdate(OFunctionResult ofr,
            EmployeeWorkingCalHistory calHistory, Boolean check, OUser loggedUser, Boolean deleted) {
        //OFunctionResult ofr = new OFunctionResult();
        try {
            List<Object[]> calendars = null;
            List<ChristianCalendar> christianCalendars;

            EmployeeShift employeeShift = null;
            Employee employee = null;
            EmployeeAnnualPlanner annualPlanner = null;
            String sql = "";

            Date startDate = null, endDate = null;
            List<Date> holidayDates = null;

            Calendar todayCal = Calendar.getInstance(),
                    tempCal = todayCal;

            Integer startYear = 1975;
            int endYear = 1975;

            String[] startDateParts = new String[3];
            String[] endDateParts = new String[3];
            String rotSequence = null,
                    currDay, currMonth;
            List<String> conds = new ArrayList<String>();
            List<String> dayMonthLst = new ArrayList<String>();

            Company company = null;
            TMCalendar newTmCalendar = null,
                    oldTmCalendar = null;
            NormalWorkingCalendar oldWC = null,
                    empRegCal = null/*, workingCalendar = null*/;
            //EmployeeWorkingCalHistory calHistory = (EmployeeWorkingCalHistory) odm.getData().get(0);

            employee = calHistory.getEmployee();
            if (calHistory.getRotSequence() == null
                    || (calHistory.getRotSequence() != null && calHistory.getRotSequence().equals(""))) {
                rotSequence = calculateRotSequence(calHistory.getShiftSequence(), calHistory.getRotationShift(), loggedUser);
                calHistory.setRotSequence(rotSequence);
            }

            if (calHistory.isCancelled()) {
                oldTmCalendar = calHistory.getCurrentWorkingCal();
                oldWC = employee.getWorkingCalendar();

                newTmCalendar = calHistory.getOriginalCalendar();

                rotSequence = calHistory.getOriginalCalSeq();

                cancelHistoryRecord(calHistory, loggedUser);
            } else {
                // get the rotation sequence in the case that the new calendar is rotation
                rotSequence = calHistory.getRotSequence();
                // update orig calendar in calHistory record
                try {
                    // divide history records & return the original calendar
                    ofr.append(cutCalHistory(calHistory, loggedUser));
                    oldTmCalendar = calHistory.getOriginalCalendar();
                    newTmCalendar = calHistory.getCurrentWorkingCal();
                    oldWC = employee.getWorkingCalendar();
                    //oldTmCalendar = getOriginalCalendar(calHistory, loggedUser);
                    calHistory.setOriginalCalendar(oldTmCalendar);
                    //calHistory.setOriginalCalSeq(tempRotSeq.toString());
                    //??????No Need To Save Here Because Of Saving at the function end
                    //oem.saveEntity(calHistory, loggedUser);
                } catch (Exception e) {
                    OLog.logException(e, loggedUser);
                    ofr.addError(usrMsgService.getUserMessage("CalHistory001", loggedUser));
                }
            }

            startDateParts = new String[3];
            endDateParts = new String[3];
            todayCal = Calendar.getInstance();
            List<Integer> distinctYear = new ArrayList<Integer>();

            startDate = calHistory.getCurrentEffectiveDate();
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
            List<Object> oldOffdays;
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                oldOffdays = (List<Object>) oem.executeEntityListNativeQuery("Select dbid from employeedailyattendance where employee_dbid="
                        + employee.getDbid() + " and dailydate >='" + dateTimeUtility.DATE_FORMAT.format(startDate) + "' and daynature_dbid=534374", loggedUser);
            } else {
                oldOffdays = (List<Object>) oem.executeEntityListNativeQuery("Select dbid from employeedailyattendance where employee_dbid="
                        + employee.getDbid() + " and dailydate >=" + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(startDate)) + " and daynature_dbid=534374", loggedUser);
            }
            if (startDate.compareTo(employee.getHiringDate()) < 0) {
                startDate = employee.getHiringDate();
            }

            startDateParts = dateTimeUtility.DATE_FORMAT.format(startDate).split("-");
            startYear = Integer.parseInt(startDateParts[0]);
            int startMonth = Integer.parseInt(startDateParts[1]);
            int startDay = Integer.parseInt(startDateParts[2]);
            try {
                endDate = calHistory.getCurrentEffectiveEndDate();
                if (endDate == null) {
                    endDate = dateTimeUtility.DATE_FORMAT.parse(endYear + "-12-31");
                }
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            endDateParts = dateTimeUtility.DATE_FORMAT.format(endDate).split("-");
            endYear = Integer.parseInt(endDateParts[0]);
            int endMonth = Integer.parseInt(endDateParts[1]);
            int endDay = Integer.parseInt(endDateParts[2]);
            String plannerSql = "update employeeannualplanner set ";
            for (int j = startMonth; j <= endMonth; j++) {
                for (int k = startDay; k <= endDay; k++) {
                    if (j == 2 && k == 30) {
                        break;
                    } else if ((j == 4 || j == 6 || j == 9 || j == 11) && k == 31) {
                        break;
                    }
                    if (j == endMonth && k == endDay) {
                        plannerSql += "m" + j + "D" + k + "='A' ";
                    } else {
                        plannerSql += "m" + j + "D" + k + "='A', ";
                    }
                }
                startDay = 1;

            }
            plannerSql += "where planneryear=2017 and employee_dbid=" + employee.getDbid();
            oem.executeEntityUpdateNativeQuery(plannerSql, loggedUser);
            endYear = (Calendar.getInstance().get(Calendar.YEAR));
            if (startYear != endYear) {
                if (calHistory.getCurrentEffectiveEndDate() == null) {

                    EmployeeWorkingCalHistory employeeWorkingCalHistory = new EmployeeWorkingCalHistory();
                    employeeWorkingCalHistory.setEmployee(employee);
                    employeeWorkingCalHistory.setCurrentWorkingCal(calHistory.getCurrentWorkingCal());
                    employeeWorkingCalHistory.setCurrentEffectiveDate(dateTimeUtility.DATE_FORMAT.parse(endYear + "-01-01"));
                    if (calHistory.getCurrentWorkingCal().getType().getCode().equals("Flexible")) {
                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            sql = "select mod((to_date('" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentWorkingCal().getCalendars().get(0).getStartTime())
                                    + "','dd-mm-yyyy')-to_date('" + dateTimeUtility.DATE_FORMAT.format(employeeWorkingCalHistory.getCurrentEffectiveDate())
                                    + "','dd-mm-yyyy'))+1,7) from dual";
                        } else {
                            sql = "select (DATEDIFF(DAY,'" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentWorkingCal().getCalendars().get(0).getStartTime()) + "','" + dateTimeUtility.DATE_FORMAT.format(employeeWorkingCalHistory.getCurrentEffectiveDate()) + "') + 1)%7";
                        }
                    } else {
                        sql = "select SUM(numberofdays)\n"
                                + "from normalworkingcalendar\n"
                                + "where calendar_dbid = (select dbid\n"
                                + "from tmcalendar\n"
                                + "where dbid=" + calHistory.getCurrentWorkingCal().getDbid() + " ) and DELETED = 0";
                        Object obj = oem.executeEntityNativeQuery(sql, loggedUser);
                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            sql = "select mod((to_date('" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentWorkingCal().getReferenceDate())
                                    + "','dd-mm-yyyy')-to_date('" + dateTimeUtility.DATE_FORMAT.format(employeeWorkingCalHistory.getCurrentEffectiveDate())
                                    + "','dd-mm-yyyy'))+1," + obj.toString() + ") from dual";
                        } else {
                            sql = "select (DATEDIFF(DAY,'" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentWorkingCal().getReferenceDate()) + "','" + dateTimeUtility.DATE_FORMAT.format(employeeWorkingCalHistory.getCurrentEffectiveDate()) + "') + 1)%" + obj.toString();
                        }
                    }
                    Object obj = oem.executeEntityNativeQuery(sql, loggedUser);
                    employeeWorkingCalHistory.setRotSequence(obj.toString());
                    entitySetupService.callEntityCreateAction(employeeWorkingCalHistory, loggedUser);
                    calHistory.setCurrentEffectiveEndDate(dateTimeUtility.DATE_FORMAT.parse(startYear + "-12-31"));
                }
            }
            endYear = startYear;
            try {
                endDate = calHistory.getCurrentEffectiveEndDate();
                if (endDate == null) {
                    endDate = dateTimeUtility.DATE_FORMAT.parse(endYear + "-12-31");
                }
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }
            String updateDASQL;
            String dayNatureSQL;
            Long absDbid = Long.valueOf(0);
            Long vacDbid = Long.valueOf(0);
            Long holDbid = Long.valueOf(0);
            Long wHolDbid = Long.valueOf(0);
            Long mDbid = Long.valueOf(0);

            //Get Abs & Vac Day Natures Dbid
            dayNatureSQL = "select dbid,type from DayNatures where type in ('A','V','H','WH','M')";
            List<Object[]> dayNatures = oem.executeEntityListNativeQuery(dayNatureSQL, loggedUser);
            if (dayNatures != null) {
                for (Object[] obj : dayNatures) {
                    if (obj[0] != null && obj[1] != null) {
                        if (obj[1].toString().equals("A")) {
                            absDbid = Long.parseLong(obj[0].toString());
                        }
                        if (obj[1].toString().equals("V")) {
                            vacDbid = Long.parseLong(obj[0].toString());
                        }
                        if (obj[1].toString().equals("H")) {
                            holDbid = Long.parseLong(obj[0].toString());
                        }
                        if (obj[1].toString().equals("WH")) {
                            wHolDbid = Long.parseLong(obj[0].toString());
                        }
                        if (obj[1].toString().equals("M")) {
                            mDbid = Long.parseLong(obj[0].toString());
                        }
                    }
                }
                //ReSet All WC Records To be Null.
//                updateDASQL = "Update EmployeeDailyAttendance Set workingCalendar_dbid = null "
//                        + " Where employee_dbid = " + employee.getDbid();
//                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
//                    updateDASQL += " AND dailyDate >= " + dateTimeUtility.dateTimeFormatForOracle(
//                            dateTimeUtility.DATE_FORMAT.format(startDate))
//                            + " AND dailyDate <= " + dateTimeUtility.dateTimeFormatForOracle(
//                                    dateTimeUtility.DATE_FORMAT.format(endDate));
//                } else {
//                    updateDASQL += " AND dailyDate >= '" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'"
//                            + " AND dailyDate <= '" + dateTimeUtility.DATE_FORMAT.format(endDate) + "'";
//                }
//                boolean result = oem.executeEntityUpdateNativeQuery(updateDASQL, loggedUser);
//                if (!result) {
//                    ofr.addError(usrMsgService.getUserMessage("ErrorUpdatingDayNature", loggedUser));
//                }

                boolean result;
                //ReSet All DA Records Day Nature To Absence.
                updateDASQL = "Update EmployeeDailyAttendance Set DayNature_Dbid = " + absDbid //absenceDayNature.getDbid()
                        + " Where employee_dbid = " + employee.getDbid();
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    updateDASQL += " AND dailyDate >= " + dateTimeUtility.dateTimeFormatForOracle(
                            dateTimeUtility.DATE_FORMAT.format(startDate))
                            + " AND dailyDate <= " + dateTimeUtility.dateTimeFormatForOracle(
                                    dateTimeUtility.DATE_FORMAT.format(endDate));
                } else {
                    updateDASQL += " AND dailyDate >= '" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'"
                            + " AND dailyDate <= '" + dateTimeUtility.DATE_FORMAT.format(endDate) + "'";
                }
                updateDASQL += " AND DayNature_Dbid not in (" + wHolDbid + ")";

                try {

                    oem.executeEntityUpdateNativeQuery(updateDASQL, loggedUser);

                } catch (Exception e) {
                    ofr.addError(usrMsgService.getUserMessage("ErrorUpdatingDayNature", loggedUser));
                }
            }
            // update the employee's data; the num of working hours and the working calendar in case of regular
            // in addition, create an employeeshift record if the new calendar is var shift or rotation
            if (newTmCalendar != null) {
                if (newTmCalendar.getType().getCode().equals("Regular") || newTmCalendar.getType().getCode().equals("Flexible")) {
                    conds.clear();
                    conds.add("calendar.dbid = " + newTmCalendar.getDbid());
                    try {
                        empRegCal = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    employee.setWorkingCalendar(empRegCal);
                } else if (newTmCalendar.getType().getCode().equals("VariableShift")
                        || newTmCalendar.getType().getCode().equals("Rotation")) {

                    conds.clear();
                    conds.add("employee.dbid = " + employee.getDbid());
                    conds.add("plannerYear = '" + startYear + "'");
                    try {
                        employeeShift = (EmployeeShift) oem.loadEntity(
                                EmployeeShift.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (employeeShift == null) {
                        employeeShift = new EmployeeShift();
                        employeeShift.setEmployee(employee);
                        employeeShift.setPlannerYear(startYear.toString());
                        try {
                            oem.saveEntity(employeeShift, loggedUser);
                        } catch (Exception ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                if (calHistory.getCurrentEffectiveEndDate() == null) {
                    String updateCalendarDataSQL;
                    updateCalendarDataSQL = "Update Oemployee Set workingHours = "
                            + newTmCalendar.getNumberOfHours().toString() + " , calendar_dbid = "
                            + newTmCalendar.getDbid()
                            + " where dbid = " + employee.getDbid();
                    try {
                        oem.executeEntityUpdateNativeQuery(updateCalendarDataSQL, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

            List<Method> weekendMethods = new ArrayList<Method>();
            if (employee != null) {
                conds.clear();
                conds.add("employee.dbid = " + employee.getDbid());
                conds.add("plannerYear = '" + startYear.toString() + "'");
                try {
                    annualPlanner = (EmployeeAnnualPlanner) oem.loadEntity(EmployeeAnnualPlanner.class.getSimpleName(), conds, null, loggedUser);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (employee.getPositionSimpleMode() != null
                        && employee.getPositionSimpleMode().getUnit() != null) {
                    company = employee.getPositionSimpleMode().getUnit().getCompany();
                }

                if (company != null && annualPlanner != null) {
                    //<editor-fold defaultstate="collapsed" desc="day natures loading">
                    conds.clear();
                    conds.add("type.code='900'");
                    conds.add("code='WeekEnd'");
                    UDC weekendDayNature = null;
                    try {
                        weekendDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    conds.clear();
                    conds.add("type.code='900'");
                    conds.add("code='Holiday'");
                    UDC holidayDayNature = null;
                    try {
                        holidayDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    conds.clear();
                    conds.add("type.code='900'");
                    conds.add("code='Vacation'");
                    UDC vacationDayNature = null;
                    try {
                        vacationDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    conds.clear();
                    conds.add("type.code='900'");
                    conds.add("code='Absence'");
                    UDC absenceDayNature = null;
                    try {
                        absenceDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    conds.clear();
                    conds.add("type.code='prs_019'"); // get udc type (religion) code
                    conds.add("code='CHR'"); // get udc (christian) code
                    UDC chrisitianReligion = null;
                    try {
                        chrisitianReligion = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //</editor-fold>

                    //Reset old offdays to either absence, holiday or vacation
                    //========================================================
                    //1. Load holidays
                    //<editor-fold defaultstate="collapsed" desc="load holidays">
                    String SQLStatement;
                    conds.clear();
                    conds.add("company.dbid=" + company.getDbid());
                    conds.add("dateFrom >='" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'");
                    conds.add("dateFrom <='" + dateTimeUtility.DATE_FORMAT.format(endDate) + "'");
                    holidayDates = new ArrayList<Date>();
                    try {
                        SQLStatement = "select holidayDate,holidayDate "
                                + " From employeeHoliday "
                                + " Where deleted = 0 and employee_dbid = " + calHistory.getEmployee().getDbid()
                                + " AND  ((holidayDate >= ";
                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            SQLStatement += dateTimeUtility.dateTimeFormatForOracle(
                                    dateTimeUtility.DATE_FORMAT.format(startDate));
                        } else {
                            SQLStatement += "'" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'";
                        }
                        SQLStatement += " AND holidayDate <= ";
                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            SQLStatement += dateTimeUtility.dateTimeFormatForOracle(
                                    dateTimeUtility.DATE_FORMAT.format(endDate));
                        } else {
                            SQLStatement += "'" + dateTimeUtility.DATE_FORMAT.format(endDate) + "'";
                        }
                        SQLStatement += " ) OR ( holidayDate >= ";
                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            SQLStatement += dateTimeUtility.dateTimeFormatForOracle(
                                    dateTimeUtility.DATE_FORMAT.format(startDate));
                        } else {
                            SQLStatement += "'" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'";
                        }
                        SQLStatement += " AND holidayDate <= ";
                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            SQLStatement += dateTimeUtility.dateTimeFormatForOracle(
                                    dateTimeUtility.DATE_FORMAT.format(endDate)) + "))";
                        } else {
                            SQLStatement += "'" + dateTimeUtility.DATE_FORMAT.format(endDate) + "'))";
                        }

                        calendars = oem.executeEntityListNativeQuery(SQLStatement, loggedUser);
                        //hijriCalendars = (List<HijriCalendar>) oem.loadEntityList(HijriCalendar.class.getSimpleName(), conds, null, null, loggedUser);
                        christianCalendars = (List<ChristianCalendar>) oem.loadEntityList(ChristianCalendar.class.getSimpleName(), conds, null, null, loggedUser);

                        for (int j = 0; j < calendars.size() && calendars != null; j++) {
                            Date from = dateTimeUtility.DATE_FORMAT.parse(calendars.get(j)[0].toString());
                            Date to = dateTimeUtility.DATE_FORMAT.parse(calendars.get(j)[1].toString());

                            holidayDates.addAll(getHolidayDates(from, to));
                        }

                        for (int j = 0; j < christianCalendars.size() && christianCalendars != null; j++) {
                            Date from = christianCalendars.get(j).getDateFrom();
                            Date to = christianCalendars.get(j).getDateTo();

                            holidayDates.addAll(getHolidayDates(from, to));
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //</editor-fold>
                    //2. load vacations
                    //<editor-fold defaultstate="collapsed" desc="load vacations">
                    conds.clear();
                    SQLStatement = "Select employeedayoffrequest.startDate,employeedayoffrequest.endDate"
                            + " From employeedayoffrequest,dayoff "
                            + " where employeedayoffrequest.employee_dbid = " + employee.getDbid() + " AND employeedayoffrequest.dayofftype='vacation'and cancelled='N' ";
                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        SQLStatement += " AND ((employeedayoffrequest.startDate >= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(startDate))
                                + " AND employeedayoffrequest.startDate<= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(endDate)) + ")"
                                + " OR (employeedayoffrequest.endDate >= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(startDate))
                                + "AND employeedayoffrequest.endDate<= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(endDate)) + "))"
                                + "and dayoff.dbid = employeedayoffrequest.vacation_dbid and dayoff.unitofmeasure = 'D' and dayoff.mission = 0";

                    } else {
                        SQLStatement += " AND ((employeedayoffrequest.startDate >= '" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'"
                                + " AND employeedayoffrequest.startDate<='" + dateTimeUtility.DATE_FORMAT.format(endDate) + "')"
                                + " OR (employeedayoffrequest.endDate >='" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'"
                                + "AND employeedayoffrequest.endDate<='" + dateTimeUtility.DATE_FORMAT.format(endDate) + "'))"
                                + "and dayoff.dbid = employeedayoffrequest.vacation_dbid and dayoff.unitofmeasure = 'D' and dayoff.mission = 0";
                    }
                    List<Object[]> vacations = null;
                    try {
                        //              vacations = oem.executeEntityListNativeQuery(SQLStatement, loggedUser);

                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    List<Date> vacationDates = new ArrayList<Date>();
                    if (vacations != null) {
                        for (int i = 0; i < vacations.size(); i++) {
                            vacationDates.addAll(
                                    getHolidayDates(dateTimeUtility.DATE_FORMAT.parse(vacations.get(i)[0].toString()),
                                            dateTimeUtility.DATE_FORMAT.parse(vacations.get(i)[1].toString())));
                        }
                    }
                    //</editor-fold>
                    //3. load old offdays
                    //<editor-fold defaultstate="collapsed" desc="Regular">
                    UDC nature;
                    String updateDayNatureSQL;
                    if (oldWC != null && oldTmCalendar != null
                            && (oldTmCalendar.getType().getCode().equals("Regular") || oldTmCalendar.getType().getCode().equals("Flexible"))) {
                        UDC[] oldDayOffs = new UDC[2];
                        oldDayOffs[0] = oldWC.getDayOff1();
                        oldDayOffs[1] = oldWC.getDayOff2();

                        for (int i = 0; i < 2; i++) {
                            if (empRegCal == null && oldDayOffs[i] != null
                                    || (oldDayOffs[i] != null && !oldDayOffs[i].getCode().equals(empRegCal.getDayOff1().getCode())
                                    && empRegCal.getDayOff2() != null
                                    && !oldDayOffs[i].getCode().equals(empRegCal.getDayOff2().getCode()))
                                    || oldDayOffs[i] != null && !oldDayOffs[i].getCode().equals(empRegCal.getDayOff1().getCode())) {
                                List<Date> dates = getDateFromDay(startDate, endDate, Integer.parseInt(oldDayOffs[i].getCode()));
                                if (dates != null) {
                                    for (int j = 0; j < dates.size(); j++) {
                                        nature = null;
                                        boolean isDayOffSet = false;
                                        String[] dateParts = dateTimeUtility.DATE_FORMAT.format(dates.get(j)).split("-");
                                        for (int k = 0; k < holidayDates.size(); k++) {
                                            String date = dateTimeUtility.DAYMONTH_FORMAT.format(dates.get(j));
                                            String holidayDate = dateTimeUtility.DAYMONTH_FORMAT.format(holidayDates.get(k));
                                            if (date.equals(holidayDate)) {
                                                BaseEntity.setValueInEntity(annualPlanner, "m" + dateParts[1] + "D" + dateParts[2], "H");
                                                nature = holidayDayNature;
                                                isDayOffSet = true;
                                                break;
                                            }
                                        }
                                        for (int k = 0; k < vacationDates.size(); k++) {
                                            String date = dateTimeUtility.DATE_FORMAT.format(dates.get(j));
                                            String vacationDate = dateTimeUtility.DATE_FORMAT.format(vacationDates.get(k));
                                            if (date.equals(vacationDate)) {
                                                BaseEntity.setValueInEntity(annualPlanner, "m" + dateParts[1] + "D" + dateParts[2], "V");
                                                nature = vacationDayNature;
                                                isDayOffSet = true;
                                                break;
                                            }
                                        }
                                        if (!isDayOffSet) {
                                            BaseEntity.setValueInEntity(annualPlanner, "m" + dateParts[1] + "D" + dateParts[2], "A");
                                            //nature = absenceDayNature;
                                        }

                                        try {
                                            if (nature != null) {
                                                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                                    updateDayNatureSQL = "Update EmployeeDailyAttendance set dayNature_dbid = " + nature.getDbid()
                                                            + " Where employee_dbid = " + employee.getDbid() + ""
                                                            + " And dailyDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(dates.get(j)));
                                                } else {
                                                    updateDayNatureSQL = "Update EmployeeDailyAttendance set dayNature_dbid = " + nature.getDbid()
                                                            + " Where employee_dbid = " + employee.getDbid() + ""
                                                            + " And dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(dates.get(j)) + "'";
                                                }
                                                try {
                                                    oem.executeEntityUpdateNativeQuery(updateDayNatureSQL, loggedUser);
                                                } catch (Exception ex) {
                                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                            }
                                        } catch (Exception ex) {
                                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                }
                            }
                        }
                    } //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Rotation OR VariableShift">
                    else if ((oldTmCalendar.getType().getCode().equals("Rotation"))
                            || (oldTmCalendar.getType().getCode().equals("VariableShift"))) {
                        DateTime startDateDT = new DateTime(startDate, dateTimeUtility.dateTimeZone);
                        //I think we don't need to make this code by this way ==>MIR20141210
                        String dayVal;
                        while (startDateDT.compareTo(new DateTime(endDate, dateTimeUtility.dateTimeZone)) <= 0) {
                            dayVal = null;
                            nature = null;
                            String[] dateParts = dateTimeUtility.DATE_FORMAT.format(startDateDT.toDate()).split("-");
                            try {
                                dayVal = BaseEntity.getValueFromEntity(annualPlanner, "m" + Integer.parseInt(dateParts[1]) + "D" + Integer.parseInt(dateParts[2])).toString();
                            } catch (Exception ex) {
                                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            if (dayVal != null && dayVal.equals("O")) {
                                boolean isDayOffSet = false;
                                for (int k = 0; k < holidayDates.size(); k++) {
                                    String date = dateTimeUtility.DAYMONTH_FORMAT.format(startDateDT.toDate());
                                    String holidayDate = dateTimeUtility.DAYMONTH_FORMAT.format(holidayDates.get(k));
                                    if (date.equals(holidayDate)) {
                                        BaseEntity.setValueInEntity(annualPlanner, "m" + Integer.parseInt(dateParts[1]) + "D" + Integer.parseInt(dateParts[2]), "H");
                                        nature = holidayDayNature;
                                        isDayOffSet = true;
                                        break;
                                    }
                                }
                                for (int k = 0; k < vacationDates.size(); k++) {
                                    String date = dateTimeUtility.DATE_FORMAT.format(startDateDT.toDate());
                                    String vacationDate = dateTimeUtility.DATE_FORMAT.format(vacationDates.get(k));
                                    if (date.equals(vacationDate)) {
                                        BaseEntity.setValueInEntity(annualPlanner, "m" + Integer.parseInt(dateParts[1]) + "D" + Integer.parseInt(dateParts[2]), "V");
                                        nature = vacationDayNature;
                                        isDayOffSet = true;
                                        break;
                                    }
                                }
                                if (!isDayOffSet) {
                                    BaseEntity.setValueInEntity(annualPlanner, "m" + Integer.parseInt(dateParts[1]) + "D" + Integer.parseInt(dateParts[2]), "A");
                                    //nature = absenceDayNature;
                                }
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
                                if (nature != null) {
                                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                        updateDayNatureSQL = "Update EmployeeDailyAttendance set dayNature_dbid = " + nature.getDbid()
                                                + " Where employee_dbid = " + employee.getDbid() + ""
                                                + " And dailyDate = '" + sdf.format(startDateDT.toDate()) + "'";
                                    } else {
                                        updateDayNatureSQL = "Update EmployeeDailyAttendance set dayNature_dbid = " + nature.getDbid()
                                                + " Where employee_dbid = " + employee.getDbid() + ""
                                                + " And dailyDate = '" + sdf.format(startDateDT.toDate()) + "'";//dateTimeUtility.DATE_FORMAT.format(startDateDT.toDate())
                                    }
                                    try {
                                        oem.executeEntityUpdateNativeQuery(updateDayNatureSQL, loggedUser);
                                    } catch (Exception ex) {
                                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                            startDateDT = startDateDT.plusDays(1);
                        }
                    }
                    //</editor-fold>

                    //Set new dayoffs
                    //===============
                    //1. AnnualPlanner
                    if (newTmCalendar.getType().getCode().equals("Regular") || newTmCalendar.getType().getCode().equals("Flexible")) {
                        if (empRegCal.getDayOff1() != null) {
                            weekendMethods.addAll(getDateFromDay(startDate,
                                    endDate, empRegCal.getDayOff1()));
                        }
                        if (empRegCal.getDayOff2() != null) {
                            weekendMethods.addAll(getDateFromDay(startDate,
                                    endDate, empRegCal.getDayOff2()));
                        }

                        if (weekendMethods != null) {
                            for (int k = 0; k < weekendMethods.size(); k++) {
                                Object arglist[] = new Object[1];
                                arglist[0] = "O";
                                try {
                                    weekendMethods.get(k).invoke(annualPlanner, arglist);
                                } catch (Exception ex) {
                                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                        //2. DailyAttendance
                        for (int i = 0; i < weekendMethods.size(); i++) {
                            dayMonthLst = getDayMonthFromFieldName(weekendMethods.get(i).getName());
                            currDay = dayMonthLst.get(0);
                            currMonth = dayMonthLst.get(1);

                            String currDateStr = startYear + "-" + currMonth + "-" + currDay;
                            Date currDate = null;
                            try {
                                currDate = dateTimeUtility.DATE_FORMAT.parse(currDateStr);
                            } catch (ParseException ex) {
                                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            if (currDate != null) {
                                if (weekendDayNature != null) {
                                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                        updateDayNatureSQL = "Update EmployeeDailyAttendance set dayNature_dbid = " + weekendDayNature.getDbid()
                                                + " Where employee_dbid = " + employee.getDbid() + ""
                                                + " And dailyDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(currDate));
                                    } else {
                                        updateDayNatureSQL = "Update EmployeeDailyAttendance set dayNature_dbid = " + weekendDayNature.getDbid()
                                                + " Where employee_dbid = " + employee.getDbid() + ""
                                                + " And dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(currDate) + "'";
                                    }
                                    try {
                                        oem.executeEntityUpdateNativeQuery(updateDayNatureSQL, loggedUser);
                                    } catch (Exception ex) {
                                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }
                    } else if (newTmCalendar.getType().getCode().equals("Rotation")) {
                        //TMCalendar rotationCal = newTmCalendar;
                        Date empDate = startDate;
                        int roundDuration = 0,
                                numRounds = 0,
                                numRoundsRem = 0,
                                rDur = 0;
                        String code = null,
                                dayName = null;

                        Map<Integer, NormalWorkingCalendar> rotCalsMap = new HashMap<Integer, NormalWorkingCalendar>();
                        try {
                            String select = "select SUM(numberOfDays) from normalworkingcalendar where deleted = 0 AND calendar_dbid=" + newTmCalendar.getDbid();
                            Object ret = oem.executeEntityNativeQuery(select, loggedUser);
                            if (ret != null) {
                                roundDuration = Integer.parseInt(ret.toString());
                                numRounds = (dateTimeUtility.getDaysBetween(empDate, endDate) + 1) / roundDuration;
                                numRoundsRem = (dateTimeUtility.getDaysBetween(empDate, endDate) + 1) % roundDuration;

                                if (rotSequence != null && !rotSequence.isEmpty()) {
                                    rDur = Integer.parseInt(rotSequence);
                                    rDur--;
                                } else {
                                    Object[] returned = getShift(newTmCalendar, empDate, loggedUser);
                                    if (returned != null && returned.length != 0) {
                                        rDur = (Integer) returned[0];
                                    }
                                    if (returned[1] == null) {
                                        ofr.addError(usrMsgService.getUserMessage("WrongCalRetrieval", loggedUser));
                                        return ofr;
                                    }
                                }
                                // fill map of calendars for each day
                                List<String> sort = new ArrayList<String>();
                                sort.add("shiftSequence");
                                conds.clear();
                                conds.add("calendar.dbid = " + newTmCalendar.getDbid());
                                List<NormalWorkingCalendar> rotShfts = oem.loadEntityList(NormalWorkingCalendar.class.getSimpleName(), conds, null, sort, loggedUser);
                                setCalendarsMap(rotCalsMap, rotShfts, roundDuration);
                                numRoundsRem += (rDur + 1);
                                /* add to the remaining days; the days missed in the first rotation*/

                                tempCal = Calendar.getInstance();
                                tempCal.setTime(empDate);
                                boolean isDayOffSet = false;
                                for (int r = 0; r < numRounds; r++) {
                                    for (; rDur < roundDuration; rDur++) {
                                        isDayOffSet = false;
                                        nature = null;
                                        code = rotCalsMap.get(rDur).getCode();
                                        empDate = tempCal.getTime();
                                        tempCal.add(Calendar.DAY_OF_MONTH, 1);
                                        dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();
                                        Object dayNature = BaseEntity.getValueFromEntity(employeeShift, dayName);
                                        String strDayNature = dayNature != null ? dayNature.toString() : null;
                                        for (int k = 0; k < holidayDates.size(); k++) {
                                            String date = dateTimeUtility.DAYMONTH_FORMAT.format(empDate);
                                            String holidayDate = dateTimeUtility.DAYMONTH_FORMAT.format(holidayDates.get(k));
                                            if (date.equals(holidayDate)) {
                                                BaseEntity.setValueInEntity(annualPlanner, dayName, "H");
                                                BaseEntity.setValueInEntity(employeeShift, dayName, "H");
                                                nature = holidayDayNature;
                                                isDayOffSet = true;
                                                break;
                                            }
                                        }
                                        if (strDayNature != null && strDayNature.equals("H")) {
                                            updateDayNatureSQL = "Update EmployeeDailyAttendance set workingcalendar_dbid = " + rotCalsMap.get(rDur).getDbid() + " Where employee_dbid = " + employee.getDbid() + "";
                                            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                                updateDayNatureSQL += " And dailyDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDate));
                                            } else {
                                                updateDayNatureSQL += " And dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(empDate) + "'";
                                            }
                                            try {
                                                oem.executeEntityUpdateNativeQuery(updateDayNatureSQL, loggedUser);
                                            } catch (Exception ex) {
                                                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            continue;
                                        }
                                        for (int k = 0; k < vacationDates.size(); k++) {
                                            String date = dateTimeUtility.DATE_FORMAT.format(empDate);
                                            String vacationDate = dateTimeUtility.DATE_FORMAT.format(vacationDates.get(k));
                                            if (date.equals(vacationDate)) {
                                                BaseEntity.setValueInEntity(annualPlanner, dayName, "V");
                                                BaseEntity.setValueInEntity(employeeShift, dayName, "V");
                                                nature = vacationDayNature;
                                                isDayOffSet = true;
                                                break;
                                            }
                                        }
                                        if (!isDayOffSet) {
                                            BaseEntity.setValueInEntity(employeeShift, dayName, code);
                                            if (rotCalsMap.get(rDur).getNumberOfHours().compareTo(BigDecimal.ZERO) == 0) {
                                                BaseEntity.setValueInEntity(annualPlanner, dayName, "O");
                                                nature = weekendDayNature;
                                            } else {
                                                BaseEntity.setValueInEntity(annualPlanner, dayName, "A");
                                                nature = absenceDayNature;
                                            }
                                        }
                                        String dayNatureUpdate = "";
                                        if (nature != null) {
                                            dayNatureUpdate = " dayNature_dbid = " + nature.getDbid() + ",";
                                        }

                                        updateDayNatureSQL = "Update EmployeeDailyAttendance set " + dayNatureUpdate + " workingcalendar_dbid = " + rotCalsMap.get(rDur).getDbid() + " Where employee_dbid = " + employee.getDbid() + "";
                                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                            updateDayNatureSQL += " And dailyDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDate));
                                        } else {
                                            updateDayNatureSQL += " And dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(empDate) + "'";
                                        }
                                        try {
                                            oem.executeEntityUpdateNativeQuery(updateDayNatureSQL, loggedUser);
                                        } catch (Exception ex) {
                                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    rDur = 0;
                                }
                                // remaining days for the last round
                                for (int r = rDur; r < numRoundsRem; r++) {
                                    int index = r % (roundDuration);
                                    isDayOffSet = false;
                                    if (rotCalsMap.get(index) == null) {
                                        continue;
                                    }
                                    nature = null;
                                    code = rotCalsMap.get(index).getCode();
                                    empDate = tempCal.getTime();
                                    if (empDate.compareTo(endDate) > 0) {
                                        break;
                                    }
                                    dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();
                                    tempCal.add(Calendar.DAY_OF_MONTH, 1);
                                    //dayName = "m" + (empDate.getMonth() + 1) + "D" + empDate.getDate();
                                    Object dayNature = BaseEntity.getValueFromEntity(employeeShift, dayName);
                                    String strDayNature = dayNature != null ? dayNature.toString() : null;
                                    if (strDayNature != null && strDayNature.equals("H")) {
                                        updateDayNatureSQL = "Update EmployeeDailyAttendance set workingcalendar_dbid = " + rotCalsMap.get(index).getDbid() + " Where employee_dbid = " + employee.getDbid() + "";
                                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                            updateDayNatureSQL += " And dailyDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDate));
                                        } else {
                                            updateDayNatureSQL += " And dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(empDate) + "'";
                                        }
                                        try {
                                            oem.executeEntityUpdateNativeQuery(updateDayNatureSQL, loggedUser);
                                        } catch (Exception ex) {
                                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                        }

                                        continue;
                                    }

                                    for (int k = 0; k < holidayDates.size(); k++) {
                                        String date = dateTimeUtility.DAYMONTH_FORMAT.format(empDate);
                                        String holidayDate = dateTimeUtility.DAYMONTH_FORMAT.format(holidayDates.get(k));
                                        if (date.equals(holidayDate)) {
                                            BaseEntity.setValueInEntity(annualPlanner, dayName, "H");
                                            BaseEntity.setValueInEntity(employeeShift, dayName, "H");
                                            nature = holidayDayNature;
                                            isDayOffSet = true;
                                            break;
                                        }
                                    }
                                    for (int k = 0; k < vacationDates.size(); k++) {
                                        String date = dateTimeUtility.DATE_FORMAT.format(empDate);
                                        String vacationDate = dateTimeUtility.DATE_FORMAT.format(vacationDates.get(k));
                                        if (date.equals(vacationDate)) {
                                            BaseEntity.setValueInEntity(annualPlanner, dayName, "V");
                                            BaseEntity.setValueInEntity(employeeShift, dayName, "V");
                                            nature = vacationDayNature;
                                            isDayOffSet = true;
                                            break;
                                        }
                                    }
                                    if (!isDayOffSet) {
                                        BaseEntity.setValueInEntity(employeeShift, dayName, code);
                                        if (rotCalsMap.get(index).getNumberOfHours().compareTo(BigDecimal.ZERO) == 0) {
                                            BaseEntity.setValueInEntity(annualPlanner, dayName, "O");
                                            nature = weekendDayNature;
                                        } else {
                                            BaseEntity.setValueInEntity(annualPlanner, dayName, "A");
                                            nature = absenceDayNature;
                                        }
                                    }
                                    String dayNatureUpdate = "";
                                    if (nature != null) {
                                        dayNatureUpdate = " dayNature_dbid = " + nature.getDbid() + ",";
                                    }

                                    updateDayNatureSQL = "Update EmployeeDailyAttendance set " + dayNatureUpdate + " workingcalendar_dbid = " + rotCalsMap.get(index).getDbid() + " Where employee_dbid = " + employee.getDbid() + "";
                                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                                        updateDayNatureSQL += " And dailyDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDate));
                                    } else {
                                        updateDayNatureSQL += " And dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(empDate) + "'";
                                    }

                                    try {
                                        oem.executeEntityUpdateNativeQuery(updateDayNatureSQL, loggedUser);
                                    } catch (Exception ex) {
                                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                                ofr.append(entitySetupService.callEntityUpdateAction(annualPlanner, loggedUser));
                                ofr.append(entitySetupService.callEntityUpdateAction(employeeShift, loggedUser));
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                //______________________________________________________________________
                String SQL;
                SQL = "Select dbid from EmployeeDailyAttendance where employee_dbid = " + employee.getDbid()
                        + " AND dailyDate >=";
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    SQL += dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()));
                } else {
                    SQL += " '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "'";
                }
                SQL += " and (timein is not null  or daynature_dbid=534374)";
                if (calHistory.getCurrentEffectiveEndDate() != null) {
                    SQL += " AND dailyDate <= ";
                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        SQL += dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()));
                    } else {
                        SQL += " '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()) + "'";

                    }
                }

                for (Object[] cal : calendars) {
                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        sql = "UPDATE employeedailyattendance set dayNature_dbid = " + holDbid + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                                + " and dailydate between " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(cal[0])) + " and " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(cal[1])) + "";
                    } else {
                        sql = "UPDATE employeedailyattendance set dayNature_dbid = " + holDbid + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                                + " and dailydate between cast('" + cal[0] + "' as date) and cast('" + cal[1] + "' as date)";
                    }
                    oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                }
                List<Object> objs = oem.executeEntityListNativeQuery(SQL, loggedUser);
                if (objs != null) {
                    EmployeeDailyAttendance attendance;
                    for (int i = 0; i < objs.size(); i++) {
                        attendance = (EmployeeDailyAttendance) oem.loadEntity("EmployeeDailyAttendance",
                                Long.parseLong(objs.get(i).toString()), null, null, loggedUser);
                        //set consolidation b false
                        attendance.setConsolidation(false);
                        if (attendance != null) {
                            entitySetupService.callEntityUpdateAction(attendance, loggedUser);
                        }
                    }
                }
                if (oldOffdays != null) {
                    EmployeeDailyAttendance attendance;
                    for (int i = 0; i < oldOffdays.size(); i++) {
                        attendance = (EmployeeDailyAttendance) oem.loadEntity("EmployeeDailyAttendance",
                                Long.parseLong(oldOffdays.get(i).toString()), null, null, loggedUser);
                        //set consolidation b false
                        attendance.setConsolidation(false);
                        if (attendance != null) {
                            entitySetupService.callEntityUpdateAction(attendance, loggedUser);
                        }
                    }
                }
                //______________________________________________________________________
                if (check == true) {
                    calHistory.setDone(true);
                    oem.saveEntity(calHistory, loggedUser);
                }
                Long calculatedperiodDbid;
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    BigDecimal calculatedperiod = (BigDecimal) oem.executeEntityNativeQuery("select dbid from calculatedperiod where closed like 'N' and code like '%M%' and startDate=(select min(startdate) from calculatedperiod where closed like 'N' and code like '%M%')", loggedUser);
                    if (calculatedperiod == null) {
                        calculatedperiodDbid = Long.parseLong("0");
                    } else {
                        calculatedperiodDbid = Long.parseLong(calculatedperiod.toString());
                    }
                } else {
                    Long calculatedperiod = (Long) oem.executeEntityNativeQuery("select dbid from calculatedperiod where closed like 'N' and code like '%M%' and startDate=(select min(startdate) from calculatedperiod where closed like 'N' and code like '%M%')", loggedUser);
                    calculatedperiodDbid = Long.parseLong(calculatedperiod.toString());
                }

                calculateConsolidationForSingleEmployee(employee.getDbid(), calculatedperiodDbid, loggedUser);
            }

            updateEmployeeVacationRequest(employee, startDate, endDate, loggedUser);
            if (calHistory.getCurrentWorkingCal().getType().getCode().equalsIgnoreCase("flexible") || calHistory.getCurrentWorkingCal().getType().getCode().equalsIgnoreCase("regular")) {
                conds.clear();
                conds.add("calendar.dbid = " + calHistory.getCurrentWorkingCal().getDbid());
                List<NormalWorkingCalendar> rotShfts = oem.loadEntityList(NormalWorkingCalendar.class.getSimpleName(), conds, null, null, loggedUser);
                String sqlDailyAttendance;
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    sqlDailyAttendance = "UPDATE EmployeeDailyAttendance Set workingcalendar_dbid = " + rotShfts.get(0).getDbid()
                            + " where employee_dbid = " + calHistory.getEmployee().getDbid() + " AND dailyDate between " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(startDate)) + " and "
                            + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(endDate));
                } else {
                    sqlDailyAttendance = "UPDATE EmployeeDailyAttendance Set workingcalendar_dbid = " + rotShfts.get(0).getDbid()
                            + " where employee_dbid = " + calHistory.getEmployee().getDbid() + " AND dailyDate between '" + dateTimeUtility.DATE_FORMAT.format(startDate) + "' and '"
                            + dateTimeUtility.DATE_FORMAT.format(endDate) + "'";
                }

                oem.executeEntityUpdateNativeQuery(sqlDailyAttendance, loggedUser);
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), e);
        }
        return ofr;
    }

    public OFunctionResult setHoliday(Long employeeDbid, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        String plannerSql = "";
        String DASql = "";
        String shiftSql = "";
        int month = 0;
        int day = 0;
        BigDecimal dayNature = BigDecimal.ZERO;
        String sql = "Select holidaydate from employeeHoliday where employee_dbid = " + employeeDbid + " AND deleted = 0";
        try {
            List<Date> holidayDates = oem.executeEntityListNativeQuery(sql, loggedUser);
            for (int i = 0; i < holidayDates.size(); i++) {
                month = (holidayDates.get(i).getMonth() + 1);
                day = (holidayDates.get(i).getDate());

                sql = "Select cast(daynature_dbid as int) from employeedailyattendance where employee_dbid = " + employeeDbid
                        + " AND dailydate = "
                        + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(holidayDates.get(i)));
                dayNature = (BigDecimal) oem.executeEntityNativeQuery(sql, loggedUser);

                if (dayNature.intValue() == 534377) {
                    plannerSql = "update employeeannualplanner set ";
                    plannerSql += "m" + month + "D" + day + "='F' ";
                    plannerSql += "where planneryear=2017 and employee_dbid=" + employeeDbid;
                    oem.executeEntityUpdateNativeQuery(plannerSql, loggedUser);

                    DASql = "UPDATE employeedailyattendance set dayNature_dbid = 579725"
                            + " where employee_dbid = " + employeeDbid
                            + " and dailydate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(holidayDates.get(i))) + "";
                    oem.executeEntityUpdateNativeQuery(DASql, loggedUser);

                    shiftSql = "update employeeshift set ";
                    shiftSql += "m" + month + "D" + day + "='F' ";
                    shiftSql += "where planneryear=2017 and employee_dbid=" + employeeDbid;
                    oem.executeEntityUpdateNativeQuery(shiftSql, loggedUser);
                } else {
                    plannerSql = "update employeeannualplanner set ";
                    plannerSql += "m" + month + "D" + day + "='H' ";
                    plannerSql += "where planneryear=2017 and employee_dbid=" + employeeDbid;
                    oem.executeEntityUpdateNativeQuery(plannerSql, loggedUser);

                    DASql = "UPDATE employeedailyattendance set dayNature_dbid = 534375"
                            + " where employee_dbid = " + employeeDbid
                            + " and dailydate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(holidayDates.get(i))) + "";
                    oem.executeEntityUpdateNativeQuery(DASql, loggedUser);

                    shiftSql = "update employeeshift set ";
                    shiftSql += "m" + month + "D" + day + "='H' ";
                    shiftSql += "where planneryear=2017 and employee_dbid=" + employeeDbid;
                    oem.executeEntityUpdateNativeQuery(shiftSql, loggedUser);
                }
                day = 0;
                month = 0;
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return ofr;
    }

    /*
     * validating on uniquness of the codes of the working calendars (shift, reg, rotation)
     */
    @Override
    public OFunctionResult validateShift(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            NormalWorkingCalendar workingCalendar = (NormalWorkingCalendar) odm.getData().get(0);
            if (workingCalendar != null) {
                List<String> conds = new ArrayList<String>();
                conds.add("calendar.dbid = " + workingCalendar.getCalendar().getDbid());
                List<NormalWorkingCalendar> workingCalendars = oem.loadEntityList(
                        NormalWorkingCalendar.class.getSimpleName(), conds, null, null, loggedUser);
                for (int i = 0; i < workingCalendars.size(); i++) {
                    if (workingCalendar.getCode().equals(workingCalendars.get(i).getCode())) {
                        ofr.addError(usrMsgService.getUserMessage("CodesNotUnique", loggedUser));
                        return ofr;
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    /*
     * validating on the codes of the working calendars such that no preserved codes are used like; O,H,V,A
     */
    @Override
    public OFunctionResult validateNormalWorkingCalendar(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            NormalWorkingCalendar calendar = (NormalWorkingCalendar) odm.getData().get(0);
            if (calendar != null) {
                if (calendar.getCode().equals("O")
                        || calendar.getCode().equals("H")
                        || calendar.getCode().equals("V")
                        || calendar.getCode().equals("A")) {
                    ofr.addError(usrMsgService.getUserMessage("PreservedCodes", loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
//            oem.closeEM(loggedUser);
            return ofr;
        }
    }

    @Override
    public OFunctionResult validateOTReqUpdate(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeOTRequest employeeOTRequest = (EmployeeOTRequest) odm.getData().get(0);
            if (employeeOTRequest != null) {
                List<String> conds = new ArrayList<String>();
                conds.add("dbid = " + employeeOTRequest.getDbid());
                EmployeeOTRequest loadedEmployeeOTRequest = (EmployeeOTRequest) oem.loadEntity(
                        EmployeeOTRequest.class.getSimpleName(), conds, null, loggedUser);
                if (loadedEmployeeOTRequest != null) {
                    if (!employeeOTRequest.getRequestedDate().equals(loadedEmployeeOTRequest.getRequestedDate())) {
                        ofr.addError(usrMsgService.getUserMessage("OTDateUpdate", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    /*
     * [Loubna]
     * validating that the number of working calendars within the same Regular TM calendar is only 1
     */
    @Override
    public OFunctionResult validateNumOfRegularCal(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<NormalWorkingCalendar> workingCalendars = (List<NormalWorkingCalendar>) odm.getData().get(1);

            if (workingCalendars != null && !workingCalendars.isEmpty()) {
                if (workingCalendars.get(0).getCalendar().getType().getCode().equals("Regular")
                        || workingCalendars.get(0).getCalendar().getType().getCode().equals("Flexible")) {
                    if (workingCalendars.size() > 1) {
                        ofr.addError(usrMsgService.getUserMessage("NumOfRegularCal", loggedUser));
                        return ofr;
                    }
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    // Loubna - 08/01/2014 - Jira: OTMS-184
    @Override
    public OFunctionResult validateExcuseVacation(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest vacRequest = (EmployeeVacationRequest) odm.getData().get(0);
            if (vacRequest != null && vacRequest.getStartDate() != null) {
                if (vacRequest.getVacation().getUnitOfMeasure().equals("H")) {
                    Employee employee = vacRequest.getEmployee();
                    TMCalendar calendar = getEmpCalendar(vacRequest.getStartDate(), employee, loggedUser);
                    NormalWorkingCalendar normalCalendar = null;
                    String timeIn = "";
                    DateFormat df = new SimpleDateFormat("HH:mm");
                    List<String> conds = new ArrayList<String>();

                    if (calendar.getType().getCode().equals("Regular") || calendar.getType().getCode().equals("Flexible")) {
                        conds.add("calendar.dbid = " + calendar.getDbid());
                        normalCalendar = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(),
                                conds, null, loggedUser);
                    } else if (calendar.getType().getCode().equals("VariableShift")) {
                        conds.clear();
                        conds.add("employee.dbid = " + employee.getDbid());
                        conds.add("plannerYear = '" + dateTimeUtility.YEAR_FORMAT.format(vacRequest.getStartDate()) + "'");
                        EmployeeShift empShift = (EmployeeShift) oem.loadEntity(EmployeeShift.class.getSimpleName(), conds, null, loggedUser);

                        if (BaseEntity.getValueFromEntity(empShift, "m" + Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(vacRequest.getStartDate())) + "D" + Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(vacRequest.getStartDate()))) == null) {
                            return ofr;
                        }

                        String dayShift = BaseEntity.getValueFromEntity(empShift, "m" + Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(vacRequest.getStartDate())) + "D" + Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(vacRequest.getStartDate()))).toString();

                        conds.clear();
                        conds.add("calendar.dbid = " + calendar.getDbid());
                        conds.add("code = '" + dayShift + "'");
                        normalCalendar = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(),
                                conds, null, loggedUser);
                    } else { // case of rotation
                        Object[] returned = getShift(calendar, vacRequest.getStartDate(), loggedUser);
                        normalCalendar = (NormalWorkingCalendar) returned[1];
                    }

                    if (normalCalendar == null) {
                        return ofr;
                    }

                    timeIn = normalCalendar.getTimeIn();
                    if (!normalCalendar.isIgnoreTolerence() && normalCalendar.getTolerence() != null) {
                        timeIn = dateTimeUtility.addTime(timeIn, dateTimeUtility.convertMinutesToTime(normalCalendar.getTolerence().doubleValue()));
                    }

                    String exceptionFrom = df.format(vacRequest.getStartDate());
//                    if (dateTimeUtility.compareTime(exceptionFrom, timeIn) < 0) {
//                        ofr.addError(usrMsgService.getUserMessage("ExcuseInLessThanTolIn", loggedUser)); // stop on error
//                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult postManualEntry(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            ManualDailyAttendance empDAEntry = (ManualDailyAttendance) odm.getData().get(0);
            if (empDAEntry != null) {
                // 1- load the employeeDA header record corresponding to that day
                Date dailyDate = empDAEntry.getDailyDate(),
                        outDate = empDAEntry.getOutDate();
                String timeIn = "",
                        timeOut = "";
                EmployeeDailyAttendance empDA = null;
                List<String> conds = new ArrayList<String>();

                Employee employee = (Employee) oem.loadEntity(Employee.class.getSimpleName(),
                        Collections.singletonList("code = '" + empDAEntry.getCode() + "'"), null, loggedUser);
                if (dailyDate != null && employee != null) {
                    conds.add("employee.dbid = " + employee.getDbid());
                    conds.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(dailyDate) + "'");
                    conds.add("header = true");
                    empDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);
                }

                // 2- update that day
                if (empDA != null) {
                    timeIn = empDAEntry.getInTime();
                    timeOut = empDAEntry.getOutTime();

                    EmployeeDailyAttendance detailEmpDA = null;

                    // check whether it is update or create
                    conds.clear();
                    conds.add("manualDA.dbid = " + empDAEntry.getDbid());
                    detailEmpDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(),
                            conds, null, loggedUser);
                    if (detailEmpDA == null) {
                        detailEmpDA = new EmployeeDailyAttendance();
                        detailEmpDA.setDailyDate(dailyDate);
                        detailEmpDA.setTimeIn(timeIn);
                        detailEmpDA.setTimeOut(timeOut);
                        if (outDate == null) {
                            detailEmpDA.setOutDate(dailyDate);
                        } else {
                            detailEmpDA.setOutDate(outDate);
                        }
                        detailEmpDA.setDayNature(empDA.getDayNature());
                        detailEmpDA.setEmpDAHeader(empDA);
                        detailEmpDA.setEmployee(employee);
                        detailEmpDA.setManualDA(empDAEntry);
                        entitySetupService.callEntityCreateAction(detailEmpDA, loggedUser);
                    } else {
                        detailEmpDA.setTimeIn(timeIn);
                        detailEmpDA.setTimeOut(timeOut);
                        if (outDate == null) {
                            detailEmpDA.setOutDate(dailyDate);
                        } else {
                            detailEmpDA.setOutDate(outDate);
                        }
                        entitySetupService.callEntityUpdateAction(detailEmpDA, loggedUser);
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult setPlannerTransaction(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            setPlannerForNonExist(odm, params, loggedUser);
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    @Override
    public OFunctionResult setTM(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            OLog.logInformation("******** ^setTM^ : Creating Employee Planner & Employee Daily Attendance Records ********");

            ofr = calendarSer.setPlannerForNonExist(odm, params, loggedUser);

//            HashMap emps = new HashMap();
//            emps.put(1, returnedEmpPlanners);
//            emps.put(2, rotEmpsCals);
//            params = new OFunctionParms();
//            params.setParams(emps);
//            ofr.append(setDAForNonExist(odm, params, loggedUser));
            OLog.logInformation("******** Finished Creating Employee Planner & Employee Daily Attendance Records ********");
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    /* ************************** */
    /*  Helper Private Frunctions */
    /* ************************** */
    private String calculatePlannedWork(String planIn, String planOut, String planBreakIn, String planBreakOut) {
        String plannedWork = "00:00";

        if (planOut != null && planIn != null && !planIn.equals("") && !planOut.equals("")) {
            plannedWork = dateTimeUtility.subtractTime(planIn, planOut);
        }

        if (planBreakIn != null && planBreakOut != null && !planBreakIn.equals("") && !planBreakOut.equals("")) {
            plannedWork = dateTimeUtility.subtractTime(dateTimeUtility.subtractTime(planBreakIn, planBreakOut), plannedWork);
        }

        return plannedWork;
    }

    private String calculateActualWork(String timeIn, String timeOut, String breakIn, String breakOut) {
        String actualWork = "00:00";

        if (timeIn != null && timeOut != null) {
            actualWork = dateTimeUtility.subtractTime(timeIn, timeOut);
        }

        if (breakIn != null && breakOut != null && !breakIn.equals("") && !breakOut.equals("")) {
            actualWork = dateTimeUtility.subtractTime(dateTimeUtility.subtractTime(breakIn, breakOut), actualWork);
        }

        return actualWork;
    }

    private String calculateDelay(String timeIn, String planIn, String tolerence, boolean isIgnoreTolerence, String exceptionFrom, String exceptionTo) {
        String delay = "00:00";
        if (timeIn != null && planIn != null && !timeIn.equals("") && !planIn.equals("")) {
            String tolerentPlanneIn = dateTimeUtility.addTime(planIn, tolerence);
            if (dateTimeUtility.compareTime(timeIn, planIn) == 1) {
                delay = dateTimeUtility.subtractTime(planIn, timeIn);

                if (dateTimeUtility.compareTime(timeIn, tolerentPlanneIn) <= 0) {
                    delay = "00:00";
                } else if (!isIgnoreTolerence) {
                    delay = dateTimeUtility.subtractTime(tolerentPlanneIn, timeIn);
                }

                // Exception Part
                //*****************
                // delayIn = planIn
                // delayOut = timeIn
                if (exceptionFrom != null && exceptionTo != null) {
                    // exception will be considered in the delay calculation if it is after the start of the day
                    if (!exceptionFrom.equals("") && !exceptionTo.equals("")
                            && dateTimeUtility.compareTime(exceptionTo, tolerentPlanneIn) > 0) {
                        delay = "00:00";
                        if (dateTimeUtility.compareTime(timeIn, tolerentPlanneIn) > 0) {
                            if (dateTimeUtility.compareTime(timeIn, exceptionFrom) <= 0) {
                                if (isIgnoreTolerence) {
                                    delay = dateTimeUtility.subtractTime(planIn, timeIn);
                                } else {
                                    delay = dateTimeUtility.subtractTime(tolerentPlanneIn, timeIn);
                                }
                            } else if (dateTimeUtility.compareTime(timeIn, exceptionFrom) > 0
                                    && dateTimeUtility.compareTime(timeIn, exceptionTo) <= 0
                                    && dateTimeUtility.compareTime(exceptionFrom, tolerentPlanneIn) > 0) {
                                if (isIgnoreTolerence) {
                                    delay = dateTimeUtility.subtractTime(planIn, exceptionFrom);
                                } else {
                                    delay = dateTimeUtility.subtractTime(tolerentPlanneIn, exceptionFrom);
                                }
                            } else if (dateTimeUtility.compareTime(timeIn, exceptionTo) > 0) {
                                if (dateTimeUtility.compareTime(exceptionFrom, tolerentPlanneIn) > 0) {
                                    delay = dateTimeUtility.subtractTime(tolerentPlanneIn, exceptionFrom);
                                }
                                delay = dateTimeUtility.addTime(delay, dateTimeUtility.subtractTime(exceptionTo, timeIn));
                            }
                        }
                    }
                }
            }
        }
        return delay;
    }

    private String calculateEarlyLeave(String timeOut, String planOut, String planIn, String tolerence,
            boolean isIgnoreTolerence, String exceptionFrom, String exceptionTo) {
        String earlyLeave = "00:00";
        if (timeOut != null && planOut != null && !timeOut.equals("") && !planOut.equals("")) {

            String tolerentPlanneOut = dateTimeUtility.addTime(timeOut, tolerence);
            if (dateTimeUtility.compareTime(timeOut, planOut) < 0) {

                if (dateTimeUtility.compareTime(timeOut, planIn) >= 0) {
                    // issue #219 solution
                    if (tolerence.equals("00:00")) {
                        earlyLeave = dateTimeUtility.subtractTime(timeOut, planOut);
                    } else {
                        if (dateTimeUtility.compareTime(tolerentPlanneOut, planOut) < 0) {
                            earlyLeave = dateTimeUtility.subtractTime(timeOut, planOut);
                        } else {
                            tolerentPlanneOut = planOut;
                        }
                        if (dateTimeUtility.compareTime(tolerentPlanneOut, timeOut) <= 0) {
                            earlyLeave = "00:00";
                        } else if (!isIgnoreTolerence) {
                            earlyLeave = dateTimeUtility.subtractTime(tolerentPlanneOut, planOut);
                        }
                    }
                    // Exception Part
                    //****************
                    // earlyIn = timeOut
                    // earlyOut = planOut
                    if (exceptionFrom != null && exceptionTo != null) {
                        if (!exceptionFrom.equals("") && !exceptionTo.equals("")) {
                            List<String> temp = getInterval(exceptionFrom, exceptionTo,
                                    timeOut, planOut);
                            if (temp != null && !temp.isEmpty()) {
                                String earlyLeaveException = temp.get(0);
                                if (earlyLeaveException != null && !earlyLeaveException.equals("00:00")) {
                                    earlyLeave = dateTimeUtility.subtractTime(earlyLeaveException, earlyLeave);
                                }
                            }
                        }
                    }
                }
            }
        }
        return earlyLeave;
    }

    private int getCurrentDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek;
    }

    private List<Method> getDateFromDay(Date dateFrom, Date dateTo,
            UDC dayOff1) {
        int day1Index = Integer.parseInt(dayOff1.getCode());

        int dayDifference = 7;

        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(dateFrom);

        SimpleDateFormat monthFormat = new SimpleDateFormat("M");
        int startingMonth = Integer.parseInt(monthFormat.format(dateFrom));
        int endMonth = Integer.parseInt(monthFormat.format(dateTo));

        int firstDay = getCurrentDayOfWeek(currentDate.getTime());
        int monthCount = startingMonth;

        SimpleDateFormat dayFormat = new SimpleDateFormat("d");
        int dayCount = Integer.parseInt(dayFormat.format(dateFrom));

        DateTime currentDateDT;

        List<Method> retWeekends = new ArrayList<Method>();
        Method weekendMethod = null;

        boolean checkIfWhile = false;

        Class types[] = new Class[1];
        types[0] = String.class;

        currentDate.add(Calendar.DATE, day1Index - firstDay);

        currentDateDT = new DateTime(currentDate, dateTimeUtility.dateTimeZone);

        if (day1Index - firstDay < 0 && currentDateDT.getMonthOfYear() != monthCount) {
            endMonth++;
        }

        while (monthCount <= endMonth) {
            checkIfWhile = false;
            while ((dayCount + 7) <= 31) {
                if (monthCount == 2 && (dayCount + 7) > 29) {
                    break;
                } else if ((monthCount == 4 || monthCount == 6
                        || monthCount == 11 || monthCount == 9) && (dayCount + 7) > 30) {
                    break;
                }
                try {
                    weekendMethod = EmployeeAnnualPlanner.class.getMethod("setM" + currentDateDT.getMonthOfYear() + "D" + currentDateDT.getDayOfMonth(), types);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                retWeekends.add(weekendMethod);
                dayCount = currentDateDT.getDayOfMonth();

                currentDate.add(Calendar.DATE, (dayDifference));
                currentDateDT = new DateTime(currentDate, dateTimeUtility.dateTimeZone);
                checkIfWhile = true;
            }
            if (!checkIfWhile) {
                try {
                    weekendMethod = EmployeeAnnualPlanner.class.getMethod("setM" + currentDateDT.getMonthOfYear() + "D" + currentDateDT.getDayOfMonth(), types);
                } catch (Exception ex) {
                    Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                }
                retWeekends.add(weekendMethod);
                dayCount = currentDateDT.getDayOfMonth();

                currentDate.add(Calendar.DATE, (dayDifference));
                currentDateDT = new DateTime(currentDate, dateTimeUtility.dateTimeZone);
            }
            monthCount++;
            dayCount = 1;
        }
        return retWeekends;
    }

    private void setHijriCalendarReflection(HijriCalendar hijriCalendar, HijriCalendarReflection reflection) {
        Date from = hijriCalendar.getDateFrom();
        Date to = hijriCalendar.getDateTo();

        Chronology arabic = IslamicChronology.getInstance();

        DateTime fromDT = new DateTime(from, dateTimeUtility.dateTimeZone);
        DateTime arabicFromDT = fromDT.withChronology(arabic);
        GregorianCalendar gregCalendarFrom = arabicFromDT.toGregorianCalendar();
        Date gregFrom = gregCalendarFrom.getTime();

        DateTime toDT = new DateTime(to, dateTimeUtility.dateTimeZone);
        DateTime arabicToDT = toDT.withChronology(arabic);
        GregorianCalendar gregCalendarTo = arabicToDT.toGregorianCalendar();
        Date gregTo = gregCalendarTo.getTime();
        if (from != null) {
            reflection.setGregorianFrom(gregFrom);
        }
        if (to != null) {
            reflection.setGregorianTo(gregTo);
        }
    }

    public List<String> getEmployeeVacations(Date start, Date end, int currentYear) {
        List<String> vacationMethods = new ArrayList<String>();

        DateTimeZone dateTimeZone = DateTimeZone.forID("EET");
        DateTime startDateTime = new DateTime(start, dateTimeZone);
        DateTime endDateTime = new DateTime(end, dateTimeZone);

        DateTime tempDT = null;

        if (currentYear == startDateTime.getYear() && currentYear == endDateTime.getYear()) {
            tempDT = startDateTime;
            endDateTime = endDateTime;
            do {
                vacationMethods.add("m" + tempDT.getMonthOfYear() + "D" + (tempDT.getDayOfMonth()));
                tempDT = tempDT.plusDays(1);
            } while (tempDT.compareTo(endDateTime) == -1 || tempDT.compareTo(endDateTime) == 0);
        } else if (currentYear == startDateTime.getYear() && currentYear != endDateTime.getYear()) {
            tempDT = startDateTime;
            do {
                vacationMethods.add("m" + tempDT.getMonthOfYear() + "D" + (tempDT.getDayOfMonth()));
                tempDT = tempDT.plusDays(1);
            } while (tempDT.getYear() == currentYear);
        } else if (currentYear != startDateTime.getYear() && currentYear == endDateTime.getYear()) {
            tempDT = endDateTime;
            do {
                vacationMethods.add("m" + tempDT.getMonthOfYear() + "D" + (tempDT.getDayOfMonth()));
                tempDT = tempDT.minusDays(1);
            } while (tempDT.getYear() == currentYear);
        }

        return vacationMethods;
    }

    public List<Date> getDateFromDay(Date startDate, Date endDate,
            int dayOffIndex) {
        List<Date> dates = new ArrayList<Date>();

        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(startDate);

        int firstDay = getCurrentDayOfWeek(currentDate.getTime());

        currentDate.add(Calendar.DATE, dayOffIndex - firstDay);

        /**/
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate.getTime());
        Date tempDate = currentDate.getTime();
        while (tempDate.before(endDate)) {
            dates.add(tempDate);
            calendar.add(Calendar.DAY_OF_MONTH, 7);
            tempDate = calendar.getTime();
        }

        return dates;
    }

    public List<Date> getHolidayDates(Date from, Date to) {

        List<Date> retDates = new ArrayList<Date>();

        DateTime fromDT = new DateTime(from, dateTimeUtility.dateTimeZone);
        DateTime toDT = new DateTime(to, dateTimeUtility.dateTimeZone);

        Calendar tempCal = Calendar.getInstance();
        tempCal.setTime(from);

        int numberOfDays = Days.daysBetween(fromDT, toDT).getDays();

        for (int k = 0; k <= numberOfDays; k++) {
            retDates.add(tempCal.getTime());
            tempCal.add(tempCal.DAY_OF_MONTH, 1);
        }
        return retDates;
    }

    //<editor-fold defaultstate="collapsed" desc="overtime calculations">
    private List<String> getInterval(String firstArg, String secondArg, String intervalFrom,
            String intervalTo) {
        List<String> results = new ArrayList<String>();
        String intersection = "00:00";
        if ((dateTimeUtility.compareTime(firstArg, intervalFrom) == 0 || dateTimeUtility.compareTime(firstArg, intervalFrom) == -1)
                && (dateTimeUtility.compareTime(intervalFrom, secondArg) == 0 || dateTimeUtility.compareTime(intervalFrom, secondArg) == -1)) // firstArg <= intervalFrom <= secondArg
        {
            if ((dateTimeUtility.compareTime(firstArg, intervalTo) == 0 || dateTimeUtility.compareTime(firstArg, intervalTo) == -1)
                    && (dateTimeUtility.compareTime(intervalTo, secondArg) == 0 || dateTimeUtility.compareTime(intervalTo, secondArg) == -1)) // firstArg <= intervalTo <= secondArg
            {
                intersection = dateTimeUtility.addTime(intersection, dateTimeUtility.subtractTime(intervalFrom, intervalTo));
                results.add(intersection);
                results.add(intervalFrom);
                results.add(intervalTo);
            } else {
                intersection = dateTimeUtility.addTime(intersection, dateTimeUtility.subtractTime(intervalFrom, secondArg));
                results.add(intersection);
                results.add(intervalFrom);
                results.add(secondArg);
            }
        } else if ((dateTimeUtility.compareTime(firstArg, intervalTo) == 0 || dateTimeUtility.compareTime(firstArg, intervalTo) == -1)
                && (dateTimeUtility.compareTime(intervalTo, secondArg) == 0 || dateTimeUtility.compareTime(intervalTo, secondArg) == -1)) // firstArg <= intervalTo <= secondArg
        {
            intersection = dateTimeUtility.addTime(intersection, dateTimeUtility.subtractTime(firstArg, intervalTo));
            results.add(intersection);
            results.add(firstArg);
            results.add(intervalTo);
        } else if ((dateTimeUtility.compareTime(intervalFrom, firstArg) == -1)
                && (dateTimeUtility.compareTime(intervalTo, secondArg) == 1)) // the interval of day or night is greater than the period
        {
            intersection = dateTimeUtility.addTime(intersection, dateTimeUtility.subtractTime(firstArg, secondArg));
            results.add(intersection);
            results.add(firstArg);
            results.add(secondArg);
        }
        return results;
    }

    private EmployeeWorkingCalHistory updateNewHistoryOrigCalendarAndSeqForCase1And3(EmployeeWorkingCalHistory calHistory,
            Date loadedStartDate, String origSequence, TMCalendar origCal, OUser user) {
        try {
            String sequence = origSequence;

            Integer sequenceVal = Integer.parseInt(sequence);
            sequenceVal += (dateTimeUtility.getDaysBetween(calHistory.getCurrentEffectiveDate(), loadedStartDate) + 1);

            sequenceVal = getFinalSequenceAfterModRoundDuration(sequenceVal, origCal.getDbid(), user);

            sequence = sequenceVal.toString();

            calHistory.setOriginalCalSeq(sequence);
            calHistory.setOriginalCalendar(origCal);
        } catch (Exception e) {
            OLog.logException(e, user);
        }
        return calHistory;
    }

    private String getOrigHistoryNewOriginalSeq(Date newStart, String origCalSeq, long origCalDbid, Long toBeUpdatedDbid, OUser user) {
        String tempRotSeq = origCalSeq;
        Integer tempRotSeqVal = Integer.parseInt(tempRotSeq);
        try {
            String selectStat = "select CurrentEffectiveDate from employeeworkingcalhistory "
                    + " where dbid = " + toBeUpdatedDbid + " and deleted = 0";

            Object resultLst = oem.executeEntityNativeQuery(selectStat, user);
            //Object[] resultArr = (Object[]) resultLst.get(0);
            Date startDate = (Date) (resultLst);

            tempRotSeqVal += (dateTimeUtility.getDaysBetween(startDate, newStart) + 1);
            tempRotSeqVal = getFinalSequenceAfterModRoundDuration(tempRotSeqVal, origCalDbid, user);
        } catch (Exception ex) {
            tempRotSeqVal = 0;
            OLog.logException(ex, user);
        }
        return tempRotSeqVal.toString();
    }

    private void updateEmpDailyAttendanceCalendarOnCalendarChange(EmployeeWorkingCalHistory calHistory, String calendarType,
            NormalWorkingCalendar empRegCal, Employee employee, OUser loggedUser) {
        Date tempDate = calHistory.getCurrentEffectiveDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(tempDate);

        long cal_dbid = 0;
        if (calendarType.equals("Regular")) {
            cal_dbid = empRegCal.getDbid();
        }

        String updateDA;

        try {
            Date endDate = calHistory.getCurrentEffectiveEndDate();
            if (endDate == null) {
                endDate = dateTimeUtility.DATE_FORMAT.parse((dateTimeUtility.YEAR_FORMAT.format(new Date()) + "-12-31"));
            }
            while (!tempDate.after(endDate)) {

                updateDA = "update EmployeeDailyAttendance set workingcalendar_dbid = " + cal_dbid;
                updateDA += " where deleted = 0 and employee_dbid = " + employee.getDbid()
                        + " and dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(tempDate) + "'";
                try {
                    oem.executeEntityUpdateNativeQuery(updateDA, loggedUser);
                } catch (Exception e) {
                    OLog.logException(e, loggedUser);
                }
                cal.add(Calendar.DAY_OF_MONTH, 1);
                tempDate = cal.getTime();
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private List<EmployeeWorkingCalHistory> getEmployeeCalHistoriesWithinDates(Date startDate, Date endDate, Employee emp,
            OUser loggedUser) {
        List<EmployeeWorkingCalHistory> calHistorys = new ArrayList<EmployeeWorkingCalHistory>();

        try {
            String SQL;
            SQL = "select dbid from EmployeeWorkingCalHistory"
                    + " where employee_dbid = " + emp.getDbid()
                    + " and ((" + dateTimeUtility.YEAR_FORMAT.format(startDate) + " >= year(currenteffectivedate) and "
                    + " year(currenteffectiveenddate) >= " + dateTimeUtility.YEAR_FORMAT.format(endDate) + ")"
                    + " or (year(currenteffectivedate)<=" + dateTimeUtility.YEAR_FORMAT.format(startDate) + " and"
                    + " currenteffectiveenddate is null)"
                    + " and cancelled = 0 and deleted = 0)";
            List dbids = (List) oem.executeEntityListNativeQuery(SQL, loggedUser);
            for (int i = 0; i < dbids.size(); i++) {
                Object historyDbid = (Object) dbids.get(i);
                calHistorys.add((EmployeeWorkingCalHistory) oem.loadEntity(
                        EmployeeWorkingCalHistory.class.getSimpleName(), Long.parseLong(historyDbid.toString()), null, null, loggedUser));
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return calHistorys;
    }

    private void fillRegularCalMapWithOffdaysDates(OUser loggedUser,
            Map<TMCalendar, List<Date>> regCalendarsMap, TMCalendar regCalendar, Integer currentYear) {

        if (regCalendarsMap.containsKey(regCalendar)) {
            return;
        }

        UDC dayOff1;
        UDC dayOff2;
        int offdaysNumber = 0;
        List<String> conds = new ArrayList<String>();
        conds.clear();
        conds.add("calendar.dbid = " + regCalendar.getDbid());
        try {
            List<NormalWorkingCalendar> nwc = oem.loadEntityList(NormalWorkingCalendar.class.getSimpleName(), conds, null, null, loggedUser);
            for (int k = 0; k < nwc.size(); k++) {
                dayOff1 = nwc.get(k).getDayOff1();
                dayOff2 = nwc.get(k).getDayOff2();

                if (dayOff1 != null) {
                    offdaysNumber++;
                }
                if (dayOff2 != null) {
                    offdaysNumber++;
                }

                for (int j = 0; j < offdaysNumber; j++) {
                    if (j == 0) {
                        regCalendarsMap.put(regCalendar, getDateFromDay(dateTimeUtility.DATE_FORMAT.parse(currentYear + "-01-01"),
                                dateTimeUtility.DATE_FORMAT.parse(currentYear + "-12-31"), Integer.parseInt(dayOff1.getCode())));
                    } else {
                        List<Date> dates = regCalendarsMap.get(regCalendar);
                        dates.addAll(getDateFromDay(dateTimeUtility.DATE_FORMAT.parse(currentYear + "-01-01"),
                                dateTimeUtility.DATE_FORMAT.parse(currentYear + "-12-31"), Integer.parseInt(dayOff2.getCode())));
                        regCalendarsMap.put(regCalendar, dates);
                    }
                    Collections.sort(regCalendarsMap.get(regCalendar));
                }
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private boolean isMissionDay(Date dailyDate, Employee employee, OUser loggedUser) throws Exception {
        String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
        String sql;
        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
            sql = "SELECT edo.DBID FROM employeedayoffrequest edo, dayoff do"
                    + " WHERE do.DBID = edo.VACATION_DBID"
                    + " AND do.mission = 1"
                    + " AND do.UNITOFMEASURE like 'D'"
                    + " AND CANCELLED = 'N' and " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(dailyDate)) + " between edo.STARTDATE AND edo.ENDDATE"
                    + " AND edo.employee_dbid = " + employee.getDbid();
        } else {
            sql = "SELECT edo.DBID FROM employeedayoffrequest edo, dayoff do"
                    + " WHERE do.DBID = edo.VACATION_DBID"
                    + " AND do.mission = 1"
                    + " AND do.UNITOFMEASURE like 'D'"
                    + " AND CANCELLED = 'N' and CAST('" + dateTimeUtility.DATE_FORMAT.format(dailyDate) + "' AS DATE) between edo.STARTDATE AND edo.ENDDATE"
                    + " AND edo.employee_dbid = " + employee.getDbid();
        }
        Object result = oem.executeEntityNativeQuery(sql, loggedUser);
        if (result != null) {
            return true;
        }
        return false;
    }

    private boolean isVacationDay(Date dailyDate, Employee employee, OUser loggedUser) throws Exception {
        String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
        String sql;
        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
            sql = "SELECT edo.DBID FROM employeedayoffrequest edo, dayoff do"
                    + " WHERE do.DBID = edo.VACATION_DBID and mission = 0"
                    + " AND do.UNITOFMEASURE like 'D'"
                    + " AND CANCELLED = 'N' and " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(dailyDate)) + " between edo.STARTDATE AND edo.ENDDATE"
                    + " AND edo.employee_dbid = " + employee.getDbid();
        } else {
            sql = "SELECT edo.DBID FROM employeedayoffrequest edo, dayoff do"
                    + " WHERE do.DBID = edo.VACATION_DBID and mission = 0"
                    + " AND do.UNITOFMEASURE like 'D'"
                    + " AND CANCELLED = 'N' and CAST('" + dateTimeUtility.DATE_FORMAT.format(dailyDate) + "' AS DATE) between edo.STARTDATE AND edo.ENDDATE"
                    + " AND edo.employee_dbid = " + employee.getDbid();
        }
        Object result = oem.executeEntityNativeQuery(sql, loggedUser);
        if (result != null) {
            return true;
        }
        return false;
    }

    private void updateEmployeeVacationRequest(Employee employee, Date startDate, Date endDate, OUser loggedUser) {
        try {
            ArrayList condition = new ArrayList();
            condition.add("employee.dbid = " + employee.getDbid());
            condition.add("cancelled = 'N'");
            condition.add("startDate >= '" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'");
            if (endDate != null) {
                condition.add("endDate <= '" + dateTimeUtility.DATE_FORMAT.format(endDate) + "'");
            }
            List<EmployeeVacationRequest> employeeVacationRequests = oem.loadEntityList(EmployeeVacationRequest.class.getSimpleName(),
                    condition, null, null, loggedUser);
            for (int i = 0; i < employeeVacationRequests.size(); i++) {
                EmployeeVacationRequest employeeVacationRequest = employeeVacationRequests.get(i);
                entitySetupService.callEntityUpdateAction(employeeVacationRequest, loggedUser);
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    @Override
    public OFunctionResult validateEmployeeWorkingCalHistory(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeWorkingCalHistory calHistory = (EmployeeWorkingCalHistory) odm.getData().get(0);
            ArrayList cond = new ArrayList();
            cond.add("currentEffectiveEndDate is null");
            cond.add("employee.dbid = " + calHistory.getEmployee().getDbid());
            List<EmployeeWorkingCalHistory> employeeWorkingCalHistorys
                    = oem.loadEntityList(EmployeeWorkingCalHistory.class.getSimpleName(), cond, null, null, loggedUser);
            if (employeeWorkingCalHistorys.size() == 1) {
                if (employeeWorkingCalHistorys.get(0).getCurrentEffectiveDate().after(calHistory.getCurrentEffectiveDate())) {
                    ofr.addError(usrMsgService.getUserMessage("Err-1223", loggedUser));
                    return ofr;
                }
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("Internal Error", loggedUser));
        }
        return ofr;
    }

    class OverTime {

        String notApprovedDayOT;
        String notApprovedNightOT;
        String nonPlanned;
        String approvedNightOT;
        String approvedDayOT;
        String dayOTIn;
        String dayOTOut;
        String nightOTIn;
        String nightOTOut;

        public String getNotApprovedDayOT() {
            return notApprovedDayOT;
        }

        public void setNotApprovedDayOT(String notApprovedDayOT) {
            this.notApprovedDayOT = notApprovedDayOT;
        }

        public String getNotApprovedNightOT() {
            return notApprovedNightOT;
        }

        public void setNotApprovedNightOT(String notApprovedNightOT) {
            this.notApprovedNightOT = notApprovedNightOT;
        }

        public String getNonPlanned() {
            return nonPlanned;
        }

        public void setNonPlanned(String nonPlanned) {
            this.nonPlanned = nonPlanned;
        }

        public String getApprovedNightOT() {
            return approvedNightOT;
        }

        public void setApprovedNightOT(String approvedNightOT) {
            this.approvedNightOT = approvedNightOT;
        }

        public String getApprovedDayOT() {
            return approvedDayOT;
        }

        public void setApprovedDayOT(String approvedDayOT) {
            this.approvedDayOT = approvedDayOT;
        }

        public String getDayOTIn() {
            return dayOTIn;
        }

        public void setDayOTIn(String dayOTIn) {
            this.dayOTIn = dayOTIn;
        }

        public String getDayOTOut() {
            return dayOTOut;
        }

        public void setDayOTOut(String dayOTOut) {
            this.dayOTOut = dayOTOut;
        }

        public String getNightOTIn() {
            return nightOTIn;
        }

        public void setNightOTIn(String nightOTIn) {
            this.nightOTIn = nightOTIn;
        }

        public String getNightOTOut() {
            return nightOTOut;
        }

        public void setNightOTOut(String nightOTOut) {
            this.nightOTOut = nightOTOut;
        }
    }

    private OverTime getOverTime(String actualIn, String actualOut, String plannedIn, String plannedOut, /*List<String> requestedFrom,
             List<String> requestedTo,*/
            List<EmployeeOTRequest> otReqs,
            OTSegmentBase otseg, String minOT,
            boolean isChangeAction) {
        OverTime ot = new OverTime();
        String notApprovedDayOT = "00:00";
        String notApprovedNightOT = "00:00";

        String approvedDayOT = "00:00";
        String approvedNightOT = "00:00";

        String nonPlanned = "00:00";

        String dayOTStart = "00:00", dayOTEnd = "00:00", nightOTStart = "00:00", nightOTEnd = "00:00";

        String otFrom = otseg.getFromTime(),
                otTo = otseg.getToTime(),
                type = otseg.getOtType().getCode(),
                maxOT = otseg.getMaxOt();
        boolean needApproval = otseg.isApprovalNeeded();

        boolean outOverTimeCalculated = false;

        if (actualIn != null && !actualIn.equals("") && plannedIn != null && !plannedIn.equals("")) {
            if (dateTimeUtility.compareTime(plannedIn, actualIn) == 1) { // the emp can be in b4 his plannedIn
                // in day period
                if (otFrom != null && !otFrom.equals("") && otTo != null && !otTo.equals("")
                        && type.equalsIgnoreCase("day") /*&& dateTimeUtility.compareTime(actualOut, dayFrom) > 0*/) {
                    String in;
                    if (dateTimeUtility.compareTime(actualOut, plannedIn) < 0) {
                        in = actualOut;
                    } else {
                        in = plannedIn;
                    }

                    List<String> temp = getInterval(actualIn, in, otFrom, otTo);
                    if (temp != null && !temp.isEmpty()) {
                        notApprovedDayOT = dateTimeUtility.addTime(notApprovedDayOT, temp.get(0));

//                        if (notApprovedDayOT.compareTo(minOT) < 0) {
//                            notApprovedDayOT = "00:00";
//                        }
                        dayOTStart = temp.get(1);
                        dayOTEnd = temp.get(2);
                        if (needApproval) {
                            List<String> ots = getRequestedOT(dayOTStart, dayOTEnd, otReqs, notApprovedDayOT, type);
                            approvedDayOT = ots.get(0);
                            notApprovedDayOT = ots.get(1);
                        } else {
                            approvedDayOT = notApprovedDayOT;
                            notApprovedDayOT = dateTimeUtility.subtractTime(notApprovedDayOT, approvedDayOT);
                            //getRequestedOT(dayOTStart, dayOTEnd, requestedFrom, requestedTo, notApprovedDayOT).get(1);
                            //approvedDayOT = getRequestedOT(dayOTStart, dayOTEnd, requestedFrom, requestedTo, notApprovedDayOT).get(0);
                            if (!isChangeAction) {
                                //TO DO : NEED TO BE REVISTED
                                //addApprovedOTRequest(empDADailyDt, dayOTStart, dayOTEnd, pEmployee, user);
                            }

                        }
                    }
                }

                // in night period
                if (type.equalsIgnoreCase("night")
                        && otFrom != null && !otFrom.equals("") && otTo != null && !otTo.equals("") /*&& dateTimeUtility.compareTime(actualOut, nightFrom) > 0*/) {
                    String in;
                    if (dateTimeUtility.compareTime(actualOut, plannedIn) < 0) {
                        in = actualOut;
                    } else {
                        in = plannedIn;
                    }
                    List<String> temp = getInterval(actualIn, in, otFrom, otTo);
                    if (temp != null && temp.isEmpty()) {
                        temp = getInterval(actualIn, plannedIn, dateTimeUtility.addTime(otFrom, "24:00"), dateTimeUtility.addTime(otTo, "24:00"));
                    }
                    if (temp != null && !temp.isEmpty()) {
                        notApprovedNightOT = dateTimeUtility.addTime(notApprovedNightOT, temp.get(0));

//                        if (notApprovedNightOT.compareTo(minOT) < 0) {
//                            notApprovedNightOT = "00:00";
//                        }
                        nightOTStart = temp.get(1);
                        nightOTEnd = temp.get(2);

                        if (needApproval) {
                            List<String> ots = getRequestedOT(nightOTStart, nightOTEnd, otReqs, notApprovedNightOT, type);
                            approvedNightOT = ots.get(0);
                            notApprovedNightOT = ots.get(1);

                        } else {
                            approvedNightOT = notApprovedNightOT;
                            notApprovedNightOT = dateTimeUtility.subtractTime(notApprovedNightOT, approvedNightOT);

                            if (!isChangeAction) {
                                //TO DO : NEED TO BE REVISTED
                                //addApprovedOTRequest(empDADailyDt, nightOTStart, nightOTEnd, pEmployee, user);
                            }
                        }
                    }
                }
            }
        }

        if (!outOverTimeCalculated && actualOut != null && !actualOut.equals("") && plannedOut != null && !plannedOut.equals("")) {
            if (dateTimeUtility.compareTime(actualOut, plannedOut) == 1) {
                String tempDayOT = "00:00";
                String tempNightOT = "00:00";
                if (otFrom != null && !otFrom.equals("") && otTo != null && !otTo.equals("") && type.equalsIgnoreCase("day") /*&& dateTimeUtility.compareTime(actualIn, dayTo) < 0*/) {
                    String in = plannedOut;
                    if (dateTimeUtility.compareTime(plannedOut, actualIn) < 0) {
                        in = actualIn;
                    }

                    List<String> temp = getInterval(in, actualOut, otFrom, otTo);
                    if (temp != null && temp.isEmpty()) {
                        temp = getInterval(in, actualOut, dateTimeUtility.addTime(otFrom, "24:00"), dateTimeUtility.addTime(otTo, "24:00"));
                    }
                    if (temp != null && !temp.isEmpty()) {
                        tempDayOT = temp.get(0);

//                        if (tempDayOT.compareTo(minOT) < 0) {
//                            tempDayOT = "00:00";
//                        }
                        dayOTStart = temp.get(1);
                        dayOTEnd = temp.get(2);

                        if (needApproval) {
                            List<String> ots = getRequestedOT(dayOTStart, dayOTEnd, otReqs, tempDayOT, type);
                            approvedDayOT = ots.get(0);
                            tempDayOT = ots.get(1);
                            notApprovedDayOT = dateTimeUtility.addTime(notApprovedDayOT, tempDayOT);
                        } else {
                            approvedDayOT = tempDayOT;
                            if (!isChangeAction) {
                                //TO DO : NEED TO BE REVISTED
                                //addApprovedOTRequest(empDADailyDt, dayOTStart, dayOTEnd, pEmployee, user);
                            }
                        }

                    }
                }
                if (otFrom != null && !otFrom.equals("") && otTo != null && !otTo.equals("")
                        && type.equalsIgnoreCase("night") /*&& dateTimeUtility.compareTime(actualIn, nightTo) < 0*/) {

                    String in = plannedOut;
                    if (dateTimeUtility.compareTime(plannedOut, actualIn) < 0) {
                        in = actualIn;
                    }

                    List<String> temp = getInterval(in, actualOut, otFrom, otTo);
                    if (temp != null && temp.isEmpty()) {
                        temp = getInterval(in, actualOut, dateTimeUtility.addTime(otFrom, "24:00"), dateTimeUtility.addTime(otTo, "24:00"));
                    }
                    if (temp != null && !temp.isEmpty()) {
                        tempNightOT = temp.get(0);

//                        if (tempNightOT.compareTo(minOT) < 0) {
//                            tempNightOT = "00:00";
//                        }
                        nightOTStart = temp.get(1);
                        nightOTEnd = temp.get(2);

                        if (needApproval) {
                            List<String> ots = getRequestedOT(nightOTStart, nightOTEnd, otReqs, tempNightOT, type);
                            approvedNightOT = ots.get(0);
                            tempNightOT = ots.get(1);
                            notApprovedNightOT = dateTimeUtility.addTime(notApprovedNightOT, tempNightOT);
                        } else {
                            approvedNightOT = tempNightOT;
                            if (!isChangeAction) {
                                //TO DO : NEED TO BE REVISTED
                                //addApprovedOTRequest(empDADailyDt, nightOTStart, nightOTEnd, pEmployee, user);
                            }

                        }
                    }
                }
            }
        }
        if (maxOT != null && !maxOT.equals("") && dateTimeUtility.compareTime(approvedNightOT, maxOT) == 1) {
            approvedNightOT = maxOT;
        }

        if (maxOT != null && !maxOT.equals("") && dateTimeUtility.compareTime(approvedDayOT, maxOT) == 1) {
            approvedDayOT = maxOT;
        }

        ot.setApprovedDayOT(approvedDayOT);
        ot.setApprovedNightOT(approvedNightOT);
        ot.setNotApprovedNightOT(notApprovedNightOT);
        ot.setNotApprovedDayOT(notApprovedDayOT);
        ot.setNonPlanned(nonPlanned);
        ot.setDayOTIn(dayOTStart);
        ot.setDayOTOut(dayOTEnd);
        ot.setNightOTIn(nightOTStart);
        ot.setNightOTOut(nightOTEnd);
        return ot;
    }

    private List<String> getRequestedOT(String otSegStart, String otSegEnd, /*List<String> requestedFrom,
             List<String> requestedTo,*/ List<EmployeeOTRequest> otReqs, String notApprovedOT, String type) {
        List<String> OTs = new ArrayList<String>();
        String approvedOT = "00:00";
        for (int i = 0; i < otReqs.size(); i++) {

            // if (requestedTo != null && !requestedTo.isEmpty()) {
            String reqFrom = otReqs.get(i).getTimeFrom();
            String reqTo = otReqs.get(i).getTimeTo();
            if (dateTimeUtility.compareTime(reqFrom, reqTo) > 0) {
                reqTo = dateTimeUtility.addTime(reqTo, "24:00");
            }
            List<String> requested = getInterval(otSegStart, otSegEnd, reqFrom, reqTo);
            if (requested != null && !requested.isEmpty() && !"00:00".equals(requested.get(0))) {
                if (type.equalsIgnoreCase("day")) {
                    approvedDayOTList.add(requested.get(0));
                    approvedDayOTInList.add(requested.get(1));
                    approvedDayOTOutList.add(requested.get(2));
                } else {
                    approvedNightOTList.add(requested.get(0));
                    approvedNightOTInList.add(requested.get(1));
                    approvedNightOTOutList.add(requested.get(2));
                }
                approvedOT = dateTimeUtility.addTime(approvedOT, requested.get(0));
                if (notApprovedOT.compareTo(requested.get(0)) >= 0) {
                    notApprovedOT = dateTimeUtility.subtractTime(requested.get(0), notApprovedOT);//
                    if (notApprovedOT != null || !notApprovedOT.equals("00:00")) {
                        if (requested.get(1).equals(otSegStart) && requested.get(2).compareTo(otSegEnd) < 0) {

                        }
                    }
                }
            }
            //}

        }
        OTs.add(approvedOT);
        OTs.add(notApprovedOT);
        return OTs;
    }

    private boolean addApprovedOTRequest(Date otDate, String requestedFrom, String requestedTo,
            Employee employee, OUser user) {
        try {
            //create employeeotrequest
            EmployeeOTRequest employeeOTRequest = new EmployeeOTRequest();
            employeeOTRequest.setApproved("Y");
            employeeOTRequest.setTimeFrom(requestedFrom);
            employeeOTRequest.setTimeTo(requestedTo);
            employeeOTRequest.setEmployee(employee);
            employeeOTRequest.setRequestedDate(otDate);
            entitySetupService.callEntityCreateAction(employeeOTRequest, user);
            //oem.saveEntity(employeeOTRequest, user);
        } catch (Exception ex) {
            OLog.logException(ex, user);
            return false;
        }

        return true;
    }

    private boolean insideInterval(String timeBegin, String timeEnd,
            String intervalFrom, String intervalTo) {
        boolean insideInterval = false;
        if (dateTimeUtility.compareTime(timeBegin, intervalFrom) >= 0
                && dateTimeUtility.compareTime(timeBegin, intervalTo) <= 0) {
            if (dateTimeUtility.compareTime(timeEnd, intervalFrom) >= 0
                    && dateTimeUtility.compareTime(timeEnd, intervalTo) <= 0) {
                insideInterval = true;
            }
        }
        return insideInterval;
    }
    //</editor-fold>

    private String unifyTimeValue(String time) {
        String unifiedTime = time;
        if (time != null && !time.isEmpty() && !time.contains(":")) {
            if (time.contains(".")) {
                String[] tempTime = time.split(".");
                unifiedTime = tempTime[0] + ":" + tempTime[1];
            } else {
                String[] tempTime = new String[2];
                tempTime[0] = time.substring(0, 2);
                tempTime[1] = time.substring(2, 4);
                unifiedTime = tempTime[0] + ":" + tempTime[1];
            }
        }
        return unifiedTime;
    }

    @Override
    public Object[] getShift(TMCalendar rotationCal, Date startDate, OUser loggedUser) {

        Object returned[] = new Object[2];

        NormalWorkingCalendar shift = null;

        List<String> shiftConds = new ArrayList<String>();
        shiftConds.add("calendar.dbid = " + rotationCal.getDbid());
        List<NormalWorkingCalendar> shifts = null;

        List<String> sort = new ArrayList<String>();
        sort.add("shiftSequence");

        try {
            shifts = (List<NormalWorkingCalendar>) oem.loadEntityList(
                    NormalWorkingCalendar.class.getSimpleName(), shiftConds, null, sort, loggedUser);
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }

        Date referenceDate = rotationCal.getReferenceDate();

        int result = referenceDate.compareTo(startDate);
        int totalNumDays = 0;

        //DateTime fromDT = new DateTime(referenceDate, dateTimeUtility.dateTimeZone);
        //DateTime toDT = new DateTime(startDate, dateTimeUtility.dateTimeZone);
        //totalNumDays = dateTimeUtility.getDaysBetween(startDate, referenceDate);
        try {
            String sql;
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                sql = "SELECT " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(startDate))
                        + "-" + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(referenceDate))
                        + " from dual";
            } else {
                sql = "SELECT datediff(day,'" + dateTimeUtility.DATE_FORMAT.format(startDate) + "','" + dateTimeUtility.DATE_FORMAT.format(referenceDate) + "')";
            }
            Object obj = oem.executeEntityNativeQuery(sql, loggedUser);
            totalNumDays = new Integer(obj.toString());
            totalNumDays = Math.abs(totalNumDays);
        } catch (Exception ex) {

        }
        //to be reviewd later
        totalNumDays = totalNumDays + 1;
        if (shifts != null && !shifts.isEmpty()) {
            int numShifts = shifts.size();
            int roundDuration = 0; // number of days in each round
            for (int j = 0; j < numShifts; j++) {
                roundDuration += shifts.get(j).getNumberOfDays();
            }

            int remainder = totalNumDays % roundDuration;

            if (remainder == 0) {
                remainder = roundDuration;
            }
            returned[0] = remainder;

            int temp = 0;
            for (int j = 0; j < shifts.size(); j++) {

                temp += shifts.get(j).getNumberOfDays();
                if (remainder <= temp) {
                    shift = shifts.get(j);
                    returned[1] = shift;
                    break;
                }
            }
        }
        return returned;
    }

    public void setCalendarsMap(Map<Integer, NormalWorkingCalendar> rotCalsMap, List<NormalWorkingCalendar> cals, int roundDuration) {
        int count = 0,
                calDays;
        NormalWorkingCalendar cal;

        for (int i = 0; i < cals.size(); i++) {
            cal = cals.get(i);
            calDays = cals.get(i).getNumberOfDays();
            for (int j = 0; j < calDays; j++) {
                rotCalsMap.put(count, cal);
                count++;
            }
        }
    }

    private void distributeOnDays(Date entryDate, Date outDate, String timeIn, String timeOut,
            String dayStart, Employee employee, OUser loggedUser) {
        try {
            Date newEntryDate = entryDate;
            String newTimeIn = timeIn,
                    newTimeOut = dayStart;

            Calendar c = Calendar.getInstance();
            c.setTime(newEntryDate);
            c.add(Calendar.DAY_OF_MONTH, 1);
            newEntryDate = c.getTime();

            do {
                if (newEntryDate.equals(outDate)) {
                    newTimeOut = timeOut;
                }

                List<String> conds = new ArrayList<String>();
                conds.add("employee.dbid = " + employee.getDbid());
                conds.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(newEntryDate) + "'");
                conds.add("header = true");
                EmployeeDailyAttendance empDA = new EmployeeDailyAttendance();
                empDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);

                EmployeeDailyAttendance detailEmpDA = new EmployeeDailyAttendance();
                detailEmpDA.setOutDate(newEntryDate);
                detailEmpDA.setTimeIn(newTimeIn);
                detailEmpDA.setTimeOut(newTimeOut);
                detailEmpDA.setDailyDate(newEntryDate);
                detailEmpDA.setDayNature(empDA.getDayNature());
                detailEmpDA.setEmpDAHeader(empDA);
                detailEmpDA.setEmployee(employee);
                detailEmpDA.setHeader(false);

                entitySetupService.callEntityCreateAction(detailEmpDA, loggedUser);

                // get new timeIn
                newTimeIn = dayStart;

                // add 1 day to entryDate
                c = Calendar.getInstance();
                c.setTime(newEntryDate);
                c.add(Calendar.DAY_OF_MONTH, 1);
                newEntryDate = c.getTime();

            } while (!newEntryDate.after(outDate));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
    }

    /**
     *
     * @param fromTime
     * @param toTime
     * @param startDayTime
     * @param applyConversionToFromTime: to be always set to TRUE, except in the
     * case of exceptions (set it as false)
     * @return
     */
    private List<String> reverseCalendar(String fromTime, String toTime, String startDayTime, boolean applyConversionToFromTime) {
        List<String> newTimes = new ArrayList<String>();
        String newFromTime = fromTime,
                newToTime = toTime;

        if (fromTime != null
                && !fromTime.equals("")
                && fromTime.substring(0, 2).equals("00")) {
            newFromTime = (dateTimeUtility.addTime(startDayTime, "24:00"));
        }
        if (fromTime != null
                && !fromTime.equals("")
                && dateTimeUtility.compareTime(startDayTime, fromTime) > 0
                && applyConversionToFromTime) {
            newFromTime = startDayTime;
        } // new case for only exception from
        else if (fromTime != null
                && !fromTime.equals("")
                && dateTimeUtility.compareTime(startDayTime, fromTime) > 0
                && !applyConversionToFromTime) {
            newFromTime = (dateTimeUtility.addTime(fromTime, "24:00"));
        }

        if (toTime != null
                && !toTime.equals("")
                && dateTimeUtility.compareTime(startDayTime, toTime) >= 0) {
            newToTime = (dateTimeUtility.addTime(toTime, "24:00"));
        }

        /* for the case:
         * if the calendar setup is: from 7->15 and the startDayTime is 7:00
         * and if the actual in & out is 23->7:30
         * then this day was stated as working with delay around 15 hours, which is wrong as it should be absence with no delay.
         * (out of scope case)
         */
        if (toTime != null
                && !toTime.equals("")
                && dateTimeUtility.compareTime(toTime, fromTime) <= 0
                && dateTimeUtility.compareTime(toTime, startDayTime) >= 0) {
            newToTime = (dateTimeUtility.addTime(toTime, "24:00"));
        }

        newTimes.add(newFromTime);
        newTimes.add(newToTime);

        return newTimes;
    }

    private void deleteDAsForReimport(OUser loggedUser) {

        String deleteStat = "delete from employeedailyattendance where dbid in (select employeedailyattendance.dbid from  employeedailyattendance"
                + " join employeedailyattendanceimport on ((employeedailyattendance.employee_dbid in "
                + " (select dbid from oemployee where code = employeedailyattendanceimport.code)) "
                + " and (employeedailyattendance.dailydate = employeedailyattendanceimport.dailydate) "
                + " and employeedailyattendance.header = 0 and employeedailyattendanceimport.done = 0))";
        try {
            oem.executeEntityUpdateNativeQuery(deleteStat, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
    }

    private boolean isOutOfScope(String timeIn, String timeOut, String planIn, String planOut) {

        List<String> workingIntersectionPlanned = getInterval(timeIn, timeOut, planIn, planOut);
        boolean outOfScope = true;
        if (workingIntersectionPlanned != null) {
            if (workingIntersectionPlanned.isEmpty()
                    || (!workingIntersectionPlanned.isEmpty() && workingIntersectionPlanned.get(0).equals("00:00"))) {
                outOfScope = true;
            } else {
                outOfScope = false;
            }
        }

        if (timeIn != null && timeOut != null && timeIn.equals(timeOut)) {
            outOfScope = false;

        }
        return outOfScope;
    }

    private List<TMEntry> getTMEntry(List<EmployeeDailyAttendance> empDAs) {
        List<TMEntry> entries = new ArrayList<TMEntry>();

        TMEntry tempEntry = null;
        for (int i = 0; i < empDAs.size(); i++) {
            tempEntry = new TMEntry();
            tempEntry.setIn(empDAs.get(i).getTimeIn());
            tempEntry.setOut(empDAs.get(i).getTimeOut());

            entries.add(tempEntry);
        }

        // sort entries
        Comparator fieldsComparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                String timeIn1 = ((TMEntry) o1).getIn();
                String timeIn2 = ((TMEntry) o2).getIn();
                return dateTimeUtility.compareTime(timeIn1, timeIn2);
            }
        };
        Collections.sort(entries, fieldsComparator);
        return entries;
    }

    /*
     * Return a list of 2 Strings - 0: day - 1: month
     */
    private List<String> getDayMonthFromFieldName(String fieldName) {
        List<String> dayMonth = new ArrayList<String>();

        String[] temp = fieldName.toLowerCase().split("d");
        String[] temp2 = temp[0].toLowerCase().split("m");

        dayMonth.add(temp[1]);
        dayMonth.add(temp2[1]);

        return dayMonth;
    }

    /* ************** */
    /* Helper Classes */
    /* ************** */
    class EmpCalData {

        String tolerence;
        String planIn;
        String planOut;
        String displayPlanOut;
        String planBreakIn;
        String planBreakOut;
        String startDayTime;

        public String getTolerence() {
            return tolerence;
        }

        public void setTolerence(String tolerence) {
            this.tolerence = tolerence;
        }

        public String getPlanIn() {
            return planIn;
        }

        public void setPlanIn(String planIn) {
            this.planIn = planIn;
        }

        public String getPlanOut() {
            return planOut;
        }

        public void setPlanOut(String planOut) {
            this.planOut = planOut;
        }

        public String getDisplayPlanOut() {
            return displayPlanOut;
        }

        public void setDisplayPlanOut(String displayPlanOut) {
            this.displayPlanOut = displayPlanOut;
        }

        public String getPlanBreakIn() {
            return planBreakIn;
        }

        public void setPlanBreakIn(String planBreakIn) {
            this.planBreakIn = planBreakIn;
        }

        public String getPlanBreakOut() {
            return planBreakOut;
        }

        public void setPlanBreakOut(String planBreakOut) {
            this.planBreakOut = planBreakOut;
        }

        public String getStartDayTime() {
            return startDayTime;
        }

        public void setStartDayTime(String startDayTime) {
            this.startDayTime = startDayTime;
        }

        public EmpCalData(String tolerence, String planIn, String planOut, String displayPlanOut, String planBreakIn, String planBreakOut, String startDayTime) {
            this.tolerence = tolerence;
            this.planIn = planIn;
            this.planOut = planOut;
            this.displayPlanOut = displayPlanOut;
            this.planBreakIn = planBreakIn;
            this.planBreakOut = planBreakOut;
            this.startDayTime = startDayTime;
        }
    };

    class TMEntry {

        String in;
        String out;
        boolean working;

        public String getIn() {
            return in;
        }

        public void setIn(String in) {
            this.in = in;
        }

        public String getOut() {
            return out;
        }

        public void setOut(String out) {
            this.out = out;
        }

        public boolean isWorking() {
            return working;
        }

        public void setWorking(boolean working) {
            this.working = working;
        }
    }
    long dbid = 0;

    public OFunctionResult createDA(List<EmployeeAnnualPlanner> empPlanners, Map<Long, Map<Date, Long>> empRotCals, String plannerYear, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            /*
             * Get initial time
             */
            long startTimeInSec = System.currentTimeMillis(),
                    endTimeInSec = 0;

            Date startDate, endDate;
            Map<String, Long> dayNaturesMap = null;

            Object[] dayNatureObjArr = null;
            List dayNaturesLst = null;
            String selectDayNatures = null;

            Employee employee = null;
            EmployeeAnnualPlanner empPlanner = null;

            Object tempDbid = null;
            try {
                tempDbid = oem.executeEntityNativeQuery("select max(dbid) from employeedailyattendance", loggedUser);
                if (tempDbid == null) {
                    dbid = 1;
                } else {
                    dbid = Long.parseLong(tempDbid.toString());
                }
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            selectDayNatures = "select dbid, type from daynatures ";
            dayNaturesLst = oem.executeEntityListNativeQuery(selectDayNatures, loggedUser);
            if (dayNaturesLst != null) {
                dayNaturesMap = new HashMap<String, Long>();
                for (int i = 0; i < dayNaturesLst.size(); i++) {
                    dayNatureObjArr = (Object[]) dayNaturesLst.get(i);
                    dayNaturesMap.put(dayNatureObjArr[1].toString(), Long.parseLong(dayNatureObjArr[0].toString()));
                }
            }

            endDate = dateTimeUtility.DATE_FORMAT.parse(plannerYear + "-12-31");

            Map<Date, Long> empRotations = null;
            for (int i = 0; i < empPlanners.size(); i++) {
                empRotations = empRotCals.get(empPlanners.get(i).getEmployee().getDbid());
                startDate = dateTimeUtility.DATE_FORMAT.parse(plannerYear + "-1-1");

                employee = empPlanners.get(i).getEmployee();
                empPlanner = empPlanners.get(i);

                if (employee.getHiringDate().compareTo(startDate) > 0) {
                    startDate = employee.getHiringDate();
                }
                dbid++;
//                ofr.append(createSingleEmpDA(empPlanner, empRotations, startDate, endDate, dayNaturesMap, loggedUser));
            }

            endTimeInSec = System.currentTimeMillis();

            System.out.println("**************************************************");
            System.out.println("***** Time for " + empPlanners.size() + " employees is " + ((endTimeInSec - startTimeInSec)) + " m. seconds *****");
            System.out.println("**************************************************");
            ofr.addSuccess(usrMsgService.getUserMessage("DAInsertedSuccessfully", Collections.singletonList(new Integer(empPlanners.size()).toString()), loggedUser));
        } catch (Exception ex) {
            Logger.getLogger(EmpDailyAttendanceBatch.class.getName()).log(Level.SEVERE, null, ex);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    private OFunctionResult createSingleEmpDA(EmployeeAnnualPlanner empPlanner, Map<Date, Long> empRotations,
            Map<String, Long> dayNaturesMap, Date startDate, Date endDate, List<EmployeeWorkingCalHistory> calHistories, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();

        StringBuilder insertDA = null;
        String dayName = null,
                dayValue = null;

        Object valueFromEntity = null;

        int month, day;
        Date tempEndDate;
        Date tempDate;
        Calendar calendar = Calendar.getInstance();

        insertDA = new StringBuilder("insert into employeedailyattendance "
                + "(dbid, deleted, employee_dbid, daynature_dbid, dailydate, header, workingCalendar_dbid)");

        String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
        try {
            Object workingCal_dbid;
            for (EmployeeWorkingCalHistory calHistory : calHistories) {
                tempDate = calHistory.getCurrentEffectiveDate();
                tempEndDate = calHistory.getCurrentEffectiveEndDate();
                if (tempEndDate == null) {
                    tempEndDate = endDate;
                }

                if (tempDate.compareTo(startDate) < 0) {
                    tempDate = startDate;
                }

                calendar.setTime(tempDate);

                workingCal_dbid = 0;
                if (calHistory.getCurrentWorkingCal().getType().getCode().equals("Regular")) {
                    // TODO: to be fixed: get from cal history!!!
                    long tmCal_dbid = calHistory.getCurrentWorkingCal().getDbid();
                    Object obj = oem.executeEntityNativeQuery("select dbid from normalworkingcalendar where"
                            + " deleted = 0 and calendar_dbid = " + tmCal_dbid, loggedUser);
                    workingCal_dbid = obj != null ? Long.parseLong(obj.toString()) : 0;
                }
                int noOfDays = dateTimeUtility.getDaysBetween(tempDate, tempEndDate);
                for (int i = 0; i <= noOfDays; i++) {
                    //while (tempDate.compareTo(tempEndDate) <= 0) {
                    try {
                        day = tempDate.getDate();
                        month = tempDate.getMonth() + 1;

                        dayName = "m" + month + "D" + day;

                        valueFromEntity = BaseEntity.getValueFromEntity(empPlanner, dayName);
                        if (valueFromEntity != null) {
                            dayValue = valueFromEntity.toString();
                        }

                        if (empRotations != null && empRotations.get(tempDate) != null) {
                            workingCal_dbid = empRotations.get(tempDate);
                        }

                        /* if the workingCal_dbid is null, then get it from the cal history
                         * * the solutions:
                         *   ----------------
                         * 1- either load it on the spot from calhistory with respect to the date range
                         * 2- or load all date ranges in a map from the beginning and pass it to this method (or global variable) with
                         * the calendar dbid
                         */
                        insertDA = insertDA.append("select ").append(dbid).append(",").
                                append(0).append(",").
                                append(empPlanner.getEmployee().getDbid()).
                                append(",").append(dayNaturesMap.get(dayValue)).
                                append(",");
                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            insertDA = insertDA.append("To_Date('")
                                    .append(dateTimeUtility.DATE_FORMAT.format(tempDate)).append("'")
                                    .append(",'YYYY-MM-DD')");
                        } else {
                            insertDA = insertDA.append("'").append(dateTimeUtility.DATE_FORMAT.format(tempDate)).append("'");
                        }
                        insertDA = insertDA.append(",").append(1).
                                append(",").append(workingCal_dbid);

                        if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                            insertDA = insertDA.append(" from dual ");
                        }

                        insertDA = insertDA.append(" union ");

                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                        tempDate = calendar.getTime();

                        dbid++;
                    } catch (Exception exception) {
                        ofr.addError(usrMsgService.getUserMessage(exception.getMessage(), loggedUser));
                        OLog.logException(exception, null);
                        OLog.logInformation(empPlanner.getEmployee().getCode());
                    }
                }
            }

            // remove last union at the end of the statement
            insertDA.delete(insertDA.length() - 7, insertDA.length() - 1);

            oem.executeEntityUpdateNativeQuery(insertDA.toString(), loggedUser);
        } catch (Exception exception) {
            ofr.addError(usrMsgService.getUserMessage(exception.getMessage(), loggedUser));
            OLog.logException(exception, null);
            OLog.logInformation(empPlanner.getEmployee().getCode());
        } finally {
            return ofr;
        }
    }

    private List checkCalHistoryCase(Date currentStartDate, Date currentEndDate, Employee emp, long currentDbid, OUser user) {
        List calHistories = null;
        try {
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(user);
            String selectHistory;
            selectHistory = "select 2, currentEffectiveEndDate,\n"
                    + "                 dbid, originalCalendar_dbid, currenteffectivedate, originalCalSeq from employeeworkingcalhistory \n"
                    + "                 where dbid <> " + currentDbid
                    + "                 and cancelled = 0 and employee_dbid =" + emp.getDbid() + "\n"
                    + "                 and deleted = 0\n and";
            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                selectHistory += " currentEffectiveDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(currentStartDate)) + "";
            } else {
                selectHistory += " currentEffectiveDate = '" + dateTimeUtility.DATE_FORMAT.format(currentStartDate) + "'\n";
            }
            calHistories = oem.executeEntityListNativeQuery(selectHistory, user);
            if (calHistories != null && !calHistories.isEmpty()) {
                return calHistories;
            } else {
                if (currentEndDate != null) {
                    selectHistory = "select 3, currentEffectiveEndDate,\n"
                            + "                 dbid, originalCalendar_dbid, currenteffectivedate, originalCalSeq from employeeworkingcalhistory \n"
                            + "                 where dbid <> " + currentDbid
                            + "                 and cancelled = 0 and employee_dbid =" + emp.getDbid() + "\n"
                            + "                 and deleted = 0\n and ";

                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        selectHistory += " currentEffectiveEndDate = " + dateTimeUtility.dateTimeFormatForOracle(
                                dateTimeUtility.DATE_FORMAT.format(currentEndDate));
                    } else {
                        selectHistory += " currentEffectiveEndDate = '" + dateTimeUtility.DATE_FORMAT.format(currentEndDate) + "'\n";
                    }

                    calHistories = oem.executeEntityListNativeQuery(selectHistory, user);
                }
                if (calHistories != null && !calHistories.isEmpty()) {
                    return calHistories;
                } else {
                    selectHistory = "select 1, currentEffectiveEndDate,\n"
                            + "                 dbid, originalCalendar_dbid, currenteffectivedate, originalCalSeq from employeeworkingcalhistory \n"
                            + "                 where dbid <> " + currentDbid
                            + "                 and cancelled = 0 and employee_dbid =" + emp.getDbid() + "\n"
                            + "                 and deleted = 0\n";
                    if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        selectHistory += "    and currentEffectiveDate < " + dateTimeUtility.dateTimeFormatForOracle(
                                dateTimeUtility.DATE_FORMAT.format(currentStartDate)) + "\n"
                                + "                 and (currentEffectiveEndDate >= " + dateTimeUtility.dateTimeFormatForOracle(
                                        dateTimeUtility.DATE_FORMAT.format(currentStartDate)) + "\n"
                                + "                 or currentEffectiveEndDate is null)\n"
                                + "                 order by currentEffectiveDate";
                    } else {
                        selectHistory += "      and currentEffectiveDate < '" + dateTimeUtility.DATE_FORMAT.format(currentStartDate) + "'\n"
                                + "                 and (currentEffectiveEndDate >= '" + dateTimeUtility.DATE_FORMAT.format(currentStartDate) + "'\n"
                                + "                 or currentEffectiveEndDate is null)\n"
                                + "                 order by currentEffectiveDate";
                    }
                }
                calHistories = oem.executeEntityListNativeQuery(selectHistory, user);
                if (calHistories != null && !calHistories.isEmpty()) {
                    return calHistories;
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, user);
        }
        return calHistories;
    }

    private OFunctionResult cutCalHistory(EmployeeWorkingCalHistory calHistory, OUser user) {
        OFunctionResult ofr = new OFunctionResult();

        List calHistories = null;
        Long origHistoryDbid, origCalDbid;
        TMCalendar originalCal = null;
        try {
            // TODO: to be fixed (get calendar from calendar history not from the employee)
            originalCal = calHistory.getEmployee().getCalendar();
            Date currentStartDate = calHistory.getCurrentEffectiveDate(),
                    currentEndDate = calHistory.getCurrentEffectiveEndDate();

            calHistories = checkCalHistoryCase(currentStartDate, currentEndDate, calHistory.getEmployee(), calHistory.getDbid(), user);

            if (calHistories == null) {
                calHistory.setOriginalCalendar(originalCal);
                return ofr;
            } else if (calHistories.isEmpty()) {
                calHistory.setOriginalCalendar(originalCal);
                return ofr;
            }

            Object[] calHistoryArr = (Object[]) calHistories.get(0);
            Long calHistoryCase = calHistoryArr[0] != null ? Long.parseLong(calHistoryArr[0].toString()) : 0;
            Date loadedEndDate = (Date) calHistoryArr[1];
            origHistoryDbid = calHistoryArr[2] != null ? Long.parseLong(calHistoryArr[2].toString()) : 0;
            origCalDbid = calHistoryArr[3] != null ? Long.parseLong(calHistoryArr[3].toString()) : 0;
            Date loadedStartDate = (Date) calHistoryArr[4];
            String origCalSeq = calHistoryArr[5] != null ? calHistoryArr[5].toString() : "0";

            // get the rotation sequence in the case that the new calendar is rotation
            calHistory.setOriginalCalendar(originalCal);
            Integer tempRotSeq = null;
            tempRotSeq = getRotSeq(calHistory, user);
            calHistory.setOriginalCalSeq(tempRotSeq.toString());

            switch (Integer.parseInt(calHistoryCase.toString())) {
                case 1:
                    updateOrigHistoryEndDate(currentStartDate, origHistoryDbid, user);
                    createNewHistoryRecord(calHistory, loadedStartDate, loadedEndDate, tempRotSeq, origCalDbid, originalCal, origHistoryDbid,
                            user);
//                    calHistory = updateNewHistoryOrigCalendarAndSeqForCase1And3(calHistory,
//                            loadedStartDate, origCalSeq, originalCal, user);
                    break;
                case 2:
                    int rotSeq = 0;
                    rotSeq = getOrigHistoryNewSeq(currentEndDate, origHistoryDbid, user);

                    origCalSeq = getOrigHistoryNewOriginalSeq(currentEndDate, origCalSeq, origCalDbid, origHistoryDbid, user);

                    updateOrigHistoryStDateAndSeqAndOrigSeq(currentEndDate, rotSeq, origCalSeq, origHistoryDbid, user);

                    calHistory.setOriginalCalSeq(origCalSeq);

                    List<String> conds = new ArrayList<String>();
                    conds.add("dbid = " + origCalDbid);
                    TMCalendar temp = (TMCalendar) oem.loadEntity("TMCalendar", conds, null, user);

                    calHistory.setOriginalCalendar(temp);
                    break;
                case 3:
                    updateOrigHistoryEndDate(currentStartDate, origHistoryDbid, user);
                    calHistory = updateNewHistoryOrigCalendarAndSeqForCase1And3(calHistory,
                            loadedStartDate, origCalSeq, originalCal, user);
                    break;
                default:
                    // retrieve message
                    ofr.addError(usrMsgService.getUserMessage("CalHistoryError004", user));
                    break;
            }

        } catch (Exception ex) {
            OLog.logException(ex, user);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", user), ex);
        }
        return ofr;
    }

    private void updateOrigHistoryStartDate(Date currentEndDate, Long toBeUpdatedDbid, OUser user) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentEndDate);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            Date newStartDate = calendar.getTime();

            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(user);

            String updateStat = null;
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                updateStat = "update employeeworkingcalhistory "
                        + " set CurrentEffectiveDate = '" + dateTimeUtility.DATE_FORMAT.format(newStartDate) + "'"
                        + " where dbid = " + toBeUpdatedDbid;
            } else {
                updateStat = "update employeeworkingcalhistory "
                        + " set CurrentEffectiveDate = "
                        + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(newStartDate))
                        + " where dbid = " + toBeUpdatedDbid;
            }
            oem.executeEntityUpdateNativeQuery(updateStat, user);
        } catch (Exception ex) {
            OLog.logException(ex, user);
        }
    }

    @Override
    public TMCalendar getEmpCalendar(Date currDate, Employee employee, OUser loggedUser) {
        TMCalendar calendar = null;
        try {
            List<String> conds = new ArrayList<String>();
            conds.add("currentEffectiveDate<='" + dateTimeUtility.DATE_FORMAT.format(currDate) + "'");
            conds.add("currentEffectiveEndDate>='" + dateTimeUtility.DATE_FORMAT.format(currDate) + "'");
            conds.add("employee.dbid=" + employee.getDbid());
            conds.add("cancelled = 'false'");
            EmployeeWorkingCalHistory calHistory = (EmployeeWorkingCalHistory) oem.loadEntity(EmployeeWorkingCalHistory.class.getSimpleName(),
                    conds, null, loggedUser);
            if (calHistory != null) {
                calendar = calHistory.getCurrentWorkingCal();
            } else {
                calendar = employee.getCalendar();
            }
        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return calendar;
    }

    @Override
    public OFunctionResult createEmpCalHistoryOnHire(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        // create history record from startDate to endDate with the def calendar or assigned one
        // cal history record for each employee
        OFunctionResult ofr = new OFunctionResult();
        Employee employee = (Employee) odm.getData().get(0);
        String rotSequence = "1";
        Object[] returnedShft = new Object[2];
        try {
            EmployeeWorkingCalHistory calHistory = new EmployeeWorkingCalHistory();
            calHistory.setEmployee(employee);
            calHistory.setCurrentEffectiveDate(employee.getHiringDate());
            calHistory.setCurrentWorkingCal(employee.getCalendar());
            calHistory.setCancelled(false);
            calHistory.setDone(true);
            if (employee.getCalendar().getType().getCode().equals("Rotation")) {
                returnedShft = getShift(employee.getCalendar(), employee.getHiringDate(), loggedUser);
                if (returnedShft != null && returnedShft.length != 0 && returnedShft[0] != null) {
                    rotSequence = returnedShft[0].toString();
                }
            }
            calHistory.setRotSequence(rotSequence);
            try {
                oem.saveEntity(calHistory, loggedUser);
            } catch (Exception ex) {
                OLog.logError("Error in ** createEmpCalHistoryOnHire **:\n"
                        + "Error encountered in creating Employee Working Calendar History", loggedUser);
                ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    public void cancelHistoryRecord(EmployeeWorkingCalHistory calHistory, OUser user) {

        Date endDate = calHistory.getCurrentEffectiveEndDate();
        Calendar cal = Calendar.getInstance();
        List retLst = null;
        Object[] retArr = null;
        long retDbid;
        String selectStat = "", updateStat = "";
        String dataBaseConnectionString = EntityUtilities.getDBConnectionString(user);

        try {

            if (endDate != null) {
                cal.setTime(endDate);
                cal.add(Calendar.DAY_OF_MONTH, 1);
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    selectStat = "select dbid, currentWorkingCal_dbid from EmployeeWorkingCalHistory "
                            + " where currentEffectiveDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(cal.getTime()))
                            + " and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                    updateStat = "update EmployeeWorkingCalHistory "
                            + " set currentEffectiveDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()));
                } else {
                    selectStat = "select dbid, currentWorkingCal_dbid from EmployeeWorkingCalHistory "
                            + " where currentEffectiveDate = '" + dateTimeUtility.DATE_FORMAT.format(cal.getTime())
                            + "' and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                    updateStat = "update EmployeeWorkingCalHistory "
                            + " set currentEffectiveDate = '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "'";

                }
            } else {
                cal.setTime(calHistory.getCurrentEffectiveDate());
                cal.add(Calendar.DAY_OF_MONTH, -1);
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    selectStat = "select dbid, currentWorkingCal_dbid from EmployeeWorkingCalHistory "
                            + " where currentEffectiveEndDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(cal.getTime()))
                            + " and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                } else {
                    selectStat = "select dbid, currentWorkingCal_dbid from EmployeeWorkingCalHistory "
                            + " where currentEffectiveEndDate = '" + dateTimeUtility.DATE_FORMAT.format(cal.getTime())
                            + "' and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                }
                updateStat = "update EmployeeWorkingCalHistory "
                        + " set currentEffectiveEndDate = NULL";
            }
            retLst = oem.executeEntityListNativeQuery(selectStat, user);
            if (retLst != null) { // the case it was not the last record
                retArr = (Object[]) retLst.get(0);
                retDbid = Long.parseLong(retArr[0].toString());

                updateStat += " where dbid = " + retDbid;
                oem.executeEntityUpdateNativeQuery(updateStat, user);

            }
        } catch (Exception e) {
            OLog.logException(e, user);
        }
    }

    public int getRotSeq(EmployeeWorkingCalHistory calHistory, OUser user) {
//        int rotSeq = 1;
//        Date startDate = calHistory.getCurrentEffectiveDate(), startDate2 = startDate;
//        List retLst = null;
//        Object[] retArr = null;
//        Calendar cal = Calendar.getInstance();
//
//        cal.setTime(calHistory.getCurrentEffectiveDate());
//        cal.add(Calendar.DAY_OF_MONTH, -1);
//        String selectStat;
//        String dataBaseConnectionString = EntityUtilities.getDBConnectionString(user);
//        try {
//            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
//                selectStat = "select rotsequence, currentEffectiveEndDate from EmployeeWorkingCalHistory "
//                        + " where currentEffectiveDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(startDate2))
//                        + " and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
//            } else {
//                selectStat = "select rotsequence, currentEffectiveEndDate from EmployeeWorkingCalHistory "
//                        + " where currentEffectiveDate = '" + dateTimeUtility.DATE_FORMAT.format(startDate2) + "'"
//                        + " and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
//            }
//            retLst = oem.executeEntityListNativeQuery(selectStat, user);
//            if (retLst != null && !retLst.isEmpty()) {
//                retArr = (Object[]) retLst.get(0);
//                if (retArr[0] != null) {
//                    rotSeq = Integer.parseInt(retArr[0].toString());
//                }
//                startDate = (Date) (retArr[1]);
//            } else {
//                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
//
//                    selectStat = "select rotsequence, currentEffectiveDate from EmployeeWorkingCalHistory "
//                            + " where (currentEffectiveEndDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(cal.getTime())) + ") "
//                            + " and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
//                } else {
//                    selectStat = "select rotsequence, currentEffectiveDate from EmployeeWorkingCalHistory "
//                            + " where (currentEffectiveEndDate = '" + dateTimeUtility.DATE_FORMAT.format(cal.getTime()) + "') "
//                            + " and deleted = 0 and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
//                }
//
//                retLst = oem.executeEntityListNativeQuery(selectStat, user);
//
//                if (retLst != null && !retLst.isEmpty()) {
//                    retArr = (Object[]) retLst.get(0);
//                    if (retArr[0] != null) {
//                        rotSeq = Integer.parseInt(retArr[0].toString());
//                    }
//                    startDate = (Date) (retArr[1]);
//
//                }
//            }
//
//            rotSeq += dateTimeUtility.getDaysBetween(startDate2, startDate);
//
//            rotSeq = getFinalSequenceAfterModRoundDuration(rotSeq, calHistory.getOriginalCalendar().getDbid(), user);
//        } catch (Exception e) {
//            OLog.logException(e, user);
//        }
//        return rotSeq;
        int rotSeq = 1, roundDuration = 0;
        Date startDate = calHistory.getCurrentEffectiveDate(), startDate2 = startDate;
        List retLst = null;
        Object[] retArr = null;
        Calendar cal = Calendar.getInstance();

        cal.setTime(calHistory.getCurrentEffectiveDate());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String selectStat;
        String dataBaseConnectionString = EntityUtilities.getDBConnectionString(user);
        if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
            selectStat = "select rotsequence, currentEffectiveDate from EmployeeWorkingCalHistory where currentEffectiveEndDate = '"
                    + dateTimeUtility.DATE_FORMAT.format(cal.getTime())
                    + "' and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
        } else {
            selectStat = "select rotsequence, currentEffectiveDate from EmployeeWorkingCalHistory where currentEffectiveEndDate = "
                    + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(cal.getTime()))
                    + " and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
        }
        try {
            retLst = oem.executeEntityListNativeQuery(selectStat, user);
            if (retLst != null && !retLst.isEmpty()) {
                retArr = (Object[]) retLst.get(0);
                if (retArr[0] != null) {
                    rotSeq = Integer.parseInt(retArr[0].toString());
                }
                startDate = (Date) (retArr[1]);
            } else {
                Object obj;
                if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    selectStat = "select MAX( currentEffectiveDate ) from EmployeeWorkingCalHistory "
                            + "where currentEffectiveEndDate is null AND currentEffectiveDate <= '"
                            + dateTimeUtility.DATE_FORMAT.format(cal.getTime())
                            + "' and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                } else {
                    selectStat = "select MAX( currentEffectiveDate ) from EmployeeWorkingCalHistory "
                            + "where currentEffectiveEndDate is null AND currentEffectiveDate <= "
                            + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(cal.getTime()))
                            + " and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                }
                obj = oem.executeEntityNativeQuery(selectStat, user);
                if (obj != null) {
                    startDate = (Date) (obj);
                }
                if (startDate != null) {
                    if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                        selectStat = "select rotsequence from EmployeeWorkingCalHistory "
                                + "where currentEffectiveEndDate is null AND currentEffectiveDate = '"
                                + dateTimeUtility.DATE_FORMAT.format(startDate)
                                + "' and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                    } else {
                        selectStat = "select rotsequence from EmployeeWorkingCalHistory "
                                + "where currentEffectiveEndDate is null AND currentEffectiveDate = "
                                + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(startDate))
                                + " and cancelled = 0 and employee_dbid = " + calHistory.getEmployee().getDbid();
                    }
                    obj = oem.executeEntityNativeQuery(selectStat, user);
                    if (obj != null) {
                        rotSeq = Integer.parseInt(obj.toString());
                    }
                }
            }
            rotSeq += dateTimeUtility.getDaysBetween(startDate2, startDate) + 1;

            String select = "select SUM(numberOfDays) from normalworkingcalendar where deleted = 0 AND calendar_dbid="
                    + calHistory.getOriginalCalendar().getDbid();
            Object ret = oem.executeEntityNativeQuery(select, user);
            if (ret != null) {
                roundDuration = Integer.parseInt(ret.toString());
            }

            rotSeq = rotSeq % roundDuration;
            if (rotSeq == 0) {
                rotSeq = roundDuration;
            }
        } catch (Exception e) {
            OLog.logException(e, user);
        }
        return rotSeq;
    }

    private int getFinalSequenceAfterModRoundDuration(int sequence, long origCalDbid, OUser user) {
        int rotSeq = sequence;
        int roundDuration = 0;
        try {
            String select = "select SUM(numberOfDays) from normalworkingcalendar where"
                    + " deleted = 0 and calendar_dbid="
                    + origCalDbid;
            Object ret = oem.executeEntityNativeQuery(select, user);
            if (ret != null) {
                roundDuration = Integer.parseInt(ret.toString());
            }

            rotSeq = rotSeq % roundDuration;
            if (rotSeq == 0) {
                rotSeq = roundDuration;
            }
        } catch (Exception e) {
        }
        return rotSeq;
    }

    public NormalWorkingCalendar getShftFromSequence(TMCalendar calendar, int rotSeq, OUser user) {
        try {
            int sum = 0;
            int prevSum = 0;
            List<String> conds = new ArrayList<String>();
            conds.add("calendar.dbid = " + calendar.getDbid());
            List<NormalWorkingCalendar> workingCalendars = (List<NormalWorkingCalendar>) oem.loadEntityList(
                    NormalWorkingCalendar.class.getSimpleName(), conds, null, null, user);
            for (int i = 0; i < workingCalendars.size(); i++) {
                prevSum = sum;
                sum += workingCalendars.get(i).getNumberOfDays();
                if (prevSum <= rotSeq && rotSeq <= sum) {
                    return workingCalendars.get(i);
                }
            }
        } catch (Exception e) {
            OLog.logException(e, user);
        }
        return null;
    }

    @Override
    public OFunctionResult validateCalendarHistoryCases(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeWorkingCalHistory calHistory = (EmployeeWorkingCalHistory) odm.getData().get(0);
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
            String select;
            // case 0 : Check the Overlapping
            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                select = "select count(*) from EmployeeWorkingCalHistory"
                        + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                        + " and deleted = 0 and cancelled <> 1"
                        + " and ( " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()))
                        + " between CURRENTEFFECTIVEDATE  AND currentEffectiveEndDate and currentEffectiveEndDate is not null )";

                if (calHistory.getCurrentEffectiveEndDate() != null) {
                    select += " OR (currenteffectivedate <= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()))
                            + " and currenteffectiveenddate is null)  ";
                }
            } else {
                select = "select count(*) from EmployeeWorkingCalHistory"
                        + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                        + " and deleted = 0 and cancelled <> 1"
                        + " and ( '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "'"
                        + " between CURRENTEFFECTIVEDATE  AND currentEffectiveEndDate and currentEffectiveEndDate is not null )";

            }
            select += " and dbid <> " + calHistory.getDbid();

            Object obj = oem.executeEntityNativeQuery(select, loggedUser);
            if (obj != null && Long.parseLong(obj.toString()) > 0) {
                ofr.addError(usrMsgService.getUserMessage("CalHistoryError005", loggedUser));
                return ofr;
            }

            // case 1 : same start & end dates; so the old record should be cancelled
            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                select = "select count(*) from EmployeeWorkingCalHistory"
                        + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                        + " and deleted = 0 and cancelled <> 1"
                        + " and currenteffectivedate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()));
                if (calHistory.getCurrentEffectiveEndDate() != null) {
                    select += " and currenteffectiveenddate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()));
                } else {
                    select += " and currenteffectiveenddate is null";
                }
            } else {
                select = "select count(*) from EmployeeWorkingCalHistory"
                        + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                        + " and deleted = 0 and cancelled <> 1"
                        + " and currenteffectivedate = '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "'";

                if (calHistory.getCurrentEffectiveEndDate() != null) {
                    select += " and currenteffectiveenddate = '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()) + "'";
                } else {
                    select += " and currenteffectiveenddate is null";
                }
            }
            select += " and dbid <> " + calHistory.getDbid();

            obj = oem.executeEntityNativeQuery(select, loggedUser);
            if (obj != null && Long.parseLong(obj.toString()) > 0) {
                ofr.addError(usrMsgService.getUserMessage("CalHistoryError005", loggedUser));
                return ofr;
            }

            // case 2: the dates intersect with two records, so the new record should be divided on two
            if (calHistory.getCurrentEffectiveEndDate() != null) {
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    select = "select currenteffectivedate from EmployeeWorkingCalHistory"
                            + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                            + " and deleted = 0 and cancelled <> 1"
                            + " and currenteffectivedate <= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()))
                            + " and currenteffectiveenddate >= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()))
                            + " union "
                            + " select currenteffectivedate from EmployeeWorkingCalHistory"
                            + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                            + " and deleted = 0 and cancelled <> 1"
                            + " and currenteffectivedate <= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()))
                            + " and (currenteffectiveenddate >= " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()))
                            + " or currenteffectiveenddate is null )";
                } else {
                    select = "select currenteffectivedate from EmployeeWorkingCalHistory"
                            + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                            + " and deleted = 0 and cancelled <> 1"
                            + " and currenteffectivedate <= '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "'"
                            + " and currenteffectiveenddate >= '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveDate()) + "'"
                            + " union "
                            + " select currenteffectivedate from EmployeeWorkingCalHistory"
                            + " where employee_dbid = " + calHistory.getEmployee().getDbid()
                            + " and deleted = 0 and cancelled <> 1"
                            + " and currenteffectivedate <= '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()) + "'"
                            + " and (currenteffectiveenddate >= '" + dateTimeUtility.DATE_FORMAT.format(calHistory.getCurrentEffectiveEndDate()) + "'"
                            + " or currenteffectiveenddate is null )";
                }
                select += " and dbid <> " + calHistory.getDbid();
                List objs = oem.executeEntityListNativeQuery(select, loggedUser);
                if (objs != null && objs.size() > 1) {
                    List<String> errorParams = new ArrayList<String>();

                    Date tempDate = dateTimeUtility.DATE_FORMAT.parse(objs.get(0).toString());
                    errorParams.add(dateTimeUtility.DATE_FORMAT.format(tempDate));

                    tempDate = dateTimeUtility.DATE_FORMAT.parse(objs.get(1).toString());
                    errorParams.add(dateTimeUtility.DATE_FORMAT.format(tempDate));

                    ofr.addError(usrMsgService.getUserMessage("CalHistoryError006", errorParams, loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);

        }
        return ofr;
    }

    @Override
    public OFunctionResult workingCalendarUpdateAll(ODataMessage odm, OFunctionParms params, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();
        List<EmployeeWorkingCalHistory> employeeWorkingCalHistory;
        ODataMessage dataMessage;
        ODataType dataType;

        List<String> condition;
        List newData;

        try {
            condition = new ArrayList<String>();
            condition
                    .add("name='" + EmployeeWorkingCalHistory.class
                            .getSimpleName() + "'");

            dataType = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(),
                    condition, null, loggedUser);

            condition.clear();

            List<String> employeeDbidList = dataSecurityServiceLocal.getEmployeeSecurityDbids(loggedUser);
            if (!employeeDbidList.isEmpty()) {
                String employeeDbids = employeeDbidList.toString().replace("[", "(").replace("]", ")");
                String securityCond = "employee.dbid in " + employeeDbids;
                condition.add(securityCond);
            }
            condition.add(
                    "done = false");
            employeeWorkingCalHistory = oem.loadEntityList(EmployeeWorkingCalHistory.class.getSimpleName(),
                    condition, null, null, loggedUser);

            newData = new ArrayList();
            if (employeeWorkingCalHistory
                    != null) {
                for (int i = 0; i < employeeWorkingCalHistory.size(); i++) {

                    newData.add(employeeWorkingCalHistory.get(i));
                    dataMessage = new ODataMessage(dataType, newData);
                    workingCalendarUpdate(dataMessage, params, loggedUser);
                    newData.clear();

                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    private int getOrigHistoryNewSeq(Date newStart, Long toBeUpdatedDbid, OUser user) {
        int tempRotSeq;
        try {
            String selectStat = "select CurrentEffectiveDate, rotsequence from employeeworkingcalhistory "
                    + " where deleted = 0 and dbid = " + toBeUpdatedDbid;

            List resultLst = oem.executeEntityListNativeQuery(selectStat, user);
            Object[] resultArr = (Object[]) resultLst.get(0);
            Date startDate = (Date) (resultArr[0]);
            tempRotSeq = Integer.parseInt(resultArr[1].toString());
            tempRotSeq += (dateTimeUtility.getDaysBetween(startDate, newStart) + 1);
        } catch (Exception ex) {
            tempRotSeq = 0;
            OLog.logException(ex, user);
        }
        return tempRotSeq;
    }

    private void updateOrigHistoryStDateAndSeqAndOrigSeq(Date currentEndDate, int rotSeq, String origSeq, Long toBeUpdatedDbid, OUser user) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentEndDate);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            Date newStartDate = calendar.getTime();
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(user);
            String updateStat;
            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                updateStat = "update employeeworkingcalhistory "
                        + " set CurrentEffectiveDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(newStartDate))
                        + " , rotSequence = '" + rotSeq + "',"
                        + " originalCalSeq = '" + origSeq + "'"
                        + " where dbid = " + toBeUpdatedDbid;
            } else {

                updateStat = "update employeeworkingcalhistory "
                        + " set CurrentEffectiveDate = '" + dateTimeUtility.DATE_FORMAT.format(newStartDate) + "'"
                        + " , rotSequence = '" + rotSeq + "',"
                        + " originalCalSeq = '" + origSeq + "'"
                        + " where dbid = " + toBeUpdatedDbid;
            }
            oem.executeEntityUpdateNativeQuery(updateStat, user);
        } catch (Exception ex) {
            OLog.logException(ex, user);
        }
    }

    private void updateOrigHistoryEndDate(Date currentStartDate, Long toBeUpdatedDbid, OUser user) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentStartDate);
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            Date newEndDate = calendar.getTime();
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(user);
            String updateStat;

            if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                updateStat = "update employeeworkingcalhistory "
                        + " set CurrentEffectiveEndDate = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(newEndDate))
                        + " where dbid = " + toBeUpdatedDbid;
            } else {

                updateStat = "update employeeworkingcalhistory "
                        + " set CurrentEffectiveEndDate = '" + dateTimeUtility.DATE_FORMAT.format(newEndDate) + "'"
                        + " where dbid = " + toBeUpdatedDbid;
            }
            oem.executeEntityUpdateNativeQuery(updateStat, user);
        } catch (Exception ex) {
            OLog.logException(ex, user);
        }
    }

    private void createNewHistoryRecord(EmployeeWorkingCalHistory calHistory, Date loadedStartDate, Date loadedEndDate,
            int tempRotSeq, Long origCalDbid, TMCalendar origCal, Long origCalHistoryDbid, OUser user) {
        try {
            Date currentEndDate = calHistory.getCurrentEffectiveEndDate();
            Date currentStartDate = calHistory.getCurrentEffectiveDate();

            if (currentEndDate != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentEndDate);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                Date newStartDate = calendar.getTime();

                String originalRecOrigSeq = null;

                Integer rotSeq = new Integer(1),
                        origRotSeq = new Integer(1);

                rotSeq = tempRotSeq + dateTimeUtility.getDaysBetween(currentStartDate, newStartDate) + 1;

                if (originalRecOrigSeq == null) {
                    originalRecOrigSeq = "0";
                }

                String select = "select SUM(numberOfDays) from normalworkingcalendar where deleted = 0 AND calendar_dbid="
                        + origCal.getDbid();
                Object ret = oem.executeEntityNativeQuery(select, user);
                int roundDuration = 0;
                if (ret != null) {
                    roundDuration = Integer.parseInt(ret.toString());
                }

                rotSeq = rotSeq % roundDuration;
                if (rotSeq == 0) {
                    rotSeq = roundDuration;
                }

                EmployeeWorkingCalHistory calHistory2 = new EmployeeWorkingCalHistory();
                calHistory2.setEmployee(calHistory.getEmployee());
                calHistory2.setCurrentEffectiveDate(newStartDate);
                calHistory2.setCurrentEffectiveEndDate(loadedEndDate);
                calHistory2.setCurrentWorkingCal(origCal);
                calHistory2.setCancelled(false);
                calHistory2.setRotSequence(rotSeq.toString());
                // the original cal for the divided record is the original calendar of the parent updated record
                // from which this record was generated
                if (origCalDbid != null) {
                    select = "select SUM(numberOfDays) from normalworkingcalendar where deleted = 0 AND calendar_dbid="
                            + origCalDbid;
                    ret = oem.executeEntityNativeQuery(select, user);
                    if (ret != null) {
                        roundDuration = Integer.parseInt(ret.toString());
                    }
                    select = "select rotSequence from EmployeeWorkingCalHistory where dbid = " + origCalHistoryDbid;
                    ret = oem.executeEntityNativeQuery(select, user);
                    if (ret != null) {
                        originalRecOrigSeq = ret.toString();
                    }
                    origRotSeq = Integer.parseInt(originalRecOrigSeq)
                            + dateTimeUtility.getDaysBetween(loadedStartDate, newStartDate) + 1;

                    origRotSeq %= roundDuration;
                    if (origRotSeq == 0) {
                        origRotSeq = roundDuration;
                    }

                    calHistory2.setOriginalCalSeq(origRotSeq.toString());
                    calHistory2
                            .setOriginalCalendar(
                                    (TMCalendar) oem.loadEntity(TMCalendar.class
                                            .getSimpleName(), origCalDbid, null, null, user));
                    calHistory2.setDone(
                            true);
                }
                //oem.saveEntity(calHistory2, user);
                entitySetupService.callEntityUpdateAction(calHistory2, user);
            }
        } catch (Exception ex) {
            OLog.logException(ex, user);
        }
    }

    @Override
    public OFunctionResult postSdkScheduledServerJob(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {

        OFunctionResult ofr = new OFunctionResult();

        List where = new ArrayList();
        where.add("done = false");
        where.add("fromTimeMachine = 'Y'");

        ofr.append(postEmployeeDailyAttendanceImport(where, loggedUser));

        return ofr;
    }

    @Override
    public OFunctionResult fixEmployeeTMEntry(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDailyAttendanceImport request = (EmployeeDailyAttendanceImport) odm.getData().get(0);
            if (request != null) {
                if (request.getCalcPeriod() != null) {
                    String SQL;
                    SQL = "exec fixEmployeeTMEntry @CP = " + request.getCalcPeriod().getDbid();
                    boolean executed = oem.executeEntityUpdateNativeQuery(SQL, loggedUser);
                    if (executed) {
                        ofr.addSuccess(usrMsgService.getUserMessage("FETME00001", loggedUser));
                    } else {
                        ofr.addError(usrMsgService.getUserMessage("FETME00002", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult reSetEmployeeTMImport(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDailyAttendanceImport request = (EmployeeDailyAttendanceImport) odm.getData().get(0);
            if (request != null) {

                DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                Connection connection = datasource.getConnection();
                String procedure = "{call reSetEmployeeTMImport(?,?)}";
                CallableStatement callableStatement = connection.prepareCall(procedure);
                callableStatement.setLong(1, request.getDbid());
                callableStatement.setString(2, "N");
                int res = callableStatement.executeUpdate();
                connection.close();
                if (res > 0) {
                    ofr.addSuccess(usrMsgService.getUserMessage("RESETTMI00001", loggedUser));
                } else if (res == 0) {
                    ofr.addWarning(usrMsgService.getUserMessage("No data updated", loggedUser));
                } else {
                    ofr.addError(usrMsgService.getUserMessage("RESETTMI00002", loggedUser));
                }

            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult reSetEmployeeTMImportFromMachine(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDailyAttendanceImport request = (EmployeeDailyAttendanceImport) odm.getData().get(0);
            if (request != null) {
                if (request.getCalcPeriod() != null) {
                    DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                    Connection connection = datasource.getConnection();
                    String procedure = "{call reSetEmployeeTMImport(?,?)}";
                    CallableStatement callableStatement = connection.prepareCall(procedure);
                    callableStatement.setLong(1, request.getCalcPeriod().getDbid());
                    callableStatement.setString(2, "Y");
                    int res = callableStatement.executeUpdate();
                    connection.close();
                    if (res > 0) {
                        ofr.addSuccess(usrMsgService.getUserMessage("RESETTMI00001", loggedUser));
                    } else if (res == 0) {
                        ofr.addWarning(usrMsgService.getUserMessage("No data updated", loggedUser));
                    } else {
                        ofr.addError(usrMsgService.getUserMessage("RESETTMI00002", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult postConsolidationForSingleEmployee(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeMonthlyConsolidation request = (EmployeeMonthlyConsolidation) odm.getData().get(0);
            if (request != null) {
                if (request.getCalculatedPeriod() != null) {
                    DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                    Connection connection = datasource.getConnection();
                    String procedure = "{call postConsolForSingEmployee(?,?)}";
                    CallableStatement callableStatement = connection.prepareCall(procedure);
                    callableStatement.setLong(1, request.getCalculatedPeriod().getDbid());
                    callableStatement.setLong(2, request.getEmployee().getDbid());
                    int res = callableStatement.executeUpdate();
                    connection.close();
                    if (res > 0) {
                        ofr.addSuccess(usrMsgService.getUserMessage("RESETTMI00001", loggedUser));
                    } else if (res == 0) {
                        ofr.addWarning(usrMsgService.getUserMessage("No data updated", loggedUser));
                    } else {
                        ofr.addError(usrMsgService.getUserMessage("RESETTMI00002", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult postConsolidation(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeMonthlyConsolidation request = (EmployeeMonthlyConsolidation) odm.getData().get(0);
            if (request != null) {
                if (request.getCalculatedPeriod() != null) {
                    DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                    Connection connection = datasource.getConnection();
                    String procedure = "{call postConsolidation(?)}";
                    CallableStatement callableStatement = connection.prepareCall(procedure);
                    callableStatement.setLong(1, request.getCalculatedPeriod().getDbid());
                    int res = callableStatement.executeUpdate();
                    connection.close();
                    if (res > 0) {
                        ofr.addSuccess(usrMsgService.getUserMessage("RESETTMI00001", loggedUser));
                    } else if (res == 0) {
                        ofr.addWarning(usrMsgService.getUserMessage("No data updated", loggedUser));
                    } else {
                        ofr.addError(usrMsgService.getUserMessage("RESETTMI00002", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult reSetEmployeeTMImportFromMachineEmp(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDailyAttendanceImport request = (EmployeeDailyAttendanceImport) odm.getData().get(0);
            if (request != null) {
                if (request.getCalcPeriod() != null) {
                    DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                    Connection connection = datasource.getConnection();
                    String procedure = "{call reSetEmployeeTMImportEmp(?,?,?)}";
                    CallableStatement callableStatement = connection.prepareCall(procedure);
                    callableStatement.setLong(1, request.getCalcPeriod().getDbid());
                    callableStatement.setString(2, "Y");
                    callableStatement.setLong(3, request.getEmployee().getDbid());

                    int res = callableStatement.executeUpdate();
                    connection.close();
                    if (res > 0) {
                        ofr.addSuccess(usrMsgService.getUserMessage("RESETTMI00001", loggedUser));
                    } else if (res == 0) {
                        ofr.addWarning(usrMsgService.getUserMessage("No data updated", loggedUser));
                    } else {
                        ofr.addError(usrMsgService.getUserMessage("RESETTMI00002", loggedUser));
                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    @Override
    public OFunctionResult reSetEmployeeTMImportEmp(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeDailyAttendanceImport request = (EmployeeDailyAttendanceImport) odm.getData().get(0);
            if (request != null) {
                DataSource datasource = ((DataSource) new InitialContext().lookup("jdbc/" + loggedUser.getTenant().getPersistenceUnitName()));
                Connection connection = datasource.getConnection();
                String procedure = "{call reSetEmployeeTMImportEmp(?,?,?)}";
                CallableStatement callableStatement = connection.prepareCall(procedure);
                callableStatement.setLong(1, request.getDbid());
                callableStatement.setString(2, "N");
                callableStatement.setString(3, request.getCode());

                int res = callableStatement.executeUpdate();
                connection.close();
                if (res > 0) {
                    ofr.addSuccess(usrMsgService.getUserMessage("RESETTMI00001", loggedUser));
                } else if (res == 0) {
                    ofr.addWarning(usrMsgService.getUserMessage("No data updated", loggedUser));
                } else {
                    ofr.addError(usrMsgService.getUserMessage("RESETTMI00002", loggedUser));
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        }
        return ofr;
    }

    private String distributeOnDaysSingleEntry(Date entryDate, Date outDate, String timeIn, String timeOut,
            String dayStart, Employee employee, TMCalendar tmCalendar, OUser loggedUser) {
        boolean returnValue = false;
        String outReturn = "";
        try {
            NormalWorkingCalendar calendar;
            Date newEntryDate = entryDate;
            String newTimeIn = timeIn,
                    newTimeOut = dayStart;
            Calendar c = Calendar.getInstance();
            c.setTime(newEntryDate);
            Calendar calendarTemp = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, 1);
            newEntryDate = c.getTime();
            String startDayStr;
            String endDayStr;
            String tempDateStr;
            Date tempDate;
            Date endDate;
            startDayStr = dateTimeUtility.DATE_FORMAT.format(entryDate);
            //_________________
            int counter = 0;
            EmployeeDailyAttendance empDA;
            Date actualEndDate;
            while (!newEntryDate.after(outDate)) {
                actualEndDate = null;
                tmCalendar = getEmpCalendar(newEntryDate, employee, loggedUser);
                calendar = getEmployeeNormalCalendar(employee, tmCalendar, newEntryDate, loggedUser);
                if (calendar == null) {
                    if (counter == 0) {
                        returnValue = false;
                    } else {
                        returnValue = true;
                    }
                    //return returnValue;
                    return outReturn;
                }
                tempDateStr = dateTimeUtility.DATE_FORMAT.format(newEntryDate);
                tempDateStr = tempDateStr + " " + calendar.getStartDayTime();

                //Next Day Start
                tempDate = dateTimeUtility.DATETIME_FOTMAT.parse(tempDateStr);

                endDayStr = dateTimeUtility.DATE_FORMAT.format(outDate);
                endDayStr = endDayStr + " " + timeOut;
                endDate = dateTimeUtility.DATETIME_FOTMAT.parse(endDayStr);

                if (tempDate.before(endDate)
                        || tempDate.equals(endDate)) {
                    newTimeIn = calendar.getStartDayTime();

                    if (newEntryDate.equals(outDate)) {
                        newTimeOut = timeOut;
                    } else {
                        actualEndDate = newEntryDate;
                        calendarTemp.setTime(actualEndDate);
                        calendarTemp.add(Calendar.DAY_OF_MONTH, 1);
                        actualEndDate = calendarTemp.getTime();
                        actualEndDate = dateTimeUtility.DATETIME_FOTMAT.parse(dateTimeUtility.DATE_FORMAT.format(actualEndDate) + " " + calendar.getStartDayTime());
                        if (actualEndDate.after(endDate)) {
                            newTimeOut = timeOut;
                        } else {
                            newTimeOut = calendar.getStartDayTime();
                        }
                    }
                    if (counter == 0) {
                        outReturn = newTimeIn;
                    }

                    List<String> conds = new ArrayList<String>();
                    conds.add("employee.dbid = " + employee.getDbid());
                    conds.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(newEntryDate) + "'");
                    conds.add("header = true");
                    empDA
                            = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class
                                    .getSimpleName(), conds, null, loggedUser);

                    if (empDA
                            != null) {
                        empDA.setOutDate(newEntryDate);
                        empDA.setTimeIn(newTimeIn);
                        empDA.setTimeOut(newTimeOut);
                        empDA.setDailyDate(newEntryDate);
                        empDA.setDayNature(empDA.getDayNature());
                        empDA.setEmployee(employee);
                        empDA.setHeader(true);

                        ODataType dt = (ODataType) oem.loadEntity(ODataType.class.getSimpleName(),
                                Collections.singletonList("name = 'EmployeeDailyAttendance'"), null, loggedUser);
                        List data = new ArrayList();
                        data.add(empDA);
                        ODataMessage odm = new ODataMessage(dt, data);
                        entitySetupService.callEntityUpdateAction(empDA, loggedUser);
                        returnValue = true;
                    }
                }
                c.add(Calendar.DAY_OF_MONTH, 1);
                newEntryDate = c.getTime();
                counter++;
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        //return returnValue;
        return outReturn;
    }

//    private NormalWorkingCalendar getEmployeeNormalCalendar(Employee employee, TMCalendar tmCalendar,
    public NormalWorkingCalendar getEmployeeNormalCalendar(Employee employee, TMCalendar tmCalendar,
            Date currDate, OUser loggedUser) {
        NormalWorkingCalendar normalCal = null;
        try {
            UDC calType;
            calType = tmCalendar.getType();
            EmployeeShift employeeShift;
            List<String> conds = new ArrayList<String>();
            if (calType.getCode().equals("Regular") || calType.getCode().equals("Flexible")) {
                conds.clear();
                conds.add("calendar.dbid = " + tmCalendar.getDbid());
                normalCal
                        = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class
                                .getSimpleName(), conds, null, loggedUser);
            }

            if (calType.getCode().equals("VariableShift")
                    || calType.getCode().equals("Rotation")) {
                String currentYear = dateTimeUtility.YEAR_FORMAT.format(currDate);
                String currentMonth = dateTimeUtility.MONTH_FORMAT.format(currDate);
                String currentDay = dateTimeUtility.DAY_FORMAT.format(currDate);

                conds.clear();
                conds.add("employee.dbid = " + employee.getDbid());
                conds.add("plannerYear = '" + currentYear.toString() + "'");

                employeeShift = (EmployeeShift) oem.loadEntity("EmployeeShift", conds, null, loggedUser);

                if (employeeShift != null) {
                    Object dayValue = BaseEntity.getValueFromEntity(employeeShift, "m" + Integer.parseInt(currentMonth) + "D" + Integer.parseInt(currentDay));
                    String dayShift = dayValue.toString();

                    conds.clear();
                    conds.add("calendar.dbid = " + tmCalendar.getDbid());
                    conds.add("code = '" + dayShift + "'");
                    normalCal
                            = (NormalWorkingCalendar) oem.loadEntity(
                                    NormalWorkingCalendar.class
                                    .getSimpleName(), conds, null, loggedUser);
                }
            }

        } catch (Exception e) {
            OLog.logException(e, loggedUser);
        }
        return normalCal;
    }

//    private String[] getFromPayrollParameter(String description, OUser loggedUser) {
    public String[] getFromPayrollParameter(String description, OUser loggedUser) {
        String[] result = null;
        try {
            String SQL;
            SQL = "Select valueData,auxiliary From PayrollParameter where company_dbid = "
                    + loggedUser.getUserPrefrence1() + " AND description = '" + description + "'";
            Object[] obj = (Object[]) oem.executeEntityNativeQuery(SQL, loggedUser);

            if (obj != null) {
                result = new String[2];
                result[0] = obj[0].toString();
                result[1] = obj[1].toString();
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return result;
    }

    private boolean checkOverTimeEntitlement(Long employeeDbid, OUser loggedUser) {
        boolean entitled = false;
        try {
            String SQL;
            SQL = "Select tmEntitled From employee payroll where employee_dbid = " + employeeDbid.toString();
            Object obj = oem.executeEntityNativeQuery(SQL, loggedUser);
            if (obj != null) {
                if (obj.toString().equalsIgnoreCase("y")) {
                    entitled = true;
                }
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return entitled;
    }

    @Override
    public OFunctionResult getPlannedInAndPlannedOut(ODataMessage odm,
            OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            EmployeeVacationRequest vacRequest = (EmployeeVacationRequest) odm.getData().get(0);
            if (vacRequest != null && vacRequest.getStartDate() != null) {
                if (vacRequest.getVacation().getUnitOfMeasure().equals("H")) {
                    Employee employee = vacRequest.getEmployee();
                    TMCalendar calendar = getEmpCalendar(vacRequest.getStartDate(), employee, loggedUser);
                    NormalWorkingCalendar normalCalendar = null;
                    String timeIn = "";
                    DateFormat df = new SimpleDateFormat("HH:mm");
                    List<String> conds = new ArrayList<String>();

                    if (calendar.getType().getCode().equals("Regular")) {
                        conds.add("calendar.dbid = " + calendar.getDbid());
                        normalCalendar
                                = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class
                                        .getSimpleName(),
                                        conds, null, loggedUser);
                    } else if (calendar.getType().getCode().equals("VariableShift")) {
                        conds.clear();
                        conds.add("employee.dbid = " + employee.getDbid());
                        conds.add("plannerYear = '" + dateTimeUtility.YEAR_FORMAT.format(vacRequest.getStartDate()) + "'");
                        EmployeeShift empShift = (EmployeeShift) oem.loadEntity(EmployeeShift.class
                                .getSimpleName(), conds, null, loggedUser);

                        if (BaseEntity.getValueFromEntity(empShift,
                                "m" + Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(vacRequest.getStartDate())) + "D" + Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(vacRequest.getStartDate()))) == null) {
                            return ofr;
                        }

                        String dayShift = BaseEntity.getValueFromEntity(empShift, "m" + Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(vacRequest.getStartDate())) + "D" + Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(vacRequest.getStartDate()))).toString();

                        conds.clear();

                        conds.add(
                                "calendar.dbid = " + calendar.getDbid());
                        conds.add(
                                "code = '" + dayShift + "'");
                        normalCalendar = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(),
                                conds, null, loggedUser);
                    } else { // case of rotation
                        Object[] returned = getShift(calendar, vacRequest.getStartDate(), loggedUser);
                        normalCalendar = (NormalWorkingCalendar) returned[1];
                    }

                    if (normalCalendar == null) {
                        return ofr;
                    }

                    timeIn = normalCalendar.getTimeIn();
                    if (!normalCalendar.isIgnoreTolerence() && normalCalendar.getTolerence() != null) {
                        timeIn = dateTimeUtility.addTime(timeIn, dateTimeUtility.convertMinutesToTime(normalCalendar.getTolerence().doubleValue()));
                    }

                    String exceptionFrom = df.format(vacRequest.getStartDate());
//                    if (dateTimeUtility.compareTime(exceptionFrom, timeIn) < 0) {
//                        ofr.addError(usrMsgService.getUserMessage("ExcuseInLessThanTolIn", loggedUser)); // stop on error
//                    }
                }
            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser), ex);
        } finally {
            return ofr;
        }
    }

    private OFunctionResult applyVacationRequestOnTM(EmployeeVacationRequest vacationRequest, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            Employee employee = vacationRequest.getEmployee();
            Vacation vacation = vacationRequest.getVacation();
            boolean isRemoveHoliday = vacation.getRemoveHoliday().equalsIgnoreCase("Y");
            boolean isRemoveOffday = vacation.getWorkingCalender().equalsIgnoreCase("Y");

            DateTime startDT = new DateTime(vacationRequest.getStartDate());

            Integer currentYear = startDT.getYear();

            List<String> vacationMethods = getEmployeeVacations(vacationRequest.getStartDate(), vacationRequest.getEndDate(), currentYear);

            boolean isMission = false;
            // loading day natures; whether vacation, mission, absence, holiday & week end
            List<String> udcConditions = new ArrayList<String>();
            UDC vacationDayNature = null;
            udcConditions.add("type.code='900'");
            if (vacationRequest.getVacation().isMission()) {
                isMission = true;
            }

            // case of mission days and the day is neither holiday nor offday
            if (isMission) {
                if (!vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                    udcConditions.add("code='Mission'");
                } else {
                    udcConditions.add("code='Absence'");
                }
            } else {
                udcConditions.add("code='Vacation'");
            }

            /*
             if (!vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
             udcConditions.add("code='Vacation'");
             } else {
             payrollParameterDsc = "MissionVacationsDayNatureEnabled";
             payrollParameterResult = getFromPayrollParameter(payrollParameterDsc, loggedUser);
             if (payrollParameterResult[0] != null
             && payrollParameterResult[0].equalsIgnoreCase("y")) {
             udcConditions.add("code='Mission'");
             isMission = true;
             } else {
             udcConditions.add("code='Absence'");
             }
             }
             */
            try {
                vacationDayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), udcConditions, null, loggedUser);
            } catch (Exception ex) {
                Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
            }

            // check that if in the case of mission entry, the mission should affect the current date in daily attendance or the day before
            // this is done by checking that the mission's start time is before the start time of the vacation's day calendar
            Date missionDay = vacationRequest.getStartDate();
            if (isMission && vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                TMCalendar tmCalendar = calendarSer.getEmpCalendar(vacationRequest.getStartDate(), employee, loggedUser);
                NormalWorkingCalendar workingCal = calendarSer.getEmployeeNormalCalendar(employee, tmCalendar, vacationRequest.getStartDate(), loggedUser);
                if (workingCal != null) {
                    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
                    if (dateTimeUtility.compareTime(timeFormat.format(vacationRequest.getStartDate()), workingCal.getStartDayTime()) < 0) {
                        missionDay = dateTimeUtility.addValueToDate(vacationRequest.getStartDate(), -1, DateTimeUtility.DateFieldType.DAY);
                    }
                }
            }
            if (isMission) {
                while (missionDay.compareTo(vacationRequest.getEndDate()) <= 0) {
                    vacationMethods.clear();
                    vacationMethods.add("m" + Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(missionDay))
                            + "D" + Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(missionDay)));

                    //<editor-fold defaultstate="collapsed" desc="insert or update records in daily attendance with vacation nature">
                    int month = Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(missionDay));
                    int day = Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(missionDay));
                    String currentDateStr = currentYear + "-" + month + "-" + day;
                    Date currentDate = null;
                    try {
                        currentDate = dateTimeUtility.DATE_FORMAT.parse(currentDateStr);
                    } catch (ParseException ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    String exceptionFrom = null, exceptionTo = null;
                    if (vacationRequest.getVacation().getUnitOfMeasure().equals("H")) {
                        DateFormat df = new SimpleDateFormat("HH:mm");
                        exceptionFrom = df.format(vacationRequest.getStartDate());
                        exceptionTo = df.format(vacationRequest.getEndDate());
                    }

                    List<String> empDAConditions = new ArrayList<String>();
                    EmployeeDailyAttendance loadedEmpDailyAttendance = null;
                    empDAConditions.add("employee.dbid = " + employee.getDbid());
                    empDAConditions.add("dailyDate = '" + dateTimeUtility.DATE_FORMAT.format(currentDate) + "'");
                    empDAConditions.add("header = true");
                    try {
                        loadedEmpDailyAttendance = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), empDAConditions, null, loggedUser);
                    } catch (Exception ex) {
                        Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (loadedEmpDailyAttendance != null) {

                        // set day nature and exception data for this day
                        if (!isRemoveHoliday && (loadedEmpDailyAttendance.getDayNature().getCode().equals("Holiday")
                                || loadedEmpDailyAttendance.getDayNature().getCode().equals("WorkingOnHoliday"))) {
                            loadedEmpDailyAttendance.setDayNature(vacationDayNature);
                        } else if (!isRemoveOffday && (loadedEmpDailyAttendance.getDayNature().getCode().equals("WeekEnd")
                                || loadedEmpDailyAttendance.getDayNature().getCode().equals("WorkingOnWeekEnd"))) { // remains as offday
                            loadedEmpDailyAttendance.setDayNature(vacationDayNature);
                        } else if (!loadedEmpDailyAttendance.getDayNature().getCode().equals("WeekEnd")
                                && !loadedEmpDailyAttendance.getDayNature().getCode().equals("WorkingOnWeekEnd")
                                && !loadedEmpDailyAttendance.getDayNature().getCode().equals("Holiday")
                                && !loadedEmpDailyAttendance.getDayNature().getCode().equals("WorkingOnHoliday")) {
                            loadedEmpDailyAttendance.setDayNature(vacationDayNature);
                        }
                        loadedEmpDailyAttendance.setExceptionFrom(exceptionFrom);
                        loadedEmpDailyAttendance.setExceptionTo(exceptionTo);
                        entitySetupService.callEntityUpdateAction(loadedEmpDailyAttendance, loggedUser);
                    }
                    //</editor-fold>
                    missionDay = dateTimeUtility.addValueToDate(missionDay, 1, DateTimeUtility.DateFieldType.DAY);
                }
            } else {
                ofr.append(updateEmployeePlannerAndShiftFromVactionRequest(
                        String.valueOf(vacationRequest.getEmployee().getDbid()),
                        currentYear.toString(),
                        vacationMethods,
                        isRemoveHoliday,
                        isRemoveOffday,
                        vacationRequest.getVacation().getUnitOfMeasure(),
                        loggedUser));
                if (!ofr.getErrors().isEmpty()) {
                    return ofr;
                }

                Map<Long, List<Date>> consolidationMap = getEmployeeMonthlyConsolidation(
                        String.valueOf(vacationRequest.getEmployee().getDbid()),
                        vacationRequest.getStartDate(),
                        vacationRequest.getEndDate(),
                        loggedUser);

                ofr.append(updateDailyAttendanceFromVactionRequest(
                        vacationRequest,
                        isRemoveOffday,
                        isRemoveHoliday,
                        loggedUser));
                if (!ofr.getErrors().isEmpty()) {
                    return ofr;
                }

                List<Long> consolidationDbids = new ArrayList<Long>(consolidationMap.keySet());

                ofr.append(updateMonthlyConsolidationFromVacationRequest(
                        String.valueOf(vacationRequest.getEmployee().getDbid()),
                        consolidationDbids,
                        loggedUser));
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("SystemInternalError", loggedUser));
        }
        return ofr;
    }

    //@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private OFunctionResult updateEmployeePlannerAndShiftFromVactionRequest(String employeeDbid, String currentYear, List<String> vacationMethods,
            boolean isRemoveHoliday, boolean isRemoveOffday, String unitOfMeasure, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String plannerSQL;
            String plannerWhere;

            plannerWhere = " Where plannerYear = '" + currentYear + "' "
                    + " AND employee_dbid = " + employeeDbid;
            plannerSQL = "Update EmployeeAnnualPlanner Set ";

            String shiftSQL;
            shiftSQL = "Update EmployeeShift Set ";

            String cols = "";
            Class types[] = new Class[1];
            types[0] = Void.class;

            for (int i = 0; i < vacationMethods.size(); i++) {
                cols += vacationMethods.get(i);
                if (i != vacationMethods.size() - 1) {
                    cols += ", ";
                }
            }
            String plannerSelect;
            Object[] planner = null;
            if (!cols.isEmpty()) {
                plannerSelect = " Select " + cols
                        + " From EmployeeAnnualPlanner"
                        + plannerWhere;

                if (!cols.contains(",") && vacationMethods.size() == 1) {
                    Object obj = oem.executeEntityNativeQuery(plannerSelect, loggedUser);
                    if (obj != null) {
                        planner = new Object[1];
                        planner[0] = obj;
                    }
                } else {
                    planner = (Object[]) oem.executeEntityNativeQuery(plannerSelect, loggedUser);
                }

            }
            if (planner == null) {
                return ofr;
            }
            Object dayValue;
            String dayValueStr;
            String value;
            cols = "";
            String shiftCols = "";
            for (int j = 0; j < vacationMethods.size(); j++) {
                String[] temp = vacationMethods.get(j).split("D");
                String[] temp2 = temp[0].split("m");
                String month = temp2[1];
                String day = temp[1];
                dayValue = planner[j];

                if (dayValue != null) {
                    dayValueStr = (String) dayValue;
                    if (isRemoveHoliday && dayValueStr.equals("H")) {
                        // holiday
                        value = "H";
                    } else if (!isRemoveHoliday && dayValueStr.equals("H")) {
                        // vacation
                        value = "V";
                    } else if (isRemoveOffday && dayValueStr.equals("O")) {
                        // offday
                        value = "O";
                    } else if (!isRemoveOffday && dayValueStr.equals("O")) {
                        // vacation
                        value = "V";
                    } else if (unitOfMeasure.equals("H")) {
                        value = "A";
                    } else {
                        value = "V";
                    }
                    if (value.equals("V") || value.equals("A")) {
                        if (!cols.equals("")) {
                            cols += ", ";
                        }
                        cols += vacationMethods.get(j) + " = '" + value + "' ";

                        if (value.equals("V")) {
                            if (!shiftCols.equals("")) {
                                shiftCols += ", ";
                            }
                            shiftCols += vacationMethods.get(j) + " = '" + value + "' ";
                        }
                    }
                }
            }
            if (!cols.equals("")) {
                plannerSQL += cols + plannerWhere;
                oem.executeEntityUpdateNativeQuery(plannerSQL, loggedUser);
            }
            if (!shiftCols.equals("")) {
                shiftSQL += shiftCols + plannerWhere;
                oem.executeEntityUpdateNativeQuery(shiftSQL, loggedUser);
            }
            ofr.addSuccess(usrMsgService.getUserMessage("EAP-S0001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("EAP-E0001", loggedUser));
        }
        return ofr;
    }

    private OFunctionResult updateDailyAttendanceFromVactionRequest(EmployeeVacationRequest employeeVacationRequest, boolean isIgnoreWeekend, boolean isIgnoreHoliday, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            ArrayList con = new ArrayList();
            con.add("employee.dbid = " + employeeVacationRequest.getEmployee().getDbid());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            con.add("dailyDate >= '" + df.format(employeeVacationRequest.getStartDate()) + "'");
            con.add("dailyDate <= '" + df.format(employeeVacationRequest.getEndDate()) + "'");
            List<EmployeeDailyAttendance> employeeDailyAttendances = oem.loadEntityList(EmployeeDailyAttendance.class.getSimpleName(), con, null, null, loggedUser);
            con.clear();
            con.add("code = 'Vacation'");
            UDC vacation = (UDC) oem.loadEntity(UDC.class.getSimpleName(), con, null, loggedUser);
            con.clear();
            con.add("code = 'Mission'");
            UDC mission = (UDC) oem.loadEntity(UDC.class.getSimpleName(), con, null, loggedUser);
            for (EmployeeDailyAttendance employeeDailyAttendance : employeeDailyAttendances) {
                employeeDailyAttendance.setBreakIn(null);
                employeeDailyAttendance.setBreakOut(null);
                employeeDailyAttendance.setBreak2to(null);
                employeeDailyAttendance.setBreak2From(null);
                employeeDailyAttendance.setBreakPeriod(null);
                employeeDailyAttendance.setPlannedIn(null);
                employeeDailyAttendance.setPlannedOut(null);
                employeeDailyAttendance.setDayOverTimeDisplay(null);
                employeeDailyAttendance.setDayOverTimePeriod(null);
                employeeDailyAttendance.setDayOverTimeIn(null);
                employeeDailyAttendance.setDayOverTimeOut(null);
                employeeDailyAttendance.setNotApproved(null);
                employeeDailyAttendance.setNotApprovedDay(null);
                employeeDailyAttendance.setNotApprovedNight(null);
                employeeDailyAttendance.setNotApprovedHolidayDisplay(null);
                employeeDailyAttendance.setNotApprovedHolidayOT(null);
                employeeDailyAttendance.setNotApprovedWeekendDisplay(null);
                employeeDailyAttendance.setNotApprovedWeekendOT(null);
                employeeDailyAttendance.setApprovedHolidayOTDisplay(null);
                employeeDailyAttendance.setApprovedHolidayOT(null);
                employeeDailyAttendance.setApprovedWeekendOTDisplay(null);
                employeeDailyAttendance.setApprovedWeekendOT(null);
                employeeDailyAttendance.setDelayIn(null);
                employeeDailyAttendance.setDelayOut(null);
                employeeDailyAttendance.setDelayHoursDisplay(null);
                employeeDailyAttendance.setDelayHoursPeriod(null);

                boolean isWeekend = false;
                boolean isHoliday = false;
                if (employeeDailyAttendance.getDayNature().getCode().toLowerCase().contains("weekend") && isIgnoreWeekend) {
                    isWeekend = true;
                } else if (employeeDailyAttendance.getDayNature().getCode().toLowerCase().contains("holiday") && isIgnoreHoliday) {
                    isHoliday = true;
                }
                if (!isHoliday && !isWeekend) {
                    if (isMissionDay(employeeDailyAttendance.getDailyDate(), employeeDailyAttendance.getEmployee(), loggedUser)) {
                        employeeDailyAttendance.setDayNature(mission);
                    } else if (isVacationDay(employeeDailyAttendance.getDailyDate(), employeeDailyAttendance.getEmployee(), loggedUser)) {
                        employeeDailyAttendance.setDayNature(vacation);
                    }
                }
                entitySetupService.callEntityUpdateAction(employeeDailyAttendance, loggedUser);
            }
//            String sql;
//            Date startDate = null;
//            Date endDate = null;
//            List<Date> dates;
//            for (Long monthlyConsolidationDbid : consolidationMap.keySet()) {
//                dates = consolidationMap.get(monthlyConsolidationDbid);
//                if (dates != null && !dates.isEmpty()) {
//                    startDate = dates.get(0);
//                    endDate = dates.get(dates.size() - 1);
//
//                    sql = "Update EmployeeDailyAttendance "
//                            + " Set monthlyConsolidation_dbid = " + monthlyConsolidationDbid
//                            + " , dayNature_Dbid = (select dbid from dayNatures Where value = 'Vacation' )"
//                            + " Where   ( employee_Dbid = " + employeeDbid + " )"
//                            + " AND     ( dailyDate between '" + dateTimeUtility.DATE_FORMAT.format(startDate) + "'"
//                            + " AND     '" + dateTimeUtility.DATE_FORMAT.format(endDate) + "' ) ";
//                    if (isRemoveHoliday) {
//                        sql += " AND (dayNature_Dbid not in (select dbid from dayNatures Where value Like '%Holiday%' ))";
//                    }
//                    if (isRemoveOffday) {
//                        sql += " AND (dayNature_Dbid not in (select dbid from dayNatures Where value Like '%WeekEnd%' ))";
//                    }
//
//                    oem.executeEntityUpdateNativeQuery(sql, loggedUser);
//                    ofr.addSuccess(usrMsgService.getUserMessage("EDA-S0001", loggedUser));
//                }
//            }
        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("EDA-E0001", loggedUser));
        }
        return ofr;
    }

    private Map<Long, List<Date>> getEmployeeMonthlyConsolidation(String employeeDbid, Date startDate, Date endDate,
            OUser loggedUser) {
        Map<Long, List<Date>> monthlyConsolidationMap = new Hashtable<Long, List<Date>>();
        try {
            String paymentMethodDbid = timeManagementService.getDefaultPaymentMethod(loggedUser);
            String paymentPeriodDbid = timeManagementService.getDefaultPaymentPeriod(loggedUser);

            Calendar calendar;
            calendar = Calendar.getInstance();
            calendar.setTime(startDate);

            Date indexDate;

            int year;
            year = calendar.get(Calendar.YEAR);
            int month;
            month = calendar.get(Calendar.MONTH) + 1;
            int day;
            day = calendar.get(Calendar.DAY_OF_MONTH);
            indexDate = startDate;
            Long calculatedPeriodDbid;
            Long monthlyConsolidationDbid = null;

            List<Date> dates;
            while (indexDate.compareTo(endDate) <= 0) {
                dates = new ArrayList<Date>();
                calculatedPeriodDbid = timeManagementService.getCalculatedPeriodOfThisDay(day, month, year, paymentMethodDbid, paymentPeriodDbid, loggedUser);
                if (calculatedPeriodDbid.equals(0)) {
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                    indexDate = calendar.getTime();
                    continue;
                }
                monthlyConsolidationDbid = timeManagementService.addingMonthlyConsolidationIfNotExist(employeeDbid, calculatedPeriodDbid.toString(), loggedUser);
                if (monthlyConsolidationDbid == -1) {
                    break;
                }
                if (monthlyConsolidationDbid != 0) {
                    if (monthlyConsolidationMap.containsKey(monthlyConsolidationDbid)) {
                        dates = monthlyConsolidationMap.get(monthlyConsolidationDbid);
                    }
                    dates.add(indexDate);
                    monthlyConsolidationMap.put(monthlyConsolidationDbid, dates);
                }
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                indexDate = calendar.getTime();
            }

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
        }
        return monthlyConsolidationMap;

    }

    private OFunctionResult updateMonthlyConsolidationFromVacationRequest(String employeeDbid, List<Long> consolidationDbids,
            OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String sql;
            String selectDaysCnt;
            Long calculatedPeriodDbid;
            String[] cutOffDates;
            Integer currentMonth = 0, currentYear = 0;

            for (Long consolidationDbid : consolidationDbids) {

                sql = "select AA.calculatedPeriod_dbid ,BB.month, BB.Year"
                        + " From EmployeeMonthlyConsolidation AA , CalculatedPeriod BB"
                        + " Where AA.dbid = " + consolidationDbid
                        + " AND AA.calculatedPeriod_dbid = BB.dbid ";
                Object[] objs = (Object[]) oem.executeEntityNativeQuery(sql, loggedUser);

                if (objs == null) {
                    continue;
                }
                calculatedPeriodDbid = (objs[0] == null) ? null : Long.parseLong(objs[0].toString());
                currentMonth = (objs[1] == null) ? null : Integer.parseInt(objs[1].toString());
                currentYear = (objs[2] == null) ? null : Integer.parseInt(objs[2].toString());

                if ((calculatedPeriodDbid != null && calculatedPeriodDbid.equals(0))
                        || calculatedPeriodDbid == null) {
                    continue;
                }

                cutOffDates = timeManagementService.getCutOffDatesFromCalculatedPeriod(currentMonth, currentYear, loggedUser);

                String startDateStr = cutOffDates[0];
                String endDateStr = cutOffDates[1];
                String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
                if (dataBaseConnectionString.toLowerCase().contains("oracle")) {
                    selectDaysCnt = "select 'V',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                            + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                            + " and daynature_dbid in (select dbid from udc where code = 'Vacation')"
                            + " union "
                            + " select 'A',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                            + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                            + " and daynature_dbid in (select dbid from udc where code = 'Absence')"
                            + " union"
                            + " select 'W',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                            + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                            + " and daynature_dbid in (select dbid from udc where code = 'Working')"
                            + " union"
                            + " select 'WO',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                            + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnWeekEnd')"
                            + " union"
                            + " select 'WH',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                            + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnHoliday')"
                            + " union"
                            + " select 'H',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                            + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                            + " and daynature_dbid in (select dbid from udc where code = 'Holiday')"
                            + " union"
                            + " select 'O',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                            + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                            + " and daynature_dbid in (select dbid from udc where code = 'WeekEnd')";
                } else {
                    selectDaysCnt = "select 'V',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= '" + endDateStr + "'"
                            + " and dailydate >= '" + startDateStr + "'"
                            + " and daynature_dbid in (select dbid from udc where code = 'Vacation')"
                            + " union "
                            + " select 'A',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= '" + endDateStr + "'"
                            + " and dailydate >= '" + startDateStr + "'"
                            + " and daynature_dbid in (select dbid from udc where code = 'Absence')"
                            + " union"
                            + " select 'W',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= '" + endDateStr + "'"
                            + " and dailydate >= '" + startDateStr + "'"
                            + " and daynature_dbid in (select dbid from udc where code = 'Working')"
                            + " union"
                            + " select 'WO',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= '" + endDateStr + "'"
                            + " and dailydate >= '" + startDateStr + "'"
                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnWeekEnd')"
                            + " union"
                            + " select 'WH',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= '" + endDateStr + "'"
                            + " and dailydate >= '" + startDateStr + "'"
                            + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnHoliday')"
                            + " union"
                            + " select 'H',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= '" + endDateStr + "'"
                            + " and dailydate >= '" + startDateStr + "'"
                            + " and daynature_dbid in (select dbid from udc where code = 'Holiday')"
                            + " union"
                            + " select 'O',count(*) from employeedailyattendance\n"
                            + " where employee_dbid =" + employeeDbid
                            + " and dailydate <= '" + endDateStr + "'"
                            + " and dailydate >= '" + startDateStr + "'"
                            + " and daynature_dbid in (select dbid from udc where code = 'WeekEnd')";
                }

                List daysCntLst = oem.executeEntityListNativeQuery(selectDaysCnt, loggedUser);
                Long vacations = (long) 0;
                Long holidays = (long) 0;
                Long weekendDays = (long) 0;
                Long absenceDays = (long) 0;
                Long actualWorkingDays = (long) 0;
                Long workingOnHolidays = (long) 0;
                Long workingOnWeekends = (long) 0;
                if (daysCntLst != null && !daysCntLst.isEmpty()) {
                    Object[] daysCntArr;
                    String dayNature;
                    for (int i = 0; i < daysCntLst.size(); i++) {
                        daysCntArr = (Object[]) daysCntLst.get(i);
                        if (daysCntArr != null && daysCntArr.length > 0) {
                            dayNature = daysCntArr[0].toString();
                            if (dayNature.equals("V")) {
                                vacations = Long.parseLong(daysCntArr[1].toString());
                            } else if (dayNature.equals("W")) {
                                actualWorkingDays = Long.parseLong(daysCntArr[1].toString());
                            } else if (dayNature.equals("WH")) {
                                workingOnHolidays = Long.parseLong(daysCntArr[1].toString());
                            } else if (dayNature.equals("WO")) {
                                workingOnWeekends = Long.parseLong(daysCntArr[1].toString());
                            } else if (dayNature.equals("H")) {
                                holidays = Long.parseLong(daysCntArr[1].toString());
                            } else if (dayNature.equals("O")) {
                                weekendDays = Long.parseLong(daysCntArr[1].toString());
                            } else {
                                absenceDays = Long.parseLong(daysCntArr[1].toString());
                            }
                        }
                    }
                }

                Long workingDays;

                int monthEnd = dateTimeUtility.getDifferenceBetweenDates(
                        dateTimeUtility.DATE_FORMAT.parse(startDateStr),
                        dateTimeUtility.DATE_FORMAT.parse(endDateStr),
                        DateTimeUtility.DateFieldType.DAY);
                workingDays = monthEnd - (weekendDays
                        + holidays);
                monthEnd++;

                sql = "Update EmployeeMonthlyConsolidation "
                        + " Set vacations = " + vacations
                        + " , absenceDays = " + absenceDays
                        + " , postedAbsenceDays = " + absenceDays
                        + " , weekendDays = " + weekendDays
                        + " , holidays = " + holidays
                        + " , workingDays = " + workingDays
                        + " Where dbid = " + consolidationDbid;

                oem.executeEntityUpdateNativeQuery(sql, loggedUser);

            }
            ofr.addSuccess(usrMsgService.getUserMessage("EMC-S0001", loggedUser));

        } catch (Exception ex) {
            OLog.logException(ex, loggedUser);
            ofr.addError(usrMsgService.getUserMessage("EMC-E0001", loggedUser));
        }
        return ofr;
    }

    private String calculateRotSequence(int shiftSequence, NormalWorkingCalendar shift, OUser loggedUser) {
        String rotSequenceStr = "1";
        try {
            TMCalendar tMCalendar = shift.getCalendar();
            String selectTotalDaysBefore = "select sum(numberOfDays) from NormalWorkingCalendar where calendar_dbid = " + tMCalendar.getDbid()
                    + " and shiftSequence < " + shift.getShiftSequence();
            Object resultObj = oem.executeEntityNativeQuery(selectTotalDaysBefore, loggedUser);
            Integer rotSequence = 0;
            if (resultObj != null) {
                rotSequence = Integer.parseInt(resultObj.toString());
            }
            rotSequence += shiftSequence;
            rotSequenceStr = rotSequence.toString();
        } catch (Exception e) {
            OLog.logException(e, null);
        }
        return rotSequenceStr;
    }

    @Override
    public OFunctionResult validateOTRequest(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
//
            EmployeeOTRequest employeeOTRequest = (EmployeeOTRequest) odm.getData().get(0);
            Employee employee = employeeOTRequest.getEmployee();
            List cond = new ArrayList();
            cond.add("employee.dbid = " + employee.getDbid());
            SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd");

            cond.add("requestedDate = '" + dformat.format(employeeOTRequest.getRequestedDate()) + "'");
            List<EmployeeOTRequest> employeeOTRequestList = oem.loadEntityList(EmployeeOTRequest.class.getSimpleName(), cond, null, null, loggedUser);
            for (EmployeeOTRequest employeeOTRequestelement : employeeOTRequestList) {
                String avg = dateTimeUtility.subtractTime(employeeOTRequest.getTimeFrom(), employeeOTRequest.getTimeTo());
                String between = dateTimeUtility.addTime(avg, employeeOTRequest.getTimeFrom());
                if (dateTimeUtility.isInterval(employeeOTRequestelement.getTimeFrom(), employeeOTRequestelement.getTimeTo(), between)) {
                    ofr.addError(usrMsgService.getUserMessage("Found another OT in this time", loggedUser));
                    return ofr;
                }

                if (dateTimeUtility.isInterval(employeeOTRequestelement.getTimeFrom(), employeeOTRequestelement.getTimeTo(), employeeOTRequest.getTimeFrom())
                        || dateTimeUtility.isInterval(employeeOTRequestelement.getTimeFrom(), employeeOTRequestelement.getTimeTo(), employeeOTRequest.getTimeTo())) {
                    ofr.addError(usrMsgService.getUserMessage("Found another OT in this time", loggedUser));
                    return ofr;
                }
            }

        } catch (Exception e) {
            ofr.addError(usrMsgService.getUserMessage("Error In DB", loggedUser), e);
        }

        return ofr;
    }

    @Override
    public OFunctionResult importOTRequest(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        Employee employee;
        try {
            List cond = new ArrayList();
            cond.add("done = false");
            List<EmployeeOTRImport> employeeOTRImportList;
            employeeOTRImportList = oem.loadEntityList("EmployeeOTRImport", cond, null, null, loggedUser);

            for (int i = 0; i < employeeOTRImportList.size(); i++) {
                EmployeeOTRImport employeeOTRImport = employeeOTRImportList.get(i);

                cond.clear();
                cond.add("code = '" + employeeOTRImport.getEmployeeCode() + "'");
                employee = (Employee) oem.loadEntity(Employee.class.getSimpleName(), cond, null, loggedUser);
                if (employee != null) {
                    EmployeeOTRequest employeeOTRequest = new EmployeeOTRequest();
                    employeeOTRequest.setEmployee(employee);
                    employeeOTRequest.setRequestedDate(employeeOTRImport.getDailyDate());
                    employeeOTRequest.setTimeFrom(employeeOTRImport.getTimeFrom());
                    employeeOTRequest.setTimeTo(employeeOTRImport.getTimeTo());
                    OFunctionResult employeeOTRImportOFR = entitySetupService.callEntityCreateAction(employeeOTRequest, loggedUser);
                    if (employeeOTRImportOFR.getErrors().isEmpty()) {
                        employeeOTRImport.setDone(true);
                        employeeOTRImport.setComment("");

                    } else {
                        String comment = "";
                        for (UserMessage msg : employeeOTRImportOFR.getErrors()) {
                            comment += msg.getMessageTextTranslated() + " ";
                        }
                        employeeOTRImportOFR.getErrors().get(0);
                        employeeOTRImport.setComment(comment);
                    }
                    oem.saveEntity(employeeOTRImport, loggedUser);
                }
            }
            ofr.addSuccess(usrMsgService.getUserMessage("EOTRI-S00001", loggedUser));
        } catch (Exception e) {
            ofr.addError(usrMsgService.getUserMessage("Error in DB", loggedUser), e);
        }

        return ofr;
    }

    @Override
    public OFunctionResult validateTMCalendarFormat(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        TMCalendar tmCalendar = (TMCalendar) odm.getData().get(0);
        boolean validateWE = true, validateHO = true;
        if (tmCalendar.getMaximumWeekEndOT() != null) {
            validateWE = dateTimeUtility.validateTimeFormat(tmCalendar.getMaximumWeekEndOT());
        }
        if (tmCalendar.getMaximumHolidayOT() != null) {
            validateHO = dateTimeUtility.validateTimeFormat(tmCalendar.getMaximumHolidayOT());
        }
        if (!validateWE || !validateHO) {
            ofr.addError(usrMsgService.getUserMessage("TMCalendar_001", loggedUser));
            return ofr;
        }
        if (tmCalendar.getMaximumWeekEndOT().length() == 4) {
            tmCalendar.setMaximumWeekEndOT("0" + tmCalendar.getMaximumWeekEndOT());
        }
        if (tmCalendar.getMaximumHolidayOT().length() == 4) {
            tmCalendar.setMaximumHolidayOT("0" + tmCalendar.getMaximumHolidayOT());
        }

        return ofr;
    }

    @Override
    public OFunctionResult validateNormalWorkingCalendarFormat(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        boolean validateTimeIn = true;
        boolean validateTimeOut = true;
        boolean validateDayStartTime = true;
        boolean validateTolerance = true;
        boolean validateNumberOfDays = true;
        boolean validateNumberOfHours = true;
        NormalWorkingCalendar normalWorkingCalendar = (NormalWorkingCalendar) odm.getData().get(0);
        if (normalWorkingCalendar.getTimeIn() != null || normalWorkingCalendar.getTimeIn() != "") {
            validateTimeIn = dateTimeUtility.validateTimeFormat(normalWorkingCalendar.getTimeIn());
        }
        if (normalWorkingCalendar.getTimeOut() != null || normalWorkingCalendar.getTimeOut() != "") {
            validateTimeOut = dateTimeUtility.validateTimeFormat(normalWorkingCalendar.getTimeOut());
        }
        if (normalWorkingCalendar.getStartDayTime() != null || normalWorkingCalendar.getStartDayTime() != "") {
            validateDayStartTime = dateTimeUtility.validateTimeFormat(normalWorkingCalendar.getStartDayTime());
        }
        if (normalWorkingCalendar.getTolerence() != null) {
            validateTolerance = isNumeric(normalWorkingCalendar.getTolerence().toBigInteger().toString());
        } else {
            normalWorkingCalendar.setTolerence(BigDecimal.ZERO);
        }
        if (normalWorkingCalendar.getNumberOfDays() != null) {
            validateNumberOfDays = isNumeric(normalWorkingCalendar.getNumberOfDays().toString());
        } else {
            normalWorkingCalendar.setNumberOfDays(0);
        }
        if (normalWorkingCalendar.getNumberOfHours() != null) {
            validateNumberOfHours = isNumeric(normalWorkingCalendar.getNumberOfHours().toBigInteger().toString());
        } else {
            normalWorkingCalendar.setNumberOfHours(BigDecimal.ZERO);
        }

        if (!validateNumberOfHours || !validateNumberOfDays || !validateTimeIn || !validateTimeOut || !validateDayStartTime || !validateTolerance) {
            ofr.addError(usrMsgService.getUserMessage("TMCalendar_001", loggedUser));
            return ofr;
        }

        if (normalWorkingCalendar.getTimeIn().length() == 4) {
            normalWorkingCalendar.setTimeIn("0" + normalWorkingCalendar.getTimeIn());
        }
        if (normalWorkingCalendar.getTimeOut().length() == 4) {
            normalWorkingCalendar.setTimeOut("0" + normalWorkingCalendar.getTimeOut());
        }
        if (normalWorkingCalendar.getStartDayTime().length() == 4) {
            normalWorkingCalendar.setStartDayTime("0" + normalWorkingCalendar.getStartDayTime());
        }
        return ofr;
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public OFunctionResult validateOTSegmentFormat(ODataMessage odm, OFunctionParms params, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        OTSegment otSegment = (OTSegment) odm.getData().get(0);
        boolean validateFromSegment = true;
        boolean validateToSegment = true;

        if (otSegment.getFromTime() != null || otSegment.getFromTime() != "") {
            validateFromSegment = dateTimeUtility.validateTimeFormat(otSegment.getFromTime());
        }

        if (otSegment.getToTime() != null || otSegment.getToTime() != "") {
            validateToSegment = dateTimeUtility.validateTimeFormat(otSegment.getToTime());
        }

        if (!validateFromSegment || !validateToSegment) {
            ofr.addError(usrMsgService.getUserMessage("TMCalendar_001", loggedUser));
            return ofr;
        }
        if (otSegment.getFromTime().length() == 4) {
            otSegment.setFromTime("0" + otSegment.getFromTime());
        }
        if (otSegment.getToTime().length() == 4) {
            otSegment.setToTime("0" + otSegment.getToTime());
        }

        return ofr;
    }

    public void calculateConsolidationForSingleEmployee(Long employeeDbid, Long calcPeriodDbid, OUser loggedUser) {
        Employee employee = new Employee();
        Long emp;
        BigDecimal dayOverTimeValue = new BigDecimal(0);
        BigDecimal nightOverTimeValue = new BigDecimal(0);
        BigDecimal delayValue = new BigDecimal(0);
        BigDecimal earlyLeaveValue = new BigDecimal(0);
        BigDecimal holidayOT = new BigDecimal(0);
        BigDecimal weekendOT = new BigDecimal(0);
        BigDecimal workingHours = new BigDecimal(0);
        BigDecimal missionHours = new BigDecimal(0);
        BigDecimal lessWorkValue = new BigDecimal(0);
        BigDecimal dayLessWorkValue = new BigDecimal(0);
        long vacations = 0;
        long absenceDays = 0;
        int missionDays = 0;
        long workingOnHolidays = 0;
        long workingOnWeekends = 0;
        try {
            CalculatedPeriod calculatedPeriod = (CalculatedPeriod) oem.executeEntityQuery("select Entity from CalculatedPeriod Entity where Entity.dbid=" + calcPeriodDbid, loggedUser);
            String[] dates = timeManagementService.getCutOffDatesFromCalculatedPeriod(calculatedPeriod.getMonth(), calculatedPeriod.getYear(), loggedUser);
            String startDateStr = dates[0];
            String endDateStr = dates[1];
            emp = employeeDbid;
            employee = (Employee) oem.executeEntityQuery("select Entity from Employee Entity where Entity.dbid=" + emp, loggedUser);
            EmployeeMonthlyConsolidation monthlyConsolidation = (EmployeeMonthlyConsolidation) oem.executeEntityQuery("select Entity from EmployeeMonthlyConsolidation Entity where Entity.employee.dbid=" + emp + " and Entity.calculatedPeriod.dbid=" + calcPeriodDbid, loggedUser);
            if (monthlyConsolidation == null) {
                monthlyConsolidation = new EmployeeMonthlyConsolidation();
                monthlyConsolidation.setPosted("N");
                monthlyConsolidation.setCalculatedPeriod(calculatedPeriod);
            }
            String dataBaseConnectionString = EntityUtilities.getDBConnectionString(loggedUser);
            String selectDaysCnt;
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                selectDaysCnt = "select 'V',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'Vacation')"
                        + " union "
                        + " select 'A',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'Absence')"
                        + " union"
                        + " select 'W',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'Working')"
                        + " union"
                        + " select 'WO',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnWeekEnd')"
                        + " union"
                        + " select 'WH',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnHoliday')"
                        + " union"
                        + " select 'H',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'Holiday')"
                        + " union"
                        + " select 'O',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'WeekEnd')"
                        + " union"
                        + " select 'M',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and daynature_dbid in (select dbid from udc where code = 'Mission')";

            } else {
                selectDaysCnt = "select 'V',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'Vacation')"
                        + " union "
                        + " select 'A',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'Absence')"
                        + " union"
                        + " select 'W',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'Working')"
                        + " union"
                        + " select 'WO',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnWeekEnd')"
                        + " union"
                        + " select 'WH',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'WorkingOnHoliday')"
                        + " union"
                        + " select 'H',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'Holiday')"
                        + " union"
                        + " select 'O',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'WeekEnd')"
                        + " union"
                        + " select 'M',count(*) from employeedailyattendance\n"
                        + " where employee_dbid =" + emp
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and daynature_dbid in (select dbid from udc where code = 'Mission')";
            }

            boolean calculateBasedOnActualDays = true; //should be from system setup
            long actualWorkingDays = 0, totalWorkingDays = 0,
                    weekendDays = 0, holidays = 0;

            List daysCntLst = oem.executeEntityListNativeQuery(selectDaysCnt, loggedUser);
            if (daysCntLst != null && !daysCntLst.isEmpty()) {
                Object[] daysCntArr;
                String dayNature;
                for (int i = 0; i < daysCntLst.size(); i++) {
                    daysCntArr = (Object[]) daysCntLst.get(i);
                    if (daysCntArr != null && daysCntArr.length > 0) {
                        dayNature = daysCntArr[0].toString();
                        if (dayNature.equals("V")) {
                            vacations = Long.parseLong(daysCntArr[1].toString());
                        } else if (dayNature.equals("W")) {
                            //workingDays = Long.parseLong(daysCntArr[1].toString());
                            actualWorkingDays = Long.parseLong(daysCntArr[1].toString());
                        } else if (dayNature.equals("WH")) {
                            workingOnHolidays = Long.parseLong(daysCntArr[1].toString());
                        } else if (dayNature.equals("WO")) {
                            workingOnWeekends = Long.parseLong(daysCntArr[1].toString());
                        } else if (dayNature.equals("H")) {
                            holidays = Long.parseLong(daysCntArr[1].toString());
                        } else if (dayNature.equals("O")) {
                            weekendDays = Long.parseLong(daysCntArr[1].toString());
                        } else if (dayNature.equals("M")) {
                            missionDays = Integer.parseInt(daysCntArr[1].toString());
                        } else {
                            absenceDays = Long.parseLong(daysCntArr[1].toString());
                        }
                    }
                }
            }

            // if the working days is based on 30 days or actual days
            if (!calculateBasedOnActualDays) {
                totalWorkingDays = 30 - (absenceDays + vacations);
            } else {
                totalWorkingDays = actualWorkingDays
                        + workingOnHolidays
                        + workingOnWeekends
                        + weekendDays
                        + holidays
                        + absenceDays
                        + missionDays;
            }
            //select the sum of mission of type hours and convert it to days
            //cust for Suez Cement
            // *&* Mission *&*
            String sumPeriodHours;
            /* sumPeriodHours = " select sum(exceptionHours) from employeedailyattendance\n"
             + " where employee_dbid =" + emp.getDbid()
             + " and dailydate <= '" + endDateStr + "'"
             + " and dailydate >= '" + startDateStr + "'"
             //        + " and daynature_dbid in (select dbid from udc where code = 'Mission')"
             + " and dbid <>" + empDA.getDbid();*/
            /* sumPeriodHours = " select sum(period) from employeedayoffrequest mission "
             + " left join employeedailyattendance on (employeedailyattendance.employee_dbid = mission.employee_dbid"
             + " and employeedailyattendance.dailydate <= '" + endDateStr + "'"
             + " and employeedailyattendance.dailydate >= '" + startDateStr + "' and employeedailyattendance.deleted = 0)"
             + " left join dayoff on (mission.vacation_dbid = dayoff.dbid and dayoff.deleted = 0)"
             + " where employeedailyattendance.dbid <>" + empDA.getDbid() + " and dayoff.unitOfMeasure = 'H'"
             + " and dayoff.mission = 1 and mission.employee_dbid = " + emp.getDbid();*/
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                sumPeriodHours = " select sum(exceptionHours) from employeedailyattendance "
                        + " where employee_dbid =" + emp
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and dailydate >= '" + startDateStr + "' "
                        + " and deleted = 0 ";
            } else {
                sumPeriodHours = " select sum(exceptionHours) from employeedailyattendance "
                        + " where employee_dbid =" + emp
                        + " and dailydate <= to_date('" + endDateStr + "','yyyy-mm-dd')"
                        + " and dailydate >= to_date('" + startDateStr + "','yyyy-mm-dd')"
                        + " and deleted = 0 ";
            }
            Object obj = oem.executeEntityNativeQuery(sumPeriodHours, loggedUser);
            if (obj != null) {
                missionHours = (BigDecimal) obj;
            }

            // select the sum of the calculations; DOT, NOT, ealyLEave, delay, WeenEndOT, HolidayOT
            String selectOT;
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                selectOT = "select sum(dayovertimeperiod), sum(nightovertimeperiod),"
                        + "sum(delayHoursPeriod), sum(earlyLeavePeriod), sum(approvedWeekendOT)"
                        + " , sum(approvedHolidayOT), sum(workinghours), sum(lessworkperiod),sum(daylessworkperiod) from employeedailyattendance "
                        + " where employee_dbid = " + emp
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and deleted = 0";
            } else {
                selectOT = "select sum(dayovertimeperiod), sum(nightovertimeperiod),"
                        + "sum(delayHoursPeriod), sum(earlyLeavePeriod), sum(approvedWeekendOT)"
                        + " , sum(approvedHolidayOT), sum(workinghours), sum(lessworkperiod),sum(daylessworkperiod) from employeedailyattendance "
                        + " where employee_dbid = " + emp
                        + " and dailydate >= " + dateTimeUtility.dateTimeFormatForOracle(startDateStr)
                        + " and dailydate <= " + dateTimeUtility.dateTimeFormatForOracle(endDateStr)
                        + " and deleted = 0";
            }
            List calcsLst = oem.executeEntityListNativeQuery(selectOT, loggedUser);
            Object[] calcsArr = !calcsLst.isEmpty() ? (Object[]) calcsLst.get(0) : null;
            if (calcsArr != null) {
                dayOverTimeValue = calcsArr[0] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[0];
                nightOverTimeValue = calcsArr[1] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[1];
                delayValue = calcsArr[2] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[2];
                earlyLeaveValue = calcsArr[3] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[3];
                weekendOT = calcsArr[4] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[4];
                holidayOT = calcsArr[5] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[5];
                workingHours = calcsArr[6] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[6];
                lessWorkValue = calcsArr[7] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[7];
                dayLessWorkValue = calcsArr[8] == null ? BigDecimal.ZERO : (BigDecimal) calcsArr[8];
            }
            monthlyConsolidation.setEmployee(employee);
            monthlyConsolidation.setDayOvertime(dayOverTimeValue);
            monthlyConsolidation.setPostedDayOvertime(dayOverTimeValue);
            monthlyConsolidation.setParametrizedDayOvertime(dayOverTimeValue);
            monthlyConsolidation.setNightOvertime(nightOverTimeValue);
            monthlyConsolidation.setPostedNightOvertime(nightOverTimeValue);
            monthlyConsolidation.setParametrizedNightOvertime(nightOverTimeValue);
            monthlyConsolidation.setDelayHours(delayValue);
            monthlyConsolidation.setPostedDelayHours(delayValue);
            monthlyConsolidation.setParametrizedDelayHours(delayValue);
            monthlyConsolidation.setEarlyLeave(earlyLeaveValue);
            monthlyConsolidation.setPostedEarlyLeave(earlyLeaveValue);
            monthlyConsolidation.setParametrizedEarlyLeave(earlyLeaveValue);
            monthlyConsolidation.setVacations(BigDecimal.valueOf(vacations));
            monthlyConsolidation.setDayLessWork(dayLessWorkValue);
            // *&* Mission *&*
            monthlyConsolidation.setMissions(missionDays);
            monthlyConsolidation.setMissionHours(missionHours);
            monthlyConsolidation.setAbsenceDays(BigDecimal.valueOf(absenceDays));
            monthlyConsolidation.setPostedAbsenceDays(BigDecimal.valueOf(absenceDays));
            monthlyConsolidation.setParametrizedAbsenceDays(BigDecimal.valueOf(absenceDays));
            monthlyConsolidation.setWorkingDays(BigDecimal.valueOf(totalWorkingDays));
            monthlyConsolidation.setActualWorkingDays((int) (actualWorkingDays));
            monthlyConsolidation.setWeekend(weekendOT);
            monthlyConsolidation.setPostedWeekend(weekendOT);
            monthlyConsolidation.setParametrizedWeekend(weekendOT);
            monthlyConsolidation.setHoliday(holidayOT);
            monthlyConsolidation.setPostedHoliday(holidayOT);
            monthlyConsolidation.setParametrizedHoliday(holidayOT);
            monthlyConsolidation.setWorkingHours(workingHours);
            monthlyConsolidation.setWorkingOnWeekEndDays((int) workingOnWeekends);
            monthlyConsolidation.setWorkingOnHolidayDays((int) workingOnHolidays);
            monthlyConsolidation.setHolidays((int) holidays);
            monthlyConsolidation.setWeekEndDays((int) weekendDays);
            monthlyConsolidation.setLessWork(lessWorkValue);
            monthlyConsolidation.setPostedLessWork(lessWorkValue);
            monthlyConsolidation.setParametrizedLessWork(lessWorkValue);
            oem.saveEntity(monthlyConsolidation, loggedUser);
            String update;
            if (!dataBaseConnectionString.toLowerCase().contains("oracle")) {
                update = "update employeedailyattendance set monthlyconsolidation_dbid =" + monthlyConsolidation.getDbid()
                        + " where employee_dbid = " + emp
                        + " and dailydate >= '" + startDateStr + "'"
                        + " and dailydate <= '" + endDateStr + "'"
                        + " and deleted = 0";
            } else {
                update = "update employeedailyattendance set monthlyconsolidation_dbid =" + monthlyConsolidation.getDbid()
                        + " where employee_dbid = " + emp
                        + " and dailydate >= to_date('" + startDateStr + "','YYYY-MM-DD')"
                        + " and dailydate <= to_date('" + endDateStr + "','YYYY-MM-DD')"
                        + " and deleted = 0";
            }
            oem.executeEntityUpdateNativeQuery(update, loggedUser);
        } catch (Exception e) {
            OLog.logException(e, null);
        }
    }

    public void calculateConsolidationForPosting(String condition, Long calcPeriodDbid, OUser loggedUser) {
        String query = "select DISTINCT(employee_dbid) from employeedailyattendanceimport" + condition + " and deleted=0";
        try {
            List<Long> employees = oem.executeEntityListNativeQuery(query, loggedUser);

            for (int i = 0; i < employees.size(); i++) {
                calculateConsolidationForSingleEmployee(employees.get(i), calcPeriodDbid, loggedUser);
                oem.executeEntityUpdateNativeQuery("update employeedailyattendanceimport set done=1 where calcperiod_dbid=" + calcPeriodDbid + " and employee_dbid=" + employees.get(i), loggedUser);

            }

        } catch (Exception e) {
            OLog.logException(e, null);
        }
    }

    private String calculateLessWorkDuringTheDay(String timeIn, String timeOut, Employee employee, Date dailydate, OUser loggedUser) {
        String lessWorkDuringTheDay = "00:00";
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-YYYY");
            String dailydateString = df.format(dailydate);
            Date dt = dailydate;
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, 1);
            dt = c.getTime();
            String nextDay = df.format(dt);
            String sql = "";
            String vacationHrsSql = "";
            String deleteEDLW = "delete from employeeDayLessWork where employee_dbid=" + employee.getDbid() + " and entrydate=" + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(dailydate));
            oem.executeEntityUpdateNativeQuery(deleteEDLW, loggedUser);
            if (dateTimeUtility.compareTime(timeOut, "24:00") == 1) {
                timeOut = dateTimeUtility.subtractTime("24:00", timeOut);
            }
            //Attendance on 2 days
            if (dateTimeUtility.compareTime(timeIn, timeOut) == 1) {
                sql += "select type,entrytime,entrydate from REPtm_clock_entry where employeecode ='" + employee.getCode()
                        + "' and (ENTRYDATE='" + dailydateString + "' and REPLACE(ENTRYTIME,':','') >" + Integer.parseInt(timeIn.replace(":", ""))
                        + ") or (ENTRYDATE='" + nextDay
                        + "' and REPLACE(ENTRYTIME,':','')<" + Integer.parseInt(timeOut.replace(":", ""))
                        + " ) order by PRINTORDER ";
                List<Object[]> clockEntries = oem.executeEntityListNativeQuery(sql, loggedUser);
                if (!clockEntries.isEmpty() && clockEntries.size() > 1) {
                    for (int i = 0; i < clockEntries.size() - 1; i++) {
                        if (clockEntries.get(i)[0].toString().equalsIgnoreCase("O") && clockEntries.get(i + 1)[0].toString().equalsIgnoreCase("I")) {
                            EmployeeDayLessWork employeeDayLessWork = new EmployeeDayLessWork();
                            employeeDayLessWork.setEmployee(employee);
                            employeeDayLessWork.setEntryDate(dailydate);
                            employeeDayLessWork.setTimeOut(clockEntries.get(i)[1].toString());
                            employeeDayLessWork.setTimeIn(clockEntries.get(i + 1)[1].toString());

                            if (clockEntries.get(i)[2].toString().equals(clockEntries.get(i + 1)[2].toString())) {
                                lessWorkDuringTheDay = dateTimeUtility.addTime(lessWorkDuringTheDay, dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), clockEntries.get(i + 1)[1].toString()));
                                employeeDayLessWork.setPeriod(dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), clockEntries.get(i + 1)[1].toString()));

                            } else {
                                lessWorkDuringTheDay = dateTimeUtility.addTime(lessWorkDuringTheDay, dateTimeUtility.subtractTime(dateTimeUtility.addTime(clockEntries.get(i)[1].toString(), "24:00"), clockEntries.get(i + 1)[1].toString()));
                                employeeDayLessWork.setPeriod(dateTimeUtility.subtractTime(dateTimeUtility.addTime(clockEntries.get(i)[1].toString(), "24:00"), clockEntries.get(i + 1)[1].toString()));

                            }
                            oem.saveEntity(employeeDayLessWork, loggedUser);
                        }

                    }
                }
            } else {
                lessWorkDuringTheDay = calculateLessWorkDuringTheDayForSingleDay(timeIn, timeOut, employee, dailydate, loggedUser);
            }
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lessWorkDuringTheDay;

    }

    @Override
    public OFunctionResult importMultiEntryAttendance(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            String sql = "";
            DateFormat df = new SimpleDateFormat("dd-MM-YYYY");
            oem.executeEntityUpdateNativeQuery("DELETE FROM TM_MULTIENTRY_IMPORT\n"
                    + "WHERE rowid not in\n"
                    + "(SELECT MIN(rowid)\n"
                    + "FROM TM_MULTIENTRY_IMPORT\n"
                    + "GROUP BY EMPLOYEE_DBID, ENTRYTIME, TYPE)", loggedUser);
            String dailydateString = "";
            List cond = new ArrayList();
            cond.add("posted = false");
            List<MultiEntryImport> entries = oem.loadEntityList("MultiEntryImport", cond, null, null, loggedUser);
            if (!entries.isEmpty()) {
                for (int i = 0; i < entries.size(); i++) {
                    dailydateString = df.format(entries.get(i).getEntryDate()) + " " + entries.get(i).getEntryTime();
                    String checkSql = "select * from tm_clock_entry where organization_id=" + entries.get(i).getEmployee().getUnit().getCompany().getDbid()
                            + " and Employee='" + entries.get(i).getEmployee().getCode() + "' and entry_date= to_date('" + dailydateString + "','dd-mm-yy HH24:MI') and in_out='"
                            + entries.get(i).getType() + "'";
                    Object obj = oem.executeEntityNativeQuery(checkSql, loggedUser);
                    if (obj == null) {
                        sql = "Insert into tm_clock_entry (organization_id,Employee,entry_date,in_out) values("
                                + entries.get(i).getEmployee().getUnit().getCompany().getDbid()
                                + ",'" + entries.get(i).getEmployee().getCode() + "',to_date('" + dailydateString + "','dd-mm-yy HH24:MI'),'" + entries.get(i).getType() + "')";
                        oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                        sql = "";

                    }
                    String updateDone = "update EMPLOYEEDAILYATTENDANCEIMPORT set done=0 where EMPLOYEE_DBID= "
                            + entries.get(i).getEmployee().getDbid()
                            + " and to_date(currentdate,'YYYY-MM-DD')=to_date('" + df.format(entries.get(i).getEntryDate()) + "','DD-MM-YYYY')";
                    oem.executeEntityUpdateNativeQuery(updateDone, loggedUser);

                }

                List<String> payConds = new ArrayList<String>();
                payConds.add("description = 'TM_MultiEntryEnabled'");
                PayrollParameter payrollParameter = (PayrollParameter) oem.loadEntity(PayrollParameter.class.getSimpleName(), payConds, null, loggedUser);
                if (payrollParameter != null
                        && payrollParameter.getValueData() != null
                        && payrollParameter.getValueData().equalsIgnoreCase("y")) {
                    return postImportedDAsME(odm, functionParms, loggedUser);
                }

                List<String> screenFilter = null;
                if (functionParms.getParams().get("ScreenFilter") != null) {
                    screenFilter = (List<String>) functionParms.getParams().get("ScreenFilter");
                }
                List conditions = new ArrayList();

                if (screenFilter != null) {
                    conditions.addAll(screenFilter);
                }
                conditions.add("done = false");
                //Fix for night-day shifts
                List<EmployeeDailyAttendanceImport> employeeDAFix = oem.loadEntityList(EmployeeDailyAttendanceImport.class.getSimpleName(),
                        conditions, null, null, loggedUser);

                for (int i = 0; i < employeeDAFix.size(); i++) {
                    EmployeeDailyAttendanceImport employeeDA = employeeDAFix.get(i);
                    fixEmployeeDailyAttendanceImport(employeeDA, loggedUser);
                }

                List<EmployeeDailyAttendanceImport> employeeDAList = oem.loadEntityList(EmployeeDailyAttendanceImport.class.getSimpleName(),
                        conditions, null, null, loggedUser);

                String dayDate = "";
                for (int i = 0; i < employeeDAList.size(); i++) {
                    EmployeeDailyAttendanceImport employeeDA = employeeDAList.get(i);

                    if (employeeDA.getCode() != null
                            && !employeeDA.getCode().equals("")
                            && !employeeDA.getCode().equals("NA")
                            && employeeDA.getInTime() != null
                            && !employeeDA.getInTime().equals("")
                            && !employeeDA.getInTime().equals("NA")
                            && employeeDA.getOutTime() != null
                            && !employeeDA.getOutTime().equals("")
                            && !employeeDA.getOutTime().equals("NA")) {

                        dayDate = employeeDA.getDate();

                        List<String> conds = new ArrayList<String>();
                        conds.add("dailyDate = '" + dayDate + "'");
                        conds.add("employee.code = '" + employeeDA.getCode() + "'");

                        EmployeeDailyAttendance empDA = (EmployeeDailyAttendance) oem.loadEntity(EmployeeDailyAttendance.class.getSimpleName(), conds, null, loggedUser);

                        if (empDA != null) {
                            if (empDA.getTimeIn() == null || empDA.getTimeIn() == ""
                                    && empDA.getTimeOut() == null || empDA.getTimeOut() == "") {
                                empDA.setTimeIn(employeeDA.getInTime());
                                empDA.setTimeOut(employeeDA.getOutTime());
                                empDA.setOutDate(employeeDA.getOutDate());
                                empDA.setExternalOrNot(employeeDA.getExternalOrNot());
                            } else {
                                empDA.setTimeOut(employeeDA.getOutTime());
                                empDA.setOutDate(employeeDA.getOutDate());
                                empDA.setExternalOrNot(employeeDA.getExternalOrNot());
                            }

                            if (employeeDA.getMachineCode() != null
                                    && !employeeDA.getMachineCode().equals("")
                                    && !employeeDA.getMachineCode().equals("NA")) {

                                TimeManagementMachine machine = (TimeManagementMachine) oem.loadEntity(TimeManagementMachine.class.getSimpleName(), Collections.singletonList("code = '" + employeeDA.getMachineCode() + "'"), null, loggedUser);

                                if (machine != null) {
                                    empDA.setAccessMachine(machine);
                                }
                            }

                            OFunctionResult empDAOfr = entitySetupService.callEntityUpdateAction(empDA, loggedUser);
                            ofr.append(empDAOfr);
                            if (empDAOfr.getErrors() == null || (empDAOfr.getErrors() != null && empDAOfr.getErrors().isEmpty())) {
                                employeeDA.setDone(true);
                                OFunctionResult employeeDataOFR = entitySetupService.callEntityUpdateAction(employeeDA, loggedUser);
                                ofr.append(employeeDataOFR);
                            }
                        }
                    }
                }
                oem.executeEntityUpdateNativeQuery("update TM_Multientry_import set posted=1 where posted=0", loggedUser);
            }
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofr;
    }

    private String calculateLessWorkDuringTheDayForSingleDay(String timeIn, String timeOut, Employee employee, Date dailydate, OUser loggedUser) {

        String lessWorkDuringTheDay = "00:00";
        DateFormat df = new SimpleDateFormat("dd-MM-YYYY");
        String dailydateString = df.format(dailydate);
        try {

            String sql = "select type,entrytime,entrydate from REPtm_clock_entry where employeecode ='"
                    + employee.getCode() + "' and TO_CHAR(to_date(ENTRYDATE,'DD-MM-YYYY'),'DD-MM-YYYY')='"
                    + dailydateString + "' and (REPLACE(ENTRYTIME,':','') >" + Integer.parseInt(timeIn.replace(":", ""))
                    + " or REPLACE(ENTRYTIME,':','')<" + Integer.parseInt(timeOut.replace(":", "")) + " ) order by PRINTORDER";
            List<Object[]> clockEntries = oem.executeEntityListNativeQuery(sql, loggedUser);

            String vacationHrsSql = "select to_char(startdate,'HH24:MI'),to_char(enddate,'HH24:MI') from EMPLOYEEDAYOFFREQUEST"
                    + " where VACATION_DBID in(select dbid from DAYOFF where UNITOFMEASURE like 'H')"
                    + " and  to_char(startdate,'dd-MM-YYYY') ='" + dailydateString + "' and  CANCELLED like 'N' and employee_dbid=" + employee.getDbid();
            List<Object[]> vacationHrs = oem.executeEntityListNativeQuery(vacationHrsSql, loggedUser);
            if (!clockEntries.isEmpty()) {
                for (int i = 0; i < clockEntries.size() - 1; i++) {
                    if (clockEntries.get(i)[0].toString().equalsIgnoreCase("O") && clockEntries.get(i + 1)[0].toString().equalsIgnoreCase("I")) {
                        if (vacationHrs.isEmpty()) {
                            EmployeeDayLessWork employeeDayLessWork = new EmployeeDayLessWork();
                            employeeDayLessWork.setEmployee(employee);
                            employeeDayLessWork.setEntryDate(dailydate);
                            employeeDayLessWork.setTimeOut(clockEntries.get(i)[1].toString());
                            employeeDayLessWork.setTimeIn(clockEntries.get(i + 1)[1].toString());
                            employeeDayLessWork.setPeriod(dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), clockEntries.get(i + 1)[1].toString()));
                            oem.saveEntity(employeeDayLessWork, loggedUser);
                            lessWorkDuringTheDay = dateTimeUtility.addTime(lessWorkDuringTheDay, dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), clockEntries.get(i + 1)[1].toString()));
                        } else {
                            boolean isCalculated = false;
                            for (int j = 0; j < vacationHrs.size(); j++) {
                                if (dateTimeUtility.isInterval(vacationHrs.get(j)[0].toString(), vacationHrs.get(j)[1].toString(), clockEntries.get(i)[1].toString())
                                        && dateTimeUtility.isInterval(vacationHrs.get(j)[0].toString(), vacationHrs.get(j)[1].toString(), clockEntries.get(i + 1)[1].toString())) {
                                    isCalculated = true;
                                    continue;
                                } else {
                                    EmployeeDayLessWork employeeDayLessWork = new EmployeeDayLessWork();
                                    employeeDayLessWork.setEmployee(employee);
                                    employeeDayLessWork.setEntryDate(dailydate);
                                    //if IN is Between interval but OUT is ont
                                    if (!dateTimeUtility.isInterval(vacationHrs.get(j)[0].toString(), vacationHrs.get(j)[1].toString(), clockEntries.get(i)[1].toString())
                                            && dateTimeUtility.isInterval(vacationHrs.get(j)[0].toString(), vacationHrs.get(j)[1].toString(), clockEntries.get(i + 1)[1].toString())) {

                                        employeeDayLessWork.setTimeOut(clockEntries.get(i)[1].toString());
                                        employeeDayLessWork.setTimeIn(vacationHrs.get(j)[0].toString());
                                        employeeDayLessWork.setPeriod(dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), vacationHrs.get(j)[0].toString()));
                                        oem.saveEntity(employeeDayLessWork, loggedUser);
                                        lessWorkDuringTheDay = dateTimeUtility.addTime(lessWorkDuringTheDay, dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), vacationHrs.get(j)[0].toString()));
                                        isCalculated = true;
                                        //if OUT is Between interval but IN is not
                                    } else if (dateTimeUtility.isInterval(vacationHrs.get(j)[0].toString(), vacationHrs.get(j)[1].toString(), clockEntries.get(i)[1].toString())
                                            && !dateTimeUtility.isInterval(vacationHrs.get(j)[0].toString(), vacationHrs.get(j)[1].toString(), clockEntries.get(i + 1)[1].toString())) {

                                        employeeDayLessWork.setTimeOut(vacationHrs.get(j)[1].toString());
                                        employeeDayLessWork.setTimeIn(clockEntries.get(i + 1)[1].toString());
                                        employeeDayLessWork.setPeriod(dateTimeUtility.subtractTime(vacationHrs.get(j)[1].toString(), clockEntries.get(i + 1)[1].toString()));
                                        oem.saveEntity(employeeDayLessWork, loggedUser);
                                        lessWorkDuringTheDay = dateTimeUtility.addTime(lessWorkDuringTheDay, dateTimeUtility.subtractTime(vacationHrs.get(j)[1].toString(), clockEntries.get(i + 1)[1].toString()));
                                    } else {
                                        if (j == vacationHrs.size() - 1 && !isCalculated) {
                                            employeeDayLessWork.setTimeOut(clockEntries.get(i)[1].toString());
                                            employeeDayLessWork.setTimeIn(clockEntries.get(i + 1)[1].toString());
                                            employeeDayLessWork.setPeriod(dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), clockEntries.get(i + 1)[1].toString()));
                                            oem.saveEntity(employeeDayLessWork, loggedUser);
                                            lessWorkDuringTheDay = dateTimeUtility.addTime(lessWorkDuringTheDay, dateTimeUtility.subtractTime(clockEntries.get(i)[1].toString(), clockEntries.get(i + 1)[1].toString()));
                                            isCalculated = true;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);

        }
        return lessWorkDuringTheDay;
    }

    private void fixEmployeeDailyAttendanceImport(EmployeeDailyAttendanceImport employeeDailyAttendanceImport, OUser loggedUser) {
        List<String> conditions = new ArrayList<String>();
        try {
            BigDecimal calDbid = (BigDecimal) oem.executeEntityNativeQuery("select WORKINGCALENDAR_DBID from employeedailyattendance where"
                    + " employee_dbid=" + employeeDailyAttendanceImport.getEmployee().getDbid()
                    + "and dailydate =" + dateTimeUtility.dateTimeFormatForOracle(employeeDailyAttendanceImport.getDate()), loggedUser);
            conditions.add("dbid=" + calDbid);
            NormalWorkingCalendar normalWorkingCalendar = (NormalWorkingCalendar) oem.loadEntity(NormalWorkingCalendar.class.getSimpleName(), conditions, null, loggedUser);
            String dayStartTime = normalWorkingCalendar.getStartDayTime();
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy ");
            DateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.setTime(df3.parse(employeeDailyAttendanceImport.getDate()));
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            int dayBefore = cal.get(Calendar.DAY_OF_MONTH) - 1;
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int dayAfter = cal.get(Calendar.DAY_OF_MONTH) + 1;
            Date timeIn = null;
            Date timeOut = null;
            String firstIn = null;
            String sqlIn = null;
            String lastOut = null;
            String sqlOut = null;
            String updateSql = "";
            int inDay = 0;
            String inDA = employeeDailyAttendanceImport.getInTime();
            String outDA = employeeDailyAttendanceImport.getOutTime();
            //dayBefore from dayBeforeStart to dayStart ----day from dayStart to dayAfterStart ---- dayAfter from dayAfterStart
            Date dayBeforeStart = df.parse(dayBefore + "-" + month + "-" + year + " " + dayStartTime);
            Date dayStart = df.parse(day + "-" + month + "-" + year + " " + dayStartTime);
            Date dayAfterStart = df.parse(dayAfter + "-" + month + "-" + year + " " + dayStartTime);
            Date dayAfterEnd = df.parse(dayAfter + 1 + "-" + month + "-" + year + " " + dayStartTime);

            if (inDA != null && !inDA.endsWith("NA")) {
                timeIn = df.parse(df1.format(df3.parse(employeeDailyAttendanceImport.getDate())) + " " + inDA);
                //daybefore
                if (timeIn.compareTo(dayBeforeStart) >= 0 && timeIn.compareTo(dayStart) < 0) {
                    inDay = 1;
                    sqlIn = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'I' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayBeforeStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE ) where rownum=1";

                    sqlOut = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'O' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayBeforeStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE DESC ) where rownum=1";

                    firstIn = (String) oem.executeEntityNativeQuery(sqlIn, loggedUser);
                    lastOut = (String) oem.executeEntityNativeQuery(sqlOut, loggedUser);
                    if (firstIn == null) {
                        firstIn = "NA";
                    }
                    if (lastOut == null) {
                        lastOut = "NA";
                    }
                    updateSql = "update EMPLOYEEDAILYATTENDANCEIMPORT set done=0,INTIME='" + firstIn + "' ,OUTTIME='" + lastOut
                            + "' where EMPLOYEE_DBID= " + employeeDailyAttendanceImport.getEmployee().getDbid()
                            + " and to_date(currentdate,'YYYY-MM-DD')=to_date('" + df1.format(dayBeforeStart) + "','DD-MM-YYYY')";
                    oem.executeEntityUpdateNativeQuery(updateSql, loggedUser);
                    //same day
                } else if (timeIn.compareTo(dayStart) >= 0 && timeIn.compareTo(dayAfterStart) < 0) {
                    inDay = 2;
                    sqlIn = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'I' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE ) where rownum=1";

                    sqlOut = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'O' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE DESC ) where rownum=1";

                    firstIn = (String) oem.executeEntityNativeQuery(sqlIn, loggedUser);
                    lastOut = (String) oem.executeEntityNativeQuery(sqlOut, loggedUser);

                    if (firstIn == null) {
                        firstIn = "NA";
                    }
                    if (lastOut == null) {
                        lastOut = "NA";
                    }
                    updateSql = "update EMPLOYEEDAILYATTENDANCEIMPORT set done=0,INTIME='" + firstIn + "' ,OUTTIME='" + lastOut
                            + "' where EMPLOYEE_DBID= " + employeeDailyAttendanceImport.getEmployee().getDbid()
                            + " and to_date(currentdate,'YYYY-MM-DD')=to_date('" + df1.format(dayStart) + "','DD-MM-YYYY')";
                    oem.executeEntityUpdateNativeQuery(updateSql, loggedUser);
                    //day after
                } else if (timeIn.compareTo(dayAfterStart) >= 0 && timeIn.compareTo(dayAfterEnd) < 0) {
                    inDay = 3;
                    sqlIn = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'I' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterEnd)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE ) where rownum=1";

                    sqlOut = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'O' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterEnd)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE DESC ) where rownum=1";

                    firstIn = (String) oem.executeEntityNativeQuery(sqlIn, loggedUser);
                    lastOut = (String) oem.executeEntityNativeQuery(sqlOut, loggedUser);
                    if (firstIn == null) {
                        firstIn = "NA";
                    }
                    if (lastOut == null) {
                        lastOut = "NA";
                    }
                    updateSql = "update EMPLOYEEDAILYATTENDANCEIMPORT set done=0,INTIME='" + firstIn + "' ,OUTTIME='" + lastOut
                            + "' where EMPLOYEE_DBID= " + employeeDailyAttendanceImport.getEmployee().getDbid()
                            + " and to_date(currentdate,'YYYY-MM-DD')=to_date('" + df1.format(dayAfterStart) + "','DD-MM-YYYY')";
                    oem.executeEntityUpdateNativeQuery(updateSql, loggedUser);
                }
            }
            if (outDA != null && !outDA.endsWith("NA")) {
                timeOut = df.parse(df1.format(df3.parse(employeeDailyAttendanceImport.getDate())) + " " + outDA);
                //daybefore
                if (timeOut.compareTo(dayBeforeStart) >= 0 && timeOut.compareTo(dayStart) < 0 && inDay != 1) {
                    sqlIn = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'I' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayBeforeStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE ) where rownum=1";

                    sqlOut = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'O' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayBeforeStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE DESC ) where rownum=1";

                    firstIn = (String) oem.executeEntityNativeQuery(sqlIn, loggedUser);
                    lastOut = (String) oem.executeEntityNativeQuery(sqlOut, loggedUser);
                    if (firstIn == null) {
                        firstIn = "NA";
                    }
                    if (lastOut == null) {
                        lastOut = "NA";
                    }
                    updateSql = "update EMPLOYEEDAILYATTENDANCEIMPORT set done=0,INTIME='" + firstIn + "' ,OUTTIME='" + lastOut
                            + "' where EMPLOYEE_DBID= " + employeeDailyAttendanceImport.getEmployee().getDbid()
                            + " and to_date(currentdate,'YYYY-MM-DD')=to_date('" + df1.format(dayBeforeStart) + "','DD-MM-YYYY')";
                    oem.executeEntityUpdateNativeQuery(updateSql, loggedUser);

                    //same day
                } else if (timeOut.compareTo(dayStart) >= 0 && timeOut.compareTo(dayAfterStart) < 0 && inDay != 2) {
                    sqlIn = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'I' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE ) where rownum=1";

                    sqlOut = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'O' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE DESC ) where rownum=1";

                    firstIn = (String) oem.executeEntityNativeQuery(sqlIn, loggedUser);
                    lastOut = (String) oem.executeEntityNativeQuery(sqlOut, loggedUser);
                    if (firstIn == null) {
                        firstIn = "NA";
                    }
                    if (lastOut == null) {
                        lastOut = "NA";
                    }
                    updateSql = "update EMPLOYEEDAILYATTENDANCEIMPORT set done=0,INTIME='" + firstIn + "' ,OUTTIME='" + lastOut
                            + "' where EMPLOYEE_DBID= " + employeeDailyAttendanceImport.getEmployee().getDbid()
                            + " and to_date(currentdate,'YYYY-MM-DD')=to_date('" + df1.format(dayStart) + "','DD-MM-YYYY')";
                    oem.executeEntityUpdateNativeQuery(updateSql, loggedUser);
                    //day after
                } else if (timeOut.compareTo(dayAfterStart) >= 0 && timeOut.compareTo(dayAfterEnd) < 0 && inDay != 3) {

                    sqlIn = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'I' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterEnd)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE ) where rownum=1";

                    sqlOut = "select * from (select to_char(ENTRY_DATE,'HH24:MI') from TM_CLOCK_ENTRY where IN_OUT like 'O' and employee='"
                            + employeeDailyAttendanceImport.getEmployee().getCode() + "' and ENTRY_DATE between To_Date('"
                            + df.format(dayAfterStart)
                            + "','DD-MM-YYYY HH24:MI') and To_Date('" + df.format(dayAfterEnd)
                            + "','DD-MM-YYYY HH24:MI') order by ENTRY_DATE DESC ) where rownum=1";

                    firstIn = (String) oem.executeEntityNativeQuery(sqlIn, loggedUser);
                    lastOut = (String) oem.executeEntityNativeQuery(sqlOut, loggedUser);
                    if (firstIn == null) {
                        firstIn = "NA";
                    }
                    if (lastOut == null) {
                        lastOut = "NA";
                    }
                    updateSql = "update EMPLOYEEDAILYATTENDANCEIMPORT set done=0,INTIME='" + firstIn + "' ,OUTTIME='" + lastOut
                            + "' where EMPLOYEE_DBID= " + employeeDailyAttendanceImport.getEmployee().getDbid()
                            + " and to_date(currentdate,'YYYY-MM-DD')=to_date('" + df1.format(dayAfterStart) + "','DD-MM-YYYY')";
                    oem.executeEntityUpdateNativeQuery(updateSql, loggedUser);

                } else {
                    if (inDay == 1 || inDay == 3) {
                        employeeDailyAttendanceImport.setInTime("NA");
                        oem.saveEntity(employeeDailyAttendanceImport, loggedUser);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean isWorkOTEligible(EmployeeDailyAttendance empDA, EmployeeProfileHistory eph) {
        if (eph.getPosition() != null) {
            if (eph.getPosition().getCategory() != null) {
                if (eph.getPosition().getCategory().getCustom1() == null) {
                    eph.getPosition().getCategory().setCustom1("-");
                }
            }
            if (eph.getDriverType() != null) {
                if (eph.getDriverType().getCustom1() == null) {
                    eph.getDriverType().setCustom1("-");
                }
            }
            if (!(eph.getType() == null && eph.getPosition().isBlueCollar()
                    && (eph.getPosition().getCategory() == null || !eph.getPosition().getCategory().getCustom1().equalsIgnoreCase("M"))
                    && (eph.getDriverType() == null || eph.getDriverType().getCode() == null || eph.getDriverType().getCustom1().equalsIgnoreCase("pool")))
                    && !(eph.getType() == null && (eph.getDriverType() == null || eph.getDriverType().getCustom1().equalsIgnoreCase("pool"))
                    && (eph.getPosition().getCategory() == null || !eph.getPosition().getCategory().getCustom1().equalsIgnoreCase("M")))
                    || (!eph.getPosition().isBlueCollar() && eph.getPosition().getCategory() == null && eph.getDriverType() == null)) {
                return false;
            }
        }
        return true;
    }

    private boolean isHolidayOTCompensationEligible(EmployeeProfileHistory eph) {
        if (eph.getPosition() != null) {
            if (eph.getPosition().getCategory() != null) {
                if (eph.getPosition().getCategory().getCustom1() == null) {
                    eph.getPosition().getCategory().setCustom1("-");
                }
            }
            if (!(eph.getType() == null && (eph.getPosition().getCategory() != null && eph.getPosition().getCategory().getCustom1().equalsIgnoreCase("M")))) {
                return false;
            }
        }
        return true;
    }

    private boolean isHolidayOTPayrollEligible(EmployeeProfileHistory eph) {
        if (eph.getPosition() != null) {
            if (eph.getPosition().getCategory() != null) {
                if (eph.getPosition().getCategory().getCustom1() == null) {
                    eph.getPosition().getCategory().setCustom1("-");
                }
            }
            if (eph.getType() != null || (eph.getPosition().getCategory() != null && eph.getPosition().getCategory().getCustom1().equalsIgnoreCase("M"))) {
                return false;
            }
        }
        return true;
    }

    private boolean isWeekendOTExceedLimit(EmployeeOTRequest request, OUser loggedUser) {
        if (request.getEmployee().getPositionSimpleMode().getLegalEntity().getMaxWeekendOT() == null) {
            return false;
        }
        try {

            BigDecimal maxOT = request.getEmployee().getPositionSimpleMode().getLegalEntity().getMaxWeekendOT();
            Integer month = Integer.parseInt(dateTimeUtility.MONTH_FORMAT.format(request.getRequestedDate()).toString());
            Integer year = Integer.parseInt(dateTimeUtility.YEAR_FORMAT.format(request.getRequestedDate()).toString());
            Integer day = Integer.parseInt(dateTimeUtility.DAY_FORMAT.format(request.getRequestedDate()).toString());

            String[] dates = timeManagementService.getCutOffDatesFromCalculatedPeriod(month, year, day, loggedUser);
            String startDateStr = dates[0];
            String endDateStr = dates[1];
            String SQLDA;
            String SQLREQ;
            String SQLWE;
            SQLWE = "Select dailydate from EmployeeDailyAttendance where employee_dbid = " + request.getEmployee().getDbid()
                    + " AND dailyDate >=";
            SQLWE += dateTimeUtility.dateTimeFormatForOracle(startDateStr);
            SQLWE += " AND dailyDate <= ";
            SQLWE += dateTimeUtility.dateTimeFormatForOracle(endDateStr);
            SQLWE += " and daynature_dbid in(select dbid from udc where code like'%WeekEnd%')";

            SQLDA = "Select sum(APPROVEDWEEKENDOT) from EmployeeDailyAttendance where employee_dbid = " + request.getEmployee().getDbid()
                    + " AND dailyDate >=";
            SQLDA += dateTimeUtility.dateTimeFormatForOracle(startDateStr);
            SQLDA += " AND dailyDate <= ";
            SQLDA += dateTimeUtility.dateTimeFormatForOracle(endDateStr);
            BigDecimal weekEndDAOT = (BigDecimal) oem.executeEntityNativeQuery(SQLDA, loggedUser);
            SQLDA = "Select dailydate from EmployeeDailyAttendance where employee_dbid = " + request.getEmployee().getDbid()
                    + " AND dailyDate >=";
            SQLDA += dateTimeUtility.dateTimeFormatForOracle(startDateStr);
            SQLDA += " AND dailyDate <= ";
            SQLDA += dateTimeUtility.dateTimeFormatForOracle(endDateStr);
            SQLDA += " and timeIn is not null and timeIn!='00:00'";
            SQLREQ = "select timeto,timefrom from employeeotrequest where REQUESTEDDATE >=";
            SQLREQ += dateTimeUtility.dateTimeFormatForOracle(startDateStr);
            SQLREQ += " AND REQUESTEDDATE <= ";
            SQLREQ += dateTimeUtility.dateTimeFormatForOracle(endDateStr);
            SQLREQ += " AND ((REQUESTEDDATE not in (";
            SQLREQ += SQLDA;
            SQLREQ += ") and APPROVED ='Y')or APPROVED='P') and REQUESTEDDATE in (";
            SQLREQ += SQLWE;
            SQLREQ += ") and employee_dbid=";
            SQLREQ += request.getEmployee().getDbid();
            List<Object[]> reqs = oem.executeEntityListNativeQuery(SQLREQ, loggedUser);
            BigDecimal weekEndReqOT = BigDecimal.ZERO;
            double period;
            for (int i = 0; i < reqs.size(); i++) {
                period = dateTimeUtility.convertToNumber(dateTimeUtility.subtractTime(reqs.get(i)[1].toString(), reqs.get(i)[0].toString()));
                weekEndReqOT = weekEndReqOT.add(BigDecimal.valueOf(period));
            }
            Double requestPeriod = dateTimeUtility.convertToNumber(dateTimeUtility.subtractTime(request.getTimeFrom(), request.getTimeTo()));
            BigDecimal totalWEOT = BigDecimal.valueOf(requestPeriod);

            if (weekEndDAOT != null) {
                totalWEOT = totalWEOT.add(weekEndDAOT);
            }
            if (weekEndReqOT != null) {
                totalWEOT = totalWEOT.add(weekEndReqOT);
            }
            return totalWEOT.compareTo(maxOT) > 0;
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }

    private void weekendMissionVacationCompensation(EmployeeDailyAttendance empDA, boolean isWeekendOTEligible, OUser loggedUser) {
        try {
            String oldDayNatureDbid = oem.executeEntityNativeQuery("select DAYNATURE_DBID from EMPLOYEEDAILYATTENDANCE WHERE DBID = " + empDA.getDbid(), loggedUser).toString();
            List<String> conds = new ArrayList<String>();
            conds = new ArrayList<String>();
            conds.add("dbid=" + oldDayNatureDbid);
            UDC OlddayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
            if (empDA.getDayNature().getCode().equalsIgnoreCase("Mission") && OlddayNature.getCode().contains("WeekEnd")) {

                String sqlMission = "SELECT edo.DBID FROM employeedayoffrequest edo, dayoff do"
                        + " WHERE do.DBID = edo.VACATION_DBID"
                        + " AND do.mission = 1"
                        + " AND do.UNITOFMEASURE like 'D'"
                        + " AND CANCELLED = 'N' and " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate())) + " between edo.STARTDATE AND edo.ENDDATE"
                        + " AND edo.employee_dbid = " + empDA.getEmployee().getDbid();
                BigDecimal missionDbid = (BigDecimal) oem.executeEntityNativeQuery(sqlMission, loggedUser);
                conds.clear();
                conds.add("dbid =" + missionDbid);
                EmployeeVacationRequest request = (EmployeeVacationRequest) oem.loadEntity(EmployeeDayOffRequest.class.getSimpleName(), conds, null, loggedUser);
                String sql = "DELETE FROM EmployeeVacationAdjustment"
                        + " where employee_dbid = " + empDA.getEmployee().getDbid()
                        + " and CREATIONDATE = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()))
                        + " and Vacation_dbid = " + request.getVacation().getWeekendVacCompensation().getDbid();
                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                if (request.getVacation().isWeekendMissionCompensation()) {
                    if (request.getVacation().isWeekendEligibleOnly()) {
                        if (isWeekendOTEligible) {
                            EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                            adjustment.setEmployee(empDA.getEmployee());
                            adjustment.setVacation(request.getVacation().getWeekendVacCompensation());
                            adjustment.setCreationDate(empDA.getDailyDate());
                            adjustment.setAdjustmentValue(request.getVacation().getWeekendCompensationValue());
                            adjustment.setComments("this record has been added automatically because of  working on weekend");
                            oem.saveEntity(adjustment, loggedUser);
                        }
                    } else {
                        EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                        adjustment.setEmployee(empDA.getEmployee());
                        adjustment.setVacation(request.getVacation().getWeekendVacCompensation());
                        adjustment.setCreationDate(empDA.getDailyDate());
                        adjustment.setAdjustmentValue(request.getVacation().getWeekendCompensationValue());
                        adjustment.setComments("this record has been added automatically because of  working on weekend");
                        oem.saveEntity(adjustment, loggedUser);
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    @Override
    public OFunctionResult updateWorkingCalendarAfterModifyOrCancel(ODataMessage odm,
            OFunctionParms functionParms, OUser loggedUser) {
        OFunctionResult ofr = new OFunctionResult();
        try {
            List<String> conds = new ArrayList<String>();
            EmployeeVacationRequest employeeVacationRequest = (EmployeeVacationRequest) odm.getData().get(0);
            odm.getData().get(0);
            conds.add("employee.dbid = " + employeeVacationRequest.getEmployee().getDbid());
            conds.add("currentEffectiveDate <= '" + dateTimeUtility.DATE_FORMAT.format(employeeVacationRequest.getStartDate()) + "'");
            conds.add("currentEffectiveEndDate >= '" + dateTimeUtility.DATE_FORMAT.format(employeeVacationRequest.getEndDate()) + "' OR entity.currentEffectiveEndDate is null");
            EmployeeWorkingCalHistory empCalHistory = (EmployeeWorkingCalHistory) oem.loadEntity(EmployeeWorkingCalHistory.class.getSimpleName(), conds, null, loggedUser);
            if (empCalHistory != null) {
//                empCalHistory.setCurrentEffectiveDate(employeeVacationRequest.getStartDate());
                //              empCalHistory.setCurrentEffectiveEndDate(employeeVacationRequest.getEndDate());
                calendarUpdate(ofr, empCalHistory, false, loggedUser, false);
            }
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofr;
    }

    private void holidayMissionVacationCompensation(EmployeeDailyAttendance empDA, boolean isHolidayOTEligible, OUser loggedUser) {
        try {
            String oldDayNatureDbid = oem.executeEntityNativeQuery("select DAYNATURE_DBID from EMPLOYEEDAILYATTENDANCE WHERE DBID = " + empDA.getDbid(), loggedUser).toString();
            List<String> conds = new ArrayList<String>();
            conds = new ArrayList<String>();
            conds.add("dbid=" + oldDayNatureDbid);
            UDC OlddayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
            if (empDA.getDayNature().getCode().equalsIgnoreCase("Mission") && OlddayNature.getCode().contains("Holiday")) {

                String sqlMission = "SELECT edo.DBID FROM employeedayoffrequest edo, dayoff do"
                        + " WHERE do.DBID = edo.VACATION_DBID"
                        + " AND do.mission = 1"
                        + " AND do.UNITOFMEASURE like 'D'"
                        + " AND CANCELLED = 'N' and " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate())) + " between edo.STARTDATE AND edo.ENDDATE"
                        + " AND edo.employee_dbid = " + empDA.getEmployee().getDbid();
                BigDecimal missionDbid = (BigDecimal) oem.executeEntityNativeQuery(sqlMission, loggedUser);
                conds.clear();
                conds.add("dbid =" + missionDbid);
                EmployeeVacationRequest request = (EmployeeVacationRequest) oem.loadEntity(EmployeeDayOffRequest.class.getSimpleName(), conds, null, loggedUser);
                String sql = "DELETE FROM EmployeeVacationAdjustment"
                        + " where employee_dbid = " + empDA.getEmployee().getDbid()
                        + " and CREATIONDATE = " + dateTimeUtility.dateTimeFormatForOracle(dateTimeUtility.DATE_FORMAT.format(empDA.getDailyDate()))
                        + " and Vacation_dbid = " + request.getVacation().getHolidayVacCompensation().getDbid();
                oem.executeEntityUpdateNativeQuery(sql, loggedUser);
                if (request.getVacation().isHolidayMissionCompensation()) {
                    if (request.getVacation().isHolidatEligibleOnly()) {
                        if (isHolidayOTEligible) {
                            EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                            adjustment.setEmployee(empDA.getEmployee());
                            adjustment.setVacation(request.getVacation().getHolidayVacCompensation());
                            adjustment.setCreationDate(empDA.getDailyDate());
                            adjustment.setAdjustmentValue(request.getVacation().getHolidayCompensationValue());
                            adjustment.setComments("this record has been added automatically because of  working on Holiday");
                            oem.saveEntity(adjustment, loggedUser);
                        }
                    } else {
                        EmployeeVacationAdjustment adjustment = new EmployeeVacationAdjustment();
                        adjustment.setEmployee(empDA.getEmployee());
                        adjustment.setVacation(request.getVacation().getHolidayVacCompensation());
                        adjustment.setCreationDate(empDA.getDailyDate());
                        adjustment.setAdjustmentValue(request.getVacation().getHolidayCompensationValue());
                        adjustment.setComments("this record has been added automatically because of  working on Holiday");
                        oem.saveEntity(adjustment, loggedUser);
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    private EmployeeDailyAttendance testDrools(EmployeeDailyAttendance empDA, OUser loggedUser) {
        List<EmployeeDailyAttendanceD> all = new ArrayList();
        List<EmployeeDailyAttendanceD> data = new ArrayList<>();

        OverTimeSegmentD otNightSegment = new OverTimeSegmentD();

        otNightSegment.setTimeFrom(10);
        otNightSegment.setTimeTo(20);
        OverTimeSegmentD otDaySegment = new OverTimeSegmentD();
        otDaySegment.setTimeFrom(7);
        otDaySegment.setTimeTo(9);

        CalendarD calendar = new CalendarD();

        calendar.setName("Calendar One");
        calendar.setOtNightSegment(otNightSegment);

        calendar.setOtDaySegment(otDaySegment);

        calendar.setPlannedInHour(dateTimeUtility.convertToNumber(empDA.getWorkingCalendar().getTimeIn()).intValue());
        calendar.setPlannedOutHour(dateTimeUtility.convertToNumber(empDA.getWorkingCalendar().getTimeOut()).intValue());
        // calendar.setDelayTolerance(1);
        EmployeeD employee = new EmployeeD();

        employee.setName("Aly");
        employee.setCalendar(calendar);
        EmployeeDailyAttendanceD eda = new EmployeeDailyAttendanceD();
        eda.setEmployee(employee);

        eda.setTimeInHour(dateTimeUtility.convertToNumber(empDA.getTimeIn()).intValue());
        eda.setTimeOutHour(dateTimeUtility.convertToNumber(empDA.getTimeOut()).intValue());
        eda.setDayNature(empDA.getDayNature().getCode());
        data.add(eda);
        all.addAll(data);
        DroolsWebService_Service service = new DroolsWebService_Service();
        all = service.getDroolsWebServicePort().fireRules(all);
        empDA.setDelayHoursDisplay("0" + all.get(0).getDelay() + ":00");
        empDA.setDayOverTimeDisplay("0" + all.get(0).getOverTimeBeforeDayStart() + ":00");
        empDA.setNightOverTimeDisplay("0" + all.get(0).getOverTimeAfterDayEnd() + ":00");
        empDA.setEarlyLeaveDisplay("0" + all.get(0).getEarlyLeave() + ":00");
        empDA.setPlannedIn(empDA.getWorkingCalendar().getTimeIn());
        empDA.setPlannedOut(empDA.getWorkingCalendar().getTimeOut());
        List<String> conds = new ArrayList<String>();
        if (all.get(0).getDayNature().equalsIgnoreCase("Working on WeekEnd")) {
            conds.add("code='WorkingOnWeekEnd'");
        } else {
            conds.add("code='Working'");
        }

        try {
            UDC dayNature = (UDC) oem.loadEntity(UDC.class.getSimpleName(), conds, null, loggedUser);
            empDA.setDayNature(dayNature);
        } catch (Exception ex) {
            Logger.getLogger(CalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empDA;
    }
}
